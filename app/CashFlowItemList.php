<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashFlowItemList extends Model
{
    protected $table = "cash_flow_item_list";

    public $timestamps = false;

    protected $guarded = [];
}

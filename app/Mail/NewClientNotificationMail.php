<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewClientNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    private $companyCode;
    private $companyName;
    private $effectivityDate;
    private $adminFeeType;
    private $adminFee;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($company)
    {
        $this->companyCode = $company->company_code;
        $this->companyName = $company->company_name;
        $this->effectivityDate = date('F d, Y', strtotime($company->client_start_date));
        $this->adminFeeType = $company->admin_fee_type;
        $this->adminFee = $company->admin_fee_type === 'Fixed' ? $company->admin_fee : "{$company->admin_fee_percentage}%";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['companyCode'] = $this->companyCode;
        $data['companyName'] = $this->companyName;
        $data['effectivityDate'] = $this->effectivityDate;
        $data['adminFeeType'] = $this->adminFeeType;
        $data['adminFee'] = $this->adminFee;

        $to = \App\Keyval::where('key', 'email_address')->first()->value;
//        $to = 'jimson.d.marcos@gmail.com';

        return $this->from('admin@gtmcportal.com', 'Admin')
            ->to($to)
            ->subject("NOTIFICATION: New Client Added Successfully - {$data['companyCode']}")
            ->markdown('emails.new_client_notification')->with($data);
    }
}

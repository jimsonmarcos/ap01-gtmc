<?php

namespace App\Mail;

use App\Keyval;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PasswordResetMail extends Mailable
{
    use Queueable, SerializesModels;

    private $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['name'] = $this->user->name;
        $data['username'] = $this->user->username;

        $to = !empty($this->user->email) ? $this->user->email : Keyval::where('key', 'email_address')->first()->value;

        return $this->from('admin@gtmcportal.com', 'Admin')
            ->to($to)
            ->subject("Your GTMC Portal password is successfully reset")
            ->markdown('emails.password_reset')->with($data);
    }
}

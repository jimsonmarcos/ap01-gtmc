<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PremiumRates extends Model
{
    protected $table = "premium_rates";
    public $timestamps = false;

    protected $fillable = [
        'tag',
        'ot',
        'premium'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayeeNames extends Model
{
    protected $table = "payee_names";
    public $timestamps = false;

    protected $fillable = [
        'id',
        'payee_name',
        'created_by'
    ];
}

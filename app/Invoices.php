<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    protected $table = "invoices";
    public $timestamps = false;

    protected $fillable = [
        'invoice_number',
        'invoice_terms_id',
        'company_id',
        'contact_person_id',
        'prepared_by',
        'approved_by',
        'bill_to',
        'company_name',
        'company_address',
        'invoice_date',
        'due_date',
        'total_amount',
        'note_to_recipient',
        'terms_and_conditions',
        'status',
        'reference',
        'memo'
    ];

    public function Terms()
    {
        return $this->belongsTo('App\InvoiceTerms', 'invoice_terms_id');
    }

    public function Company()
    {
        return $this->belongsTo('App\Companies', 'company_id');
    }

    public function ContactPerson()
    {
        return $this->belongsTo('App\CompaniesContactPersons', 'contact_person_id');
    }

    public function PreparedBy()
    {
        return $this->belongsTo('App\User', 'prepared_by');
    }

    public function ApprovedBy()
    {
        return $this->belongsTo('App\User', 'approved_by');
    }

    public function Items()
    {
        return $this->hasMany('App\InvoiceItems', 'invoice_id', 'id');
    }

    public function Files()
    {
        return $this->hasMany('App\InvoiceFiles', 'invoice_id', 'id');
    }
}

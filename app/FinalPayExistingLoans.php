<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinalPayExistingLoans extends Model
{
    protected $table = "final_pay_existing_loans";
    public $timestamps = false;

    protected $guarded = [];


    public function Loan()
    {
        return $this->belongsTo('App\Loans', 'loan_id', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckRequestItems extends Model
{
    protected $table = "check_request_items";

    protected $fillable = [
        'check_request_id',
        'cash_flow_item',
        'description',
        'particulars',
        'reference_number',
        'amount',
        'created_at',
        'updated_at'
    ];

    public function CashFlowItem() {
        return $this->belongsTo('App\CashFlowItems', 'cash_flow_item_id');
    }
}

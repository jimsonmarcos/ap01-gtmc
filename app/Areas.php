<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    protected $table = "areas";
    public $timestamps = false;

    protected $fillable = [
        'province_id',
        'area'
    ];

    public function Province()
    {
        return $this->belongsTo('App\Provinces', 'province_id');
    }
}
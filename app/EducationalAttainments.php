<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationalAttainments extends Model
{
    protected $table = "educational_attainments";
    public $timestamps = false;

    protected $fillable = [
        'title'
    ];
}
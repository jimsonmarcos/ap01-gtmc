<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveHistory extends Model
{
    protected $table = "leave_history";

    protected $fillable = [
        'user_id',
        'payroll_cycle_id',
        'leave_type',
        'days',
        'hours',
        'status'
    ];

    public function PayrollCycle()
    {
        return $this->belongsTo('App\PayrollCycle', 'payroll_cycle_id', 'id');
    }
}

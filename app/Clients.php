<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    protected $table = "clients";
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'company_id',
        'first_name',
        'middle_name',
        'last_name',
        'email'
    ];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function Company()
    {
        return $this->belongsTo('App\Companies', 'company_id', 'id');
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outlets extends Model
{
    protected $table = "outlets";
    public $timestamps = false;

    protected $fillable = [
        'outlet'
    ];
}
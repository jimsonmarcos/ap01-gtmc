<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signatories extends Model
{
    protected $table = "signatories";

    protected $fillable = [
        'user_id',
        'added_by',
        'position_id',
        'created_at',
        'updated_at'
    ];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function Position()
    {
        return $this->belongsTo('App\Positions', 'position_id', 'id');
    }
}

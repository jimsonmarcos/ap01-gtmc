<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compensations extends Model
{
    protected $table = "compensations";
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'client_id',
        'daily_rate_id',
        'daily_location_rate_id',
        'transfer_mode_id',
        'rate_type',
        'daily_category',
        'account_number',
        'mobile_number',
        'monthly_rate',
        'daily_rate',
        'hourly',
        'payroll_group_id',
        'sss',
        'compute_sss',
        'hdmf',
        'compute_hdmf',
        'philhealth',
        'compute_philhealth',
        'tin',
        'tax_type_id',
        'ecola_id',
        'fixed_allowance',
        'de_minimis',
        'membership_fee',
        'notarial_fee',
        'deduct_withholding'
    ];

//    protected $hidden = [
//        'id',
//        'user_id'
//    ];

    public function TaxType()
    {
        return $this->belongsTo('App\TaxTypes', 'tax_type_id');
    }

    public function DailyRate()
    {
        return $this->belongsTo('App\DailyRates', 'daily_rate_id');
    }

    public function PayrollGroup()
    {
        return $this->belongsTo('App\PayrollGroups', 'payroll_group_id');
    }

    public function TransferMode()
    {
        return $this->belongsTo('App\TransferModes', 'transfer_mode_id');
    }

    public function Company()
    {
        return $this->belongsTo('App\Companies', 'client_id', 'id');
    }
}
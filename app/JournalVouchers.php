<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JournalVouchers extends Model
{
    protected $table = "journal_vouchers";

    protected $guarded = [];

    public function User() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function Items() {
        return $this->hasMany('App\JournalVoucherItems', 'journal_voucher_id', 'id');
    }
}

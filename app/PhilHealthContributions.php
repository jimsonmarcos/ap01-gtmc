<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhilHealthContributions extends Model
{
    protected $table = "philhealth_contributions";
    public $timestamps = false;

    protected $fillable = [
        'salary_range_start',
        'salary_range_end',
        'salary_base',
        'total_monthly_premium',
        'premium_rate',
        'employee_share',
        'employer_share'
    ];
}

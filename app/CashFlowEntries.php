<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashFlowEntries extends Model
{
    protected $table = "cash_flow_entries";

    public $timestamps = false;

    protected $guarded = [];
}

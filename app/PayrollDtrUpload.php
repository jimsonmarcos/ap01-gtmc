<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollDtrUpload extends Model
{
    protected $table = "payroll_dtr_upload";

    protected $fillable = [
        'user_id',
        'payroll_cycle_id',
        'filename',
        'filepath'
    ];

    public function PayrollCycle()
    {
        return $this->belongsTo('App\PayrollCycle', 'payroll_cycle_id', 'id');
    }
}

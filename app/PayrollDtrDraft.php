<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollDtrDraft extends Model
{
    protected $table = "payroll_dtr_draft";

    protected $fillable = [
        'payroll_cycle_id',
        'payroll_dtr_upload_id',
        'user_id',
        'id_number',
        'employee_name',
        'wh',
        'rds',
        'spe',
        'sperd',
        'leg',
        'legrd',
        'reg_ot',
        'nd',
        'abs',
        'lt',
        'ut',
        'vl',
        'sl',
        'el',
        'spl',
        'ml',
        'pl',
        'pto',
        'bl',
        'cl',
        'status',
        'updated_by',
        'source',
        'nwage',
        'incentive',
        'adji',
        'adjd',
        'client_charges'
    ];

    public function Upload()
    {
        return $this->belongsTo('App\PayrollDtrUpload', 'payroll_dtr_upload_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function UpdatedBy()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }
}

<?php

namespace App;

class ComputePayroll
{
    public $wh;
    public $rd;
    public $spe;
    public $sperd;
    public $leg;
    public $legrd;
    public $regot;
    public $nd;
    public $abs;
    public $lt;
    public $ut;
    public $vl;
    public $sl;
    public $el;
    public $spl;
    public $ml;
    public $pl;
    public $pto;
    public $bl;
    public $cl;
    public $dailyRate;
    public $monthlyRate = 0;
    public $isMonthlyRate;

    function __construct($data, $dailyRate) {
        $this->dailyRate = is_array($dailyRate) ? $dailyRate['daily_rate'] : $dailyRate;
        $this->monthlyRate = is_array($dailyRate) ? $dailyRate['monthly_rate'] : $dailyRate;
        $this->isMonthlyRate = is_array($dailyRate) ? 'Y' : 'N';

        $this->wh = !empty($data->wh) ? $data->wh : 0;
        $this->rd = !empty($data->rds) ? $data->rds : 0;
        $this->spe = !empty($data->spe) ? $data->spe : 0;
        $this->sperd = !empty($data->sperd) ? $data->sperd : 0;
        $this->leg = !empty($data->leg) ? $data->leg : 0;
        $this->legrd = !empty($data->legrd) ? $data->legrd : 0;
        $this->regot = !empty($data->reg_ot) ? $data->reg_ot : 0;
        $this->nd = !empty($data->nd) ? $data->nd : 0;
        $this->abs = !empty($data->abs) ? $data->abs : 0;
        $this->lt = !empty($data->lt) ? $data->lt : 0;
        $this->ut = !empty($data->ut) ? $data->ut : 0;
        $this->vl = !empty($data->vl) ? $data->vl : 0;
        $this->sl = !empty($data->sl) ? $data->sl : 0;
        $this->el = !empty($data->el) ? $data->el : 0;
        $this->spl = !empty($data->spl) ? $data->spl : 0;
        $this->ml = !empty($data->ml) ? $data->ml : 0;
        $this->pl = !empty($data->pl) ? $data->pl : 0;
        $this->pto = !empty($data->pto) ? $data->pto : 0;
        $this->bl = !empty($data->bl) ? $data->bl : 0;
        $this->cl = !empty($data->cl) ? $data->cl : 0;
    }

    public function hourlyRate()
    {
        return $this->dailyRate / 8;
    }

    public function rg()
    {
        return ($this->wh - ($this->wh % 8));
//        (WH-MOD(WH,8))-
//        (RD HRS-MOD(RD HRS,8))-
//          (SPE HRS-MOD(SPE HRS,8))-
//          (SPERD HRS-MOD(SPERD HRS,8))-
//          (LEG HRS-MOD(LEG HRS,8))-
//          (LEGRD HRS-MOD(LEGRD HRS,8))
//        return ($this->wh - ($this->wh % 8)) - ($this->rd - ($this->rd % 8)) - ($this->spe - ($this->spe % 8)) - ($this->sperd - ($this->sperd % 8)) - ($this->leg - ($this->leg % 8)) - ($this->legrd - ($this->legrd % 8));
//        return (($this->wh - ($this->wh % 8))) + ($this->vl + $this->sl + $this->el + $this->spl + $this->ml + $this->pl + $this->pto + $this->bl + $this->cl);
    }

    public function rd()
    {
        return ($this->rd > 0) ? (($this->premiumRd()) * $this->rd) : 0;
        // New Computation 03/28/2018
//        return ($this->rd - ($this->rd % 8)) + ($this->sperd - ($this->sperd % 8)) + ($this->legrd - ($this->legrd % 8));
    }

    public function spe()
    {
        $spe1 = ($this->premiumSpe() > 0) ? ($this->premiumSpe()) * ($this->spe - ($this->spe % 8)) : 0;
        $spe2 = ($this->spe % 8 > 0) ? ($this->premiumSpe() + $this->OTPremiumSpe()) * ($this->spe % 8) : 0;
        $speFinal = $spe1 + $spe2;
        return $speFinal;
        // New Computation 03/28/2018
//        return ($this->spe - ($this->spe % 8)) + ($this->sperd - ($this->sperd % 8));
    }

    public function sperd()
    {
//        [IF(PREM SPERD RATE>0,((HOURLY RATE+PREM SPERD RATE)*(SPERD HRS-MOD(SPERD HRS,8))),0)]+
//          IF(MOD(SPERD HRS,8)>0,((HOURLY RATE+PREM SPERD RATE+OT PREM SPERD RATE)*MOD(SPERD HRS,8)),0)

        $sperd1 = ($this->premiumSperd() > 0) ? ($this->premiumSperd()) * ($this->sperd - ($this->sperd % 8)) : 0;
        $sperd2 = (($this->sperd % 8) > 0) ? ($this->premiumSperd() + $this->OTPremiumSperd()) * ($this->sperd % 8) : 0;
        $sperdFinal = $sperd1 + $sperd2;

        return $sperdFinal;
    }

    public function leg()
    {
        return ($this->premiumLeg() > 0 ? (($this->premiumLeg()) * ($this->leg - ($this->leg % 8))) : 0) + (($this->leg % 8) > 0 ? (($this->premiumLeg() + $this->OTPremiumLeg()) * ($this->leg % 8)) : 0);

//        $leg1 = ($this->premiumLeg() > 0) ? ($this->hourlyRate() + $this->premiumLeg()) * ($this->leg - ($this->leg % 8)) : 0;
//        $leg2 = ($this->leg % 8 > 0) ? ($this->hourlyRate() + $this->premiumLeg() + $this->OTPremiumLeg()) * ($this->leg % 8) : 0;
//        $legFinal = $leg1 + $leg2;
//
//        return $legFinal;



        // New Computation 03/28/2018
//        return ($this->leg - ($this->leg % 8)) + ($this->legrd - ($this->legrd % 8));
    }

    public function legrd()
    {
        return (($this->premiumLegrd() > 0) ? (($this->premiumLegrd()) * ($this->legrd - ($this->legrd % 8))) : 0) + (($this->legrd % 8) > 0 ? (($this->premiumLegrd()) * ($this->legrd % 8)) : 0);

//        $legrd1 = ($this->premiumLegrd() > 0) ? ($this->hourlyRate() + $this->premiumLegrd()) * ($this->legrd - ($this->legrd % 8)) : 0;
//        $legrd2 = ($this->legrd % 8 > 0) ? ($this->hourlyRate() + $this->premiumLegrd()) * ($this->legrd % 8) : 0;
//        $legrdFinal = $legrd1 + $legrd2;
//
//        return $legrdFinal;
    }

    public function premiumRd()
    {
        if($this->rd > 0) {
            return PremiumRates::where('tag', 'RD')->first()->premium * $this->hourlyRate();
        } else {
            return 0;
        }
    }

    public function premiumSpe()
    {
        if($this->spe > 0) {
            return PremiumRates::where('tag', 'SPE')->first()->premium * $this->hourlyRate();
        } else {
            return 0;
        }
    }

    public function premiumSperd()
    {
        if($this->sperd > 0) {
            return PremiumRates::where('tag', 'SPERD')->first()->premium * $this->hourlyRate();
        } else {
            return 0;
        }
    }

    public function premiumLeg()
    {
        if($this->leg > 0) {
            return PremiumRates::where('tag', 'LEG')->first()->premium * $this->hourlyRate();
        } else {
            return 0;
        }
    }

    public function premiumLegrd()
    {
        if($this->legrd > 0) {
            return PremiumRates::where('tag', 'LEGRD')->first()->premium * $this->hourlyRate();
        } else {
            return 0;
        }
    }

    public function OTReg()
    {
        if($this->regot > 0) {
            return (PremiumRates::where('tag', 'OT')->first()->ot * $this->hourlyRate());
        } else {
            return 0;
        }
    }

    public function OTPremiumSpe()
    {
        if(($this->spe % 8) > 0) {
            return PremiumRates::where('tag', 'RD')->first()->ot * ($this->hourlyRate() + $this->premiumSpe());
        } else {
            return 0;
        }
    }

    public function OTPremiumSperd()
    {
        if(($this->sperd % 8) > 0) {
            return PremiumRates::where('tag', 'RD')->first()->ot * ($this->hourlyRate() + $this->premiumSperd());
        } else {
            return 0;
        }
    }

    public function OTPremiumLeg()
    {
        if(($this->leg % 8) > 0) {
            return PremiumRates::where('tag', 'RD')->first()->ot * ($this->hourlyRate() + $this->premiumLeg());
        } else {
            return 0;
        }
    }

    public function OTPremiumLegrd()
    {
        if(($this->leg % 8) > 0) {
            return PremiumRates::where('tag', 'RD')->first()->ot * ($this->hourlyRate() + $this->premiumLegrd());
        } else {
            return 0;
        }
    }

    public function basicPay()
    {
        return $this->isMonthlyRate == 'Y' ? $this->monthlyRate : $this->rg() * $this->hourlyRate();
    }

    public function HDPay()
    {
        return $this->spe() + $this->sperd() + $this->leg() + $this->legrd();
//        $total = 0;
//        if($this->premiumSpe() > 0) {
//            $total += $this->premiumSpe() * $this->spe();
//        }
//
//        if($this->premiumSperd() > 0) {
//            $total += $this->premiumSperd() * $this->spe();
//        }
//
//        if($this->premiumLeg() > 0) {
//            $total += $this->premiumLeg() * $this->spe();
//        }
//
//        if($this->premiumLegrd() > 0) {
//            $total += $this->premiumLegrd() * $this->spe();
//        }

//        return $total;
    }

    public function OTPay()
    {
//        IF(REG OT HRS>0,((HOURLY RATE+OT REG RATE)*REG OT HRS),0)
//        dd("({$this->hourlyRate()} + {$this->OTReg()}) * {$this->regot}");
        return ($this->regot > 0) ? ($this->OTReg()) * $this->regot : 0;
//        $total = 0;
//
//        if($this->rd > 0) {
//            $total += ($this->hourlyRate() + $this->premiumRd()) * $this->rd;
//        }
//
//        if($this->regot > 0) {
//            $total += ($this->hourlyRate() + $this->OTReg()) * $this->regot;
//        }
//
//        if(($this->spe % 8) > 0) {
//            $total += ($this->hourlyRate() + $this->premiumSpe() + $this->OTPremiumSpe()) * ($this->spe % 8);
//        }
//
//        if(($this->sperd % 8) > 0) {
//            $total += ($this->hourlyRate() + $this->premiumSperd() + $this->OTPremiumSperd()) * ($this->sperd % 8);
//        }
//
//        if(($this->leg % 8) > 0) {
//            $total += ($this->hourlyRate() + $this->premiumLeg() + $this->OTPremiumLeg()) * ($this->leg % 8);
//        }
//
//        if(($this->legrd % 8) > 0) {
//            $total += ($this->hourlyRate() + $this->premiumLegrd() + $this->OTPremiumLegrd()) * ($this->legrd % 8);
//        }


//        return $total;
    }

    public function NDPay()
    {
        $total = 0;

//        return (($this->hourlyRate() > 0 ? (($this->hourlyRate() * .1) * $this->nd) : 0) > 0 ?
//        ($this->premiumRd() > 0 ? (($this->hourlyRate() + $this->premiumRd()) * 0.1 * $this->nd) : 0) +
//        ($this->premiumSpe() > 0 ? (($this->hourlyRate() + $this->premiumSpe()) * 0.1 * $this->nd) : 0) +
//        ($this->premiumSperd() > 0 ? (($this->hourlyRate() + $this->premiumSperd()) * 0.1 * $this->nd) : 0) +
//        ($this->premiumLeg() > 0 ? (($this->hourlyRate() + $this->premiumLeg()) * 0.1 * $this->nd) : 0) +
//        ($this->premiumLegrd() > 0 ? (($this->hourlyRate() + $this->premiumLegrd()) * 0.1 * $this->nd) : 0) : (($this->hourlyRate() * .1) * $this->nd));

        $total = 0;

        if($this->hourlyRate() > 0) {
            if($this->premiumRd() > 0 || $this->premiumSpe() > 0 || $this->premiumSperd() > 0 || $this->premiumLeg() > 0 || $this->premiumLegrd() > 0) {
                $total += ($this->premiumRd() > 0 ? (($this->hourlyRate() + $this->premiumRd()) * 0.1 * $this->nd) : 0);
                $total += ($this->premiumSpe() > 0 ? (($this->hourlyRate() + $this->premiumSpe()) * 0.1 * $this->nd) : 0);
                $total += ($this->premiumSperd() > 0 ? (($this->hourlyRate() + $this->premiumSperd()) * 0.1 * $this->nd) : 0);
                $total += ($this->premiumLeg() > 0 ? (($this->hourlyRate() + $this->premiumLeg()) * 0.1 * $this->nd) : 0);
                $total += ($this->premiumLegrd() > 0 ? (($this->hourlyRate() + $this->premiumLegrd()) * 0.1 * $this->nd) : 0);
            } else {
                $total = ($this->hourlyRate() * .1) * $this->nd;
            }
        }


//        IF(IF(HOURLY RATE>0,(HOURLY RATE*0.1*ND HRS))>0,
//IF(PREM RD RATE>0,((HOURLY RATE+PREM RD RATE)*0.1*ND HRS),0)+
//IF(PREM SPE RATE>0,((HOURLY RATE+PREM SPE RATE)*0.1*ND HRS),0)+
//IF(PREM SPERD RATE>0,((HOURLY RATE+REM SPERD RATE)*0.1*ND HRS),0)+
//IF(PREM LEG RATE>0,((HOURLY RATE+PREM LEG RATE)*0.1*ND HRS),0)+
//IF(PREM LEGRD RATE>0,((HOURLY RATE+PREM LEGRD RATE)*0.1*ND HRS)),
//(HOURLY RATE*0.1*ND HRS))

//        $arg1 = 0;
//        if($this->hourlyRate() > 0) {
//            $total = ($this->hourlyRate() * .1) * $this->nd;
//        }

//        if($arg1 > 0) {
//            if($this->premiumRd() > 0) {
//                $total += (($this->hourlyRate() + $this->premiumRd()) * .1) * $this->nd;
//            }
//
//            if($this->premiumSpe() > 0) {
//                $total += (($this->hourlyRate() + $this->premiumSpe()) * .1) * $this->nd;
//            }
//
//            if($this->premiumSperd() > 0) {
//                $total += (($this->hourlyRate() + $this->premiumSperd()) * .1) * $this->nd;
//            }
//
//            if($this->premiumLeg() > 0) {
//                $total += (($this->hourlyRate() + $this->premiumLeg()) * .1) * $this->nd;
//            }
//
//            if($this->premiumLegrd() > 0) {
//                $total += (($this->hourlyRate() + $this->premiumLegrd()) * .1) * $this->nd;
//            }
//        } else {
//            $total += ($this->hourlyRate() * .1) * $this->nd;
//        }

        return $total;
    }

    public function vl()
    {
        return $this->vl * $this->hourlyRate();
    }

    public function sl()
    {
        return $this->sl * $this->hourlyRate();
    }

    public function el()
    {
        return $this->el * $this->hourlyRate();
    }

    public function spl()
    {
        return $this->spl * $this->hourlyRate();
    }

    public function ml()
    {
        return $this->ml * $this->hourlyRate();
    }

    public function pl()
    {
        return $this->pl * $this->hourlyRate();
    }

    public function pto()
    {
        return $this->pto * $this->hourlyRate();
    }

    public function bl()
    {
        return $this->bl * $this->hourlyRate();
    }

    public function cl()
    {
        return $this->cl * $this->hourlyRate();
    }

    public function netPay()
    {
        return $this->basicPay() + $this->HDPay() + $this->OTPay() + $this->NDPay();
    }
}
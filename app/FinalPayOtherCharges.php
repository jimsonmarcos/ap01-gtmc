<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinalPayOtherCharges extends Model
{
    protected $table = "final_pay_other_charges";
    public $timestamps = false;

    protected $guarded = [];
}

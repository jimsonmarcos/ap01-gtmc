<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profiles extends Model
{
    protected $table = "profiles";
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'position_id',
        'cost_center_id',
        'civil_status_id',
        'educational_attainment_id',
        'id_number',
        'first_name',
        'middle_name',
        'last_name',
        'photo_path',
        'mothers_maiden_name',
        'address',
        'mobile1',
        'mobile2',
        'email',
        'birthday',
        'join_date',
        'gender',
        'icoe_contact_name',
        'icoe_contact_address',
        'icoe_contact_mobile',
        'employees_address',
        'membership_type',
        'membership_category',
        'membership_status',
        'coop_share',
        'is_orientation',
        'status'
    ];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function EmploymentDetails()
    {
        return $this->belongsTo('App\EmploymentDetails', 'user_id', 'user_id');
    }

    public function Compensation()
    {
        return $this->belongsTo('App\Compensations', 'user_id', 'user_id');
    }

    public function Beneficiaries()
    {
        return $this->hasMany('App\Beneficiaries', 'user_id', 'user_id');
    }

    public function CivilStatus()
    {
        return $this->belongsTo('App\CivilStatus');
    }

    public function EducationalAttainment()
    {
        return $this->belongsTo('App\EducationalAttainments');
    }

    public function Position()
    {
        return $this->belongsTo('App\Positions');
    }

    public function CostCenter()
    {
        return $this->belongsTo('App\CostCenters');
    }

    public function name()
    {
        $name = trim($this->first_name);
        $name = strlen(strlen($this->middle_name)) > 0 ? $name. " " .substr(trim($this->middle_name), 0, 1)."." : $name;
        $name = strlen(strlen($this->last_name)) > 0 ? $name. " " .trim($this->last_name) : $name;
        return $name;
    }

    public function name_complete()
    {
        $name = trim($this->first_name);
        $name = strlen(strlen($this->middle_name)) > 0 ? $name. " " .$this->middle_name : $name;
        $name = strlen(strlen($this->last_name)) > 0 ? $name. " " .trim($this->last_name) : $name;
        return $name;
    }

    public function namelfm()
    {
        $name = trim($this->first_name);
        $name = !empty($this->middle_name) && strlen(trim($this->middle_name)) > 0 ? $name. " " .substr(trim($this->middle_name), 0, 1)."." : $name;
        $name = !empty($this->last_name) && strlen(trim($this->last_name)) > 0 ? $name. " " .trim($this->last_name) : $name;

        $name = trim($this->last_name);
        $name = !empty($this->first_name) && strlen(trim($this->first_name)) > 0 ? $name. ", " .trim($this->first_name) : $name;
        $name = !empty($this->middle_name) && strlen(trim($this->middle_name)) > 0 ? $name. " " .trim($this->middle_name) : $name;
        return $name;
    }

    public function namefl()
    {
        $name = trim($this->first_name);
        $name = !empty($this->last_name) && strlen(trim($this->last_name)) > 0 ? $name. " " .trim($this->last_name) : $name;

        return $name;
    }


}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncomeDeductionItems extends Model
{
    protected $table = "income_deduction_items";
//    public $timestamps = false;

    protected $guarded = [];

}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashReceipts extends Model
{
    protected $table = "cash_receipts";

    protected $guarded = [];

    public function User() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function Items() {
        return $this->hasMany('App\CashReceiptItems', 'cash_receipt_id', 'id');
    }
}

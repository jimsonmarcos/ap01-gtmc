<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SSSContributions extends Model
{
    protected $table = "sss_contributions";
    public $timestamps = false;

    protected $fillable = [
        'compensation_range_start',
        'compensation_range_end',
        'monthly_salary_credit',
        'er',
        'ee',
        'ec',
        'total'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departments extends Model
{
    protected $table = "departments";
    public $timestamps = false;

    protected $fillable = [
        'province',
        'area',
        'mall',
        'outlet',
        'department'
    ];
}

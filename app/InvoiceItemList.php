<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItemList extends Model
{
    protected $table = "invoice_item_list";
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'payroll_group_id',
        'payroll_cycle_id',
        'business_tax_type_id',
        'total_head_count',
        'total_hours',
        'billing_amount',
        'item_name',
        'description'
    ];

    public function Company()
    {
        return $this->belongsTo('App\Companies', 'company_id');
    }

    public function PayrollCycle()
    {
        return $this->belongsTo('App\PayrollCycles', 'payroll_cycle_id');
    }
}

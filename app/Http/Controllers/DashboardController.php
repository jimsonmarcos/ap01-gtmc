<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    public function dashboard()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Dashboard";
        return view('admin.dashboard', $data);
    }
}

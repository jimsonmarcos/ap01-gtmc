<?php

namespace App\Http\Controllers;

use App\Companies;
use App\Loans;
use App\OtherPayments;
use App\Profiles;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DatatablesController extends Controller
{
    public function users(Request $request, $status = 'Active')
    {
        $columns = array(
            0 =>'last_name',
            1 =>'id_number',
            2=> 'membership_category',
            3=> 'principal',
            4=> 'gender',
        );

        $status = $status == 'Active' ? 'Active' : 'Deactivated';

        $users = Profiles::select(DB::raw('profiles.id, profiles.user_id, profiles.first_name, profiles.middle_name, profiles.last_name, CONCAT(profiles.first_name, " ", profiles.middle_name, " ", profiles.last_name) as name, profiles.id_number, membership_category, gender, companies.company_code as principal'))
            ->join('employment_details', 'profiles.user_id', '=', 'employment_details.user_id', 'INNER')
            ->join('companies', 'employment_details.principal_id', '=', 'companies.id', 'LEFT');

        if($status == 'Active') {
            $users->where('profiles.status', $status);
        } else {
            $users->whereIn('profiles.status', ['Deactivated', 'Inactive']);
        }

        $totalData = $users->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $users = $users->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            if($status == 'Active') {
                $users->where('profiles.status', $status);
            } else {
                $users->whereIn('profiles.status', ['Deactivated', 'Inactive']);
            }

            $users =  $users->whereRaw( "(id_number LIKE '%{$search}%' OR CONCAT(first_name, ' ', last_name) LIKE '%{$search}%' OR companies.company_code LIKE '%{$search}%')")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->select(DB::raw('profiles.id, profiles.user_id, profiles.first_name, profiles.middle_name, profiles.last_name, CONCAT(profiles.first_name, " ", profiles.middle_name, " ", profiles.last_name) as name, profiles.id_number, membership_category, gender, companies.company_code as principal'))
                ->join('employment_details as ed', 'profiles.user_id', '=', 'ed.user_id', 'INNER')
                ->join('companies as c', 'ed.principal_id', '=', 'c.id', 'LEFT')
                ->get();

            if($status == 'Active') {
                $totalFiltered = Profiles::where('profiles.status', $status);
            } else {
                $totalFiltered = Profiles::whereIn('profiles.status', ['Deactivated', 'Inactive']);
            }

            $totalFiltered = $totalFiltered->whereIn('membership_category', array('Employee', 'Member'))
                ->whereRaw( "(id_number LIKE '%{$search}%' OR CONCAT(first_name, ' ', last_name) LIKE '%{$search}%' OR companies.company_code LIKE '%{$search}%')")
                ->join('employment_details', 'profiles.user_id', '=', 'employment_details.user_id', 'INNER')
                ->join('companies', 'employment_details.principal_id', '=', 'companies.id', 'LEFT')
                ->count();
        }

        $data = array();
        if(!empty($users))
        {
            foreach ($users as $user)
            {
                $view = url('admin/user/'. $user->user_id);
//                $edit =  route('posts.edit',$post->id);

                $nestedData['name'] = trim("{$user->last_name}, {$user->first_name} {$user->middle_name}");
                $nestedData['id_number'] = $user->id_number;
                $nestedData['membership_category'] = $user->membership_category;
                $nestedData['principal'] = !empty($user->EmploymentDetails->Principal->company_code) ? $user->EmploymentDetails->Principal->company_code : 'N/A';
                $nestedData['gender'] = $user->gender;

                $nestedData['action'] = "<a href='{$view}' class='btn btn-sm btn-primary'><i class='fa fa-user'></i> VIEW</a>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function clients(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 =>'company_code',
            2 =>'company_name',
            3=> 'client_id',
            4=> 'status'

        );

//        3=> 'total_billing_amount',
//            4=> 'pending_billing_amount'

        $totalData = Companies::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $clients = Companies::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $clients =  Companies::where('company_code','LIKE',"%{$search}%")
                ->orWhere('company_name', 'LIKE',"%{$search}%")
                ->orWhere('client_id', 'LIKE',"%{$search}%")
                ->orWhere('status', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Companies::where('company_code','LIKE',"%{$search}%")
                ->orWhere('company_name', 'LIKE',"%{$search}%")
                ->orWhere('client_id', 'LIKE',"%{$search}%")
                ->orWhere('status', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($clients))
        {
            foreach ($clients as $client)
            {
                $view = url('admin/client/'. $client->id);
//                $edit =  route('posts.edit',$post->id);

                $nestedData['company_code'] = $client->company_code;
                $nestedData['company_name'] = $client->company_name;
                $nestedData['client_id'] = $client->client_id;
                $nestedData['status'] = $client->status;
//                $nestedData['total_billing_amount'] = 'N/A';
//                $nestedData['pending_billing_amount'] = 'N/A';

                $nestedData['action'] = "<a href='{$view}' class='btn btn-sm btn-primary'>VIEW</a>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function admin_users(Request $request)
    {
        $columns = array(
            0 =>'name',
            1 =>'username',
            2=> 'email',
            3=> 'company_code',
            4=> 'role',
        );

        $users = User::whereRaw(DB::raw("`users`.`role` IN ('Admin', 'AdminPayroll', 'AdminCoop', 'AdminHR', 'AdminAcctg')"))
            ->select(DB::raw('users.id, users.name, users.admin_username, users.email, companies.company_code, users.role'))
            ->join('employment_details', 'users.id', '=', 'employment_details.user_id', 'LEFT')
            ->join('companies', 'employment_details.principal_id', '=', 'companies.id', 'LEFT');
        $totalData = $users->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $users = $users->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $users =  User::whereRaw(DB::raw("users.role IN ('Admin', 'AdminPayroll', 'AdminCoop', 'AdminHR', 'AdminAcctg')"))
                ->select(DB::raw('users.id, users.name, users.admin_username, users.email, companies.company_code, users.role'))
                ->whereRaw(DB::raw("(admin_username LIKE '%{$search}%' OR name LIKE '%{$search}%' OR  email LIKE '%{$search}%' OR  companies.company_code LIKE '%{$search}%' OR  users.role LIKE '%{$search}%')"))
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->join('employment_details', 'users.id', '=', 'employment_details.user_id', 'LEFT')
                ->join('companies', 'employment_details.principal_id', '=', 'companies.id', 'LEFT')
                ->get();

            $totalFiltered = User::whereRaw(DB::raw("users.role IN ('Admin', 'AdminPayroll', 'AdminCoop', 'AdminHR', 'AdminAcctg')"))
                ->whereRaw(DB::raw("(admin_username LIKE '%{$search}%' OR name LIKE '%{$search}%' OR  email LIKE '%{$search}%' OR  companies.company_code LIKE '%{$search}%' OR  users.role LIKE '%{$search}%')"))
                ->join('employment_details', 'users.id', '=', 'employment_details.user_id', 'LEFT')
                ->join('companies', 'employment_details.principal_id', '=', 'companies.id', 'LEFT')
                ->count();
        }

        $data = array();
        if(!empty($users))
        {
            foreach ($users as $user)
            {
                $view = url('admin/manage-admin/user/'. $user->id);
                $password_reset = url('admin/super/admin/password_reset');
//                $edit =  route('posts.edit',$post->id);

                $nestedData['name'] = $user->name;
                $nestedData['username'] = $user->admin_username;
                $nestedData['email'] = $user->email;
                $nestedData['role'] = $user->role;
                $nestedData['company_code'] = !empty($user->company_code) ? $user->company_code : 'N/A';

                $nestedData['action'] = "<a href='{$view}' class='btn btn-sm btn-primary'>VIEW</a>  <a href='$password_reset' class='btn btn-sm btn-danger'>RESET</a>";

                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function admin_clients(Request $request)
    {
        $columns = array(
            0 =>'name',
            1 =>'username',
            2=> 'email',
            3=> 'company_code',
            4=> 'role',
        );

        $users = User::whereRaw(DB::raw("`users`.`role` IN ('ClientAdmin', 'ClientUser')"))
            ->select(DB::raw('users.id, users.name, users.username, users.email, companies.company_code, users.role'))
//            ->join('clients', 'users.id', '=', 'clients.user_id', 'LEFT')
            ->join('companies', 'users.company_id', '=', 'companies.id', 'LEFT');
        $totalData = $users->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $users = $users->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $users =  User::whereRaw(DB::raw("`users`.`role` IN ('ClientAdmin', 'ClientUser')"))
                ->select(DB::raw('users.id, users.name, users.username, users.email, companies.company_code, users.role'))
//                ->join('clients', 'users.id', '=', 'clients.user_id', 'LEFT')
                ->join('companies', 'users.company_id', '=', 'companies.id', 'LEFT')
                ->whereRaw(DB::raw("(username LIKE '%{$search}%' OR name LIKE '%{$search}%' OR  email LIKE '%{$search}%' OR  companies.company_code LIKE '%{$search}%' OR  users.role LIKE '%{$search}%')"))
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = User::whereRaw(DB::raw("`users`.`role` IN ('ClientAdmin', 'ClientUser')"))
                ->join('clients', 'users.id', '=', 'clients.user_id', 'LEFT')
                ->join('companies', 'clients.company_id', '=', 'companies.id', 'LEFT')
                ->whereRaw(DB::raw("(username LIKE '%{$search}%' OR name LIKE '%{$search}%' OR  email LIKE '%{$search}%' OR  companies.company_code LIKE '%{$search}%' OR  users.role LIKE '%{$search}%')"))
                ->count();
        }

        $data = array();
        if(!empty($users))
        {
            foreach ($users as $user)
            {
                $view = url('admin/manage-admin/client/'. $user->id);
                $password_reset = url('admin/super/admin/password_reset');
//                $edit =  route('posts.edit',$post->id);

                $nestedData['name'] = $user->name;
                $nestedData['username'] = $user->username;
                $nestedData['email'] = $user->email;
                $nestedData['role'] = $user->role == 'ClientAdmin' ? 'Client Admin' : 'Client User';
                $nestedData['company_code'] = !empty($user->company_code) ? $user->company_code : 'N/A';
//                print_r($user->Company);
//                $nestedData['action'] = "<a href='{$view}' class='btn btn-sm btn-primary'>VIEW</a>  <a href='$password_reset' class='btn btn-sm btn-danger'>RESET</a>";;

                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function loans(Request $request, $status)
    {
        $columns = array(
            0 =>'cla_number',
            1 =>'id_number',
            2=> 'name',
            3=> 'cost_center',
            4=> 'loan_type',
            6=> 'total_overdue_amount',
            7=> 'outstanding_balance',
            8=> 'granted_loan_amount',
            9=> 'status'
        );

        $loans = Loans::select(DB::raw('loans.id, loans.loan_type_id, loans.loan_type, loans.is_voucher, loans.cla_number, profiles.id_number, users.name, profiles.first_name, profiles.middle_name, profiles.last_name, cost_centers.cost_center, loan_types.type, loans.granted_loan_amount, loans.status, loans.releasing_status, companies.company_name, COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.total_overdue_amount, 0) as total_overdue_amount, COALESCE(loan_payment_schedules.total_overdue_amount, 0) - COALESCE(loan_payments.total_amount_paid, 0) as outstanding_balance'));
        if($status == 'Request') {
//            $loans = $loans->where('loans.releasing_status', 'Unreleased')->where('loans.status', '!=', 'Cancelled');
        } else {
            if(in_array($status, ['Pending', 'Cancelled'])) {
                $loans = $loans->where('loans.status', $status);
            } else {
                if($status == 'Approved') {
                    $loans = $loans->where('loans.status', 'Approved');
                } else {
                    $loans = $loans->where('loans.status', 'Approved')->where('loans.releasing_status', 'Released');
                }

                if($status == 'Ongoing') {
                    $loans = $loans->whereRaw("(total_overdue_amount - COALESCE(total_amount_paid, 0) > 0) AND COALESCE(total_amount_paid, 0) > 0");
                } elseif ($status == 'Completed') {
                    $loans = $loans->whereRaw("((total_overdue_amount - COALESCE(total_amount_paid, 0)) <= 0)");
                }
            }
        }


        $loans = $loans->join('users', 'users.id', '=', 'loans.user_id', 'LEFT')
            ->join('profiles', 'profiles.user_id', '=', 'users.id', 'LEFT')
            ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
            ->join('employment_details', 'employment_details.user_id', '=', 'profiles.user_id', 'LEFT')
            ->join('companies', 'companies.id', '=', 'employment_details.principal_id', 'LEFT')
            ->join('loan_types', 'loan_types.id', '=', 'loans.loan_type_id', 'LEFT')
            ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')
            ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(amortization) + SUM(lapsed_interest) as total_overdue_amount FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payment_schedules.loan_id', '=', 'loans.id')
            ->groupBy('loans.id');


        $totalFiltered = $totalData = $loans->get()->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))) {
            $loans = $loans->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $loans = Loans::select(DB::raw('loans.id, loans.loan_type_id, loans.loan_type, loans.is_voucher, loans.cla_number, profiles.id_number, users.name, profiles.first_name, profiles.middle_name, profiles.last_name, cost_centers.cost_center, loan_types.type, loans.granted_loan_amount, loans.status, loans.releasing_status, companies.company_name, COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.total_overdue_amount, 0) as total_overdue_amount, COALESCE(loan_payment_schedules.total_overdue_amount, 0) - COALESCE(loan_payments.total_amount_paid, 0) as outstanding_balance'));
            if($status == 'Request') {
//                $loans = $loans->where('loans.releasing_status', 'Unreleased')->where('loans.status', '!=', 'Cancelled');
            } else {
                if(in_array($status, ['Pending', 'Cancelled'])) {
                    $loans = $loans->where('loans.status', $status);
                } else {
                    if($status == 'Approved') {
                        $loans = $loans->where('loans.status', 'Approved');
                    } else {
                        $loans = $loans->where('loans.status', 'Approved')->where('loans.releasing_status', 'Released');
                    }

                    if($status == 'Ongoing') {
                        $loans = $loans->whereRaw("((granted_loan_amount - COALESCE(total_amount_paid, 0)) < granted_loan_amount)");
                    } elseif ($status == 'Completed') {
                        $loans = $loans->whereRaw("((granted_loan_amount - COALESCE(total_amount_paid, 0)) <= 0)");
                    }
                }
            }

            $loans = $loans->join('users', 'users.id', '=', 'loans.user_id', 'LEFT')
                ->join('profiles', 'profiles.user_id', '=', 'users.id', 'LEFT')
                ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
                ->join('employment_details', 'employment_details.user_id', '=', 'profiles.user_id', 'LEFT')
                ->join('companies', 'companies.id', '=', 'employment_details.principal_id', 'LEFT')
                ->join('loan_types', 'loan_types.id', '=', 'loans.loan_type_id', 'LEFT')
                ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')
                ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(amortization) + SUM(lapsed_interest) as total_overdue_amount FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payments.loan_id', '=', 'loans.id')
                ->whereRaw("((company_name LIKE '%{$search}%') OR (loan_types.type LIKE '%{$search}%') OR (loans.status LIKE '%{$search}%') OR (cost_center LIKE '%{$search}%') OR (name LIKE '%{$search}%') OR (id_number LIKE '%{$search}%') OR (loans.cla_number LIKE '%{$search}%'))")
                ->groupBy('loans.id');

//                ->orWhereColumn(['cla_number', 'id_number', 'name', 'cost_center', 'status', 'company_name', 'loans.status', 'loan_types.type'], 'LIKE', "%{$search}%");


            $totalFiltered = $loans->get()->count();

            $loans = $loans->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }

        $data = array();
        if(!empty($loans))
        {
            foreach ($loans as $loan)
            {
//                $view = url('admin/manage-admin/client/'. $user->id);
//                $edit =  route('posts.edit',$post->id);

                $action = '';
                if($loan->status == 'Pending') {
                    $action = "<a href='/admin/transactions/loans/update/". $loan->id ."' class='btn btn-sm btn-primary push-5' style='display: block;'>UPDATE</a>".$action;
                } elseif($loan->status == 'Approved' && $loan->releasing_status == 'Unreleased') {
                    $action = "<a href='/admin/transactions/loans/release/". $loan->id ."' class='btn btn-sm btn-primary' style='display: block;'>RELEASE</a>";
                } elseif($loan->status == 'Approved' && $loan->releasing_status == 'Released') {
                    $action = "<a href='/admin/transactions/loans/". $loan->id ."' class='btn btn-sm btn-success' style='display: block;'>VIEW</a>";
                }

                $statusArr = [
                    'Approved' => 'success',
                    'Pending' => 'warning',
                    'Cancelled' => 'danger'
                ];

                $status = $loan->status;
                if($loan->releasing_status == 'Released') {
                    if(($loan->granted_loan_amount - $loan->total_amount_paid) <= 0) {
                        $status = 'Completed';
                    } elseif(($loan->granted_loan_amount - $loan->total_amount_paid) < $loan->granted_loan_amount) {
                        $status = 'Ongoing';
                    }
                }


                $status = "<span class='label label-".$statusArr[$loan->status]."'>{$status}</span>";

//                print_r($loan);
                if($loan->status == 'Approved') {
                    $nestedData['cla_number'] = "<a href='/admin/transactions/loans/". $loan->id ."'>$loan->cla_number</a>";
                } else {
                    $nestedData['cla_number'] = $loan->cla_number;
                }


                $nestedData['id_number'] = $loan->id_number;
                $nestedData['name'] = trim("{$loan->last_name}, {$loan->first_name} {$loan->middle_name}");
                $nestedData['cost_center'] = $loan->cost_center;
                if($loan->is_voucher == 'N') {
                    $nestedData['loan_type'] = $loan->loanType->type;
                } else {
                    $nestedData['loan_type'] = $loan->loan_type;
                }


                $nestedData['total_overdue_amount'] = $loan->total_overdue_amount;
                $nestedData['outstanding_balance'] = $loan->outstanding_balance;
                $nestedData['granted_loan_amount'] = $loan->granted_loan_amount;
                $nestedData['status'] = $status;
                $nestedData['principal'] = $loan->principal;

                $nestedData['action'] = $action;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function unpaid_loans(Request $request)
    {
        $columns = array(
            0 =>'cla_number',
            1 =>'id_number',
            2=> 'name',
            3=> 'cost_center',
            4=> 'loan_type',
            5=> 'granted_loan_amount',
            6=> 'status',
            8=> 'principal',
        );

        $loans = Loans::select(DB::raw('loans.id, loans.loan_type_id, loans.loan_type, loans.is_voucher, loans.cla_number, profiles.id_number, users.name, cost_centers.cost_center, loan_types.type, loans.granted_loan_amount, loans.status, loans.releasing_status, companies.company_name, COALESCE(((loans.granted_loan_amount - loans.beginning_balance) - COALESCE(payments.amount_paid, 0)), 0) as outstanding_amount, loans.amortization, loans.date_request, loans.effective_date, loans.months_to_pay'));

        $loans = $loans->join('users', 'users.id', '=', 'loans.user_id', 'LEFT')
            ->join('profiles', 'profiles.user_id', '=', 'users.id', 'LEFT')
            ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
            ->join('employment_details', 'employment_details.user_id', '=', 'profiles.user_id', 'LEFT')
            ->join('companies', 'companies.id', '=', 'employment_details.principal_id', 'LEFT')
            ->join('loan_types', 'loan_types.id', '=', 'loans.loan_type_id', 'LEFT')
            ->leftJoin(DB::raw('(SELECT loan_id, COALESCE(SUM(total_amount_paid), 0) as amount_paid FROM loan_payments GROUP BY loan_id) payments'), function($join)
            {
                $join->on('loans.id', '=', 'payments.loan_id');
            });

        $loans->where('releasing_status', 'Released');
        $loans->whereRaw(DB::raw('COALESCE(((loans.granted_loan_amount - loans.beginning_balance) - COALESCE(payments.amount_paid, 0)), 0) > 0'));

        $totalData = $loans->get()->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))) {
            $loans = $loans->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $loans = Loans::select(DB::raw('loans.id, loans.loan_type_id, loans.loan_type, loans.is_voucher, loans.cla_number, profiles.id_number, users.name, cost_centers.cost_center, loan_types.type, loans.granted_loan_amount, loans.status, loans.releasing_status, companies.company_name, ((loans.granted_loan_amount - loans.beginning_balance) - payments.amount_paid) as outstanding_amount, loans.amortization, loans.date_request, loans.effective_date, loans.months_to_pay'));
//            if($status != 'All') {
//                $loans = $loans->where('loans.status', $status);
//            }

            $loans = $loans->join('users', 'users.id', '=', 'loans.user_id', 'LEFT')
                ->join('profiles', 'profiles.user_id', '=', 'users.id', 'LEFT')
                ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
                ->join('employment_details', 'employment_details.user_id', '=', 'profiles.user_id', 'LEFT')
                ->join('companies', 'companies.id', '=', 'employment_details.principal_id', 'LEFT')
                ->join('loan_types', 'loan_types.id', '=', 'loans.loan_type_id', 'LEFT')
                ->leftJoin(DB::raw('(SELECT loan_id, SUM(total_amount_paid) as amount_paid FROM loan_payments GROUP BY loan_id) payments'), function($join)
                {
                    $join->on('loans.id', '=', 'payments.loan_id');
                })
                ->orWhere("company_name", 'LIKE', "%{$search}%")
                ->orWhere("cla_number", 'LIKE', "%{$search}%")
                ->orWhere("id_number", 'LIKE', "%{$search}%")
                ->orWhere("name", 'LIKE', "%{$search}%")
                ->orWhere("cost_center", 'LIKE', "%{$search}%")
                ->orWhere("loans.status", 'LIKE', "%{$search}%")
                ->orWhere("loans.status", 'LIKE', "%{$search}%")
                ->orWhere("loan_types.type", 'LIKE', "%{$search}%");

            $loans->where('releasing_status', 'Released');
            $loans->whereRaw(DB::raw('COALESCE(((loans.granted_loan_amount - loans.beginning_balance) - COALESCE(payments.amount_paid, 0)), 0) > 0'));
//                ->orWhereColumn(['cla_number', 'id_number', 'name', 'cost_center', 'status', 'company_name', 'loans.status', 'loan_types.type'], 'LIKE', "%{$search}%");


            $totalFiltered = $loans->count();

            $loans = $loans->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }

        $data = array();
        if(!empty($loans))
        {
            foreach ($loans as $loan)
            {
//                $view = url('admin/manage-admin/client/'. $user->id);
//                $edit =  route('posts.edit',$post->id);

                $action = '';
//                if($loan->status == 'Pending') {
//                    $action = "<a href='/admin/transactions/loans/update/". $loan->id ."' class='btn btn-sm btn-primary push-5' style='display: block;'>UPDATE</a>".$action;
//                } elseif($loan->status == 'Approved' && $loan->releasing_status == 'Unreleased') {
//                    $action = "<a href='/admin/transactions/loans/release/". $loan->id ."' class='btn btn-sm btn-primary' style='display: block;'>RELEASE</a>";
//                } elseif($loan->status == 'Approved' && $loan->releasing_status == 'Released') {
//                }

                $action = "<a href='/admin/transactions/loans/". $loan->id ."' class='btn btn-sm btn-success' style='display: block;'>VIEW</a> <a href='/admin/transactions/loans/". $loan->id ."/payments/add' class='btn btn-sm btn-info'>Payment</a>";
                $statusArr = [
                    'Approved' => 'success',
                    'Pending' => 'warning',
                    'Rejected' => 'danger'
                ];

                $status = "<span class='label label-".$statusArr[$loan->status]."'>{$loan->status}</span>";

//                print_r($loan);
                $nestedData['cla_number'] = $loan->cla_number;
                $nestedData['id_number'] = $loan->id_number;
                $nestedData['name'] = $loan->name;
                $nestedData['cost_center'] = $loan->cost_center;
                if($loan->is_voucher == 'N') {
                    $nestedData['loan_type'] = $loan->loanType->type;
                } else {
                    $nestedData['loan_type'] = $loan->loan_type;
                }


                $nestedData['granted_loan_amount'] = number_format($loan->granted_loan_amount, 2);
                $nestedData['outstanding_amount'] = number_format($loan->outstanding_amount, 2);
                $nestedData['amortization'] = number_format($loan->amortization, 2);
                if($loan->is_voucher == 'N') {
                    $startDate = date('m/d/Y', strtotime($loan->date_request));
                } else {
                    $startDate = date('m/d/Y', strtotime($loan->effective_date));
                }

                $endDate  = Carbon::createFromFormat('m/d/Y', $startDate )->addMonths($loan->months_to_pay)->format('Y-m-d');

                $nestedData['due_date'] = "{$startDate} - {$endDate}";
                $nestedData['status'] = $status;
                $nestedData['principal'] = $loan->principal;

                $nestedData['action'] = $action;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function payments_coop_shares(Request $request, $type)
    {
        $columns = array(
            0 =>'ar_number',
            1 =>'id_number',
            2=> 'name',
            3=> 'payment_method',
            4=> 'pay_period',
            5=> 'total_amount',
            6=> 'status'
        );

        $otherPayments = OtherPayments::select(DB::raw('p.id_number, u.name, p.first_name, p.middle_name, p.last_name, other_payments.id, other_payments.ar_number, other_payments.transaction_type, other_payments.pay_period, other_payments.payment_method, other_payments.total_amount_paid as total_amount'));

        $otherPayments = $otherPayments->join('users as u', 'u.id', '=', 'other_payments.user_id', 'LEFT')
            ->join('profiles as p', 'p.user_id', '=', 'u.id', 'LEFT');

        if($type == 'coop-share') {
            $transactionType = 'COOP SHARE';
        } elseif($type == 'notarial-fee') {
            $transactionType = 'NF';
        } else {
            $transactionType = 'MF';
        }


        $otherPayments->where('transaction_type', $transactionType);

        $totalData = $otherPayments->get()->count('other_payments.*');

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))) {
            $otherPayments = $otherPayments->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $otherPayments = OtherPayments::select(DB::raw('p.id_number, u.name, p.first_name, p.middle_name, p.last_name, other_payments.id, other_payments.ar_number, other_payments.transaction_type, other_payments.pay_period, other_payments.payment_method, other_payments.total_amount_paid as total_amount'));

//            $otherPayments = $otherPayments->join('users as u', 'u.id', '=', 'other_payments.user_id', 'LEFT')
//                ->join('profiles as p', 'p.user_id', '=', 'u.id', 'LEFT');

            if($type == 'coop-share') {
                $transactionType = 'COOP SHARE';
            } elseif($type == 'notarial-fee') {
                $transactionType = 'NF';
            } else {
                $transactionType = 'MF';
            }

            $otherPayments->where('transaction_type', $transactionType);


            $otherPayments = $otherPayments->join('users as u', 'u.id', '=', 'other_payments.user_id', 'LEFT')
                ->join('profiles as p', 'p.user_id', '=', 'u.id', 'LEFT')
                ->whereRaw("(ar_number LIKE '%{$search}%' OR p.id_number = '{$search}' OR other_payments.pay_period = '{$search}' OR p.id_number LIKE '%{$search}%')");
//                ->orWhere("ar_number", 'LIKE', "%{$search}%")
//                ->orWhere("p.id_number", 'LIKE', "%{$search}%")
//                ->orWhere("u.name", 'LIKE', "%{$search}%")
//                ->orWhere("other_payments.pay_period", 'LIKE', "%{$search}%")
//                ->orWhere("other_payments.payment_method", 'LIKE', "%{$search}%");

//            $otherPayments->where('transaction_type', 'COOP SHARE');

            $totalFiltered = $otherPayments->count();

            $otherPayments = $otherPayments->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

//            dd($otherPayments);
        }

        $data = array();
        if(!empty($otherPayments))
        {
            foreach ($otherPayments as $otherPayment)
            {
                $action = '';

                $action = "<a href='/admin/transactions/payments/{$type}/{$otherPayment->id}' class='btn btn-sm btn-success' style='display: block;'>VIEW</a>";

//                print_r($loan);
                $nestedData['ar_number'] = $otherPayment->ar_number;
                $nestedData['id_number'] = $otherPayment->id_number;
//                $nestedData['name'] = $otherPayment->name;
                $nestedData['name'] = trim("{$otherPayment->last_name}, {$otherPayment->first_name} {$otherPayment->middle_name}");
                $nestedData['payment_method'] = $otherPayment->payment_method;
                $nestedData['pay_period'] = $otherPayment->pay_period;


                $nestedData['total_amount'] = number_format($otherPayment->total_amount, 2);


                $nestedData['action'] = $action;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function user_loans(Request $request, $user_id)
    {
        $columns = array(
            0 =>'cla_number',
            1 =>'id_number',
            2=> 'name',
            3=> 'cost_center',
            4=> 'loan_type',
            6=> 'total_overdue_amount',
            7=> 'outstanding_balance',
            8=> 'granted_loan_amount'
        );

        $status = 'Approved';

        $loans = Loans::select(DB::raw('loans.id, loans.loan_type_id, loans.loan_type, loans.is_voucher, loans.cla_number, profiles.id_number, users.name, profiles.first_name, profiles.middle_name, profiles.last_name, cost_centers.cost_center, loan_types.type, loans.granted_loan_amount, loans.status, loans.releasing_status, companies.company_name, COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.total_overdue_amount, 0) as total_overdue_amount, COALESCE(loan_payment_schedules.total_overdue_amount, 0) - COALESCE(loan_payments.total_amount_paid, 0) as outstanding_balance'));
        $loans->where('loans.user_id', $user_id);
        if($status == 'Request') {
//            $loans = $loans->where('loans.releasing_status', 'Unreleased')->where('loans.status', '!=', 'Cancelled');
        } else {
            if(in_array($status, ['Pending', 'Cancelled'])) {
                $loans = $loans->where('loans.status', $status);
            } else {
                if($status == 'Approved') {
                    $loans = $loans->where('loans.status', 'Approved');
                } else {
                    $loans = $loans->where('loans.status', 'Approved')->where('loans.releasing_status', 'Released');
                }

                if($status == 'Ongoing') {
                    $loans = $loans->whereRaw("(total_overdue_amount - COALESCE(total_amount_paid, 0) > 0) AND COALESCE(total_amount_paid, 0) > 0");
                } elseif ($status == 'Completed') {
                    $loans = $loans->whereRaw("((total_overdue_amount - COALESCE(total_amount_paid, 0)) <= 0)");
                }
            }
        }


        $loans = $loans->join('users', 'users.id', '=', 'loans.user_id', 'LEFT')
            ->join('profiles', 'profiles.user_id', '=', 'users.id', 'LEFT')
            ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
            ->join('employment_details', 'employment_details.user_id', '=', 'profiles.user_id', 'LEFT')
            ->join('companies', 'companies.id', '=', 'employment_details.principal_id', 'LEFT')
            ->join('loan_types', 'loan_types.id', '=', 'loans.loan_type_id', 'LEFT')
            ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')
            ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(amortization) + SUM(lapsed_interest) as total_overdue_amount FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payment_schedules.loan_id', '=', 'loans.id')
            ->groupBy('loans.id');


        $totalFiltered = $totalData = $loans->get()->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))) {
            $loans = $loans->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $loans = Loans::select(DB::raw('loans.id, loans.loan_type_id, loans.loan_type, loans.is_voucher, loans.cla_number, profiles.id_number, users.name, profiles.first_name, profiles.middle_name, profiles.last_name, cost_centers.cost_center, loan_types.type, loans.granted_loan_amount, loans.status, loans.releasing_status, companies.company_name, COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.total_overdue_amount, 0) as total_overdue_amount, COALESCE(loan_payment_schedules.total_overdue_amount, 0) - COALESCE(loan_payments.total_amount_paid, 0) as outstanding_balance'));
            if($status == 'Request') {
//                $loans = $loans->where('loans.releasing_status', 'Unreleased')->where('loans.status', '!=', 'Cancelled');
            } else {
                if(in_array($status, ['Pending', 'Cancelled'])) {
                    $loans = $loans->where('loans.status', $status);
                } else {
                    if($status == 'Approved') {
                        $loans = $loans->where('loans.status', 'Approved');
                    } else {
                        $loans = $loans->where('loans.status', 'Approved')->where('loans.releasing_status', 'Released');
                    }

                    if($status == 'Ongoing') {
                        $loans = $loans->whereRaw("((granted_loan_amount - COALESCE(total_amount_paid, 0)) < granted_loan_amount)");
                    } elseif ($status == 'Completed') {
                        $loans = $loans->whereRaw("((granted_loan_amount - COALESCE(total_amount_paid, 0)) <= 0)");
                    }
                }
            }

            $loans = $loans->join('users', 'users.id', '=', 'loans.user_id', 'LEFT')
                ->join('profiles', 'profiles.user_id', '=', 'users.id', 'LEFT')
                ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
                ->join('employment_details', 'employment_details.user_id', '=', 'profiles.user_id', 'LEFT')
                ->join('companies', 'companies.id', '=', 'employment_details.principal_id', 'LEFT')
                ->join('loan_types', 'loan_types.id', '=', 'loans.loan_type_id', 'LEFT')
                ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')
                ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(amortization) + SUM(lapsed_interest) as total_overdue_amount FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payments.loan_id', '=', 'loans.id')
                ->whereRaw("((company_name LIKE '%{$search}%') OR (loan_types.type LIKE '%{$search}%') OR (loans.status LIKE '%{$search}%') OR (cost_center LIKE '%{$search}%') OR (name LIKE '%{$search}%') OR (id_number LIKE '%{$search}%') OR (loans.cla_number LIKE '%{$search}%'))")
                ->groupBy('loans.id');

//                ->orWhereColumn(['cla_number', 'id_number', 'name', 'cost_center', 'status', 'company_name', 'loans.status', 'loan_types.type'], 'LIKE', "%{$search}%");


            $totalFiltered = $loans->get()->count();

            $loans = $loans->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }

        $data = array();
        if(!empty($loans))
        {
            foreach ($loans as $loan)
            {
//                $view = url('admin/manage-admin/client/'. $user->id);
//                $edit =  route('posts.edit',$post->id);

                $action = '';
                if($loan->status == 'Pending') {
                    $action = "<a href='/admin/transactions/loans/update/". $loan->id ."' class='btn btn-sm btn-primary push-5' style='display: block;'>UPDATE</a>".$action;
                } elseif($loan->status == 'Approved' && $loan->releasing_status == 'Unreleased') {
                    $action = "<a href='/admin/transactions/loans/release/". $loan->id ."' class='btn btn-sm btn-primary' style='display: block;'>RELEASE</a>";
                } elseif($loan->status == 'Approved' && $loan->releasing_status == 'Released') {
                    $action = "<a href='/admin/transactions/loans/". $loan->id ."' class='btn btn-sm btn-success' style='display: block;'>VIEW</a>";
                }

                $statusArr = [
                    'Approved' => 'success',
                    'Pending' => 'warning',
                    'Cancelled' => 'danger'
                ];

                $status = $loan->status;
                if($loan->releasing_status == 'Released') {
                    if(($loan->total_overdue_amount - $loan->total_amount_paid) <= 0) {
                        $status = 'Completed';
                    } elseif(($loan->granted_loan_amount - $loan->total_amount_paid) < $loan->granted_loan_amount) {
                        $status = 'Ongoing';
                    }
                }


                $status = "<span class='label label-".$statusArr[$loan->status]."'>{$status}</span>";

//                print_r($loan);
                if($loan->status == 'Approved') {
                    $nestedData['cla_number'] = "<a href='/admin/transactions/loans/". $loan->id ."'>$loan->cla_number</a>";
                } else {
                    $nestedData['cla_number'] = $loan->cla_number;
                }


                $nestedData['id_number'] = $loan->id_number;
                $nestedData['name'] = trim("{$loan->last_name}, {$loan->first_name} {$loan->middle_name}");
                $nestedData['cost_center'] = $loan->cost_center;
                if($loan->is_voucher == 'N') {
                    $nestedData['loan_type'] = $loan->loanType->type;
                } else {
                    $nestedData['loan_type'] = $loan->loan_type;
                }


                $nestedData['total_overdue_amount'] = $loan->total_overdue_amount;
                $nestedData['outstanding_balance'] = $loan->outstanding_balance;
                $nestedData['granted_loan_amount'] = $loan->granted_loan_amount;
                $nestedData['status'] = $status;
                $nestedData['principal'] = $loan->principal;

//                $nestedData['action'] = $action;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
}

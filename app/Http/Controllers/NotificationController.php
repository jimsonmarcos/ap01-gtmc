<?php

namespace App\Http\Controllers;

use App\LoanPaymentSchedules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{

    public function loan_lapses()
    {
        $data['paymentSchedules'] = $paymentSchedules = LoanPaymentSchedules::selectRaw("
                                l.*,
                                CONCAT(p.last_name,', ',p.first_name) as name,
                                COUNT(l.id) as lapse_count,
                                SUM(loan_payment_schedules.amortization) + SUM(loan_payment_schedules.lapsed_interest) as total_amount_due
                            ")
                            ->leftJoin('loans as l', 'l.id', 'loan_payment_schedules.loan_id')
                            ->leftJoin('profiles as p', 'p.user_id', 'l.user_id')
                            ->leftJoin('loan_payments as lp', function($join) {
                                $join->on('lp.loan_id', '=', 'loan_payment_schedules.loan_id')
                                    ->where('lp.pay_period', '=', 'loan_payment_schedules.pay_period');
                            })
                            ->where('loan_payment_schedules.due_date', '<=', date('Y-m-d'))
                            ->where('p.last_name', '!=', '')
                            ->where('p.status', '=', 'Active')
                            ->havingRaw("(SUM(COALESCE(lp.total_amount_paid, 0)) = 0 AND COUNT(l.id) > 1)")
                            ->groupBy('l.id')
                            ->get();

//        dd($paymentSchedules->first());

        $data['pageHeader'] = "Lapsed Loans";

        return view('admin.transactions.lapsed_loans', $data);
    }

    public function coop_loan_lapses()
    {
        $data['paymentSchedules'] = $paymentSchedules = DB::select(DB::raw("
                    SELECT
                        p.user_id as id,
                        pc.period as payroll_period,
                        p.first_name,
                        p.last_name,
                        p.id_number,
                        p.coop_share
                    FROM payroll_cycles pc
                    INNER JOIN compensations c
                        ON c.payroll_group_id = pc.payroll_group_id
                    INNER JOIN profiles p
                        ON p.user_id = c.user_id
                    LEFT JOIN other_payments op
                        ON op.transaction_type = 'COOP SHARE' AND op.pay_period = pc.period
                    WHERE op.id IS NULL AND p.`status` = 'Active'
                "));

//        dd($paymentSchedules->first());

        $data['pageHeader'] = "Lapsed Loans";

        return view('admin.transactions.lapsed_coop_shares', $data);
    }
}

<?php

namespace App\Http\Controllers\Admin\Accounting;

use App\AuditLog;
use App\CashFlows;
use App\CheckVoucherItems;
use App\CheckVouchers;
use App\Signatories;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CheckVoucherController extends Controller
{
    //
    public function index()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";


        $data['items'] = CashFlows::where('cv_number', '!=', '')->orderByDesc('entry_date')->get();

        return view('admin.accounting.check_voucher', $data);
    }

    public function view_check_voucher(Request $request, $id)
    {
        $data['checkVoucher'] = CheckVouchers::find($id);
        $data['checkVoucherItems'] = CheckVoucherItems::where('check_voucher_id', $id)->get();

        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";
        return view('admin.accounting.view_check_voucher', $data);
    }

    public function update_check_voucher(Request $request, $id)
    {
        $data['checkVoucher'] = CashFlows::find($id);
        $data['signatories'] = User::whereIn('id', Signatories::pluck('user_id'))->orderBy('name')->get();

        $data['me'] = User::find(Auth::user()->id);

        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";
        return view('admin.accounting.update_check_voucher', $data);
    }

    public function update_check_voucher_post(Request $request, $id)
    {
        $data = $request->except('_token');

        $checkVoucher = CashFlows::find($id);
        $checkVoucher->prepared_by = Auth::user()->id;
        $checkVoucher->approved_by = $request->approved_by;
        $checkVoucher->save();

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Accounting',
            'action' => 'Edit',
            'description' => "Updated Check Voucher: {$checkVoucher->cv_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Check Voucher successfully updated!");
        return redirect(url("admin/accounting/check_voucher/edit/{$id}"));
    }

    public function print(Request $request, $id)
    {
        $data['checkVoucher'] = CashFlows::find($id);

        if(empty($data['checkVoucher'])) {
            $request->session()->flash('popError', "Check Voucher does not exists");
            return redirect()->back();
        }

        return view('print.check_voucher', $data);
    }
}

<?php

namespace App\Http\Controllers\Admin\Accounting;

use App\AuditLog;
use App\ChartOfAccounts;
use App\JournalVoucherItems;
use App\JournalVouchers;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class JournalVoucherController extends Controller
{
    //
    public function index()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";

        $data['journalVouchers'] = JournalVouchers::orderByDesc('created_at')->get();

        return view('admin.accounting.journal_voucher', $data);
    }

    public function create_journal_voucher(Request $request)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";

        $data['accountTypes'] = $accountTypes = ChartOfAccounts::orderBy('account_type')->groupBy('account_type')->with('AccountTitles')->get();
        $data['me'] = User::find(Auth::user()->id);

        return view('admin.accounting.journal_voucher_create', $data);
    }

    public function create(Request $request)
    {
//        dd($request->all());


        if(!empty($request->item)) {
            $journalVoucherItems = $request->item;
        }

        $data = $request->except(['_token', 'item']);

        $data['date']  = Carbon::createFromFormat('m/d/Y', $data['date'] )->format('Y-m-d');

        $data['user_id'] = Auth::user()->id;

        $data['jv_number'] = "JV#". $data['jv_number'];

        $journalVoucher = JournalVouchers::create($data);

        if(!empty($journalVoucherItems)) {
            foreach($journalVoucherItems as $journalVoucherItem) {
                $journalVoucherItem['journal_voucher_id'] = $journalVoucher->id;
                $journalVoucherItem['debit'] = str_replace(',', '', !empty($journalVoucherItem['debit']) ? $journalVoucherItem['debit'] : 0);
                $journalVoucherItem['credit'] = str_replace(',', '', !empty($journalVoucherItem['credit']) ? $journalVoucherItem['credit'] : 0);

                JournalVoucherItems::create($journalVoucherItem);
            }
        }

//        dd($data);

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Accounting',
            'action' => 'Create',
            'description' => "Created Journal Voucher: {$journalVoucher->jv_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Journal Voucher successfully created");
        return redirect(url('/admin/accounting/journal_voucher'));
    }

    public function edit_journal_voucher(Request $request, $id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";

        $data['accountTypes'] = $accountTypes = ChartOfAccounts::orderBy('account_type')->groupBy('account_type')->with('AccountTitles')->get();
        $data['me'] = User::find(Auth::user()->id);

        $data['journalVoucher'] = JournalVouchers::find($id);

        return view('admin.accounting.journal_voucher_edit', $data);
    }

    public function update(Request $request, $id)
    {
//        dd($request->all());

        $journalVoucher = JournalVouchers::find($id);

        if(!empty($request->new_item)) {
            $journalVoucherItems = $request->new_item;
        }

        if(!empty($request->item)) {
            $existingJournalVoucherItems = $request->item;
        }

        $data = $request->except(['_token', 'item', 'new_item']);

        $data['date']  = Carbon::createFromFormat('m/d/Y', $data['date'] )->format('Y-m-d');


        JournalVouchers::where('id', $id)->update($data);

        if(!empty($journalVoucherItems)) {
            foreach($journalVoucherItems as $journalVoucherItem) {
                $journalVoucherItem['journal_voucher_id'] = $journalVoucher->id;
                $journalVoucherItem['debit'] = str_replace(',', '', !empty($journalVoucherItem['debit']) ? $journalVoucherItem['debit'] : 0);
                $journalVoucherItem['credit'] = str_replace(',', '', !empty($journalVoucherItem['credit']) ? $journalVoucherItem['credit'] : 0);

                JournalVoucherItems::create($journalVoucherItem);
            }
        }

        if(!empty($existingJournalVoucherItems)) {
            foreach($existingJournalVoucherItems as $id => $journalVoucherItem) {
                $journalVoucherItem['debit'] = str_replace(',', '', !empty($journalVoucherItem['debit']) ? $journalVoucherItem['debit'] : 0);
                $journalVoucherItem['credit'] = str_replace(',', '', !empty($journalVoucherItem['credit']) ? $journalVoucherItem['credit'] : 0);

                JournalVoucherItems::where('id', $id)->update($journalVoucherItem);
            }
        }

//        dd($data);

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Accounting',
            'action' => 'Edit',
            'description' => "Updated Journal Voucher: {$journalVoucher->jv_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Journal Voucher successfully updated");
        return redirect(url("/admin/accounting/journal_voucher/edit/{$journalVoucher->id}"));
    }

    public function print(Request $request, $id)
    {
        $data['journalVoucher'] = JournalVouchers::find($id);

        if(empty($data['journalVoucher'])) {
            $request->session()->flash('popError', "Journal Voucher does not exists");
            return redirect()->back();
        }

        return view('print.journal_voucher', $data);
    }
}

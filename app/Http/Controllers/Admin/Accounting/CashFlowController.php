<?php

namespace App\Http\Controllers\Admin\Accounting;

use App\AuditLog;
use App\CashFlowEntries;
use App\CashFlowItemList;
use App\CashFlowParticulars;
use App\CashFlows;
use App\CashReceipts;
use App\ChartOfAccounts;
use App\CheckRequestItems;
use App\CheckRequests;
use App\CheckVouchers;
use App\FinalPays;
use App\Loans;
use App\Profiles;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CashFlowController extends Controller
{
    //
    public function index()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";

        $data['cashFlows'] = CashFlows::orderByDesc('created_at')->get();

        return view('admin.accounting.cash_flow', $data);
    }

    public function create_get(Request $request)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";

        $data['accountTypes'] = $accountTypes = ChartOfAccounts::orderBy('account_type')->groupBy('account_type')->with('AccountTitles')->get();
        $data['me'] = User::find(Auth::user()->id);

        $data['cv_number_bdo'] = 'CV-101';
        $data['cv_number_ubp'] = 'CV-10001';

        if(CashFlows::where(['bank' => 'BDO', 'mode' => 'Check'])->count() > 0) {
            $cf = CashFlows::where(['bank' => 'BDO', 'mode' => 'Check'])->orderByDesc('cv_number')->first();
            if(!empty($cf->cv_number)) {
                if(substr_count($cf->cv_number, '-') == 0) {
                    $data['cv_number_bdo'] = $cf->cv_number + 1;
//                    $data['cv_number_bdo'] = 'CV-'. $data['cv_number_bdo'];
                } else {
                    $pieces = explode('-', $cf->cv_number);
                    $data['cv_number_bdo'] = $pieces[1] + 1;
//                    $data['cv_number_bdo'] = 'CV-'. $data['cv_number_bdo'];
                }
            }
        }

        if(CashFlows::where(['bank' => 'UBP', 'mode' => 'Check'])->count() > 0) {
            $cf = CashFlows::where(['bank' => 'UBP', 'mode' => 'Check'])->orderByDesc('cv_number')->first();
            if(!empty($cf->cv_number)) {
                if(substr_count($cf->cv_number, '-') == 0) {
                    $data['cv_number_ubp'] = $cf->cv_number + 1;
//                    $data['cv_number_ubp'] = 'CV-'. $data['cv_number_bdo'];
                } else {
                    $pieces = explode('-', $cf->cv_number);
                    $data['cv_number_ubp'] = $pieces[1] + 1;
//                    $data['cv_number_ubp'] = 'CV-'. $data['cv_number_bdo'];
                }
            }
        }

        if(!empty($request->source)) {
            if($request->source == 'check_request') {
                $data['checkRequest'] = $checkRequest = CheckRequests::find($request->id);
                $data['source'] = 'Check Request';
                $data['source_id'] = $request->id;
                $data['mode'] = 'Check';
                $data['entry_date'] = date('m/d/Y');
                $data['check_date'] = date('m/d/Y');
                $data['reference_number'] = $checkRequest->Items->first()->reference_number;
                $data['payee_type'] = $checkRequest->payee_type;
                $data['payee_name'] = $checkRequest->payee_name;
                $data['particulars'] = $checkRequest->Items->first()->cash_flow_item ." ". $checkRequest->payee_name ." ". $checkRequest->Items->first()->description;

                return view('admin.accounting.cash_flow_create_crf', $data);
            } elseif($request->source == 'cash_receipt') {
                $data['cashReceipt'] = $cashReceipt = CashReceipts::find($request->id);
                $data['source'] = 'Cash Receipt';
                $data['source_id'] = $request->id;
                $data['mode'] = $cashReceipt->mode;
                $data['entry_date'] = date('m/d/Y');
                $data['check_date'] = date('m/d/Y', strtotime($cashReceipt->check_date));
                $data['check_number'] = $cashReceipt->check_number;
                $data['reference_number'] = $cashReceipt->reference_number;
                $data['payee_type'] = $cashReceipt->payor_type;
                $data['payee_name'] = $cashReceipt->payor_name;
                $data['particulars'] = $cashReceipt->particulars;
                $data['bank'] = $cashReceipt->bank;

                return view('admin.accounting.cash_flow_create_cr', $data);
            }
        }



//        $data['cv_number']

        return view('admin.accounting.cash_flow_create', $data);
    }

    public function create_post(Request $request)
    {
        $data = $request->except(['_token', 'item', 'entry', 'id']);
        $data['user_id'] = Auth::user()->id;
        if($request->source == 'check_request') {
            $data['source'] = 'Check Request';
            $data['source_id'] = $request->id;
        }

        if($request->source == 'cash_receipt') {
            $data['source'] = 'Cash Receipt';
            $data['source_id'] = $request->id;
        }

//        dd($request->all());

        if(isset($data['check_date'])) {
            $data['check_date']  = Carbon::createFromFormat('m/d/Y', $data['check_date'] )->format('Y-m-d');
        }

        if(!empty($request->item)) {
            $items = $request->item;
        }

        if(!empty($request->item)) {
            $entries = $request->entry;
        }



        if(CashFlows::count() == 0) {
            $data['cf_number'] = 'CF-1001';
        } else {
            $latestInvoice = CashFlows::orderByDesc('cf_number')->first()->cf_number;
            $pieces = explode('-', $latestInvoice);
            $invoiceNumber = $pieces[1] + 1;
//            dd($invoiceNumber);
            $data['cf_number'] = "CF-{$invoiceNumber}";
        }

        $data['entry_date']  = Carbon::createFromFormat('m/d/Y', $data['entry_date'] )->format('Y-m-d');
//        dd($items);

        if(!empty($data['cv_number'])) {
            $data['cv_number'] = "CV-{$data['cv_number']}";
        }

        $cashFlow = CashFlows::create($data);


        if(!empty($items)) {
            foreach($items as $item)
            {
                $item['cash_flow_id'] = $cashFlow->id;

                $item['amount'] = str_replace(',', '', !empty($item['amount']) ? $item['amount'] : 0);

                CashFlowItemList::create($item);


                if($item['item'] == 'LOAN') {
                    $loan = Loans::where('cla_number', $cashFlow->reference_number)->first();
                    $loan->cv_number = $cashFlow->cv_number;
                    $loan->cv_date = $cashFlow->entry_date;
                    $loan->check_number = $cashFlow->check_number;
                    $loan->save();
                }

                if($item['item'] == 'FINAL PAY') {
                    $profile = Profiles::where('id_number', $cashFlow->reference_number)->first();
                    FinalPays::where('user_id', $profile->user_id)->update(['status' => 'Approved']);
                }
            }
        }

        if(!empty($entries)) {
            foreach($entries as $entry)
            {
                $entry['cash_flow_id'] = $cashFlow->id;

                $entry['debit'] = str_replace(',', '', !empty($entry['debit']) ? $entry['debit'] : 0);
                $entry['credit'] = str_replace(',', '', !empty($entry['credit']) ? $entry['credit'] : 0);


                CashFlowEntries::create($entry);
            }
        }

        $cashFlow->total_amount = $cashFlow->Items->sum('amount');
        $cashFlow->save();

        if(!empty($cashFlow->source)) {
            if($cashFlow->source == 'Check Request') {
                $checkRequest = CheckRequests::find($cashFlow->source_id);
                if($cashFlow->status == 'Cancelled') {
                    $checkRequest->status = 'Cancelled';
                } else {
                    $checkRequest->status = 'Approved';
                }
                $checkRequest->save();
            }
        }


        if(!empty($loan)) {
            $loan->check_number = $cashFlow->check_number;
        }


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Accounting',
            'action' => 'Create',
            'description' => "Cash flow {$cashFlow->cf_number} created",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Cash Flow successfully created!");
        return redirect('/admin/accounting/cash_flow');
    }

    public function cash_flow_update(Request $request, $id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";

        $data['accountTypes'] = $accountTypes = ChartOfAccounts::orderBy('account_type')->groupBy('account_type')->with('AccountTitles')->get();
        $data['me'] = User::find(Auth::user()->id);

        $data['cashFlow'] = $cashFlow = CashFlows::find($id);

        if(empty($data['cashFlow'])) {
            $request->session()->flash('popError', "Cash Flow does not exists!");
            return redirect('/admin/accounting/cash_flow');
        }

        $data['cv_number_bdo'] = '101';
        $data['cv_number_ubp'] = '10001';

        if(CashFlows::where(['bank' => 'BDO', 'mode' => 'Check'])->count() > 0) {
            $cf = CashFlows::where(['bank' => 'BDO', 'mode' => 'Check'])->orderByDesc('cv_number')->first();

            if(!empty($cf->cv_number)) {
                if(substr_count($cf->cv_number, '-') == 0) {
                    $data['cv_number_bdo'] = $cf->cv_number + 1;
//                    $data['cv_number_bdo'] = 'CV-'. $data['cv_number_bdo'];
                } else {
                    $pieces = explode('-', $cf->cv_number);
                    $data['cv_number_bdo'] = $pieces[1] + 1;
//                    $data['cv_number_bdo'] = 'CV-'. $data['cv_number_bdo'];
                }
            }
        }

        if(CashFlows::where(['bank' => 'UBP', 'mode' => 'Check'])->count() > 0) {
            $cf = CashFlows::where(['bank' => 'UBP', 'mode' => 'Check'])->orderByDesc('cv_number')->first();
            if(!empty($cf->cv_number)) {
                if(substr_count($cf->cv_number, '-') == 0) {
                    $data['cv_number_ubp'] = $cf->cv_number + 1;
//                    $data['cv_number_ubp'] = 'CV-'. $data['cv_number_bdo'];
                } else {
                    $pieces = explode('-', $cf->cv_number);
                    $data['cv_number_ubp'] = $pieces[1] + 1;
//                    $data['cv_number_ubp'] = 'CV-'. $data['cv_number_bdo'];
                }
            }
        }

        return view('admin.accounting.cash_flow_edit', $data);
    }

    public function cash_flow_update_post(Request $request, $id)
    {
        $data = $request->except(['_token', 'item', 'entry', 'id']);

        if(isset($data['check_date'])) {
            $data['check_date']  = Carbon::createFromFormat('m/d/Y', $data['check_date'] )->format('Y-m-d');
        }

        $data['entry_date']  = Carbon::createFromFormat('m/d/Y', $data['entry_date'] )->format('Y-m-d');

        if(!empty($data['cv_number'])) {
            if(substr_count($data['cv_number'], 'CV-') == 0) {
                $data['cv_number'] = "CV-{$data['cv_number']}";
            }
        }

        CashFlows::where('id', $id)->update($data);

        $cashFlow = CashFlows::find($id);


        if(!empty($request->item)) {
            $items = $request->item;
        }

        if(!empty($request->item)) {
            $entries = $request->entry;
        }

        if(!empty($items)) {
            CashFlowItemList::where('cash_flow_id', $id)->delete();
            foreach($items as $item)
            {
                $item['cash_flow_id'] = $cashFlow->id;

                $item['amount'] = str_replace(',', '', !empty($item['amount']) ? $item['amount'] : 0);

                CashFlowItemList::create($item);
            }
        }


        if(!empty($entries)) {
            CashFlowEntries::where('cash_flow_id', $id)->delete();
            foreach($entries as $entry)
            {
                $entry['cash_flow_id'] = $cashFlow->id;

                $entry['debit'] = str_replace(',', '', !empty($entry['debit']) ? $entry['debit'] : 0);
                $entry['credit'] = str_replace(',', '', !empty($entry['credit']) ? $entry['credit'] : 0);

                CashFlowEntries::create($entry);
            }
        }

        $cashFlow->total_amount = $cashFlow->Items->sum('amount');
        $cashFlow->save();


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Accounting',
            'action' => 'Edit',
            'description' => "Cash flow {$cashFlow->cf_number} updated",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);


        $request->session()->flash('popSuccess', "Cash Flow updated!");
        return redirect()->back();
    }

    public function cash_flow_update_request(Request $request, $checkRequestId)
    {
        $data['checkRequest'] = CheckRequests::find($checkRequestId);
        if(!CheckRequests::where('id', $checkRequestId)->exists()) {
            $request->session()->flash('popError', "Check Request does not exists");
            return redirect()->back();
        }

        if($data['checkRequest']->status != 'Approved') {
            $request->session()->flash('popError', "Check Request status must be Approved to continue");
            return redirect()->back();
        }


//        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";
        return view('admin.accounting.cash_flow_update_request', $data);
    }

    public function cash_flow_update_request_post(Request $request, $checkRequestId)
    {
        $data = $request->except('_token');
        $data['check_request_id'] = $checkRequestId;
        $data['check_date']  = Carbon::createFromFormat('m/d/Y', $data['check_date'] )->format('Y-m-d');
        $data['entry_date']  = Carbon::createFromFormat('m/d/Y', $data['entry_date'] )->format('Y-m-d');
        $data['user_id'] = Auth::user()->id;

        if(CashFlows::count() == 0) {
            $data['series'] = 1001;
            $data['crf_number'] = "CF-{$data['series']}";
        } else {
            $data['series'] = CashFlows::select('series')->get()->max('series') + 1;
            $data['crf_number'] = "CF-{$data['series']}";
        }

        $cvNumber = $data['check_number'] - 1;
        $checkVoucherData = [
            'series' => $cvNumber,
            'user_id' => Auth::user()->id,
            'cv_number' => "CV-{$cvNumber}",
            'payee_name' => $data['payee_name'],
            'payee_address' => $data['payee_address'],
            'check_date' => $data['check_date'],
            'check_number' => $data['check_number'],
            'particulars' => $data['particulars'],
            'amount' => CheckRequestItems::where('check_request_id', $checkRequestId)->sum('amount')
        ];

        $checkVoucher = CheckVouchers::create($checkVoucherData);

        $data['check_voucher_id'] = $checkVoucher->id;

        if(!empty($data['cv_number'])) {
            if(substr_count($data['cv_number'], 'CV-') == 0) {
                $data['cv_number'] = "CV-{$data['cv_number']}";
            }
        }

        $cashFlow = CashFlows::create($data);


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Accounting',
            'action' => 'Create',
            'description' => "Cash flow {$cashFlow->cf_number} created",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Cash Flow successfully created!");
        return redirect('/admin/accounting/check_request');
    }
}

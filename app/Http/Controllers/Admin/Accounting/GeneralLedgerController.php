<?php

namespace App\Http\Controllers\Admin\Accounting;

use App\CashFlows;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class GeneralLedgerController extends Controller
{
    //
    public function index(Request $request)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";

//        dd($request->all());

        $data['date_from'] = $dateFrom = date('Y-01-01');
        if(!empty($request->date_from)) {
            $data['date_from'] = $dateFrom = Carbon::createFromFormat('m/d/Y', $request->date_from )->format('Y-m-d');
        }

        $data['date_to'] = $dateTo = date('Y-m-d');
        if(!empty($request->date_to)) {
            $data['date_to'] = $date_to = Carbon::createFromFormat('m/d/Y', $request->date_to )->format('Y-m-d');
        }

        if($data['date_from'] == $data['date_to']) {
            $where = "WHERE `date` = '{$dateFrom}'";
        } else {
            $where = "WHERE `date` >= '{$dateFrom}' AND `date` <= '{$dateTo}'";
        }

        $data['b'] = '';
        if(!empty($request->bank)) {
            $data['b'] = $request->bank;
            if(!empty($where)) {
                $where .= " AND bank = '{$request->bank}'";
            } else {
                $where = "WHERE `bank` = '{$request->bank}'";
            }
        }

//        $data['items'] = CashFlows::orderByDesc('entry_date')->get();
        $data['items'] = DB::select("SELECT * FROM (
                                    SELECT
                                        cf.entry_date as `date`,
                                        cf.bank,
                                        cfe.account_type,
                                        cfe.account_title,
                                        cfe.txn,
                                        cfe.debit,
                                        cfe.credit
                                    FROM cash_flow_entries cfe
                                    LEFT JOIN cash_flows cf
                                        ON cf.id = cfe.cash_flow_id
                                        
                                    UNION
                                    
                                    SELECT
                                        jv.`date` as `date`,
                                        jv.bank,
                                        jvi.account_type,
                                        jvi.account_title,
                                        jvi.txn,
                                        jvi.debit,
                                        jvi.credit
                                    FROM journal_voucher_items jvi
                                    LEFT JOIN journal_vouchers jv
                                        ON jv.id = jvi.journal_voucher_id
                                        
                                    ) t1
                                    {$where}
                                    ORDER BY 
                                        `date` DESC
                                    ");
//        dd($data['items']);


//        $query = DB::select(
//                "SELECT cf_number as transaction_id,
//                        entry_date as entry_date,
//	                    mode as mode_of_payment,
//	                    particulars as particulars,
//	                    payee_name as account_title,
//	                    'CR' as txn,
//	                    'CF' as code,
//	                    0 as dr_amount,
//	                    amount as cr_amount,
//	                    reference_number,
//	                    amount as amount
//                  FROM cash_flow
//
//                  UNION
//
//                  SELECT cv.cv_number as transaction_id,
//	                     check_date as entry_date,
//	                     'Cash' as mode_of_payment,
//	                      particulars as particulars,
//	                      payee_name as account_title,
//	                      'DR/CR' as txn,
//	                      'CV' as code,
//	                      COALESCE(SUM(cvid.debit), 0) as dr_amount,
//	                      COALESCE(SUM(cvic.credit), 0) as cr_amount, check_number as reference_number,
//	                      COALESCE(SUM(cvic.credit), 0) - COALESCE(SUM(cvid.debit), 0) as amount
//
//                  FROM check_vouchers cv
//                  LEFT JOIN check_voucher_items cvid ON cvid.check_voucher_id = cv.id AND cvid.txn = 'DR'
//                  LEFT JOIN check_voucher_items cvic ON cvid.check_voucher_id = cv.id AND cvid.txn = 'CR'
//
//                  GROUP BY cv.id"
//        );

//        $data['query'] = $query;
        return view('admin.accounting.general_ledger', $data);
    }

    public function update(Request $request, $transaction_id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";

        $query = DB::select(
            "SELECT * FROM (
                SELECT
	              cf_number as transaction_id,
	              entry_date as entry_date,
	              mode as mode_of_payment,
	              particulars as particulars,
	              payee_name as account_title,
	              'CR' as txn,
	              'CF' as code,
	              0 as dr_amount,
	              amount as cr_amount,
	              reference_number,
	              amount as amount
                FROM cash_flow

                UNION

                SELECT
	              cv.cv_number as transaction_id,
	              check_date as entry_date,
	              'Cash' as mode_of_payment,
	              particulars as particulars,
	              payee_name as account_title,
	              'DR/CR' as txn,
	              'CV' as code,
	              COALESCE(SUM(cvid.debit), 0) as dr_amount,
	              COALESCE(SUM(cvic.credit), 0) as cr_amount,
	              check_number as reference_number,
	              COALESCE(SUM(cvic.credit), 0) - COALESCE(SUM(cvid.debit), 0) as amount

                FROM check_vouchers cv
                LEFT JOIN check_voucher_items cvid ON cvid.check_voucher_id = cv.id AND cvid.txn = 'DR'
                LEFT JOIN check_voucher_items cvic ON cvid.check_voucher_id = cv.id AND cvid.txn = 'CR'
                
                GROUP BY cv.id ) as transaction Where transaction_id= '$transaction_id'
             "
        );

        $data['transaction'] = $query;

        // create new table for general ledger

        return view('admin.accounting.update_general_ledger', $data);
    }


    public function download(Request $request)
    {
        $data['date_from'] = $dateFrom = date('Y-01-01');
        if(!empty($request->date_from)) {
            $data['date_from'] = $dateFrom = Carbon::createFromFormat('m/d/Y', $request->date_from )->format('Y-m-d');
        }

        $data['date_to'] = $dateTo = date('Y-m-d');
        if(!empty($request->date_to)) {
            $data['date_to'] = $date_to = Carbon::createFromFormat('m/d/Y', $request->date_to )->format('Y-m-d');
        }

        if($data['date_from'] == $data['date_to']) {
            $where = "WHERE `date` = '{$dateFrom}'";
            $title = "General Ledger ({$dateFrom})";
        } else {
            $where = "WHERE `date` >= '{$dateFrom}' AND `date` <= '{$dateTo}'";
            $title = "General Ledger ({$dateFrom} - {$dateTo})";
        }

        $data['b'] = '';
        if(!empty($request->bank)) {
            $data['b'] = $request->bank;
            if(!empty($where)) {
                $where .= " AND bank = '{$request->bank}'";
            } else {
                $where = "WHERE `bank` = '{$request->bank}'";
            }
        }

//        $data['items'] = CashFlows::orderByDesc('entry_date')->get();
        $items = DB::select("SELECT * FROM (
                                    SELECT
                                        cf.entry_date as `date`,
                                        cf.bank,
                                        cfe.account_type,
                                        cfe.account_title,
                                        cfe.txn,
                                        cfe.debit,
                                        cfe.credit
                                    FROM cash_flow_entries cfe
                                    LEFT JOIN cash_flows cf
                                        ON cf.id = cfe.cash_flow_id
                                        
                                    UNION
                                    
                                    SELECT
                                        jv.`date` as `date`,
                                        jv.bank,
                                        jvi.account_type,
                                        jvi.account_title,
                                        jvi.txn,
                                        jvi.debit,
                                        jvi.credit
                                    FROM journal_voucher_items jvi
                                    LEFT JOIN journal_vouchers jv
                                        ON jv.id = jvi.journal_voucher_id
                                        
                                    ) t1
                                    {$where}
                                    ORDER BY 
                                        `date` DESC
                                    ");


        Excel::create($title, function ($excel) use ($items, $title) {


            // Set the title
            $excel->setTitle($title);

            $excel->sheet('General Ledger', function ($sheet) use ($items) {


                // Sheet manipulation
                $sheet->row(1, array(
                    'Date',
                    'Bank',
                    'Account Type',
                    'Account Title',
                    'TXN',
                    'Debit Amount',
                    'Credit Amount'
                ));



                $x = 2;
                foreach($items as $item) {


                    $sheet->row($x, array(
                        $item->date,
                        $item->bank,
                        $item->account_type,
                        $item->account_title,
                        $item->txn,
                        $item->debit,
                        $item->credit,
                    ));

                    $x++;
                }

                $sheet->setColumnFormat(array(
                    'F2:G'.$x => '#,##0.00'
                ));

            });
        })->download('xls');
    }
}

<?php

namespace App\Http\Controllers\Admin\Accounting;

use App\AuditLog;
use App\CashFlowItems;
use App\CheckRequestItems;
use App\CheckRequests;
use App\FinalPays;
use App\Loans;
use App\PayeeNames;
use App\Signatories;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CheckRequestController extends Controller
{
    //
    public function index(Request $request)
    {
        $status = $request->segment(5);

        if(!empty($status)) {
            $data['checkRequestStatus'] = $status;
            $data['checkRequests'] = CheckRequests::where('status', $status)->orderByDesc('created_at')->get();
        } else {
            $data['checkRequests'] = CheckRequests::orderByDesc('created_at')->get();
        }


        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";
        return view('admin.accounting.check_request', $data);
    }

    public function check_request_pending()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";
        return view('admin.accounting.check_request_pending', $data);
    }

    public function check_request_approved()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";
        return view('admin.accounting.check_request_approved', $data);
    }

    public function check_request_cancelled()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";
        return view('admin.accounting.check_request_cancelled', $data);
    }

    public function create_check_request(Request $request)
    {
        $data['payeeType'] = empty($request->get('payee_type')) ? 'Member' : $request->get('payee_type');
        $data['signatories'] = User::whereIn('id', Signatories::pluck('user_id'))->orderBy('name')->get();


        $data['me'] = User::find(Auth::user()->id);

        if(!empty($request->source)) {
            if($request->source == 'loan') {
                $loan = Loans::find($request->id);
                $data['source'] = 'Loan';
                $data['source_id'] = $loan->id;
                $data['payee_name'] = $loan->User->Profile->namefl();
                $data['payee_type'] = $loan->User->Profile->membership_category;
                $data['item'] = 'LOAN';
                $data['description'] = $loan->LoanType->type;
                $data['reference_number'] = $loan->cla_number;
                $data['amount'] = $loan->disbursed_amount;
            } elseif($request->source == 'final_pay') {
                $finalPay = FinalPays::find($request->id);
                $data['source'] = 'Final Pay';
                $data['source_id'] = $finalPay->id;
                $data['payee_name'] = $finalPay->User->Profile->namefl();
                $data['payee_type'] = $finalPay->User->Profile->membership_category;
                $data['item'] = 'FINAL PAY';
                $data['reference_number'] = $finalPay->User->Profile->id_number;
                $data['amount'] = $finalPay->net_payable;
            }
        }

        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";
        return view('admin.accounting.create_check_request', $data);
    }

    public function create_check_request_post(Request $request)
    {
        $data = $request->except('_token');

        $data['request_date']  = Carbon::createFromFormat('m/d/Y', $data['request_date'] )->format('Y-m-d');

        if(CheckRequests::count() == 0) {
            $data['crf_number'] = 'CRF-1001';
        } else {
            $latestInvoice = CheckRequests::orderByDesc('crf_number')->first()->crf_number;
            $pieces = explode('-', $latestInvoice);
            $invoiceNumber = $pieces[1] + 1;
//            dd($invoiceNumber);
            $data['crf_number'] = "CRF-{$invoiceNumber}";
        }

//        if(CheckRequests::count() == 0) {
//            $data['series'] = 1001;
//            $data['crf_number'] = "CRF-{$data['series']}";
//        } else {
//            $data['series'] = CheckRequests::select('series')->get()->max('series') + 1;
//            $data['crf_number'] = "CRF-{$data['series']}";
//        }

        if(!empty($data['item'])) {
            $checkRequestItems = $data['item'];
            unset($checkRequestItems['x']);
            unset($data['item']);
        }

        $data['requested_by'] = Auth::user()->id;

        $checkRequest = CheckRequests::create($data);
        if(isset($checkRequestItems)) {
            foreach($checkRequestItems as $item) {
                $item['check_request_id'] = $checkRequest->id;
                $item['description'] = trim($item['description']);
                $item['particulars'] = trim($item['particulars']);
                $item['reference_number'] = trim($item['reference_number']);
                $item['amount'] = str_replace(',', '', $item['amount']);

                CheckRequestItems::create($item);

//                CashFlowItems::where('id', $item['cash_flow_item_id'])->update(['status' => 'Used']);
            }
        }


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Accounting',
            'action' => 'Create',
            'description' => "Check Request {$checkRequest->crf_number} created",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);


        $request->session()->flash('popSuccess', "Check Request successfully created");
        return redirect(url('/admin/accounting/check_request'));
    }

    public function update_check_request(Request $request, $id)
    {
        $data['checkRequest'] = CheckRequests::find($id);
        if(!CheckRequests::where('id', $id)->exists()) {
            $request->session()->flash('popError', "Check Request does not exists");
            return redirect()->back();
        }

        $data['payeeType'] = empty($request->get('payee_type')) ? 'Member' : $request->get('payee_type');
        $data['signatories'] = User::whereIn('id', Signatories::pluck('user_id'))->orderBy('name')->get();


        $data['me'] = User::find(Auth::user()->id);


        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";
        return view('admin.accounting.update_check_request', $data);
    }

    public function update_check_request_post(Request $request, $id)
    {
        $data = $request->except('_token');
//        dd($data);

        if($request->segment(4) != 'view') {
            $data['request_date'] = Carbon::createFromFormat('m/d/Y', $data['request_date'])->format('Y-m-d');
            if (!empty($data['item'])) {
                $checkRequestItems = $data['item'];
                unset($data['item']);
            }

            $data['requested_by'] = Auth::user()->id;
        } else {
            if (!empty($data['item'])) {
                unset($data['item']);
            }
        }






        $checkRequest = CheckRequests::find($id);
//        dd($checkRequestItems);
        CheckRequests::where('id', $id)->update($data);

        if (isset($checkRequestItems)) {
            CheckRequestItems::where('check_request_id', $id)->delete();

            foreach ($checkRequestItems as $item) {
                $item['check_request_id'] = $checkRequest->id;
                $item['description'] = trim($item['description']);
                $item['particulars'] = trim($item['particulars']);
                $item['reference_number'] = trim($item['reference_number']);
                $item['amount'] = str_replace(',', '', $item['amount']);

                CheckRequestItems::create($item);

//                CashFlowItems::where('id', $item['cash_flow_item_id'])->update(['status' => 'Used']);
            }
        }


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Accounting',
            'action' => 'Edit',
            'description' => "Check Request {$checkRequest->crf_number} updated",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);


        $request->session()->flash('popSuccess', "Check Request updated created");
        return redirect()->back();
    }

    public function view_check_request(Request $request, $id)
    {
        $data['checkRequest'] = CheckRequests::find($id);
        if(!CheckRequests::where('id', $id)->exists()) {
            $request->session()->flash('popError', "Check Request does not exists");
            return redirect()->back();
        }

//        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";
        return view('admin.accounting.update_check_request', $data);
    }

    public function update_check_request_status(Request $request, $status, $id)
    {
        $checkRequest = CheckRequests::find($id);
        if(!CheckRequests::where('id', $id)->exists()) {
            $request->session()->flash('popError', "Check Request does not exists");
            return redirect()->back();
        }

        if($checkRequest->status != 'Pending') {
            $request->session()->flash('popError', "Check Request status must be Pending");
            return redirect()->back();
        }

        if($status == 'Cancel') {
            $actionStatus =  'Cancelled';
        } elseif($status == 'Approve') {
            $actionStatus =  'Approved';
        }

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Accounting',
            'action' => 'Edit',
            'description' => "Check Request {$checkRequest->crf_number} {$actionStatus}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $checkRequest->status = $actionStatus;
        $checkRequest->save();

        if($status == 'Cancel') {
            $request->session()->flash('popSuccess', "Check Request Cancelled successfully");
            return redirect()->back();
        } elseif($status == 'Approve') {
            $request->session()->flash('popSuccess', "Check Request Approved successfully");
            return redirect()->back();
        }
    }

    public function add_payee_name(Request $request)
    {
        if($request->isMethod('post')) {
            PayeeNames::create(['payee_name' => $request->payee_name, 'created_by' => Auth::user()->id]);

            AuditLog::create([
                'user_id' => Auth::user()->id,
                'category' => 'Accounting',
                'action' => 'Create',
                'description' => "Added a Payee Name: {$request->payee_name}",
                'is_admin' => session('is_admin') ? 'Y' : 'N'
            ]);

            $request->session()->flash('popSuccess', "Payee name <strong>{$request->payee_name}</strong> successfully added");
            return redirect()->back();
        }

        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Check Request (Payee Names)";
        return view('admin.accounting.add_payee_name', $data);
    }

    public function edit_payee_name(Request $request, $id)
    {
        if(PayeeNames::where('id', $id)->exists()) {
            $payeeName = PayeeNames::find($id);
            PayeeNames::where('id', $id)->update(['payee_name' => $request->payee_name]);

            AuditLog::create([
                'user_id' => Auth::user()->id,
                'category' => 'Accounting',
                'action' => 'Edit',
                'description' => "Updated Payee Name: {$payeeName->payee_name} to {$request->payee_name}",
                'is_admin' => session('is_admin') ? 'Y' : 'N'
            ]);

            $request->session()->flash('popSuccess', "Payee name <strong>{$payeeName->payee_name}</strong> successfully updated to <strong>$request->payee_name</strong>");
            return redirect()->back();
        }
    }

    public function delete_payee_name(Request $request, $id)
    {
        if(PayeeNames::where('id', $id)->exists()) {
            $payeeName = PayeeNames::find($id);

            AuditLog::create([
                'user_id' => Auth::user()->id,
                'category' => 'Accounting',
                'action' => 'Delete',
                'description' => "Delete Payee Name: {$payeeName->payee_name}",
                'is_admin' => session('is_admin') ? 'Y' : 'N'
            ]);

            PayeeNames::where('id', $id)->delete();

            $request->session()->flash('popSuccess', "Payee name <strong>{$payeeName->payee_name}</strong> successfully deleted");
            return redirect()->back();
        }
    }

    public function add_cash_flow_items(Request $request)
    {
        if($request->isMethod('post')) {
            $cashFlowItem = CashFlowItems::create([
                'payee_type' => $request->payee_type,
                'item' => $request->item,
                'created_by' => Auth::user()->id
            ]);

            AuditLog::create([
                'user_id' => Auth::user()->id,
                'category' => 'Accounting',
                'action' => 'Create',
                'description' => "Added Cash Flow Item: {$cashFlowItem->item}",
                'is_admin' => session('is_admin') ? 'Y' : 'N'
            ]);

            $request->session()->flash('popSuccess', "Cash Flow Item <strong>{$request->item}</strong> successfully added");
            return redirect()->back();
        }

        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";
        return view('admin.accounting.add_cash_flow_items', $data);
    }

    public function delete_cash_flow_items(Request $request, $id)
    {
        if(CashFlowItems::where('id', $id)->exists()) {
            $cashFlowItem = CashFlowItems::find($id);

            AuditLog::create([
                'user_id' => Auth::user()->id,
                'category' => 'Accounting',
                'action' => 'Delete',
                'description' => "Deleted Cash Flow Item: {$cashFlowItem->item}",
                'is_admin' => session('is_admin') ? 'Y' : 'N'
            ]);

            CashFlowItems::where('id', $id)->delete();



            $request->session()->flash('popSuccess', "Cash Flow Item <strong>{$cashFlowItem->item}</strong> successfully deleted");
            return redirect()->back();
        }
    }

    public function for_request()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";

//        $data['loans'] = $loans = Loans::select(DB::raw('loans.id, loans.loan_type_id, loans.loan_type, loans.is_voucher, loans.cla_number, profiles.id_number, users.name, profiles.first_name, profiles.middle_name, profiles.last_name, cost_centers.cost_center, loan_types.type, loans.granted_loan_amount, loans.status, loans.releasing_status, companies.company_name, COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.total_overdue_amount, 0) as total_overdue_amount, COALESCE(loan_payment_schedules.total_overdue_amount, 0) - COALESCE(loan_payments.total_amount_paid, 0) as outstanding_balance'))
//                                ->where('loans.status', 'Pending')
//                                ->where('check_requests.source_id', '!=', '')
//                                ->join('users', 'users.id', '=', 'loans.user_id', 'LEFT')
//                                ->join('profiles', 'profiles.user_id', '=', 'users.id', 'LEFT')
//                                ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
//                                ->join('employment_details', 'employment_details.user_id', '=', 'profiles.user_id', 'LEFT')
//                                ->join('companies', 'companies.id', '=', 'employment_details.principal_id', 'LEFT')
//                                ->join('loan_types', 'loan_types.id', '=', 'loans.loan_type_id', 'LEFT')
//                                ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')
//                                ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(amortization) + SUM(lapsed_interest) as total_overdue_amount FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payment_schedules.loan_id', '=', 'loans.id')
//                                ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT * FROM check_requests WHERE source = 'LOAN') check_requests"), 'check_requests.source_id', '=', 'loans.id')
//                                ->groupBy('loans.id')
//                                ->get();

        $data['loans'] = $loans = Loans::select(DB::raw('loans.id, loans.loan_type_id, loans.loan_type, loans.is_voucher, loans.cla_number, profiles.id_number, users.name, profiles.first_name, profiles.middle_name, profiles.last_name, cost_centers.cost_center, loan_types.type, loans.granted_loan_amount, loans.status, loans.releasing_status, companies.company_name, COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.total_overdue_amount, 0) as total_overdue_amount, COALESCE(loan_payment_schedules.total_overdue_amount, 0) - COALESCE(loan_payments.total_amount_paid, 0) as outstanding_balance'))
                                    ->join('users', 'users.id', '=', 'loans.user_id', 'LEFT')
                                    ->join('profiles', 'profiles.user_id', '=', 'users.id', 'LEFT')
                                    ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
                                    ->join('employment_details', 'employment_details.user_id', '=', 'profiles.user_id', 'LEFT')
                                    ->join('companies', 'companies.id', '=', 'employment_details.principal_id', 'LEFT')
                                    ->join('loan_types', 'loan_types.id', '=', 'loans.loan_type_id', 'LEFT')
                                    ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')
                                    ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(amortization) + SUM(lapsed_interest) as total_overdue_amount FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payment_schedules.loan_id', '=', 'loans.id')
                                    ->where('loans.status', 'Pending')
                                    ->whereRaw("(cv_number IS NULL OR cv_number = '')")
                                    ->groupBy('loans.id')
                                    ->get();

        $data['finalPays'] = FinalPays::where('status', 'Pending')->get();

//        dd($loans);


        return view('admin.accounting.for_request', $data);
    }

    public function print(Request $request, $id)
    {
        $data['checkRequest'] = $checkRequest = CheckRequests::find($id);

        if(empty($checkRequest)) {
            $request->session()->flash('popError', "Check Request does not exists");
            return redirect()->back();
        }

//        if($checkRequest->status != 'Approved') {
//            $request->session()->flash('popError', "Check Request status must be approved");
//            return redirect()->back();
//        }

        return view('print.check_request', $data);
    }
}

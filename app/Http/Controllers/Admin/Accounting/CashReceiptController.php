<?php

namespace App\Http\Controllers\Admin\Accounting;

use App\AuditLog;
use App\CashFlows;
use App\CashReceiptItems;
use App\CashReceipts;
use App\Invoices;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CashReceiptController extends Controller
{
    //
    public function index()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";

        $data['cashReceipts'] = CashReceipts::orderByDesc('created_at')->get();

        return view('admin.accounting.cash_receipt', $data);
    }

    public function create_cash_receipt()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Accounting";

        if(CashReceipts::count() == 0) {
            $data['orNumber'] = 1001;
        } else {
            $data['orNumber'] = CashReceipts::orderByDesc('or_number')->first()->or_number + 1;
        }

        return view('admin.accounting.create_cash_receipt', $data);
    }

    public function post(Request $request)
    {
//        dd($request->all());

        if(!empty($request->item)) {
            $cashReceiptItems = $request->item;
        }

        $data = $request->except(['_token', 'item']);

        $data['or_date']  = Carbon::createFromFormat('m/d/Y', $data['or_date'] )->format('Y-m-d');

        if(!empty($request->check_date)) {
            $data['check_date']  = Carbon::createFromFormat('m/d/Y', $data['check_date'] )->format('Y-m-d');
        }

        if($request->billing_type == 'Billing Invoice') {
            $data['payor_type'] = 'Client';
        }

        $data['total_amount'] = str_replace(',', '', $data['total_amount']);

        $data['user_id'] = Auth::user()->id;

        $cashReceipt = CashReceipts::create($data);

        if(!empty($cashReceiptItems)) {
            foreach($cashReceiptItems as $cashReceiptItem) {
                $cashReceiptItem['cash_receipt_id'] = $cashReceipt->id;
                $cashReceiptItem['amount'] = str_replace(',', '', $cashReceiptItem['amount']);

                $item = CashReceiptItems::create($cashReceiptItem);

                Invoices::where('invoice_number', $item->invoice_number)->update(['status' => 'Paid']);
            }
        }

//        dd($data);

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Accounting',
            'action' => 'Create',
            'description' => "Cash Receipt OR Date: {$cashReceipt->or_date} created",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);


        $request->session()->flash('popSuccess', "Cash Receipt successfully created");
        return redirect(url('/admin/accounting/cash_receipt'));
    }


}

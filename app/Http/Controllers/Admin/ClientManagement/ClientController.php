<?php
namespace App\Http\Controllers\Admin\ClientManagement;

use App\AuditLog;
use App\Companies;
use App\CompaniesContactPersons;
use App\CostCenters;
use App\PayrollGroups;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CivilStatus;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    // View Page
    public function index()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = 'Client Profile';

        return view('admin.client.index', $data);
    }

    public function profile($id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = 'Client Profile';
        $data['client'] =  Companies::find($id);
        $data['id'] = $id;

        return view('admin.client.profile', $data);
    }

    public function profile_edit($id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = 'Client Profile';
        $data['client'] =  Companies::find($id);
        $data['id'] = $id;

        return view('admin.client.profile_edit', $data);
    }

    public function profile_edit_post(Request $request, $id)
    {
        $data = $request->except('_token');

        $data['series'] = str_replace('C', '', $data['client_id']);

        $client = Companies::find($id);

        if(!empty($data['client_start_date'])) {
            $data['client_start_date'] = Carbon::createFromFormat('m/d/Y', $data['client_start_date'])->format('Y-m-d');
        }


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Client Management',
            'action' => 'Edit',
            'description' => "Updated Client: {$client->company_code}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        Companies::find($id)->update($data);


        $request->session()->flash('popSuccess', 'Client successfully updated!');
        return redirect(url("admin/client/{$id}"));
    }

    public function upload_logo(Request $request, $id)
    {
//        dd($request->logo);
        $file =  $request->logo->store('public/client_logo');
        $logo_path = ltrim(Storage::url($file), '/');

        $client = Companies::find($id);
        $client->logo_path = $logo_path;
        $client->save();

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Client Management',
            'action' => 'Edit',
            'description' => "Updated Client Logo: {$client->company_code}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);


        $request->session()->flash('popSuccess', 'Client Logo successfully updated!');
        return redirect()->back();
    }

    public function contact_persons(Request $request, $id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = 'Client Profile';
        $data['client'] =  Companies::find($id);
        $data['contactPersons'] = $data['client']->ContactPersons;
//        dd($data['contactPersons']);
        $data['id'] = $id;

        return view('admin.client.contact_persons', $data);
    }

    public function contact_persons_add(Request $request, $id)
    {
        $data = $request->except('_token');

        $data['company_id'] = $id;
//        $data['series'] = Companies::all()->max('series') + 1;
//        dd($data);

        $client = Companies::find($id);
        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Client Management',
            'action' => 'Create',
            'description' => "Created Client Contact Person: {$client->company_code}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        CompaniesContactPersons::create($data);

        $request->session()->flash('popSuccess', 'Contact Person successfully added!');
        return redirect()->back();
    }

    public function contact_persons_edit(Request $request, $client_id, $id)
    {
        $data = $request->except('_token');

        $client = Companies::find($client_id);
        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Client Management',
            'action' => 'Edit',
            'description' => "Updated Client: {$client->company_code}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        CompaniesContactPersons::find($id)->update($data);

        $request->session()->flash('popSuccess', 'Contact Person successfully updated!');
        return redirect()->back();
    }

    public function cost_center(Request $request, $id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = 'Client Profile';
        $data['client'] =  Companies::find($id);
        $data['payrollGroups'] = $data['client']->PayrollGroups;
        $data['costCenters'] = $data['client']->CostCenters;

        $data['id'] = $id;

        return view('admin.client.cost_center', $data);
    }

    public function cost_center_add(Request $request, $id)
    {
        $data = $request->except('_token');

        $data['company_id'] = $id;

        $data['cost_center'] = strtoupper($data['cost_center']);

        CostCenters::create($data);

        $client = Companies::find($id);
        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Client Management',
            'action' => 'Create',
            'description' => "Added Cost Center: {$data['cost_center']} for Client: {$client->company_code}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', 'Cost Center successfully added!');
        return redirect()->back();
    }

    public function cost_center_edit(Request $request, $client_id, $id)
    {
        $data = $request->except('_token');
        $data['cost_center'] = strtoupper($data['cost_center']);
        $costCenter = CostCenters::find($id);
        CostCenters::find($id)->update($data);

        $client = Companies::find($client_id);
        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Client Management',
            'action' => 'Edit',
            'description' => "Updated Cost Center: {$costCenter->cost_center} for Client: {$client->company_code}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);


        $request->session()->flash('popSuccess', 'Cost Center successfully updated!');
        return redirect()->back();
    }

    public function payroll_group_add(Request $request, $id)
    {
        $data = $request->except('_token');

        $data['company_id'] = $id;

        $data['group_name'] = strtoupper($data['group_name']);

        PayrollGroups::create($data);

        $client = Companies::find($id);
        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Client Management',
            'action' => 'Create',
            'description' => "Added Payroll Group: {$data['group_name']} for Client: {$client->company_code}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', 'Payroll Group successfully added!');
        return redirect()->back();
    }

    public function payroll_group_edit(Request $request, $client_id, $id)
    {
        $data = $request->except('_token');
        $data['group_name'] = strtoupper($data['group_name']);

        PayrollGroups::find($id)->update($data);

        $client = Companies::find($client_id);
        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Client Management',
            'action' => 'Edit',
            'description' => "Updated Payroll Group: {$data['group_name']} for Client: {$client->company_code}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', 'Payroll Group successfully updated!');
        return redirect()->back();
    }

    public function add()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = 'Client Registration';

        return view('admin.client.add_profile', $data);
    }

    public function add_post(Request $request)
    {
        $profile = $request->except(['_token']);

        $profile['series'] = str_replace('C', '', $profile['client_id']);

        if(Companies::where('company_code', $profile['company_code'])->count()) {
            $request->session()->flash('popError', 'Company Code already exists.');
            return redirect()->back()->withInput($request->all());
        }

//        dd($profile);

        $request->session()->put('client.profile', $profile);

        return redirect('admin/clients/add/contact-persons');
    }

    public function add_contact_persons()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = 'Client Registration';

        return view('admin.client.add_contact_persons', $data);
    }

    public function add_contact_persons_post(Request $request)
    {
        $contactPersons = $request->except(['_token']);
        if(!empty($contactPersons['contact_persons'])) {
            $contactPersons = $contactPersons['contact_persons'];
            $request->session()->put('client.contactPersons', $contactPersons);
        }


        return redirect('admin/clients/add/cost-center');
    }

    public function add_cost_center()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = 'Client Registration';
//        dd(session('client'));
//        dd(session('client.costCenters'));

        return view('admin.client.add_cost_center', $data);
    }

    public function add_cost_center_post(Request $request)
    {
        $data = $request->except(['_token']);
//        dd($request->all());
        if(!empty($data['cost_centers'])) {
            $costCenters = $data['cost_centers'];
            $request->session()->put('client.costCenters', $costCenters);
        }

        if(!empty($data['payroll_groups'])) {
            $payrollGroups = $data['payroll_groups'];
            $request->session()->put('client.payrollGroups', $payrollGroups);
        }

//        dd(session('client'));

        return redirect('admin/clients/add/billing');
    }

    public function add_billing()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = 'Client Registration';
//                dd(session('client'));

        return view('admin.client.add_billing', $data);
    }

    public function add_billing_post(Request $request)
    {
        $data = $request->except(['_token']);
//        dd(session('client'));
        $profile = session('client.profile');
        $profile['admin_fee_type'] = $data['admin_fee_type'];

//        dd($profile);

        if(!empty($data['admin_fee_percentage'])) {
            $profile['admin_fee_percentage'] = $data['admin_fee_percentage'];
        }

        if(!empty($data['admin_fee'])) {
            $profile['admin_fee'] = $data['admin_fee'];
        }

        if(!empty($profile['client_start_date'])) {
            $profile['client_start_date'] = Carbon::createFromFormat('m/d/Y', $profile['client_start_date'])->format('Y-m-d');
        }

        $profile['company_code'] = strtoupper($profile['company_code']);

        if(!empty($profile['parent_company_id'])) {
            $profile['company_code'] = Companies::find($profile['parent_company_id'])->company_code. "-{$profile['company_code']}";
        }

//        dd($profile);

        $profile['admin_breakdown'] = empty($request->admin_breakdown) ? '' : implode(';', $request->admin_breakdown);

        $client = Companies::create($profile);

        $company_id = $client->id;

        $contactPersons = session('client.contactPersons');
        if(!empty($contactPersons)) {
            foreach($contactPersons as $contactPerson) {
                $contactPerson['company_id'] = $company_id;

                CompaniesContactPersons::create($contactPerson);
            }
        }

        $costCenters = session('client.costCenters');
        if(!empty($costCenters)) {
            foreach($costCenters as $costCenter) {
                $costCenter['company_id'] = $company_id;

                CostCenters::create($costCenter);
            }
        }

        $payrollGroups = session('client.payrollGroups');
        if(!empty($payrollGroups)) {
            foreach($payrollGroups as $payrollGroup) {
                $payrollGroup['company_id'] = $company_id;

                PayrollGroups::create($payrollGroup);
            }
        }

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Client Management',
            'action' => 'Create',
            'description' => "Created new Client: {$client->company_code}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->forget('client');


        $request->session()->flash('popSuccess', 'Client registration completed!');
        return redirect('admin/client/'. $company_id);
    }

    public function client_billing(Request $request, $id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = 'Client Billing';
        $data['client'] =  Companies::find($id);
//        $data['payrollGroups'] = $data['client']->PayrollGroups;
//        $data['costCenters'] = $data['client']->CostCenters;

        $data['me'] = User::find(Auth::user()->id);

        $data['id'] = $id;

        return view('admin.client.billing', $data);
    }

    public function client_billing_post(Request $request, $id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = 'Client Billing';
        $data['client'] =  Companies::find($id);

        $data['id'] = $id;

        $post = $request->except('_token');

        if(!empty($post['admin_fee'])) {
            $post['admin_fee'] = str_replace(',', '', $post['admin_fee']);
        }

        if(!empty($post['admin_fee_percentage'])) {
            $post['admin_fee_percentage'] = str_replace('%', '', $post['admin_fee_percentage']);
            $post['admin_fee'] = str_replace(',', '', $post['admin_fee']);
        }

        Companies::where('id', $id)->update($post);

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Client Management',
            'action' => 'Edit',
            'description' => "Updated Billing for Client: {$data['client']->company_code}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', 'Billing successfully updated!');
        return redirect()->back();
    }

    public function user_accounts(Request $request, $id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = 'Client Profile';
        $data['client'] =  Companies::find($id);

        $data['id'] = $id;
//        dd($data['client']->UserAccounts());
        return view('admin.client.user_accounts', $data);
    }

    public function admin_breakdown_post(Request $request, $id)
    {
        $company = Companies::find($id);

        $adminBreakdown = empty($request->admin_breakdown) ? '' : implode(';', $request->admin_breakdown);

        $company->admin_breakdown = $adminBreakdown;
        $company->save();

        $request->session()->flash('popSuccess', 'Billing successfully updated!');
        return redirect()->back();
    }
}
?>
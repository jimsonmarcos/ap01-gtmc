<?php

namespace App\Http\Controllers\Admin;

use App\Departments;
use App\EmploymentDetails;
use App\Malls;
use App\Outlets;
use App\Provinces;
use App\TransferModes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CivilStatus;

class EmployeeController extends Controller
{
    //

    public function add()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Employee's Registration";

        $data['profile'] = session('user.profile');

        return view('admin.employee.add_profile', $data);
    }

    public function add_post(Request $request)
    {
        $data = $request->except(['_token']);

        $p = session('user.profile');

        $profile = [
            'birthday' => Carbon::createFromFormat('d/m/Y', $data['birthday'])->format('Y-m-d'),
            'gender' => strtoupper($data['gender']),
            'address' => strtoupper($data['address']),
            'mothers_maiden_name' => strtoupper($data['mothers_maiden_name']),
            'civil_status_id' => $data['civil_status_id'],
            'educational_attainment_id' => $data['educational_attainment_id'],
            'mobile1' => $data['mobile1'],
            'mobile2' => $data['mobile2'],
            'email' => strtolower($data['email']),
            'icoe_contact_name' => strtoupper($data['icoe_contact_name']),
            'icoe_contact_mobile' => strtoupper($data['icoe_contact_mobile']),
            'icoe_contact_address' => strtoupper($data['icoe_contact_address'])
        ];

        if($request->hasFile('photo')) {
            $profile['photo_path'] = $request->photo->store("tmp");
        }


        $profile = array_merge($p, $profile);
        // Store initial User Profile to session
        $request->session()->put('user.profile', $profile);

//        dd(session('user.profile'));

        return redirect('admin/employee/add/coop');
    }

    public function add_coop()
    {
        $data['profile'] = session('user.profile');
        $data['pageHeader'] = "Employee's Registration";
        $data['sidebar'] = view("admin.sidebar");

        return view('admin.employee.add_coop', $data);
    }

    public function add_coop_post(Request $request)
    {
        $data = $request->except(['_token']);

        $p = session('user.profile');

        $p['coop_share'] = $data['coop_share'];
        $request->session()->put('user.profile', $p);

        if(!empty($data['beneficiaries'])) {
            $beneficiaries = $data['beneficiaries'];
            $request->session()->put('user.beneficiaries', $beneficiaries);
        }


        return redirect('admin/employee/add/employment');
    }

    public function add_employment()
    {
        $data['profile'] = session('user.profile');
        $data['pageHeader'] = "Employee's Registration";
        $data['sidebar'] = view("admin.sidebar");
        $data['coordinators'] = EmploymentDetails::where('position_id', 3)->get();

        $data['provinces'] = Provinces::orderBy('province')->get();
        $data['malls'] = Malls::orderBy('mall')->get();
        $data['outlets'] = Outlets::orderBy('outlet')->get();

//        $data['provinces'] = Departments::groupBy('province')->orderBy('province')->get();
//        $data['areas'] = Departments::where('province', $data['provinces']->first()->province)->groupBy('area')->orderBy('area')->get();
//        $data['malls'] = Departments::where('province', $data['provinces']->first()->province)->where('area', $data['areas']->first()->area)->groupBy('mall')->orderBy('mall')->get();
//        $data['outlets'] = Departments::where('province', $data['provinces']->first()->province)->where('area', $data['areas']->first()->area)->where('mall', $data['malls']->first()->mall)->groupBy('outlet')->orderBy('outlet')->get();
//        $data['departments'] = Departments::where('province', $data['provinces']->first()->province)->where('area', $data['areas']->first()->area)->where('mall', $data['malls']->first()->mall)->where('outlet', $data['outlets']->first()->outlet)->groupBy('department')->orderBy('department')->get();

        return view('admin.employee.add_employment', $data);
    }

    public function add_employment_post(Request $request)
    {
        $data = $request->except(['_token']);

        $p = session('user.profile');

        if($request->has('orientation')) {
            $p['orientation'] = 'Y';
        }

        if($request->has('trainee_start_date')) {
            $p['trainee_start_date'] = Carbon::createFromFormat('d/m/Y', $data['trainee_start_date'])->format('Y-m-d');
        }

        if($request->has('trainee_end_date')) {
            $p['trainee_end_date'] = Carbon::createFromFormat('d/m/Y', $data['trainee_end_date'])->format('Y-m-d');
        }

//        $p['department_id'] = $request->input('department_id');
        $p['province_id'] = $request->province_id;
        $p['area_id'] = $request->area_id;
        $p['mall_id'] = $request->mall_id;
        $p['outlet_id'] = $request->outlet_id;
        if(!empty($request->input('coordinator_id'))) {
            $p['coordinator_id'] = $request->input('coordinator_id');
        }
//        dd($p);
        $request->session()->put('user.profile', $p);

        return redirect('admin/employee/add/compensation');
    }

    public function add_compensation()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Employee's Registration";

        $data['profile'] = session('user.profile');
        $data['transferModes'] = TransferModes::groupBy('transfer_mode')->orderBy('transfer_mode')->get();
        $data['transferTypes'] = TransferModes::where('transfer_mode', $data['transferModes']->first()->transfer_mode)->groupBy('transfer_type')->orderBy('transfer_type')->get();
        $data['banks'] = TransferModes::where('transfer_mode', $data['transferModes']->first()->transfer_mode)->where('transfer_type', $data['transferTypes']->first()->transfer_type)->orderBy('bank')->get();


        return view('admin.employee.add_compensation', $data);
    }

    public function add_compensation_post(Request $request)
    {
        $data = $request->except('_token');

        $compensation = $request->except(['_token', 'compute_sss', 'compute_hdmf', 'compute_philhealth']);

        if($request->has('compute_sss')) {
            $compensation['compute_sss'] = 'Y';
        }

        if($request->has('compute_hdmf')) {
            $compensation['compute_hdmf'] = 'Y';
        }

        if($request->has('compute_philhealth')) {
            $compensation['compute_philhealth'] = 'Y';
        }

        $profile = session('user.profile');

        if(!empty($profile['payroll_group_id'])) {
            $compensation['payroll_group_id'] = $profile['payroll_group_id'];
            unset($profile['payroll_group_id']);
        }

        $request->session()->put('user.profile', $profile);
        $request->session()->put('user.compensation', $compensation);

        return redirect('admin/complete-registration');
    }

}

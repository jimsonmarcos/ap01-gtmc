<?php

namespace App\Http\Controllers\Admin;

use App\AuditLog;
use App\Clients;
use App\Profiles;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index()
    {
        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = "User Admin List";

        $data['users'] = $users = User::whereIn('role', ['Admin', 'AdminPayroll', 'AdminCoop', 'AdminHR', 'AdminAcctg'])->get();

        return view('admin.user.admin_users', $data);
    }

    public function add_admin(Request $request)
    {
        if($request->isMethod('post')) {

            $user = User::find($request['user_id']);

//            $user->update(['role' => $request['role'], 'admin_username' => $request['username']]);

            if(User::where('parent_id', $user->id)->whereIn('role', ['Admin', 'AdminPayroll', 'AdminCoop', 'AdminHR', 'AdminAcctg'])->exists()) {
                User::where('parent_id', $user->id)->whereIn('role', ['Admin', 'AdminPayroll', 'AdminCoop', 'AdminHR', 'AdminAcctg'])->update(['role' => $request['role']]);
            } else {
                $data = [
                    'company_id' => $user->Profile->EmploymentDetails->Principal->id,
                    'parent_id' => $user->id,
                    'username' => $request['username'],
                    'password' => bcrypt($request['username']),
                    'name' => $user->name,
                    'email' => $user->Profile->email,
                    'is_new' => 'Y',
                    'role' => $request['role']
                ];

//            dd($data);

                User::create($data);

                AuditLog::create([
                    'user_id' => Auth::user()->id,
                    'category' => 'User Admin',
                    'action' => 'Edit',
                    'description' => "Updated {$user->Profile->name()} account has been updated to {$request['role']}",
                    'is_admin' => session('is_admin') ? 'Y' : 'N'
                ]);
            }



            $request->session()->flash('success', $user->Profile->name() ." account has been updated to {$request['role']}");
            return redirect('/admin/manage-admin/users');
        }

        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = "Add New User Admin";

        return view('admin.user.add_admin', $data);
    }

    public function edit_admin(Request $request, $user_id)
    {
        if($request->isMethod('post')) {

            $user = User::find($user_id);

            $user->update(['role' => $request['role']]);

            AuditLog::create([
                'user_id' => Auth::user()->id,
                'category' => 'User Admin',
                'action' => 'Edit',
                'description' => "Updated {$user->Profile->name()} account has been updated to {$request['role']}",
                'is_admin' => session('is_admin') ? 'Y' : 'N'
            ]);

            $request->session()->flash('success', $user->Profile->name() ." account has been updated to {$request['role']}");
            return redirect()->back();
        }

        $data['admin'] = User::find($user_id);
        if(!in_array($data['admin']->role, ['Employee', 'Member'])) {
            $request->session()->flash('error', 'Admin does not exists!');
            return redirect('/admin/manage-admin/users');
        }

        if($data['admin']->role == '') {
            $request->session()->flash('error', 'Admin does not exists!');
            return redirect('/admin/manage-admin/users');
        }


        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = "Edit User Admin";

        return view('admin.user.edit_admin', $data);
    }

    public function index_clients()
    {
        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = "Client Admin List";

        $data['users'] = $users = User::where("client_username", '!=', '')->get();
//        dd($users);
        return view('admin.user.admin_clients', $data);
    }

    public function add_client_admin(Request $request)
    {
        if($request->isMethod('post')) {
//            dd($request->all());

            if(!empty($request->user_id)) {
                $data = [
                    'client_username' => $request->client_username,
                    'client_type' => $request->client_type,
                    'company_id' => $request->company_id
                ];

                User::where('id', $request->user_id)->update($data);

                $profile = Profiles::where('user_id', $request->user_id)->first();
                AuditLog::create([
                    'user_id' => Auth::user()->id,
                    'category' => 'Client Admin',
                    'action' => 'Edit',
                    'description' => "Updated {$profile->name()} account has been updated to {$request->client_type}",
                    'is_admin' => session('is_admin') ? 'Y' : 'N'
                ]);

                $request->session()->flash('success', "<strong>Success!</strong> Client Account has been updated");
            } else {
                $data = [
                    'company_id' => $request->company_id,
                    'contact_person_id' => $request->contact_person_id,
                    'client_type' => $request->client_type,
                    'client_username' => $request->username,
                    'role' => 'Client User',
                    'password' => bcrypt($request->username)
                ];

                $user = User::create($data);


                AuditLog::create([
                    'user_id' => Auth::user()->id,
                    'category' => 'Client Admin',
                    'action' => 'Create',
                    'description' => "Updated {$user->username} account has been updated to {$request->client_type}",
                    'is_admin' => session('is_admin') ? 'Y' : 'N'
                ]);

                $request->session()->flash('success', "<strong>Success!</strong> Client Account has been created");
            }

//            if(User::where('username', $request['username'])->exists()) {
//                $request->session()->flash('error', 'Username already exists!');
//                return redirect()->back();
//            }
//
////            dd($request->all());
//
//            $name = trim($request['first_name']);
//            $name = !empty(trim($request['middle_name'])) ? $name. " ".trim($request['middle_name']) : $name;
//            $name = !empty(trim($request['last_name'])) ? $name. " ".trim($request['last_name']) : $name;
//
//            $user = User::create([
//                'company_id' => $request['company_id'],
//                'username' => $request['username'],
//                'email' => $request['email'],
//                'first_name' => $request['first_name'],
//                'middle_name' => $request['middle_name'],
//                'last_name' => $request['last_name'],
//                'password' => bcrypt( $request['username'] ),
//                'role' => 'ClientAdmin',
//                'name' =>  $name
//            ]);

//            $clientProfile = $request->except(['_token', 'username', 'role']);
//            $clientProfile['user_id'] = $user->id;
//            Clients::create($clientProfile);


            return redirect('/admin/manage-admin/clients');
        }

        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = "Add New Client Admin";

        return view('admin.user.add_client_admin', $data);
    }

    public function edit_client_admin(Request $request, $user_id)
    {
        if($request->isMethod('post')) {

            if(User::where('username', $request['username'])->where('id', '!=', $user_id)->exists()) {
                $request->session()->flash('error', 'Username already exists!');
                return redirect()->back();
            }

            $request['first_name'] = strtoupper($request['first_name']);
            $request['middle_name'] = strtoupper($request['middle_name']);
            $request['last_name'] = strtoupper($request['last_name']);
            $request['email'] = strtolower($request['email']);

            $user = User::find($user_id);
            $user = $user->update([
                'username' => $request['username'],
                'email' => $request['email'],
                'role' => $request['role'],
                'name' => trim($request['first_name']) ." ". trim($request['middle_name']) . " ". trim($request['last_name'])
            ]);

            $clientProfile = $request->except(['_token', 'username', 'role']);
//            dd($clientProfile);
            Clients::where('user_id', $user_id)->update($clientProfile);

            $request->session()->flash('success', "<strong>Success!</strong> account has been updated.");
            return redirect()->back();
        }

        $data['client'] = User::find($user_id);

        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = "Add Edit Client Admin";

        return view('admin.user.edit_client_admin', $data);
    }
}

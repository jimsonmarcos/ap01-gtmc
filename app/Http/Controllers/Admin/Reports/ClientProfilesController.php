<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Companies;
use App\CompaniesContactPersons;
use App\CostCenters;
use App\PayrollGroups;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class ClientProfilesController extends Controller
{
    //
    public function masterlist(Request $request)
    {
        $data['pageHeader'] = "Masterlist";
//        $data['companies'] = Companies::with('PayrollGroups')->with('CostCenters')->orderBy('company_code')->get();

        $data['principals'] = $data['companies'] = [];


        if($request->status == 'All') {
            $data['principals'] = $data['companies'] = Companies::orderBy('company_code')->get();
        } else {
            $data['principals'] = $data['companies'] = Companies::where('status', $request->status)->orderBy('company_code');
            $data['principals'] = $data['principals']->get();
        }

        if(empty($request->status)) {
            $data['principals'] = $data['companies'] = Companies::with('PayrollGroups')->orderBy('company_code');
            $data['principals'] = $data['principals']->get();
            $data['companies'] = $data['companies']->get();
//            dd($data['companies']->get());
        }

        if($request->company_id) {
            $data['companies'] = Companies::where('id', $request->company_id)->get();
        }

//        dd($data['companies']);


        $data['qs'] = $request->getQueryString();

//        $data['request'] = $request->all();

        return view('admin.reports.clients_profiles.masterlist', $data);
    }

    public function masterlist_download(Request $request)
    {
        $data['companies'] = [];


        if($request->status == 'All') {
            $companies = Companies::orderBy('company_code')->get();
        } else {
            $companies = Companies::where('status', $request->status)->orderBy('company_code');
        }

        if(empty($request->status)) {
            $data['companies'] = Companies::with('PayrollGroups')->orderBy('company_code');
            $companies = $companies->get();
//            dd($data['companies']->get());
        }

        if($request->company_id) {
            $companies = Companies::where('id', $request->company_id)->get();
        }


        $title = 'Client Masterlist';

        Excel::create($title, function ($excel) use ($companies, $title, $request) {


            // Set the title
            $excel->setTitle($title);

            $excel->sheet('Leave Credits', function ($sheet) use ($companies, $request) {

                $headers = [
                    'Client ID',
                    'Company Code',
                    'Company Name',
                    'Company Address',
                    'Trunk Line',
                    'Fax Number',
                    'Effectivity Date',
                    'TIN',
                    'Admin Fee Type',
                    'Admin Fee %',
                    'Admin Fee',
                    'Admin Fee Breakdown',
                ];

                // Sheet manipulation
                $sheet->row(1, $headers);


                $x = 2;
                foreach($companies as $company) {
                    $row = array(
                        $company->client_id,
                        $company->company_code,
                        $company->company_name,
                        $company->address,
                        $company->trunk_line,
                        $company->fax,
                        $company->client_start_date,
                        $company->tin,
                        $company->admin_fee_type,
                        $company->admin_fee_type == 'Percentage' ? $company->admin_fee_percentage : '',
                        $company->admin_fee_type == 'Fixed' ? $company->admin_fee : '',
                        $company->admin_breakdown
                    );


                    $sheet->row($x, $row);

                    $x++;
                }
            });
        })->download('xls');
    }

    public function contact_list()
    {
        $data['pageHeader'] = "Contact List";
        return view('admin.reports.clients_profiles.contact_list', $data);
    }

    public function cost_centers(Request $request)
    {
        $data['pageHeader'] = "Cost Center";

        $data['qs'] = $request->getQueryString();

        $data['principals'] = $data['companies'] = [];


        if($request->status == 'All') {
            $data['principals'] = Companies::orderBy('company_code')->get();
        } else {
            $data['principals'] = Companies::where('status', $request->status)->orderBy('company_code');
            $data['principals'] = $data['principals']->get();
        }

        if(empty($request->status)) {
            $data['principals'] = Companies::with('PayrollGroups')->orderBy('company_code');
            $data['principals'] = $data['principals']->get();
        }

//        $data['companies'] = Companies::with('PayrollGroups')->with('CostCenters')->orderBy('company_code')->where(function($query) use ($request) {
//            if($request->get('status') == 'Active') {
////                $query->where('status', 'Active');
//            } elseif($request->get('status') == 'Deactivated') {
////                $query->where('status', 'Deactivated');
//            }
//        })->get();

        $data['costCenters'] = CostCenters::leftJoin('companies', 'cost_centers.company_id', '=', 'companies.id')
            ->where(function($query) use ($request) {
            if(!empty($request->get('company_id'))) {
                $query->where('company_id', $request->get('company_id'));
            }

            if(!empty($request->status) && $request->status != 'All') {
                $query->where('companies.status', $request->status);
            }
        })->get();

        return view('admin.reports.clients_profiles.cost_centers', $data);
    }

    public function cost_centers_download(Request $request)
    {
        $costCenters = CostCenters::leftJoin('companies', 'cost_centers.company_id', '=', 'companies.id')
            ->where(function($query) use ($request) {
                if(!empty($request->get('company_id'))) {
                    $query->where('company_id', $request->get('company_id'));
                }

                if(!empty($request->status) && $request->status != 'All') {
                    $query->where('companies.status', $request->status);
                }
            })->get();

        $title = 'Cost Center';

        Excel::create($title, function ($excel) use ($costCenters, $title, $request) {


            // Set the title
            $excel->setTitle($title);

            $excel->sheet('Cost Center', function ($sheet) use ($costCenters, $request) {

                $headers = [
                    'Company Code',
                    'Cost Center',
                    'Cost Center Definition',
                    'Status'
                ];

                // Sheet manipulation
                $sheet->row(1, $headers);


                $x = 2;
                foreach($costCenters as $costCenter) {

                    $row = array(
                        $costCenter->Company->company_code,
                        $costCenter->cost_center,
                        $costCenter->group_definition,
                        $costCenter->Company->status
                    );


                    $sheet->row($x, $row);

                    $x++;
                }

//                $sheet->setColumnFormat(array(
//                    'A' => '@',
//                    'B' => '@',
//                    'C' => '@',
//                    'D' => '@'
//                ));

//                $sheet->setColumnFormat(array(
//                    'F2:G'.$x => '#,##0.00'
//                ));

            });
        })->download('xls');
    }

    public function payroll_groups(Request $request)
    {
        $data['pageHeader'] = "Payroll Group";
        $data['principals'] = $data['companies'] = [];


        if($request->status == 'All') {
            $data['principals'] = $data['companies'] = Companies::orderBy('company_code')->get();
        } else {
            $data['principals'] = $data['companies'] = Companies::where('status', $request->status)->orderBy('company_code');
            $data['principals'] = $data['principals']->get();
        }

        if(empty($request->status)) {
            $data['principals'] = $data['companies'] = Companies::with('PayrollGroups')->orderBy('company_code');
            $data['principals'] = $data['principals']->get();
            $data['companies'] = $data['companies']->get();
//            dd($data['companies']->get());
        }

        if($request->company_id) {
            $data['companies'] = Companies::where('id', $request->company_id)->get();
        }

//        dd(Companies::all());


        $data['qs'] = $request->getQueryString();

//        $data['request'] = $request->all();

        return view('admin.reports.clients_profiles.payroll_groups', $data);
    }

    public function payroll_groups_download(Request $request)
    {
        if($request->status == 'All') {
            $companies = Companies::orderBy('company_code')->get();
        } else {
            $companies = Companies::where('status', $request->status)->orderBy('company_code')->get();
        }

        if(empty($request->status)) {
            $companies = Companies::with('PayrollGroups')->orderBy('company_code');
            $companies = $companies->get();
//            dd($data['companies']->get());
        }

        if($request->company_id) {
            $companies = Companies::where('id', $request->company_id)->get();
        }


        $title = 'Client Payroll Groups';

        Excel::create($title, function ($excel) use ($companies, $title, $request) {


            // Set the title
            $excel->setTitle($title);

            $excel->sheet('Leave Credits', function ($sheet) use ($companies, $request) {

                $headers = [
                    'Company Code',
                    'Payroll Group',
                    'Start Date A',
                    'Payroll Date A',
                    'Start Date B',
                    'Payroll Date B',
                    'Payroll Group Definition',
                    'Status'
                ];

                // Sheet manipulation
                $sheet->row(1, $headers);


                $x = 2;
                foreach($companies as $company) {
                    foreach($company->PayrollGroups as $payrollGroup) {
                        $row = array(
                            $company->company_code,
                            $payrollGroup->group_name,
                            $payrollGroup->start_date_a,
                            $payrollGroup->payroll_date_a,
                            $payrollGroup->start_date_b,
                            $payrollGroup->payroll_date_b,
                            $payrollGroup->group_definition,
                            $company->status,
                        );


                        $sheet->row($x, $row);

                        $x++;
                    }
                }
            });
        })->download('xls');
    }

    public function contact_persons(Request $request)
    {
        $data['pageHeader'] = "Client Contact Person";

        if($request->status) {
            $data['contactPersons'] = CompaniesContactPersons::leftJoin('companies', 'companies_contact_persons.company_id', '=', 'companies.id')
                ->where('companies.status', $request->status)->get();
        } else {
            $data['contactPersons'] = CompaniesContactPersons::all();
        }


        $data['qs'] = $request->getQueryString();

        return view('admin.reports.clients_profiles.contact_persons', $data);
    }

    public function contact_persons_download(Request $request)
    {
//        $contactPersons = CompaniesContactPersons::with('Company')->get();

        if($request->status) {
            $contactPersons = CompaniesContactPersons::leftJoin('companies', 'companies_contact_persons.company_id', '=', 'companies.id')
                ->where('companies.status', $request->status)->get();
        } else {
            $contactPersons = CompaniesContactPersons::all();
        }

        $title = 'Client Contact Person';

        Excel::create($title, function ($excel) use ($contactPersons, $title, $request) {


            // Set the title
            $excel->setTitle($title);

            $excel->sheet('Client Contact Person', function ($sheet) use ($contactPersons, $request) {

                $headers = [
                    'Company Code',
                    'Contact Name',
                    'Position',
                    'Email Address',
                    'Mobile Number',
                    'Office Number'
                ];

                // Sheet manipulation
                $sheet->row(1, $headers);


                $x = 2;
                foreach($contactPersons as $contactPerson) {
                    $mobile = $contactPerson->mobile1;
                    $mobile = !empty($contactPerson->mobile2) ? "{$mobile}, {$contactPerson->mobile2}" : $mobile;

                    $officeNumber = $contactPerson->office_number;
                    $officeNumber = !empty($contactPerson->local_number) ? "{$officeNumber} loc. {$contactPerson->local_number}" : $officeNumber;

                    $row = array(
                        $contactPerson->Company->company_code,
                        $contactPerson->complete_name(),
                        $contactPerson->Position->title,
                        $contactPerson->email,
                        $mobile,
                        trim($officeNumber)
                    );


                    $sheet->row($x, $row);

                    $x++;
                }
            });
        })->download('xls');
    }
}

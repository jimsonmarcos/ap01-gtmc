<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Companies;
use App\PayrollCycles;
use App\PayrollDtr;
use App\PayrollGroups;
use App\Profiles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class PayrollListingController extends Controller
{
    //
    public function index(Request $request)
    {
        $data['companies'] = Companies::with('PayrollGroups.PayrollCycles')->orderBy('company_code')->get();

//        $users = User::select()

//        $users = Profiles::selectRaw('profiles.*')
//            ->orderBy('profiles.last_name');
//
//        $users->selectRaw('ed.hire_date, cp.company_code, ed.status as employment_status')
//            ->leftJoin('employment_details as ed', 'ed.user_id', 'profiles.user_id')
//            ->leftJoin('compensations as c', 'c.user_id', 'profiles.user_id')
//            ->leftJoin('companies as cp', 'cp.id', 'ed.principal_id');
//
//        $users->where('profiles.membership_category', 'Employee');
//
//        if(!empty($request->search)) {
//            $users->whereRaw("(profiles.id_number LIKE '%{$request->search}% OR CONCAT(profiles.first_name, ' ', profiles.last_name) LIKE '%{$request->search}%'')");
//        }
//
//        $users->where('ed.status', 'Active');


        if(!empty($request->payroll_code)) {
            $data['company'] = Companies::find($request->company_id);
            $data['payrollGroup'] = PayrollGroups::find($request->payroll_group);

//            dd($data['company']->PayrollGroups);

            $data['payrollCycle'] = PayrollCycles::find($request->payroll_code);


//            $users->where('ed.principal_id', $request->company_id);

//            if($request->payroll_group != 'All') {
////                $users->where('pg.id', $request->payroll_group);
//                $data['payrollCycles'] = PayrollCycles::whereIn('payroll_group_id', $request->payroll_group)->where('status', 'Processed')->orderBy('payroll_date');
//            } else {
//                $data['payrollCycles'] = PayrollCycles::whereIn('payroll_group_id', PayrollGroups::where('company_id', $request->company_id)->pluck('id'))->where('status', 'Processed')->orderBy('payroll_date');
//            }
//
//
//            if($data['payrollCycles']->count() > 0) {
//                if(!empty($request->payroll_code)) {
//                    $data['payrollMembers'] = PayrollDtr::where('payroll_cycle_id', $data['payrollCycles']->where('payroll_code', PayrollCycles::where('payroll_code', $request->payroll_code))->first()->id)->get();
//                } else {
//                    $data['payrollMembers'] = PayrollDtr::whereIn('payroll_cycle_id', $data['payrollCycles']->pluck('id')->toArray())->get();
//                }
//            }
        }

//        dd($data['payrollCycle']);

//        $data['users'] = $users->get();

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['nav'] = 'payroll-listing';

        $data['pageHeader'] = "Payroll";
        return view('admin.reports.payroll.payroll_listing', $data);
    }

    public function download(Request $request)
    {
        $payrollCycle = PayrollCycles::find($request->payroll_code);
        $company = Companies::find($request->company_id);
        $payrollGroup = PayrollGroups::find($request->payroll_group);

        $title = 'Payroll Listing';

        Excel::create($title, function ($excel) use ($company, $payrollGroup, $payrollCycle, $title, $request) {
            // Set the title
            $excel->setTitle($title);

            $excel->sheet($payrollCycle->payroll_code, function ($sheet) use ($company, $payrollGroup, $payrollCycle, $request) {

                $headers = [
                    'Company Code',
                    'Payroll Group',
                    'Payroll Code',
                    'Payroll Date',
                    'Employee Name',
                    'Net Pay',
                    'Coordinator',
                    'Transfer Type',
                    'Transfer Mode',
                    'Account Number',
                    'GCASH Number'
                ];

                // Sheet manipulation
                $sheet->row(1, $headers);


                $x = 2;
                foreach ($payrollCycle->Dtr as $payrollMember) {

                    $row = array(
                        $company->company_code,
                        $payrollGroup->group_name,
                        $payrollCycle->payroll_code,
                        date('m/d/Y', strtotime($payrollCycle->payroll_date)),
                        $payrollMember->User->Profile->namelfm(),
                        $payrollMember->Summary()->net_pay,
                        !empty($payrollMember->Profile->EmploymentDetails->Coordinator) ? $payrollMember->Profile->EmploymentDetails->Coordinator : '',
                        !empty($payrollMember->Profile->Compensation->TransferMode->transfer_type) ? $payrollMember->Profile->Compensation->TransferMode->transfer_type : '',
                        !empty($payrollMember->Profile->Compensation->TransferMode->transfer_mode) ? $payrollMember->Profile->Compensation->TransferMode->transfer_mode : '',
                        $payrollMember->Profile->Compensation->account_number,
                        $payrollMember->Profile->Compensation->mobile_number
                    );


                    $sheet->row($x, $row);

                    $x++;
                }

                $sheet->setColumnFormat(array(
//                    'A' => '@',
//                    'B' => '@',
//                    'C' => '@',
                    'F' => '#,##0.00'
                ));
            });
        })->download('xls');
    }
}

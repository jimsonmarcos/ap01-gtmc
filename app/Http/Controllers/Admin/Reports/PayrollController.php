<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Companies;
use App\PayrollCycles;
use App\PayrollGroups;
use App\PayrollSummary;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class PayrollController extends Controller
{
    //
    public function payslip()
    {
        $data['pageHeader'] = "Payslip";
        return view('admin.reports.payroll.payslip', $data);
    }

    public function summary(Request $request)
    {
        $data['companies'] = Companies::with('PayrollGroups')->with('CostCenters')->orderBy('company_code')->get();

        if(!empty($request->company_id)) {
            $data['company'] = $company = Companies::find($request->company_id);
            if($request->payroll_group_id == 'All') {
                $payrollCycles = PayrollCycles::whereIn('payroll_group_id', $company->PayrollGroups->toArray());
            } else {
                $payrollCycles = PayrollCycles::where('payroll_group_id', $request->payroll_group_id);
            }

            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $payrollCycles->whereRaw("payroll_date >= '{$dateFrom}' AND payroll_date <= '{$dateTo}'");
            }

            $payrollCycles->orderByDesc('payroll_date');
            $data['payrollCycles'] = $payrollCycles->get();
        }

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['nav'] = 'payroll-summary';

        $data['pageHeader'] = "Summary of Payroll";
        return view('admin.reports.payroll.summary', $data);
    }

    public function summary_download(Request $request)
    {
        $company = Companies::find($request->company_id);
        $payrollGroup = '';
        if($request->payroll_group_id == 'All') {
            $payrollCycles = PayrollCycles::whereIn('payroll_group_id', $company->PayrollGroups->toArray());
        } else {
            $payrollGroup = PayrollGroups::find($request->payroll_group_id);
            $payrollCycles = PayrollCycles::where('payroll_group_id', $request->payroll_group_id);
        }

        if(!empty($request->date_from) && !empty($request->date_to)) {
            $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
            $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
            $payrollCycles->whereRaw("payroll_date >= '{$dateFrom}' AND payroll_date <= '{$dateTo}'");
        }

        $payrollCycles->orderByDesc('payroll_date');

        $title = "{$company->company_code} - {$payrollGroup->group_name}";
//        $title = 'Payroll Summary';

        Excel::create($title, function ($excel) use ($company, $payrollCycles, $payrollGroup, $title, $request) {
            // Set the title
            $excel->setTitle($title);

            $sheetTitle = !empty($payrollGroup) ? $payrollGroup->group_name : 'All';

            $excel->sheet($sheetTitle, function ($sheet) use ($company, $payrollGroup, $payrollCycles, $request) {

                $headers = [
                    'Payroll Code',
                    'Principal',
                    'Payroll Group',
                    'Payroll Period',
                    'Payroll Date',
                    'BASIC PAY',
                    'ECOLA',
                    'OVERTIME PAY',
                    'HOLIDAY PAY',
                    'NIGHT DIFFERENTIAL',
                    'HOLIDAY ECOLA',
                    'ADJUSTMENT WAGE',
                    'ADJUSTMENT DEDUCTION',
                    'ADJUSTMENT INCOME',
                    'DE MINIMIS',
                    'FIXED ALLOWANCE',
                    'LEAVE ENCASHMENT',
                    'INCENTIVES',
                    'COOP SAVINGS',
                    'SSS EC',
                    'SSS ER',
                    'HDMF ER',
                    'PHILHEALTH ER',
                    'SSS EE',
                    'HDMF EE',
                    'PHILHEALTH EE',
                    'TARDINESS',
                    'ABSENCES',
                    'SSS LOAN',
                    'HDMF LOAN',
                    'CLIENT CHARGES',
                    'MEMBERSHIP FEE',
                    'NOTARIAL FEE',
                    'COOP LOAN',
                    'COOP SHARE',
                    'UNPAID LEAVE',
                    'GROSS AMOUNT',
                    'DEDUCTIONS',
                    'TAX',
                    'NET PAY',
                    'THIRTEENTH MONTH BONUS',
                    'THIRTEENTH MONTH PRO RATED'
                ];

                // Sheet manipulation
                $sheet->row(1, $headers);



                $x = 2;
                foreach ($payrollCycles->get() as $payrollCycle) {

                    if(!empty($request->cost_center_id) && $request->cost_center_id != 'All') {
                        $payrollSummary = PayrollSummary::where('payroll_cycle_id', $payrollCycle->id);
                        $payrollSummary->leftJoin('profiles as p', 'p.user_id', '=', 'payroll_summary.user_id');
                        $payrollSummary->where('p.cost_center_id', $request->cost_center_id);
                        $payrollSummary->get();

                        $row = array(
                            $payrollCycle->payroll_code,
                            $company->company_code,
                            $payrollCycle->PayrollGroup->group_name,
                            $payrollCycle->payroll_period,
                            date('m/d/Y', strtotime($payrollCycle->payroll_date)),
                            $payrollSummary->sum('basic_pay'),
                            $payrollSummary->sum('ecola'),
                            $payrollSummary->sum('overtime_pay'),
                            $payrollSummary->sum('holiday_pay'),
                            $payrollSummary->sum('night_differential'),
                            $payrollSummary->sum('holiday_ecola'),
                            $payrollSummary->sum('adjustment_wage'),
                            $payrollSummary->sum('adjustment_deduction'),
                            $payrollSummary->sum('adjustment_income'),
                            $payrollSummary->sum('de_minimis'),
                            $payrollSummary->sum('fixed_allowance'),
                            $payrollSummary->sum('leave_encashment'),
                            $payrollSummary->sum('incentives'),
                            $payrollSummary->sum('coop_savings'),
                            $payrollSummary->sum('sss_ec'),
                            $payrollSummary->sum('sss_er'),
                            $payrollSummary->sum('hdmf_er'),
                            $payrollSummary->sum('philhealth_er'),
                            $payrollSummary->sum('sss_ee'),
                            $payrollSummary->sum('hdmf_ee'),
                            $payrollSummary->sum('philhealth_ee'),
                            $payrollSummary->sum('tardiness'),
                            $payrollSummary->sum('absences'),
                            $payrollSummary->sum('sss_loan'),
                            $payrollSummary->sum('hdmf_loan'),
                            $payrollSummary->sum('client_charges'),
                            $payrollSummary->sum('membership_fee'),
                            $payrollSummary->sum('notarial_fee'),
                            $payrollSummary->sum('coop_loan'),
                            $payrollSummary->sum('coop_share'),
                            $payrollSummary->sum('unpaid_leave'),
                            $payrollSummary->sum('gross_amount'),
                            $payrollSummary->sum('deductions'),
                            $payrollSummary->sum('tax'),
                            $payrollSummary->sum('net_pay'),
                            $payrollSummary->sum('thirteenth_month_bonus'),
                            $payrollSummary->sum('thirteenth_month_pro_rated')
                        );
                    } else {
                        $row = array(
                            $payrollCycle->payroll_code,
                            $company->company_code,
                            $payrollCycle->PayrollGroup->group_name,
                            $payrollCycle->payroll_period,
                            date('m/d/Y', strtotime($payrollCycle->payroll_date)),
                            $payrollCycle->Summary->sum('basic_pay'),
                            $payrollCycle->Summary->sum('ecola'),
                            $payrollCycle->Summary->sum('overtime_pay'),
                            $payrollCycle->Summary->sum('holiday_pay'),
                            $payrollCycle->Summary->sum('night_differential'),
                            $payrollCycle->Summary->sum('holiday_ecola'),
                            $payrollCycle->Summary->sum('adjustment_wage'),
                            $payrollCycle->Summary->sum('adjustment_deduction'),
                            $payrollCycle->Summary->sum('adjustment_income'),
                            $payrollCycle->Summary->sum('de_minimis'),
                            $payrollCycle->Summary->sum('fixed_allowance'),
                            $payrollCycle->Summary->sum('leave_encashment'),
                            $payrollCycle->Summary->sum('incentives'),
                            $payrollCycle->Summary->sum('coop_savings'),
                            $payrollCycle->Summary->sum('sss_ec'),
                            $payrollCycle->Summary->sum('sss_er'),
                            $payrollCycle->Summary->sum('hdmf_er'),
                            $payrollCycle->Summary->sum('philhealth_er'),
                            $payrollCycle->Summary->sum('sss_ee'),
                            $payrollCycle->Summary->sum('hdmf_ee'),
                            $payrollCycle->Summary->sum('philhealth_ee'),
                            $payrollCycle->Summary->sum('tardiness'),
                            $payrollCycle->Summary->sum('absences'),
                            $payrollCycle->Summary->sum('sss_loan'),
                            $payrollCycle->Summary->sum('hdmf_loan'),
                            $payrollCycle->Summary->sum('client_charges'),
                            $payrollCycle->Summary->sum('membership_fee'),
                            $payrollCycle->Summary->sum('notarial_fee'),
                            $payrollCycle->Summary->sum('coop_loan'),
                            $payrollCycle->Summary->sum('coop_share'),
                            $payrollCycle->Summary->sum('unpaid_leave'),
                            $payrollCycle->Summary->sum('gross_amount'),
                            $payrollCycle->Summary->sum('deductions'),
                            $payrollCycle->Summary->sum('tax'),
                            $payrollCycle->Summary->sum('net_pay'),
                            $payrollCycle->Summary->sum('thirteenth_month_bonus'),
                            $payrollCycle->Summary->sum('thirteenth_month_pro_rated')
                        );
                    }




                    $sheet->row($x, $row);

                    $x++;
                }

                $sheet->setColumnFormat(array(
//                    'A' => '@',
//                    'B' => '@',
//                    'C' => '@',
                    'F' => '#,##0.00'
                ));
            });
        })->download('xls');
    }
}

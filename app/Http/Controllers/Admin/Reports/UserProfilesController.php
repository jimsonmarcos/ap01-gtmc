<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Companies;
use App\Profiles;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class UserProfilesController extends Controller
{
    //
    public function masterlist(Request $request)
    {
        $data['pageHeader'] = 'Masterlist';

        $data['companies'] = Companies::with('PayrollGroups')->with('CostCenters')->orderBy('company_code')->get();

//        $users = User::select()

        $users = Profiles::selectRaw('profiles.*, cc.cost_center')
                ->leftJoin('cost_centers as cc', 'cc.id', 'profiles.cost_center_id')
                ->orderBy('profiles.last_name');

        $users->selectRaw('profiles.user_id, ed.hire_date, pg.group_name as payroll_group, cp.company_code, ed.status as employment_status')
            ->leftJoin('employment_details as ed', 'ed.user_id', 'profiles.user_id')
            ->leftJoin('compensations as c', 'c.user_id', 'profiles.user_id')
            ->leftJoin('payroll_groups as pg', 'pg.id', 'c.payroll_group_id')
            ->leftJoin('companies as cp', 'cp.id', 'ed.principal_id');

        if(!empty($request->category) && $request->category == 'Employee') {


            if($request->employment_status == 'Deactivated') {
                $users->where('ed.status', 'Deactivated');
            }



            if(!empty($request->company_id)) {
                $users->where('ed.principal_id', $request->company_id);
            }
            if($request->payroll_group != 'All') {
                $users->where('pg.id', $request->payroll_group);
            }

            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $users->whereRaw("(ed.hire_date >= '{$dateFrom}' AND ed.hire_date <= '{$dateTo}')");
            }
        };


        if(!empty($request->cost_center) && $request->cost_center != 'All') {
            $users->where('cc.id', $request->cost_center);
        }

        if(!empty($request->category)) {
            $users->where('profiles.membership_category', $request->category);
        } else {
            $users->where('profiles.membership_category', 'Employee');
        }

        if(!empty($request->membership_status)) {
            $users->where('profiles.status', $request->membership_status);
        }

        if(!empty($request->membership_type)) {
            $users->where('profiles.membership_type', $request->membership_type);
        }

        if(!empty($request->search)) {
            $users->whereRaw("(profiles.first_name LIKE '%{$request->search}%' OR profiles.last_name LIKE '%{$request->search}%' OR profiles.id_number LIKE '%{$request->search}%')");
        }

        $data['users'] = $users->get();

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        return view('admin.reports.user_profiles.masterlist', $data);
    }

    public function masterlist_download(Request $request)
    {
        ini_set('max_execution_time', 3000); //5 minutes

        $users = Profiles::selectRaw('profiles.*, cc.cost_center')
            ->leftJoin('cost_centers as cc', 'cc.id', 'profiles.cost_center_id')
            ->orderBy('profiles.last_name');

        $users->selectRaw('ed.hire_date, pg.group_name as payroll_group, cp.company_code, ed.status as employment_status')
            ->leftJoin('employment_details as ed', 'ed.user_id', 'profiles.user_id')
            ->leftJoin('compensations as c', 'c.user_id', 'profiles.user_id')
            ->leftJoin('payroll_groups as pg', 'pg.id', 'c.payroll_group_id')
            ->leftJoin('companies as cp', 'cp.id', 'ed.principal_id');

        if(!empty($request->category) && $request->category == 'Employee') {


            if($request->employment_status == 'Deactivated') {
                $users->where('ed.status', 'Deactivated');
            }



            if(!empty($request->company_id)) {
                $users->where('ed.principal_id', $request->company_id);
            }
            if($request->payroll_group != 'All') {
                $users->where('pg.id', $request->payroll_group);
            }

            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $users->whereRaw("(ed.hire_date >= '{$dateFrom}' AND ed.hire_date <= '{$dateTo}')");
            }
        };


        if(!empty($request->cost_center) && $request->cost_center != 'All') {
            $users->where('cc.id', $request->cost_center);
        }

        if(!empty($request->category)) {
            $users->where('profiles.membership_category', $request->category);
        } else {
            $users->where('profiles.membership_category', 'Employee');
        }

        if(!empty($request->membership_status)) {
            $users->where('profiles.status', $request->membership_status);
        }

        if(!empty($request->membership_type)) {
            $users->where('profiles.membership_type', $request->membership_type);
        }

        if(!empty($request->search)) {
            $users->whereRaw("(profiles.first_name LIKE '%{$request->search}%' OR profiles.last_name LIKE '%{$request->search}%' OR profiles.id_number LIKE '%{$request->search}%')");
        }

        $users = $users->get();

        $title = 'User Masterlist';

        Excel::create($title, function ($excel) use ($users, $title, $request) {


            // Set the title
            $excel->setTitle($title);

            $sheetTitle = (!empty($request->category) && $request->category == 'Employee') || empty($request->category) ? 'Employee' : 'Member';

            $excel->sheet($sheetTitle, function ($sheet) use ($users, $request) {

                $headers = [
                    'GTMC ID Number',
                    'Last Name',
                    'First Name',
                    'Middle Name',
                    'Civil Status',
                    'Gender',
                    'Address',
                    'Mobile 1',
                    'Mobile 2',
                    'Email',
                    'Birth Date',
                    'Age',
                    'Mother\'s Maiden Name',
                    'Education Attainment',
                    'Contact Person',
                    'Contact Number',
                    'Contact Address',
                    'Category',
                    'Principal'
                ];

                if((!empty($request->category) && $request->category == 'Employee') || empty($request->category)) {
                    $headers[] = 'Hire Date';
                    $headers[] = 'Position';
                    $headers[] = 'Department';
                    $headers[] = 'Coordinator';
                    $headers[] = 'SSS';
                    $headers[] = 'HDMF';
                    $headers[] = 'PHIC';
                    $headers[] = 'TIN';
                    $headers[] = 'Payroll Group';
                    $headers[] = 'Mode of Transfer';
                    $headers[] = 'Transfer Type';
                    $headers[] = 'Bank';
                    $headers[] = 'Mobile Number';
                    $headers[] = 'Account Number';
                    $headers[] = 'Tax Type';
                    $headers[] = 'Minimum Wage Earner (Yes/No)';
                    $headers[] = 'Rate Type';
                    $headers[] = 'Daily Rate Category';
                    $headers[] = 'Location';
                    $headers[] = 'Monthly';
                    $headers[] = 'Daily';
                    $headers[] = 'Hourly';
                    $headers[] = 'Employment Status';
                }
                $headers[] = 'Membership Type';
                $headers[] = 'Cost Center';
                $headers[] = 'Join Date';
                $headers[] = 'Coop Share Contribution';
                $headers[] = 'Membership Status';

                // Sheet manipulation
                $sheet->row(1, $headers);



                $x = 2;
                foreach($users as $user) {

                    $row = array(
                        $user->id_number,
                        $user->last_name,
                        $user->first_name,
                        $user->middle_name,
                        !empty($user->CivilStatus->title) ? $user->CivilStatus->title : '',
                        $user->gender,
                        $user->address,
                        $user->mobile1,
                        $user->mobile2,
                        $user->email,
                        !empty($user->birthday && $user->birthday != '0000-00-00') ? Carbon::createFromFormat('Y-m-d', $user->birthday)->format('m/d/Y') : '',
                        !empty($user->birthday && $user->birthday != '0000-00-00') ? Carbon::createFromFormat('Y-m-d', $user->birthday)->age : '',
                        $user->mothers_maiden_name,
                        !empty($user->EducationalAttainment->title) ? $user->EducationalAttainment->title : '',
                        $user->icoe_contact_name,
                        $user->icoe_contact_mobile,
                        $user->icoe_contact_address,
                        $user->membership_category,
                        $user->company_code,
                    );

                    if((!empty($request->category) && $request->category == 'Employee') || empty($request->category)) {

                        $department = [];
                        if(!empty($user->EmploymentDetails->Outlet->outlet)) $department[] = $user->EmploymentDetails->Outlet->outlet;
                        if(!empty($user->EmploymentDetails->Mall->mall)) $department[] = $user->EmploymentDetails->Mall->mall;
                        if(!empty($user->EmploymentDetails->Area->area)) $department[] = $user->EmploymentDetails->Area->area;
                        if(!empty($user->EmploymentDetails->Province->province)) $department[] = $user->EmploymentDetails->Province->province;
                        $department = implode(' - ', $department);

                        $monthlyRate = 0;

//                        if($user->Compensation->rate_type == 'Monthly') {
//                            $monthlyRate = number_format($user->Compensation->monthly_rate, 2);
//                        } else {
//                            if($user->Compensation->daily_category == 'Fixed') {
//                                $monthlyRate = number_format($user->Compensation->daily_rate * 26, 2);
//                            } else {
//                                $monthlyRate = !empty($user->Compensation->daily_rate_id) ? number_format($user->Compensation->DailyRate->rate * 26, 2) : 0;
//                            }
//                        }

                        $row[] = Carbon::createFromFormat('Y-m-d', $user->hire_date)->format('m/d/Y');
                        $row[] = !empty($user->employmentdetails->position->title) ? $user->employmentdetails->position->title : '';
                        $row[] = $department;
                        $row[] = !empty($user->EmploymentDetails->Coordinator->first_name) ? "{$user->EmploymentDetails->Coordinator->first_name} {$user->EmploymentDetails->Coordinator->last_name}" : '';
                        $row[] = !empty($user->Compensation->sss) ? $user->Compensation->sss : '';
                        $row[] = !empty($user->Compensation->hdmf) ? $user->Compensation->hdmf : '';
                        $row[] = !empty($user->Compensation->philhealth) ? $user->Compensation->philhealth : '';
                        $row[] = !empty($user->Compensation->tin) ? $user->Compensation->tin : '';
                        $row[] = !empty($user->Compensation->PayrollGroup->group_name) ? $user->Compensation->PayrollGroup->group_name : '';
                        $row[] = !empty($user->Compensation->TransferMode->transfer_mode) ? $user->Compensation->TransferMode->transfer_mode : '';
                        $row[] = !empty($user->Compensation->TransferMode->transfer_type) ? $user->Compensation->TransferMode->transfer_type : '';
                        $row[] = !empty($user->Compensation->TransferMode->bank) ? $user->Compensation->TransferMode->bank : '';
                        $row[] = $user->Compensation->mobile_number;
                        $row[] = $user->Compensation->account_number;
                        $row[] = !empty($user->Compensation->tax_type_id) ? $user->Compensation->TaxType->title : '';
                        $row[] = ($user->Compensation->deduct_withholding == 'N') ? 'Yes' : 'No';
                        $row[] = $user->Compensation->rate_type;
                        $row[] = $user->Compensation->daily_category;
                        $row[] = !empty($user->Compensation->daily_rate_id) ? $user->Compensation->DailyRate->location : '';
                        $row[] = $user->Compensation->monthly_rate;
                        $row[] = $user->Compensation->daily_rate;
                        $row[] = $user->hourly;
                        $row[] = $user->employment_status;
                    }

                    $row[] = $user->membership_type;
                    $row[] = $user->cost_center;
                    $row[] = Carbon::createFromFormat('Y-m-d', $user->join_date)->format('m/d/Y');
                    $row[] = $user->coop_share;
                    $row[] = $user->status;



                    $sheet->row($x, $row);

                    $x++;
                }

                $sheet->setColumnFormat(array(
                    'AF' => '@',
                    'AG' => '@',
                    'AA' => '@',
                    'Z' => '@',
                    'Y' => '@',
                    'X' => '@',
                    'P' => '@',
                    'H' => '@'
                ));

//                $sheet->setColumnFormat(array(
//                    'F2:G'.$x => '#,##0.00'
//                ));

            });
        })->download('xls');
    }

    public function coop_members_list()
    {
        $data['pageHeader'] = 'Coop Members List';
        return view('admin.reports.user_profiles.coop_members_list', $data);
    }

    public function employee_members_list()
    {
        $data['pageHeader'] = 'Employee Members List';
        return view('admin.reports.user_profiles.employee_members_list', $data);
    }

    public function leave_credits(Request $request)
    {
        $data['pageHeader'] = 'Leave Credits';

        $data['companies'] = Companies::where('status', 'Active')->with('PayrollGroups')->with('CostCenters')->orderBy('company_code')->get();

//        $users = User::select()

        $users = Profiles::selectRaw('profiles.*')
            ->orderBy('profiles.last_name');

        $users->selectRaw('ed.hire_date, cp.company_code, ed.status as employment_status')
            ->leftJoin('employment_details as ed', 'ed.user_id', 'profiles.user_id')
            ->leftJoin('compensations as c', 'c.user_id', 'profiles.user_id')
            ->leftJoin('companies as cp', 'cp.id', 'ed.principal_id');

        $users->where('profiles.membership_category', 'Employee');

//        if(!empty($request->search)) {
//            $users->whereRaw("(profiles.id_number LIKE '%{$request->search}% OR CONCAT(profiles.first_name, ' ', profiles.last_name) LIKE '%{$request->search}%')");
//        }

        if(!empty($request->search)) {
            $users->whereRaw("(profiles.first_name LIKE '%{$request->search}%' OR profiles.last_name LIKE '%{$request->search}%' OR profiles.id_number LIKE '%{$request->search}%')");
        }

        $users->where('ed.status', 'Active');

        if(!empty($request->company_id)) {
            $users->where('ed.principal_id', $request->company_id);

            if($request->payroll_group != 'All') {
                $users->where('pg.id', $request->payroll_group);
            }
        }



        $data['users'] = $users->get();

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();


        return view('admin.reports.user_profiles.leave_credits', $data);
    }

    public function leave_credits_download(Request $request)
    {
        $users = Profiles::selectRaw('profiles.*, cc.cost_center')
            ->leftJoin('cost_centers as cc', 'cc.id', 'profiles.cost_center_id')
            ->orderBy('profiles.last_name');

        $users->selectRaw('ed.hire_date, cp.company_code, ed.status as employment_status')
            ->leftJoin('employment_details as ed', 'ed.user_id', 'profiles.user_id')
            ->leftJoin('compensations as c', 'c.user_id', 'profiles.user_id')
            ->leftJoin('companies as cp', 'cp.id', 'ed.principal_id');

        $users->where('profiles.membership_category', 'Employee');

//        if(!empty($request->search)) {
//            $users->whereRaw("(profiles.id_number LIKE '%{$request->search}% OR CONCAT(profiles.first_name, ' ', profiles.last_name) LIKE '%{$request->search}%'')");
//        }

        if(!empty($request->search)) {
            $users->whereRaw("(profiles.first_name LIKE '%{$request->search}%' OR profiles.last_name LIKE '%{$request->search}%' OR profiles.id_number LIKE '%{$request->search}%')");
        }

        $users->where('ed.status', 'Active');

        if(!empty($request->company_id)) {
            $users->where('ed.principal_id', $request->company_id);

            if($request->payroll_group != 'All') {
                $users->where('pg.id', $request->payroll_group);
            }
        }

        $users = $users->get();


        $title = 'Leave Credits';

        Excel::create($title, function ($excel) use ($users, $title, $request) {


            // Set the title
            $excel->setTitle($title);

            $excel->sheet('Leave Credits', function ($sheet) use ($users, $request) {

                $headers = [
                    'Principal',
                    'Payroll Group',
                    'ID Number',
                    'Complete Name',
                    'Hire Date',
                    'YTD Leave Credits (Days)',
                    'YTD Accumulated Leave (Days)',
                    'YTD Leave Balance (Days)',
                    'YTD Leave Credits (Hrs)',
                    'YTD Accumulated Leave (Hrs)',
                    'YTD Leave Balance (Hrs)'
                ];

                // Sheet manipulation
                $sheet->row(1, $headers);


                $x = 2;
                foreach($users as $user) {

                    $leaveHistory = \App\LeaveHistory::select(['leave_history.*', 'payroll_cycles.payroll_date'])->where('leave_history.user_id', $user->user_id)->where('effective_year', date('Y'))->join('payroll_cycles', 'payroll_cycles.id', '=', 'leave_history.payroll_cycle_id')->orderByDesc('payroll_cycles.payroll_date')->get();

                    $row = array(
                        $user->company_code,
                        $user->Compensation->PayrollGroup->group_name,
                        $user->id_number,
                        $user->namelfm(),
                        !empty($user->hire_date && $user->hire_date != '0000-00-00') ? Carbon::createFromFormat('Y-m-d', $user->hire_date)->format('m/d/Y') : '',
                        (Carbon::createFromFormat('Y-m-d', $user->employmentdetails->hire_date)->age > 0) ? 5 : 0,
                        $leaveHistory->where('status', 'Paid')->sum('days'),
                        (Carbon::createFromFormat('Y-m-d', $user->employmentdetails->hire_date)->age > 0 && $leaveHistory->where('status', 'Paid')->sum('days') < 5) ? 5 - $leaveHistory->where('status', 'Paid')->sum('days') : 0,
                        (Carbon::createFromFormat('Y-m-d', $user->employmentdetails->hire_date)->age > 0) ? 5 * 8 : 0,
                        $leaveHistory->where('status', 'Paid')->sum('hours'),
                        (Carbon::createFromFormat('Y-m-d', $user->employmentdetails->hire_date)->age > 0 && $leaveHistory->where('status', 'Paid')->sum('days') < 5) ? (5 * 8) - $leaveHistory->where('status', 'Paid')->sum('hours') : 0
                    );


                    $sheet->row($x, $row);

                    $x++;
                }

                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                    'D' => '@'
                ));

//                $sheet->setColumnFormat(array(
//                    'F2:G'.$x => '#,##0.00'
//                ));

            });
        })->download('xls');
    }

    public function sss_contribution()
    {
        $data['pageHeader'] = 'SSS EE_ER Contribution';
        return view('admin.reports.user_profiles.sss_contribution', $data);
    }

    public function hdmf_contribution()
    {
        $data['pageHeader'] = 'HDMF EE_ER Contribution';
        return view('admin.reports.user_profiles.hdmf_contribution', $data);
    }
}

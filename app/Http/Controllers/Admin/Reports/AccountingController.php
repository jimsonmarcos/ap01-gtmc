<?php

namespace App\Http\Controllers\Admin\Reports;

use App\CashFlows;
use App\CashReceipts;
use App\CheckRequests;
use App\Invoices;
use App\JournalVouchers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class AccountingController extends Controller
{
    //
    public function cash_flow(Request $request)
    {
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $data['date_from'] = $request->date_from;
            $data['date_to'] = $request->date_to;
            $cashFlows = CashFlows::orderBy('entry_date');


            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $cashFlows->whereRaw("(cash_flows.entry_date >= '{$dateFrom}' AND cash_flows.entry_date <= '{$dateTo}')");
            }

            if(!empty($request->search)) {
                $cashFlows->whereRaw("(cash_flows.check_number LIKE '%{$request->search}%' OR cash_flows.reference_number LIKE '%{$request->search}%')");
            }

            if(!empty($request->bank)) {
                $cashFlows->where("bank", $request->bank);
            }

            $data['cashFlows'] = $cashFlows->get();

            $data['sBank'] = $request->bank;

        }

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['nav'] = 'cash-flow';


        $data['pageHeader'] = 'Cash Flow';
        return view('admin.reports.accounting.cash_flow', $data);
    }

    public function cash_flow_download(Request $request)
    {
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $data['date_from'] = $request->date_from;
            $data['date_to'] = $request->date_to;
            $cashFlows = CashFlows::orderBy('entry_date');


            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $cashFlows->whereRaw("(cash_flows.entry_date >= '{$dateFrom}' AND cash_flows.entry_date <= '{$dateTo}')");
            }

            if(!empty($request->search)) {
                $cashFlows->whereRaw("(cash_flows.check_number LIKE '%{$request->search}%' OR cash_flows.reference_number LIKE '%{$request->search}%')");
            }

            if(!empty($request->bank)) {
                $cashFlows->where("bank", $request->bank);
            }



            $cashFlows = $cashFlows->get();

            $data['sBank'] = $request->bank;


            $title = 'Cash Flow';

            Excel::create($title, function ($excel) use ($cashFlows, $title, $request) {


                // Set the title
                $excel->setTitle($title);

                $excel->sheet('Cash Flow', function ($sheet) use ($cashFlows, $request) {

                    $headers = [
                        'Bank Account',
                        'Status',
                        'Date',
                        'Mode',
                        'Check Number',
                        'Check Date',
                        'Payee Type',
                        'Payee Name',
                        'Reference Number',
                        'Particulars',
                        'Total Amount',
                        'Bank Transaction ID',
                        'CV Number',
                        'Remarks'
                    ];

                    // Sheet manipulation
                    $sheet->row(1, $headers);


                    $x = 2;
                    foreach($cashFlows as $cashFlow) {

                        $row = array(
                            $cashFlow->bank,
                            $cashFlow->status,
                            date('m/d/Y', strtotime($cashFlow->entry_date)),
                            $cashFlow->mode,
                            $cashFlow->check_number,
                            !empty($cashFlow->check_date) ? date('m/d/Y', strtotime($cashFlow->check_date)) : '',
                            $cashFlow->payee_type,
                            $cashFlow->payee_name,
                            $cashFlow->reference_number,
                            $cashFlow->particulars,
                            $cashFlow->Items->sum('amount'),
                            $cashFlow->bank_transaction_id,
                            $cashFlow->cv_number,
                            $cashFlow->remarks
                        );


                        $sheet->row($x, $row);

                        $x++;
                    }

                    $sheet->setColumnFormat(array(
//                    'A' => '@',
//                    'B' => '@',
//                    'C' => '@',
//                    'K' => '#,##0.00'
                    ));

                $sheet->setColumnFormat(array(
                    'K2:K'.$x => '#,##0.00'
                ));

                });
            })->download('xls');
        }


    }

    public function sales_book(Request $request)
    {
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $data['date_from'] = $request->date_from;
            $data['date_to'] = $request->date_to;
            $salesBook = Invoices::where('invoices.status', 'Paid')->orderBy('invoice_date');


            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $salesBook->whereRaw("(invoices.invoice_date >= '{$dateFrom}' AND invoices.invoice_date <= '{$dateTo}')");
            }

            if(!empty($request->search)) {
                $salesBook->whereRaw("(invoices.invoice_number LIKE '%{$request->client}%' OR invoices.company_name LIKE '%{$request->search}%')");
            }

            $salesBook->join('cash_flows as cf', 'cf.reference_number', '=', 'invoices.invoice_number', 'INNER');

            if(!empty($request->principal)) {
                $salesBook->where("company_id", $request->principal);
            }

            $salesBook->selectRaw("invoices.*, cf.entry_date, cf.id as cash_flow_id");

            $data['salesBook'] = $salesBook->get();

            $data['p'] = $request->principal;

        }

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['nav'] = 'sales-book';


        $data['pageHeader'] = 'Sales Book';
        return view('admin.reports.accounting.sales_book', $data);
    }

    public function schedule(Request $request)
    {
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $data['date_from'] = $request->date_from;
            $data['date_to'] = $request->date_to;
            $cashFlows = CashFlows::orderBy('entry_date');


            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $cashFlows->whereRaw("(cash_flows.entry_date >= '{$dateFrom}' AND cash_flows.entry_date <= '{$dateTo}')");
                $cashFlows->whereIn('status', ['Deposit', 'Withdrawal']);
            }

            if(!empty($request->search)) {
                $cashFlows->whereRaw("(cash_flows.check_number LIKE '%{$request->search}%' OR cash_flows.reference_number LIKE '%{$request->search}%')");
            }

            if(!empty($request->bank)) {
                $cashFlows->where("bank", $request->bank);
            }

            $data['cashFlows'] = $cashFlows->get();

            $data['sBank'] = $request->bank;

        }

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['nav'] = 'schedule';


        $data['pageHeader'] = 'Schedule';
        return view('admin.reports.accounting.schedule', $data);
    }

    public function cash_collection(Request $request)
    {
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $data['date_from'] = $request->date_from;
            $data['date_to'] = $request->date_to;
            $cashFlows = CashFlows::orderBy('entry_date');

            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $cashFlows->whereRaw("(cash_flows.entry_date >= '{$dateFrom}' AND cash_flows.entry_date <= '{$dateTo}')");
                $cashFlows->where('source', 'Cash Receipt');
                $cashFlows->whereIn('status', ['Deposit', 'Withdrawal']);
            }

            $data['cashFlows'] = $cashFlows->get();
        }

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['nav'] = 'cash-collection';


        $data['pageHeader'] = 'Cash Collection';
        return view('admin.reports.accounting.cash_collection', $data);
    }

    public function cash_collection_download(Request $request)
    {
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $data['date_from'] = $request->date_from;
            $data['date_to'] = $request->date_to;
            $cashFlows = CashFlows::orderBy('entry_date');

            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $cashFlows->whereRaw("(cash_flows.entry_date >= '{$dateFrom}' AND cash_flows.entry_date <= '{$dateTo}')");
                $cashFlows->where('source', 'Cash Receipt');
                $cashFlows->whereIn('status', ['Deposit', 'Withdrawal']);
            }



            $cashFlows = $cashFlows->get();


            $title = 'Cash Collection';

            Excel::create($title, function ($excel) use ($cashFlows, $title, $request) {


                // Set the title
                $excel->setTitle($title);

                $excel->sheet('Cash Collection', function ($sheet) use ($cashFlows, $request) {

                    $headers = [
                        'PAYOR',
                        'OR DATE',
                        'OR NUMBER',
                        'REFERENCE NUMBER',
                        'BANK',
                        'ENTRY DATE'
                    ];

                    $debitColumns = [];
                    foreach($cashFlows as $cashFlow) {
                        foreach($cashFlow->Entries->where('txn', 'Debit') as $item) {
                            if(!in_array($item->account_title, $debitColumns)) {
                                $debitColumns[] = $item->account_title;
                                $headers[] = $item->account_title;
                            }
                        }
                    }

                    $creditColumns = [];
                    foreach($cashFlows as $cashFlow) {
                        foreach($cashFlow->Entries->where('txn', 'Credit') as $item) {
                            if(!in_array($item->account_title, $creditColumns)) {
                                $creditColumns[] = $item->account_title;
                                $headers[] = $item->account_title;
                            }
                        }
                    }

                    $headers[] = 'TOTAL DEBIT';
                    $headers[] = 'TOTAL CREDIT';
                    $headers[] = 'GRAND TOTAL';


                    // Sheet manipulation
                    $sheet->row(1, $headers);


                    $x = 2;
                    foreach($cashFlows as $cashFlow) {
                        if($cashFlow->source == 'Check Request') {
                            $source = CheckRequests::find($cashFlow->source_id);
                        }

                        if($cashFlow->source == 'Cash Receipt') {
                            $source = CashReceipts::find($cashFlow->source_id);
                        }

                        $row = array(
                            $source->payor_name,
                            date('m/d/Y', strtotime($source->or_date)),
                            $source->or_number,
                            $source->reference_number,
                            $cashFlow->bank,
                            date('m/d/Y', strtotime($cashFlow->entry_date))
                        );

                        $customColumns = [];

                        foreach($cashFlow->Entries->where('txn', 'Debit') as $item) {
                            $customColumns[$item->account_title] = $item->debit;
                        }

                        foreach($debitColumns as $debitColumn) {
                            $row[] = !empty($customColumns[$debitColumn]) ? $customColumns[$debitColumn] : 0;
                        }

                        $customColumns = [];

                        foreach($cashFlow->Entries->where('txn', 'Credit') as $item) {
                            $customColumns[$item->account_title] = $item->credit;
                        }

                        foreach($creditColumns as $creditColumn) {
                            $row[] = !empty($customColumns[$creditColumn]) ? $customColumns[$creditColumn] : 0;
                        }

                        $row[] = $cashFlow->Entries->where('txn', 'Debit')->sum('debit');
                        $row[] = $cashFlow->Entries->where('txn', 'Credit')->sum('credit');
                        $row[] = $cashFlow->Entries->where('txn', 'Debit')->sum('debit') - $cashFlow->Entries->where('txn', 'Credit')->sum('credit');



                        $sheet->row($x, $row);

                        $x++;
                    }

                    $sheet->setColumnFormat(array(
//                    'A' => '@',
//                    'B' => '@',
//                    'C' => '@',
//                    'K' => '#,##0.00'
                    ));

//                    $sheet->setColumnFormat(array(
//                        'I2:K'.$x => '#,##0.00'
//                    ));

                });
            })->download('xls');
        }
    }

    public function cash_disbursement(Request $request)
    {
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $data['date_from'] = $request->date_from;
            $data['date_to'] = $request->date_to;
            $cashFlows = CashFlows::orderBy('entry_date');

            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $cashFlows->whereRaw("(cash_flows.entry_date >= '{$dateFrom}' AND cash_flows.entry_date <= '{$dateTo}')");
//                $cashFlows->where('source', 'Cash Receipt');
                $cashFlows->whereIn('status', ['Deposit', 'Withdrawal']);
            }

            $data['cashFlows'] = $cashFlows->get();
        }

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['nav'] = 'cash-disbursement';


        $data['pageHeader'] = 'Cash Disbursement';
        return view('admin.reports.accounting.cash_disbursement', $data);
    }

    public function cash_disbursement_download(Request $request)
    {
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $data['date_from'] = $request->date_from;
            $data['date_to'] = $request->date_to;
            $cashFlows = CashFlows::orderBy('entry_date');

            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $cashFlows->whereRaw("(cash_flows.entry_date >= '{$dateFrom}' AND cash_flows.entry_date <= '{$dateTo}')");
                $cashFlows->whereIn('status', ['Deposit', 'Withdrawal']);
            }



            $cashFlows = $cashFlows->with('Entries')->get();


            $title = 'Cash Disbursement';

            Excel::create($title, function ($excel) use ($cashFlows, $title, $request) {


                // Set the title
                $excel->setTitle($title);

                $excel->sheet('Cash Collection', function ($sheet) use ($cashFlows, $request) {

                    $headers = [
                        'PAYEE',
                        'ENTRY DATE',
                        'CV NUMBER',
                        'CHECK NUMBER'
                    ];

                    $debitColumns = [];
                    foreach($cashFlows as $cashFlow) {
                        foreach($cashFlow->Entries->where('txn', 'Debit') as $item) {
                            if(!in_array($item->account_title, $debitColumns)) {
                                $debitColumns[] = $item->account_title;
                                $headers[] = $item->account_title;
                            }
                        }
                    }

                    $creditColumns = [];
                    foreach($cashFlows as $cashFlow) {
                        foreach($cashFlow->Entries->where('txn', 'Credit') as $item) {
                            if(!in_array($item->account_title, $creditColumns)) {
                                $creditColumns[] = $item->account_title;
                                $headers[] = $item->account_title;
                            }
                        }
                    }

                    $headers[] = 'TOTAL DEBIT';
                    $headers[] = 'TOTAL CREDIT';
                    $headers[] = 'GRAND TOTAL';


                    // Sheet manipulation
                    $sheet->row(1, $headers);


                    $x = 2;
                    foreach($cashFlows as $cashFlow) {
                        $row = array(
                            $cashFlow->payee_name,
                            date('m/d/Y', strtotime($cashFlow->entry_date)),
                            $cashFlow->cv_number,
                            $cashFlow->check_number
                        );

                        $customColumns = [];

                        foreach($cashFlow->Entries->where('txn', 'Debit') as $item) {
                            $customColumns[$item->account_title] = $item->debit;
                        }

                        foreach($debitColumns as $debitColumn) {
                            $row[] = !empty($customColumns[$debitColumn]) ? $customColumns[$debitColumn] : 0;
                        }

                        $customColumns = [];

                        foreach($cashFlow->Entries->where('txn', 'Credit') as $item) {
                            $customColumns[$item->account_title] = $item->credit;
                        }

                        foreach($creditColumns as $creditColumn) {
                            $row[] = !empty($customColumns[$creditColumn]) ? $customColumns[$creditColumn] : 0;
                        }



                        $row[] = $cashFlow->Entries->where('txn', 'Debit')->sum('debit');
                        $row[] = $cashFlow->Entries->where('txn', 'Credit')->sum('credit');
                        $row[] = $cashFlow->Entries->where('txn', 'Debit')->sum('debit') - $cashFlow->Entries->where('txn', 'Credit')->sum('credit');



                        $sheet->row($x, $row);

                        $x++;
                    }

                    $sheet->setColumnFormat(array(
//                    'A' => '@',
//                    'B' => '@',
//                    'C' => '@',
//                        'K' => '#,##0.00'
                    ));

//                    $sheet->setColumnFormat(array(
//                        'G2:I'.$x => '#,##0.00'
//                    ));

                });
            })->download('xls');
        }
    }

    public function journal_voucher(Request $request)
    {
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $data['date_from'] = $request->date_from;
            $data['date_to'] = $request->date_to;
            $journalVouchers = JournalVouchers::orderBy('date');

            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $journalVouchers->whereRaw("(`date` >= '{$dateFrom}' AND `date` <= '{$dateTo}')");
            }

            $data['journalVouchers'] = $journalVouchers->get();
        }

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['nav'] = 'journal-voucher';


        $data['pageHeader'] = 'Journal Voucher';
        return view('admin.reports.accounting.journal_voucher', $data);
    }

    public function journal_voucher_download(Request $request)
    {
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $data['date_from'] = $request->date_from;
            $data['date_to'] = $request->date_to;
            $journalVouchers = JournalVouchers::orderBy('date');

            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $journalVouchers->whereRaw("(`date` >= '{$dateFrom}' AND `date` <= '{$dateTo}')");
            }

//            $journalVouchers->join('journal_voucher_items', 'journal_voucher_items.journal_voucher_id', '=', 'journal_vouchers.id');


            $journalVouchers = $journalVouchers->get();


            $title = 'Journal Voucher';

            Excel::create($title, function ($excel) use ($journalVouchers, $title, $request) {


                // Set the title
                $excel->setTitle($title);

                $excel->sheet('Cash Collection', function ($sheet) use ($journalVouchers, $request) {

                    $headers = [
                        'PARTICULARS',
                        'DATE',
                        'JV NUMBER',
                    ];

                    $debitColumns = [];
                    foreach($journalVouchers as $journalVoucher) {
                        foreach($journalVoucher->Items->where('txn', 'Debit') as $item) {
                            if(!in_array($item->account_title, $debitColumns)) {
                                $debitColumns[] = $item->account_title;
                                $headers[] = $item->account_title;
                            }

                        }
                    }

                    $creditColumns = [];
                    foreach($journalVouchers as $journalVoucher) {
                        foreach($journalVoucher->Items->where('txn', 'Credit') as $item) {
                            if(!in_array($item->account_title, $creditColumns)) {
                                $creditColumns[] = $item->account_title;
                                $headers[] = $item->account_title;
                            }
                        }
                    }


                    $headers[] = 'TOTAL DEBIT';
                    $headers[] = 'TOTAL CREDIT';
                    $headers[] = 'GRAND TOTAL';

//                    dd($headers);

                    // Sheet manipulation
                    $sheet->row(1, $headers);


                    $x = 2;
                    foreach($journalVouchers as $journalVoucher) {
                        $row = array(
                            $journalVoucher->particulars,
                            date('m/d/Y', strtotime($journalVoucher->date)),
                            $journalVoucher->jv_number
                        );

                        $customColumns = [];

                        foreach($journalVoucher->Items->where('txn', 'Debit') as $item) {
                            $customColumns[$item->account_title] = $item->debit;
                        }

                        foreach($debitColumns as $debitColumn) {
                            $row[] = !empty($customColumns[$debitColumn]) ? $customColumns[$debitColumn] : 0;
                        }

                        $customColumns = [];

                        foreach($journalVoucher->Items->where('txn', 'Credit') as $item) {
                            $customColumns[$item->account_title] = $item->credit;
                        }

                        foreach($creditColumns as $creditColumn) {
                            $row[] = !empty($customColumns[$creditColumn]) ? $customColumns[$creditColumn] : 0;
                        }




                        $row[] = $journalVoucher->Items->where('txn', 'Debit')->sum('debit');
                        $row[] = $journalVoucher->Items->where('txn', 'Credit')->sum('credit');
                        $row[] = $journalVoucher->Items->where('txn', 'Debit')->sum('debit') - $journalVoucher->Items->where('txn', 'Credit')->sum('credit');


                        $sheet->row($x, $row);

                        $x++;
                    }

                    $sheet->setColumnFormat(array(
//                    'A' => '@',
//                    'B' => '@',
//                    'C' => '@',
//                        'K' => '#,##0.00'
                    ));

//                    $sheet->setColumnFormat(array(
//                        'G2:I'.$x => '#,##0.00'
//                    ));

                });
            })->download('xls');
        }
    }

    public function trial_balance(Request $request)
    {
//        if(!empty($request->date_from) && !empty($request->date_to)) {
//            $data['date_from'] = $request->date_from;
//            $data['date_to'] = $request->date_to;
//            $cashFlows = CashFlows::orderBy('entry_date');
//
//
//            if(!empty($request->date_from) && !empty($request->date_to)) {
//                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
//                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
//                $cashFlows->whereRaw("(cash_flows.entry_date >= '{$dateFrom}' AND cash_flows.entry_date <= '{$dateTo}')");
//            }
//
//            if(!empty($request->search)) {
//                $cashFlows->whereRaw("(cash_flows.check_number LIKE '%{$request->search}%' OR cash_flows.reference_number LIKE '%{$request->search}%')");
//            }
//
//            if(!empty($request->bank)) {
//                $cashFlows->where("bank", $request->bank);
//            }
//
//            $data['cashFlows'] = $cashFlows->get();
//
//            $data['sBank'] = $request->bank;
//
//        }

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['nav'] = 'trial-balance';


        $data['pageHeader'] = 'Trial Balance';
        return view('admin.reports.accounting.trial_balance', $data);
    }

    public function balance_sheet(Request $request)
    {
//        if(!empty($request->date_from) && !empty($request->date_to)) {
//            $data['date_from'] = $request->date_from;
//            $data['date_to'] = $request->date_to;
//            $cashFlows = CashFlows::orderBy('entry_date');
//
//
//            if(!empty($request->date_from) && !empty($request->date_to)) {
//                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
//                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
//                $cashFlows->whereRaw("(cash_flows.entry_date >= '{$dateFrom}' AND cash_flows.entry_date <= '{$dateTo}')");
//            }
//
//            if(!empty($request->search)) {
//                $cashFlows->whereRaw("(cash_flows.check_number LIKE '%{$request->search}%' OR cash_flows.reference_number LIKE '%{$request->search}%')");
//            }
//
//            if(!empty($request->bank)) {
//                $cashFlows->where("bank", $request->bank);
//            }
//
//            $data['cashFlows'] = $cashFlows->get();
//
//            $data['sBank'] = $request->bank;
//
//        }

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['nav'] = 'balance_sheet';


        $data['pageHeader'] = 'Balance Sheet';
        return view('admin.reports.accounting.balance_sheet', $data);
    }

    public function income_statement(Request $request)
    {
//        if(!empty($request->date_from) && !empty($request->date_to)) {
//            $data['date_from'] = $request->date_from;
//            $data['date_to'] = $request->date_to;
//            $cashFlows = CashFlows::orderBy('entry_date');
//
//
//            if(!empty($request->date_from) && !empty($request->date_to)) {
//                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
//                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
//                $cashFlows->whereRaw("(cash_flows.entry_date >= '{$dateFrom}' AND cash_flows.entry_date <= '{$dateTo}')");
//            }
//
//            if(!empty($request->search)) {
//                $cashFlows->whereRaw("(cash_flows.check_number LIKE '%{$request->search}%' OR cash_flows.reference_number LIKE '%{$request->search}%')");
//            }
//
//            if(!empty($request->bank)) {
//                $cashFlows->where("bank", $request->bank);
//            }
//
//            $data['cashFlows'] = $cashFlows->get();
//
//            $data['sBank'] = $request->bank;
//
//        }

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['nav'] = 'income_sheet';


        $data['pageHeader'] = 'Income Statement';
        return view('admin.reports.accounting.income_statement', $data);
    }

    public function summary()
    {
        $data['pageHeader'] = 'Summary Report';
        return view('admin.reports.accounting.summary', $data);
    }

    public function financial_summary()
    {
        $data['pageHeader'] = 'Financial Summary';
        return view('admin.reports.accounting.financial_statement', $data);
    }
}

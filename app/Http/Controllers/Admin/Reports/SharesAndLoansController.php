<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Companies;
use App\Loans;
use App\Profiles;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class SharesAndLoansController extends Controller
{
    //
    public function gtmc_request_loan(Request $request)
    {
        $data['companies'] = Companies::with('PayrollGroups')->with('CostCenters')->orderBy('company_code')->get();

        if(!empty($request->date_request_from) && !empty($request->date_request_to)) {
            $loans = Loans::select(DB::raw('pg.group_name as payroll_group, loans.*, profiles.id_number, users.name, profiles.first_name, profiles.middle_name, profiles.last_name, cost_centers.cost_center, loan_types.type, loans.granted_loan_amount, loans.status, loans.releasing_status, companies.company_name, companies.company_code, 
            COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.total_overdue_amount, 0) as total_overdue_amount, 
            loans.granted_loan_amount - COALESCE(loan_payments.total_amount_paid, 0) as outstanding_balance'));

            $loans = $loans->join('users', 'users.id', '=', 'loans.user_id', 'LEFT')
                ->join('profiles', 'profiles.user_id', '=', 'users.id', 'LEFT')
                ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
                ->join('employment_details', 'employment_details.user_id', '=', 'profiles.user_id', 'LEFT')
                ->join('companies', 'companies.id', '=', 'employment_details.principal_id', 'LEFT')
                ->join('loan_types', 'loan_types.id', '=', 'loans.loan_type_id', 'LEFT')
                ->leftJoin('compensations as c', 'c.user_id', 'profiles.user_id')
                ->leftJoin('payroll_groups as pg', 'pg.id', 'c.payroll_group_id')
                ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')
                ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(amortization) + SUM(lapsed_interest) as total_overdue_amount FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payments.loan_id', '=', 'loans.id')
                ->groupBy('loans.id');


            if(!empty($request->category) && $request->category == 'Employee') {


                if(!empty($request->company_id)) {
                    $loans->where('employment_details.principal_id', $request->company_id);
                }
                if($request->payroll_group != 'All') {
                    $loans->where('pg.id', $request->payroll_group);
                }

                if(!empty($request->date_request_from) && !empty($request->date_request_to)) {
                    $dateRequestFrom = Carbon::createFromFormat("m/d/Y", $request->date_request_from)->format('Y-m-d');
                    $dateRequestTo = Carbon::createFromFormat("m/d/Y", $request->date_request_to)->format('Y-m-d');
                    $loans->whereRaw("(loans.date_request >= '{$dateRequestFrom}' AND loans.date_request <= '{$dateRequestTo}')");
                }
            };


            if(!empty($request->cost_center) && $request->cost_center != 'All') {
                $loans->where('cc.id', $request->cost_center);
            }

            if(!empty($request->category)) {
                $loans->where('profiles.membership_category', $request->category);
            }

            if(!empty($request->membership_status)) {
                $loans->where('profiles.status', $request->membership_status);
            }

            if(!empty($request->membership_type)) {
                $loans->where('profiles.membership_type', $request->membership_type);
            }

            if(!empty($request->search)) {
                $loans->whereRaw("(profiles.id_number LIKE '%{$request->search}%' OR CONCAT(profiles.first_name, ' ', profiles.last_name) LIKE '%{$request->search}%')");
            }

            $data['loans'] = $loans->get();

        }

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['pageHeader'] = 'Loans';
        return view('admin.reports.shares_loans.gtmc_request_loan', $data);
    }

    public function gtmc_request_loan_download(Request $request)
    {
        $data['companies'] = Companies::with('PayrollGroups')->with('CostCenters')->orderBy('company_code')->get();

        $loans = Loans::select(DB::raw('pg.group_name as payroll_group, loans.*, profiles.id_number, users.name, profiles.first_name, profiles.middle_name, profiles.last_name, cost_centers.cost_center, loan_types.type, loans.granted_loan_amount, loans.status, loans.releasing_status, companies.company_name, companies.company_code, COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.total_overdue_amount, 0) as total_overdue_amount, COALESCE(loan_payment_schedules.total_overdue_amount, 0) - COALESCE(loan_payments.total_amount_paid, 0) as outstanding_balance'));

        $loans = $loans->join('users', 'users.id', '=', 'loans.user_id', 'LEFT')
            ->join('profiles', 'profiles.user_id', '=', 'users.id', 'LEFT')
            ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
            ->join('employment_details', 'employment_details.user_id', '=', 'profiles.user_id', 'LEFT')
            ->join('companies', 'companies.id', '=', 'employment_details.principal_id', 'LEFT')
            ->join('loan_types', 'loan_types.id', '=', 'loans.loan_type_id', 'LEFT')
            ->leftJoin('compensations as c', 'c.user_id', 'profiles.user_id')
            ->leftJoin('payroll_groups as pg', 'pg.id', 'c.payroll_group_id')
            ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')
            ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(amortization) + SUM(lapsed_interest) as total_overdue_amount FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payments.loan_id', '=', 'loans.id')
            ->where('loans.is_voucher', 'N')
            ->groupBy('loans.id');


        if(!empty($request->category) && $request->category == 'Employee') {


            if(!empty($request->company_id)) {
                $loans->where('employment_details.principal_id', $request->company_id);
            }
            if($request->payroll_group != 'All') {
                $loans->where('pg.id', $request->payroll_group);
            }

            if(!empty($request->date_request_from) && !empty($request->date_request_to)) {
                $dateRequestFrom = Carbon::createFromFormat("m/d/Y", $request->date_request_from)->format('Y-m-d');
                $dateRequestTo = Carbon::createFromFormat("m/d/Y", $request->date_request_to)->format('Y-m-d');
                $loans->whereRaw("(loans.date_request >= '{$dateRequestFrom}' AND loans.date_request <= '{$dateRequestTo}')");
            }
        };


        if(!empty($request->cost_center) && $request->cost_center != 'All') {
            $loans->where('cc.id', $request->cost_center);
        }

        if(!empty($request->category)) {
            $loans->where('profiles.membership_category', $request->category);
        }

        if(!empty($request->membership_status)) {
            $loans->where('profiles.status', $request->membership_status);
        }

        if(!empty($request->membership_type)) {
            $loans->where('profiles.membership_type', $request->membership_type);
        }

        if(!empty($request->search)) {
            $loans->whereRaw("(profiles.id_number LIKE '%{$request->search}%' OR CONCAT(profiles.first_name, ' ', profiles.last_name) LIKE '%{$request->search}%')");
        }

        $loans = $loans->get();

        $title = 'Loans';

        Excel::create($title, function ($excel) use ($loans, $title, $request) {


            // Set the title
            $excel->setTitle($title);

            $excel->sheet('Loans', function ($sheet) use ($loans, $request) {

                $headers = [
                    'CLA Number',
                    'ID Number',
                    'Complete Name',
                    'Principal',
                    'Payroll Group',
                    'Cost Center',
                    'Loan Type',
                    'Date Request',
                    'Months to Pay',
                    'CV Number',
                    'CV Date',
                    'Cheque Number',
                    'Effective Year',
                    'Effective Month',
                    'Request Amount',
                    'Service Charge %',
                    'Service Charge',
                    'Savings %',
                    'Savings',
                    'Loan Interest %',
                    'Interest',
                    'Overall Interest',
                    'CLA Number Reference',
                    'Additional Deduction',
                    'Amortization',
                    'Granted Loan Amount',
                    'Disbursed Amount',
                    'Beginning Balance',
                    'Outstanding Balance',
                    'Status',
                    'Remarks'
                ];

                // Sheet manipulation
                $sheet->row(1, $headers);


                $x = 2;
                foreach($loans as $loan) {

                    if($loan->is_paid == 'Y') {
                        $status = 'Completed';
                    } else {
                        if($loan->total_amount_paid > 0) {
                            $status = 'Ongoing';
                        } else {
                            $status = 'Approved';
                        }
                    }

                    $row = array(
                        $loan->cla_number,
                        $loan->id_number,
                        "{$loan->last_name}, {$loan->first_name}",
                        $loan->company_code,
                        $loan->payroll_group,
                        $loan->cost_center,
                        $loan->type,
                        date('m/d/Y', strtotime($loan->date_request)),
                        $loan->months_to_pay,
                        $loan->cv_number,
                        $loan->cv_date,
                        $loan->cheque_number,
                        date('Y', strtotime($loan->effective_date)),
                        date('m', strtotime($loan->effective_date)),
                        $loan->request_amount,
                        $loan->service_charge_percent,
                        $loan->service_charge,
                        $loan->savings_percent,
                        $loan->savings,
                        $loan->interest_percent,
                        $loan->interest,
                        $loan->overall_interest,
                        $loan->cla_number_reference,
                        $loan->additional_deduction,
                        $loan->amortization,
                        $loan->granted_loan_amount,
                        $loan->disbursed_amount,
                        $loan->beginning_balance,
                        $loan->outstanding_balance,
                        $status,
                        $loan->remarks
                    );


                    $sheet->row($x, $row);

                    $x++;
                }

                $sheet->setColumnFormat(array(
//                    'A' => '@',
//                    'B' => '@',
//                    'C' => '@',
//                    'I' => '#,##0.00'
                ));

//                $sheet->setColumnFormat(array(
//                    'F2:G'.$x => '#,##0.00'
//                ));

            });
        })->download('xls');
    }

    public function sss_hdmf_loan()
    {
        $data['pageHeader'] = "SSS and HDMF Loan";
        return view('admin.reports.shares_loans.sss_hdmf_loan', $data);
    }

    public function account_statement()
    {
        $data['pageHeader'] = "Loan Payment";
        return view('admin.reports.shares_loans.account_statement', $data);
    }

    public function coop_share_contribution()
    {
        $data['pageHeader'] = 'Loan Payment';
        return view('admin.reports.shares_loans.coop_share_contribution', $data);
    }

    public function shares(Request $request)
    {
        $data['pageHeader'] = 'Shares';

        $data['companies'] = Companies::with('PayrollGroups')->with('CostCenters')->orderBy('company_code')->get();

//        $users = User::select()

        $users = Profiles::selectRaw('profiles.*, cc.cost_center')
            ->leftJoin('cost_centers as cc', 'cc.id', 'profiles.cost_center_id')
            ->orderBy('profiles.last_name');

        $users->selectRaw('ed.hire_date, pg.group_name as payroll_group, cp.company_code, ed.status as employment_status, COALESCE(op.total_amount_paid, 0) as total_shares')
            ->leftJoin('employment_details as ed', 'ed.user_id', 'profiles.user_id')
            ->leftJoin('compensations as c', 'c.user_id', 'profiles.user_id')
            ->leftJoin('payroll_groups as pg', 'pg.id', 'c.payroll_group_id')
            ->leftJoin('companies as cp', 'cp.id', 'ed.principal_id')
            ->leftJoin(DB::raw("(
                SELECT
                    user_id, SUM(total_amount_paid) as total_amount_paid
                FROM other_payments
                WHERE transaction_type = 'COOP SHARE'
                GROUP BY user_id
            ) op"), 'op.user_id', 'profiles.user_id', 'OUTER');

        if(!empty($request->category) && $request->category == 'Employee') {


            if($request->employment_status == 'Deactivated') {
                $users->where('ed.status', 'Deactivated');
            }



            if(!empty($request->company_id)) {
                $users->where('ed.principal_id', $request->company_id);
            }
            if($request->payroll_group != 'All') {
                $users->where('pg.id', $request->payroll_group);
            }

            if(!empty($request->join_date_from) && !empty($request->join_date_to)) {
                $joinDateFrom = Carbon::createFromFormat("m/d/Y", $request->join_date_from)->format('Y-m-d');
                $joinDateTo = Carbon::createFromFormat("m/d/Y", $request->join_date_to)->format('Y-m-d');
                $users->whereRaw("(profiles.join_date >= '{$joinDateFrom}' AND profiles.join_date <= '{$joinDateTo}')");
            }
        };


        if(!empty($request->cost_center) && $request->cost_center != 'All') {
            $users->where('cc.id', $request->cost_center);
        }

        if(!empty($request->category)) {
            $users->where('profiles.membership_category', $request->category);
        }

        if(!empty($request->membership_status)) {
            $users->where('profiles.status', $request->membership_status);
        }

        if(!empty($request->membership_type)) {
            $users->where('profiles.membership_type', $request->membership_type);
        }

        if(!empty($request->search)) {
            $users->whereRaw("(profiles.id_number LIKE '%{$request->search}%' OR CONCAT(profiles.first_name, ' ', profiles.last_name) LIKE '%{$request->search}%')");
        }

        $data['users'] = $users->get();

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();


        return view('admin.reports.shares_loans.shares', $data);
    }

    public function shares_download(Request $request)
    {
        $data['companies'] = Companies::with('PayrollGroups')->with('CostCenters')->orderBy('company_code')->get();

//        $users = User::select()

        $users = Profiles::selectRaw('profiles.*, cc.cost_center')
            ->leftJoin('cost_centers as cc', 'cc.id', 'profiles.cost_center_id')
            ->orderBy('profiles.last_name');

        $users->selectRaw('ed.hire_date, pg.group_name as payroll_group, cp.company_code, ed.status as employment_status, COALESCE(op.total_amount_paid, 0) as total_shares')
            ->leftJoin('employment_details as ed', 'ed.user_id', 'profiles.user_id')
            ->leftJoin('compensations as c', 'c.user_id', 'profiles.user_id')
            ->leftJoin('payroll_groups as pg', 'pg.id', 'c.payroll_group_id')
            ->leftJoin('companies as cp', 'cp.id', 'ed.principal_id')
            ->leftJoin(DB::raw("(
                SELECT
                    user_id, SUM(total_amount_paid) as total_amount_paid
                FROM other_payments
                WHERE transaction_type = 'COOP SHARE'
                GROUP BY user_id
            ) op"), 'op.user_id', 'profiles.user_id', 'OUTER');

        if(!empty($request->category) && $request->category == 'Employee') {


            if($request->employment_status == 'Deactivated') {
                $users->where('ed.status', 'Deactivated');
            }



            if(!empty($request->company_id)) {
                $users->where('ed.principal_id', $request->company_id);
            }
            if($request->payroll_group != 'All') {
                $users->where('pg.id', $request->payroll_group);
            }

            if(!empty($request->join_date_from) && !empty($request->join_date_to)) {
                $joinDateFrom = Carbon::createFromFormat("m/d/Y", $request->join_date_from)->format('Y-m-d');
                $joinDateTo = Carbon::createFromFormat("m/d/Y", $request->join_date_to)->format('Y-m-d');
                $users->whereRaw("(profiles.join_date >= '{$joinDateFrom}' AND profiles.join_date <= '{$joinDateTo}')");
            }
        };


        if(!empty($request->cost_center) && $request->cost_center != 'All') {
            $users->where('cc.id', $request->cost_center);
        }

        if(!empty($request->category)) {
            $users->where('profiles.membership_category', $request->category);
        }

        if(!empty($request->membership_status)) {
            $users->where('profiles.status', $request->membership_status);
        }

        if(!empty($request->membership_type)) {
            $users->where('profiles.membership_type', $request->membership_type);
        }

        if(!empty($request->search)) {
            $users->whereRaw("(profiles.id_number LIKE '%{$request->search}%' OR CONCAT(profiles.first_name, ' ', profiles.last_name) LIKE '%{$request->search}%')");
        }

        $users = $users->get();

        $title = 'Shares';

        Excel::create($title, function ($excel) use ($users, $title, $request) {


            // Set the title
            $excel->setTitle($title);

            $excel->sheet('Shares', function ($sheet) use ($users, $request) {

                $headers = [
                    'Company Code',
                    'Payroll Group',
                    'Cost Center',
                    'Membership Type',
                    'Category',
                    'GTMC ID Number',
                    'Complete Name',
                    'Join Date',
                    'Total Shares',
                    'Employment Status',
                    'Membership Status'
                ];

                // Sheet manipulation
                $sheet->row(1, $headers);


                $x = 2;
                foreach($users as $user) {

                    $row = array(
                        $user->company_code,
                        $user->payroll_group,
                        $user->cost_center,
                        $user->membership_type,
                        $user->membership_category,
                        $user->id_number,
                        $user->namelfm(),
                        date('m/d/Y', strtotime($user->join_date)),
                        $user->total_shares,
                        $user->EmploymentDetails->status,
                        $user->status
                    );


                    $sheet->row($x, $row);

                    $x++;
                }

                $sheet->setColumnFormat(array(
//                    'A' => '@',
//                    'B' => '@',
//                    'C' => '@',
                    'I' => '#,##0.00'
                ));

//                $sheet->setColumnFormat(array(
//                    'F2:G'.$x => '#,##0.00'
//                ));

            });
        })->download('xls');

    }

    public function payments(Request $request)
    {
        $data['companies'] = Companies::with('PayrollGroups')->with('CostCenters')->orderBy('company_code')->get();

        $where = [];

        if(!empty($request->payment_date_from) && !empty($request->payment_date_to)) {
            $paymentDateFrom = Carbon::createFromFormat("m/d/Y", $request->payment_date_from)->format('Y-m-d');
            $paymentDateTo = Carbon::createFromFormat("m/d/Y", $request->payment_date_to)->format('Y-m-d');
            $where[] = "(payment_date >= '{$paymentDateFrom}' AND payment_date <= '{$paymentDateTo}')";


            if(!empty($request->category) && $request->category == 'Employee') {

                if(!empty($request->company_id)) {
                    $where[] = "company_id = '{$request->company_id}'";
                }

                if($request->payroll_group != 'All') {
                    $where[] = "payroll_group_id = '{$request->payroll_group}'";
                }
            };


            if(!empty($request->cost_center) && $request->cost_center != 'All') {
                $where[] = "cost_center_id = '{$request->cost_center}'";
            }

            if(!empty($request->category)) {
                $where[] = "membership_category = '{$request->category}'";
            }

            if(!empty($request->membership_status)) {
                $where[] = "status = '{$request->membership_status}'";
            }

            if(!empty($request->membership_type)) {
                $where[] = "membership_type = '{$request->membership_type}'";
            }

            if(!empty($request->search)) {
                $where[] = "(profiles.id_number LIKE '%{$request->search}%' OR CONCAT(p.first_name, ' ', p.last_name) LIKE '%{$request->search}%')";
            }

            if(!empty($where)) {
                $where = "WHERE ". implode(" AND ", $where);
            }

            $payments = DB::select("SELECT * FROM ( SELECT
                                payment_date as `date`,
                                ar_number,
                                id_number,
                                CONCAT(last_name, ', ', first_name) as complete_name,
                                'LOAN' as transaction_type,
                                payment_date,
                                lp.payment_method,
                                total_amount_paid,
                                c.company_code,
                                c.id as company_id,
                                p.membership_type,
                                p.`status` as membership_status,
                                p.membership_category,
                                pg.group_name as payroll_group,
                                pg.id as payroll_group_id,
                                cc.cost_center,
                                cc.id as cost_center_id,
                                ed.`status` as employment_status
                            FROM
                                loan_payments lp
                            LEFT JOIN loans l
                                ON l.id = lp.loan_id
                            LEFT JOIN profiles p
                                ON p.user_id = l.user_id
                            LEFT JOIN employment_details ed
                                ON ed.user_id = p.user_id
                            LEFT JOIN companies as c
                                ON c.id = ed.principal_id
                            LEFT JOIN compensations cm
                                ON cm.user_id = p.user_id
                            LEFT JOIN payroll_groups pg
                                ON pg.id = cm.payroll_group_id
                            LEFT JOIN cost_centers cc
                                ON cc.id = p.cost_center_id
                                
                                
                            UNION
                            
                            SELECT
                                payment_date as `date`,
                                ar_number,
                                id_number,
                                CONCAT(last_name, ', ', first_name) as complete_name,
                                transaction_type,
                                payment_date,
                                lp.payment_method,
                                total_amount_paid,
                                c.company_code,
                                c.id as company_id,
                                p.membership_type,
                                p.`status` as membership_status,
                                p.membership_category,
                                pg.group_name as payroll_group,
                                pg.id as payroll_group_id,
                                cc.cost_center,
                                cc.id as cost_center_id,
                                ed.`status` as employment_status
                            FROM
                                other_payments lp
                            LEFT JOIN profiles p
                                ON p.user_id = lp.user_id
                            LEFT JOIN employment_details ed
                                ON ed.user_id = p.user_id
                            LEFT JOIN companies as c
                                ON c.id = ed.principal_id
                            LEFT JOIN compensations cm
                                ON cm.user_id = p.user_id
                            LEFT JOIN payroll_groups pg
                                ON pg.id = cm.payroll_group_id
                            LEFT JOIN cost_centers cc
                                ON cc.id = p.cost_center_id ) t1
                            {$where}
                            ORDER BY payment_date DESC
                            ");
            $data['payments'] = (object) $payments;
        }




        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['pageHeader'] = 'Payments';
        return view('admin.reports.shares_loans.payments', $data);

    }

    public function payments_download(Request $request)
    {
        $data['companies'] = Companies::with('PayrollGroups')->with('CostCenters')->orderBy('company_code')->get();

        $where = [];

        $paymentDateFrom = Carbon::createFromFormat("m/d/Y", $request->payment_date_from)->format('Y-m-d');
        $paymentDateTo = Carbon::createFromFormat("m/d/Y", $request->payment_date_to)->format('Y-m-d');
        $where[] = "(payment_date >= '{$paymentDateFrom}' AND payment_date <= '{$paymentDateTo}')";


        if(!empty($request->category) && $request->category == 'Employee') {

            if(!empty($request->company_id)) {
                $where[] = "company_id = '{$request->company_id}'";
            }

            if($request->payroll_group != 'All') {
                $where[] = "payroll_group_id = '{$request->payroll_group}'";
            }
        };


        if(!empty($request->cost_center) && $request->cost_center != 'All') {
            $where[] = "cost_center_id = '{$request->cost_center}'";
        }

        if(!empty($request->category)) {
            $where[] = "membership_category = '{$request->category}'";
        }

        if(!empty($request->membership_status)) {
            $where[] = "status = '{$request->membership_status}'";
        }

        if(!empty($request->membership_type)) {
            $where[] = "membership_type = '{$request->membership_type}'";
        }

        if(!empty($request->search)) {
            $where[] = "(profiles.id_number LIKE '%{$request->search}%' OR CONCAT(p.first_name, ' ', p.last_name) LIKE '%{$request->search}%')";
        }

        if(!empty($where)) {
            $where = "WHERE ". implode(" AND ", $where);
        }

        $payments = DB::select("SELECT * FROM ( SELECT
                            payment_date as `date`,
                            ar_number,
                            id_number,
                            CONCAT(last_name, ', ', first_name) as complete_name,
                            'LOAN' as transaction_type,
                            payment_date,
                            lp.payment_method,
                            pay_period,
                            lp.amount,
                            lp.interest,
                            lp.additional_payment,
                            total_amount_paid,
                            c.company_code,
                            c.id as company_id,
                            p.membership_type,
                            p.`status` as membership_status,
                            p.membership_category,
                            pg.group_name as payroll_group,
                            pg.id as payroll_group_id,
                            cc.cost_center,
                            cc.id as cost_center_id,
                            ed.`status` as employment_status,
                            lp.reference_number,
                            b.bank_account as bank
                        FROM
                            loan_payments lp
                        LEFT JOIN banks b
                            ON b.id = lp.bank_id
                        LEFT JOIN loans l
                            ON l.id = lp.loan_id
                        LEFT JOIN profiles p
                            ON p.user_id = l.user_id
                        LEFT JOIN employment_details ed
                            ON ed.user_id = p.user_id
                        LEFT JOIN companies as c
                            ON c.id = ed.principal_id
                        LEFT JOIN compensations cm
                            ON cm.user_id = p.user_id
                        LEFT JOIN payroll_groups pg
                            ON pg.id = cm.payroll_group_id
                        LEFT JOIN cost_centers cc
                            ON cc.id = p.cost_center_id
                            
                            
                        UNION
                        
                        SELECT
                            payment_date as `date`,
                            ar_number,
                            id_number,
                            CONCAT(last_name, ', ', first_name) as complete_name,
                            transaction_type,
                            payment_date,
                            lp.payment_method,
                            pay_period,
                            lp.amount,
                            lp.interest,
                            lp.additional_payment,
                            total_amount_paid,
                            c.company_code,
                            c.id as company_id,
                            p.membership_type,
                            p.`status` as membership_status,
                            p.membership_category,
                            pg.group_name as payroll_group,
                            pg.id as payroll_group_id,
                            cc.cost_center,
                            cc.id as cost_center_id,
                            ed.`status` as employment_status,
                            '' as reference_number,
                            b.bank_account as bank
                        FROM
                            other_payments lp
                        LEFT JOIN banks b
                            ON b.id = lp.bank_id
                        LEFT JOIN profiles p
                            ON p.user_id = lp.user_id
                        LEFT JOIN employment_details ed
                            ON ed.user_id = p.user_id
                        LEFT JOIN companies as c
                            ON c.id = ed.principal_id
                        LEFT JOIN compensations cm
                            ON cm.user_id = p.user_id
                        LEFT JOIN payroll_groups pg
                            ON pg.id = cm.payroll_group_id
                        LEFT JOIN cost_centers cc
                            ON cc.id = p.cost_center_id ) t1
                        {$where}
                        ORDER BY payment_date DESC
                        ");
        $payments = (object) $payments;

        $title = 'Payments';

        Excel::create($title, function ($excel) use ($payments, $title, $request) {


            // Set the title
            $excel->setTitle($title);

            $excel->sheet('Payments', function ($sheet) use ($payments, $request) {

                $headers = [
                    'Payment Date',
                    'AR Number',
                    'ID Number',
                    'Complete Name',
                    'Transaction Type',
                    'Payment Method',
                    'Pay Period',
                    'Amount',
                    'Interest',
                    'Additional Amount',
                    'Total Amount Paid',
                    'Bank Account',
                    'Reference Number',
                    'Remarks'
                ];

                // Sheet manipulation
                $sheet->row(1, $headers);


                $x = 2;
                foreach($payments as $payment) {

                    $row = array(
                        date('m/d/Y', strtotime($payment->payment_date)),
                        $payment->ar_number,
                        $payment->id_number,
                        $payment->complete_name,
                        $payment->transaction_type,
                        $payment->payment_method,
                        $payment->pay_period,
                        $payment->amount,
                        $payment->interest,
                        $payment->additional_amount,
                        $payment->total_amount_paid,
                        $payment->bank,
                        $payment->reference_number,
                        $payment->remarks,
                    );


                    $sheet->row($x, $row);

                    $x++;
                }

                $sheet->setColumnFormat(array(
//                    'A' => '@',
//                    'B' => '@',
//                    'C' => '@',
                    'I' => '#,##0.00'
                ));

//                $sheet->setColumnFormat(array(
//                    'F2:G'.$x => '#,##0.00'
//                ));

            });
        })->download('xls');

    }

    public function individual_shares()
    {
        $data['pageHeader'] = 'Individual Shares';
        return view('admin.reports.shares_loans.individual_shares', $data);
    }

    public function individual_loans()
    {
        $data['pageHeader'] = 'Individual Shares';
        return view('admin.reports.shares_loans.individual_loans', $data);
    }

}

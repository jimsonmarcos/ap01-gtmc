<?php

namespace App\Http\Controllers\Admin\Reports;

use App\PayrollSummary;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GovernmentReports extends Controller
{
    //
    public function sss_monthly_contribution(Request $request)
    {
        if(!empty($request->period_from)) {
            $periodFrom = Carbon::createFromFormat('F, Y', $request->period_from);
//            dd($periodFrom->format('Y-m-d'));



            if(!empty($request->period_to)) {
                $periodTo = Carbon::createFromFormat('F, Y', $request->period_to);
//                dd($periodTo->format('Y-m-d'));
                $where = "(DATE_FORMAT(pc.payroll_date, '%Y%m') >= '{$periodFrom->format("Ym")}' AND DATE_FORMAT(pc.payroll_date, '%Y%m') <= '{$periodTo->format("Ym")}')";
            } else {
                $where = "DATE_FORMAT(pc.payroll_date, '%Y%m') = '{$periodFrom->format("Ym")}'";
            }

//            dd($where);

            $data['employees'] = PayrollSummary::selectRaw("SUM(payroll_summary.sss_ee) as sss_ee,
                SUM(payroll_summary.sss_er) as sss_er,
                SUM(payroll_summary.sss_ec) as sss_ec,
                p.id_number,
                CONCAT(p.last_name, ', ', p.first_name) as name,
                c.sss")
                ->leftJoin('payroll_cycles as pc', 'pc.id', '=', 'payroll_summary.payroll_cycle_id')
                ->leftJoin('profiles as p', 'p.user_id', '=', 'payroll_summary.user_id')
                ->leftJoin('compensations as c', 'c.user_id', '=', 'payroll_summary.user_id')
                ->groupBy(['payroll_summary.user_id', 'pc.effective_month', 'pc.effective_year'])
                ->whereRaw($where)
                ->orderBy('p.last_name')
                ->get();

//            dd($data['employees']);

            $data['period_from'] = $request->period_from;
            $data['period_to'] = $request->period_to;
        }



        $data['pageHeader'] = 'Government Reports';
        return view('admin.reports.government.sss_monthly_contribution', $data);
    }

    public function hdmf(Request $request)
    {
        if(!empty($request->period_from)) {
            $periodFrom = Carbon::createFromFormat('F, Y', $request->period_from);
//            dd($periodFrom->format('Y-m-d'));



            if(!empty($request->period_to)) {
                $periodTo = Carbon::createFromFormat('F, Y', $request->period_to);
//                dd($periodTo->format('Y-m-d'));
                $where = "(DATE_FORMAT(pc.payroll_date, '%Y%m') >= '{$periodFrom->format("Ym")}' AND DATE_FORMAT(pc.payroll_date, '%Y%m') <= '{$periodTo->format("Ym")}')";
            } else {
                $where = "DATE_FORMAT(pc.payroll_date, '%Y%m') = '{$periodFrom->format("Ym")}'";
            }

//            dd($where);

            $data['employees'] = PayrollSummary::selectRaw("SUM(payroll_summary.hdmf_ee) as hdmf_ee,
                SUM(payroll_summary.hdmf_er) as hdmf_er,
                p.id_number,
                p.birthday,
                CONCAT(p.last_name, ', ', p.first_name) as name,
                c.tin,
                c.hdmf")
                ->leftJoin('payroll_cycles as pc', 'pc.id', '=', 'payroll_summary.payroll_cycle_id')
                ->leftJoin('profiles as p', 'p.user_id', '=', 'payroll_summary.user_id')
                ->leftJoin('compensations as c', 'c.user_id', '=', 'payroll_summary.user_id')
                ->groupBy(['payroll_summary.user_id', 'pc.effective_month', 'pc.effective_year'])
                ->whereRaw($where)
                ->orderBy('p.last_name')
                ->get();

//            dd($data['employees']);

            $data['period_from'] = $request->period_from;
            $data['period_to'] = $request->period_to;
        }

        $data['pageHeader'] = 'Government Reports';
        return view('admin.reports.government.hdmf', $data);
    }

    public function philhealth(Request $request)
    {
        if(!empty($request->period_from)) {
            $periodFrom = Carbon::createFromFormat('F, Y', $request->period_from);
//            dd($periodFrom->format('Y-m-d'));



            if(!empty($request->period_to)) {
                $periodTo = Carbon::createFromFormat('F, Y', $request->period_to);
//                dd($periodTo->format('Y-m-d'));
                $where = "(DATE_FORMAT(pc.payroll_date, '%Y%m') >= '{$periodFrom->format("Ym")}' AND DATE_FORMAT(pc.payroll_date, '%Y%m') <= '{$periodTo->format("Ym")}')";
            } else {
                $where = "DATE_FORMAT(pc.payroll_date, '%Y%m') = '{$periodFrom->format("Ym")}'";
            }

//            dd($where);

            $data['employees'] = PayrollSummary::selectRaw("SUM(payroll_summary.philhealth_ee) as philhealth_ee,
                SUM(payroll_summary.philhealth_er) as philhealth_er,
                p.id_number,
                p.birthday,
                CONCAT(p.last_name, ', ', p.first_name) as name,
                c.philhealth")
                ->leftJoin('payroll_cycles as pc', 'pc.id', '=', 'payroll_summary.payroll_cycle_id')
                ->leftJoin('profiles as p', 'p.user_id', '=', 'payroll_summary.user_id')
                ->leftJoin('compensations as c', 'c.user_id', '=', 'payroll_summary.user_id')
                ->groupBy(['payroll_summary.user_id', 'pc.effective_month', 'pc.effective_year'])
                ->whereRaw($where)
                ->orderBy('p.last_name')
                ->get();

//            dd($data['employees']);

            $data['period_from'] = $request->period_from;
            $data['period_to'] = $request->period_to;
        }

        $data['pageHeader'] = 'Government Reports';
        return view('admin.reports.government.philhealth', $data);
    }
}

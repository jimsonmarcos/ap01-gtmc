<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BillingController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Billing';
        return view('admin.reports.billing.billing', $data);
    }
}

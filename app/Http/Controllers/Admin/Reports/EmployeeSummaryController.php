<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Companies;
use App\PayrollCycles;
use App\PayrollGroups;
use App\PayrollSummary;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeSummaryController extends Controller
{
    public function index(Request $request)
    {
        $data['companies'] = Companies::with('PayrollGroups')->with('CostCenters')->orderBy('company_code')->get();

        if(!empty($request->company_id)) {
            $data['company'] = $company = Companies::find($request->company_id);
            if($request->payroll_group_id == 'All') {
                $payrollCycles = PayrollCycles::whereIn('payroll_group_id', $company->PayrollGroups->pluck('id'));
            } else {
                $payrollCycles = PayrollCycles::where('payroll_group_id', $request->payroll_group_id);
            }

            if(!empty($request->date_from) && !empty($request->date_to)) {
                $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
                $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
                $payrollCycles->whereRaw("payroll_date >= '{$dateFrom}' AND payroll_date <= '{$dateTo}'");
            }

            $payrollCycles->orderByDesc('payroll_date');
            $data['payrollCycles'] = $payrollCycles
                ->selectRaw("payroll_cycles.*, ps.user_id, p.first_name, p.last_name, SUM(ps.net_pay) as net_pay")
                ->leftJoin('payroll_summary as ps', 'ps.payroll_cycle_id', '=', 'payroll_cycles.id')
                ->leftJoin('profiles as p', 'p.user_id', '=', 'ps.user_id')
                ->where('payroll_cycles.status', 'Processed')
                ->groupBy('ps.user_id')
                ->orderBy('p.last_name')
                ->get();
        }

//        dd($data['payrollCycles']);

        $data['qs'] = $request->getQueryString();

        $data['request'] = $request->all();

        $data['nav'] = 'employee-summary';

        $data['pageHeader'] = "Summary of Employee";
        return view('admin.reports.payroll.employee', $data);
    }

    public function download(Request $request)
    {
        $data['company'] = $company = Companies::find($request->company_id);
        $payrollGroup = '';
        if($request->payroll_group_id == 'All') {
            $payrollCycles = PayrollCycles::whereIn('payroll_group_id', $company->PayrollGroups->pluck('id'));
        } else {
            $payrollCycles = PayrollCycles::where('payroll_group_id', $request->payroll_group_id);
            $payrollGroup = PayrollGroups::find($request->payroll_group_id);
        }

        if(!empty($request->date_from) && !empty($request->date_to)) {
            $dateFrom = Carbon::createFromFormat("m/d/Y", $request->date_from)->format('Y-m-d');
            $dateTo = Carbon::createFromFormat("m/d/Y", $request->date_to)->format('Y-m-d');
            $payrollCycles->whereRaw("payroll_date >= '{$dateFrom}' AND payroll_date <= '{$dateTo}'");
        }

        $payrollCycles = $payrollCycles
            ->selectRaw("payroll_cycles.id, payroll_cycles.payroll_group_id, GROUP_CONCAT(payroll_cycles.payroll_code) as payroll_code, GROUP_CONCAT(payroll_cycles.payroll_period) as payroll_period,GROUP_CONCAT(payroll_cycles.payroll_date) as payroll_date, ps.user_id, p.first_name, p.last_name, p.id_number, 
                SUM(ps.basic_pay) as basic_pay,
                SUM(ps.ecola) as ecola,
                SUM(ps.overtime_pay) as overtime_pay,
                SUM(ps.holiday_pay) as holiday_pay,
                SUM(ps.night_differential) as night_differential,
                SUM(ps.holiday_ecola) as holiday_ecola,
                SUM(ps.adjustment_wage) as adjustment_wage,
                SUM(ps.adjustment_deduction) as adjustment_deduction,
                SUM(ps.adjustment_income) as adjustment_income,
                SUM(ps.de_minimis) as de_minimis,
                SUM(ps.fixed_allowance) as fixed_allowance,
                SUM(ps.leave_encashment) as leave_encashment,
                SUM(ps.incentives) as incentives,
                SUM(ps.coop_savings) as coop_savings,
                SUM(ps.other_incomes) as other_incomes,
                SUM(ps.sss_ec) as sss_ec,
                SUM(ps.sss_er) as sss_er,
                SUM(ps.hdmf_er) as hdmf_er,
                SUM(ps.philhealth_er) as philhealth_er,
                SUM(ps.sss_ee) as sss_ee,
                SUM(ps.hdmf_ee) as hdmf_ee,
                SUM(ps.philhealth_ee) as philhealth_ee,
                SUM(ps.tardiness) as tardiness,
                SUM(ps.absences) as absences,
                SUM(ps.sss_loan) as sss_loan,
                SUM(ps.hdmf_loan) as hdmf_loan,
                SUM(ps.client_charges) as client_charges,
                SUM(ps.membership_fee) as membership_fee,
                SUM(ps.notarial_fee) as notarial_fee,
                SUM(ps.coop_loan) as coop_loan,
                SUM(ps.coop_share) as coop_share,
                SUM(ps.unpaid_leave) as unpaid_leave,
                SUM(ps.gross_amount) as gross_amount,
                SUM(ps.deductions) as deductions,
                SUM(ps.other_deductions) as other_deductions,
                SUM(ps.tax) as tax,
                SUM(ps.net_pay) as net_pay,
                SUM(ps.billing_net_pay) as billing_net_pay,
                SUM(ps.admin_fee) as admin_fee,
                SUM(ps.thirteenth_month_bonus) as thirteenth_month_bonus,
                SUM(ps.thirteenth_month_pro_rated) as thirteenth_month_pro_rated,
                SUM(ps.total_billing) as total_billing
            ")
            ->leftJoin('payroll_summary as ps', 'ps.payroll_cycle_id', '=', 'payroll_cycles.id')
            ->leftJoin('profiles as p', 'p.user_id', '=', 'ps.user_id')
            ->where('payroll_cycles.status', 'Processed')
            ->groupBy('ps.user_id')
            ->orderBy('p.last_name')
            ->get();

//        dd($payrollCycles);

        $title = 'Employee Summary';

        Excel::create($title, function ($excel) use ($company, $payrollCycles, $payrollGroup, $title, $request) {
            // Set the title
            $excel->setTitle($title);

            $sheetTitle = !empty($payrollGroup) ? $payrollGroup->group_name : 'All';

            $excel->sheet($sheetTitle, function ($sheet) use ($company, $payrollGroup, $payrollCycles, $request) {

                $headers = [
                    'Employee Name',
                    'ID Number',
                    'Payroll Code',
                    'Principal',
                    'Payroll Group',
                    'Payroll Date',
                    'BASIC PAY',
                    'ECOLA',
                    'OVERTIME PAY',
                    'HOLIDAY PAY',
                    'NIGHT DIFFERENTIAL',
                    'HOLIDAY ECOLA',
                    'ADJUSTMENT WAGE',
                    'ADJUSTMENT DEDUCTION',
                    'ADJUSTMENT INCOME',
                    'DE MINIMIS',
                    'FIXED ALLOWANCE',
                    'LEAVE ENCASHMENT',
                    'INCENTIVES',
                    'COOP SAVINGS',
                    'SSS EC',
                    'SSS ER',
                    'HDMF ER',
                    'PHILHEALTH ER',
                    'SSS EE',
                    'HDMF EE',
                    'PHILHEALTH EE',
                    'TARDINESS',
                    'ABSENCES',
                    'SSS LOAN',
                    'HDMF LOAN',
                    'CLIENT CHARGES',
                    'MEMBERSHIP FEE',
                    'NOTARIAL FEE',
                    'COOP LOAN',
                    'COOP SHARE',
                    'UNPAID LEAVE',
                    'GROSS AMOUNT',
                    'DEDUCTIONS',
                    'TAX',
                    'NET PAY',
                    'THIRTEENTH MONTH BONUS',
                    'THIRTEENTH MONTH PRO RATED',
                    'OTHER INCOMES',
                    'OTHER DEDUCTIONS'
                ];

                // Sheet manipulation
                $sheet->row(1, $headers);



                $x = 2;
                foreach ($payrollCycles as $payrollCycle) {

                    $row = array(
                        "{$payrollCycle->last_name}, {$payrollCycle->first_name}",
                        $payrollCycle->id_number,
                        $payrollCycle->payroll_code,
                        $payrollCycle->PayrollGroup->Company->company_code,
                        $payrollCycle->payroll_period,
                        $payrollCycle->payroll_date,
                        $payrollCycle->basic_pay,
                        $payrollCycle->ecola,
                        $payrollCycle->overtime_pay,
                        $payrollCycle->holiday_pay,
                        $payrollCycle->night_differential,
                        $payrollCycle->holiday_ecola,
                        $payrollCycle->adjustment_wage,
                        $payrollCycle->adjustment_deduction,
                        $payrollCycle->adjustment_income,
                        $payrollCycle->de_minimis,
                        $payrollCycle->fixed_allowance,
                        $payrollCycle->leave_encashment,
                        $payrollCycle->incentives,
                        $payrollCycle->coop_savings,
                        $payrollCycle->sss_ec,
                        $payrollCycle->sss_er,
                        $payrollCycle->hdmf_er,
                        $payrollCycle->philhealth_er,
                        $payrollCycle->sss_ee,
                        $payrollCycle->hdmf_ee,
                        $payrollCycle->philhealth_ee,
                        $payrollCycle->tardiness,
                        $payrollCycle->absences,
                        $payrollCycle->sss_loan,
                        $payrollCycle->hdmf_loan,
                        $payrollCycle->client_charges,
                        $payrollCycle->membership_fee,
                        $payrollCycle->notarial_fee,
                        $payrollCycle->coop_loan,
                        $payrollCycle->coop_share,
                        $payrollCycle->unpaid_leave,
                        $payrollCycle->gross_amount,
                        $payrollCycle->deductions,
                        $payrollCycle->tax,
                        $payrollCycle->net_pay,
                        $payrollCycle->thirteenth_month_bonus,
                        $payrollCycle->thirteenth_month_pro_rated,
                        $payrollCycle->other_incomes,
                        $payrollCycle->other_deductions
                    );




                    $sheet->row($x, $row);

                    $x++;
                }

                $sheet->setColumnFormat(array(
                    'G2:AS'.$x => '#,##0.00'
                ));
            });
        })->download('xls');
    }
}

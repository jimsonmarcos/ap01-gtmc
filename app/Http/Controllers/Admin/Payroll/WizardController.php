<?php

namespace App\Http\Controllers\Admin\Payroll;

use App\AuditLog;
use App\Compensations;
use App\ComputePayroll;
use App\EmploymentDetails;
use App\HDMFContributions;
use App\Keyval;
use App\LeaveHistory;
use App\LoanPayments;
use App\LoanPaymentSchedules;
use App\Loans;
use App\LoanTypes;
use App\OtherPayments;
use App\PayrollCycles;
use App\PayrollDtr;
use App\PayrollDtrDraft;
use App\PayrollDtrUpload;
use App\PayrollGroups;
use App\PayrollOthers;
use App\PayrollSummary;
use App\PhilHealthContributions;
use App\Profiles;
use App\SSSContributions;
use App\TaxTypes;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class WizardController extends Controller
{
    public function index(Request $request)
    {
        $data['pageHeader'] = "Payroll Wizard";

        $data['company_id'] = empty($request->get('company_id')) ? 'all' : $request->get('company_id');
        $data['payroll_group_id'] = empty($request->get('payroll_group_id')) ? 'all' : $request->get('payroll_group_id');
        $data['status'] = $status = empty($request->get('status')) ? '' : $request->get('status');

//        if(!empty($data['company_id']) && $data['company_id'] != 'all') {
//            $data['payrollGroups'] = PayrollGroups::where('company_id', $data['company_id'])->orderByDesc('payroll_date')->get();
//        }

        if(!empty($data['company_id']) && $data['company_id'] != 'all') {
            if(!empty($data['payroll_group_id']) && $data['payroll_group_id'] != 'all') {
                $payrollCycles = $data['payrollCycles'] = PayrollCycles::where('payroll_group_id', $data['payroll_group_id'])->orderByDesc('payroll_date')->get();
            } else {
                $payrollCycles = $data['payrollCycles'] = PayrollCycles::whereIn('payroll_group_id', PayrollGroups::where('company_id', $data['company_id'])->get()->pluck('id'))->orderByDesc('payroll_date')->get();
            }
        } else {
            $payrollCycles = $data['payrollCycles'] = PayrollCycles::orderByDesc('payroll_date')->get();
        }


        $data['open'] = $payrollCycles->where('status', '!=', 'Processed')->count();
//        dd($data['payrollCycles']);
        $data['processed'] = $payrollCycles->where('status', 'Processed')->count();

        if($status == 'Processed') {
            $data['payrollCycles'] = $data['payrollCycles']->where('status', 'Processed');
        } else {
            $data['payrollCycles'] = $data['payrollCycles']->where('status', '!=', 'Processed');
        }

//        $data['payrollCycles'] = $data['payrollCycles']->get();


        return view('admin.payroll.wizard', $data);
    }

    public function create_payroll_cycle()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Payroll";

        return view('admin.payroll.payroll_cycle', $data);
    }

    public function create_payroll_cycle_post(Request $request)
    {
        $data = $request->except('_token');

        if(PayrollCycles::where('payroll_code', $data['payroll_code'])->exists()) {
            $request->session()->flash('popError', "Payroll Cycle <strong>{$data['payroll_code']}</strong> already exists!");
            return redirect("/admin/payroll/wizard/payroll_cycle");
        }

        if($data['cycle'] == 'B') {
            $payrollCode = substr($data['payroll_code'], 0, -1). 'A';
            if(!PayrollCycles::where('payroll_code', $payrollCode)->exists()) {
                $request->session()->flash('popError', "Payroll Code <strong>{$payrollCode}</strong> must be processed first in order to process this payroll cycle!");
                return redirect()->back();
            }
        }

        $data['start_date'] = date('m/d/Y', strtotime($data['start_date']));
        $data['start_date'] = date('Y-m-d', strtotime($data['start_date']));
        $data['end_date'] = date('m/d/Y', strtotime($data['end_date']));
        $data['end_date'] = date('Y-m-d', strtotime($data['end_date']));
        $data['payroll_date'] = date('m/d/Y', strtotime($data['payroll_date']));
        $data['payroll_date'] = date('Y-m-d', strtotime($data['payroll_date']));

        $data['user_id'] = Auth::user()->id;
        if(!empty($data['calculate_13th_month'])) {
            $data['calculate_13th_month'] = 'Y';
        }
        if(!empty($data['calculate_leave_encashment'])) {
            $data['calculate_leave_encashment'] = 'Y';
        }

        $data['period'] = explode('-', $data['payroll_code']);
        $data['period'] = $data['period'][1];

        $payrollCycle = PayrollCycles::create($data);


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Payroll Wizard',
            'action' => 'Create',
            'description' => "Created Payroll Cycle: {$payrollCycle->payroll_code}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Payroll Cycle successfully created!");
        return redirect("/admin/payroll/wizard/payroll_members/{$payrollCycle->id}/upload_dtr");

    }

    public function payroll_members(Request $request, $id)
    {
        $data['pageHeader'] = "Payroll";

        $data['payrollCycle'] = PayrollCycles::find($id);
        $data['payrollCycleId'] = $id;

        $data['approvedItems'] = PayrollDtr::selectRaw('payroll_dtr.*, profiles.first_name, profiles.last_name')->leftJoin('profiles', 'profiles.user_id', '=', 'payroll_dtr.user_id')->where('payroll_cycle_id', $id)->orderByDesc('created_at')->get();
        $data['ongoingItems'] = PayrollDtrDraft::selectRaw('payroll_dtr_draft.*, profiles.first_name, profiles.last_name')->leftJoin('profiles', 'profiles.user_id', '=', 'payroll_dtr_draft.user_id')->where('payroll_cycle_id', $id)->whereNotIn('payroll_dtr_draft.id', $data['approvedItems']->pluck('draft_id'))->where('payroll_dtr_draft.status', 'Success')->get();

        $data['allItems'] = $data['approvedItems']->merge($data['ongoingItems'])->sortBy('last_name');

        return view('admin.payroll.payroll_members', $data);
    }

    public function upload_dtr(Request $request, $id, $status = '')
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Payroll";

        $data['payrollCycle'] = PayrollCycles::find($id);
        $data['payrollCycleId'] = $id;

        $data['status'] = empty($status) ? 'All' : $status;

        $view = 'admin.payroll.upload_dtr';

        if($status == 'Existing') {
            $view = 'admin.payroll.upload_dtr_existing';
        }

        $status = empty($status) ? ['Success', 'Failed', 'Removed', 'Overwrite', 'No Match', 'Existing'] : [$status];
        $data['allItems'] = PayrollDtrDraft::where(['payroll_cycle_id' => $id])->whereIn('status', ['Success', 'Failed', 'Removed', 'Overwrite', 'No Match', 'Existing']);
        $data['items'] = PayrollDtrDraft::where(['payroll_cycle_id' => $id])->whereIn('status', $status)->get();



        return view($view, $data);
    }

    public function upload_dtr_existing(Request $request)
    {
        $status = $request->action;
        $existing = $request->input('payroll_dtr_draft');

        if(!empty($existing)) {
            foreach($existing as $key => $val) {
                $draft = PayrollDtrDraft::find($key);
                $draft->status = $status;
                $draft->save();
            }
        }


        $request->session()->flash('popSuccess', "{$status} successful!");
        $payrollCycleId = $request->segment(5);
        return redirect(url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/upload_dtr"));
    }

    public function upload_dtr_post(Request $request, $id)
    {
        $data['user_id'] = Auth::user()->id;
        $data['payroll_cycle_id'] = $id;
        $data['filepath'] = $request->dtr->store("public/dtr");
        $data['filename'] = $request->file('dtr')->getClientOriginalName();

        $dtrUpload = PayrollDtrUpload::create($data);

        $excel = App::make('excel');

        $result = $excel->load('storage/app/'.$data['filepath'], function($reader) {
            $reader->first();
            $reader->skipRows(0);

//            $reader->get();
//            $reader->toArray();
//            dd($reader);
        })->get();

//        dd($result);

        $payrollCycle = PayrollCycles::find($id);

        if(!empty($result) && $result->count() > 0) {
            foreach($result as $item) {
                if(!empty(trim($item->employee_id))) {
                    $profile = Profiles::where('id_number', $item->employee_id)->first();

                    if(!empty($profile) && $profile->status == 'Active') {
                        $existingDtrDraft = PayrollDtrDraft::where(['payroll_cycle_id' => $id, 'user_id' => $profile->user_id])->orderByDesc('id')->first();

                        if(!empty($existingDtrDraft)) {
                            PayrollDtrDraft::where(['payroll_cycle_id' => $id, 'user_id' => $profile->user_id])->where('id', '<', $existingDtrDraft->id)->delete();
                        }

                        $draft = new PayrollDtrDraft();
                        $draft->payroll_cycle_id = $id;
                        $draft->payroll_dtr_upload_id = $dtrUpload->id;
                        $draft->user_id = $profile->user_id;
                        $draft->id_number = $item->employee_id;
                        $draft->employee_name = $item->employee_name;
                        $draft->wh = $item->wh;
                        $draft->rds = $item->rd_hrs;
                        $draft->spe = $item->spe_hrs;
                        $draft->sperd = $item->sperd_hrs;
                        $draft->leg = $item->leg_hrs;
                        $draft->legrd = $item->legrd_hrs;
                        $draft->reg_ot = $item->reg_ot_hrs;
                        $draft->nd = $item->nd_hrs;
                        $draft->abs = $item->abs_mins;
                        $draft->lt = $item->lt_mins;
                        $draft->ut = $item->ut_mins;
                        $draft->vl = $item->vl_hrs;
                        $draft->sl = $item->sl_hrs;
                        $draft->el = $item->el_hrs;
                        $draft->spl = $item->spl_hrs;
                        $draft->ml = $item->ml_hrs;
                        $draft->pl = $item->pl_hrs;
                        $draft->pto = $item->pto_hrs;
                        $draft->bl = $item->bl_hrs;
                        $draft->cl = $item->cl_hrs;

                        $draft->incentive = !empty($item->incentive) ? $item->incentive : 0;
                        $draft->nwage = !empty($item->nwage) ? $item->nwage : 0;
                        $draft->client_charges = !empty($item->clcharges) ? $item->clcharges : 0;
                        $draft->adji = !empty($item->adji) ? $item->adji : 0;
                        $draft->adjd = !empty($item->adjd) ? $item->adjd : 0;

                        $draft->updated_by = Auth::user()->id;
                        $draft->source = 'Time Sheet';

                        if(empty($draft->user_id)) {
                            $draft->status = 'No Match';
                        } else {
                            if(!in_array($payrollCycle->payroll_group_id, \App\EmploymentDetails::where('user_id', $draft->user_id)->first()->Principal->PayrollGroups->pluck('id')->toArray())) {
                                $draft->status = 'No Match';
                            } elseif(PayrollDtr::where(['payroll_cycle_id' => $id, 'user_id' => $draft->user_id])->exists()) {
                                $draft->status = 'Existing';
                            } elseif(PayrollDtrDraft::where(['payroll_cycle_id' => $id, 'user_id' => $draft->user_id, 'status' => 'Success'])->exists()) {
                                PayrollDtrDraft::where(['payroll_cycle_id' => $id, 'user_id' => $draft->user_id, 'status' => 'Success'])->update(['status' => 'Archived']);
                            } else {
                                $draft->status = 'Success';
                            }
                        }

                        $draft->save();

                    }

                }
            }
        }

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Payroll Wizard',
            'action' => 'Create',
            'description' => "Uploaded DTR for Payroll Cycle: {$payrollCycle->payroll_code}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "DTR successfully uploaded!");
        return redirect("/admin/payroll/wizard/payroll_members/{$id}/upload_dtr");
    }

    public function upload_dtr_approve(Request $request, $id)
    {
        $items = PayrollDtrDraft::where(['payroll_cycle_id' => $id])->whereIn('status', ['Success', 'Failed', 'Removed', 'Overwrite', 'No Match', 'Existing'])->get();

        foreach($items as $i) {
            if($i->status == 'Success') {
                $i->draft_id = $i->id;
                unset($i->created_at);
                unset($i->updated_at);
                unset($i->payroll_dtr_upload_id);
                unset($i->status);
                unset($i->updated_by);
                unset($i->id);

                PayrollDtr::create($i->toArray());

                PayrollDtrDraft::find($i->draft_id)->update(['status' => 'Applied']);
            } elseif(in_array($i->status, ['Failed', 'No Match', 'Removed', 'Existing'])) {
                PayrollDtrDraft::find($i->id)->update(['status' => 'Archived']);
            } elseif($i->status == 'Overwrite') {
                $i->draft_id = $i->id;
                unset($i->created_at);
                unset($i->updated_at);
                unset($i->payroll_dtr_upload_id);
                unset($i->status);
                unset($i->updated_by);
                unset($i->id);

                $dtr = PayrollDtr::where(['payroll_cycle_id' => $i->payroll_cycle_id, 'user_id' => $i->user_id])->first();
                $dtr->update($i->toArray());

                PayrollDtrDraft::find($i->draft_id)->update(['status' => 'Overwritten', 'updated_by' => Auth::user()->id]);
            }
        }

        $payrollCycle = PayrollCycles::find($id);
//        dd($payrollCycle->DTR);
        if(!empty($payrollCycle->DTR) && $payrollCycle->DTR->count() > 0) {
            $payrollCycle->status = 'In Progress';
            $payrollCycle->save();

            AuditLog::create([
                'user_id' => Auth::user()->id,
                'category' => 'Payroll Wizard',
                'action' => 'Edit',
                'description' => "Payroll Cycle: {$payrollCycle->payroll_code} status changed to In Progress",
                'is_admin' => session('is_admin') ? 'Y' : 'N'
            ]);
        }

        $request->session()->flash('popSuccess', "DTR successfully Approved!");
        return redirect("/admin/payroll/wizard/payroll_members/{$id}");
    }

    public function upload_dtr_cancel(Request $request, $id)
    {
        PayrollDtrDraft::whereIn('status', ['Success', 'Failed', 'Removed', 'Overwrite', 'No Match', 'Existing'])->where('payroll_cycle_id', $id)->update(['status' => 'Archived']);
        
        $request->session()->flash('popSuccess', "DTR successfully Cancelled!");
        return redirect("/admin/payroll/wizard/payroll_members/{$id}");
    }

    public function upload_dtr_failed()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Payroll";

        return view('admin.payroll.upload_dtr_failed', $data);
    }

    public function upload_dtr_manually()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Payroll";

        return view('admin.payroll.upload_dtr_manually', $data);
    }

    public function process(Request $request, $id)
    {
        $data['pageHeader'] = "Payroll";

        $data['payrollCycle'] = $payrollCycle = PayrollCycles::find($id);

        if($payrollCycle->status == 'Processed') {
//            $request->session()->flash('popError', "Payroll cycle was already processed!");
            return redirect("/admin/payroll/wizard/payroll_members/{$id}/processed_payroll");
        }

        $data['payrollCycleId'] = $id;
        $data['dtr'] = $dtr = PayrollDtr::select('payroll_dtr.*')->leftJoin('profiles', 'profiles.user_id', '=', 'payroll_dtr.user_id')->where('payroll_cycle_id', $id)->orderBy('last_name')->get();

        foreach($dtr as $d) {
            if(empty($d->User->Profile->Compensation->PayrollGroup->id)) {
                $request->session()->flash('popError', "ID Number: <strong>{$d->User->Profile->id_number}</strong> does not have payroll group!");
                return redirect()->back();
            }
        }

        if($payrollCycle->cycle == 'B') {
            $payrollCode = substr($payrollCycle->payroll_code, 0, -1). 'A';
            if(!PayrollCycles::where('payroll_code', $payrollCode)->where('status', 'Processed')->exists()) {
                $request->session()->flash('popError', "Payroll Code <strong>{$payrollCode}</strong> must be processed first in order to process this payroll cycle!");
                return redirect()->back();
            }
        }



        $ongoingItems = PayrollDtrDraft::where('payroll_cycle_id', $id)->whereNotIn('id', $data['dtr']->pluck('draft_id'))->where('status', 'Success')->get();

        if($ongoingItems->count() > 0) {
            $request->session()->flash('popError', "Payroll Cycle still has ongoing items!");
            return redirect()->back();
        }

        return view('admin.payroll.process', $data);
    }

    public function process_post(Request $request, $id)
    {
        $payrollCycle = PayrollCycles::find($id);

        if($payrollCycle->status == 'Processed') {
//            $request->session()->flash('popError', "Payroll cycle was already processed!");
            return redirect("/admin/payroll/wizard/payroll_members/{$id}/processed_payroll");
        }

//        dd($request->input('dtr'));

        foreach($request->input('dtr') as $key => $holdStatus) {
            if(isset($payroll)) {
                unset($payroll);
            }
            $d = PayrollDtr::find($key);

            $user = $d->User;
            $payrollCode = explode('-', $payrollCycle->payroll_code);
            $user_id = $user->id;

            if($d->User->Profile->Compensation->rate_type == 'Monthly') {
                $payrollComputation = $payroll = new ComputePayroll($d, ['daily_rate' => $d->User->Profile->Compensation->monthly_rate / 26, 'monthly_rate' => $d->User->Profile->Compensation->monthly_rate / 2, 'rate_type' => 'Monthly']);
            } else {
                if($d->User->Profile->Compensation->daily_category == 'Location') {
                    $payrollComputation = $payroll = new ComputePayroll($d, $d->User->Profile->Compensation->DailyRate->rate);
                } else {
                    $payrollComputation = $payroll = new ComputePayroll($d, $d->User->Profile->Compensation->daily_rate);
                }
            }

            $ytdPayrollCycles = PayrollCycles::where(['payroll_group_id' => $user->Profile->Compensation->PayrollGroup->id, 'effective_year' => $payrollCycle->effective_year, 'status' => 'Processed'])->pluck('id');
            $ytdPayrollSummary = PayrollSummary::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $d->User->id)->get();

            //        dd(PayrollCycles::where('status', 'Processed')->whereYear('payroll_date', date('Y', strtotime( $data['payrollCycle']->payroll_date)))->pluck('id'));

            $accumulatedHours = 0;
            $totalLeaves = 0;
            $unpaidLeave = 0;
            if(!empty($ytdPayrollSummary)) {
                $ytdDtr = PayrollDtr::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $user->id)->get();
                $accumulatedHours = $ytdDtr->sum('vl') + $ytdDtr->sum('sl') + $ytdDtr->sum('el') + $ytdDtr->sum('spl') + $ytdDtr->sum('ml') + $ytdDtr->sum('pl') + $ytdDtr->sum('pto') + $ytdDtr->sum('bl') + $ytdDtr->sum('cl');

                $leaves = [
                  [
                      'leave_type' => 'Vacation Leave',
                      'user_id' => $user_id,
                      'payroll_cycle_id' => $payrollCycle->id,
                      'hours' => $payroll->vl,
                      'days' => floor($payroll->vl / 8)
                  ],
                  [
                      'leave_type' => 'Sick Leave',
                      'user_id' => $user_id,
                      'payroll_cycle_id' => $payrollCycle->id,
                      'hours' => $payroll->sl,
                      'days' => floor($payroll->sl / 8)
                  ],
                  [
                      'leave_type' => 'Emergency Leave',
                      'user_id' => $user_id,
                      'payroll_cycle_id' => $payrollCycle->id,
                      'hours' => $payroll->el,
                      'days' => floor($payroll->el / 8)
                  ],
                  [
                      'leave_type' => 'Special Parent Leave',
                      'user_id' => $user_id,
                      'payroll_cycle_id' => $payrollCycle->id,
                      'hours' => $payroll->spl,
                      'days' => floor($payroll->spl / 8)
                  ],
                  [
                      'leave_type' => 'Maternity Leave',
                      'user_id' => $user_id,
                      'payroll_cycle_id' => $payrollCycle->id,
                      'hours' => $payroll->ml,
                      'days' => floor($payroll->ml / 8)
                  ],
                  [
                      'leave_type' => 'Paternity Leave',
                      'user_id' => $user_id,
                      'payroll_cycle_id' => $payrollCycle->id,
                      'hours' => $payroll->pl,
                      'days' => floor($payroll->pl / 8)
                  ],
                  [
                      'leave_type' => 'Paid Time Off',
                      'user_id' => $user_id,
                      'payroll_cycle_id' => $payrollCycle->id,
                      'hours' => $payroll->pto,
                      'days' => floor($payroll->pto / 8)
                  ],
                  [
                      'leave_type' => 'Bereavement Leave',
                      'user_id' => $user_id,
                      'payroll_cycle_id' => $payrollCycle->id,
                      'hours' => $payroll->bl,
                      'days' => floor($payroll->bl / 8)
                  ],
                  [
                      'leave_type' => 'Calamity Leave',
                      'user_id' => $user_id,
                      'payroll_cycle_id' => $payrollCycle->id,
                      'hours' => $payroll->cl,
                      'days' => floor($payroll->cl / 8)
                  ],
                ];

                $leaveBalance = (5 * 8) - $accumulatedHours;
                foreach($leaves as $leave) {
                    if($leave['hours'] > 0) {
                        if($leaveBalance > 0) {
                            if($leaveBalance >= $leave['hours']) {
                                $leave['status'] = 'Paid';
                                LeaveHistory::create($leave);
                                $leaveBalance -= $leave['hours'];
                            } else {
                                $unpaidHours = $leave['hours'] - $leaveBalance;
                                $paidHours = $leaveBalance;

                                // Insert Paid Hours
                                $leave['hours'] = $paidHours;
                                $leave['days'] = floor($paidHours / 8);
                                $leave['status'] = 'Paid';
                                LeaveHistory::create($leave);

                                // Insert Unpaid Hours
                                $leave['hours'] = $unpaidHours;
                                $leave['days'] = floor($unpaidHours / 8);
                                $leave['status'] = 'Unpaid';
                                LeaveHistory::create($leave);

                                // Set Leave Balance to zero
                                $leaveBalance = 0;
                            }
                        } else {
                            LeaveHistory::create($leave);
                        }
                    }
                }

                $leaves = $payroll->vl + $payroll->sl + $payroll->el + $payroll->spl + $payroll->ml + $payroll->pl + $payroll->pto + $payroll->bl + $payroll->cl;

                if($leaves > 0) {
                    $bal = (5 * 8) - $accumulatedHours;
                    if($bal > 0) {

                        $bal -= $leaves;
                        if($bal < 0) {
                            $unpaidLeave = abs($bal)* $payroll->hourlyRate();
                        }

                    } else {
                        $unpaidLeave = $leaves* $payroll->hourlyRate();
                    }
                }
            }


            $effectiveMonth = strlen($payrollCycle->effective_month) == 1 ? "0{$payrollCycle->effective_month}" : $payrollCycle->effective_month;



            $loans = \App\LoanPaymentSchedules::where(['pay_period' => $payrollCode[1], 'user_id' => $user_id]);

            $sssLoan = 0;
            $hdmfLoan = 0;
            $coopLoan = 0;
            foreach($loans->get() as $lps) {
                if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'SSS') {
                    $sssLoan += $lps->Loan->amortization;

                    $transactionId = "P-". str_replace('-', '', $payrollCycle->payroll_date);

                    $latestPayment = LoanPayments::where('payment_date', $payrollCycle->payroll_date)->orderByDesc('payment_date')->get();

                    $series = !empty($latestPayment) ? $latestPayment->max('series') + 1 : "0001";

                    if(strlen($series) < 4) {
                        while(strlen($series) < 4) {
                            $series = "0{$series}";
                        }
                    }

                    $transactionId = "{$transactionId}-{$series}";

//                    $claNumber = LoanPayments::count() == 0 ? 1001 : LoanPayments::select('series')->get()->max('series') + 1;
//                    $arNumber = LoanPayments::orderBy('ar_number', 'desc')->first();
//                    if(!empty($arNumber)) {
//                        $arNumber = explode('-', $arNumber->ar_number);
//                        $arNumber = $arNumber[1] + 1;
//                        $arNumber = "AR-{$arNumber}";
//                    } else {
//                        $arNumber = "AR-101";
//                    }

                    $arNumber = 'AR-000';


                    $payment = [
                        'series' => (int) $series,
                        'ar_number' => $arNumber,
                        'transaction_id' => $transactionId,
                        'pay_period' => "{$payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}",
                        'loan_id' => $lps->Loan->id,
                        'loan_installment_code' => "PR-{$payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}",
                        'amount' => $lps->Loan->amortization,
                        'total_amount_paid' => $lps->Loan->amortization,
                        'payment_date' => $payrollCycle->payroll_date,
                        'payment_method' => 'Salary Deduction',
                        'remarks' => "Salary Deduction from Payroll {$payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}"
                    ];

                    LoanPayments::create($payment);


                    $loan = Loans::selectRaw('loans.*, COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.lapsed_interest, 0) as lapsed_interest')->where('id', $lps->Loan->id);
                    $loan = $loan->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id');
                    $loan = $loan->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(lapsed_interest) as lapsed_interest FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payment_schedules.loan_id', '=', 'loans.id');

                    $loan = $loan->first();


                    if(($loan->granted_loan_amount + $loan->lapsed_interest) - $loan->total_amount_paid <= 0) {
                        $loan->is_paid = 'Y';
                        $loan->save();
                    }
                }

                if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'HDMF') {
                    $hdmfLoan += $lps->Loan->amortization;

                    $transactionId = "P-". str_replace('-', '', $payrollCycle->payroll_date);

                    $latestPayment = LoanPayments::where('payment_date', $payrollCycle->payroll_date)->orderByDesc('payment_date')->get();

                    $series = !empty($latestPayment) ? $latestPayment->max('series') + 1 : "0001";

                    if(strlen($series) < 4) {
                        while(strlen($series) < 4) {
                            $series = "0{$series}";
                        }
                    }

                    $transactionId = "{$transactionId}-{$series}";

//                    $claNumber = LoanPayments::count() == 0 ? 1001 : LoanPayments::select('series')->get()->max('series') + 1;
//                    $arNumber = LoanPayments::orderBy('ar_number', 'desc')->first();
//                    if(!empty($arNumber)) {
//                        $arNumber = explode('-', $arNumber->ar_number);
//                        $arNumber = $arNumber[1] + 1;
//                        $arNumber = "AR-{$arNumber}";
//                    } else {
//                        $arNumber = "AR-101";
//                    }

                    $arNumber = 'AR-000';

                    $payment = [
                        'series' => (int) $series,
                        'ar_number' => $arNumber,
                        'transaction_id' => $transactionId,
                        'pay_period' => "{$payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}",
                        'loan_id' => $lps->Loan->id,
                        'loan_installment_code' => "PR-{$payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}",
                        'amount' => $lps->Loan->amortization,
                        'total_amount_amount' => $lps->Loan->amortization,
                        'payment_date' => $payrollCycle->payroll_date,
                        'payment_method' => 'Salary Deduction',
                        'remarks' => "Salary Deduction from Payroll {$payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}"
                    ];

                    LoanPayments::create($payment);


                    $loan = Loans::selectRaw('loans.*, COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.lapsed_interest, 0) as lapsed_interest')->where('id', $lps->Loan->id);
                    $loan = $loan->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id');
                    $loan = $loan->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(lapsed_interest) as lapsed_interest FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payment_schedules.loan_id', '=', 'loans.id');

                    $loan = $loan->first();


                    if(($loan->granted_loan_amount + $loan->lapsed_interest) - $loan->total_amount_paid <= 0) {
                        $loan->is_paid = 'Y';
                        $loan->save();
                    }
                }

                if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && in_array($lps->Loan->loan_type_id, ['1', '2', '3', '4'])) {
                    $coopLoan += $lps->Loan->amortization;

                    $transactionId = "P-". str_replace('-', '', $payrollCycle->payroll_date);

                    $latestPayment = LoanPayments::where('payment_date', $payrollCycle->payroll_date)->orderByDesc('payment_date')->get();

                    $series = !empty($latestPayment) ? $latestPayment->max('series') + 1 : "0001";

                    if(strlen($series) < 4) {
                        while(strlen($series) < 4) {
                            $series = "0{$series}";
                        }
                    }

                    $transactionId = "{$transactionId}-{$series}";

//                    $claNumber = LoanPayments::count() == 0 ? 1001 : LoanPayments::select('series')->get()->max('series') + 1;
//                    $arNumber = LoanPayments::orderBy('ar_number', 'desc')->first();
//                    if(!empty($arNumber)) {
//                        $arNumber = explode('-', $arNumber->ar_number);
//                        $arNumber = $arNumber[1] + 1;
//                        $arNumber = "AR-{$arNumber}";
//                    } else {
//                        $arNumber = "AR-101";
//                    }

                    $arNumber = 'AR-000';

                    $payment = [
                        'series' => (int) $series,
                        'ar_number' => $arNumber,
                        'transaction_id' => $transactionId,
                        'pay_period' => "{$payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}",
                        'loan_id' => $lps->Loan->id,
                        'loan_installment_code' => "PR-{$payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}",
                        'amount' => $lps->Loan->amortization,
                        'total_amount_paid' => $lps->Loan->amortization,
                        'payment_date' => $payrollCycle->payroll_date,
                        'payment_method' => 'Salary Deduction',
                        'remarks' => "Salary Deduction from Payroll {$payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}"
                    ];

                    LoanPayments::create($payment);


                    $loan = Loans::selectRaw('loans.*, COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.lapsed_interest, 0) as lapsed_interest')->where('id', $lps->Loan->id);
                    $loan = $loan->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id');
                    $loan = $loan->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(lapsed_interest) as lapsed_interest FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payment_schedules.loan_id', '=', 'loans.id');

                    $loan = $loan->first();


                    if(($loan->granted_loan_amount + $loan->lapsed_interest) - $loan->total_amount_paid <= 0) {
                        $loan->is_paid = 'Y';
                        $loan->save();
                    }
                }
            }

            if($payrollCycle->cycle == 'B') {
                $payrollCycleA = PayrollCycles::where([
                    'payroll_group_id' => $payrollCycle->payroll_group_id,
                    'effective_year' => $payrollCycle->effective_year,
                    'effective_month' => $payrollCycle->effective_month,
                    'cycle' => 'A',
                    'status' => 'Processed'
                ])->first();

                $payrollSummaryA = PayrollSummary::where('payroll_cycle_id', $payrollCycleA->id)->where('user_id', $d->user_id)->first();
                $basicPayA = !empty($payrollSummaryA->basic_pay) ? $payrollSummaryA->basic_pay : 0;
            }

            $basicPayB = 0;
            if($payrollCycle->cycle == 'A') {
                $effectiveDate = Carbon::createFromFormat('Y-m-d', "{$payrollCycle->effective_year}-{$payrollCycle->effective_month}-01")->subMonth(1);
                $payrollCycleB = PayrollCycles::where([
                    'payroll_group_id' => $payrollCycle->payroll_group_id,
                    'effective_year' => $effectiveDate->format('Y'),
                    'effective_month' => $effectiveDate->format('m'),
                    'cycle' => 'B',
                    'status' => 'Processed'
                ])->first();

                if(empty($payrollCycleB)) {
                    $basicPayB = 0;
                } else {
                    $payrollSummaryB = PayrollSummary::where('payroll_cycle_id', $payrollCycleB->id)->where('user_id', $d->user_id)->first();
                    $basicPayB = !empty($payrollSummaryB->basic_pay) ? $payrollSummaryB->basic_pay : 0;
                }


            }

            $philhealth_ee = 0;
            $philhealth_er = 0 ;
            if($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_philhealth == 'Y') {
                $philHealthContribution = PhilHealthContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first();
                if($philHealthContribution->id == 3) {
                    $monthlyPremium = ($payroll->basicPay() + $basicPayB) * ($philHealthContribution->premium_rate / 100);
                } elseif($philHealthContribution->id == 1) {
                    $monthlyPremium = 275;
                } elseif($philHealthContribution->id == 2) {
                    $monthlyPremium = 1100;
                }

                $philhealth_ee = $philhealth_er = $monthlyPremium / 2;
            }

            $thirteenthMonth = 0;
            if($payrollCycle->calculate_13th_month == 'Y' && EmploymentDetails::where('user_id', $d->user_id)->first()->position_id != 1) {
                $lastDecember = PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)) - 1)->whereMonth('payroll_date', 12)->get();
//            $toNovemberThisYear = PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)))->whereMonth('payroll_date', 11)->get();
                $toNovemberThisYear = \App\PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereDate('payroll_date', '>=', date('Y-01-01', strtotime($payrollCycle->payroll_date)))->whereDate('payroll_date', '<=', date('Y-11-30', strtotime($payrollCycle->payroll_date)))->get();

                foreach($lastDecember as $pc) {
                    $pastSummary = PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $d->user_id])->first();
                    $thirteenthMonth += !empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0;
                }

                foreach($toNovemberThisYear as $pc) {
                    $pastSummary = PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $d->user_id])->first();
                    $thirteenthMonth +=  (!empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0);
                }
            }

            $membershipFee = 0;
            if(OtherPayments::where('user_id', $d->user_id)->where('transaction_type', 'MF')->sum('total_amount_paid') < \App\Keyval::where('key', 'membership_fee')->first()->value) {
                $membershipFee = \App\Keyval::where('key', 'membership_fee')->first()->value / 2;

//                $arNumber = OtherPayments::orderBy('ar_number', 'desc')->first();
//                if(!empty($arNumber)) {
//                    $arNumber = explode('-', $arNumber->ar_number);
//                    $arNumber = $arNumber[1] + 1;
//                    $arNumber = "AR-{$arNumber}";
//                } else {
//                    $arNumber = "AR-101";
//                }

                $payment = [
                    'ar_number' => 'AR-000',
                    'user_id' => $d->user_id,
                    'transaction_type' => 'MF',
                    'payment_method' => 'Salary Deduction',
                    'pay_period' => "{$payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}",
                    'payment_date' => $payrollCycle->payroll_date,
                    'amount' => $membershipFee,
                    'total_amount_paid' => $membershipFee,
                    'remarks' => "Salary Deduction from Payroll $payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}"
                ];

                OtherPayments::create($payment);
            }

            $notarialFee = 0;
            if(OtherPayments::where('user_id', $d->user_id)->where('transaction_type', 'NF')->sum('total_amount_paid') < \App\Keyval::where('key', 'notarial_fee')->first()->value) {
                $notarialFee = \App\Keyval::where('key', 'notarial_fee')->first()->value / 2;

//                $arNumber = OtherPayments::orderBy('ar_number', 'desc')->first();
//                if(!empty($arNumber)) {
//                    $arNumber = explode('-', $arNumber->ar_number);
//                    $arNumber = $arNumber[1] + 1;
//                    $arNumber = "AR-{$arNumber}";
//                } else {
//                    $arNumber = "AR-101";
//                }

                $payment = [
                    'ar_number' => 'AR-000',
                    'user_id' => $d->user_id,
                    'transaction_type' => 'NF',
                    'payment_method' => 'Salary Deduction',
                    'pay_period' => "{$payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}",
                    'payment_date' => $payrollCycle->payroll_date,
                    'amount' => $notarialFee,
                    'total_amount_paid' => $notarialFee,
                    'remarks' => "Salary Deduction from Payroll $payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}"
                ];

                OtherPayments::create($payment);
            }

            if($d->Profile->coop_share > 0) {

                $payment = [
                    'ar_number' => 'AR-000',
                    'user_id' => $d->user_id,
                    'transaction_type' => 'COOP SHARE',
                    'payment_method' => 'Salary Deduction',
                    'pay_period' => "{$payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}",
                    'payment_date' => $payrollCycle->payroll_date,
                    'amount' => $d->Profile->coop_share,
                    'total_amount_paid' => $d->Profile->coop_share,
                    'remarks' => "Salary Deduction from Payroll $payrollCycle->effective_year}{$effectiveMonth}{$payrollCycle->cycle}"
                ];

                OtherPayments::create($payment);
            }






//            ($payrollCycle->cycle == 'B' && $d->Profile->Compensation->compute_philhealth == 'Y') ? PhilHealthContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayA)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayA)->first()->employer_share : 0;



            $data = [
                'payroll_cycle_id' => $d->payroll_cycle_id,
                'user_id'=> $d->user_id,
                'payroll_dtr_id'=> $d->id,
                'basic_pay' => $payroll->basicPay(),
                'ecola' => ($d->Profile->Compensation->daily_category == 'Location') ? ($payroll->rg() / 8) * $d->Profile->Compensation->DailyRate->ecola : 0.00,
                'overtime_pay' => $payroll->OTPay(),
                'holiday_pay' => $payroll->HDPay(),
                'night_differential' => $payroll->NDPay(),
                'holiday_ecola' => $d->Profile->Compensation->daily_category == 'Location' ? ($payroll->leg / 8) * $d->Profile->Compensation->DailyRate->ecola : 0.00,
                'adjustment_wage' => $d->nwage,
                'adjustment_income' => $d->adji,
                'adjustment_deduction' => $d->adjd,
                'de_minimis' => $d->Profile->Compensation->de_minimis,
                'thirteenth_month_bonus' => $thirteenthMonth,
                'thirteenth_month_pro_rated' => EmploymentDetails::where('user_id', $d->user_id)->first()->position_id != 1 ? $payroll->basicPay() / 12 : 0,
                'fixed_allowance' => $d->Profile->Compensation->fixed_allowance / 2,
                'leave_encashment' => 0, // TODO
                'incentives' => $d->incentive,
                'coop_savings' => 0, // Omitted
                'sss_ec' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ec : 0,
                'sss_er' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->er : 0,
                'hdmf_er' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_hdmf == 'Y') ? \App\HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employer_share : 0,
                'philhealth_er' => $philhealth_er,
                'sss_ee' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ee : 0,
                'hdmf_ee' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_hdmf == 'Y') ? \App\HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employee_share : 0,
                'philhealth_ee' => $philhealth_ee,
                'tardiness' => ($payroll->hourlyRate() / 60) * ($payroll->lt + $payroll->ut),
                'absences' => ($payroll->hourlyRate() / 60) * $payroll->abs,
                'sss_loan' => $sssLoan,
                'hdmf_loan' => $hdmfLoan,
                'client_charges' => $d->client_charges,
                'membership_fee' => $membershipFee,
                'notarial_fee' => $notarialFee,
                'coop_loan' => $coopLoan,
                'coop_share' => $d->Profile->coop_share,
                'unpaid_leave' => $unpaidLeave
            ];


            if($unpaidLeave > 0) {
                $incomeTx = $data['basic_pay'] + $payrollComputation->rd() + $payrollComputation->spe() + $payrollComputation->sperd() + $payrollComputation->leg() + $payrollComputation->legrd() + $data['overtime_pay'] + $data['night_differential'] + $data['ecola'] + $data['holiday_ecola'] + $data['adjustment_wage'];
            } else {
                $incomeTx = $data['basic_pay'] + $payrollComputation->rd() + $payrollComputation->spe() + $payrollComputation->sperd() + $payrollComputation->leg() + $payrollComputation->legrd() + $data['overtime_pay'] + $data['night_differential'] + $payrollComputation->vl() + $payrollComputation->sl() + $payrollComputation->el() + $payrollComputation->spl() + $payrollComputation->ml() + $payrollComputation->pl() + $payrollComputation->pto() + $payrollComputation->bl() + $payrollComputation->cl() + $data['ecola'] + $data['holiday_ecola'] + $data['adjustment_wage'];
            }

            $incomeNtx = $data['de_minimis'] + $data['thirteenth_month_bonus'] + $data['fixed_allowance'] + $data['leave_encashment'] + $data['incentives'] + $payroll['adjustment_income'];
            $deductionTx = $data['sss_ee'] + $data['hdmf_ee'] + $data['philhealth_ee'] + $data['tardiness'] + $data['absences'];
            $deduction = $data['sss_loan'] + $data['hdmf_loan'] + $data['client_charges'] + $data['membership_fee'] + $data['notarial_fee'] + $data['coop_loan'] + $data['coop_share'] + $data['adjustment_deduction'];

            $data['tax'] = 0;
            if(\App\Compensations::where('user_id', $d->user_id)->first()->deduct_withholding == 'Y') {
                $taxTable = new \App\TaxTable($d->User->Profile->Compensation->TaxType->title, $incomeTx);
                $data['tax'] = $taxTable->taxDue();
            }

            $data['gross_amount'] = $incomeTx + $incomeNtx;
            $data['deductions'] = $deductionTx + $deduction + $unpaidLeave;
            $data['net_pay'] = ($data['gross_amount'] - $data['deductions']) - $data['tax'];

            $data['total_billing'] = ($data['gross_amount'] - $data['tardiness'] - $data['absences']) + $data['deductions'] + $data['tax'] + $data['thirteenth_month_bonus'];

            if($d->net_pay > 0) {
                $fields = [
                    'sss_er',
                    'sss_ec',
                    'hdmf_er',
                    'philhealth_er',
                    'basic_pay',
                    'ecola',
                    'holiday_ecola',
                    'adjustment_wage',
                    'de_minimis',
                    'thirteenth_month_bonus',
                    'fixed_allowance',
                    'leave_encashment',
                    'incentives',
                    'adjustment_income',
                    'absences',
                    'tardiness',
                    'sss_ee',
                    'hdmf_ee',
                    'philhealth_ee',
                    'sss_loan',
                    'hdmf_loan',
                    'client_charges',
                    'membership_fee',
                    'notarial_fee',
                    'coop_loan',
                    'coop_share',
                    'unpaid_leave',
                    'adjustment_deduction',
                    'gross_amount',
                    'deductions',
                    'net_pay',
                    'other_deductions',
                    'other_incomes'
                ];

                foreach($fields as $k) {
                    $data[$k] = $d->{$k};
                }
            }

//            dd($data);


            PayrollSummary::create($data);

            if($data['membership_fee'] == 100) {
                $compensation = Compensations::where('user_id',$d->user_id)->first();
                $compensation->membership_fee = $compensation->membership_fee + $membershipFee;
                $compensation->save();
            }

            if($data['notarial_fee'] == 50) {
                $compensation = Compensations::where('user_id',$d->user_id)->first();
                $compensation->notarial_fee = $compensation->notarial_fee + $notarialFee;
                $compensation->save();
            }


            $d->hold_status = $holdStatus;
            $d->save();
        }


        $payrollCycle->status = 'Processed';
        $payrollCycle->save();


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Payroll Wizard',
            'action' => 'Edit',
            'description' => "Payroll Cycle: {$payrollCycle->payroll_code} has been Processed",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Payroll Cycle successfully processed!");
        return redirect("/admin/payroll/wizard/payroll_members/{$id}");
    }

    public function processed(Request $request, $id)
    {
        $data['pageHeader'] = "Payroll";

        $data['payrollCycle'] = PayrollCycles::find($id);
        $data['payrollCycleId'] = $id;
        $data['dtr'] = PayrollSummary::leftJoin('profiles', 'profiles.user_id', '=', 'payroll_summary.user_id')->where('payroll_cycle_id', $id)->orderBy('last_name')->get();

        // Update 02/28/2018 include hold status = YES
//        $data['dtr'] = PayrollDtr::where('payroll_cycle_id', $id)->where('hold_status', 'N')->orderBy('id_number')->get();


        return view('admin.payroll.processed', $data);
    }

    public function processed_payroll(Request $request, $id, $user_id)
    {
        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = "Payroll";

        $data['payrollCycle'] = PayrollCycles::find($id);
        $data['payrollCycleId'] = $id;

        $data['user'] = User::find($user_id);

        $data['payroll'] = PayrollSummary::where('payroll_cycle_id', $id)->where('user_id', $user_id)->first();

        $data['dtr'] = $dtr = PayrollDtr::where('payroll_cycle_id', $id)->where('user_id', $user_id)->first();


        $ytdPayrollCycles = PayrollCycles::where(['payroll_group_id' => $data['user']->Profile->Compensation->PayrollGroup->id, 'effective_year' => $data['payrollCycle']->effective_year])->pluck('id');
        $data['ytdPayrollSummary'] = PayrollSummary::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $user_id)->get();

        if($dtr->User->Profile->Compensation->rate_type == 'Monthly') {
            $data['payrollComputation'] = $payroll = new \App\ComputePayroll($dtr, ['daily_rate' => $dtr->User->Profile->Compensation->monthly_rate / 26, 'monthly_rate' => $dtr->User->Profile->Compensation->monthly_rate / 2, 'rate_type' => 'Monthly']);
        } else {
            if($dtr->User->Profile->Compensation->daily_category == 'Location') {
                $data['payrollComputation'] = $payroll = new \App\ComputePayroll($dtr, $dtr->User->Profile->Compensation->DailyRate->rate);
            } else {
                $data['payrollComputation'] = $payroll = new \App\ComputePayroll($dtr, $dtr->User->Profile->Compensation->daily_rate);
            }
        }


        $data['ytd'] = PayrollSummary::whereIn('payroll_cycle_id', PayrollCycles::where('status', 'Processed')->whereYear('payroll_date', date('Y'))->pluck('id'))->where('user_id', $dtr->User->id)->get();


        return view('admin.payroll.payslip', $data);
    }

    public function payslip_edit(Request $request, $id, $user_id)
    {
        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = "Payroll";

        $payrollCycle = $data['payrollCycle'] = PayrollCycles::find($id);
        $data['payrollCycleId'] = $id;

        $payrollCode = explode('-', $payrollCycle->payroll_code);

        $data['user'] = User::find($user_id);

        $data['dtr'] = $dtr = PayrollDtr::where('payroll_cycle_id', $id)->where('user_id', $user_id)->first();

        if($dtr->User->Profile->Compensation->rate_type == 'Monthly') {
            $data['payrollComputation'] = $payroll = new \App\ComputePayroll($dtr, ['daily_rate' => $dtr->User->Profile->Compensation->monthly_rate / 26, 'monthly_rate' => $dtr->User->Profile->Compensation->monthly_rate / 2, 'rate_type' => 'Monthly']);
        } else {
            if($dtr->User->Profile->Compensation->daily_category == 'Location') {
                $data['payrollComputation'] = $payroll = new \App\ComputePayroll($dtr, $dtr->User->Profile->Compensation->DailyRate->rate);
            } else {
                $data['payrollComputation'] = $payroll = new \App\ComputePayroll($dtr, $dtr->User->Profile->Compensation->daily_rate);
            }
        }

        $ytdPayrollCycles = PayrollCycles::where(['payroll_group_id' => $data['user']->Profile->Compensation->PayrollGroup->id, 'effective_year' => $data['payrollCycle']->effective_year, 'status' => 'Processed'])->pluck('id');
        $data['ytdPayrollSummary'] = PayrollSummary::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $data['user']->id)->get();

        if($dtr->net_pay > 0) {
            $data['payroll'] = (object) $dtr;
        } else {


            $accumulatedHours = 0;
            $totalLeaves = 0;
            $unpaidLeave = 0;
            if(!empty($data['ytdPayrollSummary'])) {
                $ytdDtr = PayrollDtr::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $data['user']->id)->get();
                $accumulatedHours = $ytdDtr->sum('vl') + $ytdDtr->sum('sl') + $ytdDtr->sum('el') + $ytdDtr->sum('spl') + $ytdDtr->sum('ml') + $ytdDtr->sum('pl') + $ytdDtr->sum('pto') + $ytdDtr->sum('bl') + $ytdDtr->sum('cl');
                $leaves = $payroll->vl + $payroll->sl + $payroll->el + $payroll->spl + $payroll->ml + $payroll->pl + $payroll->pto + $payroll->bl + $payroll->cl;
//            dd("(5 * 8) - $accumulatedHours - $leaves");
//            print_r($ytdPayrollCycles);
//            echo "(5 * 8) - $accumulatedHours - $leaves";
//            dd("{$data['user']->Profile->Compensation->PayrollGroup->id} {$data['payrollCycle']->effective_year}");
                if($leaves > 0) {
                    $bal = (5 * 8) - $accumulatedHours;
                    if($bal > 0) {

                        $bal -= $leaves;
                        if($bal < 0) {
                            $unpaidLeave = abs($bal) * $payroll->hourlyRate();
                        }

                    } else {
                        $unpaidLeave = $leaves * $payroll->hourlyRate();
                    }
                }
            }


            $loans = LoanPaymentSchedules::where(['pay_period' => $payrollCode[1], 'user_id' => $user_id]);

            $sssLoan = 0;
            $hdmfLoan = 0;
            $coopLoan = 0;
            foreach($loans->get() as $lps) {
                if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'SSS') {
                    $sssLoan += $lps->Loan->amortization;
                }

                if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'HDMF') {
                    $hdmfLoan += $lps->Loan->amortization;
                }

                if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && in_array($lps->Loan->loan_type_id, ['1', '2', '3', '4'])) {
                    $coopLoan += $lps->Loan->amortization;
                }
            }

            if($payrollCycle->cycle == 'B') {
                $payrollCycleA = PayrollCycles::where([
                    'payroll_group_id' => $payrollCycle->payroll_group_id,
                    'effective_year' => $payrollCycle->effective_year,
                    'effective_month' => $payrollCycle->effective_month,
                    'cycle' => 'A',
                    'status' => 'Processed'
                ])->first();
                // dd([
                //     'payroll_group_id' => $payrollCycle->payroll_group_id,
                //     'effective_year' => $payrollCycle->effective_year,
                //     'effective_month' => $payrollCycle->effective_month,
                //     'cycle' => 'A'
                // ]);

                $payrollSummaryA = PayrollSummary::where('payroll_cycle_id', $payrollCycleA->id)->where('user_id', $data['user']->id)->first();
                $basicPayA = !empty($payrollSummaryA->basic_pay) ? $payrollSummaryA->basic_pay : 0;
            }

            $basicPayB = 0;
            if($payrollCycle->cycle == 'A') {
                $effectiveDate = Carbon::createFromFormat('Y-m-d', "{$payrollCycle->effective_year}-{$payrollCycle->effective_month}-01")->subMonth(1);
                $payrollCycleB = PayrollCycles::where([
                    'payroll_group_id' => $payrollCycle->payroll_group_id,
                    'effective_year' => $effectiveDate->format('Y'),
                    'effective_month' => $effectiveDate->format('m'),
                    'cycle' => 'B',
                    'status' => 'Processed'
                ])->first();

                if(empty($payrollCycleB)) {
                    $basicPayB = 0;
                } else {
                    $payrollSummaryB = PayrollSummary::where('payroll_cycle_id', $payrollCycleB->id)->where('user_id', $data['user']->id)->first();
                    $basicPayB = !empty($payrollSummaryB->basic_pay) ? $payrollSummaryB->basic_pay : 0;
                }


            }


            $philhealth_ee = 0;
            $philhealth_er = 0 ;
            if($payrollCycle->cycle == 'A' && $dtr->Profile->Compensation->compute_philhealth == 'Y') {
                $philHealthContribution = PhilHealthContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first();
                if($philHealthContribution->id == 3) {
                    $monthlyPremium = ($payroll->basicPay() + $basicPayB) * ($philHealthContribution->premium_rate / 100);
                } elseif($philHealthContribution->id == 1) {
                    $monthlyPremium = 275;
                } elseif($philHealthContribution->id == 2) {
                    $monthlyPremium = 1100;
                }

                $philhealth_ee = $philhealth_er = $monthlyPremium / 2;
//            dd($payrollCycle);
            }

            $thirteenthMonth = 0;
            if($payrollCycle->calculate_13th_month == 'Y' && EmploymentDetails::where('user_id', $data['user']->id)->first()->position_id != 1) {
                $lastDecember = PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)) - 1)->whereMonth('payroll_date', 12)->get();
//            $toNovemberThisYear = PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)))->whereMonth('payroll_date', 11)->get();
                $toNovemberThisYear = \App\PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereDate('payroll_date', '>=', date('Y-01-01', strtotime($payrollCycle->payroll_date)))->whereDate('payroll_date', '<=', date('Y-11-30', strtotime($payrollCycle->payroll_date)))->get();

                foreach($lastDecember as $pc) {
                    $pastSummary = PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $data['user']->id])->first();
                    $thirteenthMonth += !empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0;
                }

                foreach($toNovemberThisYear as $pc) {
                    $pastSummary = PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $data['user']->id])->first();
                    $thirteenthMonth +=  (!empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0);
                }
            }

            $membershipFee = 0;
            if(OtherPayments::where('user_id', $dtr->user_id)->where('transaction_type', 'MF')->sum('total_amount_paid') < Keyval::where('key', 'membership_fee')->first()->value) {
                $membershipFee = Keyval::where('key', 'membership_fee')->first()->value / 2;
            }

            $notarialFee = 0;
            if(OtherPayments::where('user_id', $dtr->user_id)->where('transaction_type', 'NF')->sum('total_amount_paid') < Keyval::where('key', 'notarial_fee')->first()->value) {
                $notarialFee = Keyval::where('key', 'notarial_fee')->first()->value / 2;
            }



            $payroll = [
                'payroll_cycle_id' => $id,
                'user_id'=> $dtr->user_id,
                'basic_pay' => $payroll->basicPay(),
                'ecola' => ($dtr->Profile->Compensation->daily_category == 'Location') ? ($payroll->rg() / 8) * $dtr->Profile->Compensation->DailyRate->ecola : 0.00,
                'overtime_pay' => $payroll->OTPay(),
                'holiday_pay' => $payroll->HDPay(),
                'night_differential' => $payroll->NDPay(),
                'holiday_ecola' => $dtr->Profile->Compensation->daily_category == 'Location' ? ($payroll->leg / 8) * $dtr->Profile->Compensation->DailyRate->ecola : 0.00,
                'adjustment_wage' => $dtr->nwage,
                'adjustment_income' => $dtr->adji,
                'adjustment_deduction' => $dtr->adjd,
                'de_minimis' => $dtr->Profile->Compensation->de_minimis,
                'thirteenth_month_bonus' => $thirteenthMonth,
                'thirteenth_month_pro_rated' => EmploymentDetails::where('user_id', $data['user']->id)->first()->position_id != 1  ? $payroll->basicPay() / 12 : 0,
                'fixed_allowance' => $dtr->Profile->Compensation->fixed_allowance / 2,
                'leave_encashment' => 0, // TODO
                'incentives' => $dtr->incentive,
                'coop_savings' => 0, // Omitted
                'sss_ec' => ($payrollCycle->cycle == 'A' && $dtr->Profile->Compensation->compute_sss == 'Y') ? SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ec : 0,
                'sss_er' => ($payrollCycle->cycle == 'A' && $dtr->Profile->Compensation->compute_sss == 'Y') ? SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->er : 0,
                'hdmf_er' => ($payrollCycle->cycle == 'A' && $dtr->Profile->Compensation->compute_hdmf == 'Y') ? HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employer_share : 0,
                'philhealth_er' => $philhealth_er,
                'sss_ee' => ($payrollCycle->cycle == 'A' && $dtr->Profile->Compensation->compute_sss == 'Y') ? SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ee : 0,
                'hdmf_ee' => ($payrollCycle->cycle == 'A' && $dtr->Profile->Compensation->compute_hdmf == 'Y') ? HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employee_share + $data['user']->Profile->hdmf_additional : 0,
                'philhealth_ee' => $philhealth_ee,
                'tardiness' => ($payroll->hourlyRate() / 60) * ($payroll->lt + $payroll->ut),
                'absences' => ($payroll->hourlyRate() / 60) * $payroll->abs,
                'sss_loan' => $sssLoan,
                'hdmf_loan' => $hdmfLoan,
                'client_charges' => $dtr->client_charges,
                'membership_fee' => $membershipFee,
                'notarial_fee' => $notarialFee,
                'coop_loan' => $coopLoan,
                'coop_share' => $dtr->Profile->coop_share,
                'unpaid_leave' => $unpaidLeave
            ];

//        dd($payroll);

            if($unpaidLeave > 0) {
                $incomeTx = $payroll['basic_pay'] + $data['payrollComputation']->rd() + $data['payrollComputation']->spe() + $data['payrollComputation']->sperd() + $data['payrollComputation']->leg() + $data['payrollComputation']->legrd() + $payroll['overtime_pay'] + $payroll['night_differential'] + $payroll['ecola'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
            } else {
                $incomeTx = $payroll['basic_pay'] + $data['payrollComputation']->rd() + $data['payrollComputation']->spe() + $data['payrollComputation']->sperd() + $data['payrollComputation']->leg() + $data['payrollComputation']->legrd() + $payroll['overtime_pay'] + $payroll['night_differential'] + $data['payrollComputation']->vl() + $data['payrollComputation']->sl() + $data['payrollComputation']->el() + $data['payrollComputation']->spl() + $data['payrollComputation']->ml() + $data['payrollComputation']->pl() + $data['payrollComputation']->pto() + $data['payrollComputation']->bl() + $data['payrollComputation']->cl() + $payroll['ecola'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
            }

//        $incomeTx = $payroll['basic_pay'] + $data['payrollComputation']->rd() + $data['payrollComputation']->spe() + $data['payrollComputation']->sperd() + $data['payrollComputation']->leg() + $data['payrollComputation']->legrd() + $data['payrollComputation']->vl() + $data['payrollComputation']->sl() + $data['payrollComputation']->el() + $data['payrollComputation']->spl() + $data['payrollComputation']->ml() + $data['payrollComputation']->pl() + $data['payrollComputation']->pto() + $data['payrollComputation']->bl() + $data['payrollComputation']->cl() + $payroll['ecola'] + $payroll['overtime_pay'] + $payroll['holiday_pay'] + $payroll['night_differential'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
            $incomeNtx = $payroll['de_minimis'] + $payroll['thirteenth_month_bonus'] + $payroll['fixed_allowance'] + $payroll['leave_encashment'] + $payroll['incentives'] + $payroll['adjustment_income'];
            $deductionTx = $payroll['sss_ee'] + $payroll['hdmf_ee'] + $payroll['philhealth_ee'] + $payroll['tardiness'] + $payroll['absences'];
            $deduction = $payroll['sss_loan'] + $payroll['hdmf_loan'] + $payroll['client_charges'] + $payroll['membership_fee'] + $payroll['notarial_fee'] + $payroll['coop_loan'] + $payroll['coop_share'] + $payroll['adjustment_deduction'];
//        dd($payroll);
            $payroll['tax'] = 0;
            if(Compensations::where('user_id', $dtr->user_id)->first()->deduct_withholding == 'Y') {
                $taxTable = new \App\TaxTable($dtr->User->Profile->Compensation->TaxType->title, $incomeTx);
                $payroll['tax'] = $taxTable->taxDue();
            }

            $payroll['gross_amount'] = $incomeTx + $incomeNtx;
            $payroll['deductions'] = $deductionTx + $deduction + $unpaidLeave;
            $payroll['net_pay'] = ($payroll['gross_amount'] - $payroll['deductions']) - $payroll['tax'];

            $data['payroll'] = (object) $payroll;
        }


        return view('admin.payroll.payslip_edit', $data);
    }

    public function payslip_edit_post(Request $request, $id, $user_id)
    {
//        $payrollDtr = PayrollDtr::where('payroll_cycle_id', $id)->where('user_id', $user_id)->first();

//        dd($request->all());

        PayrollOthers::where(['payroll_cycle_id' => $id, 'user_id' => $user_id])->delete();

        $otherIncomes = 0;
        if(isset($request['other_income'])) {
            foreach($request['other_income'] as $otherIncome) {
                PayrollOthers::create([
                    'payroll_cycle_id' => $id,
                    'user_id' => $user_id,
                    'type' => 'Income',
                    'title' => $otherIncome['title'],
                    'amount' => str_replace(',', '', $otherIncome['amount'])
                ]);

                $otherIncomes += str_replace(',', '', $otherIncome['amount']);
            }
        }

        $otherDeductions = 0;
        if(isset($request['other_deduction'])) {
            foreach($request['other_deduction'] as $otherDeduction) {
                PayrollOthers::create([
                    'payroll_cycle_id' => $id,
                    'user_id' => $user_id,
                    'type' => 'Deduction',
                    'title' => $otherDeduction['title'],
                    'amount' => str_replace(',', '', $otherDeduction['amount'])
                ]);

                $otherDeductions += str_replace(',', '', $otherDeduction['amount']);
            }
        }

        $summary = $request->except('_token', 'other_income', 'other_deduction');

        $summary['other_incomes'] = $otherIncomes;
        $summary['other_deductions'] = $otherDeductions;

        foreach($summary as $k => $s) {
            $summary[$k] = str_replace(',', '', $s);
        }

        PayrollDtr::where('payroll_cycle_id', $id)->where('user_id', $user_id)->update($summary);


//        $payrollDtr->save($summary);


        $request->session()->flash('popSuccess', "Payslip update successful!");
        return redirect("/admin/payroll/wizard/payroll_members/{$id}/payslip/{$user_id}/edit");
    }

    public function payslip_preview(Request $request, $id, $user_id)
    {
        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = "Payroll";

        $payrollCycle = $data['payrollCycle'] = PayrollCycles::find($id);
        $data['payrollCycleId'] = $id;

        $payrollCode = explode('-', $payrollCycle->payroll_code);

        $data['user'] = User::find($user_id);

        $data['payroll'] = PayrollSummary::where('payroll_cycle_id', $id)->where('user_id', $user_id)->first();
//        dd($data['payroll']);

        $data['dtr'] = $dtr = PayrollDtr::where('payroll_cycle_id', $id)->where('user_id', $user_id)->first();

        if($dtr->User->Profile->Compensation->rate_type == 'Monthly') {
            $data['payrollComputation'] = $payroll = new \App\ComputePayroll($dtr, ['daily_rate' => $dtr->User->Profile->Compensation->monthly_rate / 26, 'monthly_rate' => $dtr->User->Profile->Compensation->monthly_rate / 2, 'rate_type' => 'Monthly']);
        } else {
            if($dtr->User->Profile->Compensation->daily_category == 'Location') {
                $data['payrollComputation'] = $payroll = new \App\ComputePayroll($dtr, $dtr->User->Profile->Compensation->DailyRate->rate);
            } else {
                $data['payrollComputation'] = $payroll = new \App\ComputePayroll($dtr, $dtr->User->Profile->Compensation->daily_rate);
            }
        }

        $ytdPayrollCycles = PayrollCycles::where(['payroll_group_id' => $data['user']->Profile->Compensation->PayrollGroup->id, 'effective_year' => $data['payrollCycle']->effective_year, 'status' => 'Processed'])->pluck('id');
        $data['ytdPayrollSummary'] = PayrollSummary::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $data['user']->id)->get();

        $accumulatedHours = 0;
        $totalLeaves = 0;
        $unpaidLeave = 0;
        if(!empty($data['ytdPayrollSummary'])) {
            $ytdDtr = PayrollDtr::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $data['user']->id)->get();
            $accumulatedHours = $ytdDtr->sum('vl') + $ytdDtr->sum('sl') + $ytdDtr->sum('el') + $ytdDtr->sum('spl') + $ytdDtr->sum('ml') + $ytdDtr->sum('pl') + $ytdDtr->sum('pto') + $ytdDtr->sum('bl') + $ytdDtr->sum('cl');
            $leaves = $payroll->vl + $payroll->sl + $payroll->el + $payroll->spl + $payroll->ml + $payroll->pl + $payroll->pto + $payroll->bl + $payroll->cl;
//            dd("(5 * 8) - $accumulatedHours - $leaves");
//            print_r($ytdPayrollCycles);
//            echo "(5 * 8) - $accumulatedHours - $leaves";
//            dd("{$data['user']->Profile->Compensation->PayrollGroup->id} {$data['payrollCycle']->effective_year}");
            if($leaves > 0) {
                $bal = (5 * 8) - $accumulatedHours;
                if($bal > 0) {

                    $bal -= $leaves;
                    if($bal < 0) {
                        $unpaidLeave = abs($bal) * $payroll->hourlyRate();
                    }

                } else {
                    $unpaidLeave = $leaves * $payroll->hourlyRate();
                }
            }
        }


        $loans = LoanPaymentSchedules::where(['pay_period' => $payrollCode[1], 'user_id' => $user_id]);

        $sssLoan = 0;
        $hdmfLoan = 0;
        $coopLoan = 0;
        foreach($loans->get() as $lps) {
            if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'SSS') {
                $sssLoan += $lps->Loan->amortization;
            }

            if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'HDMF') {
                $hdmfLoan += $lps->Loan->amortization;
            }

            if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && in_array($lps->Loan->loan_type_id, ['1', '2', '3', '4'])) {
                $coopLoan += $lps->Loan->amortization;
            }
        }

        if($payrollCycle->cycle == 'B') {
            $payrollCycleA = PayrollCycles::where([
                'payroll_group_id' => $payrollCycle->payroll_group_id,
                'effective_year' => $payrollCycle->effective_year,
                'effective_month' => $payrollCycle->effective_month,
                'cycle' => 'A',
                'status' => 'Processed'
            ])->first();
            // dd([
            //     'payroll_group_id' => $payrollCycle->payroll_group_id,
            //     'effective_year' => $payrollCycle->effective_year,
            //     'effective_month' => $payrollCycle->effective_month,
            //     'cycle' => 'A'
            // ]);

            $payrollSummaryA = PayrollSummary::where('payroll_cycle_id', $payrollCycleA->id)->where('user_id', $data['user']->id)->first();
            $basicPayA = !empty($payrollSummaryA->basic_pay) ? $payrollSummaryA->basic_pay : 0;
        }

        $basicPayB = 0;
        if($payrollCycle->cycle == 'A') {
            $effectiveDate = Carbon::createFromFormat('Y-m-d', "{$payrollCycle->effective_year}-{$payrollCycle->effective_month}-01")->subMonth(1);
            $payrollCycleB = PayrollCycles::where([
                'payroll_group_id' => $payrollCycle->payroll_group_id,
                'effective_year' => $effectiveDate->format('Y'),
                'effective_month' => $effectiveDate->format('m'),
                'cycle' => 'B',
                'status' => 'Processed'
            ])->first();

            if(empty($payrollCycleB)) {
                $basicPayB = 0;
            } else {
                $payrollSummaryB = PayrollSummary::where('payroll_cycle_id', $payrollCycleB->id)->where('user_id', $data['user']->id)->first();
                $basicPayB = !empty($payrollSummaryB->basic_pay) ? $payrollSummaryB->basic_pay : 0;
            }


        }


        $philhealth_ee = 0;
        $philhealth_er = 0 ;
        if($payrollCycle->cycle == 'A' && $dtr->Profile->Compensation->compute_philhealth == 'Y') {
            $philHealthContribution = PhilHealthContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first();
            if($philHealthContribution->id == 3) {
                $monthlyPremium = ($payroll->basicPay() + $basicPayB) * ($philHealthContribution->premium_rate / 100);
            } elseif($philHealthContribution->id == 1) {
                $monthlyPremium = 275;
            } elseif($philHealthContribution->id == 2) {
                $monthlyPremium = 1100;
            }

            $philhealth_ee = $philhealth_er = $monthlyPremium / 2;
//            dd($payrollCycle);
        }

        $thirteenthMonth = 0;
        if($payrollCycle->calculate_13th_month == 'Y' && EmploymentDetails::where('user_id', $data['user']->id)->first()->position_id != 1) {
            $lastDecember = PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)) - 1)->whereMonth('payroll_date', 12)->get();
//            $toNovemberThisYear = PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)))->whereMonth('payroll_date', 11)->get();
            $toNovemberThisYear = \App\PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereDate('payroll_date', '>=', date('Y-01-01', strtotime($payrollCycle->payroll_date)))->whereDate('payroll_date', '<=', date('Y-11-30', strtotime($payrollCycle->payroll_date)))->get();

            foreach($lastDecember as $pc) {
                $pastSummary = PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $data['user']->id])->first();
                $thirteenthMonth += !empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0;
            }

            foreach($toNovemberThisYear as $pc) {
                $pastSummary = PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $data['user']->id])->first();
                $thirteenthMonth +=  (!empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0);
            }
        }

        $membershipFee = 0;
        if(OtherPayments::where('user_id', $dtr->user_id)->where('transaction_type', 'MF')->sum('total_amount_paid') < Keyval::where('key', 'membership_fee')->first()->value) {
            $membershipFee = Keyval::where('key', 'membership_fee')->first()->value / 2;
        }

        $notarialFee = 0;
        if(OtherPayments::where('user_id', $dtr->user_id)->where('transaction_type', 'NF')->sum('total_amount_paid') < Keyval::where('key', 'notarial_fee')->first()->value) {
            $notarialFee = Keyval::where('key', 'notarial_fee')->first()->value / 2;
        }



        $payroll = [
            'payroll_cycle_id' => $id,
            'user_id'=> $dtr->user_id,
            'basic_pay' => $payroll->basicPay(),
            'ecola' => ($dtr->Profile->Compensation->daily_category == 'Location') ? ($payroll->rg() / 8) * $dtr->Profile->Compensation->DailyRate->ecola : 0.00,
            'overtime_pay' => $payroll->OTPay(),
            'holiday_pay' => $payroll->HDPay(),
            'night_differential' => $payroll->NDPay(),
            'holiday_ecola' => $dtr->Profile->Compensation->daily_category == 'Location' ? ($payroll->leg / 8) * $dtr->Profile->Compensation->DailyRate->ecola : 0.00,
            'adjustment_wage' => $dtr->nwage,
            'adjustment_income' => $dtr->adji,
            'adjustment_deduction' => $dtr->adjd,
            'de_minimis' => $dtr->Profile->Compensation->de_minimis,
            'thirteenth_month_bonus' => $thirteenthMonth,
            'thirteenth_month_pro_rated' => EmploymentDetails::where('user_id', $data['user']->id)->first()->position_id != 1 ? $payroll->basicPay() / 12 : 0,
            'fixed_allowance' => $dtr->Profile->Compensation->fixed_allowance / 2,
            'leave_encashment' => 0, // TODO
            'incentives' => $dtr->incentive,
            'coop_savings' => 0, // Omitted
            'sss_ec' => ($payrollCycle->cycle == 'A' && $dtr->Profile->Compensation->compute_sss == 'Y') ? SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ec : 0,
            'sss_er' => ($payrollCycle->cycle == 'A' && $dtr->Profile->Compensation->compute_sss == 'Y') ? SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->er : 0,
            'hdmf_er' => ($payrollCycle->cycle == 'A' && $dtr->Profile->Compensation->compute_hdmf == 'Y') ? HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employer_share : 0,
            'philhealth_er' => $philhealth_er,
            'sss_ee' => ($payrollCycle->cycle == 'A' && $dtr->Profile->Compensation->compute_sss == 'Y') ? SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ee : 0,
            'hdmf_ee' => ($payrollCycle->cycle == 'A' && $dtr->Profile->Compensation->compute_hdmf == 'Y') ? HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employee_share + $data['user']->Profile->hdmf_additional : 0,
            'philhealth_ee' => $philhealth_ee,
            'tardiness' => ($payroll->hourlyRate() / 60) * ($payroll->lt + $payroll->ut),
            'absences' => ($payroll->hourlyRate() / 60) * $payroll->abs,
            'sss_loan' => $sssLoan,
            'hdmf_loan' => $hdmfLoan,
            'client_charges' => $dtr->client_charges,
            'membership_fee' => $membershipFee,
            'notarial_fee' => $notarialFee,
            'coop_loan' => $coopLoan,
            'coop_share' => $dtr->Profile->coop_share,
            'unpaid_leave' => $unpaidLeave
        ];

//        dd($payroll);

        if($unpaidLeave > 0) {
            $incomeTx = $payroll['basic_pay'] + $data['payrollComputation']->rd() + $data['payrollComputation']->spe() + $data['payrollComputation']->sperd() + $data['payrollComputation']->leg() + $data['payrollComputation']->legrd() + $payroll['overtime_pay'] + $payroll['night_differential'] + $payroll['ecola'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
        } else {
            $incomeTx = $payroll['basic_pay'] + $data['payrollComputation']->rd() + $data['payrollComputation']->spe() + $data['payrollComputation']->sperd() + $data['payrollComputation']->leg() + $data['payrollComputation']->legrd() + $payroll['overtime_pay'] + $payroll['night_differential'] + $data['payrollComputation']->vl() + $data['payrollComputation']->sl() + $data['payrollComputation']->el() + $data['payrollComputation']->spl() + $data['payrollComputation']->ml() + $data['payrollComputation']->pl() + $data['payrollComputation']->pto() + $data['payrollComputation']->bl() + $data['payrollComputation']->cl() + $payroll['ecola'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
        }

//        $incomeTx = $payroll['basic_pay'] + $data['payrollComputation']->rd() + $data['payrollComputation']->spe() + $data['payrollComputation']->sperd() + $data['payrollComputation']->leg() + $data['payrollComputation']->legrd() + $data['payrollComputation']->vl() + $data['payrollComputation']->sl() + $data['payrollComputation']->el() + $data['payrollComputation']->spl() + $data['payrollComputation']->ml() + $data['payrollComputation']->pl() + $data['payrollComputation']->pto() + $data['payrollComputation']->bl() + $data['payrollComputation']->cl() + $payroll['ecola'] + $payroll['overtime_pay'] + $payroll['holiday_pay'] + $payroll['night_differential'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
        $incomeNtx = $payroll['de_minimis'] + $payroll['thirteenth_month_bonus'] + $payroll['fixed_allowance'] + $payroll['leave_encashment'] + $payroll['incentives'] + $payroll['adjustment_income'];
        $deductionTx = $payroll['sss_ee'] + $payroll['hdmf_ee'] + $payroll['philhealth_ee'] + $payroll['tardiness'] + $payroll['absences'];
        $deduction = $payroll['sss_loan'] + $payroll['hdmf_loan'] + $payroll['client_charges'] + $payroll['membership_fee'] + $payroll['notarial_fee'] + $payroll['coop_loan'] + $payroll['coop_share'] + $payroll['adjustment_deduction'];
//        dd($payroll);
        $payroll['tax'] = 0;
        if(Compensations::where('user_id', $dtr->user_id)->first()->deduct_withholding == 'Y') {
            $taxTable = new \App\TaxTable($dtr->User->Profile->Compensation->TaxType->title, $incomeTx);
            $payroll['tax'] = $taxTable->taxDue();
        }

        $payroll['gross_amount'] = $incomeTx + $incomeNtx;
        $payroll['deductions'] = $deductionTx + $deduction + $unpaidLeave;
        $payroll['net_pay'] = ($payroll['gross_amount'] - $payroll['deductions']) - $payroll['tax'];

        $data['payroll'] = (object) $payroll;

        if($dtr->net_pay > 0) {
            $data['payroll'] = (object) $dtr;
        }
//        dd($payroll);
        return view('admin.payroll.payslip_preview', $data);
    }

    public function reset(Request $request, $payrollCycleId)
    {
        PayrollDtr::where('payroll_cycle_id', $payrollCycleId)->delete();
        PayrollDtrDraft::where('payroll_cycle_id', $payrollCycleId)->delete();

        PayrollCycles::where('id', $payrollCycleId)->update(['status' => 'Open']);

        $request->session()->flash('popSuccess', "Payroll Cycle reset successful!");
        return redirect("/admin/payroll/wizard/payroll_members/{$payrollCycleId}");
    }

    public function exportOpen(Request $request, $payrollCycleId)
    {
        $payrollCycle = PayrollCycles::find($payrollCycleId);
        $dtr = PayrollDtr::where('payroll_cycle_id', $payrollCycleId)->get();

        if($payrollCycle->cycle == 'B') {
            $payrollCode = substr($payrollCycle->payroll_code, 0, -1). 'A';
            if(!PayrollCycles::where('payroll_code', $payrollCode)->where('status', 'Processed')->exists()) {
                $request->session()->flash('popError', "Payroll Code <strong>{$payrollCode}</strong> must be processed first in order to process this payroll cycle!");
                return redirect()->back();
            }
        }

        Excel::create("Payroll Open ({$payrollCycle->payroll_code})", function ($excel) use ($payrollCycle, $dtr) {


            // Set the title
            $excel->setTitle("Payroll Open ({$payrollCycle->payroll_code})");

            $excel->sheet($payrollCycle->payroll_code, function ($sheet) use ($payrollCycle, $dtr) {

                $headers = array(
                    'EMPLOYEE ID NUMBER',
                    "EMPLOYEE NAME",
                    'RATE TYPE',
                    'BASIC PAY',
                    'DAYS',
                    'RD',
                    'DAYS',
                    'SPE',
                    'DAYS',
                    'SPERD',
                    'DAYS',
                    'LEG',
                    'DAYS',
                    'LEGRD',
                    'DAYS',
                    'OT',
                    'DAYS',
                    'ND',
                    'DAYS',
                    'VL',
                    'DAYS',
                    'SL',
                    'DAYS',
                    'EL',
                    'DAYS',
                    'SPL',
                    'DAYS',
                    'ML',
                    'DAYS',
                    'PL',
                    'DAYS',
                    'PTO',
                    'DAYS',
                    'BL',
                    'DAYS',
                    'CL',
                    'DAYS',
                    'ECOLA',
                    'HECOLA',
                    'NWAGE',
                    'DE MINIMIS',
                    '13TH MONTH',
                    'FIXED ALLOWANCE',
                    'LEAVE',
                    'INCENTIVES',
                    'ADJUSTMENT INCOME'
                );

                $headersIncome = [];
                foreach($dtr as $d) {
                    $otherIncomes = PayrollOthers::where([
                        'payroll_cycle_id' => $d->payroll_cycle_id,
                        'user_id' => $d->user_id,
                        'type' => 'Income'
                    ])->get();

                    foreach($otherIncomes as $income) {
                        if(!in_array($income->title, $headersIncome)) {
                            $headersIncome[] = $income->title;
                            $headers[] = $income->title;
                        }
                    }
                }


                $headers[] = strtoupper('Absences');
                $headers[] = strtoupper('Tardiness');
                $headers[] = strtoupper('SSS EE');
                $headers[] = strtoupper('HDMF EE');
                $headers[] = strtoupper('PhilHealth EE');
                $headers[] = strtoupper('SSS Loan');
                $headers[] = strtoupper('HDMF Loan');
                $headers[] = strtoupper('Client Charges');
                $headers[] = strtoupper('Membership Fee');
                $headers[] = strtoupper('Notarial Fee');
                $headers[] = strtoupper('Coop Loan');
                $headers[] = strtoupper('Coop Share');
                $headers[] = strtoupper('Unpaid Leave');
                $headers[] = 'ADJUSTMENT DEDUCTION';



                $headersDeduction = [];
                foreach($dtr as $d) {
                    $otherDeductions = PayrollOthers::where([
                        'payroll_cycle_id' => $d->payroll_cycle_id,
                        'user_id' => $d->user_id,
                        'type' => 'Deduction'
                    ])->get();

                    foreach($otherDeductions as $deduction) {
                        if(!in_array($deduction->title, $headersDeduction)) {
                            $headersDeduction[] = $deduction->title;
                            $headers[] = $deduction->title;
                        }
                    }
                }

                $headers[] = 'GROSS AMOUNT';
                $headers[] = 'DEDUCTIONS';
                $headers[] = 'TAX';
                $headers[] = 'NET PAY';

                $sheet->row(1, $headers);

                $x = 2;
                foreach($dtr as $d) {
                    $user = $d->User;
                    $payrollCode = explode('-', $payrollCycle->payroll_code);
                    $user_id = $user->id;
//                                $payroll = \App\PayrollSummary::where(['payroll_cycle_id' => $d->payroll_cycle_id, 'user_id' => $d->User->id])->first();

                    if($d->User->Profile->Compensation->rate_type == 'Monthly') {
                        $payrollComputation = $payroll = new \App\ComputePayroll($d, ['daily_rate' => $d->User->Profile->Compensation->monthly_rate / 26, 'monthly_rate' => $d->User->Profile->Compensation->monthly_rate / 2, 'rate_type' => 'Monthly']);
                    } else {
                        if($d->User->Profile->Compensation->daily_category == 'Location') {
                            $payrollComputation = $payroll = new \App\ComputePayroll($d, $d->User->Profile->Compensation->DailyRate->rate);
                        } else {
                            $payrollComputation = $payroll = new \App\ComputePayroll($d, $d->User->Profile->Compensation->daily_rate);
                        }
                    }

//                                dd($payroll);

                    $ytdPayrollCycles = \App\PayrollCycles::where(['payroll_group_id' => $user->Profile->Compensation->PayrollGroup->id, 'effective_year' => $payrollCycle->effective_year, 'status' => 'Processed'])->pluck('id');
                    $ytdPayrollSummary = \App\PayrollSummary::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $d->User->id)->get();

                    //        dd(PayrollCycles::where('status', 'Processed')->whereYear('payroll_date', date('Y', strtotime( $data['payrollCycle']->payroll_date)))->pluck('id'));

                    $accumulatedHours = 0;
                    $totalLeaves = 0;
                    $unpaidLeave = 0;
                    if(!empty($ytdPayrollSummary)) {
                        $ytdDtr = \App\PayrollDtr::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $user->id)->get();
                        $accumulatedHours = $ytdDtr->sum('vl') + $ytdDtr->sum('sl') + $ytdDtr->sum('el') + $ytdDtr->sum('spl') + $ytdDtr->sum('ml') + $ytdDtr->sum('pl') + $ytdDtr->sum('pto') + $ytdDtr->sum('bl') + $ytdDtr->sum('cl');
                        $leaves = $payroll->vl + $payroll->sl + $payroll->el + $payroll->spl + $payroll->ml + $payroll->pl + $payroll->pto + $payroll->bl + $payroll->cl;

                        if($leaves > 0) {
                            $bal = (5 * 8) - $accumulatedHours;
                            if($bal > 0) {

                                $bal -= $leaves;
                                if($bal < 0) {
                                    $unpaidLeave = abs($bal)* $payroll->hourlyRate();
                                }

                            } else {
                                $unpaidLeave = $leaves* $payroll->hourlyRate();
                            }
                        }
                    }


                    $loans = \App\LoanPaymentSchedules::where(['pay_period' => $payrollCode[1], 'user_id' => $user_id]);

                    $sssLoan = 0;
                    $hdmfLoan = 0;
                    $coopLoan = 0;
                    foreach($loans->get() as $lps) {
                        if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'SSS') {
                            $sssLoan += $lps->Loan->amortization;
                        }

                        if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'HDMF') {
                            $hdmfLoan += $lps->Loan->amortization;
                        }

                        if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && in_array($lps->Loan->loan_type_id, ['1', '2', '3', '4'])) {
                            $coopLoan += $lps->Loan->amortization;
                        }
                    }

                    if($payrollCycle->cycle == 'B') {
                        $payrollCycleA = \App\PayrollCycles::where([
                            'payroll_group_id' => $payrollCycle->payroll_group_id,
                            'effective_year' => $payrollCycle->effective_year,
                            'effective_month' => $payrollCycle->effective_month,
                            'cycle' => 'A',
                            'status' => 'Processed'
                        ])->first();

                        $payrollSummaryA = \App\PayrollSummary::where('payroll_cycle_id', $payrollCycleA->id)->where('user_id', $d->user_id)->first();
                        $basicPayA = !empty($payrollSummaryA->basic_pay) ? $payrollSummaryA->basic_pay : 0;
                    }

                    $basicPayB = 0;
                    if($payrollCycle->cycle == 'A') {
                        $effectiveDate = Carbon::createFromFormat('Y-m-d', "{$payrollCycle->effective_year}-{$payrollCycle->effective_month}-01")->subMonth(1);
                        $payrollCycleB = PayrollCycles::where([
                            'payroll_group_id' => $payrollCycle->payroll_group_id,
                            'effective_year' => $effectiveDate->format('Y'),
                            'effective_month' => $effectiveDate->format('m'),
                            'cycle' => 'B',
                            'status' => 'Processed'
                        ])->first();

                        if(empty($payrollCycleB)) {
                            $basicPayB = 0;
                        } else {
                            $payrollSummaryB = PayrollSummary::where('payroll_cycle_id', $payrollCycleB->id)->where('user_id', $d->user_id)->first();
                            $basicPayB = !empty($payrollSummaryB->basic_pay) ? $payrollSummaryB->basic_pay : 0;
                        }


                    }

                    $philhealth_ee = 0;
                    $philhealth_er = 0 ;
                    if($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_philhealth == 'Y') {
                        $philHealthContribution = \App\PhilHealthContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first();
                        if($philHealthContribution->id == 3) {
                            $monthlyPremium = ($payroll->basicPay() + $basicPayB) * ($philHealthContribution->premium_rate / 100);
                        } elseif($philHealthContribution->id == 1) {
                            $monthlyPremium = 275;
                        } elseif($philHealthContribution->id == 2) {
                            $monthlyPremium = 1100;
                        }

                        $philhealth_ee = $philhealth_er = $monthlyPremium / 2;
                    }

                    $thirteenthMonth = 0;
                    if($payrollCycle->calculate_13th_month == 'Y' && EmploymentDetails::where('user_id', $user_id)->first()->position_id != 1) {
                        $lastDecember = \App\PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)) - 1)->whereMonth('payroll_date', 12)->get();
                        $toNovemberThisYear = \App\PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)))->whereMonth('payroll_date', 11)->get();

                        foreach($lastDecember as $pc) {
                            $pastSummary = \App\PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $user_id])->first();
                            $thirteenthMonth += !empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0;
                        }

                        foreach($toNovemberThisYear as $pc) {
                            $pastSummary = \App\PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $user_id])->first();
                            $thirteenthMonth += !empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0;
                        }
                    }

                    $membershipFee = 0;
                    if(OtherPayments::where('user_id', $user_id)->where('transaction_type', 'MF')->sum('total_amount_paid') < \App\Keyval::where('key', 'membership_fee')->first()->value) {
                        $membershipFee = \App\Keyval::where('key', 'membership_fee')->first()->value / 2;
                    }

                    $notarialFee = 0;
                    if(OtherPayments::where('user_id', $user_id)->where('transaction_type', 'NF')->sum('total_amount_paid') < \App\Keyval::where('key', 'notarial_fee')->first()->value) {
                        $notarialFee = \App\Keyval::where('key', 'notarial_fee')->first()->value / 2;
                    }


                    $payroll = [
                        'payroll_cycle_id' => $d->payroll_cycle_id,
                        'user_id'=> $d->user_id,
                        'basic_pay' => $payroll->basicPay(),
                        'ecola' => ($d->Profile->Compensation->daily_category == 'Location') ? ($payroll->rg() / 8) * $d->Profile->Compensation->DailyRate->ecola : 0.00,
                        'overtime_pay' => $payroll->OTPay(),
                        'holiday_pay' => $payroll->HDPay(),
                        'night_differential' => $payroll->NDPay(),
                        'holiday_ecola' => $d->Profile->Compensation->daily_category == 'Location' ? ($payroll->leg / 8) * $d->Profile->Compensation->DailyRate->ecola : 0.00,
                        'adjustment_wage' => $d->nwage,
                        'adjustment_income' => $d->adji,
                        'adjustment_deduction' => $d->adjd,
                        'de_minimis' => $d->Profile->Compensation->de_minimis,
                        'thirteenth_month_bonus' => $thirteenthMonth,
                        'thirteenth_month_pro_rated' => EmploymentDetails::where('user_id', $user_id)->first()->position_id != 1 ?  $payroll->basicPay() / 12 : 0,
                        'fixed_allowance' => $d->Profile->Compensation->fixed_allowance / 2,
                        'leave_encashment' => 0, // TODO
                        'incentives' => $d->incentive,
                        'coop_savings' => 0, // Omitted
                        'sss_ec' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ec : 0,
                        'sss_er' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->er : 0,
                        'hdmf_er' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_hdmf == 'Y') ? \App\HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employer_share : 0,
                        'philhealth_er' => $philhealth_er,
                        'sss_ee' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ee : 0,
                        'hdmf_ee' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_hdmf == 'Y') ? \App\HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employee_share + $user->Profile->hdmf_additional : 0,
                        'philhealth_ee' => $philhealth_ee,
                        'tardiness' => ($payroll->hourlyRate() / 60) * ($payroll->lt + $payroll->ut),
                        'absences' => ($payroll->hourlyRate() / 60) * $payroll->abs,
                        'sss_loan' => $sssLoan,
                        'hdmf_loan' => $hdmfLoan,
                        'client_charges' => $d->client_charges,
                        'membership_fee' => $membershipFee,
                        'notarial_fee' => $notarialFee,
                        'coop_loan' => $coopLoan,
                        'coop_share' => $d->Profile->coop_share,
                        'unpaid_leave' => $unpaidLeave
                    ];



                    if($unpaidLeave > 0) {
                        $incomeTx = $payroll['basic_pay'] + $payrollComputation->rd() + $payrollComputation->spe() + $payrollComputation->sperd() + $payrollComputation->leg() + $payrollComputation->legrd() + $payroll['overtime_pay'] + $payroll['night_differential'] + $payroll['ecola'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
                    } else {
                        $incomeTx = $payroll['basic_pay'] + $payrollComputation->rd() + $payrollComputation->spe() + $payrollComputation->sperd() + $payrollComputation->leg() + $payrollComputation->legrd() + $payroll['overtime_pay'] + $payroll['night_differential'] + $payrollComputation->vl() + $payrollComputation->sl() + $payrollComputation->el() + $payrollComputation->spl() + $payrollComputation->ml() + $payrollComputation->pl() + $payrollComputation->pto() + $payrollComputation->bl() + $payrollComputation->cl() + $payroll['ecola'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
                    }

                    $incomeNtx = $payroll['de_minimis'] + $payroll['thirteenth_month_bonus'] + $payroll['fixed_allowance'] + $payroll['leave_encashment'] + $payroll['incentives'] + $payroll['adjustment_income'];
                    $deductionTx = $payroll['sss_ee'] + $payroll['hdmf_ee'] + $payroll['philhealth_ee'] + $payroll['tardiness'] + $payroll['absences'];
                    $deduction = $payroll['sss_loan'] + $payroll['hdmf_loan'] + $payroll['client_charges'] + $payroll['membership_fee'] + $payroll['notarial_fee'] + $payroll['coop_loan'] + $payroll['coop_share'] + $payroll['adjustment_deduction'];

                    $payroll['tax'] = 0;
                    if(\App\Compensations::where('user_id', $d->user_id)->first()->deduct_withholding == 'Y') {
                        $taxTable = new \App\TaxTable($d->User->Profile->Compensation->TaxType->title, $incomeTx);
                        $payroll['tax'] = $taxTable->taxDue();
                    }

                    $payroll['gross_amount'] = $incomeTx + $incomeNtx;
                    $payroll['deductions'] = $deductionTx + $deduction + $unpaidLeave;
                    $payroll['net_pay'] = ($payroll['gross_amount'] - $payroll['deductions']) - $payroll['tax'];

                    $payroll['other_incomes'] = 0;
                    $payroll['other_deductions'] = 0;
                    $otherIncomes = PayrollOthers::where([
                        'payroll_cycle_id' => $d->payroll_cycle_id,
                        'user_id' => $d->user_id,
                        'type' => 'Income'
                    ])->get();
                    if($otherIncomes->count() > 0) {
                        $payroll['other_incomes'] = $otherIncomes->sum('amount');
                    }

                    $otherDeductions = PayrollOthers::where([
                        'payroll_cycle_id' => $d->payroll_cycle_id,
                        'user_id' => $d->user_id,
                        'type' => 'Deduction'
                    ])->get();
                    if($otherDeductions->count() > 0) {
                        $payroll['other_deductions'] = $otherDeductions->sum('amount');
                    }


                    $payroll = (object) $payroll;


                    if($d->net_pay > 0) {
                        $fields = [
                            'sss_er',
                            'sss_ec',
                            'hdmf_er',
                            'philhealth_er',
                            'basic_pay',
                            'ecola',
                            'holiday_ecola',
                            'adjustment_wage',
                            'de_minimis',
                            'thirteenth_month_bonus',
                            'fixed_allowance',
                            'leave_encashment',
                            'incentives',
                            'adjustment_income',
                            'absences',
                            'tardiness',
                            'sss_ee',
                            'hdmf_ee',
                            'philhealth_ee',
                            'sss_loan',
                            'hdmf_loan',
                            'client_charges',
                            'membership_fee',
                            'notarial_fee',
                            'coop_loan',
                            'coop_share',
                            'unpaid_leave',
                            'adjustment_deduction',
                            'gross_amount',
                            'deductions',
                            'tax',
                            'net_pay',
                            'other_deductions',
                            'other_incomes'
                        ];

                        foreach($fields as $k) {
                            $payroll->{$k} = $d->{$k};
                        }
                    }

                    $p = [
                        $d->User->Profile->id_number,
                        $d->User->Profile->namelfm(),
                        $d->User->Profile->Compensation->rate_type,
                        $payroll->basic_pay,
                        ($payrollComputation->rg() / 8),
                        $payrollComputation->rd(),
                        ($payrollComputation->rd / 8),
                        $payrollComputation->spe(),
                        ($payrollComputation->spe / 8),
                        $payrollComputation->sperd(),
                        ($payrollComputation->sperd / 8),
                        $payrollComputation->leg(),
                        ($payrollComputation->leg / 8),
                        $payrollComputation->legrd(),
                        ($payrollComputation->legrd / 8),
                        $payrollComputation->OTPay(),
                        ($payrollComputation->regot / 8),
                        $payrollComputation->NDPay(),
                        ($payrollComputation->nd / 8),
                        $payrollComputation->vl(),
                        ($payrollComputation->vl / 8),
                        $payrollComputation->sl(),
                        ($payrollComputation->sl / 8),
                        $payrollComputation->el(),
                        ($payrollComputation->el / 8),
                        $payrollComputation->spl(),
                        ($payrollComputation->spl / 8),
                        $payrollComputation->ml(),
                        ($payrollComputation->ml / 8),
                        $payrollComputation->pl(),
                        ($payrollComputation->pl / 8),
                        $payrollComputation->pto(),
                        ($payrollComputation->pto / 8),
                        $payrollComputation->bl(),
                        ($payrollComputation->bl / 8),
                        $payrollComputation->cl(),
                        ($payrollComputation->cl / 8),
                        $payroll->ecola,
                        $payroll->holiday_ecola,
                        $payroll->adjustment_wage,
                        $payroll->de_minimis,
                        $payroll->thirteenth_month_bonus,
                        $payroll->fixed_allowance,
                        $payroll->leave_encashment,
                        $payroll->incentives,
                        $payroll->incentives
                    ];

                    foreach($headersIncome as $headerIncome) {
                        if($otherIncomes->where('title', $headerIncome)->count() == 0) {
                            $p[] = 0;
                        } else {
                            $p[] = $otherIncomes->where('title', $headerIncome)->first()->amount;
                        }
                    }

                    $p[] = $payroll->absences;
                    $p[] = $payroll->tardiness;
                    $p[] = $payroll->sss_ee;
                    $p[] = $payroll->hdmf_ee;
                    $p[] = $payroll->philhealth_ee;
                    $p[] = $payroll->sss_loan;
                    $p[] = $payroll->hdmf_loan;
                    $p[] = $payroll->client_charges;
                    $p[] = $payroll->membership_fee;
                    $p[] = $payroll->notarial_fee;
                    $p[] = $payroll->coop_loan;
                    $p[] = $payroll->coop_share;
                    $p[] = $payroll->unpaid_leave;
                    $p[] = $payroll->adjustment_deduction;


                    foreach($headersDeduction as $headerDeduction) {
                        if($otherDeductions->where('title', $headerDeduction)->count() == 0) {
                            $p[] = 0;
                        } else {
                            $p[] = $otherDeductions->where('title', $headerDeduction)->first()->amount;
                        }
                    }

                    $p[] = $payroll->gross_amount;
                    $p[] = $payroll->deductions;
                    $p[] = $payroll->tax;
                    $p[] = $payroll->net_pay;

                    $sheet->row($x, $p);

                    $x++;
                }


                $sheet->setColumnFormat(array(
                    'D2:D'. $x => '#,##0.00',
                    'E2:E'. $x  => '0.00',
                    'F2:F'. $x  => '#,##0.00',
                    'G2:G'. $x  => '0.00',
                    'H2:H'. $x  => '#,##0.00',
                    'I2:I'. $x  => '0.00',
                    'J2:J'. $x  => '#,##0.00',
                    'K2:K'. $x  => '0.00',
                    'L2:L'. $x  => '#,##0.00',
                    'M2:M'. $x  => '0.00',
                    'N2:N'. $x  => '#,##0.00',
                    'O2:O'. $x  => '0.00',
                    'P2:P'. $x  => '#,##0.00',
                    'Q2:Q'. $x  => '0.00',
                    'R2:R'. $x  => '#,##0.00',
                    'S2:S'. $x  => '0.00',
                    'T2:T'. $x  => '#,##0.00',
                    'U2:U'. $x  => '0.00',
                    'V2:V'. $x  => '#,##0.00',
                    'W2:W'. $x  => '0.00',
                    'X2:X'. $x  => '#,##0.00',
                    'Y2:Y'. $x  => '0.00',
                    'Z2:Z'. $x  => '#,##0.00',
                    'AA2:AA'. $x  => '#,##0.00',
                    'AB2:AB'. $x  => '0.00',
                    'AC2:AC'. $x  => '#,##0.00',
                    'AD2:AD'. $x  => '0.00',
                    'AE2:AE'. $x  => '#,##0.00',
                    'AF2:AF'. $x  => '0.00',
                    'AG2:AG'. $x  => '#,##0.00',
                    'AH2:AH'. $x  => '0.00',
                    'AI2:AI'. $x  => '#,##0.00',
                    'AJ2:AJ'. $x  => '0.00',
                    'AK2:AK'. $x  => '#,##0.00',
                    'AL2:AL'. $x  => '0.00',
                    'AM2:DA'. $x => '#,##0.00',
                ));

            });
        })->download('xls');
    }

    public function exportProcessed(Request $request, $payrollCycleId)
    {
//        dd(json_decode($request->get('data')));

        $payrollCycle = PayrollCycles::find($payrollCycleId);


        if($request->get('data') != '[]') {
            $json = json_decode($request->get('data'));
            foreach($json as $j) {
                $idNumbers[] = $j->id_number;
            }

            if(empty($idNumbers)) {
                $request->session()->flash('popError', "Please select atleast 1 employee.");
                return redirect()->back();
            }

            $dtr = PayrollSummary::leftJoin('profiles', 'profiles.user_id', '=', 'payroll_summary.user_id')->whereIn('profiles.id_number', $idNumbers)->where('payroll_cycle_id', $payrollCycleId)->orderBy('last_name')->get();
        } else {
            $dtr = PayrollSummary::leftJoin('profiles', 'profiles.user_id', '=', 'payroll_summary.user_id')->where('payroll_cycle_id', $payrollCycleId)->orderBy('last_name')->get();
        }

        Excel::create("Payroll ({$payrollCycle->payroll_code})", function ($excel) use ($payrollCycle, $dtr) {


            // Set the title
            $excel->setTitle("Payroll ({$payrollCycle->payroll_code})");

            $excel->sheet($payrollCycle->payroll_code, function ($sheet) use ($payrollCycle, $dtr) {


                $headers = array(
                    'EMPLOYEE ID NUMBER',
                    "EMPLOYEE NAME",
                    'RATE TYPE',
                    'BASIC PAY',
                    'DAYS',
                    'RD',
                    'DAYS',
                    'SPE',
                    'DAYS',
                    'SPERD',
                    'DAYS',
                    'LEG',
                    'DAYS',
                    'LEGRD',
                    'DAYS',
                    'OT',
                    'DAYS',
                    'ND',
                    'DAYS',
                    'VL',
                    'DAYS',
                    'SL',
                    'DAYS',
                    'EL',
                    'DAYS',
                    'SPL',
                    'DAYS',
                    'ML',
                    'DAYS',
                    'PL',
                    'DAYS',
                    'PTO',
                    'DAYS',
                    'BL',
                    'DAYS',
                    'CL',
                    'DAYS',
                    'ECOLA',
                    'HECOLA',
                    'NWAGE',
                    'DE MINIMIS',
                    '13TH MONTH',
                    'FIXED ALLOWANCE',
                    'LEAVE',
                    'INCENTIVES',
                    'ADJUSTMENT INCOME'
                );

                $headersIncome = [];
                foreach($dtr as $d) {
                    $otherIncomes = PayrollOthers::where([
                        'payroll_cycle_id' => $d->payroll_cycle_id,
                        'user_id' => $d->user_id,
                        'type' => 'Income'
                    ])->get();

                    foreach($otherIncomes as $income) {
                        if(!in_array($income->title, $headersIncome)) {
                            $headersIncome[] = $income->title;
                            $headers[] = $income->title;
                        }
                    }
                }


                $headers[] = strtoupper('Absences');
                $headers[] = strtoupper('Tardiness');
                $headers[] = strtoupper('SSS EE');
                $headers[] = strtoupper('HDMF EE');
                $headers[] = strtoupper('PhilHealth EE');
                $headers[] = strtoupper('SSS Loan');
                $headers[] = strtoupper('HDMF Loan');
                $headers[] = strtoupper('Client Charges');
                $headers[] = strtoupper('Membership Fee');
                $headers[] = strtoupper('Notarial Fee');
                $headers[] = strtoupper('Coop Loan');
                $headers[] = strtoupper('Coop Share');
                $headers[] = strtoupper('Unpaid Leave');
                $headers[] = 'ADJUSTMENT DEDUCTION';



                $headersDeduction = [];
                foreach($dtr as $d) {
                    $otherDeductions = PayrollOthers::where([
                        'payroll_cycle_id' => $d->payroll_cycle_id,
                        'user_id' => $d->user_id,
                        'type' => 'Deduction'
                    ])->get();

                    foreach($otherDeductions as $deduction) {
                        if(!in_array($deduction->title, $headersDeduction)) {
                            $headersDeduction[] = $deduction->title;
                            $headers[] = $deduction->title;
                        }
                    }
                }

                $headers[] = 'GROSS AMOUNT';
                $headers[] = 'DEDUCTIONS';
                $headers[] = 'TAX';
                $headers[] = 'NET PAY';

                $sheet->row(1, $headers);



                $x = 2;
                foreach($dtr as $payroll) {

                    $user = $payroll->User;
                    $payrollCode = explode('-', $payrollCycle->payroll_code);
                    $user_id = $user->id;

                    if($payroll->User->Profile->Compensation->rate_type == 'Monthly') {
                        $payrollComputation = new \App\ComputePayroll($payroll->Dtr, ['daily_rate' => $payroll->User->Profile->Compensation->monthly_rate / 26, 'monthly_rate' => $payroll->User->Profile->Compensation->monthly_rate / 2, 'rate_type' => 'Monthly']);
                    } else {
                        if($payroll->User->Profile->Compensation->daily_category == 'Location') {
                            $payrollComputation = new \App\ComputePayroll($payroll->Dtr, $payroll->User->Profile->Compensation->DailyRate->rate);
                        } else {
                            $payrollComputation = new \App\ComputePayroll($payroll->Dtr, $payroll->User->Profile->Compensation->daily_rate);
                        }
                    }


                    $p = [
                        $user->Profile->id_number,
                        $user->Profile->namelfm(),
                        $user->Profile->Compensation->rate_type,
                        $payroll->basic_pay,
                        ($payrollComputation->rg() / 8),
                        $payrollComputation->rd(),
                        ($payrollComputation->rd / 8),
                        $payrollComputation->spe(),
                        ($payrollComputation->spe / 8),
                        $payrollComputation->sperd(),
                        ($payrollComputation->sperd / 8),
                        $payrollComputation->leg(),
                        ($payrollComputation->leg / 8),
                        $payrollComputation->legrd(),
                        ($payrollComputation->legrd / 8),
                        $payrollComputation->OTPay(),
                        ($payrollComputation->regot / 8),
                        $payrollComputation->NDPay(),
                        ($payrollComputation->nd / 8),
                        $payrollComputation->vl(),
                        ($payrollComputation->vl / 8),
                        $payrollComputation->sl(),
                        ($payrollComputation->sl / 8),
                        $payrollComputation->el(),
                        ($payrollComputation->el / 8),
                        $payrollComputation->spl(),
                        ($payrollComputation->spl / 8),
                        $payrollComputation->ml(),
                        ($payrollComputation->ml / 8),
                        $payrollComputation->pl(),
                        ($payrollComputation->pl / 8),
                        $payrollComputation->pto(),
                        ($payrollComputation->pto / 8),
                        $payrollComputation->bl(),
                        ($payrollComputation->bl / 8),
                        $payrollComputation->cl(),
                        ($payrollComputation->cl / 8),
                        $payroll->ecola,
                        $payroll->holiday_ecola,
                        $payroll->adjustment_wage,
                        $payroll->de_minimis,
                        $payroll->thirteenth_month_bonus,
                        $payroll->fixed_allowance,
                        $payroll->leave_encashment,
                        $payroll->incentives,
                        $payroll->adjustment_income
                    ];

                    foreach($headersIncome as $headerIncome) {
                        if($otherIncomes->where('title', $headerIncome)->count() == 0) {
                            $p[] = 0;
                        } else {
                            $p[] = $otherIncomes->where('title', $headerIncome)->first()->amount;
                        }
                    }

                    $p[] = $payroll->absences;
                    $p[] = $payroll->tardiness;
                    $p[] = $payroll->sss_ee;
                    $p[] = $payroll->hdmf_ee;
                    $p[] = $payroll->philhealth_ee;
                    $p[] = $payroll->sss_loan;
                    $p[] = $payroll->hdmf_loan;
                    $p[] = $payroll->client_charges;
                    $p[] = $payroll->membership_fee;
                    $p[] = $payroll->notarial_fee;
                    $p[] = $payroll->coop_loan;
                    $p[] = $payroll->coop_share;
                    $p[] = $payroll->unpaid_leave;
                    $p[] = $payroll->adjustment_deduction;


                    foreach($headersDeduction as $headerDeduction) {
                        if($otherDeductions->where('title', $headerDeduction)->count() == 0) {
                            $p[] = 0;
                        } else {
                            $p[] = $otherDeductions->where('title', $headerDeduction)->first()->amount;
                        }
                    }

                    $p[] = $payroll->gross_amount;
                    $p[] = $payroll->deductions;
                    $p[] = $payroll->tax;
                    $p[] = $payroll->net_pay;

                    $sheet->row($x, $p);


//                    $sheet->row($x, array(
//                        $payroll->User->Profile->id_number,
//                        $payroll->User->Profile->namelfm(),
//                        $payroll->User->Profile->Compensation->rate_type,
//                        $payroll->basic_pay,
//                        ($payrollComputation->rg() / 8),
//                        $payroll->ecola,
//                        $payroll->overtime_pay,
//                        ($payrollComputation->regot / 8),
//                        $payroll->holiday_pay,
//                        $payroll->night_differential,
//                        ($payrollComputation->nd / 8),
//                        $payroll->holiday_ecola,
//                        $payroll->adjustment_wage,
//                        $payroll->adjustment_deduction,
//                        $payroll->adjustment_income,
//                        $payroll->de_minimis,
//                        $payroll->fixed_allowance,
//                        $payroll->leave_encashment,
//                        $payroll->incentives,
//                        $payroll->other_incomes,
//                        $payroll->coop_savings,
//                        $payroll->sss_ec,
//                        $payroll->sss_er,
//                        $payroll->hdmf_er,
//                        $payroll->philhealth_er,
//                        $payroll->sss_ee,
//                        $payroll->hdmf_ee,
//                        $payroll->philhealth_ee,
//                        $payroll->tardiness,
//                        $payroll->absences,
//                        $payroll->sss_loan,
//                        $payroll->hdmf_loan,
//                        $payroll->client_charges,
//                        $payroll->membership_fee,
//                        $payroll->notarial_fee,
//                        $payroll->coop_loan,
//                        $payroll->coop_share,
//                        $payroll->unpaid_leave,
//                        $payroll->other_deductions,
//                        $payroll->gross_amount,
//                        $payroll->deductions,
//                        $payroll->tax,
//                        $payroll->net_pay,
//                        $payroll->thirteenth_month_bonus,
//                        $payroll->thirteenth_month_pro_rated
//                    ));

                    $x++;
                }

                $sheet->setColumnFormat(array(
                    'D2:D'. $x => '#,##0.00',
                    'E2:E'. $x  => '0.00',
                    'F2:F'. $x  => '#,##0.00',
                    'G2:G'. $x  => '0.00',
                    'H2:H'. $x  => '#,##0.00',
                    'I2:I'. $x  => '0.00',
                    'J2:J'. $x  => '#,##0.00',
                    'K2:K'. $x  => '0.00',
                    'L2:L'. $x  => '#,##0.00',
                    'M2:M'. $x  => '0.00',
                    'N2:N'. $x  => '#,##0.00',
                    'O2:O'. $x  => '0.00',
                    'P2:P'. $x  => '#,##0.00',
                    'Q2:Q'. $x  => '0.00',
                    'R2:R'. $x  => '#,##0.00',
                    'S2:S'. $x  => '0.00',
                    'T2:T'. $x  => '#,##0.00',
                    'U2:U'. $x  => '0.00',
                    'V2:V'. $x  => '#,##0.00',
                    'W2:W'. $x  => '0.00',
                    'X2:X'. $x  => '#,##0.00',
                    'Y2:Y'. $x  => '0.00',
                    'Z2:Z'. $x  => '#,##0.00',
                    'AA2:AA'. $x  => '#,##0.00',
                    'AB2:AB'. $x  => '0.00',
                    'AC2:AC'. $x  => '#,##0.00',
                    'AD2:AD'. $x  => '0.00',
                    'AE2:AE'. $x  => '#,##0.00',
                    'AF2:AF'. $x  => '0.00',
                    'AG2:AG'. $x  => '#,##0.00',
                    'AH2:AH'. $x  => '0.00',
                    'AI2:AI'. $x  => '#,##0.00',
                    'AJ2:AJ'. $x  => '0.00',
                    'AK2:AK'. $x  => '#,##0.00',
                    'AL2:AL'. $x  => '0.00',
                    'AM2:DA'. $x => '#,##0.00',
                ));

            });
        })->download('xls');
    }
}

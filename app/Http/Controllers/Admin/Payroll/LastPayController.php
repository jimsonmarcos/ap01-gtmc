<?php

namespace App\Http\Controllers\Admin\Payroll;

use App\AuditLog;
use App\EmploymentDetails;
use App\FinalPay13thSummary;
use App\FinalPayExistingLoans;
use App\FinalPayOtherCharges;
use App\FinalPays;
use App\LoanPayments;
use App\LoanPaymentSchedules;
use App\Loans;
use App\OtherPayments;
use App\PayrollDtr;
use App\PayrollSummary;
use App\Profiles;
use App\SSSContributions;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LastPayController extends Controller
{
    //
    public function index()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Last Pay";
        return view('admin.payroll.last_pay', $data);
    }

    public function final_pay($status = 'Hold')
    {
        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = "Final Pay";
        if($status == 'Hold') {
            $data['employees'] = $employees = PayrollDtr::where('hold_status', 'Y')
                                            ->leftJoin('final_pays as fp', 'fp.user_id', '=', 'payroll_dtr.user_id')
                                            ->whereNull('fp.id')
                                            ->select('payroll_dtr.*')
                                            ->get();
            $view = 'admin.payroll.final_pay';
        } else {
            $data['employees'] = FinalPays::where('status', $status)->orderByDesc('created_at')->get();
            $view = 'admin.payroll.final_pay_status';
        }

        $data['status'] = $status;

//        dd($employees);
        $data['holdEmployees'] = PayrollDtr::where('hold_status', 'Y')->leftJoin('final_pays as fp', 'fp.user_id', '=', 'payroll_dtr.user_id')
            ->whereNull('fp.id')->count();
        $data['pending'] = FinalPays::where('status', 'Pending')->count();
        $data['approved'] = FinalPays::where('status', 'Approved')->count();
        $data['released'] = FinalPays::where('status', 'Released')->count();
        $data['disbursed'] = FinalPays::where('status', 'Disbursed')->count();

        return view($view, $data);
    }

    public function final_pay_add(Request $request, $user_id)
    {
        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = 'Final Pay';

        $data['me'] = User::find(Auth::user()->id);

        $data['employee'] = $employee = User::find($user_id);

        if(empty($employee->Profile->EmploymentDetails->reason) && $employee->Profile->status == 'Active') {
            $request->session()->flash('popError', "Employee's status must be <strong>Inactive</strong> first before you can proceed");
            return redirect(url("admin/payroll/final_pay"));
        }

        $data['lastPayroll'] = $data['dtr'] = PayrollDtr::where(['user_id' => $user_id, 'hold_status' => 'Y'])->first();
        $data['clientCharges'] = $data['lastPayroll']->client_charges;
        $data['lastPayroll'] = $data['lastPayroll']->Summary();

        $previousYear = date('Y') - 1;
        $data['payrolls'] = $payrolls = PayrollSummary::join('payroll_cycles as pc', 'pc.id', '=', 'payroll_summary.payroll_cycle_id')
                            ->where('payroll_summary.user_id', $employee->id)
                            ->whereDate('payroll_date', '>=', date("{$previousYear}-12-1"))
                            ->orderByDesc('pc.payroll_date')
                            ->get();

        $totalProRated = 0;
        foreach($payrolls as $index => $payroll) {
            $totalProRated += $payroll->basic_pay / 12;
        }

        $data['totalProRated'] = str_replace(',', '', number_format($totalProRated, 2));

        $data['cashSavings'] = OtherPayments::selectRaw('COALESCE(total_amount_paid, 0) as total_amount_paid')->where('user_id', $user_id)->where('transaction_type', 'COOP SHARE')->sum('total_amount_paid');
        $data['loanSavings'] = Loans::selectRaw('COALESCE(savings, 0) as savings')->where('user_id', $user_id)->where('releasing_status', 'Released')->sum('savings');

        $unpaidLoans = Loans::selectRaw("loans.*, COALESCE(total_amount_paid, 0) total_amount_paid, COALESCE(lapsed_interest, 0) lapsed_interest")->where('user_id', $request->user_id)->where('status', 'Approved')
            ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')
            ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(lapsed_interest) as lapsed_interest FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payment_schedules.loan_id', '=', 'loans.id')
            ->whereRaw("((granted_loan_amount - COALESCE(total_amount_paid, 0)) > 0)")
            ->where('is_voucher', 'N');
        if($unpaidLoans->count() > 0) {
            $data['unpaidLoans'] = $unpaidLoans->with('payments')->orderBy('cla_series')->get();
        }


//        dd($data['payrolls']);

        return view('admin.payroll.final_pay.add', $data);
    }

    public function final_pay_add_post(Request $request, $user_id)
    {
//        dd($request->all());

        $data['user_id'] = $user_id;
        $data['created_by'] = Auth::user()->id;
        $data['effective_date'] = Carbon::createFromFormat('m/d/Y', $request->effective_date )->format('Y-m-d');
        $data['salary'] = $request->salary;
        $data['payroll_dtr_id'] = $request->payroll_dtr_id;
        $data['payroll_cycle_id'] = $request->payroll_cycle_id;
        $data['cash_savings'] = $request->cash_savings;
        $data['loan_savings'] = $request->loan_savings;
        $data['unpaid_loans'] = $request->unpaid_loans;
        $data['thirteenth_month'] = $request->thirteenth_month;
        $data['net_payable'] = str_replace(',', '', $request->net_payable);
        $data['total_collectible'] = str_replace(',', '', $request->total_collectible);
        $data['total_payable'] = str_replace(',', '', $request->total_payable);
        $data['client_charges'] = str_replace(',', '', $request->client_charges);
        $data['remarks'] = $request->remarks;
        $data['noted_by'] = $request->noted_by;
        $data['approved_by'] = $request->approved_by;



//        dd($data);

        $finalPay = FinalPays::create($data);

        if(!empty($request->existing_loans)) {
            foreach($request->existing_loans as $existingLoan) {
                $data = [
                    'final_pay_id' => $finalPay->id,
                    'loan_id' => $existingLoan['loan_id'],
                    'amount' => $existingLoan['amount'],
                    'payments_left' => $existingLoan['payments_left']
                ];

                FinalPayExistingLoans::create($data);
            }
        }

        if(!empty($request->other_charges)) {
            foreach($request->other_charges as $otherCharges) {
                $data = [
                    'final_pay_id' => $finalPay->id,
                    'particular' => $otherCharges['particular'],
                    'collectible' => str_replace(',', '', $otherCharges['collectible'])
                ];

                FinalPayOtherCharges::create($data);
            }
        }

        if(!empty($request->final_pay_13th_summary)) {
            foreach($request->final_pay_13th_summary as $summary) {
                $data = [
                    'final_pay_id' => $finalPay->id,
                    'payroll_code' => $summary['payroll_code'],
                    'payroll_period' => $summary['payroll_period'],
                    'basic_pay' => $summary['basic_pay'],
                    'pro_rated' => $summary['pro_rated']
                ];

                FinalPay13thSummary::create($data);
            }
        }

//        dd($finalPay);

        $profile = Profiles::where('user_id', Auth::user()->id)->first();
        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Final Pay',
            'action' => 'Create',
            'description' => "Created Final Pay for {$profile->id_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Successfully created Final Pay");
        return redirect(url("/admin/payroll/final_pay/Pending"));

    }

    public function update($finalPayId)
    {
        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = 'Final Pay';

        $data['employee'] = $employee = FinalPays::find($finalPayId);

        return view('admin.payroll.final_pay.update', $data);
    }

    public function update_post(Request $request, $finalPayId)
    {
//        dd($request->all());

//        dd(array_keys($request->other_charges));
        $data['updated_by'] = Auth::user()->id;
//        $data['effective_date'] = Carbon::createFromFormat('m/d/Y', $request->effective_date )->format('Y-m-d');
        $data['client_charges'] = str_replace(',', '', $request->client_charges);
        $data['remarks'] = $request->remarks;
        $data['noted_by'] = $request->noted_by;
        $data['approved_by'] = $request->approved_by;
        $otherExistingCharges = $request->other_changes_existing;

        if(!empty($otherExistingCharges)) {
            $getAllKeys = array_keys($otherExistingCharges);
            FinalPayOtherCharges::whereNotIn('id', $getAllKeys)->where('final_pay_id', $finalPayId)->delete();

            foreach($request->other_changes_existing as $k => $otherCharges) {
                FinalPayOtherCharges::where('id', $k)->update([
                    'particular' => $otherCharges['particular'],
                    'collectible' => str_replace(',', '', $otherCharges['collectible'])
                ]);
            }
        } else {
            FinalPayOtherCharges::where('final_pay_id', $finalPayId)->delete();
        }

        if(!empty($request->other_charges)) {
            foreach($request->other_charges as $otherCharges) {
                $newOtherCharges = [
                    'final_pay_id' => $finalPayId,
                    'particular' => $otherCharges['particular'],
                    'collectible' => str_replace(',', '', $otherCharges['collectible'])
                ];

                FinalPayOtherCharges::create($newOtherCharges);
            }
        }


        if($request->action == 'approve') {
            $data['status'] = 'Approved';

            $unpaidLoans = FinalPayExistingLoans::where('final_pay_id', $finalPayId)->get();
            if(!empty($unpaidLoans)) {
                foreach($unpaidLoans as $unpaidLoan) {
                    $pendingPayments = LoanPaymentSchedules::where('loan_id', $unpaidLoan->loan_id)->whereNotIn('pay_period', LoanPayments::where('loan_id', $unpaidLoan->loan_id)->pluck('pay_period'))->get();
                    foreach($pendingPayments as $pendingPayment) {
                        $latestPaymentSeries = LoanPayments::max('series');
                        $series = !empty($latestPaymentSeries) ? $latestPaymentSeries + 1 : 1001;
                        $transaction_id = "LD".date('Ymd')."-{$series}";

                        $arNumber = LoanPayments::orderBy('ar_number', 'desc')->first();
                        if(!empty($arNumber)) {
                            $arNumber = explode('-', $arNumber->ar_number);
                            $arNumber = $arNumber[1] + 1;
                        } else {
                            $arNumber = 101;
                        }

                        $arNumber = "AR-". $arNumber;

                        $payment = [
                            'transaction_id' => $transaction_id,
                            'series' => $series,
                            'loan_id' => $pendingPayment->loan_id,
                            'ar_number' => $arNumber,
                            'remarks' => 'FINAL PAY',
                            'payment_method' => 'Salary Deduction',
                            'payment_date' => date('Y-m-d'),
                            'amount' => $pendingPayment->amortization,
                            'interest' => $pendingPayment->lapsed_interest,
                            'total_amount_paid' => $pendingPayment->amortization + $pendingPayment->lapsed_interest,
                            'pay_period' => $pendingPayment->pay_period
                        ];

                        LoanPayments::create($payment);
                    }

                    Loans::where('id', $unpaidLoan->loan_id)->update(['is_paid' => 'Y']);
                }
            }
        }

        $data['net_payable'] = str_replace(',', '', $request->net_payable);



        FinalPays::where('id', $finalPayId)->update($data);

        $finalPay = FinalPays::find($finalPayId);
        $profile = Profiles::where('user_id', $finalPay->user_id)->first();
        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Final Pay',
            'action' => 'Edit',
            'description' => "Updated Final Pay for {$profile->id_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);


        if(!empty($request->other_changes_existing)) {
            foreach($request->other_changes_existing as $id => $otherCharges) {
                $data = [
                    'particular' => $otherCharges['particular'],
                    'collectible' => $otherCharges['collectible']
                ];

                FinalPayOtherCharges::where('id', $id)->update($data);
            }
        }

//        dd($finalPay);

        $request->session()->flash('popSuccess', "Successfully created Final Pay");
        return redirect(url("/admin/payroll/final_pay/update/{$finalPayId}"));
    }

    public function release(Request $request, $finalPayId) {
        $finalPay = FinalPays::find($finalPayId);

        if(empty($finalPay)) {
            $request->session()->flash('popError', "Final Pay does not exist");
            return redirect(url("admin/payroll/final_pay"));
        }

        if($finalPay->status != 'Approved') {
            $request->session()->flash('popError', "Final Pay status must be <strong>Approved</strong>");
            return redirect(url("admin/payroll/final_pay"));
        }

        $finalPay->status = 'Released';
        $finalPay->save();

        $profile = Profiles::where('user_id', $finalPay->user_id)->first();
        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Final Pay',
            'action' => 'Edit',
            'description' => "Released Final Pay for {$profile->id_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

//        Profiles::where('user_id', $finalPay->user_id)->update(['status' => 'Inactive']);
//        EmploymentDetails::where('user_id', $finalPay->user_id)->update(['status' => 'Inactive']);


        $request->session()->flash('popSuccess', "Final Pay for ". $finalPay->Profile->namelfm() . " successfully released");
        return redirect(url("/admin/payroll/final_pay/preview/{$finalPayId}"));
    }

    public function disburse(Request $request, $finalPayId) {
        $data['finalPay'] = $finalPay = FinalPays::find($finalPayId);

        if(empty($finalPay)) {
            $request->session()->flash('popError', "Final Pay does not exist");
            return redirect(url("admin/payroll/final_pay"));
        }

        if($finalPay->status != 'Approved') {
            $request->session()->flash('popError', "Final Pay status must be <strong>Approved</strong>");
            return redirect(url("admin/payroll/final_pay"));
        }


        $data['pageHeader'] = "Last Pay";

        return view('admin.payroll.final_pay.disburse', $data);
    }

    public function disburse_post(Request $request, $finalPayId) {
        $data = $request->except('_token');
        $data['released_date'] = Carbon::createFromFormat('m/d/Y', $request->released_date)->format('Y-m-d');
        $data['status'] = 'Released';

        $finalPay = FinalPays::find($finalPayId);
//        $finalPay->save($data);

        FinalPays::where('id', $finalPayId)->update($data);
        $finalPay = FinalPays::find($finalPayId);
//        dd($finalPay);

        EmploymentDetails::where('user_id', $finalPay->user_id)->update(['status' => 'Deactivated']);
//        Profiles::where('user_id', $finalPay->user_id)->update(['status' => 'Inactive']);

        $coopWithdrawal = [
            'ar_number' => 'AR-000',
            'payment_date' => $data['released_date'],
            'transaction_type' => 'COOP Withdrawal',
            'payment_method' => 'Cash',
            'user_id' => $finalPay->user_id,
            'pay_period' => '',
            'amount' => $finalPay->cash_savings,
            'total_amount_paid' => $finalPay->cash_savings
        ];
        OtherPayments::create($coopWithdrawal);


        $request->session()->flash('popSuccess', "Successfully released Final Pay");
        return redirect(url("/admin/payroll/final_pay/Released"));
    }

    public function preview(Request $request, $finalPayId)
    {
        $data['finalPay'] = $finalPay = FinalPays::find($finalPayId);

        if(empty($finalPay)) {
            $request->session()->flash('popError', "Final Pay does not exist");
            return redirect(url("admin/payroll/final_pay"));
        }

//        if($finalPay->status == 'Pending') {
//            $request->session()->flash('popError', "Final Pay status must be not be <strong>on Hold</strong>");
//            return redirect(url("admin/payroll/final_pay"));
//        }

        $data['pageHeader'] = "Final Pay Preview";

        $data['me'] = User::find(Auth::user()->id);

//        return view('admin.payroll.final_pay.preview', $data);
        return view('print.final_pay', $data);
    }

    public function final_pay_details($dtrId)
    {
        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = 'Final Pay';

        $data['employee'] = $employee = PayrollDtr::find($dtrId);

        return view('admin.payroll.final_pay.details', $data);
    }

    public function final_pay_preview()
    {
        $data['sidebar'] = view('admin.sidebar');
        $data['pageHeader'] = 'Final Pay';
        return view('admin.payroll.final_pay.preview', $data);
    }

}

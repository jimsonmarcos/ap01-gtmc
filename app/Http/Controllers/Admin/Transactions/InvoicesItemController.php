<?php
namespace App\Http\Controllers\Admin\Transactions;

use App\InvoiceItemList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CivilStatus;

class InvoicesItemController extends Controller
{
    // Create Page
    public function create()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Create Invoice Item";

        return view('admin.transactions.invoices_create_item', $data);
    }

    public function create_post(Request $request)
    {
//        $invoiceItems = $request->session()->get('invoiceItems');
//        $invoiceItems[] = $request->except('_token');
//        $request->session()->put('invoiceItems', $invoiceItems);

        $invoiceItem =  $request->except('_token');
        $invoiceItem['total_hours'] = $invoiceItem['total_number_hours'];

//        dd($invoiceItem);
        InvoiceItemList::create($invoiceItem);



        $request->session()->flash('popSuccess', "Invoice Item successfully created.");
        return redirect(url('/admin/transactions/invoices/items'));
//        return redirect('/admin/transactions/invoices/create_invoice');
    }

    public function add_new_tax()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Invoices";

        return view('admin.transactions.item_add_new_tax', $data);
    }

    public function add_invoice_item()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Add Invoice Item";

        return view('admin.transactions.invoice_available_items', $data);
    }

    public function add_invoice_item_post(Request $request)
    {

    }
}
?>
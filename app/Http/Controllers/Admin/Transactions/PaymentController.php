<?php

namespace App\Http\Controllers\Admin\Transactions;

use App\AuditLog;
use App\LoanPayments;
use App\OtherPayments;
use App\Profiles;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CivilStatus;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    //

    public function index($id = "")
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Payment Transactions";

        return view('admin.transactions.payments_loan_payments', $data);
    }

    public function coop_shares()
    {
        $data['pageHeader'] = "Coop Shares";
        return view('admin.transactions.payments_coop_shares', $data);
    }

    public function coop_share_view($id)
    {
        $data['pageHeader'] = "Coop Shares";

        $data['payment'] = OtherPayments::find($id);

        return view('admin.transactions.view_other_payment', $data);
    }

    public function notarial_fee()
    {
        $data['pageHeader'] = "Notarial Fee";
        return view('admin.transactions.payments_notarial_fee', $data);
    }

    public function notarial_fee_view($id)
    {
        $data['pageHeader'] = "Notarial Fee";

        $data['payment'] = OtherPayments::find($id);

        return view('admin.transactions.view_other_payment', $data);
    }

    public function membership_fee()
    {
        $data['pageHeader'] = "Membership Fee";
        return view('admin.transactions.payments_membership_fee', $data);
    }

    public function membership_fee_view($id)
    {
        $data['pageHeader'] = "Membership Fee";

        $data['payment'] = OtherPayments::find($id);

        return view('admin.transactions.view_other_payment', $data);
    }

    public function coop_shares_add()
    {
        $data['pageHeader'] = "Add Coop Share";
        return view('admin.transactions.coop_shares_add', $data);
    }

    public function withdrawals()
    {
        $data['pageHeader'] = "Withdrawals";
        return view('admin.transactions.payments_withdrawals', $data);
    }

    public function withdrawals_add()
    {
        $data['pageHeader'] = "Add Withdrawal";
        return view('admin.transactions.payments_withdrawals_add', $data);
    }

    public function add_other_payment(Request $request)
    {
        if(!empty($request->get('user_id'))) {
            $data['user'] = User::find($request->get('user_id'));
        }
        if(!empty($request->get('transaction_type'))) {
            $data['transaction_type'] = $request->get('transaction_type');
        }

        $data['arNumber'] = OtherPayments::orderBy('ar_number', 'desc')->first();
        if(!empty($data['arNumber'])) {
            $data['arNumber'] = explode('-', $data['arNumber']->ar_number);
            $data['arNumber'] = $data['arNumber'][1] + 1;
        } else {
            $data['arNumber'] = 101;
        }

        $data['pageHeader'] = "Other Payments";
        return view('admin.transactions.add_other_payment', $data);
    }

    public function add_other_payment_post(Request $request)
    {
        $data = $request->except('_token');

        $data['ar_number'] = "AR-".$data['ar_number'];

        $data['payment_date'] = Carbon::createFromFormat('m/d/Y', $data['payment_date'])->format('Y-m-d');

        $data['created_by'] = Auth::user()->id;
        $otherPayment = OtherPayments::create($data);


        $profile = Profiles::where('user_id', $otherPayment->user_id)->first();
        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Other Payment',
            'action' => 'Create',
            'description' => "Added {$otherPayment->transaction_type} Payment for {$profile->id_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', 'Payment successfully added.');
        return redirect(url('/admin/transactions/payments/add'));
    }
}

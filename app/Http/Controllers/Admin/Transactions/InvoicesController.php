<?php
namespace App\Http\Controllers\Admin\Transactions;

use App\AuditLog;
use App\CompaniesContactPersons;
use App\InvoiceFiles;
use App\InvoiceItemList;
use App\InvoiceItems;
use App\Invoices;
use App\Profiles;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CivilStatus;
use Illuminate\Support\Facades\Auth;

class InvoicesController extends Controller
{
    // View Page
    public function view($status = 'All')
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Invoices";

        if($status != 'All') {
            $data['invoices'] = Invoices::where('status', $status)->orderByDesc('created_at')->get();
        } else {
            $data['invoices'] = Invoices::orderByDesc('created_at')->get();
        }

        $data['status'] = $status;

        return view('admin.transactions.invoices', $data);
    }

    public function invoices_status_draft()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Invoices";

        return view('admin.transactions.invoices_status_draft', $data);
    }

    public function invoices_status_unpaid()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Invoices";

        return view('admin.transactions.invoices_status_unpaid', $data);
    }

    public function invoices_status_paid()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Invoices";

        return view('admin.transactions.invoices_status_paid', $data);
    }
    // Create Page
    public function invoice_create(Request $request)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Invoices";

        if($request->has('invoice_item')) {
            foreach($request->invoice_item as $k => $v) {
                $invoiceItems[] = $k;
            }

            $data['invoiceItems'] = InvoiceItemList::whereIn('id', $invoiceItems);
        }

        $data['me'] = User::find(Auth::user()->id);


        return view('admin.transactions.invoices_form', $data);
    }

    public function invoice_create_post(Request $request)
    {
        $data = $request->except(['_token', 'files']);

//        $data['bill_to'] = CompaniesContactPersons::find($data['contact_person_id']);
//        $data['bill_to'] = $data['bill_to']->first_name ." ". $data['bill_to']->last_name;

        $data['invoice_date']  = Carbon::createFromFormat('m/d/Y', $data['invoice_date'] )->format('Y-m-d');
        $data['due_date']  = Carbon::createFromFormat('m/d/Y', $data['due_date'] )->format('Y-m-d');

        if(!empty($request->allFiles())) {
            foreach($request->allFiles() as $photos) {
                foreach($photos as $photo) {
                    $invoiceFiles[] = [
                        'file_name' => $photo->getClientOriginalName(),
                        'file_path' => $photo->store("public/invoice_files")
                    ];
                }
            }
        }

//        dd($data);


//        die();

        if(Invoices::count() == 0) {
            $data['invoice_number'] = 'BI-1001';
        } else {
            $latestInvoice = Invoices::orderByDesc('invoice_number')->first()->invoice_number;
            $pieces = explode('-', $latestInvoice);
            $invoiceNumber = $pieces[1] + 1;
//            dd($invoiceNumber);
            $data['invoice_number'] = "BI-{$invoiceNumber}";
        }


        $invoiceItems = $request->items;
        unset($data['items']);


        $data['total_amount'] = str_replace(',', '', $data['total_amount']);
        $invoice = Invoices::create($data);

        if(!empty($invoiceItems)) {
            foreach($invoiceItems as $invoiceItem) {

                $invoiceItem['invoice_id'] = $invoice->id;
                $invoiceItem['amount'] = str_replace(',', '', $invoiceItem['amount']);
                $invoiceItem['tax_amount'] = str_replace(',', '', $invoiceItem['tax_amount']);
                $invoiceItem['billing_amount'] = str_replace(',', '', $invoiceItem['billing_amount']);

                InvoiceItems::create($invoiceItem);
            }
        }

        if(!empty($invoiceFiles)) {
            foreach($invoiceFiles as $invoiceFile) {
                $invoiceFile['invoice_id'] = $invoice->id;

                InvoiceFiles::create($invoiceFile);
            }
        }

//        die();


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Invoice',
            'action' => 'Create',
            'description' => "Created Invoice: {$invoice->invoice_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Invoice successfully created.");
        return redirect("/admin/transactions/invoices/status/{$invoice->status}");

    }

    // Update Page
    public function invoice_edit($id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Invoices";

        $data['invoice'] = Invoices::find($id);

        return view('admin.transactions.invoices_edit', $data);
    }

    public function invoice_edit_post(Request $request, $id)
    {
        $data = $request->except(['_token', 'files']);

        $data['invoice_date']  = Carbon::createFromFormat('m/d/Y', $data['invoice_date'] )->format('Y-m-d');
        $data['due_date']  = Carbon::createFromFormat('m/d/Y', $data['due_date'] )->format('Y-m-d');

        if(!empty($request->allFiles())) {
            foreach($request->allFiles() as $photos) {
                foreach($photos as $photo) {
                    $invoiceFiles[] = [
                        'file_name' => $photo->getClientOriginalName(),
                        'file_path' => $photo->store("public/invoice_files")
                    ];
                }
            }
        }

//        dd($data);

        if(!empty($data['existing_files'])) {
            InvoiceFiles::where('invoice_id', $id)->whereNotIn('id', $data['existing_files'])->delete();

            unset($data['existing_files']);
        } else {
            InvoiceFiles::where('invoice_id', $id)->delete();
        }

        if(!empty($data['existing_items'])) {
            $invoiceItems = [];
            foreach($data['existing_items'] as $ItemId => $v) {
                $invoiceItems[] = $ItemId;

                InvoiceItems::where('id', $ItemId)->update($v);
            }

            InvoiceItems::where('invoice_id', $id)->whereNotIn('id', $invoiceItems)->delete();

            unset($data['existing_items']);
        } else {
            InvoiceItems::where('invoice_id', $id)->delete();
        }



//        die();


        $invoiceItems = $request->items;
        unset($data['items']);
//        dd($invoiceItems);

        $invoice['total_amount'] = str_replace(',', '', $data['total_amount']);
        Invoices::where('id', $id)->update($data);

        $invoice = Invoices::find($id);

        if(!empty($invoiceItems)) {
            foreach($invoiceItems as $invoiceItem) {

                $invoiceItem['invoice_id'] = $invoice->id;
                $invoiceItem['amount'] = str_replace(',', '', $invoiceItem['amount']);
                $invoiceItem['tax_amount'] = str_replace(',', '', $invoiceItem['tax_amount']);
                $invoiceItem['billing_amount'] = str_replace(',', '', $invoiceItem['billing_amount']);

                InvoiceItems::create($invoiceItem);
            }
        }


        if(!empty($invoiceFiles)) {
            foreach($invoiceFiles as $invoiceFile) {
                $invoiceFile['invoice_id'] = $invoice->id;

                InvoiceFiles::create($invoiceFile);
            }
        }

//        die();


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Invoice',
            'action' => 'Edit',
            'description' => "Updated Invoice: {$invoice->invoice_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Invoice successfully updated.");
        return redirect("/admin/transactions/invoices/edit/{$invoice->id}");

    }

    public function add_description()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Invoices";

        return view('admin.transactions.invoices_add_description', $data);
    }

    public function preview()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Invoices";

        return view('admin.transactions.preview', $data);
    }

    public function item_list()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Invoices";

        return view('admin.transactions.item_list', $data);
    }

    public function invoice_view(Request $request, $id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Invoices";

        $data['invoice'] = Invoices::find($id);

        return view('admin.transactions.invoices_view', $data);
    }

    public function invoice_print(Request $request, $id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Invoices";

        $data['invoice'] = Invoices::find($id);

        return view('print.invoice', $data);
    }

    public function invoice_send(Request $request, $id)
    {
        $invoice = Invoices::find($id);
        $invoice->status = 'Unpaid';
        $invoice->save();

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Invoice',
            'action' => 'Edit',
            'description' => "Invoice: {$invoice->invoice_number} has been sent",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Invoice successfully sent.");
        return redirect("/admin/transactions/invoices/status/{$invoice->status}");
    }

    public function invoice_delete(Request $request, $id)
    {
        $invoice = Invoices::find($id);

        if($invoice->status != 'Draft') {
            $request->session()->flash('popError', "Only Draft Invoices can be deleted.");
            return redirect()->back();
        }

        InvoiceItems::where('invoice_id', $id)->delete();
        InvoiceFiles::where('invoice_id', $id)->delete();

        Invoices::where('id', $id)->delete();


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Invoice',
            'action' => 'Delete',
            'description' => "Deleted Invoice: {$invoice->invoice_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Invoice successfully deleted.");
        return redirect("/admin/transactions/invoices/status/All");
    }


}
?>
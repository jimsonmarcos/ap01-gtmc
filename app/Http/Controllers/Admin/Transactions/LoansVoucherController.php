<?php
namespace App\Http\Controllers\Admin\Transactions;

use App\AuditLog;
use App\LoanPaymentSchedules;
use App\Loans;
use App\Profiles;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CivilStatus;
use Illuminate\Support\Facades\Auth;

class LoansVoucherController extends Controller
{
    // View Page
    public function create(Request $request)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Loans";

        if(!empty($request->get('user_id'))) {
            $data['user'] = User::find($request->user_id);
        }

        return view('admin.transactions.loans_voucher', $data);
    }

    public function create_post(Request $request)
    {
        $data = $request->except('_token');

//        $data['user_id'] = Profiles::find($data['user_id'])->user_id;

//        if(Loans::where('user_id', $data['user_id'])->where('releasing_status', 'Released')->where('is_paid', 'N')->count() >= 2) {
//            $request->session()->flash('popError', 'User has already 2 Loans! Please settle at least 1 Loan.');
//            return redirect('/admin/transactions/loans');
//        }

        $data['date_request']  = Carbon::createFromFormat('m/d/Y', $data['date_request'] )->format('Y-m-d');
//        dd($data);
        $data['granted_loan_amount'] = !empty($data['loan_amount']) ? str_replace(',', '', $data['loan_amount']) : 0;
        unset($data['loan_amount']);
        $data['check_voucher_id'] = !empty($data['check_voucher_id']) ? $data['check_voucher_id'] : null;
        $data['disbursed_amount'] = !empty($data['disbursed_amount']) ? str_replace(',', '', $data['disbursed_amount']) : 0;
        $data['amortization'] = !empty($data['amortization']) ? $data['amortization'] : 0;
        $data['beginning_balance'] = !empty($data['beginning_balance']) ? str_replace(',', '', $data['beginning_balance']) : 0;
        if($request->has('effective_year') && $request->has('effective_month')) {
            $data['effective_date'] = "{$data['effective_year']}-{$data['effective_month']}-01";
        }
        unset($data['effective_year']);
        unset($data['effective_month']);

        $claNumber = Loans::select('cla_series')->get()->max('cla_series') + 1;
        $data['cla_number'] = Loans::count() == 0 ? "CLA-1001" : "CLA-". $claNumber;
        $data['cla_series'] = Loans::count() == 0 ? 1001 : $claNumber;
        $data['status'] = 'Approved';
        $data['is_voucher'] = 'Y';

//        dd($data);

        $loan = Loans::create($data);



        if($loan->User->role == 'Employee') {
            $payrollGroup = $loan->User->Profile->Compensation->PayrollGroup;
            $effectiveDate = Carbon::createFromFormat('Y-m-d', !empty($loan->effective_date) ? $loan->effective_date : $loan->date_request);
        } else {
            $effectiveDate = Carbon::createFromFormat('Y-m-d', $loan->effective_date);
        }

        for($x = 1; $x <= $loan->months_to_pay; $x++) {
//            echo "{$x} <= {$loan->months_to_pay} <br>";
//            dd(" {$x} < {$loan->months_to_pay}");
//            if($loan->beginning_balance > 0) {
                if($loan->User->role == 'Employee') {
                    $dueDate = $effectiveDate->format('Y')."-".$effectiveDate->format('m')."-";
//                    if($x % 2 == 0) {
                        if($effectiveDate->format('t') >= $payrollGroup->payroll_date_b) {
                            $dueDate .= $payrollGroup->payroll_date_b;
                        } else {
                            $dueDate .= $effectiveDate->format('t');
                        }
//                    $dueDate .= $payrollGroup->payroll_date_b;
//                    } else {
//                        $dueDate .= $payrollGroup->payroll_date_a;
//                    }
                } else {
//                    if($x % 2 == 0) {
                        $dueDate = $effectiveDate->format('Y-m-t');
//                    } else {
//                        $dueDate = $effectiveDate->format('Y-m-15');
//                    }
                }

//                if($x % 2 == 0) {
                    $payPeriod = $effectiveDate->format('Ym')."B";
//                } else {
//                    $payPeriod = $effectiveDate->format('Ym')."A";
//                }



                $paymentSchedule = [
                    'loan_id' => $loan->id,
                    'month' => $x,
                    'pay_period' => $payPeriod,
                    'due_date' => $dueDate,
                    'amortization' => $loan->beginning_balance > $loan->amortization ? $loan->amortization : $loan->beginning_balance,
                    'lapsed_interest' => 0
                ];

                LoanPaymentSchedules::create($paymentSchedule);

//                if($x % 2 == 0) {
                    $effectiveDate = $effectiveDate->addMonth(1);
//                }

                $loan->beginning_balance -= $loan->amortization;
//            }
        }


//        dd('Wow');

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Loan',
            'action' => 'Edit',
            'description' => "Disbursed Loan: {$loan->cla_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', 'The Loan has been created successfully.');
        return redirect(url("/admin/transactions/loans/release/{$loan->id}"));
    }
}
?>
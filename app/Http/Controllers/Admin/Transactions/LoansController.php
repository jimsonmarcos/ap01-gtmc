<?php

namespace App\Http\Controllers\Admin\Transactions;

use App\AuditLog;
use App\CheckVouchers;
use App\LoanPayments;
use App\LoanPaymentSchedules;
use App\Loans;
use App\LoanTypes;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CivilStatus;
use Illuminate\Support\Facades\Auth;

class LoansController extends Controller
{
    // View Page
    public function index(Request $request, $status = 'Request')
    {
        $data['status'] = $status;

        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Loans";

        $view = view('admin.transactions.loans', $data);
        return response($view)->header('Cache-control', 'no-store, no-cache, must-revalidate');
    }

    public function details(Request $request, $id)
    {
        $data['loan'] = $loan = Loans::find($id);

        if (empty($data['loan'])) {
            $request->session()->flash('popError', 'Loan does not exist.');
            return redirect()->url('admin/transactions/loans');
        }

        if ($data['loan']->status == 'Pending') {
            $request->session()->flash('popError', 'Loan status must be Approved to view the loan details.');
            return redirect()->url('admin/transactions/loans');
        }

        if ($data['loan']->status == 'Cancelled') {
            $request->session()->flash('popError', 'This Loan was cancelled.');
            return redirect()->url('admin/transactions/loans');
        }

        $data['pageHeader'] = "Loan Details";
        $data['pageSubtitle'] = "{$loan->User->Profile->id_number} - {$loan->User->Profile->namelfm()}";

        $view = 'loans_request_info';
        if ($data['loan']->is_voucher == 'Y') {
            $view = 'loans_voucher_info';
        }

        return view('admin.transactions.' . $view, $data);
    }

    public function payments_schedule(Request $request, $id)
    {
        $data['loan'] = $loan = Loans::find($id);

        if (empty($data['loan'])) {
            $request->session()->flash('popError', 'Loan does not exist.');
            return redirect()->back();
        }

        if (!Loans::find($id)->exists()) {
            $request->session()->flash('popError', 'Loan does not exist.');
            return redirect()->back();
        }

        $data['paymentSchedules'] = LoanPaymentSchedules::where('loan_id', $id)->orderBy('month')->get();

        $data['pageHeader'] = "Loan Details";
        $data['pageSubtitle'] = "{$loan->User->Profile->id_number} - {$loan->User->Profile->namelfm()}";

        return view('admin.transactions.loans_payment_schedule', $data);
    }

    public function loan_amortization_schedule(Request $request, $id)
    {
        $data['loan'] = $loan = Loans::find($id);

        if (empty($data['loan'])) {
            $request->session()->flash('popError', 'Loan does not exist.');
            return redirect()->back();
        }

        if (!Loans::find($id)->exists()) {
            $request->session()->flash('popError', 'Loan does not exist.');
            return redirect()->back();
        }


        $data['paymentSchedules'] = LoanPaymentSchedules::where('loan_id', $id)->orderBy('month')->get();

        $data['pageHeader'] = "Loan Details";
        $data['pageSubtitle'] = "{$loan->User->Profile->id_number} - {$loan->User->Profile->namelfm()}";

        return view('admin.transactions.loans_amortization_schedule', $data);
    }

    public function payments(Request $request, $id)
    {
        $data['loan'] = $loan = Loans::find($id);

        if (empty($data['loan'])) {
            $request->session()->flash('popError', 'Loan does not exist.');
            return redirect()->back();
        }

        if (!Loans::find($id)->exists()) {
            $request->session()->flash('popError', 'Loan does not exist.');
            return redirect()->back();
        }

        $data['user'] = User::find($data['loan']->user_id);
        $data['payments'] = LoanPayments::where('loan_id', $id)->orderBy('payment_date')->get();

        $data['arNumber'] = LoanPayments::orderBy('ar_number', 'desc')->first();
        if (!empty($data['arNumber'])) {
            $data['arNumber'] = explode('-', $data['arNumber']->ar_number);
            $data['arNumber'] = $data['arNumber'][1] + 1;
        } else {
            $data['arNumber'] = 101;
        }

        $data['pageHeader'] = "Loan Payments";
        $data['pageSubtitle'] = "{$loan->User->Profile->id_number} - {$loan->User->Profile->namelfm()}";

        return view('admin.transactions.loans_request_payments', $data);
    }

    public function payment_add(Request $request, $id)
    {
        $data['loan'] = Loans::find($id);

        $data['arNumber'] = LoanPayments::orderBy('ar_number', 'desc')->first();
        if (!empty($data['arNumber'])) {
            $data['arNumber'] = explode('-', $data['arNumber']->ar_number);
            $data['arNumber'] = $data['arNumber'][1] + 1;
        } else {
            $data['arNumber'] = 101;
        }

        if (empty($data['loan'])) {
            $request->session()->flash('popError', 'Loan does not exist.');
            return redirect()->back();
        }

        if (!Loans::find($id)->exists()) {
            $request->session()->flash('popError', 'Loan does not exist.');
            return redirect()->back();
        }

        $data['currentPayment'] = LoanPaymentSchedules::where(['loan_id' => $id])->whereNotIn('pay_period', LoanPayments::where(['loan_id' => $id])->pluck('pay_period'))->orderBy('month')->first();
        if (empty($data['currentPayment'])) {
            $data['currentPayment'] = LoanPaymentSchedules::where(['loan_id' => $id])->orderByDesc('pay_period')->first();

        }

        $data['pageHeader'] = "Add Payment";

        return view('admin.transactions.loan_payment_add', $data);
    }

    public function payment_add_post(Request $request, $id)
    {
        $data = $request->except('_token');

        $data['loan_id'] = $id;
        $paymentDate = Carbon::createFromFormat('m/d/Y', $data['payment_date'])->format('Ymd');
        $data['payment_date'] = Carbon::createFromFormat('m/d/Y', $data['payment_date'])->format('Y-m-d');
        $data['ar_number'] = "AR-" . $data['ar_number'];

        $claNumber = LoanPayments::count() == 0 ? 1001 : LoanPayments::select('series')->get()->max('series') + 1;
//            P20180320-0001
        $transactionID = "P{$paymentDate}-$claNumber";
//            dd($transactionID);
        $data['transaction_id'] = $transactionID;
        $data['series'] = $claNumber;

//            dd($data);


        $loanPayment = LoanPayments::create($data);

        $loan = Loans::selectRaw('loans.*, COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.lapsed_interest, 0) as lapsed_interest')->where('id', $id);
        $loan = $loan->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id');
        $loan = $loan->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(lapsed_interest) as lapsed_interest FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payment_schedules.loan_id', '=', 'loans.id');

        $loan = $loan->first();


        if (($loan->granted_loan_amount + $loan->lapsed_interest) - $loan->total_amount_paid <= 0) {
            $loan->is_paid = 'Y';
            $loan->save();
        }


        $loan = Loans::find($id);
        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Loan Payment',
            'action' => 'Create',
            'description' => "Added Payment (". number_format($loanPayment->total_amount_paid, 2) .") for Loan: {$loan->cla_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', 'The Payment has been created successfully.');
        return redirect("/admin/transactions/loans/{$id}/payments");
    }

    public function details_edit(Request $request, $id)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Loans";

        $data['loan'] = Loans::find($id);

        if (empty($data['loan'])) {
            $request->session()->flash('popError', 'Loan does not exist.');
            return redirect()->back();
        }

        if ($data['loan']->status == 'Approved') {
            $request->session()->flash('popError', 'Loan status is already approved.');
            return redirect()->back();
        }

        if ($data['loan']->status == 'Cancelled') {
            $request->session()->flash('popError', 'Loan status was already cancelled.');
            return redirect()->back();
        }

        return view('admin.transactions.loans_details', $data);
    }

    public function details_edit_post(Request $request, $id)
    {
        $data = $request->except('_token');


        $loan = Loans::find($id);
        if ($request->has('effective_year') && $request->has('effective_month')) {
            $data['effective_date'] = "{$data['effective_year']}-{$data['effective_month']}-01";
            unset($data['effective_month']);
            unset($data['effective_year']);
        }

        if ($loan->loan_type_id != $data['loan_type_id']) {
            $loanType = LoanTypes::find($data['loan_type_id']);
            $data['interest_percent'] = $loanType->interest;
            $data['service_charge_percent'] = $loanType->service_charge;
            $data['savings_percent'] = $loanType->savings;
        }

        if (!empty($data['check_number_id'])) {
            $checkVoucher = CheckVouchers::find($data['check_voucher_id']);
            $checkVoucher->is_used = 'Y';
            $checkVoucher->save();
        }
//            dd($data);

        Loans::find($id)->update($data);

        if ($request['status'] == 'Approved') {
            $loan = Loans::find($id);

            // Pay Selected Existing loan by calculating outstanding balance..
//                if(!empty($loan->cla_number_reference)) {
//                    $loanReferenced = Loans::find($loan->cla_number_reference);
//                    $paymentSchedules = \App\LoanPaymentSchedules::where('loan_id', $loanReferenced->id)->get();
//
//                    $paymentsLeft = ($paymentSchedules->sum('amortization') + $paymentSchedules->sum('lapsed_interest')) - $loanReferenced->Payments->sum('total_amount_paid');
//                }

            if ($loan->User->role == 'Employee') {
                $payrollGroup = $loan->User->Profile->Compensation->PayrollGroup;
                $effectiveDate = Carbon::createFromFormat('Y-m-d', !empty($loan->effective_date) ? $loan->effective_date : $loan->date_request);
            } else {
                $effectiveDate = Carbon::createFromFormat('Y-m-d', $loan->effective_date);
            }

//                dd($effectiveDate);


            if (!empty($loan->cla_number_reference)) {
                $unpaidLoan = Loans::where('cla_number', $loan->cla_number_reference)->first();
                $pendingPayments = LoanPaymentSchedules::where('loan_id', $unpaidLoan->id)->whereNotIn('pay_period', LoanPayments::where('loan_id', $unpaidLoan->id)->pluck('pay_period'))->get();
                foreach ($pendingPayments as $pendingPayment) {
                    $latestPaymentSeries = LoanPayments::max('series');
                    $series = !empty($latestPaymentSeries) ? $latestPaymentSeries + 1 : 1001;
                    $transaction_id = "LD" . date('Ymd') . "-{$series}";

                    $arNumber = LoanPayments::orderBy('ar_number', 'desc')->first();
                    if (!empty($arNumber)) {
                        $arNumber = explode('-', $arNumber->ar_number);
                        $arNumber = $arNumber[1] + 1;
                    } else {
                        $arNumber = 101;
                    }

                    $arNumber = "AR-" . $arNumber;

                    $payment = [
                        'transaction_id' => $transaction_id,
                        'series' => $series,
                        'loan_id' => $pendingPayment->loan_id,
                        'ar_number' => $arNumber,
                        'remarks' => 'LOAN DEDUCTION',
                        'payment_method' => 'Cash',
                        'payment_date' => date('Y-m-d'),
                        'amount' => $pendingPayment->amortization,
                        'interest' => $pendingPayment->lapsed_interest,
                        'total_amount_paid' => $pendingPayment->amortization + $pendingPayment->lapsed_interest,
                        'pay_period' => $pendingPayment->pay_period
                    ];

                    LoanPayments::create($payment);
                }


                $unpaidLoan->is_paid = 'Y';
                $unpaidLoan->save();

            }

            for ($x = 1; $x <= $loan->months_to_pay * 2; $x++) {
                if ($loan->User->role == 'Employee') {
                    $dueDate = $effectiveDate->format('Y') . "-" . $effectiveDate->format('m') . "-";
                    if ($x % 2 == 0) {
                        if ($effectiveDate->format('t') >= $payrollGroup->payroll_date_b) {
                            $dueDate .= $payrollGroup->payroll_date_b;
                        } else {
                            $dueDate .= $effectiveDate->format('t');
                        }
//                            $dueDate .= ($payrollGroup->payroll_date_b > $effectiveDate->format('t')) ? $effectiveDate->format('t') : $payrollGroup->payroll_date_b;
                    } else {
                        $dueDate .= $payrollGroup->payroll_date_a;
                    }
                } else {
                    if ($x % 2 == 0) {
                        $dueDate = $effectiveDate->format('Y-m-t');
                    } else {
                        $dueDate = $effectiveDate->format('Y-m-15');
                    }
                }

                if ($x % 2 == 0) {
                    $payPeriod = $effectiveDate->format('Ym') . "B";
                } else {
                    $payPeriod = $effectiveDate->format('Ym') . "A";
                }


                $paymentSchedule = [
                    'loan_id' => $loan->id,
                    'user_id' => $loan->user_id,
                    'month' => $x,
                    'pay_period' => $payPeriod,
                    'due_date' => $dueDate,
                    'amortization' => $loan->amortization,
                    'lapsed_interest' => 0
                ];

                LoanPaymentSchedules::create($paymentSchedule);

                if ($x % 2 == 0) {
                    $effectiveDate = $effectiveDate->addMonth(1);
                }
            }


            AuditLog::create([
                'user_id' => Auth::user()->id,
                'category' => 'Loan',
                'action' => 'Edit',
                'description' => "Approved Loan: {$loan->cla_number}",
                'is_admin' => session('is_admin') ? 'Y' : 'N'
            ]);

            $request->session()->flash('popSuccess', "{$loan->cla_number} has been approved.");
            return redirect(url("/admin/transactions/loans/release/{$id}"));
        } else {
            AuditLog::create([
                'user_id' => Auth::user()->id,
                'category' => 'Loan',
                'action' => 'Edit',
                'description' => "Updated Loan: {$loan->cla_number}",
                'is_admin' => session('is_admin') ? 'Y' : 'N'
            ]);

            $request->session()->flash('popSuccess', "{$loan->cla_number} has been updated.");
            return redirect()->back();
        }


    }

    public function disbursed(Request $request, $id)
    {
        $loan = $data['loan'] = Loans::find($id);

        if (empty($data['loan'])) {
            $request->session()->flash('popError', 'Loan does not exist.');
            return redirect()->back();
        }

        if ($loan['status'] != 'Approved') {
            $request->session()->flash('popError', 'Loan status must be <strong>approved</strong> to proceed.');
            return redirect()->back();
        }

        if ($loan['relasing_status'] == 'Released') {
            $request->session()->flash('popError', 'This Loan was already released.');
            return redirect()->back();
        }

        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Loans";


        return view('admin.transactions.loans_disbursed', $data);
    }

    public function disbursed_post(Request $request, $id)
    {
        $data = $request->except('_token');
        $data['released_date'] = Carbon::createFromFormat('m/d/Y', $data['released_date'])->format('Y-m-d');
        $data['releasing_status'] = 'Released';

        Loans::find($id)->update($data);

        $loan = Loans::find($id);
        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Loan',
            'action' => 'Edit',
            'description' => "Disbursed Loan: {$loan->cla_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Disbursed loan has been updated");
        return redirect('/admin/transactions/loans');
    }
}

?>
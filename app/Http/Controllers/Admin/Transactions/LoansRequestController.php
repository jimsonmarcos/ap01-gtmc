<?php
namespace App\Http\Controllers\Admin\Transactions;

use App\AuditLog;
use App\LoanPaymentSchedules;
use App\Loans;
use App\Profiles;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CivilStatus;
use Illuminate\Support\Facades\Auth;

class LoansRequestController extends Controller
{
    // View Page
    public function create(Request $request)
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Loans";

        if(!empty($request->get('user_id'))) {
            $data['user'] = User::find($request->user_id);

            $unpaidLoans = Loans::selectRaw("loans.*, COALESCE(total_amount_paid, 0) total_amount_paid, COALESCE(lapsed_interest, 0) lapsed_interest")->where('user_id', $request->user_id)->where('status', 'Approved')
                ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')
                ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(lapsed_interest) as lapsed_interest FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payment_schedules.loan_id', '=', 'loans.id')
                ->whereRaw("((granted_loan_amount - COALESCE(total_amount_paid, 0)) > 0)")
                ->where('loans.is_voucher', 'N');
            if($unpaidLoans->count() > 0) {
                $data['unpaidLoans'] = $unpaidLoans->with('payments')->orderBy('cla_series')->get();
            }

        }
//        dd($data['user']->Profile);

        return view('admin.transactions.loans_request', $data);
    }

    public function create_post(Request $request)
    {
        $data = $request->except('_token');


        $unpaidLoans = Loans::where('user_id', $request->user_id)->where('status', 'Approved')
            ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')
            ->whereRaw("((granted_loan_amount - COALESCE(total_amount_paid, 0)) > 0)")
            ->where('loans.is_voucher', 'N');
//        if($unpaidLoans->count() == 2 && empty($data['cla_number_reference'])) {
//            $request->session()->flash('popError', 'User has already 2 Loans! Please settle at least 1 Loan.');
//            return redirect('/admin/transactions/loans');
//        }

//        Reached Maxium Loan Request, Please Select 1 Existing Loan

//        if(Loans::where('user_id', $data['user_id'])->where('releasing_status', 'Released')->where('is_paid', 'N')->count() > 2) {
//        $activeLoans = Loans::where(['user_id' => $request->user_id, 'is_paid' => 'N'])->whereIn('status', ['Approved', 'Pending'])->where('loans.is_voucher', 'N')->count();
//        if($activeLoans >= 2 && empty($data['cla_number_reference'])) {
//            $request->session()->flash('popError', 'User has already 2 Loans! Please settle at least 1 Loan.');
//            return redirect('/admin/transactions/loans');
//        }

        $data['user_id'] = $request->user_id;

        $data['date_request']  = Carbon::createFromFormat('m/d/Y', $data['date_request'] )->format('Y-m-d');

        $claNumber = Loans::select('cla_series')->get()->max('cla_series') + 1;
        $data['cla_number'] = Loans::count() == 0 ?  "CLA-1001" : "CLA-{$claNumber}";
        $data['cla_series'] = Loans::count() == 0 ? 1001 : $claNumber;

//        dd($data);

        $loan = Loans::create($data);

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Loan',
            'action' => 'Create',
            'description' => "Created Loan: {$loan->cla_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', 'The Loan has been created successfully.');
        return redirect('/admin/transactions/loans');
    }
}
?>
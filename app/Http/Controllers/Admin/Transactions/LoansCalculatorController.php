<?php
namespace App\Http\Controllers\Admin\Transactions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CivilStatus;

class LoansCalculatorController extends Controller
{
    // View Page
    public function create()
    {
        $data['sidebar'] = view("admin.sidebar");
        $data['pageHeader'] = "Loans Calculator";

        return view('admin.transactions.loans_calculator', $data);
    }
}
?>
<?php

namespace App\Http\Controllers\Admin;

use App\AuditLog;
use App\Beneficiaries;
use App\Companies;
use App\Compensations;
use App\CostCenters;
use App\Departments;
use App\EmploymentDetails;
use App\LeaveHistory;
use App\Malls;
use App\Outlets;
use App\PayrollGroups;
use App\Profiles;
use App\Provinces;
use App\TransferModes;
use App\User;
use App\UserSeries;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CivilStatus;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function __construct()
    {

    }

    public function index($status = 'Active')
    {
//        $data['users'] = User::join('profiles', 'profiles.user_id', '=', 'users.id')->whereIn('membership_type', array('Employee', 'Member'))->get();

        $data['status'] = $status;
        $data['pageHeader'] = "User Profiles";
        $data['sidebar'] = view ("admin.sidebar");

        return view('admin.user.index', $data);
    }

    public function add()
    {
        $data['sidebar'] = view ("admin.sidebar");
        $data['pageHeader'] = "Add New User";
//        $data['costCenters'] = CostCenters::orderBy('cost_center')->get();
        $data['principals'] = Companies::orderBy('company_code')->get();
//        $data['payrollGroups'] = PayrollGroups::orderBy('group_name')->get();

        return view('admin.user.add', $data);
    }

    public function add_post(Request $request)
    {
        $data = $request->except(['_token']);
        $joinDate = $data['join_date'];
        $joinDate = Carbon::createFromFormat('m/d/Y', $joinDate)->format('Y-m-d');

        $profile = [
            'first_name' => strtoupper($data['first_name']),
            'middle_name' => strtoupper($data['middle_name']),
            'last_name' => strtoupper($data['last_name']),
            'membership_type' => strtoupper($data['membership_type']),
            'membership_category' => !empty($data['membership_category']) ? $data['membership_category'] : 'Employee',
            'principal_id' => $data['principal_id'],
            'cost_center_id' => $data['cost_center_id'],
            'position_id' => !empty($data['position_id']) ? $data['position_id'] : null,
            'payroll_group_id' => !empty($data['payroll_group_id']) ? $data['payroll_group_id'] : null,
            'id_number' => $data['id_number'],
            'join_date' => $joinDate,
        ];

        if(empty($data['principal_id'])) {
            $request->session()->flash('error', 'Please select Principal to proceed');
            return redirect()->back()->withInput($profile);
        }

        if(empty($data['cost_center_id'])) {
            $request->session()->flash('error', 'Please select Cost Center to proceed');
            return redirect()->back()->withInput($profile);
        }

        if(strtoupper($profile['membership_category']) == 'Employee') {
            if(empty($data['payroll_group_id'])) {
                $request->session()->flash('error', 'Please select Payroll Group to proceed');
                return redirect()->back()->withInput($profile);
            }
            if(empty($data['position_id'])) {
                $request->session()->flash('error', 'Please select Position to proceed');
                return redirect()->back()->withInput($profile);
            }
        }

//        dd($profile);

        if(Profiles::where('first_name', $profile['first_name'])->where('middle_name', $profile['middle_name'])->where('last_name', $profile['last_name'])->exists()) {
            $request->session()->flash('error', 'Name already exists!');
            return redirect()->back()->withInput($profile);
        }

        // Store initial User Profile to session
        $request->session()->put('user.profile', $profile);


        // Redirect to employee form if membership category is selected
        // otherwise, redirect to member registration form page.
        if($profile['membership_category'] == 'Employee') {
            return redirect('admin/employee/add');
        }

        return redirect('admin/member/add');
    }

    public function complete_registration(Request $request)
    {
        $profile = session('user.profile');
//        dd(session('user'));
        if(!empty($profile['photo_path'])) {
            $photo_path = Storage::move($profile['photo_path'], 'public/'.str_replace('tmp/', 'user_photo/', $profile['photo_path']));
            $profile['photo_path'] = "storage/".str_replace('tmp/', 'user_photo/', $profile['photo_path']);
        }
//        dd($profile['photo_path']);
        $beneficiaries = session('user.beneficiaries');
        $compensation = session('user.compensation');

        $username = substr($profile['id_number'], strripos($profile['id_number'], '-') + 1);
//        dd($username);
        $password = strtolower(trim($profile['first_name']). $username);
        $series = str_replace('E', '', $username);
        $series = str_replace('M', '', $series);
//        dd($series);

        while(User::where('username', $username)->count() == 1) {
            $newSeries = \App\UserSeries::min('series');

            if($profile['membership_category'] == 'Employee') {
                $newUsername = "E{$newSeries}";
            } else {
                $newUsername = "M{$newSeries}";
            }

            $profile['id_number'] = str_replace($username, $newUsername, $profile['id_number']);
            $username = $newUsername;
            $series = $newSeries;
        }

        if(User::where('username', $username)->count() == 1) {
            dd('Username already exists!');
        }

        // CREATE USER
        $user = User::create([
            'name' => trim($profile['first_name']) ." ".trim($profile['middle_name'])." ".trim($profile['last_name']),
            'series' => UserSeries::count() == 0 ? User::get()->max('series') + 1 : $series,
            'username' => $username,
            'email' => strtolower($profile['email']),
            'password' => bcrypt( $username ),
//            'password' => bcrypt( $password ),
//            'verification_hash' => str_random(48),
            'role' => $profile['membership_category']
        ]);

        if(UserSeries::where('series', $series)->count() == 1) {
            UserSeries::where('series', $series)->delete();
        }

        $profile['user_id'] = $user->id;

        $employmentDetails = [
            'user_id' => $user->id,
            'principal_id' => !empty($profile['principal_id']) ? $profile['principal_id'] : '',
            'position_id' => !empty($profile['position_id']) ? $profile['position_id'] : '',
//            'department_id' => !empty($profile['department_id']) ? $profile['department_id'] : '',
            'province_id' => !empty($profile['province_id']) ? $profile['province_id'] : '',
            'area_id' => !empty($profile['area_id']) ? $profile['area_id'] : '',
            'mall_id' => !empty($profile['mall_id']) ? $profile['mall_id'] : '',
            'outlet_id' => !empty($profile['outlet_id']) ? $profile['outlet_id'] : '',
            'coordinator_id' => !empty($profile['coordinator_id']) ? $profile['coordinator_id'] : '',
            'hire_date' => !empty($profile['join_date']) ? $profile['join_date'] : ''
        ];

        unset($profile['principal_id']);
        unset($profile['province_id']);
        unset($profile['area_id']);
        unset($profile['mall_id']);
        unset($profile['outlet_id']);
        unset($profile['coordinator_id']);
        unset($profile['position_id']);

//        if(!empty($compensation)) {
//            $compensation['hire_date'] = $compensation['join_date'];
//        }

        $compensation['user_id'] = $user->id;

        Profiles::create($profile);
        EmploymentDetails::create($employmentDetails);
        Compensations::create($compensation);


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'User Management',
            'action' => 'Create',
            'description' => "Registration completed for {$profile['id_number']}"
        ]);

        $request->session()->flash('popSuccess', "User has been successfully created!");
        return redirect("admin/user/{$user->id}/profile");
    }

    public function profile($id)
    {
        $data['id'] = $id;
        $data['pageHeader'] = "User Profiles";
        $data['user'] = Profiles::where('user_id', $id)->first();
//        dd($data['user']->Beneficiaries);
        $data['sidebar'] = view ("admin.sidebar");

        return view('admin.user.profile', $data);
    }

    public function profile_edit($id)
    {
        $data['id'] = $id;
        $data['pageHeader'] = "User Profiles";
        $data['user'] = Profiles::where('user_id', $id)->first();

        $data['sidebar'] = view ("admin.sidebar");

        return view('admin.user.profile_edit', $data);
    }

    public function profile_edit_post(Request $request, $id)
    {
        $data = $request->except('_token');
        $data['birthday'] = Carbon::createFromFormat('m/d/Y', $data['birthday'])->format('Y-m-d');

        $user = Profiles::where('user_id', $id)->first();

        $data['first_name'] = strtoupper($data['first_name']);
        $data['middle_name'] = strtoupper($data['middle_name']);
        $data['last_name'] = strtoupper($data['last_name']);

        $user->update($data);

        User::find($id)->update(['name' => trim($data['first_name']) ." ".trim($data['middle_name']) ." ".trim($data['last_name'])]);

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'User Management',
            'action' => 'Edit',
            'description' => "{$user->id_number} profile updated"
        ]);

        return redirect("admin/user/{$id}");
    }

    public function employment_details($id)
    {
        $data['id'] = $id;
        $data['pageHeader'] = "User Profiles";
        $data['user'] = Profiles::where('user_id', $id)->with('employmentdetails.position')->first();

        $data['sidebar'] = view ("admin.sidebar");

        return view('admin.user.employment_details', $data);
    }

    public function employment_details_edit($id)
    {
        $data['id'] = $id;
        $data['pageHeader'] = "User Profiles";
        $data['user'] = Profiles::where('user_id', $id)->with('employmentdetails.position')->first();
        $data['employmentDetails'] = $data['user']->employmentdetails;
        $data['department'] = $department = !empty($data['user']->employmentdetails->Department) ? $data['user']->employmentdetails->Department : Departments::orderBy('province')->first();
//        dd( $data['user']->employmentdetails);

        $data['coordinators'] = EmploymentDetails::where('position_id', 3)->get();
        $data['provinces'] = Provinces::orderBy('province')->get();
        $data['malls'] = Malls::orderBy('mall')->get();
        $data['outlets'] = Outlets::orderBy('outlet')->get();


//        if($data['user']->EmploymentDetails->position_id == 1 || $data['user']->EmploymentDetails->position_id == 2) {
//            $data['provinces'] = Departments::groupBy('province')->orderBy('province')->get();
//            $data['areas'] = Departments::where('province', $department->province)->groupBy('area')->orderBy('area')->get();
//            $data['malls'] = Departments::where('province', $department->province)->where('area', $department->area)->groupBy('mall')->orderBy('mall')->get();
//            $data['outlets'] = Departments::where('province', $department->province)->where('area', $department->area)->where('mall', $department->mall)->groupBy('outlet')->orderBy('outlet')->get();
//            $data['departments'] = Departments::where('province', $department->province)->where('area', $department->area)->where('mall', $department->mall)->where('outlet', $department->outlet)->groupBy('department')->orderBy('department')->get();

//        }


        $data['sidebar'] = view ("admin.sidebar");

        return view('admin.user.employment_details_edit', $data);
    }

    public function employment_details_edit_post(Request $request, $user_id)
    {
        $data = $request->except('_token');
        $data['hire_date'] = Carbon::createFromFormat('m/d/Y', $data['hire_date'])->format('Y-m-d');


        $data['coordinator_id'] = $request->has('coordinator_id') ? $data['coordinator_id'] : NULL;
        $employmentDetails = EmploymentDetails::where('user_id', $user_id)->first();
        $employmentDetails->hire_date = $data['hire_date'];
        $employmentDetails->principal_id = $data['principal_id'];

        if($employmentDetails->position_id == 1 && $data['position_id'] == 2) {
            $profile = Profiles::where('user_id', $user_id)->first();
            $user = User::find($user_id);
//            $profile->id_number = "GTMC-00-00-{$user->username}";
//            $profile->save();
        }

        $employmentDetails->position_id = $data['position_id'];
        $employmentDetails->coordinator_id = $data['coordinator_id'];
        $employmentDetails->trainee_start_date = NULL;
        $employmentDetails->trainee_end_date = NULL;
        $employmentDetails->department_id = NULL;

//        dd($data);

        if($data['position_id'] == 1) {
            if($request->has('trainee_start_date')) {
                $data['trainee_start_date'] = Carbon::createFromFormat('m/d/Y', $data['trainee_start_date'])->format('Y-m-d');
                $employmentDetails->trainee_start_date = $data['trainee_start_date'];
            }

            if($request->has('trainee_end_date')) {
                $data['trainee_end_date'] = Carbon::createFromFormat('m/d/Y', $data['trainee_end_date'])->format('Y-m-d');
                $employmentDetails->trainee_end_date = $data['trainee_end_date'];
            }

            $employmentDetails->orientation = $request->has('orientation') ? 'Y' : 'N';
        }

        // if(in_array($data['position_id'], [1, 2])) {
//            $employmentDetails->department_id = !empty($data['department_id']) ? $data['department_id'] : '';
            $employmentDetails->province_id = !empty($data['province_id']) ? $data['province_id'] : '';
            $employmentDetails->area_id = !empty($data['area_id']) ? $data['area_id'] : '';
            $employmentDetails->mall_id = !empty($data['department_id']) ? $data['mall_id'] : '';
            $employmentDetails->outlet_id = !empty($data['outlet_id']) ? $data['outlet_id'] : '';
        // }

        $employmentDetails->save();



        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'User Management',
            'action' => 'Edit',
            'description' => Profiles::where('user_id', $user_id)->first()->id_number ." Employment details updated"
        ]);

//        EmploymentDetails::where('user_id', $user_id)->first()->update($data);
        $request->session()->flash('popSuccess', 'Employment Details update successful!');
        return redirect("/admin/user/{$user_id}/employment-details");
    }

    public function compensations($id)
    {
        $data['id'] = $id;
        $data['pageHeader'] = "User Profiles";
        $data['user'] = Profiles::where('user_id', $id)->with('employmentdetails')->first();
        $data['employmentDetails'] = $data['user']->EmploymentDetails;
        $data['compensations'] = Compensations::where('user_id', $data['user']->user_id)->first();
        $data['leaveHistory'] = LeaveHistory::select(['leave_history.*', 'payroll_cycles.payroll_date'])->where('leave_history.user_id', $data['user']->user_id)->where('effective_year', date('Y'))->join('payroll_cycles', 'payroll_cycles.id', '=', 'leave_history.payroll_cycle_id')->orderByDesc('payroll_cycles.payroll_date')->get();

//        dd($data['compensations']->DailyRate);
        $data['sidebar'] = view ("admin.sidebar");

        return view('admin.user.compensations', $data);
    }

    public function compensations_edit(Request $request, $id)
    {
        $data['id'] = $id;
        $data['pageHeader'] = "User Profiles";
        $data['user'] = Profiles::where('user_id', $id)->first();
        $data['employmentDetails'] = $data['user']->EmploymentDetails;
        $data['compensation'] = $compensation = Compensations::where('user_id', $data['user']->user_id)->first();

        $data['transferModes'] = TransferModes::groupBy('transfer_mode')->orderBy('transfer_mode')->get();
        if(empty($compensation->TransferMode->transfer_mode)) {
            $data['transferTypes'] = TransferModes::where('transfer_mode', $data['transferModes']->first()->transfer_mode)->groupBy('transfer_type')->orderBy('transfer_type')->get();

        } else {
            $data['transferTypes'] = TransferModes::where('transfer_mode', $compensation->TransferMode->transfer_mode)->groupBy('transfer_type')->orderBy('transfer_type')->get();
        }
        if(empty($compensation->TransferMode->transfer_type)) {
            $data['banks'] = TransferModes::where('transfer_mode', $data['transferModes']->first()->transfer_mode)->where('transfer_type', $data['transferModes']->first()->transfer_type)->orderBy('bank')->get();
        } else {
            $data['banks'] = TransferModes::where('transfer_mode', $compensation->TransferMode->transfer_mode)->where('transfer_type', $compensation->TransferMode->transfer_type)->orderBy('bank')->get();
        }


//        dd($data['compensations']->DailyRate);
        $data['sidebar'] = view ("admin.sidebar");

        return view('admin.user.compensations_edit', $data);
    }

    public function compensations_edit_post(Request $request, $user_id)
    {
        $data = $request->except('_token');
//        dd($data);
        $principal_id = $request->principal_id;
        EmploymentDetails::where('user_id', $user_id)->update(['principal_id' => $principal_id]);
        unset($data['principal_id']);

        $data['compute_sss'] = $request->has('compute_sss') ? 'Y' : 'N';
        $data['compute_hdmf'] = $request->has('compute_hdmf') ? 'Y' : 'N';
        $data['compute_philhealth'] = $request->has('compute_philhealth') ? 'Y' : 'N';
        $data['tax_type_id'] = $request->has('tax_type_id') ? $data['tax_type_id'] : null;

        Compensations::where('user_id', $user_id)->first()->update($data);


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'User Management',
            'action' => 'Edit',
            'description' => Profiles::where('user_id', $user_id)->first()->id_number ." Compensation updated"
        ]);

        $request->session()->flash('popSuccess', 'Compensation update successful!');
        return redirect("admin/user/{$user_id}/compensations-leave-credits");
    }

    public function loans_shares(Request $request, $id)
    {
        $data['id'] = $id;
        $data['pageHeader'] = "User Profiles";
        $data['user'] = Profiles::where('user_id', $id)->first();

        $data['beneficiaries'] = $data['user']->Beneficiaries;
//        dd($data['beneficiaries']);
        $data['sidebar'] = view ("admin.sidebar");

        return view('admin.user.loans_shares', $data);
    }

    public function loans_shares_post(Request $request, $id)
    {
        if(!empty($request->coop_share)) {
            Profiles::where('user_id', $id)->update(['coop_share' => $request->coop_share, 'hdmf_additional' => $request->hdmf_additional, 'cost_center_id' => $request->cost_center_id]);
            $request->session()->flash('popSuccess', 'Loans and Shares successfully updated!');

            AuditLog::create([
                'user_id' => Auth::user()->id,
                'category' => 'User Management',
                'action' => 'Edit',
                'description' => Profiles::where('user_id', $id)->first()->id_number ." Compensation updated"
            ]);
        }





        return redirect()->back();
    }

    public function beneficiary_add(Request $request, $user_id)
    {
        $data = $request->except('_token');
        $data['birthday'] = Carbon::createFromFormat('m/d/Y', $data['birthday'])->format('Y-m-d');
        $data['user_id'] = $user_id;

        Beneficiaries::create($data);

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'User Management',
            'action' => 'Create',
            'description' => Profiles::where('user_id', $user_id)->first()->id_number ." Beneficiary created"
        ]);

        $request->session()->flash('popSuccess', 'Beneficiary successfully created!');
        return redirect()->back();
    }

    public function add_admin_success(Request $request)
    {
//        dd($request->except(['_token']));


//        $data['pageHeader'] = "Add New Admin";

//        $data['newUsername'] = $request['username'];
//        $data['adminType'] = $request['role'];
//
//        $data['sidebar'] = view('admin.sidebar');
//
//        return view('admin.user.add_admin_success', $data);
    }

    public function upload_photo(Request $request, $user_id)
    {
//        dd($request->profile_picture)
        $file =  $request->profile_picture->store('public/user_photo');
        $photo_path = ltrim(Storage::url($file), '/');

        $profile = Profiles::where('user_id', $user_id)->first();
        $profile->photo_path = $photo_path;
        $profile->save();

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'User Management',
            'action' => 'Edit',
            'description' => Profiles::where('user_id', $user_id)->first()->id_number ." Profile Picture updated"
        ]);

        $request->session()->flash('popSuccess', 'Profile Picture successfully updated!');
        return redirect()->back();
    }

    public function deactivate_user($id)
    {
        $data['id'] = $id;
        $data['user'] = Profiles::where('user_id', $id)->first();

        $data['pageHeader'] = 'Deactivate User';

        return view('admin.user.deactivate_user', $data);
    }

    public function deactivate_user_post(Request $request, $id)
    {
        $data['id'] = $id;

        $profile = Profiles::where('user_id', $id)->first();
        if(Carbon::createFromFormat('m/d/Y', $request->effective_date)->format('Y-m-d') <= date('Y-m-d')) {
            $profile->status = 'Inactive';
            $profile->save();

            $request->session()->flash('popSuccess', "The account of <strong>{$profile->name()}</strong> has been changed to <strong>Inactive</strong>!");
        } else {
            $request->session()->flash('popSuccess', "The account of <strong>{$profile->name()}</strong> will be <strong>Inactive</strong> on {$request->effective_date}");
        }


        $employmentDetails = $profile->EmploymentDetails;
        $employmentDetails->resignation_date = Carbon::createFromFormat('m/d/Y', $request->resignation_date)->format('Y-m-d');
        $employmentDetails->effective_date = Carbon::createFromFormat('m/d/Y', $request->effective_date)->format('Y-m-d');
        $employmentDetails->reason = $request->reason;
        $employmentDetails->remarks = $request->remarks;
        $employmentDetails->save();



        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'User Management',
            'action' => 'Edit',
            'description' => $profile->id_number ." deactivated"
        ]);

        return redirect(url("admin/user/{$id}/profile"));
    }
}

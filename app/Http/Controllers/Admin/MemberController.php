<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CivilStatus;

class MemberController extends Controller
{
    //

    public function add()
    {
        $data['sidebar'] = view ("admin.sidebar");
        $data['pageHeader'] = "Member's Registration";

        $data['profile'] = session('user.profile');

        return view('admin.member.add_profile', $data);
    }

    public function add_post(Request $request)
    {
        $data = $request->except(['_token']);

        $p = session('user.profile');

        $profile = [
            'birthday' => Carbon::createFromFormat('d/m/Y', $data['birthday'])->format('Y-m-d'),
            'gender' => $data['gender'],
            'address' => strtoupper( $data['address']),
            'mothers_maiden_name' => strtoupper($data['mothers_maiden_name']),
            'civil_status_id' => $data['civil_status_id'],
            'educational_attainment_id' => $data['educational_attainment_id'],
            'mobile1' => $data['mobile1'],
            'mobile2' => $data['mobile2'],
            'email' => strtolower($data['email']),
//            'join_date' => $data['join_date'],
            'icoe_contact_name' => $data['icoe_contact_name'],
            'icoe_contact_mobile' => $data['icoe_contact_mobile'],
            'icoe_contact_address' => $data['icoe_contact_address']
        ];

        if($request->hasFile('photo')) {
            $profile['photo_path'] = $request->photo->store("tmp");
        }

        $profile = array_merge($p, $profile);
        // Store initial User Profile to session
        $request->session()->put('user.profile', $profile);

//        dd(session('user.profile'));

        return redirect('admin/member/add/coop');
    }

    public function add_coop()
    {
        $data['profile'] = session('user.profile');
        $data['pageHeader'] = "Member's Registration";
        $data['sidebar'] = view ("admin.sidebar");

        return view('admin.member.add_coop', $data);
    }

    public function add_coop_post(Request $request)
    {
        $data = $request->except(['_token']);

        $p = session('user.profile');

        $p['coop_share'] = $data['coop_share'];

        $request->session()->put('user.profile', $p);

        if(!empty($data['beneficiaries'])) {
            $beneficiaries = $data['beneficiaries'];
            $request->session()->put('user.beneficiaries', $beneficiaries);
        }


        return redirect('admin/complete-registration');
    }

    public function add_compensation()
    {
        $data['sidebar'] = view ("admin.sidebar");

        return view('admin.member.add_compensation', $data);
    }
}

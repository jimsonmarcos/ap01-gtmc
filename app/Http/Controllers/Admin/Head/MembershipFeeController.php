<?php

namespace App\Http\Controllers\Admin\Head;

use App\Keyval;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MembershipFeeController extends Controller
{
    public function index()
    {
        $data['pageHeader'] = "Membership Fee";

        return view('admin.head.membership_fee', $data);
    }

    public function save(Request $request)
    {

        Keyval::where('key', 'membership_fee')->update(['value' => $request->membership_fee]);

        $request->session()->flash('popSuccess', 'Membership Fee successfully updated.');
        return redirect()->back();
    }
}

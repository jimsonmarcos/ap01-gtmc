<?php

namespace App\Http\Controllers\Admin\Head;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EarningTypeController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Income Type";

        return view('admin.head.earning_type', $data);
    }

    public function create_earning_type()
    {
        $data['pageHeader'] = "New Income Type";

        return view('admin.head.earning_type_create', $data);
    }
}

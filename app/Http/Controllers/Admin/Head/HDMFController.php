<?php

namespace App\Http\Controllers\Admin\Head;

use App\HDMFContributions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HDMFController extends Controller
{
    //
    public function hdmf_schedule()
    {
        $data['pageHeader'] = "Admin Settings";
        $data['HDMFContributions'] = HDMFContributions::orderBy('salary_range_start')->get();

        return view('admin.head.table.hdmf_schedule', $data);
    }

    public function hdmf_schedule_update()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.table.update_hdmf_schedule', $data);
    }

    public function create_hdmf_schedule_post(Request $request)
    {
        HDMFContributions::create($request->except('_token'));

        $request->session()->flash('popSuccess', "HDMF Salary Bracket successfully added");
        return redirect()->route('head_hdmf_schedule');

    }

    public function hdmf_schedule_edit(Request $request,$id)
    {
        if(HDMFContributions:: where('id', $id)->exists()) {
            HDMFContributions:: where('id', $id)->update($request->except('_token'));
            $request->session()->flash('popSuccess', 'HDMF Salary Bracket Successfully updated');
        } else {
            $request->session()->flash('popError', 'HDMF Contribution does not exists');
        }


        return redirect()->back();
    }
    public function hdmf_schedule_delete(Request $request, $id)
    {
        if(HDMFContributions:: where('id', $id)->exists()) {
            HDMFContributions:: where('id', $id)->delete();
            $request->session()->flash('popSuccess', 'HDMF Salary Bracket Successfully deleted');
        } else {
            $request->session()->flash('popError', 'HDMF Salary Bracket does not exists');
        }

        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin\Head;

use App\SSSContributions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SSSController extends Controller
{
    //
    public function sss_schedule()
    {
        $data['pageHeader'] = "Admin Settings";
        $data['SSSContributions'] = SSSContributions::orderBy('compensation_range_start')->get();

        return view('admin.head.table.sss_schedule', $data);
    }

    public function sss_schedule_update()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.table.update_sss_schedule', $data);
    }

    public function create_sss_schedule_post(Request $request)
    {
        SSSContributions::create($request->except('_token'));

        $request->session()->flash('popSuccess', "SSS Salary Bracket successfully added");
        return redirect()->route('head_sss_schedule');

    }

    public function sss_schedule_edit(Request $request,$id)
    {
        if(SSSContributions::where('id', $id)->exists()) {
            SSSContributions::where('id', $id)->update($request->except('_token'));
            $request->session()->flash('popSuccess', 'SSS Salary Bracket Successfully updated');
        } else {
            $request->session()->flash('popError', 'SSS Contribution does not exists');
        }


        return redirect()->back();
    }
    public function sss_schedule_delete(Request $request, $id)
    {
        if(SSSContributions::where('id', $id)->exists()) {
            SSSContributions::where('id', $id)->delete();
            $request->session()->flash('popSuccess', 'SSS Salary Bracket Successfully deleted');
        } else {
            $request->session()->flash('popError', 'SSS Salary Bracket does not exists');
        }

        return redirect()->back();
    }
}

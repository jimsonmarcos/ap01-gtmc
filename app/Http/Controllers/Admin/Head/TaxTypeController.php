<?php

namespace App\Http\Controllers\Admin\Head;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaxTypeController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Tax Type";

        return view('admin.head.tax_type', $data);
    }

    public function create_tax_type()
    {
        $data['pageHeader'] = "New Tax Type";

        return view('admin.head.tax_type_create', $data);
    }
}

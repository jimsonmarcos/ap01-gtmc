<?php



namespace App\Http\Controllers\Admin\Head;

ini_set('max_execution_time', 3000); //5 minutes

use App\Areas;
use App\Banks;
use App\CivilStatus;
use App\Companies;
use App\Compensations;
use App\CostCenters;
use App\DailyRates;
use App\EducationalAttainments;
use App\EmploymentDetails;
use App\Malls;
use App\OtherPayments;
use App\Outlets;
use App\PayrollGroups;
use App\Positions;
use App\Profiles;
use App\Provinces;
use App\TaxTypes;
use App\TransferModes;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use PHPExcel_Style_NumberFormat;
use Illuminate\Support\Facades\Validator;

class ImportOtherPaymentsController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Import Other Payments";

        return view('admin.head.import_other_payments', $data);
    }

    public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|file|mimes:xlsx,xls,csv',
        ]);

        if($validator->fails())
        {
            $request->session()->flash('popError', $validator->errors()->first());

            return redirect()
                ->back()
                ->withErrors($validator);
        }

//        $data['filepath'] = $request->import_file->store("public/imports/employees");
        $data['pageHeader'] = "Import Other Payments";
        $data['filename'] = $request->file('import_file')->getClientOriginalName();

        $filePath = $request->file('import_file')->getRealPath();

//        dd($filePath);

        $excel = App::make('excel');


        $excel->filter('chunk')->selectSheetsByIndex(0)->load($filePath)->chunk(250, function ($results) {
//            dd($results);

            $data['valid'] = [];
            $data['invalid'] = [];
            $dateFields = ['date'];
            foreach ($results as $item) {
                if(empty($item)) {
                    break;
                }

                foreach($dateFields as $field) {
                    if(!empty($item->$field) && substr_count('-', $item->$field) == 0) {
                        $item->$field = PHPExcel_Style_NumberFormat::toFormattedString($item->$field, 'YYYY-MM-DD');
                    }
                }

                $user = User::where('username', $item->username)->first();

                if(!empty($user)) {
                    $transactionType = $item->code;
                    if($transactionType == 'NOTARIAL FEE') {
                        $transactionType = 'NF';
                    }
                    if($transactionType == 'MEMBERSHIP FEE') {
                        $transactionType = 'MF';
                    }

//                    if($item->payment_method == 'Salary Deduction') {
//                        $arNumber = 'AR-000';
//                    } else {
//                        $arNumber = OtherPayments::orderBy('ar_number', 'desc')->first();
//                        if(!empty($arNumber)) {
//                            $arNumber = explode('-', $arNumber->ar_number);
//                            $arNumber = $arNumber[1] + 1;
//                            $arNumber = "AR-{$arNumber}";
//                        } else {
//                            $arNumber = "AR-101";
//                        }
//                    }

                    if(!empty($item->bank)) {
                        $bank = Banks::where('bank_account', $item->bank)->first();
                        if(!empty($bank)) {
                            $bankId = $bank->id;
                        }
                    }

                    $payment = [
                        'user_id' => $user->id,
                        'transaction_type' => $transactionType,
                        'payment_method' => $item->payment_method,
                        'pay_period' => $item->pay_period,
                        'payment_date' => $item->date,
                        'ar_number' => $item->ar_number,
                        'bank_id' => !empty($bankId) ? $bankId : '',
                        'reference_number' => !empty($item->reference_number) ? $item->reference_number : '',
                        'remarks' => !empty($item->remarks) ? $item->remarks : '',
                        'amount' => !empty($item->amount) ? str_replace(',', '', $item->amount) : 0,
                        'interest' => !empty($item->interest) ? str_replace(',', '', $item->interest) : 0,
                        'additional_payment' => !empty($item->additional_payment) ? str_replace(',', '', $item->additional_payment) : 0,
                        'total_amount_paid' => !empty($item->total_amount) ? str_replace(',', '', $item->total_amount) : 0,
                    ];

                    $payment = OtherPayments::create($payment);
                    $data['valid'][] = $payment;
                } else {
                    $data['invalid'][] = $item;
                }

            }

            return view('admin.head.import_other_payments_result', $data);

        });

        return view('admin.head.import_other_payments_result', $data);

        return redirect()->back();

    }
}

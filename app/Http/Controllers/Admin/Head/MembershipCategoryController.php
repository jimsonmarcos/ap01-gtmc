<?php

namespace App\Http\Controllers\Admin\Head;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MembershipCategoryController extends Controller
{
    //
    public function membership_category()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.membership_category', $data);
    }

    public function membership_category_create()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.membership_category_create', $data);
    }
}

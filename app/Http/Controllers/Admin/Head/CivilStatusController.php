<?php

namespace App\Http\Controllers\Admin\Head;

use App\CivilStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CivilStatusController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Civil Status";

        return view('admin.head.civil_status', $data);
    }

    public function create_civil_status()
    {
        $data['pageHeader'] = "New Civil Status";
        return view('admin.head.civil_status_create', $data);
    }

    public function create_civil_status_post(Request $request)
    {
        $civilStatus = $request['title'];

        if(CivilStatus::where('title', $civilStatus)->exists()) {
            $request->session()->flash('popError', 'Civil Status name already exists!');
            return redirect()->back();
        }

        CivilStatus::create(['title' => $civilStatus]);

        $request->session()->flash('popSuccess', "Civil Status <strong>{$civilStatus}</strong> successfully added");
        return redirect()->route('head_civil_status_create');

    }

    public function civil_status_edit(Request $request,$id)
    {
        if(CivilStatus::where('id', $id)->exists()) {
            CivilStatus::where('id', $id)->update(['title' => $request->title]);
            $request->session()->flash('popSuccess', 'Civil Status Successfully updated');
        } else {
            $request->session()->flash('popError', 'Civil Status does not exists');
        }


        return redirect()->back();
    }
    public function civil_status_delete(Request $request, $id)
    {
        if(CivilStatus::where('id', $id)->exists()) {
            CivilStatus::where('id', $id)->delete();
            $request->session()->flash('popSuccess', 'Civil Status Successfully deleted');
        } else {
            $request->session()->flash('popError', 'Civil Status does not exists');
        }

        return redirect()->back();
    }
}

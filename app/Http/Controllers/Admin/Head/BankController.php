<?php

namespace App\Http\Controllers\Admin\Head;

use App\Banks;
use App\TransferModes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankController extends Controller
{
    //
    public function bank()
    {
        $data['pageHeader'] = "Admin Settings";
        $matchThese = [
            'transfer_mode' => 'BANK DEPOSIT',
            'transfer_type' => 'CASH',
        ];
        $data['bank'] = TransferModes::where($matchThese)->get();
        return view('admin.head.bank', $data);
    }

    public function bank_create()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.bank_create', $data);
    }

    public function bank_create_post(Request $request)
    {
        $bank_name = $request['bank_name'];
        $bank_code = $request['bank_code'];
        $bank_full_name = $request['bank_full_name'];

        if(TransferModes::where('bank', $bank_name)->exists()) {
            $request->session()->flash('popError', 'Bank name already exists!');
            return redirect()->back();
        }

        TransferModes::create(
            [
                'bank' => strtoupper($bank_name),
                'bank_code' => strtoupper($bank_code),
                'bank_full_name' => strtoupper($bank_full_name),
                'transfer_mode' => 'BANK DEPOSIT',
                'transfer_type' => 'CASH',
            ]
        );

        $request->session()->flash('popSuccess', "Bank <strong>{$bank_name}</strong> successfully added");
        return redirect()->route('head_bank');

    }

    public function bank_edit(Request $request,$id)
    {
        if(TransferModes::where('id', $id)->exists()) {
            TransferModes::where('id', $id)->update(
                [
                    'bank' => strtoupper($request->bank_name),
                    'bank_code' => strtoupper($request->bank_code),
                    'bank_full_name' => strtoupper($request->bank_full_name),
                ]
            );
            $request->session()->flash('popSuccess', 'Bank Successfully updated');
        } else {
            $request->session()->flash('popError', 'Bank does not exists');
        }


        return redirect()->back();
    }

    public function bank_delete(Request $request, $id)
    {
        if(TransferModes::where('id', $id)->exists()) {
            TransferModes::where('id', $id)->delete();
            $request->session()->flash('popSuccess', 'Bank Successfully deleted');
        } else {
            $request->session()->flash('popError', 'Bank does not exists');
        }

        return redirect()->back();
    }
}

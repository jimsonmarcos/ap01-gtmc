<?php

namespace App\Http\Controllers\Admin\Head;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MembershipTypeController extends Controller
{
    //
    public function membership_type()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.membership_type', $data);
    }

    public function membership_type_create()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.membership_type_create', $data);
    }
}

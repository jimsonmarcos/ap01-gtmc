<?php

namespace App\Http\Controllers\Admin\Head;

use App\EducationalAttainments;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EducationalAttainmentController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Educational Attainment";

        return view('admin.head.educational_attainment', $data);
    }

    public function create_educational_attainment()
    {
        $data['pageHeader'] = "New Educational Attainment";

        return view('admin.head.educational_attainment_create', $data);
    }

    public function create_educational_attainment_post(Request $request)
    {
        $educationalAttainment = $request['title'];

        if(EducationalAttainments::where('title', $educationalAttainment)->exists()) {
            $request->session()->flash('popError', 'Educational Attainment name already exists!');
            return redirect()->back();
        }

        EducationalAttainments::create(['title' => $educationalAttainment]);

        $request->session()->flash('popSuccess', "Educational Attainment <strong>{$educationalAttainment}</strong> successfully added");
        return redirect()->route('head_educational_attainment');

    }

    public function educational_attainment_edit(Request $request,$id)
    {
        if(EducationalAttainments::where('id', $id)->exists()) {
            EducationalAttainments::where('id', $id)->update(['title' => $request->title]);
            $request->session()->flash('popSuccess', 'Educational Attainment Successfully updated');
        } else {
            $request->session()->flash('popError', 'Educational Attainment does not exists');
        }


        return redirect()->back();
    }
    public function educational_attainment_delete(Request $request, $id)
    {
        if(EducationalAttainments::where('id', $id)->exists()) {
            EducationalAttainments::where('id', $id)->delete();
            $request->session()->flash('popSuccess', 'Educational Attainment Successfully deleted');
        } else {
            $request->session()->flash('popError', 'Educational Attainment does not exists');
        }

        return redirect()->back();
    }
}

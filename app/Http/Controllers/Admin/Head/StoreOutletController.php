<?php

namespace App\Http\Controllers\Admin\Head;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreOutletController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Store Outlet";

        return view('admin.head.store_outlet', $data);
    }

    public function create_store_outlet()
    {
        $data['pageHeader'] = "New Store Outlet";

        return view('admin.head.store_outlet_create', $data);
    }

//    public function create_store_outlet_post(Request $request)
//    {
//        $positionName = $request['title'];
//
//        if(Positions::where('title', $positionName)->exists()) {
//            $request->session()->flash('popError', 'Position name already exists!');
//            return redirect()->back();
//        }
//
//        Positions::create(['title' => $positionName]);
//
//        $request->session()->flash('popSuccess', "Position <strong>{$positionName}</strong> successfully added");
//        return redirect()->route('head_position');
//
//    }
}

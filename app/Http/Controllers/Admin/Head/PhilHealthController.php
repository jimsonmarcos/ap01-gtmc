<?php

namespace App\Http\Controllers\Admin\Head;

use App\PhilHealthContributions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhilHealthController extends Controller
{
    //
    public function philhealth_schedule()
    {
        $data['pageHeader'] = "Admin Settings";
        $data['PhilHealthContributions'] = PhilHealthContributions::orderBy('salary_range_start')->get();

        return view('admin.head.table.philhealth_schedule', $data);
    }

    public function philhealth_schedule_update()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.table.update_philhealth_schedule', $data);
    }

    public function create_philhealth_schedule_post(Request $request)
    {
        PhilHealthContributions::create($request->except('_token'));

        $request->session()->flash('popSuccess', "PhilHealth Salary Bracket successfully added");
        return redirect()->route('head_philhealth_schedule');

    }

    public function philhealth_schedule_edit(Request $request,$id)
    {
        if(PhilHealthContributions:: where('id', $id)->exists()) {
            PhilHealthContributions:: where('id', $id)->update($request->except('_token'));
            $request->session()->flash('popSuccess', 'PhilHealth Salary Bracket Successfully updated');
        } else {
            $request->session()->flash('popError', 'PhilHealth Contribution does not exists');
        }


        return redirect()->back();
    }
    public function philhealth_schedule_delete(Request $request, $id)
    {
        if(PhilHealthContributions:: where('id', $id)->exists()) {
            PhilHealthContributions:: where('id', $id)->delete();
            $request->session()->flash('popSuccess', 'PhilHealth Salary Bracket Successfully deleted');
        } else {
            $request->session()->flash('popError', 'PhilHealth Salary Bracket does not exists');
        }

        return redirect()->back();
    }
}

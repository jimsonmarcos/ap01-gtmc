<?php

namespace App\Http\Controllers\Admin\Head;

use App\PremiumRates;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PremiumRateController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Premium Rate";

        return view('admin.head.table.premium_rate', $data);
    }

    public function create_premium_rate()
    {
        $data['pageHeader'] = "New Premium Rate";

        return view('admin.head.table.premium_rate_create', $data);
    }

    public function edit_premium_rate(Request $request, $id)
    {
        $data['pageHeader'] = "Edit Premium Rate";
        $data['premiumRate'] = PremiumRates::find($id);

        return view('admin.head.table.premium_rate_edit', $data);
    }

    public function edit_premium_rate_post(Request $request, $id)
    {
        $data = $request->except('_token');

        PremiumRates::where('id', $id)->update(['ot' => $data['ot'], 'premium' => $data['premium']]);

        $request->session()->flash('popSuccess', "Premium Rate successfully updated!");
        return redirect(url('/admin/super/premium-rate'));
    }
}

<?php

namespace App\Http\Controllers\Admin\Head;

use App\AuditLog;
use App\CivilStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AuditLogController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Audit Log";

        return view('admin.head.audit_log', $data);
    }

    public function datatables(Request $request)
    {
        $columns = array(
            0 =>'username',
            1 =>'category',
            2 => 'action',
            3 => 'description',
            4 => 'created_at'
        );


        $totalData = AuditLog::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $auditLog = AuditLog::select(DB::raw('u.username, audit_log.*'))
                ->join('users as u', 'u.id', '=', 'audit_log.user_id', 'LEFT')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $auditLog =  AuditLog::whereRaw( "(u.username LIKE '%{$search}%' OR audit_log.action LIKE '%{$search}%' OR audit_log.description LIKE '%{$search}%' OR audit_log.category LIKE '%{$search}%')")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->select(DB::raw('u.username, audit_log.*'))
                ->join('users as u', 'u.id', '=', 'audit_log.user_id', 'LEFT')
                ->get();

            $totalFiltered = AuditLog::whereRaw( "(u.username LIKE '%{$search}%' OR audit_log.action LIKE '%{$search}%' OR audit_log.description LIKE '%{$search}%' OR audit_log.category LIKE '%{$search}%')")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->select(DB::raw('u.username, audit_log.*'))
                ->whereRaw( "(u.username LIKE '%{$search}%' OR audit_log.action LIKE '%{$search}%' OR audit_log.description LIKE '%{$search}%' OR audit_log.category LIKE '%{$search}%')")
                ->join('users as u', 'u.id', '=', 'audit_log.user_id', 'LEFT')
                ->count();
        }

        $data = array();
        if(!empty($auditLog))
        {
            foreach ($auditLog as $user)
            {
//                $nestedData['username'] = $user->is_admin == 'Y' ? $user->admin_username : $user->username;
                $nestedData['username'] = $user->username;
                $nestedData['category'] = $user->category;
                $nestedData['action'] = $user->action;
                $nestedData['description'] = $user->description;
                $nestedData['created_at'] = date('m/d/Y g:i a', strtotime($user->created_at));
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
}

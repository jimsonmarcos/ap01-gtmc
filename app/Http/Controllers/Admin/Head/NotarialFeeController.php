<?php

namespace App\Http\Controllers\Admin\Head;

use App\Keyval;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotarialFeeController extends Controller
{
    public function index()
    {
        $data['pageHeader'] = "Membership Fee";

        return view('admin.head.notarial_fee', $data);
    }

    public function save(Request $request)
    {

        Keyval::where('key', 'notarial_fee')->update(['value' => $request->notarial_fee]);

        $request->session()->flash('popSuccess', 'Notarial Fee successfully updated.');
        return redirect()->back();
    }
}

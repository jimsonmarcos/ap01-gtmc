<?php

namespace App\Http\Controllers\Admin\Head;

use App\Areas;
use App\Departments;
use App\Malls;
use App\Outlets;
use App\Positions;
use App\Provinces;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AreaMallOutletController extends Controller
{
    //
    public function provinces(Request $request)
    {
        if(!empty($request->all())) {
            if(Provinces::where('province', $request->province)->count() == 0) {
                Provinces::create(['province' => $request->province]);

                $request->session()->flash('popSuccess', "City/Province successfully added");
                return redirect(url('/admin/super/provinces'));
            }
        }

        $data['pageHeader'] = "Departments";

        return view('admin.head.provinces', $data);
    }

    public function province_edit(Request $request, $id)
    {
        Provinces::where('id', $id)->update(['province' => $request->province]);

        $request->session()->flash('popSuccess', "City/Province successfully updated");
        return redirect(url('/admin/super/provinces'));
    }

    public function province_delete(Request $request, $id)
    {
        if(Provinces::where('id', $id)->count() == 1) {
            Provinces::where('id', $id)->delete();

            $request->session()->flash('popSuccess', "City/Province successfully deleted");
            return redirect(url('/admin/super/provinces'));
        }
    }

    public function provinces_post(Request $request)
    {
        $data = $request->except('_token');
        foreach($data as $d) {
            $d = strtoupper(trim($d));
        }

        if(Departments::where([
            'province' => $data['province'],
            'area' => $data['area'],
            'mall' => $data['mall'],
            'outlet' => $data['outlet'],
            'department' => $data['area']
        ])->exists()) {
            $request->session()->flash('popError', 'City/Province, Area, Mall, Outlet and Department already exists!');
            return redirect()->back();
        }

        Departments::create($data);

        $request->session()->flash('popSuccess', "City/Province, Area, Mall, Outlet and Department successfully added");
        return redirect()->route('head_area_mall_outlet');

    }

    public function areas(Request $request)
    {
        if(!empty($request->all())) {
            if(Areas::where('area', $request->area)->count() == 0) {
                Areas::create(['area' => $request->area]);

                $request->session()->flash('popSuccess', "Area successfully added");
                return redirect(url('/admin/super/areas'));
            }
        }

        $data['pageHeader'] = "Departments";

        return view('admin.head.areas', $data);
    }

    public function area_edit(Request $request, $id)
    {
        Areas::where('id', $id)->update(['area' => $request->province]);

        $request->session()->flash('popSuccess', "Area successfully updated");
        return redirect(url('/admin/super/areas'));
    }

    public function area_delete(Request $request, $id)
    {
        if(Areas::where('id', $id)->count() == 1) {
            Areas::where('id', $id)->delete();

            $request->session()->flash('popSuccess', "Area successfully deleted");
            return redirect(url('/admin/super/areas'));
        }
    }

    public function malls(Request $request)
    {
        if(!empty($request->all())) {
            if(Malls::where('mall', $request->mall)->count() == 0) {
                Malls::create(['mall' => $request->mall]);

                $request->session()->flash('popSuccess', "Mall successfully added");
                return redirect(url('/admin/super/malls'));
            }
        }

        $data['pageHeader'] = "Departments";

        return view('admin.head.malls', $data);
    }

    public function mall_edit(Request $request, $id)
    {
        Malls::where('id', $id)->update(['mall' => $request->mall]);

        $request->session()->flash('popSuccess', "Mall successfully updated");
        return redirect(url('/admin/super/malls'));
    }

    public function mall_delete(Request $request, $id)
    {
        if(Malls::where('id', $id)->count() == 1) {
            Malls::where('id', $id)->delete();

            $request->session()->flash('popSuccess', "Mall successfully deleted");
            return redirect(url('/admin/super/malls'));
        }
    }

    public function outlets(Request $request)
    {
        if(!empty($request->all())) {
            if(Outlets::where('outlet', $request->outlet)->count() == 0) {
                Outlets::create(['outlet' => $request->outlet]);

                $request->session()->flash('popSuccess', "Outlet successfully added");
                return redirect(url('/admin/super/outlets'));
            }
        }

        $data['pageHeader'] = "Departments";

        return view('admin.head.outlets', $data);
    }

    public function outlet_edit(Request $request, $id)
    {
        Outlets::where('id', $id)->update(['outlet' => $request->outlet]);

        $request->session()->flash('popSuccess', "Outlet successfully updated");
        return redirect(url('/admin/super/outlets'));
    }

    public function outlet_delete(Request $request, $id)
    {
        if(Outlets::where('id', $id)->count() == 1) {
            Outlets::where('id', $id)->delete();

            $request->session()->flash('popSuccess', "Outlet successfully deleted");
            return redirect(url('/admin/super/outlets'));
        }
    }

    public function area_mall_outlet_edit(Request $request,$id)
    {
        if(Departments::where('id', $id)->exists()) {
            Departments::where('id', $id)->update($request->except('_token'));

            $request->session()->flash('popSuccess', 'City/Province, Area, Mall, Outlet and Department Successfully updated');
        } else {
            $request->session()->flash('popError', 'City/Province, Area, Mall, Outlet and Department does not exists');
        }


        return redirect()->back();
    }
    public function area_mall_outlet_delete(Request $request, $id)
    {
        if(Departments::where('id', $id)->exists()) {
            Departments::where('id', $id)->delete();
            $request->session()->flash('popSuccess', 'City/Province, Area, Mall, Outlet and Department Successfully deleted');
        } else {
            $request->session()->flash('popError', 'City/Province, Area, Mall, Outlet and Department does not exists');
        }

        return redirect()->back();
    }
}

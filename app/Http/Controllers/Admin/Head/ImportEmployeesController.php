<?php



namespace App\Http\Controllers\Admin\Head;

ini_set('max_execution_time', 3000); //5 minutes

use App\Areas;
use App\CivilStatus;
use App\Companies;
use App\Compensations;
use App\CostCenters;
use App\DailyRates;
use App\EducationalAttainments;
use App\EmploymentDetails;
use App\Malls;
use App\Outlets;
use App\PayrollGroups;
use App\Positions;
use App\Profiles;
use App\Provinces;
use App\TaxTypes;
use App\TransferModes;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use PHPExcel_Style_NumberFormat;
use Illuminate\Support\Facades\Validator;

class ImportEmployeesController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Import Employees";

        return view('admin.head.import_employees', $data);
    }

    public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|file|mimes:xlsx,xls,csv',
        ]);

        if($validator->fails())
        {
            $request->session()->flash('popError', $validator->errors()->first());

            return redirect()
                ->back()
                ->withErrors($validator);
        }


//        $data['filepath'] = $request->import_file->store("public/imports/employees");
        $data['pageHeader'] = "Import Employees";
        $data['filename'] = $request->file('import_file')->getClientOriginalName();

        $filePath = $request->file('import_file')->getRealPath();

//        dd($filePath);

        $excel = App::make('excel');


        $items = [];
        $invalid = [];
        $x = 1;
        $excel->filter('chunk')->selectSheetsByIndex(0)->load($filePath)->chunk(250, function ($results) use ($items, $invalid, $x) {

            $data['validUsers'] = [];
            $data['invalidUsers'] = [];

            foreach ($results as $item) {
                if(empty($item)) {
                    session()->flash('popError', 'The uploaded file was empty.');
                    return redirect()->back();
                    break;
                }

                if(empty($item->gtmc_id_number)) {
                    session()->flash('popError', "Empty GTMC Number in Row #{$x}");
                    return redirect()->back();
                    break;
                }

                if(empty($item->status)) {
                    session()->flash('popError', "Empty Status in Row #{$x}");
                    return redirect()->back();
                    break;
                }

                if(empty($item->username)) {
                    session()->flash('popError', "Empty Status in Row #{$x}");
                    return redirect()->back();
                    break;
                }

                if(empty($item->principal)) {
                    session()->flash('popError', "Empty Principal in Row #{$x}");
                    return redirect()->back();
                    break;
                }

//                foreach(['gtmc_id_number', 'username', 'status', 'principal'] as $check) {
//                    if(empty($item->{$check})) {
//                        $invalid->merge($item);
//                    }
//                }



//                }

                $items[] = $item;
                $x++;
            }

//            dd($invalid);
//
//            foreach($items as $item) {
//                if(empty($item->gtmc_id_number)) {
//                    continue;
//                }
//
//                $dateFields = ['birthday', 'hire_date', 'trainee_start_date', 'trainee_end_date'];
//
//                foreach($dateFields as $field) {
//                    if(!empty($item->$field) && substr_count('-', $item->$field) == 0) {
//                        $item->$field = PHPExcel_Style_NumberFormat::toFormattedString($item->birthday,PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
//                    }
//                }
//
//
//                dd($items);
//
//
//            }


//            $x++;

            foreach($items as $item) {
                $user = [
                    'series' => str_replace(['M', 'E'], '', trim($item->username)),
                    'email' => strtolower($item->email),
                    'username' => strtoupper($item->username),
                    'role' => (strtoupper($item->membership_category) == 'EMPLOYEE') ? "Employee" : "Member",
                ];


                $name = $item->first_name;
                $name = !empty($item->middle_name) ? $name . " {$item->middle_name}" : $name;
                $user['name'] = !empty($item->last_name) ? $name . " {$item->last_name}" : $name;

                $user['password'] = bcrypt($user['username']);


                if(!empty($user['username'])) {
                    if(User::where('username', $user['username'])->count() == 0) {
                        $user = User::create($user);


                        $civilStatus = '';
                        if (!empty($item->civil_status)) {
                            if (CivilStatus::where('title', strtoupper($item->civil_status))->count() == 0) {
                                $civilStatus = CivilStatus::create(['title' => strtoupper($item->civil_status)]);
                                $civilStatus = $civilStatus->id;
                            } else {
                                $civilStatus = CivilStatus::where('title', strtoupper($item->civil_status))->first()->id;
                            }
                        }


                        $educationalAttainment = '';
                        if (!empty(!empty($item->educational_attainment))) {
                            if (EducationalAttainments::where('title', strtoupper($item->educational_attainment))->count() == 0) {
                                $educationalAttainment = EducationalAttainments::create(['title' => strtoupper($item->educational_attainment)]);
                                $educationalAttainment = $educationalAttainment->id;
                            } else {
                                $educationalAttainment = EducationalAttainments::where('title', strtoupper($item->educational_attainment))->first()->id;
                            }
                        }


                        $profile = [
                            'id_number' => $item->gtmc_id_number,
                            'first_name' => strtoupper($item->first_name),
                            'middle_name' => strtoupper($item->middle_name),
                            'last_name' => strtoupper($item->last_name),
                            'address' => strtoupper($item->address),
                            'email' => strtolower($item->email),
                            'mobile1' => $item->mobile_1,
                            'mobile2' => $item->mobile_2,
                            'gender' => $item->gender,
                            'mothers_maiden_name' => strtoupper($item->mothers_maiden_name),
                            'birthday' => !empty($item->birthday) ? PHPExcel_Style_NumberFormat::toFormattedString($item->birthday,PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2) : '',
                            'join_date' => !empty($item->hire_date) ? PHPExcel_Style_NumberFormat::toFormattedString($item->hire_date,PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2) : '',
                            'civil_status_id' => $civilStatus,
                            'educational_attainment_id' => $educationalAttainment,
                            'icoe_contact_name' => strtoupper($item->icoe_contact_name),
                            'icoe_contact_address' => strtoupper($item->icoe_contact_address),
                            'icoe_contact_mobile' => (string) $item->icoe_contact_number,
                            'membership_category' => ucfirst(strtolower($item->membership_category)),
                            'membership_type' => ucfirst(strtolower($item->membership_type)),
                            'status' => ucfirst(strtolower($item->status)),
                            'coop_share' => $item->contribution_per_payday,
                            'membership_status' => 'Active',
                            'cost_center_id' => !empty($item->cost_center) && CostCenters::where('cost_center', $item->cost_center)->exists() ? CostCenters::where('cost_center', $item->cost_center)->first()->id : ''
                        ];

                        $profile['user_id'] = $user->id;

//                dd($profile);
                        Profiles::create($profile);

                        $principal = Companies::where('company_code', $item->principal)->first();
                        if (!empty($principal)) {
                            $employmentDetails['principal_id'] = $principal->id;
                        }


                        $employmentDetails['coordinator'] = $item->coordinator;
//                        if ($profile['membership_category'] == 'Employee') {
                        $position = Positions::where('title', $item->position)->first();
                        if (!empty($position)) {
                            $employmentDetails['position_id'] = $position->id;
                        }

                        $employmentDetails['coordinator'] = $item->coordinator;
                        $employmentDetails['orientation'] = ($item->orientation_yn == 'Y' ? 'Y' : 'N');
                        $employmentDetails['trainee_start_date'] = (!empty($item->trainee_start_date) ? PHPExcel_Style_NumberFormat::toFormattedString($item->trainee_start_date,PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2) : '');
                        $employmentDetails['trainee_end_date'] = (!empty($item->trainee_end_date) ? PHPExcel_Style_NumberFormat::toFormattedString($item->trainee_end_date,PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2) : '');
                        $employmentDetails['hire_date'] = (!empty($item->hire_date) ? PHPExcel_Style_NumberFormat::toFormattedString($item->hire_date,PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2) : '');

                        if(!empty($item->province)) {
                            $province = Provinces::where('province', $item->province)->first();
                            if (!empty($province)) {
                                $employmentDetails['province_id'] = $province->id;

                                if(!empty($item->area)) {
                                    $area = Areas::where(['area' => $item->area, 'province_id' => $province->id])->first();
                                    if (!empty($area)) {
                                        $employmentDetails['area_id'] = $area->id;
                                    }
                                }
                            }
                        }

                        if(!empty($item->mall)) {
                            $mall = Malls::where('mall', $item->mall)->first();
                            if (!empty($mall)) {
                                $employmentDetails['mall_id'] = $mall->id;
                            }
                        }

                        if(!empty($item->outlet)) {
                            $outlet = Outlets::where('outlet', $item->outlet)->first();
                            if (!empty($outlet)) {
                                $employmentDetails['outlet_id'] = $outlet->id;
                            }
                        }

                        if (!empty($item->coordinator)) {
                            $profile = Profiles::where('id_number', $item->coordinator)->first();
                            if (!empty($coordinator)) {
                                $employmentDetails['coordinator_id'] = $profile->user_id;
                            }
                        }

                        $compensation = [
                            'daily_rate' => $item->daily_rate,
                            'hourly' => $item->hourly,
                            'monthly_rate' => $item->monthly_rate,
                            'account_number' => trim($item->account_number),
                            'rate_type' => trim(ucfirst(strtolower($item->rate_type))),
                            'daily_category' => trim(ucfirst(strtolower($item->daily_category))),
                            'mobile_number' => trim($item->gcash_mobile_number),
                            'sss' => (string) $item->sss_10_numbers,
                            'compute_sss' => $item->compute_sss_yn == 'Y' ? 'Y' : 'N',
                            'hdmf' => (string) $item->hdmf_12_numbers,
                            'compute_hdmf' => $item->compute_hdmf_yn == 'Y' ? 'Y' : 'N',
                            'philhealth' => (string) $item->philhealth_12_numbers,
                            'compute_philhealth' => $item->compute_phic_yn == 'Y' ? 'Y' : 'N',
                            'tin' => (string) $item->tin_12_numbers,
                            'deduct_withholding' => $item->deduct_withholding_yn == 'Y' ? 'Y' : 'N',
                            'ecola' => $item->ecola
                        ];

//                            dd($compensation);

                        if(empty($compensation['daily_rate']) && !empty($compensation['monthly_rate'])) {
                            $compensation['daily_rate'] = $compensation['monthly_rate'] / 26;
                        }

                        if(empty($compensation['monthly_rate']) && !empty($compensation['daily_rate'])) {
                            $compensation['monthly_rate'] = $compensation['daily_rate'] * 26;
                        }

                        if(empty($compensation['hourly'])) {
                            $compensation['hourly'] = $compensation['daily_rate'] / 8;
                        }

                        if ($compensation['daily_category'] == 'Location' || $compensation['daily_category'] == 'Region') {
                            $dailyRate = DailyRates::where(['category' => strtoupper($item->daily_category), 'location' => strtoupper($item->location)])->first();
                            if (!empty($dailyRate)) {
                                $compensation['daily_rate_id'] = $dailyRate->id;
                                $compensation['daily_rate'] = $dailyRate->rate;
                                $compensation['monthly_rate'] = $dailyRate->rate * 26;
                                $compensation['hourly'] = $dailyRate->rate / 8;
                            }
                        }

                        $transferMode = TransferModes::where(['transfer_mode' => $item->transfer_mode, 'transfer_type' => $item->transfer_type, 'bank' => $item->bank])->first();
                        if (!empty($transferMode)) {
                            $compensation['transfer_mode_id'] = $transferMode->id;
                        }

                        $taxType = TaxTypes::where('title', $item->tax_type)->first();
                        if (!empty($taxType)) {
                            $compensation['tax_type_id'] = $taxType->id;
                        }

                        if(!empty($item->payroll_group)) {
                            $payrollGroup = PayrollGroups::where('group_name', $item->payroll_group)->first();
                            if (!empty($payrollGroup)) {
                                if (!empty($payrollGroup->Company->parent_company)) {
//                            $parentCompany = Companies::find($payrollGroup->Company->parent_company);
//                            $payrollGroup = PayrollGroups::where(['group_name' => $item->payroll_group, 'company_id' => $parentCompany->id])->first();
                                    if (!empty($payrollGroup)) {
                                        $compensation['payroll_group_id'] = $payrollGroup->id;
                                        $compensation['client_id'] = $payrollGroup->company_id;
                                    }
                                } else {
                                    $compensation['payroll_group_id'] = $payrollGroup->id;
                                    $compensation['client_id'] = $payrollGroup->company_id;
                                }
                            }
                        }


                        $compensation['user_id'] = $user->id;
//                    dd($employmentDetails);
                        Compensations::create($compensation);
                    }

                    $employmentDetails['user_id'] = $user->id;
                    EmploymentDetails::create($employmentDetails);

//                        $data['validUsers'][] = $user;
                } else {
//                        $data['invalidUsers'][] = $item;
                }
            }


            session()->flash('popSuccess', 'Employee Import successful!');
            return view('admin.head.import_employees_result', $data);

        });



        return redirect()->back();

    }
}

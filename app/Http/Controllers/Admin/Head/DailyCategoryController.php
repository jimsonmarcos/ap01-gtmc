<?php

namespace App\Http\Controllers\Admin\Head;

use App\DailyRates;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DailyCategoryController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Daily Category";

        return view('admin.head.daily_category', $data);
    }

    public function create_daily_category()
    {
        $data['pageHeader'] = "New Daily Category";

        return view('admin.head.daily_category_create', $data);
    }

    public function create_daily_category_post(Request $request)
    {
        $data = $request->except('_token');
        foreach($data as $d) {
            $d = strtoupper(trim($d));
        }

        if(DailyRates::where([
            'category' => $data['category'],
            'location' => $data['location'],
            'rate' => $data['rate'],
            'ecola' => $data['ecola']
        ])->exists()) {
            $request->session()->flash('popError', 'Daily Rate already exists!');
            return redirect()->back();
        }

        DailyRates::create($data);

        $request->session()->flash('popSuccess', "Daily Rate successfully added");
        return redirect()->route('head_daily_category');

    }

    public function daily_category_edit(Request $request,$id)
    {
        if(DailyRates::where('id', $id)->exists()) {
            DailyRates::where('id', $id)->update($request->except('_token'));

            $request->session()->flash('popSuccess', 'Daily Rate Successfully updated');
        } else {
            $request->session()->flash('popError', 'Daily Rate does not exists');
        }


        return redirect()->back();
    }
    public function daily_category_delete(Request $request, $id)
    {
        if(DailyRates::where('id', $id)->exists()) {
            DailyRates::where('id', $id)->delete();
            $request->session()->flash('popSuccess', 'Daily Rate Successfully deleted');
        } else {
            $request->session()->flash('popError', 'Daily Rate does not exists');
        }

        return redirect()->back();
    }
}

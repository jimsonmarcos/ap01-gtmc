<?php

namespace App\Http\Controllers\Admin\Head;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AreaController extends Controller
{
    //
    public function area()
    {
        $data['pageHeader'] = "Area";

        return view('admin.head.area', $data);
    }

    public function area_create()
    {
        $data['pageHeader'] = "Area";

        return view('admin.head.area_create', $data);
    }
}

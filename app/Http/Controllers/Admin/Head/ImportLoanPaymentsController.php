<?php



namespace App\Http\Controllers\Admin\Head;

ini_set('max_execution_time', 3000); //5 minutes

use App\Banks;
use App\LoanPayments;
use App\LoanPaymentSchedules;
use App\Loans;
use App\LoanTypes;
use App\TransferModes;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use PHPExcel_Style_NumberFormat;
use Illuminate\Support\Facades\Validator;

class ImportLoanPaymentsController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Import Loan Payments";

        return view('admin.head.import_loan_payments', $data);
    }


    public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|file|mimes:xlsx,xls,csv',
        ]);

        if($validator->fails())
        {
            $request->session()->flash('popError', $validator->errors()->first());

            return redirect()
                ->back()
                ->withErrors($validator);
        }

//        $data['filepath'] = $request->import_file->store("public/imports/employees");
        $data['pageHeader'] = "Import Loan Payments";
        $data['filename'] = $request->file('import_file')->getClientOriginalName();

        $filePath = $request->file('import_file')->getRealPath();

//        dd($filePath);

        $excel = App::make('excel');

        $x = 1;

        $excel->filter('chunk')->selectSheetsByIndex(0)->ignoreEmpty()->load($filePath)->chunk(250, function ($results) use ($x) {
//            dd($results);
//            dd($x);


            $data['valid'] = [];
            $data['invalid'] = [];
            $dateFields = ['date'];
            foreach ($results as $item) {

                if(empty($item)) {
                    break;
                }

                foreach($dateFields as $field) {
                    if(!empty($item->$field) && substr_count('-', $item->$field) == 0) {
                        $item->$field = PHPExcel_Style_NumberFormat::toFormattedString($item->$field, 'YYYY-MM-DD');
                    }
                }

                $item->cla_number = PHPExcel_Style_NumberFormat::toFormattedString($item->cla_number, '0');

                $loan = Loans::where('cla_number', "CLA-{$item->cla_number}")->first();



                if(!empty($loan)) {

                    $paymentDate = $item->date;
                    $transactionId = "P-". str_replace('-', '', $paymentDate);

                    $latestPayment = LoanPayments::where('payment_date', $paymentDate)->orderByDesc('payment_date')->get();

                    $series = !empty($latestPayment) ? $latestPayment->max('series') + 1 : "0001";

                    if(strlen($series) < 4) {
                        while(strlen($series) < 4) {
                            $series = "0{$series}";
                        }
                    }

                    $transactionId = "{$transactionId}-{$series}";

                    $bank = Banks::where('bank_account', trim($item->bank))->first();
                    $bankId = '';
                    if(!empty($bank)) {
                        $bankId = $bank->id;
                    }


                    $payment = [
                        'loan_id' => $loan->id,
                        'series' => (int) $series,
                        'transaction_id' => $transactionId,
                        'loan_installment_code' => "PR-{$item->pay_period}",
                        'ar_number' => $item->ar_number,
                        'payment_method' => $item->payment_method,
                        'payment_date' => $item->date,
                        'amount' => str_replace(',', '', $item->amount),
                        'interest' => str_replace(',', '', $item->interest),
                        'additional_payment' => str_replace(',', '', $item->additional_payment),
                        'total_amount_paid' => str_replace(',', '', $item->total_amount),
                        'bank_id' => $bankId,
                        'reference_number' => $item->reference_number,
                        'pay_period' => $item->pay_period,
                        'remarks' => $item->remarks,
                    ];

                    $payment = LoanPayments::create($payment);




                    $data['valid'][] = $payment;
                } else {
                    $data['invalid'][] = $item;
                }

                $x++;
            }


            return view('admin.head.import_loan_payments_result', $data);

        });

        return view('admin.head.import_loan_payments_result', $data);

        return redirect()->back();

    }
}

<?php



namespace App\Http\Controllers\Admin\Head;

ini_set('max_execution_time', 3000); //5 minutes

use App\Areas;
use App\CivilStatus;
use App\Companies;
use App\Compensations;
use App\CostCenters;
use App\DailyRates;
use App\EducationalAttainments;
use App\EmploymentDetails;
use App\Malls;
use App\Outlets;
use App\PayrollGroups;
use App\Positions;
use App\Profiles;
use App\Provinces;
use App\TaxTypes;
use App\TransferModes;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use PHPExcel_Style_NumberFormat;
use Illuminate\Support\Facades\Validator;

class ImportMembersController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Import Members";

        return view('admin.head.import_members', $data);
    }

    public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|file|mimes:xlsx,xls,csv',
        ]);

        if($validator->fails())
        {
            $request->session()->flash('popError', $validator->errors()->first());

            return redirect()
                ->back()
                ->withErrors($validator);
        }

//        $data['filepath'] = $request->import_file->store("public/imports/employees");
        $data['pageHeader'] = "Import Members";
        $data['filename'] = $request->file('import_file')->getClientOriginalName();

        $filePath = $request->file('import_file')->getRealPath();

//        dd($filePath);

        $excel = App::make('excel');


        $excel->filter('chunk')->selectSheetsByIndex(0)->load($filePath)->chunk(250, function ($results) {
//            dd($results);

            $data['validUsers'] = [];
            $data['invalidUsers'] = [];

            foreach ($results as $item) {
                if(empty($item)) {
                    break;
                }

                $dateFields = ['birthday', 'hire_date', 'trainee_start_date', 'trainee_end_date'];

                foreach($dateFields as $field) {
                    if(!empty($item->$field) && substr_count('-', $item->$field) == 0) {
                        $item->$field = PHPExcel_Style_NumberFormat::toFormattedString($item->$field, 'YYYY-MM-DD');
                    }
                }


//                dd($item);

                $user = [
                    'series' => str_replace(['M', 'E'], '', trim($item->username)),
                    'email' => strtolower($item->email),
                    'username' => strtoupper($item->username),
                    'role' => (strtoupper($item->membership_category) == 'EMPLOYEE') ? "Employee" : "Member",
                ];


                $name = $item->first_name;
                $name = !empty($item->middle_name) ? $name . " {$item->middle_name}" : $name;
                $user['name'] = !empty($item->last_name) ? $name . " {$item->last_name}" : $name;

                $user['password'] = bcrypt($user['username']);


                if(!empty($user['username'])) {
                    if(User::where('username', $user['username'])->count() == 0) {
                        $user = User::create($user);


                        $civilStatus = '';
                        if (!empty($item->civil_status)) {
                            if (CivilStatus::where('title', strtoupper($item->civil_status))->count() == 0) {
                                $civilStatus = CivilStatus::create(['title' => strtoupper($item->civil_status)]);
                                $civilStatus = $civilStatus->id;
                            } else {
                                $civilStatus = CivilStatus::where('title', strtoupper($item->civil_status))->first()->id;
                            }
                        }


                        $educationalAttainment = '';
                        if (!empty(!empty($item->educational_attainment))) {
                            if (EducationalAttainments::where('title', strtoupper($item->educational_attainment))->count() == 0) {
                                $educationalAttainment = EducationalAttainments::create(['title' => strtoupper($item->educational_attainment)]);
                                $educationalAttainment = $educationalAttainment->id;
                            } else {
                                $educationalAttainment = EducationalAttainments::where('title', strtoupper($item->educational_attainment))->first()->id;
                            }
                        }


                        $profile = [
                            'id_number' => $item->gtmc_id_number,
                            'first_name' => strtoupper($item->first_name),
                            'middle_name' => strtoupper($item->middle_name),
                            'last_name' => strtoupper($item->last_name),
                            'address' => strtoupper($item->address),
                            'email' => strtolower($item->email),
                            'mobile1' => $item->mobile_1,
                            'mobile2' => $item->mobile_2,
                            'gender' => $item->gender,
                            'mothers_maiden_name' => strtoupper($item->mothers_maiden_name),
                            'birthday' => !empty($item->birthday) ? $item->birthday : '',
                            'join_date' => !empty($item->hire_date) ? $item->hire_date : '',
                            'civil_status_id' => $civilStatus,
                            'educational_attainment_id' => $educationalAttainment,
                            'icoe_contact_name' => strtoupper($item->icoe_contact_name),
                            'icoe_contact_address' => strtoupper($item->icoe_contact_address),
                            'icoe_contact_mobile' => strtoupper($item->icoe_contact_number),
                            'membership_category' => ucfirst(strtolower($item->membership_category)),
                            'membership_type' => ucfirst(strtolower($item->membership_type)),
                            'status' => ucfirst(strtolower($item->status)),
                            'coop_share' => $item->contribution_per_payday,
                            'membership_status' => 'Active',
                            'cost_center_id' => CostCenters::where('cost_center', $item->cost_center)->exists() ? CostCenters::where('cost_center', $item->cost_center)->first()->id : ''
                        ];

                        $profile['user_id'] = $user->id;

                        Profiles::create($profile);


                        $data['validUsers'][] = $user;
                    } else {
                        $data['invalidUsers'][] = $item;
                    }
                }
            }

            return view('admin.head.import_members_result', $data);

        });

        return redirect()->back();

    }
}

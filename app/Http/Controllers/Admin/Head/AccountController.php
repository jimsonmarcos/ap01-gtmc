<?php

namespace App\Http\Controllers\Admin\Head;

use App\Keyval;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class AccountController extends Controller
{
    //
    public function account()
    {
        $data['pageHeader'] = "Account Settings";

        return view('admin.head.account', $data);
    }

    public function account_post(Request $request)
    {
        $data = $request->except('_token');

        if(count($request->allFiles()) > 0) {
            foreach($request->allFiles() as $key => $file) {
                $files[] = $key;
                $file =  $request->file($key)->store('public/account_settings');
                $file_path = ltrim(Storage::url($file), '/');
//                dd($file_path);

                Keyval::where('key', $key)->update(['value' => $file_path]);
            }
        }


        foreach($data as $k => $v) {
            if(!empty($files) && in_array($k, $files)) {
                // Do nothing
            } else {
                Keyval::where('key', $k)->update(['value' => $v]);
            }
        }

        $request->session()->flash('popSuccess', 'Account successfully updated.');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin\Head;

use App\BusinessTaxTypes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusinessTaxTypeController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Business Tax Type";

        return view('admin.head.business_tax_type', $data);
    }

    public function create_tax_type()
    {
        $data['pageHeader'] = "New Business Tax Type";

        return view('admin.head.business_tax_type_create', $data);
    }

    public function tax_type_create_post(Request $request)
    {
        $data = $request->except('_token');

        BusinessTaxTypes::create($data);

        $request->session()->flash('popSuccess', "Business Tax Type successfully added");
        return redirect(url('/admin/super/business-tax-type'));

    }

    public function edit(Request $request, $id)
    {
        $data = $request->except('_token');

        BusinessTaxTypes::where('id', $id)->update($data);

        $request->session()->flash('popSuccess', "Business Tax Type successfully updated");
        return redirect(url('/admin/super/business-tax-type'));
    }

    public function delete(Request $request, $id)
    {
        if(BusinessTaxTypes::where('id', $id)->count() == 1) {
            BusinessTaxTypes::where('id', $id)->delete();

            $request->session()->flash('popSuccess', "Business Tax Type successfully deleted");
            return redirect(url('/admin/super/business-tax-type'));
        }
    }
}

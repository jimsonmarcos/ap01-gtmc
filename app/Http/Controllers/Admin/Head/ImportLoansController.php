<?php



namespace App\Http\Controllers\Admin\Head;

ini_set('max_execution_time', 3000); //5 minutes

use App\LoanPaymentSchedules;
use App\Loans;
use App\LoanTypes;
use App\TransferModes;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use PHPExcel_Style_NumberFormat;
use Illuminate\Support\Facades\Validator;

class ImportLoansController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Import Loans";

        return view('admin.head.import_loans', $data);
    }


    public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|file|mimes:xlsx,xls,csv',
        ]);

        if($validator->fails())
        {
            $request->session()->flash('popError', $validator->errors()->first());

            return redirect()
                ->back()
                ->withErrors($validator);
        }

//        $data['filepath'] = $request->import_file->store("public/imports/employees");
        $data['pageHeader'] = "Import Loans";
        $data['filename'] = $request->file('import_file')->getClientOriginalName();

        $filePath = $request->file('import_file')->getRealPath();

//        dd($filePath);

        $excel = App::make('excel');

        $x = 1;

        $excel->filter('chunk')->selectSheetsByIndex(0)->ignoreEmpty()->load($filePath)->chunk(250, function ($results) use ($x) {
//            dd($results);
//            dd($x);


            $data['valid'] = [];
            $data['invalid'] = [];
            $dateFields = ['cv_date', 'date_request', 'effective_date', 'released_date'];
            foreach ($results as $item) {

                if(empty($item)) {
                    break;
                }

                foreach($dateFields as $field) {
                    if(!empty($item->$field) && substr_count('-', $item->$field) == 0) {
                        $item->$field = PHPExcel_Style_NumberFormat::toFormattedString($item->$field, 'YYYY-MM-DD');
                    }
                }

                $item->cla_number = PHPExcel_Style_NumberFormat::toFormattedString($item->cla_number, '0');
                $item->months_to_pay = PHPExcel_Style_NumberFormat::toFormattedString($item->months_to_pay, '0');
                $user = User::where('username', $item->username)->first();



//                dd($item);

                if(!empty($user)) {

                    if(!empty($item->is_voucher) && $item->is_voucher == 'Y') {

                    } else {
                        $loanType = LoanTypes::where('type', $item->loan_type)->first();
                        if(!empty($loanType)) {
                            $loanTypeId = $loanType->id;
                        } else {
                            $data['invalid'][] = $item;
                            continue;
                        }

                        $transferMode = TransferModes::where(['transfer_mode' => 'BANK DEPOSIT', 'bank' => strtoupper($item->bank)])->first();
                        if(!empty($transferMode)) {
                            $transferModeId = $transferMode->id;
                        } else {
                            $transferModeId = null;
                        }
                    }



                    $loan = [
                        'cla_number' => "CLA-{$item->cla_number}",
                        'cla_series' => $item->cla_number,
                        'cv_number' => $item->cv_number,
                        'cv_date' => $item->cv_date,
                        'user_id' => $user->id,
                        'date_request' => $item->date_request,
                        'loan_type_id' => !empty($loanTypeId) ? $loanTypeId : '',
                        'loan_type' => (!empty($item->is_voucher) && $item->is_voucher == 'Y') ? $item->loan_type : '',
                        'months_to_pay' => $item->months_to_pay,
                        'request_amount' => str_replace(',', '', $item->request_amount),
                        'interest' => str_replace(',', '', $item->interest),
                        'interest_percent' => !empty($loanType->interest) ? $loanType->interest : '',
                        'overall_interest' => str_replace(',', '', $item->overall_interest),
                        'granted_loan_amount' => str_replace(',', '', $item->granted_loan_amount),
                        'amortization' => str_replace(',', '', $item->amortization),
                        'service_charge' => str_replace(',', '', $item->service_charge),
                        'service_charge_percent' => !empty($loanType->service_charge) ? str_replace(',', '', $loanType->service_charge) : '',
                        'savings' => str_replace(',', '', $item->savings),
                        'savings_percent' => !empty($loanType->savings) ? str_replace(',', '', $loanType->savings) : '',
                        'additional_deduction' => !empty($item->additional_deduction) ? str_replace(',', '', $item->additional_deduction) : '',
                        'disbursed_amount' => str_replace(',', '', $item->disbursed_amount),
                        'beginning_balance' => str_replace(',', '', $item->beginning_balance),
                        'status' => in_array(strtoupper($item->status), ['APPROVED', 'ONGOING']) ? 'Approved' : $item->status,
                        'releasing_status' => $item->releasing_status,
                        'effective_date' => $item->effective_date,
                        'payment_method' => $item->payment_method,
                        'remarks' => $item->remarks,
                        'released_date' => $item->released_date,
                        'transfer_mode_id' => !empty($transferModeId) ? $transferModeId : '',
                        'reference_number' => $item->reference_number,
                        'disbursed_remarks' => $item->disbursed_remarks
                    ];

                    if(!empty($item->is_voucher) && $item->is_voucher == 'Y') {
                        $loan['status'] = 'Approved';
                        $loan['releasing_status'] = 'Released';
                        $loan['is_voucher'] = 'Y';
                    }

//                    dd($loan);


                    $loan = Loans::create($loan);

                    if(!empty($item->is_voucher) && $item->is_voucher == 'Y') {
                        if($loan->User->role == 'Employee') {
                            $payrollGroup = $loan->User->Profile->Compensation->PayrollGroup;
                            $effectiveDate = Carbon::createFromFormat('Y-m-d', !empty($loan->effective_date) ? $loan->effective_date : $loan->date_request);
                        } else {
                            $effectiveDate = Carbon::createFromFormat('Y-m-d', $loan->effective_date);
                        }


                        for($x = 1; $x <= $loan->months_to_pay; $x++) {
//                            if($loan->beginning_balance > 0) {
                                if($loan->User->role == 'Employee') {
                                    $dueDate = $effectiveDate->format('Y')."-".$effectiveDate->format('m')."-";
//                                    if($x % 2 == 0) {
                                        if($effectiveDate->format('t') >= $payrollGroup->payroll_date_b) {
                                            $dueDate .= $payrollGroup->payroll_date_b;
                                        } else {
                                            $dueDate .= $effectiveDate->format('t');
                                        }
//                                    } else {
//                                        $dueDate .= $payrollGroup->payroll_date_a;
//                                    }
                                } else {
                                    if($x % 2 == 0) {
                                        $dueDate = $effectiveDate->format('Y-m-t');
                                    } else {
                                        $dueDate = $effectiveDate->format('Y-m-15');
                                    }
                                }

//                                if($x % 2 == 0) {
                                    $payPeriod = $effectiveDate->format('Ym')."B";
//                                } else {
//                                    $payPeriod = $effectiveDate->format('Ym')."A";
//                                }



                                $paymentSchedule = [
                                    'loan_id' => $loan->id,
                                    'month' => $x,
                                    'pay_period' => $payPeriod,
                                    'due_date' => $dueDate,
                                    'amortization' => $loan->beginning_balance > $loan->amortization ? $loan->amortization : $loan->beginning_balance,
                                    'lapsed_interest' => 0
                                ];

                                LoanPaymentSchedules::create($paymentSchedule);

//                                if($x % 2 == 0) {
                                    $effectiveDate = $effectiveDate->addMonth(1);
//                                }

                                $loan->beginning_balance -= $loan->amortization;
//                            }
                        }
                    } else {
                        if($loan->User->role == 'Employee') {
                            $payrollGroup = $loan->User->Profile->Compensation->PayrollGroup;
                            $effectiveDate = Carbon::createFromFormat('Y-m-d', !empty($loan->effective_date) ? $loan->effective_date : $loan->date_request);
                        } else {
                            $effectiveDate = Carbon::createFromFormat('Y-m-d', $loan->effective_date);
                        }

                        for($x = 1; $x <= $loan->months_to_pay * 2; $x++) {
                            if($loan->User->role == 'Employee') {
                                $dueDate = $effectiveDate->format('Y')."-".$effectiveDate->format('m')."-";
                                if($x % 2 == 0) {
                                    if($effectiveDate->format('t') >= $payrollGroup->payroll_date_b) {
                                        $dueDate .= $payrollGroup->payroll_date_b;
                                    } else {
                                        $dueDate .= $effectiveDate->format('t');
                                    }
                                } else {
                                    $dueDate .= $payrollGroup->payroll_date_a;
                                }
                            } else {
                                if($x % 2 == 0) {
                                    $dueDate = $effectiveDate->format('Y-m-t');
                                } else {
                                    $dueDate = $effectiveDate->format('Y-m-15');
                                }
                            }

                            if($x % 2 == 0) {
                                $payPeriod = $effectiveDate->format('Ym')."B";
                            } else {
                                $payPeriod = $effectiveDate->format('Ym')."A";
                            }



                            $paymentSchedule = [
                                'loan_id' => $loan->id,
                                'user_id' => $loan->user_id,
                                'month' => $x,
                                'pay_period' => $payPeriod,
                                'due_date' => $dueDate,
                                'amortization' => $loan->amortization,
                                'lapsed_interest' => 0
                            ];

                            if(!LoanPaymentSchedules::create($paymentSchedule)) {
                                dd('Problem encountered at row: '. $x);
                            }

                            if($x % 2 == 0) {
                                $effectiveDate = $effectiveDate->addMonth(1);
                            }
                        }
                    }




                    $data['valid'][] = $loan;
                } else {
                    $data['invalid'][] = $item;
                }

                $x++;
            }

            return view('admin.head.import_loans_result', $data);

        });

        return view('admin.head.import_loans_result', $data);

        return redirect()->back();

    }
}

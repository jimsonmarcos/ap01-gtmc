<?php

namespace App\Http\Controllers\Admin\Head;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaxScheduleController extends Controller
{
    //
    public function daily_tax_schedule()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.table.daily_tax_schedule', $data);
    }

    public function daily_tax_schedule_update()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.table.update_daily_tax_schedule', $data);
    }

    public function weekly_tax_schedule()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.table.weekly_tax_schedule', $data);
    }

    public function weekly_tax_schedule_update()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.table.update_weekly_tax_schedule', $data);
    }

    public function semi_monthly_tax_schedule()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.table.semi_monthly_tax_schedule', $data);
    }

    public function semi_monthly_tax_schedule_update()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.table.update_semi_monthly_tax_schedule', $data);
    }

    public function monthly_tax_schedule()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.table.monthly_tax_schedule', $data);
    }

    public function monthly_tax_schedule_update()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.table.update_monthly_tax_schedule', $data);
    }
}

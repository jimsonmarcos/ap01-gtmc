<?php

namespace App\Http\Controllers\Admin\Head;

use App\Keyval;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoopShareInterestController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Coop Share Interest";

        return view('admin.head.coop_share_interest', $data);
    }

    public function save(Request $request)
    {

        Keyval::where('key', 'coop_share_interest')->update(['value' => $request->coop_share_interest]);

        $request->session()->flash('popSuccess', 'COOP Share Interest successfully updated.');
        return redirect()->back();
    }

}

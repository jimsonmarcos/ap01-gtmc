<?php

namespace App\Http\Controllers\Admin\Head;

use App\IncomeDeductionItems;
use App\Keyval;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class IncomeDeductionItemsController extends Controller
{
    public function index(Request $request, $type = 'income')
    {
        $data['pageHeader'] = $type == 'income' ? 'Income Items' : 'Deduction Items';
        $data['nav'] = $type;

        $data['t'] = ($type == 'income' ? 'Income' : 'Deduction');
        $data['items'] = IncomeDeductionItems::where('type', $data['t'])->orderBy('item')->get();

        return view('admin.head.income_deduction_items', $data);
    }

    public function save(Request $request)
    {
        $item = $request->except('_token');
        $item['user_id'] = Auth::user()->id;

        IncomeDeductionItems::create($item);

        $request->session()->flash('popSuccess', "{$item['type']} Item successfully added.");
        return redirect()->back();
    }

    public function delete(Request $request, $id)
    {
        $item = IncomeDeductionItems::find($id);
        IncomeDeductionItems::where('id', $id)->delete();

        $request->session()->flash('popSuccess', "{$item->item} successfully deleted.");
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin\Head;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeductionTypeController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Deduction Type";

        return view('admin.head.deduction_type', $data);
    }

    public function create_deduction_type()
    {
        $data['pageHeader'] = "New Deduction Type";

        return view('admin.head.deduction_type_create', $data);
    }
}

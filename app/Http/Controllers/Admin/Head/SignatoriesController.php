<?php

namespace App\Http\Controllers\Admin\Head;

use App\Signatories;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SignatoriesController extends Controller
{
    //
    public function signatories()
    {
        $data['pageHeader'] = "Admin Settings";
        $data['signatories'] = Signatories::select(['signatories.*', 'u.name'])->join('users as u', 'signatories.user_id', '=', 'u.id')->orderBy('u.name')->get();

        return view('admin.head.signatories', $data);
    }

    public function signatories_create($role = 'Employee')
    {
        $data['pageHeader'] = "Admin Settings";
        $data['role'] = $role;
        $data['users'] = User::where('role', $role)->whereNotIn('id', Signatories::pluck('user_id'))->orderBy('name')->get();

        return view('admin.head.signatories_create', $data);
    }

    public function signatories_create_post(Request $request)
    {
        $signatory = new Signatories();
        $signatory->user_id = $request->user_id;
        $signatory->added_by = Auth::user()->id;
        if(!empty($request->position_id)) {
            $signatory->position_id = $request->position_id;
        }
        $signatory->save();

        $user = User::find($request->user_id);

        $request->session()->flash('popSuccess', "<strong>{$user->name}</strong> successfully added as new Signatory");
        return redirect()->route('head_signatories');
    }

    public function signatories_delete(Request $request, $id)
    {
        $user = Signatories::find($id);
        $user->delete();

        $request->session()->flash('popSuccess', "<strong>{$user->name}</strong> successfully added as new Signatory");
        return redirect()->route('head_signatories');
    }
}

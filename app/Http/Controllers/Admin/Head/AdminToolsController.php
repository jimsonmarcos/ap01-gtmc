<?php

namespace App\Http\Controllers\Admin\Head;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminToolsController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.admin_tools', $data);
    }

    public function settings()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.settings', $data);
    }

}

<?php

namespace App\Http\Controllers\Admin\Head;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeductionCategoryController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Deduction Category";

        return view('admin.head.deduction_category', $data);
    }

    public function create_deduction_category()
    {
        $data['pageHeader'] = "New Deduction Category";

        return view('admin.head.deduction_category_create', $data);
    }

//    public function create_income_category(Request $request)
//    {
//        $positionName = $request['title'];
//
//        if(Positions::where('title', $positionName)->exists()) {
//            $request->session()->flash('popError', 'Position name already exists!');
//            return redirect()->back();
//        }
//
//        Positions::create(['title' => $positionName]);
//
//        $request->session()->flash('popSuccess', "Position <strong>{$positionName}</strong> successfully added");
//        return redirect()->route('head_position');
//
//    }

}

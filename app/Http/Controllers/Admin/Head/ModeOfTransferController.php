<?php

namespace App\Http\Controllers\Admin\Head;

use App\TransferModes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModeOfTransferController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Mode Of Transfer";

        return view('admin.head.mode_of_transfer', $data);
    }

    public function create_mode_of_transfer()
    {
        $data['pageHeader'] = "New Mode Of Transfer";

        return view('admin.head.mode_of_transfer_create', $data);
    }

    public function create_mode_of_transfer_post(Request $request)
    {
        $data = $request->except('_token');
        foreach($data as $d) {
            $d = strtoupper(trim($d));
        }

        if(TransferModes::where([
            'transfer_mode' => $data['transfer_mode'],
            'transfer_type' => $data['transfer_type'],
            'bank' => $data['bank']
        ])->exists()) {
            $request->session()->flash('popError', 'Mode of Transfer already exists!');
            return redirect()->back();
        }

        TransferModes::create($data);

        $request->session()->flash('popSuccess', "Mode of Transfer successfully added");
        return redirect()->route('head_mode_of_transfer');

    }

    public function mode_of_transfer_edit(Request $request,$id)
    {
        if(TransferModes::where('id', $id)->exists()) {
            TransferModes::where('id', $id)->update(['title' => $request->title]);
            $request->session()->flash('popSuccess', 'Mode of Transfer Successfully updated');
        } else {
            $request->session()->flash('popError', 'Mode of Transfer does not exists');
        }


        return redirect()->back();
    }

    public function mode_of_transfer_delete(Request $request, $id)
    {
        if(TransferModes::where('id', $id)->exists()) {
            TransferModes::where('id', $id)->delete();
            $request->session()->flash('popSuccess', 'Mode of Transfer Successfully deleted');
        } else {
            $request->session()->flash('popError', 'Mode of Transfer does not exists');
        }

        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin\Head;

use App\EmploymentDetails;
use App\Positions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PositionController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Position";

        return view('admin.head.position', $data);
    }

    public function create_position()
    {
        $data['pageHeader'] = "New Position";

        return view('admin.head.position_create', $data);
    }

    public function create_position_post(Request $request)
    {
        $positionName = strtoupper($request['title']);

        if(strlen($positionName) == 0) {
            $request->session()->flash('popError', 'Position name must not be empty.');
            return redirect()->back();
        }

        if(Positions::where('title', $positionName)->exists()) {
            $request->session()->flash('popError', 'Position name already exists!');
            return redirect()->back();
        }

        Positions::create(['title' => $positionName]);

        $request->session()->flash('popSuccess', "Position <strong>{$positionName}</strong> successfully added");
        return redirect()->route('head_position');

    }

    public function position_edit(Request $request,$id)
    {
        if(Positions::where('id', $id)->exists()) {
            Positions::where('id', $id)->update(['title' => strtoupper($request->title)]);
            $request->session()->flash('popSuccess', 'Position Successfully updated');
        } else {
            $request->session()->flash('popError', 'Position does not exists');
        }


        return redirect()->back();
    }
    public function position_delete(Request $request, $id)
    {
        if(Positions::where('id', $id)->exists()) {
            if(EmploymentDetails::where('position_id', $id)->count() == 0) {
                Positions::where('id', $id)->delete();
                $request->session()->flash('popSuccess', 'Position Successfully deleted');
            } else {
                $request->session()->flash('popError', 'You cannot delete this Position.');
            }
        } else {
            $request->session()->flash('popError', 'Position does not exists');
        }

        return redirect()->back();
    }
}

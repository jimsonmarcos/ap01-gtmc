<?php

namespace App\Http\Controllers\Admin\Head;

use App\MemberRelationships;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RelationshipController extends Controller
{
    //
    public function relationship()
    {
        $data['pageHeader'] = "Admin Settings";
        $data['relationship'] = MemberRelationships::all();
        return view('admin.head.relationship', $data);
    }

    public function relationship_create()
    {
        $data['pageHeader'] = "Admin Settings";

        return view('admin.head.relationship_create', $data);
    }

    public function relationship_create_post(Request $request)
    {
        $relationshipName = $request['title'];

        if(MemberRelationships::where('title', $relationshipName)->exists()) {
            $request->session()->flash('popError', 'Relationship name already exists!');
            return redirect()->back();
        }

        MemberRelationships::create(['title' => $relationshipName]);

        $request->session()->flash('popSuccess', "Relationship <strong>{$relationshipName}</strong> successfully added");
        return redirect()->route('head_relationship');

    }

    public function relationship_edit(Request $request,$id)
    {
        if(MemberRelationships::where('id', $id)->exists()) {
            MemberRelationships::where('id', $id)->update(['title' => $request->title]);
            $request->session()->flash('popSuccess', 'Relationship Successfully updated');
        } else {
            $request->session()->flash('popError', 'Relatioship does not exists');
        }


        return redirect()->back();
    }

    public function relationship_delete(Request $request, $id)
    {
        if(MemberRelationships::where('id', $id)->exists()) {
            MemberRelationships::where('id', $id)->delete();
            $request->session()->flash('popSuccess', 'Relationship Successfully deleted');
        } else {
            $request->session()->flash('popError', 'Relationship does not exists');
        }

        return redirect()->back();
    }
}

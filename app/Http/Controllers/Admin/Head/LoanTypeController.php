<?php

namespace App\Http\Controllers\Admin\Head;

use App\LoanTypes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanTypeController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Loan Type";

        return view('admin.head.loan_type', $data);
    }

    public function create_loan_type()
    {
        $data['pageHeader'] = "New Loan Type";

        return view('admin.head.loan_type_create', $data);
    }

    public function loan_type_create_post(Request $request)
    {
        $data = $request->except('_token');

        LoanTypes::create($data);

        $request->session()->flash('popSuccess', "Loan Type successfully added");
        return redirect(url('/admin/super/loan-type'));

    }

    public function edit(Request $request, $id)
    {
        $data = $request->except('_token');

        LoanTypes::where('id', $id)->update($data);

        $request->session()->flash('popSuccess', "Loan Type successfully updated");
        return redirect(url('/admin/super/loan-type'));
    }

    public function delete(Request $request, $id)
    {
        if(LoanTypes::where('id', $id)->count() == 1) {
            LoanTypes::where('id', $id)->delete();

            $request->session()->flash('popSuccess', "Loan Type successfully deleted");
            return redirect(url('/admin/super/loan-type'));
        }
    }
}

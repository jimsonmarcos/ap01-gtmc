<?php

namespace App\Http\Controllers\Admin\Head;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MallController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Mall";

        return view('admin.head.mall', $data);
    }

    public function create_mall()
    {
        $data['pageHeader'] = "New Mall";

        return view('admin.head.mall_create', $data);
    }

//    public function create_mall_post(Request $request)
//    {
//        $positionName = $request['title'];
//
//        if(Positions::where('title', $positionName)->exists()) {
//            $request->session()->flash('popError', 'Position name already exists!');
//            return redirect()->back();
//        }
//
//        Positions::create(['title' => $positionName]);
//
//        $request->session()->flash('popSuccess', "Position <strong>{$positionName}</strong> successfully added");
//        return redirect()->route('head_position');
//
//    }

}

<?php

namespace App\Http\Controllers\Admin\Head;

use App\Keyval;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanablePercentageController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "Loanable Percentage";

        return view('admin.head.loanable_percentage', $data);
    }

    public function save(Request $request)
    {

        Keyval::where('key', 'loanable_percentage')->update(['value' => $request->loanable_percentage]);

        $request->session()->flash('popSuccess', 'Loanable Percentage successfully updated.');
        return redirect()->back();
    }

}

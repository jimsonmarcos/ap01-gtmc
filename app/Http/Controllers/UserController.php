<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Home';
        $data['user'] = User::find(Auth::user()->id);
        $data['banner'] = view('user.banner', $data);

        return view('user.home', $data);
    }

    public function password_reset()
    {
        $data['pageHeader'] = 'Password Reset';
        $data['user'] = User::find(Auth::user()->id);
        $data['banner'] = view('user.banner', $data);

        return view('settings.password_reset_user_profile', $data);
    }


}

<?php

namespace App\Http\Controllers;

use App\Compensations;
use App\EmploymentDetails;
use App\PayrollGroups;
use App\Profiles;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function for_deletion()
    {

        Excel::load(storage_path('app/FOR-DELETION.xlsx'), function($reader) {

            $results = $reader->get();

//            dd($results);

            foreach($results as $result) {
                $user = User::where('username', $result->username)->first();

                if(!empty($user)) {
                    $compensation = Compensations::where('user_id', $user->id)->first();
                    if(!empty($compensation)) {
                        Compensations::where('user_id', $user->id)->delete();
                    }

                    $employmentDetails = EmploymentDetails::where('user_id', $user->id)->first();
                    if(!empty($employmentDetails)) {
                        EmploymentDetails::where('user_id', $user->id)->delete();
                    }

                    $profile = Profiles::where('user_id', $user->id)->first();
                    if(!empty($profile)) {
                        Profiles::where('user_id', $user->id)->delete();
                    }

                    User::where('username', $result->username)->delete();
                } else {
                    echo "Already deleted {$result->username}<br>";
                }
            }

        });
    }

    public function payroll_group_update()
    {

        Excel::load(storage_path('app/gtmc-no-payroll-group.csv'), function($reader) {

            $results = $reader->get();

//            dd($results);

            foreach($results as $result) {
                $profile = Profiles::where('id_number', $result->id_number)->first();

                if(!empty($profile)) {
                    $compensation = Compensations::where('user_id', $profile->user_id)->first();

                    $payrollGroup = PayrollGroups::where('group_name', $result->payroll_group)->first();

                    if(!empty($payrollGroup)) {
                        if(!empty($compensation)) {
                            $compensation->payroll_group_id = $payrollGroup->id;
                            $compensation->save();
                        }
                    }



                }
            }

        });
    }
}

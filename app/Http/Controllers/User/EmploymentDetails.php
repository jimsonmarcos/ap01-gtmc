<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class EmploymentDetails extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Employment Details';
        $data['user'] = User::find(Auth::user()->id);

        $data['nav'] = 'employment';

//        $data['banner'] = view('user.banner', $data);

        return view('user.employment_details', $data);
    }
}

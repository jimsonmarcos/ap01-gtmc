<?php

namespace App\Http\Controllers\User;

use App\OtherPayments;
use App\PayrollCycles;
use App\PayrollSummary;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Shares extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Contributions - Share';
        $data['user'] = $user = User::find(Auth::user()->id);
        $data['nav'] = 'contributions';
        $data['subnav'] = 'shares';

        $data['shares'] = OtherPayments::where(['transaction_type' => 'COOP SHARE', 'user_id' => $user->id])->get();

        return view('user.shares', $data);
    }
}

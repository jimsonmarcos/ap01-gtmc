<?php

namespace App\Http\Controllers\User\LoansAndShares;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PhilHealthController extends Controller
{
    //
    public function contribution()
    {
        $data['pageHeader'] = 'PhilHealth Contribution';
        $data['user'] = User::find(Auth::user()->id);
        $data['banner'] = view('user.banner', $data);

        return view('user.loans_and_shares.philhealth_contribution', $data);
    }
}
<?php

namespace App\Http\Controllers\User\LoansAndShares;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CoopController extends Controller
{
    //
    public function share()
    {
        $data['pageHeader'] = 'Coop Share';
        $data['user'] = User::find(Auth::user()->id);
        $data['banner'] = view('user.banner', $data);

        return view('user.loans_and_shares.coop_share', $data);
    }

    public function savings()
    {
        $data['pageHeader'] = 'Coop Savings';
        $data['user'] = User::find(Auth::user()->id);
        $data['banner'] = view('user.banner', $data);

        return view('user.loans_and_shares.coop_savings', $data);
    }

    public function loans()
    {
        $data['pageHeader'] = 'Coop Loans';
        $data['user'] = User::find(Auth::user()->id);
        $data['banner'] = view('user.banner', $data);

        return view('user.loans_and_shares.coop_loans', $data);
    }
}




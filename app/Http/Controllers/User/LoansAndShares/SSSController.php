<?php

namespace App\Http\Controllers\User\LoansAndShares;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SSSController extends Controller
{
    //
    public function contribution()
    {
        $data['pageHeader'] = 'SSS Contribution';
        $data['user'] = User::find(Auth::user()->id);
        $data['banner'] = view('user.banner', $data);

        return view('user.loans_and_shares.sss_contribution', $data);
    }

    public function loan()
    {
        $data['pageHeader'] = 'SSS Loan';
        $data['user'] = User::find(Auth::user()->id);
        $data['banner'] = view('user.banner', $data);

        return view('user.loans_and_shares.sss_loan', $data);
    }
}

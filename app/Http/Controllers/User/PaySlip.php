<?php

namespace App\Http\Controllers\User;

use App\PayrollCycles;
use App\PayrollSummary;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PaySlip extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Pay Slip';
        $data['user'] = $user = User::find(Auth::user()->id);
        $data['nav'] = 'payslip';

        $data['payrollCycles'] = PayrollCycles::whereIn('id', PayrollSummary::where('user_id', $user->id)->pluck('payroll_cycle_id'))->orderByDesc('payroll_date')->get();

        return view('user.payslip', $data);
    }
}

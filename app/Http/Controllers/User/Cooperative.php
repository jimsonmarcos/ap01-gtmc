<?php

namespace App\Http\Controllers\User;

use App\LeaveHistory;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Cooperative extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Cooperative';
        $data['user'] = User::find(Auth::user()->id);
        $data['nav'] = 'coop';
        $data['beneficiaries'] = $data['user']->Profile->Beneficiaries;

//        $data['banner'] = view('user.banner', $data);

        return view('user.cooperative', $data);
    }
}

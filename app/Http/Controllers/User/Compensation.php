<?php

namespace App\Http\Controllers\User;

use App\LeaveHistory;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Compensation extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Compensation';
        $data['user'] = User::find(Auth::user()->id);
        $data['nav'] = 'compensation';

//        $data['banner'] = view('user.banner', $data);

        return view('user.compensation', $data);
    }
}

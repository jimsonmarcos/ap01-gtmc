<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PersonalInformationController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Personal Information';

        $data['user'] = User::find(Auth::user()->id);

        $data['nav'] = 'profile';


        return view('user.personal_information', $data);
    }
}

<?php

namespace App\Http\Controllers\User;

use App\LoanPaymentSchedules;
use App\LoanPayments;
use App\Loans;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoansController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Loans';
        $data['user'] = $user = User::find(Auth::user()->id);
        $data['nav'] = 'loans';

        $loans = Loans::select(DB::raw('loans.id, loans.loan_type_id, loans.loan_type, loans.is_voucher, loans.cla_number, profiles.id_number, users.name, profiles.first_name, profiles.middle_name, profiles.last_name, cost_centers.cost_center, loan_types.type, loans.granted_loan_amount, loans.status, loans.releasing_status, companies.company_name, COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.total_overdue_amount, 0) as total_overdue_amount, COALESCE(loan_payment_schedules.total_overdue_amount, 0) - COALESCE(loan_payments.total_amount_paid, 0) as outstanding_balance'));
        $loans->where('loans.user_id', $user->id);

        $data['loans'] = $loans = $loans->join('users', 'users.id', '=', 'loans.user_id', 'LEFT')
            ->join('profiles', 'profiles.user_id', '=', 'users.id', 'LEFT')
            ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
            ->join('employment_details', 'employment_details.user_id', '=', 'profiles.user_id', 'LEFT')
            ->join('companies', 'companies.id', '=', 'employment_details.principal_id', 'LEFT')
            ->join('loan_types', 'loan_types.id', '=', 'loans.loan_type_id', 'LEFT')
            ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')
            ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(amortization) + SUM(lapsed_interest) as total_overdue_amount FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payment_schedules.loan_id', '=', 'loans.id')
            ->groupBy('loans.id')
            ->get();


//        $data['banner'] = view('user.banner', $data);

        return view('user.loans', $data);
    }

    public function details($claNumber)
    {
        $data['pageHeader'] = 'Loans';
        $data['user'] = $user = User::find(Auth::user()->id);
        $data['nav'] = 'loans';

        $data['loan'] = $loan = Loans::where('cla_number', $claNumber)->first();

        if(empty($loan)) {
            return redirect()->back();
        }

        if($loan->user_id != $user->id) {
            return redirect()->back();
        }


        return view('user.loans_details', $data);
    }

    public function las($claNumber)
    {
        $data['pageHeader'] = 'Loans';
        $data['user'] = $user = User::find(Auth::user()->id);
        $data['nav'] = 'loans';

        $data['loan'] = $loan = Loans::where('cla_number', $claNumber)->first();

        if(empty($loan)) {
            dd($loan);
            return redirect()->back();
        }

        if($loan->user_id != $user->id) {
            return redirect()->back();
        }

        $data['paymentSchedules'] = LoanPaymentSchedules::where('loan_id', $loan->id)->orderBy('month')->get();


        return view('user.loans_las', $data);
    }

    public function payment_schedule($claNumber)
    {
        $data['pageHeader'] = 'Loans';
        $data['user'] = $user = User::find(Auth::user()->id);
        $data['nav'] = 'loans';

        $data['loan'] = $loan = Loans::where('cla_number', $claNumber)->first();

        if(empty($loan)) {
            dd($loan);
            return redirect()->back();
        }

        if($loan->user_id != $user->id) {
            return redirect()->back();
        }

        $data['paymentSchedules'] = LoanPaymentSchedules::where('loan_id', $loan->id)->orderBy('month')->get();


        return view('user.loans_payment_schedule', $data);
    }

    public function payments($claNumber)
    {
        $data['pageHeader'] = 'Loans';
        $data['user'] = $user = User::find(Auth::user()->id);
        $data['nav'] = 'loans';

        $data['loan'] = $loan = Loans::where('cla_number', $claNumber)->first();

        if(empty($loan)) {
            dd($loan);
            return redirect()->back();
        }

        if($loan->user_id != $user->id) {
            return redirect()->back();
        }

        $data['paymentSchedules'] = LoanPaymentSchedules::where('loan_id', $loan->id)->orderBy('month')->get();
        $data['payments'] = LoanPayments::where('loan_id', $loan->id)->orderBy('payment_date')->get();


        return view('user.loans_payments', $data);
    }
}

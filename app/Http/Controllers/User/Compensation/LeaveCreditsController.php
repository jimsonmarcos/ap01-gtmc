<?php

namespace App\Http\Controllers\User\Compensation;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LeaveCreditsController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Leave Credits';
        $data['user'] = User::find(Auth::user()->id);
        $data['banner'] = view('user.banner', $data);

        return view('user.compensation.leave_credits', $data);
    }
}

<?php

namespace App\Http\Controllers\User\Compensation;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PayslipController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Payslip';
        $data['user'] = User::find(Auth::user()->id);
        $data['banner'] = view('user.banner', $data);

        return view('user.compensation.payslip', $data);
    }
}

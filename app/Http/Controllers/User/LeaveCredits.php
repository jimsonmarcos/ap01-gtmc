<?php

namespace App\Http\Controllers\User;

use App\LeaveHistory;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LeaveCredits extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Leave Credits';
        $data['user'] = User::find(Auth::user()->id);
        $data['leaveHistory'] = LeaveHistory::select(['leave_history.*', 'payroll_cycles.payroll_date'])->where('leave_history.user_id', $data['user']->id)->where('effective_year', date('Y'))->join('payroll_cycles', 'payroll_cycles.id', '=', 'leave_history.payroll_cycle_id')->orderByDesc('payroll_cycles.payroll_date')->get();
        $data['nav'] = 'leave';

//        $data['banner'] = view('user.banner', $data);

        return view('user.leave_credits', $data);
    }
}

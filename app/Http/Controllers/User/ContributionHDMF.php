<?php

namespace App\Http\Controllers\User;

use App\PayrollCycles;
use App\PayrollSummary;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ContributionHDMF extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Contributions - HDMF';
        $data['user'] = $user = User::find(Auth::user()->id);
        $data['nav'] = 'contributions';
        $data['subnav'] = 'hdmf';

        $data['contributions'] = PayrollCycles::whereIn('id', PayrollSummary::where('user_id', $user->id)->pluck('payroll_cycle_id'))->orderByDesc('payroll_date')->get();

        return view('user.contributions.hdmf', $data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Companies;
use App\CostCenters;
use App\Departments;
use App\Loans;
use App\OtherPayments;
use App\PayeeNames;
use App\PayrollGroups;
use App\Profiles;
use App\TransferModes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function MongoDB\BSON\toJSON;

class ApiController extends Controller
{
    public function cost_centers($id)
    {
        if(empty($id)) {
            $costCenters = CostCenters::all();
        } else{
            $costCenters = CostCenters::where('company_id', $id)->get();
        }

        return $costCenters->toJson();
    }

    public function payroll_groups($id)
    {
        if(empty($id)) {
            $costCenters = PayrollGroups::all();
        } else{
            $company = Companies::find($id);

            $costCenters = PayrollGroups::where('company_id', $id)->get();
            if(!empty($company->parent_company_id)) {
                $costCenters = PayrollGroups::where('company_id', $company->ParentCompany->id)->get();
            }
        }

        return $costCenters->toJson();
    }

    public function department_areas(Request $request)
    {
        return Departments::select(DB::raw("COALESCE(area, '') as area"))->where('province', $request->input('province'))->groupBy('area')->orderBy('area')->select('area')->get()->toJson();
    }

    public function department_malls(Request $request)
    {
        return Departments::select(DB::raw("COALESCE(mall, '') as mall"))->where('province', $request->input('province'))->where('area', $request->area == null ? '' : $request->area)->groupBy('mall')->orderBy('mall')->select('mall')->get()->toJson();
    }

    public function department_outlets(Request $request)
    {
        return Departments::select(DB::raw("COALESCE(outlet, '') as outlet"))->where('province', $request->input('province'))->where('area', $request->area == null ? '' : $request->area)->where('mall', $request->mall == null ? '' : $request->mall)->groupBy('outlet')->orderBy('outlet')->select('outlet')->get()->toJson();
    }

    public function department_departments(Request $request)
    {
        return Departments::select(DB::raw("COALESCE(department, '') as department"))->where('province', $request->input('province'))->where('area', $request->area == null ? '' : $request->area)->where('mall', $request->mall == null ? '' : $request->mall)->where('outlet', $request->outlet == null ? '' : $request->outlet)->groupBy('department')->orderBy('department')->select(['department', 'id'])->get()->toJson();
    }

    public function transfer_mode_types(Request $request)
    {
        return TransferModes::where('transfer_mode', $request->input('transfer_mode'))->orderBy('transfer_type')->groupBy('transfer_type')->select('transfer_type')->get()->toJson();
    }

    public function transfer_mode_banks(Request $request)
    {
        return TransferModes::where('transfer_mode', $request->input('transfer_mode'))->where('transfer_type', $request->input('transfer_type'))->orderBy('bank')->select(DB::raw("id, COALESCE(bank, 'N/A') as bank"))->get()->toJson();
    }

    public function users_search_add_admin(Request $request)
    {
        return json_encode(array('items' => Profiles::where('profiles.status', 'Active')->whereRaw(DB::raw("(users.parent_id IS NULL)"))
            ->whereRaw( "(id_number LIKE '%{$request['q']}%' OR CONCAT(profiles.first_name, ' ', profiles.last_name) LIKE '%{$request['q']}%')")
            ->select(DB::raw('users.role, profiles.id, profiles.user_id, CONCAT(profiles.first_name, " ", profiles.middle_name, " ", profiles.last_name) as name,profiles.first_name, profiles.middle_name, profiles.last_name, profiles.id_number, profiles.email, membership_category, gender, companies.company_code as principal'))
            ->join('employment_details', 'profiles.user_id', '=', 'employment_details.user_id', 'INNER')
            ->join('companies', 'employment_details.principal_id', '=', 'companies.id', 'LEFT')
            ->join('users', 'users.id', '=', 'profiles.user_id', 'LEFT')
            ->get()->toArray()));
    }

    public function users(Request $request, $status = 'Active')
    {
        return json_encode(array('items' =>  User::where('id_number', 'LIKE', "%{$request['q']}%")->orWhere('name', 'LIKE', "%{$request['q']}%")
            ->select(DB::raw('users.role, profiles.id, profiles.user_id, CONCAT(profiles.first_name, " ", profiles.middle_name, " ", profiles.last_name) as name,profiles.first_name, profiles.middle_name, profiles.last_name, profiles.id_number, profiles.email, membership_category, gender, companies.company_code as principal, companies.id as company_id, cost_centers.cost_center'))
            ->join('profiles', 'users.id', '=', 'profiles.user_id', 'LEFT')
            ->join('employment_details', 'profiles.user_id', '=', 'employment_details.user_id', 'INNER')
            ->join('companies', 'employment_details.principal_id', '=', 'companies.id', 'LEFT')
            ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
            ->where('profiles.status', $status)
            ->whereIn('users.role', ['Employee', 'Member'])
            ->get()->toArray()));
    }

    public function employees(Request $request, $status = 'Active')
    {
        return json_encode(array('items' =>  User::where('role', 'Employee')->where('id_number', 'LIKE', "%{$request['q']}%")->orWhere('name', 'LIKE', "%{$request['q']}%")
            ->select(DB::raw('users.role, profiles.id, profiles.user_id, CONCAT(profiles.first_name, " ", profiles.middle_name, " ", profiles.last_name) as name,profiles.first_name, profiles.middle_name, profiles.last_name, profiles.id_number, profiles.email, membership_category, gender, companies.company_code as principal, cost_centers.cost_center'))
            ->join('profiles', 'users.id', '=', 'profiles.user_id', 'LEFT')
            ->join('employment_details', 'profiles.user_id', '=', 'employment_details.user_id', 'INNER')
            ->join('companies', 'employment_details.principal_id', '=', 'companies.id', 'LEFT')
            ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
            ->where('profiles.status', $status)
            ->whereIn('users.role', ['Employee'])
            ->get()->toArray()));
    }

    public function members(Request $request)
    {
        return json_encode(array('items' =>  User::where('role', 'Member')->where('id_number', 'LIKE', "%{$request['q']}%")->orWhere('name', 'LIKE', "%{$request['q']}%")
            ->select(DB::raw('users.role, profiles.id, profiles.user_id, CONCAT(profiles.first_name, " ", profiles.middle_name, " ", profiles.last_name) as name,profiles.first_name, profiles.middle_name, profiles.last_name, profiles.id_number, profiles.email, membership_category, gender'))
            ->join('profiles', 'users.id', '=', 'profiles.user_id', 'LEFT')
            ->whereIn('users.role', ['Member'])
            ->get()->toArray()));
    }

    public function clients(Request $request)
    {
        return json_encode(array('items' =>  Companies::where('company_name', 'LIKE', "%{$request['q']}%")->orWhere('company_code', 'LIKE', "%{$request['q']}%")->orderBy('company_code')
            ->get()->toArray()));
    }

    public function payee_names(Request $request)
    {
        return json_encode(array('items' =>  PayeeNames::where('payee_name', 'LIKE', "%{$request['q']}%")->orderBy('payee_name')
            ->get()->toArray()));
    }

    public function reports_users(Request $request, $membershipCategory)
    {
        $query = $request->q;
        return json_encode([
            'items' => Profiles::where('membership_category', $membershipCategory)
                ->selectRaw('profiles.*, DATE_FORMAT(profiles.join_date, "%m/%d/%Y") as join_date, profiles.membership_category, cost_centers.cost_center, companies.company_name, companies.company_code')
                ->join('cost_centers', 'cost_centers.id', '=', 'profiles.cost_center_id', 'LEFT')
                ->join('employment_details', 'employment_details.user_id', '=', 'profiles.user_id', 'LEFT')
                ->join('companies', 'companies.id', '=', 'employment_details.principal_id', 'LEFT')
                ->whereRaw("(id_number LIKE '%{$query}%' OR CONCAT(first_name, ' ', last_name) LIKE '%{$query}%')")
                ->get()
                ->toArray()
        ]);
    }

    public function reports_individual_loans(Request $request, $user_id)
    {
        $loans = Loans::select(DB::raw('loans.id, loans.loan_type_id, loans.loan_type, loans.is_voucher, loans.cla_number, DATE_FORMAT(loans.date_request, "%m/%d/%Y") as date_request, loan_types.type, loans.request_amount, loans.granted_loan_amount, loans.status, loans.releasing_status, COALESCE(GROUP_CONCAT(loan_payments.ar_number), "") as ar_number, COALESCE(GROUP_CONCAT(loan_payments.payment_method), "") as payment_method, COALESCE(GROUP_CONCAT(loan_payments.pay_period), "") as pay_period, COALESCE(loan_payments.total_interest_paid, 0) as total_interest_paid, COALESCE(loan_payments.total_amount_paid, 0) as total_amount_paid, COALESCE(loan_payment_schedules.total_overdue_amount, 0) as total_overdue_amount, COALESCE(loan_payment_schedules.total_overdue_amount, 0) - COALESCE(loan_payments.total_amount_paid, 0) as outstanding_balance'))

            ->join('loan_types', 'loan_types.id', '=', 'loans.loan_type_id', 'LEFT')
            ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, ar_number, pay_period, payment_method, SUM(total_amount_paid) as total_amount_paid, SUM(interest) as total_interest_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')
            ->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(amortization) + SUM(lapsed_interest) as total_overdue_amount FROM loan_payment_schedules GROUP BY loan_id) loan_payment_schedules"), 'loan_payment_schedules.loan_id', '=', 'loans.id')
            ->where('loans.user_id', $user_id)
            ->where('loans.is_voucher', 'N')
            ->where('status', 'Approved')
            ->groupBy('loans.id')
            ->orderByDesc('loans.cla_number')
            ->get()
            ->toArray();

        return json_encode($loans);
    }

    public function reports_individual_shares(Request $request, $user_id)
    {
        $shares = OtherPayments::selectRaw("other_payments.*, DATE_FORMAT(other_payments.payment_date, '%m/%d/%Y') as payment_date")
            ->where('user_id', $user_id)
            ->where('transaction_type', 'COOP SHARE')
            ->orderByDesc('payment_date')
            ->get()
            ->toArray();

        return json_encode($shares);
    }
}

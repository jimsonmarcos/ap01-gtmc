<?php

namespace App\Http\Controllers\Member;

use App\Profiles;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = "User Profile";
        $data['user'] = User::find(Auth::user()->id)->Profile;

        return view('member.profile', $data);
    }

    public function loans_shares(Request $request, $id)
    {

        $data['id'] = $id;
        $data['pageHeader'] = "User Profiles";
        $data['user'] = User::find(Auth::user()->id)->Profile;
        $data['beneficiaries'] = $data['user']->Beneficiaries;
//        dd($data['beneficiaries']);
        return view('member.loans_shares', $data);
    }

    public function loan_payments(){
        $data['pageHeader'] = "User Profile";
        $data['user'] = User::find(Auth::user()->id)->Profile;

        return view('member.loan_payments', $data);
    }
}

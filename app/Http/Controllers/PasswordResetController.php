<?php

namespace App\Http\Controllers;

use App\AuditLog;
use App\Mail\PasswordResetMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class PasswordResetController extends Controller
{
    //
    public function admin_password_reset()
    {
        $data['pageHeader'] = 'Password Reset';
        return view('settings.password_reset_admin', $data);
    }

    public function client_password_reset()
    {
        $data['pageHeader'] = 'Password Reset';
        return view('settings.password_reset_client', $data);
    }

    public function employees_and_members_password_reset($userAccess = null)
    {
        if(!empty($userAccess)) {
            if($userAccess == 'Admin') {
                $data['users'] = User::where('role', '!=', '')
                    ->select('users.*')
//                    ->join('profiles', 'profiles.user_id', '=', 'users.id', 'inner')
//                    ->where('profiles.status', 'Active')
                    ->whereIn('role', ['Admin', 'AdminPayroll', 'AdminCoop', 'AdminHR', 'AdminAcctg'])
//                    ->orderBy('profiles.first_name')
                    ->get();
            } elseif($userAccess == 'Employee') {
                $data['users'] = User::where('role', 'Employee')
                    ->select('users.*')
                    ->join('profiles', 'profiles.user_id', '=', 'users.id')
                    ->leftJoin('employment_details', 'employment_details.user_id', '=', 'users.id')
                    ->where('profiles.status', 'Active')
                    ->whereRaw( "(employment_details.reason != 'Resigned' OR employment_details.reason IS NULL)")
                    ->orderBy('profiles.first_name')
                    ->get();
            } elseif($userAccess == 'Member') {
                $data['users'] = User::where('role', 'Member')
                    ->select('users.*')
                    ->join('profiles', 'profiles.user_id', '=', 'users.id', 'inner')
                    ->where('profiles.status', 'Active')
                    ->orderBy('profiles.first_name')
                    ->get();
            }


            $data['userAccess'] = $userAccess;
        }

        $data['pageHeader'] = 'Password Reset';
        return view('settings.password_reset_employees_members', $data);
    }

    public function reset_password_post(Request $request)
    {
        $user = User::find($request->id);

//        $role = $user->role == 'Employee' ? 'E' : 'M';
//        $series = (string) $user->series;
//        if(strlen($series) < 4) {
//            while(strlen($series) < 4) {
//                $series = "0{$series}";
//            }
//        }
//        $password = "{$role}{$series}";
//
//        dd($user);

        $user->password = bcrypt( $user->username );
        $user->save();

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'User Account',
            'action' => 'Reset Password',
            'description' => "Reset password of {$user->username}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        Mail::send(
            new PasswordResetMail($user)
        );

        $name = !empty($user->Profile) ? $user->Profile->name() : $user->name;

        $request->session()->flash('success', $name ." password has been reset");
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;
ini_set('max_execution_time', 1200); //3 minutes
use App\Areas;
use App\CivilStatus;
use App\Companies;
use App\Compensations;
use App\CostCenters;
use App\DailyRates;
use App\Departments;
use App\EducationalAttainments;
use App\EmploymentDetails;
use App\Malls;
use App\Outlets;
use App\PayrollGroups;
use App\Positions;
use App\Profiles;
use App\Provinces;
use App\TaxTypes;
use App\TransferModes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use PHPExcel_Style_NumberFormat;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function index()
    {
        $data['sidebar'] = view("admin.sidebar");

        return view('admin.home', $data);
    }

    public function uploadEmployees()
    {
        $excel = App::make('excel');


//        $result = $excel->load('storage/app/employees.xlsx', function($reader) {
//            $reader->first();
//            $reader->skipRows(0);
//            // Disable date formatting
////            $reader->formatDates(false);
//            $reader->calculate(false);
////            $reader->get();
////            $reader->toArray();
////            dd($reader);
//        })->get();

        $excel->filter('chunk')->selectSheetsByIndex(0)->load('storage/app/employees-v1.xlsx')->chunk(250, function ($results) {



            foreach ($results as $item) {
                $dateFields = ['birthday', 'hire_date', 'trainee_start_date', 'trainee_end_date'];

                foreach($dateFields as $field) {
                    if(!empty($item->$field) && substr_count('-', $item->$field) == 0) {
                        $item->$field = PHPExcel_Style_NumberFormat::toFormattedString($item->$field, 'YYYY-MM-DD');
                    }
                }


//                dd($item);

                $user = [
                    'series' => str_replace(['M', 'E'], '', trim($item->username)),
                    'email' => strtolower($item->email),
                    'username' => strtoupper($item->username),
                    'role' => (strtoupper($item->membership_category) == 'EMPLOYEE') ? "Employee" : "Member",
                ];


                $name = $item->first_name;
                $name = !empty($item->middle_name) ? $name . " {$item->middle_name}" : $name;
                $user['name'] = !empty($item->last_name) ? $name . " {$item->last_name}" : $name;

                $user['password'] = bcrypt($user['username']);
//                dd($user);
                $user = User::create($user);


                $civilStatus = '';
                if (!empty($item->civil_status)) {
                    if (CivilStatus::where('title', strtoupper($item->civil_status))->count() == 0) {
                        $civilStatus = CivilStatus::create(['title' => strtoupper($item->civil_status)]);
                        $civilStatus = $civilStatus->id;
                    } else {
                        $civilStatus = CivilStatus::where('title', strtoupper($item->civil_status))->first()->id;
                    }
                }


                $educationalAttainment = '';
                if (!empty(!empty($item->educational_attainment))) {
                    if (EducationalAttainments::where('title', strtoupper($item->educational_attainment))->count() == 0) {
                        $educationalAttainment = EducationalAttainments::create(['title' => strtoupper($item->educational_attainment)]);
                        $educationalAttainment = $educationalAttainment->id;
                    } else {
                        $educationalAttainment = EducationalAttainments::where('title', strtoupper($item->educational_attainment))->first()->id;
                    }
                }


                $profile = [
                    'id_number' => $item->gtmc_id_number,
                    'first_name' => strtoupper($item->first_name),
                    'middle_name' => strtoupper($item->middle_name),
                    'last_name' => strtoupper($item->last_name),
                    'address' => strtoupper($item->address),
                    'email' => strtolower($item->email),
                    'mobile1' => $item->mobile_1,
                    'mobile2' => $item->mobile_2,
                    'gender' => $item->gender,
                    'mothers_maiden_name' => strtoupper($item->mothers_maiden_name),
                    'birthday' => !empty($item->birthday) ? $item->birthday : '',
                    'join_date' => !empty($item->hire_date) ? $item->hire_date : '',
                    'civil_status_id' => $civilStatus,
                    'educational_attainment_id' => $educationalAttainment,
                    'icoe_contact_name' => strtoupper($item->icoe_contact_name),
                    'icoe_contact_address' => strtoupper($item->icoe_contact_address),
                    'icoe_contact_mobile' => strtoupper($item->icoe_contact_number),
                    'membership_category' => ucfirst(strtolower($item->membership_category)),
                    'membership_type' => ucfirst(strtolower($item->membership_type)),
                    'status' => ucfirst(strtolower($item->status)),
                    'coop_share' => $item->contribution_per_payday,
                    'cost_center_id' => CostCenters::where('cost_center', $item->cost_center)->exists() ? CostCenters::where('cost_center', $item->cost_center)->first()->id : ''
                ];

                $profile['user_id'] = $user->id;

//                dd($profile);
                Profiles::create($profile);

                $principal = Companies::where('company_code', $item->principal)->first();
                if (!empty($principal)) {
                    $employmentDetails['principal_id'] = $principal->id;
                }


                $employmentDetails['coordinator'] = $item->coordinator;
                if ($profile['membership_category'] == 'Employee') {
                    $position = Positions::where('title', $item->position)->first();
                    if (!empty($position)) {
                        $employmentDetails['position_id'] = $position->id;
                    }

//                    $department = Departments::where(['province' => $item->province, 'area' => $item->area, 'mall' => $item->mall, 'outlet' => $item->outlet, 'department' => $item->department]);
//                    if ($department->count() > 0) {
//                        $department = $department->first()->id;
//                    } else {
//                        $department = Departments::create(['province' => $item->province, 'area' => $item->area, 'mall' => $item->mall, 'outlet' => $item->outlet, 'department' => $item->department]);
//                        $department = $department->id;
//                    }


//                    $employmentDetails['department_id'] = $department;
                    $employmentDetails['coordinator'] = $item->coordinator;
                    $employmentDetails['orientation'] = ($item->orientation_yn == 'Y' ? 'Y' : 'N');
                    $employmentDetails['trainee_start_date'] = (!empty($item->trainee_start_date) ? $item->trainee_start_date : '');
                    $employmentDetails['trainee_end_date'] = (!empty($item->trainee_end_date) ? $item->trainee_end_date : '');
                    $employmentDetails['hire_date'] = (!empty($item->hire_date) ? $item->hire_date : '');

                    if(!empty($item->province)) {
                        $province = Provinces::where('province', $item->province)->first();
                        if (!empty($province)) {
                            $employmentDetails['province_id'] = $province->id;

                            if(!empty($item->area)) {
                                $area = Areas::where(['area' => $item->area, 'province_id' => $province->id])->first();
                                if (!empty($area)) {
                                    $employmentDetails['area_id'] = $area->id;
                                }
                            }
                        }
                    }

                    if(!empty($item->mall)) {
                        $mall = Malls::where('mall', $item->mall)->first();
                        if (!empty($mall)) {
                            $employmentDetails['mall_id'] = $mall->id;
                        }
                    }

                    if(!empty($item->outlet)) {
                        $outlet = Outlets::where('outlet', $item->outlet)->first();
                        if (!empty($outlet)) {
                            $employmentDetails['outlet_id'] = $outlet->id;
                        }
                    }

                    if (!empty($item->coordinator)) {
                        $coordinator = User::where('name', $item->coordinator)->first();
                        if (!empty($coordinator)) {
                            $employmentDetails['coordinator_id'] = $coordinator->id;
                        }
                    }

                    $compensation = [
                        'daily_rate' => $item->daily_rate,
                        'hourly' => $item->hourly,
                        'monthly_rate' => $item->monthly_rate,
                        'account_number' => trim($item->account_number),
                        'rate_type' => trim(ucfirst(strtolower($item->rate_type))),
                        'daily_category' => trim(ucfirst(strtolower($item->daily_category))),
                        'mobile_number' => trim($item->gcash_mobile_number),
                        'sss' => $item->sss_10_numbers,
                        'compute_sss' => $item->compute_sss_yn == 'Y' ? 'Y' : 'N',
                        'hdmf' => $item->hdmf_12_numbers,
                        'compute_hdmf' => $item->compute_hdmf_yn == 'Y' ? 'Y' : 'N',
                        'philhealth' => $item->philhealth_12_numbers,
                        'compute_philhealth' => $item->compute_philhealth_yn == 'Y' ? 'Y' : 'N',
                        'tin' => $item->tin_12_numbers,
                        'deduct_withholding' => $item->deduct_withholding_yn == 'Y' ? 'Y' : 'N',
                    ];

                    if(empty($compensation['daily_rate']) && !empty($compensation['monthly_rate'])) {
                        $compensation['daily_rate'] = $compensation['monthly_rate'] / 26;
                    }

                    if(empty($compensation['monthly_rate']) && !empty($compensation['daily_rate'])) {
                        $compensation['monthly_rate'] = $compensation['daily_rate'] * 26;
                    }

                    if ($compensation['daily_category'] == 'Location' || $compensation['daily_category'] == 'Region') {
                        $dailyRate = DailyRates::where(['category' => strtoupper($item->daily_category), 'location' => strtoupper($item->location)])->first();
                        if (!empty($dailyRate)) {
                            $compensation['daily_rate_id'] = $dailyRate->id;
                        }
                    }

                    $transferMode = TransferModes::where(['transfer_mode' => $item->transfer_mode, 'transfer_type' => $item->transfer_type, 'bank' => $item->bank])->first();
                    if (!empty($transferMode)) {
                        $compensation['transfer_mode_id'] = $transferMode->id;
                    }

                    $taxType = TaxTypes::where('title', $item->tax_type)->first();
                    if (!empty($taxType)) {
                        $compensation['tax_type_id'] = $taxType->id;
                    }

                    $payrollGroup = PayrollGroups::where('group_name', $item->payroll_group)->first();
                    if (!empty($payrollGroup)) {
                        if (!empty($payrollGroup->Company->parent_company)) {
//                            $parentCompany = Companies::find($payrollGroup->Company->parent_company);
//                            $payrollGroup = PayrollGroups::where(['group_name' => $item->payroll_group, 'company_id' => $parentCompany->id])->first();
                            if (!empty($payrollGroup)) {
                                $compensation['payroll_group_id'] = $payrollGroup->id;
                                $compensation['client_id'] = $payrollGroup->Company->id;
                            }
                        } else {
                            $compensation['payroll_group_id'] = $payrollGroup->id;
                            $compensation['client_id'] = $payrollGroup->Company->id;
                        }
                    }

                    $compensation['user_id'] = $user->id;
//                    dd($employmentDetails);
                    Compensations::create($compensation);
                }

                $employmentDetails['user_id'] = $user->id;
                EmploymentDetails::create($employmentDetails);
            }
        });

    }

    public function uploadMembers()
    {
        $excel = App::make('excel');


        $excel->filter('chunk')->selectSheetsByIndex(0)->load('storage/app/members.xlsx')->chunk(250, function ($results) {
//            dd($results[0]);
            foreach ($results as $item) {

                $dateFields = ['birthday', 'hire_date', 'trainee_start_date', 'trainee_end_date'];

                foreach($dateFields as $field) {
                    if(!empty($item->$field) && substr_count('-', $item->$field) == 0) {
                        $item->$field = PHPExcel_Style_NumberFormat::toFormattedString($item->$field, 'YYYY-MM-DD');
                    }
                }

                $user = [
                    'series' => str_replace(['M', 'E'], '', trim($item->username)),
                    'email' => strtolower($item->email),
                    'username' => strtoupper($item->username),
                    'role' => (strtoupper($item->membership_category) == 'EMPLOYEE') ? "Employee" : "Member",
                ];


                $user['name'] = $name = $item->full_name;
//                $name = !empty($item->middle_name) ? $name . " {$item->middle_name}" : $name;
//                $user['name'] = !empty($item->last_name) ? $name . " {$item->last_name}" : $name;

                $user['password'] = bcrypt($user['username']);
//                dd($user);
                $user = User::create($user);


                $civilStatus = '';
                if (!empty($item->civil_status)) {
                    if (CivilStatus::where('title', strtoupper($item->civil_status))->count() == 0) {
                        $civilStatus = CivilStatus::create(['title' => strtoupper($item->civil_status)]);
                        $civilStatus = $civilStatus->id;
                    } else {
                        $civilStatus = CivilStatus::where('title', strtoupper($item->civil_status))->first()->id;
                    }
                }


                $educationalAttainment = '';
                if (!empty(!empty($item->educational_attainment))) {
                    if (EducationalAttainments::where('title', strtoupper($item->educational_attainment))->count() == 0) {
                        $educationalAttainment = EducationalAttainments::create(['title' => strtoupper($item->educational_attainment)]);
                        $educationalAttainment = $educationalAttainment->id;
                    } else {
                        $educationalAttainment = EducationalAttainments::where('title', strtoupper($item->educational_attainment))->first()->id;
                    }
                }


                $profile = [
                    'id_number' => $item->system_gtmc_number,
                    'first_name' => strtoupper($item->first_name),
                    'middle_name' => strtoupper($item->middle_name),
                    'last_name' => strtoupper($item->last_name),
                    'address' => strtoupper($item->address),
                    'email' => strtolower($item->email),
                    'mobile1' => $item->mobile_1,
                    'mobile2' => $item->mobile_2,
                    'gender' => $item->gender,
                    'mothers_maiden_name' => strtoupper($item->mothers_maiden_name),
                    'birthday' => !empty($item->birthday) ? $item->birthday : '',
                    'join_date' => !empty($item->hire_date) ? $item->hire_date : '',
                    'civil_status_id' => $civilStatus,
                    'educational_attainment_id' => $educationalAttainment,
                    'icoe_contact_name' => strtoupper($item->icoe_contact_name),
                    'icoe_contact_address' => strtoupper($item->icoe_contact_address),
                    'icoe_contact_mobile' => strtoupper($item->icoe_contact_number),
                    'membership_category' => ucfirst(strtolower($item->membership_category)),
                    'membership_type' => ucfirst(strtolower($item->membership_type)),
                    'status' => ucfirst(strtolower($item->status)),
                    'coop_share' => $item->contribution_per_payday,
                    'cost_center_id' => CostCenters::where('cost_center', $item->cost_center)->exists() ? CostCenters::where('cost_center', $item->cost_center)->first()->id : ''
                ];

                $profile['user_id'] = $user->id;

//                dd($profile);
                Profiles::create($profile);

                $principal = Companies::where('company_code', $item->principal)->first();
                if (!empty($principal)) {
                    $employmentDetails['principal_id'] = $principal->id;
                }


                $employmentDetails['coordinator'] = $item->coordinator;
                if ($profile['membership_category'] == 'Employee') {
                    $position = Positions::where('title', $item->position)->first();
                    if (!empty($position)) {
                        $employmentDetails['position_id'] = $position->id;
                    }

//                    $department = Departments::where(['province' => $item->province, 'area' => $item->area, 'mall' => $item->mall, 'outlet' => $item->outlet, 'department' => $item->department]);
//                    if ($department->count() > 0) {
//                        $department = $department->first()->id;
//                    } else {
//                        $department = Departments::create(['province' => $item->province, 'area' => $item->area, 'mall' => $item->mall, 'outlet' => $item->outlet, 'department' => $item->department]);
//                        $department = $department->id;
//                    }


//                    $employmentDetails['department_id'] = $department;
                    $employmentDetails['coordinator'] = $item->coordinator;
                    $employmentDetails['orientation'] = ($item->orientation_yn == 'Y' ? 'Y' : 'N');
                    $employmentDetails['trainee_start_date'] = (!empty($item->trainee_start_date) ? $item->trainee_start_date : '');
                    $employmentDetails['trainee_end_date'] = (!empty($item->trainee_end_date) ? $item->trainee_end_date : '');
                    $employmentDetails['hire_date'] = (!empty($item->hire_date) ? $item->hire_date : '');

                    if(!empty($item->province)) {
                        $province = Provinces::where('province', $item->province)->first();
                        if (!empty($province)) {
                            $employmentDetails['province_id'] = $province->id;

                            if(!empty($item->area)) {
                                $area = Areas::where(['area' => $item->area, 'province_id' => $province->id])->first();
                                if (!empty($area)) {
                                    $employmentDetails['area_id'] = $area->id;
                                }
                            }
                        }
                    }

                    if(!empty($item->mall)) {
                        $mall = Malls::where('mall', $item->mall)->first();
                        if (!empty($mall)) {
                            $employmentDetails['mall_id'] = $mall->id;
                        }
                    }

                    if(!empty($item->outlet)) {
                        $outlet = Outlets::where('outlet', $item->outlet)->first();
                        if (!empty($outlet)) {
                            $employmentDetails['outlet_id'] = $outlet->id;
                        }
                    }

                    if (!empty($item->coordinator)) {
                        $coordinator = User::where('name', $item->coordinator)->first();
                        if (!empty($coordinator)) {
                            $employmentDetails['coordinator_id'] = $coordinator->id;
                        }
                    }

                    $compensation = [
                        'daily_rate' => $item->daily_rate,
                        'hourly' => $item->hourly,
                        'monthly_rate' => $item->monthly_rate,
                        'account_number' => trim($item->account_number),
                        'rate_type' => trim(ucfirst(strtolower($item->rate_type))),
                        'daily_category' => trim(ucfirst(strtolower($item->daily_category))),
                        'mobile_number' => trim($item->gcash_mobile_number),
                        'sss' => $item->sss_10_numbers,
                        'compute_sss' => $item->compute_sss_yn == 'Y' ? 'Y' : 'N',
                        'hdmf' => $item->hdmf_12_numbers,
                        'compute_hdmf' => $item->compute_hdmf_yn == 'Y' ? 'Y' : 'N',
                        'philhealth' => $item->philhealth_12_numbers,
                        'compute_philhealth' => $item->compute_philhealth_yn == 'Y' ? 'Y' : 'N',
                        'tin' => $item->tin_12_numbers,
                        'deduct_withholding' => $item->deduct_withholding_yn == 'Y' ? 'Y' : 'N',
                    ];

                    if(empty($compensation['daily_rate']) && !empty($compensation['monthly_rate'])) {
                        $compensation['daily_rate'] = $compensation['monthly_rate'] / 26;
                    }

                    if(empty($compensation['monthly_rate']) && !empty($compensation['daily_rate'])) {
                        $compensation['monthly_rate'] = $compensation['daily_rate'] * 26;
                    }

                    if ($compensation['daily_category'] == 'Location' || $compensation['daily_category'] == 'Region') {
                        $dailyRate = DailyRates::where(['category' => strtoupper($item->daily_category), 'location' => strtoupper($item->location)])->first();
                        if (!empty($dailyRate)) {
                            $compensation['daily_rate_id'] = $dailyRate->id;
                        }
                    }

                    $transferMode = TransferModes::where(['transfer_mode' => $item->transfer_mode, 'transfer_type' => $item->transfer_type, 'bank' => $item->bank])->first();
                    if (!empty($transferMode)) {
                        $compensation['transfer_mode_id'] = $transferMode->id;
                    }

                    $taxType = TaxTypes::where('title', $item->tax_type)->first();
                    if (!empty($taxType)) {
                        $compensation['tax_type_id'] = $taxType->id;
                    }

//                    $payrollGroup = PayrollGroups::where('group_name', $item->payroll_group)->first();
//                    if (!empty($payrollGroup)) {
//                        if (!empty($payrollGroup->Company->parent_company)) {
////                            $parentCompany = Companies::find($payrollGroup->Company->parent_company);
////                            $payrollGroup = PayrollGroups::where(['group_name' => $item->payroll_group, 'company_id' => $parentCompany->id])->first();
//                            if (!empty($payrollGroup)) {
//                                $compensation['payroll_group_id'] = $payrollGroup->id;
//                                $compensation['client_id'] = $payrollGroup->Company->id;
//                            }
//                        } else {
//                            $compensation['payroll_group_id'] = $payrollGroup->id;
//                            $compensation['client_id'] = $payrollGroup->Company->id;
//                        }
//
//
//                    }

                    $compensation['user_id'] = $user->id;
//                    dd($employmentDetails);
                    Compensations::create($compensation);
                }

                $employmentDetails['user_id'] = $user->id;
                EmploymentDetails::create($employmentDetails);
            }
        });
    }

    public function uploadProvinces()
    {
        $excel = App::make('excel');

        $result = $excel->load('storage/app/LIST OF PROVINCE AND AREA.xlsx', function ($reader) {
            $reader->first();
            $reader->skipRows(0);
            $reader->calculate(false);
        })->get();

//        dd($result[0]);

//        foreach($result as $item) {
//
//            if(Provinces::where('province', $item->cityprovince)->count() == 0) {
//                Provinces::create(['province' => $item->cityprovince]);
//            }
//        }

        foreach ($result as $item) {

            $province = Provinces::where('province', $item->cityprovince)->first();
            if (Areas::where(['province_id' => $province->id, 'area' => $item->area])->count() == 0) {
                Areas::create(['province_id' => $province->id, 'area' => $item->area]);
            }
        }
    }

    public function uploadMalls()
    {
        $excel = App::make('excel');

        $result = $excel->load('storage/app/malls.xlsx', function ($reader) {
            $reader->first();
            $reader->skipRows(0);
            $reader->calculate(false);
        })->get();

//        dd($result[0]);

        foreach ($result as $item) {

            if (Malls::where('mall', $item->mall)->count() == 0) {
                Malls::create(['mall' => $item->mall]);
            }
        }
    }

    public function uploadOutlets()
    {
        $excel = App::make('excel');

        $result = $excel->load('storage/app/outlets.xlsx', function ($reader) {
            $reader->first();
            $reader->skipRows(0);
            $reader->calculate(false);
        })->get();

//        dd($result[0]);

        foreach ($result as $item) {

            if (Outlets::where('outlet', $item->outlet)->count() == 0) {
                Outlets::create(['outlet' => $item->outlet]);
            }
        }
    }
}

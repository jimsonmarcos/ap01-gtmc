<?php

namespace App\Http\Controllers\Client;

use App\Companies;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ClientInformationController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Client Information';
        $data['nav'] = 'dashboard';

        $data['user'] = $user = Auth::user();
        $data['client'] = Companies::find($user->company_id);
//        dd($data['user']);

        return view('client.client_information', $data);
    }
}


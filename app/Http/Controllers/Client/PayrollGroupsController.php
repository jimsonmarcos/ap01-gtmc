<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PayrollGroupsController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Payroll Groups';

        $data['user'] = Auth::user();

        return view('client.payroll_groups', $data);
    }

    public function add_payroll_groups()
    {
        $data['pageHeader'] = 'Add Payroll Groups';
        return view('client.payroll_groups_add', $data);
    }
}

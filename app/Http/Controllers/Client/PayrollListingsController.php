<?php

namespace App\Http\Controllers\Client;

use App\AuditLog;
use App\Companies;
use App\Compensations;
use App\EmploymentDetails;
use App\Profiles;
use App\TransferModes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PayrollListingsController extends Controller
{
    //
    public function index(Request $request)
    {
        $data['pageHeader'] = 'Payroll Listings';
        $data['nav'] = 'payroll_listings';

        $data['user'] = $user = Auth::user();
        $data['client'] = $client = Companies::find($user->company_id);

        $data['payrollGroupId'] = $payrollGroup = !empty($request->payroll_group_id) ? $request->payroll_group_id : 'All';

        $data['profiles'] = Profiles::select()
            ->join('employment_details as e', 'e.user_id', '=', 'profiles.user_id')
            ->join('compensations as c', 'c.user_id', '=', 'profiles.user_id')
            ->where('e.principal_id', $client->id);

        if($payrollGroup != 'All') {
            $data['profiles'] = $data['profiles']->where('c.payroll_group_id', $payrollGroup);
        }

        $data['profiles'] = $data['profiles']->get();

        return view('client.payroll_listing', $data);
    }

    public function edit($IDNumber)
    {
        $data['pageHeader'] = 'Payroll Listings';
        $data['nav'] = 'payroll_listings';

        $data['user'] = $user = Auth::user();
        $data['client'] = Companies::find($user->company_id);

        $data['profile'] = $profile = Profiles::where('id_number', $IDNumber)->first();

        if(empty($profile)) {
            return redirect()->back();
        }

        $data['transferModes'] = TransferModes::groupBy('transfer_mode')->orderBy('transfer_mode')->get();
        if(empty($profile->Compensation->TransferMode->transfer_mode)) {
            $data['transferTypes'] = TransferModes::where('transfer_mode', $data['transferModes']->first()->transfer_mode)->groupBy('transfer_type')->orderBy('transfer_type')->get();

        } else {
            $data['transferTypes'] = TransferModes::where('transfer_mode', $profile->Compensation->TransferMode->transfer_mode)->groupBy('transfer_type')->orderBy('transfer_type')->get();
        }
        if(empty($profile->Compensation->TransferMode->transfer_type)) {
            $data['banks'] = TransferModes::where('transfer_mode', $data['transferModes']->first()->transfer_mode)->where('transfer_type', $data['transferModes']->first()->transfer_type)->orderBy('bank')->get();
        } else {
            $data['banks'] = TransferModes::where('transfer_mode', $profile->Compensation->TransferMode->transfer_mode)->where('transfer_type', $profile->Compensation->TransferMode->transfer_type)->orderBy('bank')->get();
        }

        $data['coordinators'] = EmploymentDetails::where('position_id', 3)->get();

        return view('client.payroll_listing_edit', $data);
    }

    public function edit_post(Request $request, $IDNumber)
    {
        $data['pageHeader'] = 'Payroll Listings';

        $data['user'] = $user = Auth::user();
        $data['client'] = Companies::find($user->company_id);

        $data['profile'] = $profile = Profiles::where('id_number', $IDNumber)->first();

        if(!empty($request->coordinator_id)) {
            EmploymentDetails::where('user_id', $profile->user_id)->update(['coordinator_id' => $request->coordinator_id]);
        }

        $compensation = Compensations::where('user_id', $profile->user_id)->first();


        if(empty($profile)) {
            return redirect()->back();
        }


        if(!empty($request->transfer_mode_id)) {
            $compensation->transfer_mode_id = $request->transfer_mode_id;
        }

        $compensation->mobile_number = $request->mobile_number;

        $compensation->account_number = $request->account_number;


//        dd($compensation);

        $compensation->save();


        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'Client Admin',
            'action' => 'Create',
            'description' => "Updated Compensation of {$profile->id_number}",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', 'Payroll Details updated!');
        return redirect("/client/payroll-listings/edit/{$profile->id_number}");
    }
}





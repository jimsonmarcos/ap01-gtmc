<?php

namespace App\Http\Controllers\Client;

use App\Companies;
use App\LeaveHistory;
use App\Profiles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserProfilesController extends Controller
{
    //
    public function index($status = 'Active')
    {
        $data['pageHeader'] = 'User Profiles';
        $data['nav'] = 'user_profiles';
        $data['status'] = $status;

        $data['user'] = $user = Auth::user();
        $data['client'] = $client = Companies::find($user->company_id);

        $data['usersActive'] = Profiles::select(DB::raw('profiles.id, profiles.user_id, profiles.first_name, profiles.middle_name, profiles.last_name, CONCAT(profiles.first_name, " ", profiles.middle_name, " ", profiles.last_name) as name, profiles.id_number, membership_category, gender, c.company_code as principal'))
                            ->join('cost_centers as cc', 'cc.id', '=', 'profiles.cost_center_id')
                            ->join('companies as c', 'c.id', '=', 'cc.company_id', 'INNER')
                            ->where('c.id', $user->company_id)->where('profiles.status', 'Active')
                            ->count();

        $data['usersInactive'] = Profiles::select(DB::raw('profiles.id, profiles.user_id, profiles.first_name, profiles.middle_name, profiles.last_name, CONCAT(profiles.first_name, " ", profiles.middle_name, " ", profiles.last_name) as name, profiles.id_number, membership_category, gender, c.company_code as principal'))
                            ->join('cost_centers as cc', 'cc.id', '=', 'profiles.cost_center_id')
                            ->join('companies as c', 'c.id', '=', 'cc.company_id', 'INNER')
                            ->where('c.id', $user->company_id)->where('profiles.status', 'Deactivated')
                            ->count();

        $data['profiles'] = Profiles::select(DB::raw('profiles.id, profiles.user_id, profiles.first_name, profiles.middle_name, profiles.last_name, CONCAT(profiles.first_name, " ", profiles.middle_name, " ", profiles.last_name) as name, profiles.id_number, membership_category, gender, c.company_code as principal'))
                            ->join('cost_centers as cc', 'cc.id', '=', 'profiles.cost_center_id')
                            ->join('companies as c', 'c.id', '=', 'cc.company_id', 'INNER')
                            ->where('c.id', $user->company_id)->where('profiles.status', $status)
                            ->get();

        return view('client.user_profiles', $data);
    }

    public function profile($IDNumber)
    {
        $data['pageHeader'] = "User Profiles";
        $data['nav'] = 'user_profiles';

        $data['user'] = $user = Auth::user();
        $data['client'] = $client =Companies::find($user->company_id);


        $data['profile'] = $profile = Profiles::where('id_number', $IDNumber)->first();

        if(empty($profile)) {
            return redirect()->back();
        }

        $costCenters =  $client->CostCenters->pluck('id')->toArray();
        if(!in_array($profile->cost_center_id, $costCenters)) {
            return redirect()->back();
        }


        return view('client.user_profile', $data);
    }

    public function employment_details($IDNumber)
    {
        $data['pageHeader'] = "User Profiles";
        $data['nav'] = 'user_profiles';

        $data['user'] = $user = Auth::user();
        $data['client'] = $client =Companies::find($user->company_id);


        $data['profile'] = $profile = Profiles::where('id_number', $IDNumber)->first();

        if(empty($profile)) {
            return redirect()->back();
        }

        $costCenters =  $client->CostCenters->pluck('id')->toArray();
        if(!in_array($profile->cost_center_id, $costCenters)) {
            return redirect()->back();
        }


        return view('client.user_employment_details', $data);
    }

    public function compensation($IDNumber)
    {
        $data['pageHeader'] = "User Profiles";
        $data['nav'] = 'user_profiles';

        $data['user'] = $user = Auth::user();
        $data['client'] = $client =Companies::find($user->company_id);


        $data['profile'] = $profile = Profiles::where('id_number', $IDNumber)->first();

        if(empty($profile)) {
            return redirect()->back();
        }

        $costCenters =  $client->CostCenters->pluck('id')->toArray();
        if(!in_array($profile->cost_center_id, $costCenters)) {
            return redirect()->back();
        }

        $data['leaveHistory'] = LeaveHistory::select(['leave_history.*', 'payroll_cycles.payroll_date'])->where('leave_history.user_id', $profile->user_id)->where('effective_year', date('Y'))->join('payroll_cycles', 'payroll_cycles.id', '=', 'leave_history.payroll_cycle_id')->orderByDesc('payroll_cycles.payroll_date')->get();


        return view('client.user_compensation', $data);
    }

    public function coop($IDNumber)
    {
        $data['pageHeader'] = "User Profiles";
        $data['nav'] = 'user_profiles';

        $data['user'] = $user = Auth::user();
        $data['client'] = $client =Companies::find($user->company_id);


        $data['profile'] = $profile = Profiles::where('id_number', $IDNumber)->first();

        if(empty($profile)) {
            return redirect()->back();
        }

        $costCenters =  $client->CostCenters->pluck('id')->toArray();
        if(!in_array($profile->cost_center_id, $costCenters)) {
            return redirect()->back();
        }


        return view('client.user_coop', $data);
    }
}




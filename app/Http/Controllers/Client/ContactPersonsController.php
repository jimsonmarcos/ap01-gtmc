<?php

namespace App\Http\Controllers\Client;

use App\Companies;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ContactPersonsController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Contact Persons';
        $data['nav'] = 'contact_persons';

        $data['user'] = $user = Auth::user();
        $data['client'] = Companies::find($user->company_id);

        return view('client.contact_persons', $data);
    }
}


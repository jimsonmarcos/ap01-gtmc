<?php

namespace App\Http\Controllers\Client;

use App\Companies;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PayrollsController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Payrolls';
        $data['nav'] = 'payrolls';

        $data['user'] = $user = Auth::user();
        $data['client'] = Companies::find($user->company_id);

        return view('client.payrolls', $data);
    }
}





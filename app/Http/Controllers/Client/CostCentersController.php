<?php

namespace App\Http\Controllers\Client;

use App\Companies;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CostCentersController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Cost Centers';
        $data['nav'] = 'cost_center';

        $data['user'] = $user = Auth::user();
        $data['client'] = Companies::find($user->company_id);

        return view('client.cost_centers', $data);
    }
}




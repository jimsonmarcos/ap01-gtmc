<?php

namespace App\Http\Controllers\Client;

use App\Companies;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class InvoicesController extends Controller
{
    //
    public function index()
    {
        $data['pageHeader'] = 'Invoices';
        $data['nav'] = 'invoices';

        $data['user'] = $user = Auth::user();
        $data['client'] = Companies::find($user->company_id);

        return view('client.invoices', $data);
    }
}





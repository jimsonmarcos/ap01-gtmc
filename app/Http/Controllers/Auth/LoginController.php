<?php

namespace App\Http\Controllers\Auth;

use App\AuditLog;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function login(Request $request)
    {
        $username = $request->username;
        $password = $request->password;



        if (Auth::attempt(['client_username' => $username, 'password' => $password])) {
//            dd(Auth::user());
            $request->session()->put('is_client', TRUE);
            return redirect('/client/dashboard');
        }

        Auth::attempt(['username' => $username, 'password' => $password]);
//        dd(Auth::user());

//        if (in_array(Auth::user()->role, ['Admin', 'AdminPayroll', 'AdminCoop', 'AdminHR', 'AdminAcctg'])) {
//            dd(Auth::user());
//            $request->session()->put('is_admin', TRUE);
//            return redirect('/admin/dashboard');
//        }

//        dd(Auth::user());

//        Admin AdminPayroll AdminCoop AdminHR AdminAcctg
        if(Auth::check()) {
            $user = User::find(Auth::user()->id);
            AuditLog::create([
                'user_id' => $user->id,
                'category' => 'User',
                'action' => 'Login',
                'description' => ""
            ]);
        }

        return redirect('/authenticate');
    }

    use AuthenticatesUsers;

    public function username()
    {
        return 'username';
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/authenticate';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}

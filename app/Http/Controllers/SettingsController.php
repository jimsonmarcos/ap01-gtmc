<?php

namespace App\Http\Controllers;

use App\AuditLog;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    //
    public function change_password()
    {
        $data['pageHeader'] = '';
        $data['layout'] = 'layouts.app';

        $data['user'] = $user = Auth::user();
        $data['nav'] = 'settings';


        if(in_array(Auth::user()->role, ['Member', 'Employee'])) {
            $data['layout'] = 'layouts.users_app';
        }

        if(!empty(session()->get('is_client'))) {
            $data['layout'] = 'layouts.client_app';
        }

        if(!empty(session()->get('is_admin'))) {
            $data['layout'] = 'layouts.app';
        }



        return view('settings.change_password', $data);
    }

    public function change_password_post(Request $request)
    {
//        $old_password = $request->old_password;
        $new_password = $request->new_password;
        $confirm_password = $request->confirm_password;

        if($new_password != $confirm_password) {
            $request->session()->flash('popError', "New Password and Confirm Password does not match!");
            return redirect()->back();
        }

//        if(bcrypt($old_password) != User::find(Auth::user()->id)->password) {
//            $request->session()->flash('popError', "Invalid Old Password!");
//            return redirect()->back();
//        }

        $user = User::find(Auth::user()->id);
        $user->password = bcrypt($new_password);
        $user->save();

        AuditLog::create([
            'user_id' => Auth::user()->id,
            'category' => 'User Account',
            'action' => 'Change Password',
            'description' => "",
            'is_admin' => session('is_admin') ? 'Y' : 'N'
        ]);

        $request->session()->flash('popSuccess', "Password successfully updated!");
        return redirect()->back();
    }
}

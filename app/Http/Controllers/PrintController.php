<?php

namespace App\Http\Controllers;

use App\LoanPaymentSchedules;
use App\Loans;
use App\PayrollCycles;
use App\PayrollDtr;
use App\PayrollSummary;
use Illuminate\Http\Request;

class PrintController extends Controller
{
    public function payslip(Request $request, $payrollCycleId, $DtrId)
    {
        $data['payrollCycle'] = PayrollCycles::find($payrollCycleId);
        $data['dtr'] = $dtr = PayrollDtr::find($DtrId);
        $data['user'] = $data['dtr']->User;
        $data['payroll'] = PayrollSummary::where(['payroll_cycle_id' => $payrollCycleId, 'user_id' => $data['dtr']->User->id])->first();

        if ($dtr->User->Profile->Compensation->rate_type == 'Monthly') {
            $data['payrollComputation'] = $payroll = new \App\ComputePayroll($dtr, ['daily_rate' => $dtr->User->Profile->Compensation->monthly_rate / 26, 'monthly_rate' => $dtr->User->Profile->Compensation->monthly_rate / 2, 'rate_type' => 'Monthly']);
        } else {
            if ($dtr->User->Profile->Compensation->daily_category == 'Location') {
                $data['payrollComputation'] = $payroll = new \App\ComputePayroll($dtr, $dtr->User->Profile->Compensation->DailyRate->rate);
            } else {
                $data['payrollComputation'] = $payroll = new \App\ComputePayroll($dtr, $dtr->User->Profile->Compensation->daily_rate);
            }
        }

        $data['ytd'] = PayrollSummary::whereIn('payroll_cycle_id', PayrollCycles::where('status', 'Processed')->whereYear('payroll_date', date('Y', strtotime($data['payrollCycle']->payroll_date)))->pluck('id'))->where('user_id', $dtr->User->id)->get();
//        dd(PayrollCycles::where('status', 'Processed')->whereYear('payroll_date', date('Y', strtotime( $data['payrollCycle']->payroll_date)))->pluck('id'));

        $ytdPayrollCycles = PayrollCycles::where(['payroll_group_id' => $data['user']->Profile->Compensation->PayrollGroup->id, 'effective_year' => $data['payrollCycle']->effective_year])->pluck('id');
        $data['ytdPayrollSummary'] = PayrollSummary::whereIn('payroll_cycle_id', $ytdPayrollCycles)->get();


        $view = $request->segment(5) == 'detailed' ? 'payslip' : 'payslip_actual';

        return view('print.' . $view, $data);
    }

    public function payslips(Request $request, $payrollCycleId)
    {
        $data['payrollCycle'] = $payrollCycle = PayrollCycles::find($payrollCycleId);

        $data['id_numbers'] = [];
        if(!empty($request->get('data'))) {
            $json = json_decode($request->get('data'));
            foreach($json as $j) {
                $data['id_numbers'][] = $j->id_number;
            }
        }

        $data['dtr'] = $dtr = PayrollDtr::where('payroll_cycle_id', $payrollCycleId)->get();

        if($payrollCycle->cycle == 'B') {
            $payrollCode = substr($payrollCycle->payroll_code, 0, -1). 'A';
            if(!PayrollCycles::where('payroll_code', $payrollCode)->where('status', 'Processed')->exists()) {
                $request->session()->flash('popError', "Payroll Code <strong>{$payrollCode}</strong> must be processed first in order to process this payroll cycle!");
                return redirect()->back();
            }
        }

        if($payrollCycle->status == 'In Progress') {
            return view('print.payslips_inprogress', $data);
        } else {
            return view('print.payslips', $data);
        }
    }

    public function loan_amortization_schedule($loanId)
    {
        $data['loan'] = Loans::find($loanId);
        $data['paymentSchedules'] = LoanPaymentSchedules::where('loan_id', $loanId)->orderBy('month')->get();

        return view('print.loan_amortization_schedule', $data);
    }
}

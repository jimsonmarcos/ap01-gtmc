<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class Client
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @rturn mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check())
        {
            $user = Auth::user();
            $user = User::find($user->id);

            if (in_array($user->client_type, ['ClientAdmin', 'ClientUser']))
            {
                return $next($request);
            }
            else
            {
                return redirect("/");
            }
        }
        else
        {
//            return redirect("/");
        }
    }
}

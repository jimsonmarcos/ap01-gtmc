<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @rturn mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check())
        {
            $user = Auth::user();
            $user = User::find($user->id);

            if ($user->username == 'admin' || in_array($user->role, ['SuperAdmin', 'Admin', 'AdminPayroll', 'AdminCoop', 'AdminHR', 'AdminAcctg']))
            {
                return $next($request);
            }
            else
            {
                return redirect("/logout");
            }
        }
        else
        {
            return redirect("/");
        }
    }
}

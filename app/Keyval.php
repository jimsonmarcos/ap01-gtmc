<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keyval extends Model
{
    protected $table = "keyval";
    public $timestamps = false;
    protected $guarded = [];

}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanPaymentSchedules extends Model
{
    protected $table = "loan_payment_schedules";
    public $timestamps = false;

    protected $guarded = [];

    public function Loan()
    {
        return $this->belongsTo('App\Loans', 'loan_id', 'id');
    }
}
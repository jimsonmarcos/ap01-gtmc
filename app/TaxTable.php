<?php

namespace App;

class TaxTable
{
    public $taxType;
    public $value;
    public $incomeTx;
    public $percentage = 0;
    public $excemption = 0;

    function __construct($taxType, $incomeTx) {
        $this->taxType = $taxType;
        $this->incomeTx = $incomeTx;

        if($this->incomeTx >= 1 && $this->incomeTx <= 10416) {
            $this->value = 1;
        } elseif($this->incomeTx >= 10417 && $this->incomeTx <= 16666) {
            $this->percentage = .2;
            $this->value = 10417;
        } elseif($this->incomeTx >= 16667 && $this->incomeTx <= 33332) {
            $this->percentage = .25;
            $this->value = 16667;
            $this->excemption = 1250;
        } elseif($this->incomeTx >= 33333 && $this->incomeTx <= 83332) {
            $this->percentage = .3;
            $this->value = 33333;
            $this->excemption = 5416.67;
        } elseif($this->incomeTx >= 83333 && $this->incomeTx <= 333332) {
            $this->percentage = .32;
            $this->value = 83333;
            $this->excemption = 20416.67;
        } elseif($this->incomeTx >= 333333) {
            $this->percentage = .2;
            $this->value = 333333;
            $this->excemption = 100416.67;
        }

//        if(in_array($this->taxType, ['S', 'M'])) {
//            if($this->incomeTx >= 1 && $this->incomeTx <= 2082) {
//                $this->value = 1;
//            } elseif($this->incomeTx >= 2083 && $this->incomeTx <= 2499) {
//                $this->percentage = .05;
//                $this->value = 2083;
//            } elseif($this->incomeTx >= 2500 && $this->incomeTx <= 3332) {
//                $this->percentage = .1;
//                $this->excemption = 20.83;
//                $this->value = 2500;
//            } elseif($this->incomeTx >= 3333 && $this->incomeTx <= 4999) {
//                $this->percentage = .15;
//                $this->excemption = 104.17;
//                $this->value = 3333;
//            } elseif($this->incomeTx >= 5000 && $this->incomeTx <= 7916) {
//                $this->percentage = .20;
//                $this->excemption = 354.17;
//                $this->value = 5000;
//            } elseif($this->incomeTx >= 7917 && $this->incomeTx <= 12499) {
//                $this->percentage = .25;
//                $this->excemption = 937.5;
//                $this->value = 7917;
//            } elseif($this->incomeTx >= 12500 && $this->incomeTx <= 22916) {
//                $this->percentage = .30;
//                $this->excemption = 2083.33;
//                $this->value = 12500;
//            } elseif($this->incomeTx >= 22917) {
//                $this->percentage = .32;
//                $this->excemption = 5208.33;
//                $this->value = 22917;
//            }
//        } elseif(in_array($this->taxType, ['S1', 'M1'])) {
//            if($this->incomeTx >= 1 && $this->incomeTx <= 3124) {
//                $this->value = 1;
//            } elseif($this->incomeTx >= 3125 && $this->incomeTx <= 3541) {
//                $this->percentage = .05;
//                $this->value = 3125;
//            } elseif($this->incomeTx >= 3542 && $this->incomeTx <= 4374) {
//                $this->percentage = .1;
//                $this->excemption = 20.83;
//                $this->value = 3542;
//            } elseif($this->incomeTx >= 4375 && $this->incomeTx <= 6041) {
//                $this->percentage = .15;
//                $this->excemption = 104.17;
//                $this->value = 4375;
//            } elseif($this->incomeTx >= 6042 && $this->incomeTx <= 8957) {
//                $this->percentage = .20;
//                $this->excemption = 354.17;
//                $this->value = 6042;
//            } elseif($this->incomeTx >= 8958 && $this->incomeTx <= 13541) {
//                $this->percentage = .25;
//                $this->excemption = 937.5;
//                $this->value = 8958;
//            } elseif($this->incomeTx >= 13542 && $this->incomeTx <= 23957) {
//                $this->percentage = .30;
//                $this->excemption = 2083.33;
//                $this->value = 13542;
//            } elseif($this->incomeTx >= 23958) {
//                $this->percentage = .32;
//                $this->excemption = 5208.33;
//                $this->value = 23958;
//            }
//        } elseif(in_array($this->taxType, ['S2', 'M2'])) {
//            if($this->incomeTx >= 1 && $this->incomeTx <= 4166) {
//                $this->value = 1;
//            } elseif($this->incomeTx >= 4167 && $this->incomeTx <= 4582) {
//                $this->percentage = .05;
//                $this->value = 4167;
//            } elseif($this->incomeTx >= 4583 && $this->incomeTx <= 5416) {
//                $this->percentage = .1;
//                $this->excemption = 20.83;
//                $this->value = 4583;
//            } elseif($this->incomeTx >= 5417 && $this->incomeTx <= 7082) {
//                $this->percentage = .15;
//                $this->excemption = 104.17;
//                $this->value = 5417;
//            } elseif($this->incomeTx >= 7083 && $this->incomeTx <= 9999) {
//                $this->percentage = .20;
//                $this->excemption = 354.17;
//                $this->value = 7083;
//            } elseif($this->incomeTx >= 10000 && $this->incomeTx <= 14582) {
//                $this->percentage = .25;
//                $this->excemption = 937.5;
//                $this->value = 10000;
//            } elseif($this->incomeTx >= 14583 && $this->incomeTx <= 24999) {
//                $this->percentage = .30;
//                $this->excemption = 2083.33;
//                $this->value = 14583;
//            } elseif($this->incomeTx >= 25000) {
//                $this->percentage = .32;
//                $this->excemption = 5208.33;
//                $this->value = 25000;
//            }
//        } elseif(in_array($this->taxType, ['S3', 'M3'])) {
//            if($this->incomeTx >= 1 && $this->incomeTx <= 5207) {
//                $this->value = 1;
//            } elseif($this->incomeTx >= 5028 && $this->incomeTx <= 5624) {
//                $this->percentage = .05;
//                $this->value = 5028;
//            } elseif($this->incomeTx >= 5625 && $this->incomeTx <= 6457) {
//                $this->percentage = .1;
//                $this->excemption = 20.83;
//                $this->value = 5625;
//            } elseif($this->incomeTx >= 6458 && $this->incomeTx <= 8124) {
//                $this->percentage = .15;
//                $this->excemption = 104.17;
//                $this->value = 6458;
//            } elseif($this->incomeTx >= 8125 && $this->incomeTx <= 11041) {
//                $this->percentage = .20;
//                $this->excemption = 354.17;
//                $this->value = 8125;
//            } elseif($this->incomeTx >= 11042 && $this->incomeTx <= 15624) {
//                $this->percentage = .25;
//                $this->excemption = 937.5;
//                $this->value = 11042;
//            } elseif($this->incomeTx >= 15625 && $this->incomeTx <= 26041) {
//                $this->percentage = .30;
//                $this->excemption = 2083.33;
//                $this->value = 15625;
//            } elseif($this->incomeTx >= 26042) {
//                $this->percentage = .32;
//                $this->excemption = 5208.33;
//                $this->value = 26042;
//            }
//        } elseif(in_array($this->taxType, ['S4', 'M4'])) {
//            if($this->incomeTx >= 1 && $this->incomeTx <= 6249) {
//                $this->value = 1;
//            } elseif($this->incomeTx >= 6250 && $this->incomeTx <= 6666) {
//                $this->percentage = .05;
//                $this->value = 6250;
//            } elseif($this->incomeTx >= 6667 && $this->incomeTx <= 7499) {
//                $this->percentage = .1;
//                $this->excemption = 20.83;
//                $this->value = 6667;
//            } elseif($this->incomeTx >= 7500 && $this->incomeTx <= 9166) {
//                $this->percentage = .15;
//                $this->excemption = 104.17;
//                $this->value = 7500;
//            } elseif($this->incomeTx >= 9167 && $this->incomeTx <= 12082) {
//                $this->percentage = .20;
//                $this->excemption = 354.17;
//                $this->value = 9167;
//            } elseif($this->incomeTx >= 12083 && $this->incomeTx <= 16666) {
//                $this->percentage = .25;
//                $this->excemption = 937.5;
//                $this->value = 11042;
//            } elseif($this->incomeTx >= 16667 && $this->incomeTx <= 27082) {
//                $this->percentage = .30;
//                $this->excemption = 2083.33;
//                $this->value = 16667;
//            } elseif($this->incomeTx >= 27083) {
//                $this->percentage = .32;
//                $this->excemption = 5208.33;
//                $this->value = 27083;
//            }
//        }
    }

    public function excess()
    {
        return ($this->incomeTx - $this->value) * $this->percentage;
    }

    public function taxDue()
    {
        return $this->excemption + $this->excess();
    }
}
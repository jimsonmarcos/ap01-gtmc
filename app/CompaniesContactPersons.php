<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompaniesContactPersons extends Model
{
    protected $table = "companies_contact_persons";
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'position_id',
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'mobile1',
        'mobile2',
        'office_number',
        'local_number'
    ];

    public function Company() {
        return $this->belongsTo('App\Companies', 'id', 'company_id');
    }

    public function Position() {
        return $this->belongsTo('App\Positions');
    }

    public function name()
    {
        $name = $this->first_name;
        $name = !empty($this->middle_name) ? $name .' '. substr($this->middle_name, 0, 1).'.' : $name;
        $name = $name. " {$this->last_name}";
        return $name;
//        return "{$this->first_name} ". substr($this->middle_name, 0, 1) .". {$this->last_name}";
    }

    public function namelfm()
    {
//        $name = trim($this->first_name);
//        $name = !empty($this->middle_name) && strlen(trim($this->middle_name)) > 0 ? $name. " " .substr(trim($this->middle_name), 0, 1)."." : $name;
//        $name = !empty($this->last_name) && strlen(trim($this->last_name)) > 0 ? $name. " " .trim($this->last_name) : $name;

        $name = trim($this->last_name);
        $name = !empty($this->first_name) && strlen(trim($this->first_name)) > 0 ? $name. ", " .trim($this->first_name) : $name;
        $name = !empty($this->middle_name) && strlen(trim($this->middle_name)) > 0 ? $name. " " .trim($this->middle_name) : $name;
        return $name;
    }

    public function namelf()
    {
//        $name = trim($this->first_name);
//        $name = !empty($this->middle_name) && strlen(trim($this->middle_name)) > 0 ? $name. " " .substr(trim($this->middle_name), 0, 1)."." : $name;
//        $name = !empty($this->last_name) && strlen(trim($this->last_name)) > 0 ? $name. " " .trim($this->last_name) : $name;

        $name = trim($this->last_name);
        $name = !empty($this->first_name) && strlen(trim($this->first_name)) > 0 ? $name. ", " .trim($this->first_name) : $name;
        return $name;
    }

    public function complete_name()
    {
        return "{$this->first_name} {$this->middle_name} {$this->last_name}";
    }
}

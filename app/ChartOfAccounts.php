<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChartOfAccounts extends Model
{
    protected $table = "chart_of_accounts";
    public $timestamps = false;

    protected $fillable = [
        'id',
        'account_type',
        'account_title'
    ];

    public function AccountTitles() {
        return $this->hasMany('App\ChartOfAccounts', 'account_type', 'account_type');
    }
}

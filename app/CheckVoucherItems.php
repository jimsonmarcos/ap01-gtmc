<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckVoucherItems extends Model
{
    protected $table = "check_voucher_items";
    public $timestamps = false;

    protected $fillable = [
        'id',
        'check_voucher_id',
        'chart_of_account_id',
        'txn',
        'debit',
        'credit'
    ];

    public function ChartOfAccount()
    {
        return $this->belongsTo('App\ChartOfAccounts', 'chart_of_account_id');
    }
}

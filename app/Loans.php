<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loans extends Model
{
    protected $table = "loans";

    protected $guarded = [];

//    protected $fillable = [
//        'user_id',
//        'cla_number',
//        'cla_series',
//        'date_request',
//        'check_voucher_id',
//        'loan_type_id',
//        'request_type',
//        'months_to_pay',
//        'request_amount',
//        'loan_type',
//        'interest',
//        'overall_interest',
//        'granted_loan_amount',
//        'amortization',
//        'service_charge',
//        'savings',
//        'additional_deduction',
//        'beginning_balance',
//        'status',
//        'releasing_status',
//        'is_voucher',
//        'effective_date',
//        'remarks',
//        'payment_method',
//        'released_date',
//        'transfer_mode_id',
//        'reference_number',
//        'disbursed_remarks',
//    ];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function LoanType()
    {
        return $this->belongsTo('App\LoanTypes', 'loan_type_id', 'id');
    }

    public function CheckVoucher()
    {
        return $this->belongsTo('App\CheckVouchers', 'check_voucher_id', 'id');
    }

    public function Payments()
    {
        return $this->hasMany('App\LoanPayments', 'loan_id');
    }
}
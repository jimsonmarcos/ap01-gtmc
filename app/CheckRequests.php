<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckRequests extends Model
{
    protected $table = "check_requests";

    protected $fillable = [
        'series',
        'crf_number',
        'payee_type',
        'payee_id',
        'payee_name',
        'payee_address',
        'request_date',
        'particulars',
        'requested_by',
        'noted_by',
        'approved_by',
        'status',
        'total_amount',
        'created_at',
        'updated_at'
    ];

    public function User() {
        return $this->belongsTo('App\User', 'payee_id');
    }

    public function Items() {
        return $this->hasMany('App\CheckRequestItems', 'check_request_id', 'id');
    }

    public function Payee() {
        return $this->belongsTo('App\User', 'payee_id');
    }

    public function RequestedBy() {
        return $this->belongsTo('App\User', 'requested_by');
    }

    public function NotedBy() {
        return $this->belongsTo('App\User', 'noted_by');
    }

    public function ApprovedBy() {
        return $this->belongsTo('App\User', 'approved_by');
    }
}

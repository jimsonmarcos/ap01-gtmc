<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferModes extends Model
{
    protected $table = "transfer_modes";
    public $timestamps = false;

    protected $fillable = [
        'transfer_mode',
        'transfer_type',
        'bank',
        'bank_code',
        'bank_full_name'
    ];
}
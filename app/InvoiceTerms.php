<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceTerms extends Model
{
    protected $table = "invoice_terms";
    public $timestamps = false;

    protected $fillable = [
        'terms',
        'days'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollCycles extends Model
{
    protected $table = "payroll_cycles";

    protected $fillable = [
        'user_id',
        'payroll_code',
        'effective_year',
        'effective_month',
        'cycle',
        'payroll_group_id',
        'start_date',
        'end_date',
        'payroll_period',
        'calculate_13th_month',
        'calculate_leave_encashment',
        'payroll_date'
    ];

    public function DTR()
    {
        return $this->hasMany('App\PayrollDtr', 'payroll_cycle_id', 'id');
    }

    public function Summary()
    {
        return $this->hasMany('App\PayrollSummary', 'payroll_cycle_id', 'id');
    }

    public function PayrollGroup()
    {
        return $this->belongsTo('App\PayrollGroups', 'payroll_group_id', 'id');
    }
}

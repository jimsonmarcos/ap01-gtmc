<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessTaxTypes extends Model
{
    protected $table = "business_tax_types";
    public $timestamps = false;

    protected $fillable = [
        'tax',
        'percent'
    ];
}

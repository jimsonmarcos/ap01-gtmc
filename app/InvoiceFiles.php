<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceFiles extends Model
{
    protected $table = "invoice_files";
    public $timestamps = false;

    protected $fillable = [
        'invoice_id',
        'file_name',
        'file_path'
    ];

    public function Invoice()
    {
        return $this->belongsTo('App\Invoices', 'invoice_id');
    }
}

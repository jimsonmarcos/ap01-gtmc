<?php

namespace App;

use App\Mail\NewClientNotificationMail;
use Illuminate\Database\Eloquent\Model;
use App\PayrollCycles;
use Illuminate\Support\Facades\Mail;

class Companies extends Model
{
    protected $table = "companies";
    public $timestamps = false;

    protected $fillable = [
        'client_id',
        'company_code',
        'company_name',
        'parent_company_id',
        'series',
        'address',
        'trunk_line',
        'fax',
        'sss',
        'hdmf',
        'philhealth',
        'tin',
        'admin_fee_type',
        'admin_fee_percentage',
        'admin_fee',
        'admin_breakdown',
        'status',
        'client_start_date'
    ];

    public function ContactPersons()
    {
        return $this->hasMany('App\CompaniesContactPersons','company_id');
    }

    public function PayrollGroups() {
        return $this->hasMany('App\PayrollGroups', 'company_id');
    }

    public function CostCenters() {
        return $this->hasMany('App\CostCenters', 'company_id');
    }

    public function ParentCompany() {
        return $this->belongsTo('App\Companies', 'parent_company_id', 'id');
    }

    public function Employees() {
        return $this->hasMany('App\EmploymentDetails', 'principal_id');
    }

    public function Invoices() {
        return $this->hasMany('App\Invoices', 'company_id');
    }

    public function PayrollCycles() {
//        return $this->hasMany('App\EmploymentDetails', 'principal_id');
        $payrollGroups = $this->PayrollGroups();

//        return $payrollGroups->count();
        if($payrollGroups->count() > 0) {
//            return $payrollGroups->pluck('id');
//            if(!is_array($payrollGroups->pluck('id'))) return $payrollGroups->pluck('id');
            $payrollCycles = PayrollCycles::whereIn('payroll_group_id', $payrollGroups->pluck('id'))->where('status', 'Processed');
            return $payrollCycles->orderByDesc('payroll_code')->get();
        }


        return json_encode([]);
    }

    public function UserAccounts() {
        $userAccounts = User::where('company_id', $this->id)->whereIn('admin_type', ['ClientAdmin', 'ClientUser'])->get();
        return $userAccounts;
    }

    public static function boot() {
        parent::boot();

        static::created(function($company) {
            Mail::send(
                new NewClientNotificationMail($company)
            );
        });

//        static::updated(function($company) {
//
//        });
//
//        static::deleted(function($company) {
//
//        });
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyRates extends Model
{
    protected $table = "daily_rates";
    public $timestamps = false;

    protected $fillable = [
        'category',
        'location',
        'rate',
        'ecola'
    ];
}
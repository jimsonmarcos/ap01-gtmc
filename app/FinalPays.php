<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinalPays extends Model
{
    protected $table = "final_pays";

    protected $guarded = [];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function CreatedBy()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function ApprovedBy()
    {
        return $this->belongsTo('App\Signatories', 'approved_by', 'user_id');
    }

    public function NotedBy()
    {
        return $this->belongsTo('App\Signatories', 'noted_by', 'user_id');
    }

    public function Profile()
    {
        return $this->belongsTo('App\Profiles', 'user_id', 'user_id');
    }

    public function Dtr()
    {
        return $this->belongsTo('App\PayrollDtr', 'payroll_dtr_id', 'id');
    }

    public function PayrollCycle()
    {
        return $this->belongsTo('App\PayrollCycles', 'payroll_cycle_id' );
    }

    public function Summaries()
    {
        return $this->hasMany('App\FinalPay13thSummary', 'final_pay_id' );
    }

    public function ExistingLoans()
    {
        return $this->hasMany('App\FinalPayExistingLoans', 'final_pay_id' );
    }

    public function OtherCharges()
    {
        return $this->hasMany('App\FinalPayOtherCharges', 'final_pay_id' );
    }

}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinces extends Model
{
    protected $table = "provinces";
    public $timestamps = false;

    protected $fillable = [
        'province'
    ];

    public function Areas()
    {
        return $this->hasMany('App\Areas', 'province_id');
    }
}
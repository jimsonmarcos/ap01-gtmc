<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSeries extends Model
{
    protected $table = "user_series";
    public $timestamps = false;

    protected $fillable = [
        'series'
    ];
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberRelationships extends Model
{
    protected $table = "member_relationships";
    public $timestamps = false;

    protected $fillable = [
        'title'
    ];
}
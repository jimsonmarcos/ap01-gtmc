<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinalPay13thSummary extends Model
{
    protected $table = "final_pay_13th_summary";
    public $timestamps = false;

    protected $guarded = [];
}

<?php

namespace App;

use App\Mail\NewUserAdmin;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Mail;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'series', 'name', 'email', 'password', 'username', 'is_new', 'admin_type', 'admin_username', 'role', 'company_id', 'parent_id', 'first_name', 'middle_name', 'last_name',
        'contact_person_id', 'client_type', 'client_username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Profile()
    {
//        return $this->hasOne('App\Profiles');
        return $this->belongsTo('App\Profiles', 'id', 'user_id');
    }

    public function name()
    {
        $profile = Profiles::where('user_id', $this->id)->first();
        $name = $profile->first_name;
        $name = (!empty($profile->middle_name)) ? "{$name} ". substr($profile->middle_name, 0, 1) ."." : $name;
        $name = (!empty($profile->last_name)) ? "{$name} {$profile->last_name}" : $name;
        return $name;
    }

    public function Client()
    {
        return $this->belongsTo('App\Clients', 'id', 'user_id');
    }

    public function Company()
    {
        return $this->belongsTo('App\Companies', 'company_id', 'id');
    }

    public function ContactPerson()
    {
        return $this->belongsTo('App\CompaniesContactPersons', 'contact_person_id', 'id');
    }

    public static function boot() {
        parent::boot();

        static::created(function($user) {
            if(in_array($user->role, ['Admin', 'AdminPayroll', 'AdminCoop', 'AdminHR', 'AdminAcctg']))
            Mail::send(
                new NewUserAdmin($user)
            );
        });

//        static::updated(function($company) {
//
//        });
//
//        static::deleted(function($company) {
//
//        });
    }
}

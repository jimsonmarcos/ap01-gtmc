<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollSummary extends Model
{
    protected $table = "payroll_summary";
    public $timestamps = false;

    protected $guarded = [];

//    protected $fillable = [
//        'payroll_cycle_id',
//        'user_id',
//        'basic_pay',
//        'ecola',
//        'overtime_pay',
//        'holiday_pay',
//        'night_differential',
//        'holiday_ecola',
//        'adjustment_wage',
//        'adjustment_double_salary',
//        'adjustment_negative_salary',
//        'de_minimis',
//        'thirteenth_month_bonus',
//        'fixed_allowance',
//        'leave_encashment',
//        'incentives',
//        'coop_savings',
//        'sss_ec',
//        'sss_er',
//        'hdmf_er',
//        'philhealth_er',
//        'sss_ee',
//        'hdmf_ee',
//        'philhealth_ee',
//        'tardiness',
//        'absences',
//        'sss_loan',
//        'hdmf_loan',
//        'client_charges',
//        'membership_fee',
//        'notarial_fee',
//        'coop_loan',
//        'coop_share',
//        'gross_amount',
//        'deductions',
//        'tax',
//        'net_pay',
//        'billing_net_pay',
//        'admin_fee',
//        'total_billing'
//    ];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function Dtr()
    {
        return $this->belongsTo('App\PayrollDtr', 'payroll_dtr_id', 'id');
    }

    public function Profile()
    {
        return $this->belongsTo('App\Profiles', 'user_id', 'user_id');
    }

    public function Others()
    {
        return PayrollOthers::where(['payroll_cycle_id' => $this->payroll_cycle_id, 'user_id' => $this->user_id])->get();
//        return $this->hasMany('App\PayrollOthers', 'payroll_summary_id');
    }
}

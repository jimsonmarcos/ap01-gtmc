<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckVouchers extends Model
{
    protected $table = "check_vouchers";

    protected $fillable = [
        'id',
        'user_id',
        'series',
        'cv_number',
        'payee_name',
        'payee_address',
        'check_date',
        'check_number',
        'particulars',
        'amount',
        'chart_of_account_id',
        'txn',
        'debit',
        'credit',
        'is_updated',
        'prepared_by',
        'approved_by'
    ];

    public function Items()
    {
        return $this->hasMany('App\CheckVoucherItems','check_voucher_id');
    }

    public function PreparedBy() {
        return $this->belongsTo('App\User', 'prepared_by');
    }

    public function ApprovedBy() {
        return $this->belongsTo('App\User', 'approved_by');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashFlowParticulars extends Model
{
    protected $table = "cash_flow_particulars";
    public $timestamps = false;

    protected $fillable = [
        'id',
        'cash_flow_id',
        'cash_flow_item_id',
        'description',
        'amount'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxTypes extends Model
{
    protected $table = "tax_types";
    public $timestamps = false;

    protected $fillable = [
        'title'
    ];
}
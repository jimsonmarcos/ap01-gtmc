<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmploymentDetails extends Model
{
    protected $table = "employment_details";
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'principal_id',
        'position_id',
        'department_id',
        'province_id',
        'area_id',
        'mall_id',
        'outlet_id',
        'coordinator_id',
        'hire_date',
        'orientation',
        'trainee_start_date',
        'trainee_end_date',
        'coordinator',
        'effective_date',
        'reason',
        'resignation_date',
        'remarks',
    ];

//    protected $hidden = [
//        'id',
//        'user_id'
//    ];

    public function Position()
    {
        return $this->belongsTo('App\Positions', 'position_id', 'id');
    }

    public function Principal()
    {
        return $this->belongsTo('App\Companies', 'principal_id');
    }

    public function Coordinator()
    {
        return $this->belongsTo('App\Profiles', 'coordinator', 'id_number');
    }

    public function Department()
    {
        return $this->belongsTo('App\Departments', 'department_id');
    }

    public function Province()
    {
        return $this->belongsTo('App\Provinces', 'province_id');
    }

    public function Area()
    {
        return $this->belongsTo('App\Areas', 'area_id');
    }

    public function Mall()
    {
        return $this->belongsTo('App\Malls', 'mall_id');
    }

    public function Outlet()
    {
        return $this->belongsTo('App\Outlets', 'outlet_id');
    }

    public function Profile()
    {
        return $this->belongsTo('App\Profiles', 'user_id', 'user_id');
    }
}
<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Beneficiaries extends Model
{
    protected $table = "beneficiaries";
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'member_relationship_id',
        'first_name',
        'middle_name',
        'last_name',
        'birthday'
    ];

//    protected $hidden = [
//        'id',
//        'user_id'
//    ];

    public function birthdate()
    {
        return Carbon::createFromFormat('Y-m-d', $this->birthday)->format('m/d/Y');
    }

    public function MemberRelationship()
    {
        return $this->belongsTo('App\MemberRelationships');
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashReceiptItems extends Model
{
    protected $table = "cash_receipt_items";

    public $timestamps = false;

    protected $guarded = [];
}

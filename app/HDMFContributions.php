<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HDMFContributions extends Model
{
    protected $table = "hdmf_contributions";
    public $timestamps = false;

    protected $fillable = [
        'salary_range_start',
        'salary_range_end',
        'salary_base',
        'total_monthly_premium',
        'employee_share',
        'employer_share'
    ];
}

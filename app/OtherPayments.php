<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherPayments extends Model
{
    protected $table = "other_payments";
//    public $timestamps = false;

    public $guarded = [];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function Profile()
    {
        return $this->belongsTo('App\Profiles', 'user_id', 'user_id');
    }

    public function Bank()
    {
        return $this->belongsTo('App\banks', 'bank_id');
    }
}
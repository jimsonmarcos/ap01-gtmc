<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JournalVoucherItems extends Model
{
    protected $table = "journal_voucher_items";

    public $timestamps = false;

    protected $guarded = [];
}

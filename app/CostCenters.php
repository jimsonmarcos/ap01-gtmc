<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostCenters extends Model
{
    protected $table = "cost_centers";
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'cost_center',
        'group_definition'
    ];

    public function Company() {
        return $this->belongsTo('App\Companies', 'company_id', 'id');
    }

    public function Members() {
        return $this->hasMany('App\Profiles', 'cost_center_id' );
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Malls extends Model
{
    protected $table = "malls";
    public $timestamps = false;

    protected $fillable = [
        'mall'
    ];
}
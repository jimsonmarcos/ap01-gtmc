<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashFlowItems extends Model
{
    protected $table = "cash_flow_items";
    public $timestamps = false;

    protected $fillable = [
        'id',
        'payee_type',
        'item',
        'status',
        'created_by'
    ];
}

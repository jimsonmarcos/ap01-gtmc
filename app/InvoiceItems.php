<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItems extends Model
{
    protected $table = "invoice_items";
    public $timestamps = false;

//    protected $fillable = [
//        'invoice_id',
//        'invoice_item_id'
//    ];

    protected $guarded = [];

    public function Invoice()
    {
        return $this->belongsTo('App\Invoices', 'invoice_id');
    }

    public function BusinessTaxType()
    {
        return $this->belongsTo('App\BusinessTaxTypes', 'business_tax_type_id');
    }
}

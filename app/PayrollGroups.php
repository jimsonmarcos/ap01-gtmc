<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollGroups extends Model
{
    protected $table = "payroll_groups";
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'group_name',
        'start_date_a',
        'payroll_date_a',
        'start_date_b',
        'payroll_date_b',
        'group_definition'
    ];

    public function Company()
    {
        return $this->belongsTo('App\Companies', 'company_id', 'id');
    }

    public function PayrollCycles()
    {
        return $this->hasMany('App\PayrollCycles', 'payroll_group_id');
    }

    public function Members()
    {
        return $this->hasMany('App\Compensations', 'payroll_group_id');
    }
}

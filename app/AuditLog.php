<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditLog extends Model
{
    protected $table = "audit_log";
    public $timestamps = false;

    protected $guarded = [];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'user_id');
    }
}
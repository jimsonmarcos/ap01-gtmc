<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanPayments extends Model
{
    protected $table = "loan_payments";
    public $timestamps = false;

    public $guarded = [];

//    protected $fillable = [
//        'loan_id',
//        'transaction_id',
//        'series',
//        'ar_number',
//        'loan_installment_code',
//        'payment_method',
//        'payment_date',
//        'amount_paid',
//        'bank_id',
//        'reference_number',
//        'pay_period',
//        'remarks',
//        'date_created',
//    ];

    public function Loan()
    {
        return $this->belongsTo('App\Loans', 'id', 'loan_id');
    }

    public function Banks()
    {
        return $this->belongsTo('App\banks', 'id', 'bank_id');
    }
}
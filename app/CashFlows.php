<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashFlows extends Model
{
    protected $table = "cash_flows";

    protected $guarded = [];

//    protected $fillable = [
//        'id',
//        'user_id',
//        'check_request_id',
//        'series',
//        'cf_number',
//        'entry_date',
//        'check_date',
//        'check_number',
//        'status',
//        'mode',
//        'payee_name',
//        'payee_address',
//        'particulars',
//        'amount',
//        'bank_id',
//        'reference_number',
//        'cv_number',
//        'check_voucher_number_id',
//        'chart_of_account_id',
//        'remarks'
//    ];

    public function Items()
    {
        return $this->hasMany('App\CashFlowItemList','cash_flow_id');
    }

    public function Entries()
    {
        return $this->hasMany('App\CashFlowEntries','cash_flow_id');
    }

    public function User() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function PreparedBy() {
        return $this->belongsTo('App\User', 'prepared_by');
    }

    public function ApprovedBy() {
        return $this->belongsTo('App\User', 'approved_by');
    }
}

<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PayrollDtr extends Model
{
    protected $table = "payroll_dtr";

    protected $guarded = [];

//    protected $fillable = [
//        'draft_id',
//        'user_id',
//        'payroll_cycle_id',
//        'id_number',
//        'employee_name',
//        'source',
//        'hold_status',
//        'wh',
//        'rds',
//        'spe',
//        'sperd',
//        'leg',
//        'legrd',
//        'reg_ot',
//        'nd',
//        'abs',
//        'lt',
//        'ut',
//        'vl',
//        'sl',
//        'el',
//        'spl',
//        'ml',
//        'pl',
//        'pto',
//        'bl',
//        'cl',
//        'nwage',
//        'incentive',
//        'adji',
//        'adjd',
//        'client_charges'
//    ];

    public function Draft()
    {
        return $this->belongsTo('App\PayrollDtrDraft', 'draft_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function Profile()
    {
        return $this->belongsTo('App\Profiles', 'user_id', 'user_id');
    }

    public function Summary()
    {
        $summary = PayrollSummary::where(['user_id' => $this->user_id, 'payroll_cycle_id' => $this->payroll_cycle_id])->first();
        return $summary;
    }

    public function PayrollCycle()
    {
        return $this->belongsTo('App\PayrollCycles', 'payroll_cycle_id' );
    }

    public function Others()
    {
        return PayrollOthers::where(['payroll_cycle_id' => $this->payroll_cycle_id, 'user_id' => $this->user_id])->get();
//        return $this->hasMany('App\PayrollOthers', 'payroll_summary_id');
    }

    public function CalculatePayslip($d)
    {
        $user = $d->User;
        $payrollCycle = $d->PayrollCycle;
        $payrollCode = explode('-', $payrollCycle->payroll_code);
        $user_id = $user->id;
        //                                $payroll = \App\PayrollSummary::where(['payroll_cycle_id' => $d->payroll_cycle_id, 'user_id' => $d->User->id])->first();

        if($d->User->Profile->Compensation->rate_type == 'Monthly') {
            $payrollComputation = $payroll = new \App\ComputePayroll($d, ['daily_rate' => $d->User->Profile->Compensation->monthly_rate / 26, 'monthly_rate' => $d->User->Profile->Compensation->monthly_rate / 2, 'rate_type' => 'Monthly']);
        } else {
            if($d->User->Profile->Compensation->daily_category == 'Location') {
                $payrollComputation = $payroll = new \App\ComputePayroll($d, $d->User->Profile->Compensation->DailyRate->rate);
            } else {
                $payrollComputation = $payroll = new \App\ComputePayroll($d, $d->User->Profile->Compensation->daily_rate);
            }
        }

        $ytdPayrollCycles = \App\PayrollCycles::where(['payroll_group_id' => $user->Profile->Compensation->PayrollGroup->id, 'effective_year' => $payrollCycle->effective_year, 'status' => 'Processed'])->pluck('id');
        $ytdPayrollSummary = \App\PayrollSummary::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $d->User->id)->get();

        //        dd(PayrollCycles::where('status', 'Processed')->whereYear('payroll_date', date('Y', strtotime( $data['payrollCycle']->payroll_date)))->pluck('id'));

        $accumulatedHours = 0;
        $totalLeaves = 0;
        $unpaidLeave = 0;
        if(!empty($ytdPayrollSummary)) {
            $ytdDtr = \App\PayrollDtr::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $user->id)->get();
            $accumulatedHours = $ytdDtr->sum('vl') + $ytdDtr->sum('sl') + $ytdDtr->sum('el') + $ytdDtr->sum('spl') + $ytdDtr->sum('ml') + $ytdDtr->sum('pl') + $ytdDtr->sum('pto') + $ytdDtr->sum('bl') + $ytdDtr->sum('cl');
            $leaves = $payroll->vl + $payroll->sl + $payroll->el + $payroll->spl + $payroll->ml + $payroll->pl + $payroll->pto + $payroll->bl + $payroll->cl;

            if($leaves > 0) {
                $bal = (5 * 8) - $accumulatedHours;
                if($bal > 0) {

                    $bal -= $leaves;
                    if($bal < 0) {
                        $unpaidLeave = abs($bal)* $payroll->hourlyRate();
                    }

                } else {
                    $unpaidLeave = $leaves* $payroll->hourlyRate();
                }
            }
        }


        $ytdPayrollCycles = \App\PayrollCycles::where(['payroll_group_id' => $user->Profile->Compensation->PayrollGroup->id, 'effective_year' => $payrollCycle->effective_year])->pluck('id');
        $ytdPayrollSummary = \App\PayrollSummary::whereIn('payroll_cycle_id', $ytdPayrollCycles)->get();


        $loans = \App\LoanPaymentSchedules::where(['pay_period' => $payrollCode[1], 'user_id' => $user_id]);

        $sssLoan = 0;
        $hdmfLoan = 0;
        $coopLoan = 0;
        foreach($loans->get() as $lps) {
            if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'SSS') {
                $sssLoan += $lps->Loan->amortization;
            }

            if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'HDMF') {
                $hdmfLoan += $lps->Loan->amortization;
            }

            if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && in_array($lps->Loan->loan_type_id, ['1', '2', '3', '4'])) {
                $coopLoan += $lps->Loan->amortization;
            }
        }

        if($payrollCycle->cycle == 'B') {
            $payrollCycleA = \App\PayrollCycles::where([
                'payroll_group_id' => $payrollCycle->payroll_group_id,
                'effective_year' => $payrollCycle->effective_year,
                'effective_month' => $payrollCycle->effective_month,
                'cycle' => 'A',
                'status' => 'Processed'
            ])->first();

            $payrollSummaryA = \App\PayrollSummary::where('payroll_cycle_id', $payrollCycleA->id)->where('user_id', $user_id)->first();
            $basicPayA = !empty($payrollSummaryA->basic_pay) ? $payrollSummaryA->basic_pay : 0;
        }

        $basicPayB = 0;
        if($payrollCycle->cycle == 'A') {
            $effectiveDate = Carbon::createFromFormat('Y-m-d', "{$payrollCycle->effective_year}-{$payrollCycle->effective_month}-01")->subMonth(1);
            $payrollCycleB = PayrollCycles::where([
                'payroll_group_id' => $payrollCycle->payroll_group_id,
                'effective_year' => $effectiveDate->format('Y'),
                'effective_month' => $effectiveDate->format('m'),
                'cycle' => 'B',
                'status' => 'Processed'
            ])->first();

            if(empty($payrollCycleB)) {
                $basicPayB = 0;
            } else {
                $payrollSummaryB = PayrollSummary::where('payroll_cycle_id', $payrollCycleB->id)->where('user_id', $user_id)->first();
                $basicPayB = !empty($payrollSummaryB->basic_pay) ? $payrollSummaryB->basic_pay : 0;
            }


        }

        $philhealth_ee = 0;
        $philhealth_er = 0 ;
        if($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_philhealth == 'Y') {
            $philHealthContribution = \App\PhilHealthContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first();
            if($philHealthContribution->id == 3) {
                $monthlyPremium = ($payroll->basicPay() + $basicPayB) * ($philHealthContribution->premium_rate / 100);
            } elseif($philHealthContribution->id == 1) {
                $monthlyPremium = 275;
            } elseif($philHealthContribution->id == 2) {
                $monthlyPremium = 1100;
            }

            $philhealth_ee = $philhealth_er = $monthlyPremium / 2;
        }

        $thirteenthMonth = 0;
        if($payrollCycle->calculate_13th_month == 'Y') {
            $lastDecember = \App\PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)) - 1)->whereMonth('payroll_date', 12)->get();
            $toNovemberThisYear = \App\PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)))->whereMonth('payroll_date', 11)->get();

            foreach($lastDecember as $pc) {
                $pastSummary = \App\PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $user_id])->first();
                $thirteenthMonth += !empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0;
            }

            foreach($toNovemberThisYear as $pc) {
                $pastSummary = \App\PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $user_id])->first();
                $thirteenthMonth += !empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0;
            }
        }

        $membershipFee = 0;
        if(\App\Compensations::where('user_id', $d->user_id)->first()->membership_fee < \App\Keyval::where('key', 'membership_fee')->first()->value) {
            $membershipFee = \App\Keyval::where('key', 'membership_fee')->first()->value / 2;
        }

        $notarialFee = 0;
        if(\App\Compensations::where('user_id', $d->user_id)->first()->notarial_fee < \App\Keyval::where('key', 'notarial_fee')->first()->value) {
            $notarialFee = \App\Keyval::where('key', 'notarial_fee')->first()->value / 2;
        }


        $payroll = [
            'payroll_cycle_id' => $d->payroll_cycle_id,
            'user_id'=> $d->user_id,
            'basic_pay' => $payroll->basicPay(),
            'ecola' => ($d->Profile->Compensation->daily_category == 'Location') ? ($payroll->rg() / 8) * $d->Profile->Compensation->DailyRate->ecola : 0.00,
            'overtime_pay' => $payroll->OTPay(),
            'holiday_pay' => $payroll->HDPay(),
            'night_differential' => $payroll->NDPay(),
            'holiday_ecola' => $d->Profile->Compensation->daily_category == 'Location' ? ($payroll->leg / 8) * $d->Profile->Compensation->DailyRate->ecola : 0.00,
            'adjustment_wage' => $d->nwage,
            'adjustment_income' => $d->adji,
            'adjustment_deduction' => $d->adjd,
            'de_minimis' => $d->Profile->Compensation->de_minimis,
            'thirteenth_month_bonus' => $thirteenthMonth,
            'thirteenth_month_pro_rated' => $payroll->basicPay() / 12,
            'fixed_allowance' => $d->Profile->Compensation->fixed_allowance,
            'leave_encashment' => 0, // TODO
            'incentives' => $d->incentive,
            'coop_savings' => 0, // Omitted
            'sss_ec' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ec : 0,
            'sss_er' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->er : 0,
            'hdmf_er' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_hdmf == 'Y') ? \App\HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employer_share : 0,
            'philhealth_er' => $philhealth_er,
            'sss_ee' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ee : 0,
            'hdmf_ee' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_hdmf == 'Y') ? \App\HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employee_share : 0,
            'philhealth_ee' => $philhealth_ee,
            'tardiness' => ($payroll->hourlyRate() / 60) * ($payroll->lt + $payroll->ut),
            'absences' => ($payroll->hourlyRate() / 60) * $payroll->abs,
            'sss_loan' => $sssLoan,
            'hdmf_loan' => $hdmfLoan,
            'client_charges' => $d->client_charges,
            'membership_fee' => $membershipFee,
            'notarial_fee' => $notarialFee,
            'coop_loan' => $coopLoan,
            'coop_share' => $d->Profile->coop_share,
            'unpaid_leave' => $unpaidLeave
        ];



        if($unpaidLeave > 0) {
            $incomeTx = $payroll['basic_pay'] + $payrollComputation->rd() + $payrollComputation->spe() + $payrollComputation->sperd() + $payrollComputation->leg() + $payrollComputation->legrd() + $payroll['overtime_pay'] + $payroll['night_differential'] + $payroll['ecola'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
        } else {
            $incomeTx = $payroll['basic_pay'] + $payrollComputation->rd() + $payrollComputation->spe() + $payrollComputation->sperd() + $payrollComputation->leg() + $payrollComputation->legrd() + $payroll['overtime_pay'] + $payroll['night_differential'] + $payrollComputation->vl() + $payrollComputation->sl() + $payrollComputation->el() + $payrollComputation->spl() + $payrollComputation->ml() + $payrollComputation->pl() + $payrollComputation->pto() + $payrollComputation->bl() + $payrollComputation->cl() + $payroll['ecola'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
        }

        $incomeNtx = $payroll['de_minimis'] + $payroll['thirteenth_month_bonus'] + $payroll['fixed_allowance'] + $payroll['leave_encashment'] + $payroll['incentives'] + $payroll['adjustment_income'];
        $deductionTx = $payroll['sss_ee'] + $payroll['hdmf_ee'] + $payroll['philhealth_ee'] + $payroll['tardiness'] + $payroll['absences'];
        $deduction = $payroll['sss_loan'] + $payroll['hdmf_loan'] + $payroll['client_charges'] + $payroll['membership_fee'] + $payroll['notarial_fee'] + $payroll['coop_loan'] + $payroll['coop_share'] + $payroll['adjustment_deduction'];

        $payroll['tax'] = 0;
        if(\App\Compensations::where('user_id', $d->user_id)->first()->deduct_withholding == 'Y') {
            $taxTable = new \App\TaxTable($d->User->Profile->Compensation->TaxType->title, $incomeTx);
            $payroll['tax'] = $taxTable->taxDue();
        }

        $payroll['gross_amount'] = $incomeTx + $incomeNtx;
        $payroll['deductions'] = $deductionTx + $deduction + $unpaidLeave;
        $payroll['net_pay'] = ($payroll['gross_amount'] - $payroll['deductions']) - $payroll['tax'];

        $payroll = (object) $payroll;

        return $payroll;
    }
}

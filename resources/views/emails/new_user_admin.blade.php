@component('mail::message')

<p>Hi <strong>{{ $name }}</strong>,</p>
<br>
<p>Please be advised that you have been given an {{ $adminType }} access to the GTMC Portal.</p>
<br>
<p>Username: <strong>{{ $username }}</strong></p>
<p>Password: <i>(Your username is your default password)</i></p>
<br>
<p>Please change your password immediately.</p>
<br>
<br>
<p><i>*** This is an auto-generated email. ***</i></p>

<br>
<br>
@endcomponent

@component('mail::message')

<p>Hi <strong>{{ $name }}</strong>,</p>
<br>
<p>Your GTMC Portal account password was recently reset.</p>
<br>
<p>Username: <strong>{{ $username }}</strong></p>
<p><i>Password: (Your username is your default password)</i></p>
<br>
<p>Please change your password immediately.</p>
<br>
<br>
<p><i>*** This is an auto-generated email. ***</i></p>

<br>
<br>
@endcomponent

@component('mail::message')

<p>This is a notification that a new client is added:</p>
<br>
<p>
    Company Code : {{ $companyCode }} <br>
    Company Name : {{ $companyName }} <br>
    Effectivity Date : {{ $effectivityDate }} <br>
    Admin Fee Type : {{ $adminFeeType }} <br>
    Admin Fee : {{ $adminFee }}
</p>

<p></p>
<p></p>

<br>
<br>

<p><i>*** This is an auto-generated email. ***</i></p>
@endcomponent

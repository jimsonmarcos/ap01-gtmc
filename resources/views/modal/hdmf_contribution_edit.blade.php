<form action="{{ url("admin/super/hdmf_schedule/edit/{$HDMFContribution->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit PhilHealth Salary Bracket</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Salary Range Start</label>
                    <input type="text" class="form-control" name="salary_range_start" value="{{ $HDMFContribution->salary_range_start }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Salary Range End</label>
                    <input type="text" class="form-control" name="salary_range_end" value="{{ $HDMFContribution->salary_range_end }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Salary Base</label>
                    <input type="text" class="form-control" name="salary_base" value="{{ $HDMFContribution->salary_base }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Total Monthly Premium</label>
                    <input type="text" class="form-control" name="total_monthly_premium" value="{{ $HDMFContribution->total_monthly_premium }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Employer Share</label>
                    <input type="text" class="form-control" name="employee_share" value="{{ $HDMFContribution->employee_share }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Employee Share</label>
                    <input type="text" class="form-control" name="employer_share" value="{{ $HDMFContribution->employer_share }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
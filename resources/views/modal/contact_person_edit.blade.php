<form action="{{ url("admin/client/{$client_id}/contact-persons/edit/{$contactPerson->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit Contact Person</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-4" id="">
                <div class="form-group">
                    <label class="form-control-label">First Name</label>
                    <input type="text" class="form-control" name="first_name" value="{{ $contactPerson->first_name }}" required>
                </div>
            </div>

            <div class="col-md-4" id="">
                <div class="form-group">
                    <label class="form-control-label">Middle Name</label>
                    <input type="text" class="form-control" name="middle_name" value="{{ $contactPerson->middle_name }}">
                </div>
            </div>

            <div class="col-md-4" id="">
                <div class="form-group">
                    <label class="form-control-label">Last Name</label>
                    <input type="text" class="form-control" name="last_name" value="{{ $contactPerson->last_name }}" required>
                </div>
            </div>

            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Position</label>
                    <select name="position_id" id="" class="form-control">
                        @foreach(\App\Positions::orderBy('title')->get() as $position)
                            <option value="{{ $position->id }}" @if($position->id == $contactPerson->position_id) selected @endif>{{ $position->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Email Address</label>
                    <input type="email" class="form-control" name="email" value="{{ $contactPerson->email }}" required>
                </div>
            </div>

            <div class="col-md-12"></div>

            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Mobile</label>
                    <input type="text" class="mask-cellphone form-control" name="mobile1" value="{{ $contactPerson->mobile1 }}" placeholder="mobile 1">
                </div>
            </div>

            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label"></label>
                    <input type="text" class="mask-cellphone form-control" name="mobile2" value="{{ $contactPerson->mobile2 }}" placeholder="mobile 2" style="margin-top: 5px;">
                </div>
            </div>

            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Office Number</label>
                    <input type="text" class="mask-telephone form-control" name="office_number" value="{{ $contactPerson->office_number }}">
                </div>
            </div>

            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Local Number</label>
                    <input type="text" class="form-control" name="local_number" value="{{ $contactPerson->local_number }}" oninput="this.value = this.value.replace(/[^0-9]/g, '')" maxlength="7">
                </div>
            </div>


        </div>



        <div class="form-group">
            <button id="" type="reset" class="btn btn-warning"><span class="fa fa-refresh"></span> Reset</button>
        </div>


    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
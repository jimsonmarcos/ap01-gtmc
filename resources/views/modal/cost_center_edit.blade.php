<form action="{{ url("admin/client/{$client_id}/cost-center/edit/{$costCenter->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit Cost Center</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>
    <div class="modal-body">


        <div class="form-horizontal">
            <div class="form-group row" style="padding: 10px 15px;">
                <label class="col-sm-4 form-control-label">Cost Center</label>
                <div class="col-sm-8">
                    <input id="" type="text" name="cost_center" class="form-control" value="{{ $costCenter->cost_center }}" oninput="this.value = this.value.toUpperCase().replace(/[^A-Z_-]/g, '').replace(/(\..*)\./g, '$1');" required>
                </div>
            </div>
            <div class="form-group row" style="padding: 10px 15px;">
                <label class="col-sm-4 form-control-label">Group Definition</label>
                <div class="col-sm-8">
                    <input id="" type="text" name="group_definition" class="form-control" value="{{ $costCenter->group_definition }}">
                </div>
            </div>
        </div>

            <div class="form-group">
                <button id="" type="reset" class="btn btn-warning"><span class="fa fa-refresh"></span> Reset</button>
            </div>


    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
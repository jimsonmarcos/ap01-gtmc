<form action="{{ url("admin/super/loan-type/edit/{$loanType->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Loan Type</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Loan Type</label>
                    <input type="text" class="form-control" name="type" value="{{ $loanType->type }}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Loan Interest Percent</label>
                    <input type="text" class="form-control" name="interest" value="{{ $loanType->interest }}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Service Charge Percent</label>
                    <input type="text" class="form-control" name="service_charge" value="{{ $loanType->service_charge }}" required>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Savings Percent</label>
                    <input type="text" class="form-control" name="savings" value="{{ $loanType->savings }}" required>
                </div>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
<form action="{{ url("admin/super/tax/edit/{$tax->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit Tax</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Tax Schedule</label>
                    <input type="text" class="form-control" name="tax_schedule" value="{{ $tax->bank }}" required>
                </div>
            </div>

            <div class="col-md-6">
                <label class="col-xs-12" for="tax_type">Tax Type</label>
                <div class="col-xs-12">

                    <select class="form-control" id="tax_type" name="tax_type" size="1">
                        @foreach( $tax_type as $tax_type)
                            <option value="{{ $tax_type->title }}">{{ $tax_type->title }}</option>
                        @endforeach
                    </select>
                    <br/>
                </div>
            </div>

            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Base Rate</label>
                    <input type="text" class="form-control" name="base_rate" value="{{ $tax->base_rate }}" required>
                </div>
            </div>

            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Tax Status</label>
                    <input type="text" class="form-control" name="tax_status" value="{{ $bank->tax_status }}" required>
                </div>
            </div>

            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Exemption</label>
                    <input type="text" class="form-control" name="exemption" value="{{ $bank->exemption }}" required>
                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
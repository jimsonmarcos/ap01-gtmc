<form action="{{ url("admin/super/relationship/edit/{$relationship->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit Relationship</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Relationship</label>
                    <input type="text" class="form-control" name="title" value="{{ $relationship->title }}" required>
                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
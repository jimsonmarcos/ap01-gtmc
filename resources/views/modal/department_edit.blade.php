<form action="{{ url("admin/super/departments/edit/{$department->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit Department</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">City/Province</label>
                    <input type="text" class="form-control" name="province" value="{{ $department->province }}" required>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Area</label>
                    <input type="text" class="form-control" name="area" value="{{ $department->area }}" required>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Mall</label>
                    <input type="text" class="form-control" name="mall" value="{{ $department->mall }}" required>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Outlet</label>
                    <input type="text" class="form-control" name="outlet" value="{{ $department->outlet }}" required>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Department</label>
                    <input type="text" class="form-control" name="department" value="{{ $department->department }}" required>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
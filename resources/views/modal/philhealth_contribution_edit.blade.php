<form action="{{ url("admin/super/philhealth_schedule/edit/{$PhilHealthContribution->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit PhilHealth Salary Bracket</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Salary Range Start</label>
                    <input type="text" class="form-control" name="salary_range_start" value="{{ $PhilHealthContribution->salary_range_start }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Salary Range End</label>
                    <input type="text" class="form-control" name="salary_range_end" value="{{ $PhilHealthContribution->salary_range_end }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Salary Base</label>
                    <input type="text" class="form-control" name="salary_base" value="{{ $PhilHealthContribution->salary_base }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Premium Rate %</label>
                    <input type="text" class="form-control" name="premium_rate" value="{{ $PhilHealthContribution->premium_rate }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        {{-- Sarah ask to remove this, so i hide it for now --}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6" id="">--}}
                {{--<div class="form-group">--}}
                    {{--<label class="form-control-label">Total Monthly Premium</label>--}}
                    {{--<input type="text" class="form-control" name="total_monthly_premium" value="{{ $PhilHealthContribution->total_monthly_premium }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6" id="">--}}
                {{--<div class="form-group">--}}
                    {{--<label class="form-control-label">Employer Share</label>--}}
                    {{--<input type="text" class="form-control" name="employee_share" value="{{ $PhilHealthContribution->employee_share }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6" id="">--}}
                {{--<div class="form-group">--}}
                    {{--<label class="form-control-label">Employee Share</label>--}}
                    {{--<input type="text" class="form-control" name="employer_share" value="{{ $PhilHealthContribution->employer_share }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{-- End --}}

    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
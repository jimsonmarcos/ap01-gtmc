<form action="{{ url("admin/user/{$user_id}/beneficiary/add") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Add Beneficiary</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-4" id="">
                <div class="form-group">
                    <label class="form-control-label">First Name</label>
                    <input type="text" class="form-control" name="first_name" required>
                </div>
            </div>

            <div class="col-md-4" id="">
                <div class="form-group">
                    <label class="form-control-label">Middle Name</label>
                    <input type="text" class="form-control" name="middle_name" required>
                </div>
            </div>

            <div class="col-md-4" id="">
                <div class="form-group">
                    <label class="form-control-label">Last Name</label>
                    <input type="text" class="form-control" name="last_name" required>
                </div>
            </div>

            <div class="col-md-6" id="">
                <label class="form-control-label">Birth Date</label>
                <div class='input-group btn-mmddyyyy'>
                    <input type='text' class="form-control" name="birthday" value="{{ date('m/d/Y') }}" placeholder="mm/dd/yyyy" required />
                    <span class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                    </span>
                </div>
            </div>

            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Relationship to Member</label>
                    <select name="member_relationship_id" class="form-control">
                        @foreach(\App\MemberRelationships::all()->sortBy('title') as $relationsip)
                            <option value="{{ $relationsip->id }}">{{ $relationsip->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>




        </div>



        <div class="form-group">
            <button id="" type="reset" class="btn btn-warning"><span class="fa fa-refresh"></span> Reset</button>
        </div>


    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
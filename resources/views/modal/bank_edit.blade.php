<form action="{{ url("admin/super/bank/edit/{$bank->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit Relationship</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Bank</label>
                    <input type="text" class="form-control" name="bank_name" value="{{ $bank->bank }}" required>
                </div>
            </div>

            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Bank Code</label>
                    <input type="text" class="form-control" name="bank_code" value="{{ $bank->bank_code }}" required>
                </div>
            </div>

            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Bank Full Name</label>
                    <input type="text" class="form-control" name="bank_full_name" value="{{ $bank->bank_full_name }}" required>
                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
<form action="{{ url("admin/client/{$client_id}/payroll-group/edit/{$payrollGroup->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit Payroll Group</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>
    <div class="modal-body">
        <div class="row">

            <div class="form-horizontal">
                <div class="form-group row" style="padding: 10px 15px;">
                    <label class="col-sm-4 form-control-label">Payroll Group</label>
                    <div class="col-sm-8">
                        <input id="" type="text" name="group_name" class="form-control" value="{{ $payrollGroup->group_name }}" oninput="this.value = this.value.toUpperCase().replace(/[^0-9A-Z_-]/g, '').replace(/(\..*)\./g, '$1');" required>
                    </div>
                </div>
                <div class="form-group row" style="padding: 10px 15px;">
                    <label class="col-sm-4 form-control-label">Group Definition</label>
                    <div class="col-sm-8">
                        <input id="" type="text" name="group_definition" class="form-control" value="{{ $payrollGroup->group_definition }}">
                    </div>
                </div>

                <div class="col-md-12">
                    <table class="table" style="max-width: 100%;">
                        <tr>
                            <th></th>
                            <th>Start Date</th>
                            <th>Payroll Date</th>
                        </tr>

                        <tr>
                            <th>A</th>
                            <td>
                                <select name="start_date_a" id="" class="form-control" style="width: 75px;">
                                    @for($x = 1; $x <= 31; $x++)
                                        <option value="{{ $x }}" @if($payrollGroup->start_date_a == $x) selected @endif>{{ $x }}</option>
                                    @endfor
                                </select>
                            </td>
                            <td>
                                <select name="payroll_date_a" id="" class="form-control" style="width: 75px;">
                                    @for($x = 1; $x <= 31; $x++)
                                        <option value="{{ $x }}" @if($payrollGroup->payroll_date_a == $x) selected @endif>{{ $x }}</option>
                                    @endfor
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <th>B</th>
                            <td>
                                <select name="start_date_b" id="" class="form-control" style="width: 75px;">
                                    @for($x = 1; $x <= 31; $x++)
                                        <option value="{{ $x }}" @if($payrollGroup->start_date_b == $x) selected @endif>{{ $x }}</option>
                                    @endfor
                                </select>
                            </td>
                            <td>
                                <select name="payroll_date_b" id="" class="form-control" style="width: 75px;">
                                    @for($x = 1; $x <= 31; $x++)
                                        <option value="{{ $x }}" @if($payrollGroup->payroll_date_b == $x) selected @endif>{{ $x }}</option>
                                    @endfor
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>



        <div class="form-group">
            <button id="" type="reset" class="btn btn-warning"><span class="fa fa-refresh"></span> Reset</button>
        </div>


    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
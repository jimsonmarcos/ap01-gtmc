<form action="{{ url("admin/super/sss_schedule/edit/{$SSSContribution->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit SSS Salary Bracket</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Compensation Range Start</label>
                    <input type="text" class="form-control" name="compensation_range_start" value="{{ $SSSContribution->compensation_range_start }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Compensation Range End</label>
                    <input type="text" class="form-control" name="compensation_range_end" value="{{ $SSSContribution->compensation_range_end }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Monthly Salary Credit</label>
                    <input type="text" class="form-control" name="monthly_salary_credit" value="{{ $SSSContribution->monthly_salary_credit }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">ER</label>
                    <input type="text" class="form-control" name="er" value="{{ $SSSContribution->er }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">EE</label>
                    <input type="text" class="form-control" name="ee" value="{{ $SSSContribution->ee }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">EC</label>
                    <input type="text" class="form-control" name="ec" value="{{ $SSSContribution->ec }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Total</label>
                    <input type="text" class="form-control" name="total" value="{{ $SSSContribution->total }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
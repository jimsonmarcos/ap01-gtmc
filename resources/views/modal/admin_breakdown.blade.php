<form action="{{ url("admin/client/{$company->id}/admin-breakdown") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit Admin Breakdown</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>
    <div class="modal-body">


        <div class="form-group">
            <label for="disabledSelect">Admin Breakdown list</label>
            <?php
                $list = [
                    'BASIC',
                    'RDOT',
                    'SPE',
                    'SPERD',
                    'LEG',
                    'LEGRD',
                    'OT',
                    'ND',
                    'SIL',
                    'ECOLA',
                    'HECOLA',
                    '13TH MONTH',
                    'INCENTIVES',
                    'SSS ER',
                    'SSS EC',
                    'HDMF ER',
                    'PhilHealth ER'
                ];

                $selected = !empty($company->admin_breakdown) ? explode(';', $company->admin_breakdown) : [];
            ?>
            <select id="mt" class="form-control" name="admin_breakdown[]" multiple='multiple'>
                @foreach($list as $l)
                    <option value="{{ $l }}" @if(in_array($l, $selected)) selected @endif>{{ $l }}</option>
                @endforeach
            </select>
        </div>


    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
<form action="{{ url("admin/super/daily-category/edit/{$dailyRate->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit Daily Rate</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Category</label>
                    <select class="form-control" id="category" name="category" size="1">
                        <option value="LOCATION" {{ $dailyRate->category == 'LOCATION' ? 'selected' : '' }}>LOCATION</option>
                        <option value="REGION" {{ $dailyRate->category == 'REGION' ? 'selected' : '' }}>REGION</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Location</label>
                    <input type="text" class="form-control" name="location" value="{{ $dailyRate->location }}" required>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Rate</label>
                    <input type="text" class="form-control" name="rate" value="{{ $dailyRate->rate }}" required>
                </div>
                <div class="form-group">
                    <label class="form-control-label">ECOLA</label>
                    <input type="text" class="form-control" name="ecola" value="{{ $dailyRate->ecola }}" required>
                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
<form action="{{ url("admin/super/mode-of-transfer/edit/{$transferMode->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit Mode of Transfer</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Mode of Transfer</label>
                    <select name="transfer_mode" id="" class="form-control">
                        <option value="BANK DEPOSIT" @if($transferMode->transfer_mode == 'BANK DEPOSIT') selected @endif>BANK DEPOSIT</option>
                        <option value="BDO" @if($transferMode->transfer_mode == 'BDO') selected @endif>BDO</option>
                        <option value="COURIER" @if($transferMode->transfer_mode == 'COURIER') selected @endif>COURIER</option>
                        <option value="GCASH" @if($transferMode->transfer_mode == 'GCASH') selected @endif>GCASH</option>
                        <option value="HAND OVER" @if($transferMode->transfer_mode == 'HAND OVER') selected @endif>HAND OVER</option>
                        <option value="METROBANK" @if($transferMode->transfer_mode == 'METROBANK') selected @endif>METROBANK</option>
                        <option value="PICK UP" @if($transferMode->transfer_mode == 'PICK UP') selected @endif>PICK UP</option>
                        <option value="UNIONBANK" @if($transferMode->transfer_mode == 'UNIONBANK') selected @endif>UNIONBANK</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Transfer Type</label>
                    <select name="transfer_type" id="" class="form-control">
                        <option value="ATM" @if($transferMode->transfer_type == 'ATM') selected @endif>ATM</option>
                        <option value="CASH" @if($transferMode->transfer_type == 'CASH') selected @endif>CASH</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Bank</label>
                    <input type="text" class="form-control" name="bank" value="{{ $transferMode->bank }}">
                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
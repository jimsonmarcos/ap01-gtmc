<form action="{{ url("admin/super/business-tax-type/edit/{$businessTaxType->id}") }}" method="POST">
    {{ csrf_field() }}

    <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">Edit Business Tax Type</h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Business Tax Type</label>
                    <input type="text" class="form-control" name="tax" value="{{ $businessTaxType->tax }}" required>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6" id="">
                <div class="form-group">
                    <label class="form-control-label">Percent</label>
                    <input type="text" class="form-control" name="percent" value="{{ $businessTaxType->percent }}" required>
                </div>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

        <button class="btn btn-primary">Confirm</button>
    </div>
</form>
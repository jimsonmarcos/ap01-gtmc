<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GTMC - Multi-Purpose System</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">--}}
<!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('css/style.blue.css') }}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{ asset('datatables/datatables.min.css') }}"/>
    <!-- DateTime Picker -->
    <link rel="stylesheet" type="text/css" href="{{ asset('datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/>
    <!-- Font Icons CSS-->
    <!-- Select2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
{{--<link rel="stylesheet" href="https://file.myfontastic.com/da58YPMQ7U5HY8Rb6UxkNf/icons.css">--}}
<!-- Font Awesome CDN-->
    <script src="https://use.fontawesome.com/8be58b1748.js"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>

<div class="page login-page">
    <div class="container d-flex align-items-center" style="position: relative; z-index: 5;">
        <div class="form-holder">
            <div class="row">
                <!-- Logo & Information Panel-->
                <div class="col-lg-4"></div>
                <!-- Form Panel    -->
                <div class="col-lg-4">
                    <div class="form d-flex align-items-center">
                        <div class="content">
                            <div class="m-login__logo">
                                <a href="#">
                                    <img src="{{ ('img/logo.png') }}" width="250">
                                </a>
                            </div>
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                    Sign In
                                </h3>
                            </div>
                            <form id="login-form" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}

                                @if ($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                </div>
                                @endif
                                
                                <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                    <input id="login-username" type="text" name="username" class="input-material" value="" placeholder="Username" required>
                                    @if ($errors->has('username'))
                                        <label id="login-username-error" class="error" for="login-username" style="">{{ $errors->first('username') }}</label>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                    <input id="login-password" type="password" name="password" class="input-material" placeholder="Password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    <!-- <a href="#" class="forgot-pass pull-right">Forgot Password?</a> -->
                                </div>
                                
                                <div class="form-group login-btn">
                                    <button id="login" class="btn btn-primary">Login</button>
                                </div>
                                <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                            </form>
                            
                            {{--<br><small>Do not have an account? </small><a href="register.html" class="signup">Signup</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyrights text-center">
        {{--<p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a></p>--}}
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
    </div>
</div>


<!-- Javascript files-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
<script src="{{ asset('datatables/datatables.min.js') }}"></script>
<script src="{{ asset('js/tether.min.js') }}"></script>
{{--<script src="{{ asset('js/bootstrap.min.js') }}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/jquery.cookie.js') }}"> </script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/front.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script src="{{ asset('js/custom.js') }}"></script>

<div id="page-loader" style="display: none;"><span class="fa fa-spin fa-refresh"></span></div>

<div id="ajaxPopup" tabindex="-1" role="dialog" aria-labelledby="" class="modal fade text-left" aria-hidden="true" style="display: none;">
    <div role="document" class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>

</body>
</html>
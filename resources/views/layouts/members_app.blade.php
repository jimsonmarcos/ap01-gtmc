<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GTMC - Multi Purpose Portal</title>
    <meta name="description" content="GTMC - Multi-Purpose System<">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

    <!-- Stylesheets -->
    <!-- Web fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <!-- Bootstrap and OneUI CSS framework -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('assets/css/oneui.css') }}">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
{{--    <link rel="stylesheet" id="css-theme" href="{{ asset('assets/css/themes/smooth.min.css') }}">--}}
    <!-- END Stylesheets -->

    {{--<!-- Bootstrap CSS-->--}}
    {{--<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">--}}
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">--}}
    {{--<!-- Google fonts - Roboto -->--}}
    {{--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">--}}
    {{--<!-- theme stylesheet-->--}}
    {{--<link rel="stylesheet" href="{{ asset('css/style.blue.css') }}" id="theme-stylesheet">--}}

    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <!-- SweetAlert -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/js/plugins/sweetalert2/sweetalert2.min.css') }}"/>
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{ asset('datatables/datatables.min.css') }}"/>
    <!-- DateTime Picker -->
    <link rel="stylesheet" type="text/css" href="{{ asset('datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/>
    <!-- Select2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    {{--<link rel="stylesheet" href="https://file.myfontastic.com/da58YPMQ7U5HY8Rb6UxkNf/icons.css">--}}
    <!-- Font Awesome CDN-->
    <script src="https://use.fontawesome.com/8be58b1748.js"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>

<!-- Page Container -->
<!--
    Available Classes:

    'enable-cookies'             Remembers active color theme between pages (when set through color theme list)

    'sidebar-l'                  Left Sidebar and right Side Overlay
    'sidebar-r'                  Right Sidebar and left Side Overlay
    'sidebar-mini'               Mini hoverable Sidebar (> 991px)
    'sidebar-o'                  Visible Sidebar by default (> 991px)
    'sidebar-o-xs'               Visible Sidebar by default (< 992px)

    'side-overlay-hover'         Hoverable Side Overlay (> 991px)
    'side-overlay-o'             Visible Side Overlay by default (> 991px)

    'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

    'header-navbar-fixed'        Enables fixed header
-->

<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">

    <!-- Sidebar -->
    <nav id="sidebar">
        <!-- Sidebar Scroll Container -->
        <div id="sidebar-scroll">
            <!-- Sidebar Content -->
            <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
            <div class="sidebar-content">
                <!-- Side Header -->
                <div class="side-header side-content bg-white-op">
                    <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                        <i class="fa fa-times"></i>
                    </button>

                    <a class="h5 text-white" href="#">
                        <span class="h4 font-w600" id="sidebar-brand">GTMC</span>
                        {{--<span class="h4 font-w600 sidebar-mini-hide">ne</span>--}}
                    </a>
                    <p class="text-white sidebar-mini-hidden"><strong>Multi-Purpose Portal</strong></p>
                    <p class="h6 sidebar-mini-hidden"><small style="color: #FFF;"><?php date_default_timezone_set('Asia/Manila'); echo   date('F d, Y h:i:s A'); ?></small></p>
                </div>
                <!-- END Side Header -->

                <!-- Side Content -->
                <div class="side-content side-content-full">
                    <ul class="nav-main">
                        <li> <a href="{{ route('member_profile') }}" class="active"><i class="fa fa-home"></i><span class="sidebar-mini-hide">Home</span></a></li>
                        <li> <a href="{{ route('member_loan_shares', ['id' => $user->id ]) }}" class="active"><i class="fa fa-home"></i><span class="sidebar-mini-hide">Loans and Shares</span></a></li>
                        <li> <a href="{{ route('member_loan_payments', ['id' => $user->id ]) }}" class="active"><i class="fa fa-home"></i><span class="sidebar-mini-hide">Loans Payments</span></a></li>
                    </ul>
                </div>
            </div>
            <!-- Sidebar Content -->
        </div>
        <!-- END Sidebar Scroll Container -->
    </nav>
    <!-- END Sidebar -->

    <!-- Header -->
    <header id="header-navbar" class="content-mini content-mini-full">
        <!-- Header Navigation Right -->
        <ul class="nav-header pull-right">
            <li>
                <div class="btn-group">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button">
                        <span>Hello, </span>
                        <i class="fa fa-user"></i>
                        <span>{{ \Illuminate\Support\Facades\Auth::user()->username }}</span>
                        {{--<img src="assets/img/avatars/avatar10.jpg" alt="Avatar">--}}
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        {{--<li class="dropdown-header">Profile</li>--}}
                        {{--<li>--}}
                            {{--<a tabindex="-1" href="#">--}}
                                {{--<i class="si si-user pull-right"></i>--}}
                                {{--<span class="badge badge-success pull-right">1</span>Profile--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a tabindex="-1" href="javascript:void(0)">--}}
                                {{--<i class="si si-settings pull-right"></i>Settings--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="divider"></li>--}}
                        <li class="dropdown-header">Actions</li>
                        <li>
                            <a tabindex="-1" href="{{ url('/settings/change-password') }}">
                                <i class="si si-key pull-right"></i>Change Password
                            </a>
                            <a tabindex="-1" href="{{ url('/logout') }}">
                                <i class="si si-logout pull-right"></i>Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <!-- END Header Navigation Right -->

        <!-- Header Navigation Left -->
        <ul class="nav-header pull-left">
            <li class="hidden-md hidden-lg">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                    <i class="fa fa-navicon"></i>
                </button>
            </li>

            <li class="hidden-xs hidden-sm">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                    <i class="fa fa-list"></i>
                </button>
            </li>
        </ul>
        <!-- END Header Navigation Left -->
    </header>
    <!-- END Header -->


    <?php /*
    <!-- Main Navbar-->
    <header class="header">
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                    <!-- Navbar Header-->
                    <div class="navbar-header">
                        <!-- Navbar Brand --><a href="index.html" class="navbar-brand">
                            <div class="brand-text brand-big hidden-lg-down" style="position: relative;">
                                <strong>GTMC</strong> <span>Multi-Purpose System</span>
                                <div style="position: absolute; left:0; top: 24px;font-size: 1rem"><small><?php echo date('F t, Y h:i:s A'); ?></small></div>
                            </div>
                            <div class="brand-text brand-small" style="position: relative;"><strong>GTMC</strong>
                                <div style="position: absolute; left:0; top: 24px;font-size: 1rem"><small><?php echo date('F t, Y h:i:s A'); ?></small></div>
                            </div></a>
                        <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>

                    </div>
                    <!-- Navbar Menu -->
                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                        <!-- Logout -->
                        <li class="nav-item">Welcome, {{ Auth::user()->username }}</li>
                        <li class="nav-item"><a href="{{ url('logout') }}" class="nav-link logout">Logout<i class="fa fa-sign-out"></i></a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
 */ ?>




    <!-- Main Container -->
    <main id="main-container">
        <!-- Page Header -->
        <div class="content bg-image overflow-hidden" style="background: rgb(43, 144, 217);">
            <div class="push-20-t push-15">
                <h1 class="h2 text-white animated zoomIn">{{ $pageHeader }}</h1>
                @if(isset($pageSubtitle))
                <h2 class="h5 text-white-op animated zoomIn">{{ $pageSubtitle }}</h2>
                @endif
            </div>
        </div>

        @if(!empty(session('success')))
            <div class="alert alert-success">{!! session('success') !!}</div>
        @endif

        @if(!empty(session('error')))
            <div class="alert alert-danger">{!! session('error') !!}</div>
        @endif

{{--        {!! $sidebar !!}--}}

        <div id="main-content" class="content bg-white">



@yield('content')

<!-- Javascript files-->

<!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
<script src="{{ asset('assets/js/core/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/core/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/core/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('assets/js/core/jquery.scrollLock.min.js') }}"></script>
<script src="{{ asset('assets/js/core/jquery.appear.min.js') }}"></script>
<script src="{{ asset('assets/js/core/jquery.countTo.min.js') }}"></script>
<script src="{{ asset('assets/js/core/jquery.placeholder.min.js') }}"></script>
<script src="{{ asset('assets/js/core/js.cookie.min.js') }}"></script>
<script src="{{ asset('assets/js/app.js') }}"></script>

{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>--}}
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
<script src="{{ asset('datatables/datatables.min.js') }}"></script>
{{--<script src="{{ asset('js/tether.min.js') }}"></script>--}}
{{--<script src="{{ asset('js/bootstrap.min.js') }}"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>--}}
<!-- SweetAlert -->
<script src="{{ asset('assets/js/plugins/sweetalert2/es6-promise.auto.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
{{--<script src="{{ asset('js/jquery.cookie.js') }}"> </script>--}}
{{--<script src="{{ asset('js/jquery.validate.min.js') }}"></script>--}}
{{--<script src="{{ asset('js/front.js') }}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script src="{{ asset('js/custom.js') }}"></script>
@yield('extendedscript')

<div id="page-loader" style="display: none;"><span class="fa fa-spin fa-refresh"></span></div>

<div id="ajaxPopup" tabindex="-1" role="dialog" aria-labelledby="" class="modal fade text-left" aria-hidden="true" style="display: none;">
    <div role="document" class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>

@if(!empty(session('popSuccess')))
<script>swal('Success!', "{!! session('popSuccess') !!}", 'success')</script>
@endif

@if(!empty(session('popError')))
<script>swal('error!', "{!! session('popError') !!}", 'error')</script>
@endif

</body>
</html>
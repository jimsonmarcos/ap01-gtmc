@extends('layouts.client_app')

@section('content')

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-9">

                                    <table class="table" style="max-width: 100%;">
                                        <tr>
                                            <td width="150">Client ID</td>
                                            <td>{{ $client->client_id }}</td>
                                            <td width="150">Company Code</td>
                                            <td>{{ $client->company_code }}</td>
                                        </tr>
                                        <tr>
                                            <td width="150">Company Name</td>
                                            <td colspan="3">{{ $client->company_name }}</td>
                                        </tr>
                                        @if(!empty($client->parent_company_id))
                                            <tr>
                                                <td width="150">Parent Company</td>
                                                <td colspan="3">{{ $client->ParentCompany->company_code }}</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td width="150">Company Address</td>
                                            <td colspan="3">{{ $client->company_address }}</td>
                                        </tr>
                                        <tr>
                                            <td width="150">Trunk Line</td>
                                            <td>{{ $client->trunk_line }}</td>
                                            <td>Fax Number</td>
                                            <td>{{ $client->fax }}</td>
                                        </tr>
                                        <tr>
                                            <td width="150">SSS</td>
                                            <td colspan="3">{{ $client->sss }}</td>
                                        </tr>
                                        <tr>
                                            <td width="150">HDMF</td>
                                            <td colspan="3">{{ $client->hdmf }}</td>
                                        </tr>
                                        <tr>
                                            <td>PhilHealth</td>
                                            <td colspan="3">{{ $client->philhealth }}</td>
                                        </tr>
                                        <tr>
                                            <td>TIN</td>
                                            <td colspan="3">{{ $client->tin }}</td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="col-md-3">
                                    <div class="" style="padding: 5px;">
                                        @if(empty($client->logo_path))
                                            <img src="{{ url("img/logo-placeholder.jpg") }}" alt="" style="max-width: 100%;">
                                        @else
                                            <img src="{{ url($client->logo_path) }}" alt="" style="max-width: 100%;">
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

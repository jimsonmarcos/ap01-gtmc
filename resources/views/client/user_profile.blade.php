@extends('layouts.client_app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <a href="{{ url("client/user-profiles") }}"><i class="fa fa-arrow-left"></i> Back to User Profiles</a>
                                    <br>
                                    <br>
                                </div>

                                <div class="col-md-12">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ url("client/user-profile/{$profile->id_number}") }}">Personal Information </a>
                                        </li>
                                        @if($profile->User->role != 'Member')
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ url("client/user-profile/{$profile->id_number}/employment-details") }}">Employment Details</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ url("client/user-profile/{$profile->id_number}/compensations-leave-credits") }}">Compensation and Leave Credits</a>
                                            </li>
                                        @endif
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url("client/user-profile/{$profile->id_number}/loans-shares") }}">Loans and Shares</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-9">
                                    <br>
                                    <br>

                                    <table class="table no-margin-bottom">
                                        <tbody>
                                        <tr>
                                            <td width="110">First Name:</td>
                                            <td width="">{{ strtoupper($profile->first_name) }}</td>
                                            <td width="130">Middle Name:</td>
                                            <td width="">{{ strtoupper($profile->middle_name) }}</td>
                                            <td width="110">Last Name:</td>
                                            <td width="">{{ strtoupper($profile->last_name) }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td>Civil Status:</td>
                                            <td>{{ strtoupper($profile->CivilStatus->title) }}</td>
                                            <td>Gender:</td>
                                            <td colspan="3">{{ strtoupper($profile->gender) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Address:</td>
                                            <td colspan="5">{{ strtoupper($profile->address) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Mobile:</td>
                                            <td>{{ $profile->mobile1 }}</td>
                                            <td>{{ $profile->mobile2 }}</td>
                                            <td>Email:</td>
                                            <td>{{ strtolower($profile->email) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Birth Date</td>
                                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $profile->birthday)->format('M. j, Y') }}</td>
                                            <td>Age</td>
                                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $profile->birthday)->age }}</td>
                                        </tr>
                                        <tr>
                                            <td>Mother's Maiden <br />Name</td>
                                            <td colspan="5">{{ strtoupper($profile->mothers_maiden_name) }}</td></tr>
                                        <tr>
                                            <td>Educational Attainment</td>
                                            <td colspan="5">@if(!empty($profile->EducationalAttainment->title)) {{ strtoupper($profile->EducationalAttainment->title) }} @endif</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <hr />
                                    <h2>In Case of Emergency</h2>
                                    <br>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td>Contact Person</td>
                                            <td>{{ strtoupper($profile->icoe_contact_name) }}</td>
                                            <td>Contact Number</td>
                                            <td>{{ strtoupper($profile->icoe_contact_mobile) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td colspan="3">{{ strtoupper($profile->icoe_contact_address) }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <div class="" style="padding: 5px;">
                                        @if(empty($profile->photo_path))
                                            <img src="{{ url("img/user-placeholder.png") }}" alt="" style="max-width: 100%;">
                                        @else
                                            <img src="{{ url($profile->photo_path) }}" alt="" style="max-width: 100%;">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#users-dt').DataTable({
                "pageLength": 50

            });
        })
    </script>
@endsection

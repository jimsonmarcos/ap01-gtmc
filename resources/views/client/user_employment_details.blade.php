@extends('layouts.client_app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <a href="{{ url("client/user-profiles") }}"><i class="fa fa-arrow-left"></i> Back to User Profiles</a>
                                    <br>
                                    <br>
                                </div>

                                <div class="col-md-12">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url("client/user-profile/{$profile->id_number}") }}">Personal Information </a>
                                        </li>
                                        @if($profile->User->role != 'Member')
                                            <li class="nav-item">
                                                <a class="nav-link active" href="{{ url("client/user-profile/{$profile->id_number}/employment-details") }}">Employment Details</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ url("client/user-profile/{$profile->id_number}/compensations-leave-credits") }}">Compensation and Leave Credits</a>
                                            </li>
                                        @endif
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url("client/user-profile/{$profile->id_number}/loans-shares") }}">Loans and Shares</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12">
                                    <br>
                                    <br>

                                    <table class="table no-margin-bottom" style="max-width: 800px;">
                                        <tbody>
                                        <tr>
                                            <td>Employment Status:</td>
                                            <td colspan="3"><strong>@if(!empty($profile->employmentdetails->reason) && date('Y-m-d', strtotime($profile->employmentdetails->effective_date)) <= date('Y-m-d')) <span class="text-danger">{{ strtoupper($profile->employmentdetails->reason) }}</span> @else <span class="text-success">ACTIVE</span> @endif</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="110">Principal:</td>
                                            <td width="">{{ $profile->employmentdetails->principal->company_code }}</td>
                                            <td width="130">Hire Date:</td>
                                            <td width="">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $profile->employmentdetails->hire_date)->format('M. j, Y') }}</td>
                                        </tr>
                                        <tr>
                                            <td width="110">Position</td>
                                            <td colspan="5">@if(!empty($profile->employmentdetails->position->title)) {{ $profile->employmentdetails->position->title }} @endif <?php if($profile->employmentdetails->orientation == 'Y' && $profile->employmentdetails->position_id == '1') { echo "<br /> <small class='text-success'><span class='fa fa-check'></span> Orientation</small>"; } ?></td>
                                        </tr>
                                        <?php if($profile->employmentdetails->position_id == '1') { ?>
                                        <tr>
                                            <td>Trainee Start<br />Date</td>
                                            <td>@if(!empty($profile->employmentdetails->trainee_start_date)) {{ \Carbon\Carbon::createFromFormat('Y-m-d', $profile->employmentdetails->trainee_start_date)->format('M. j, Y') }} @endif</td>
                                            <td>Trainee End<br />Date</td>
                                            <td>@if(!empty($profile->employmentdetails->trainee_end_date)) {{ \Carbon\Carbon::createFromFormat('Y-m-d', $profile->employmentdetails->trainee_end_date)->format('M. j, Y') }} @endif</td>
                                        </tr>
                                        <?php } ?>

                                        {{--                                                    @if($profile->EmploymentDetails->position_id == 1 || $profile->EmploymentDetails->position_id == 2)--}}
                                        <tr>
                                            <td width="110">Department</td>
                                            <td colspan="5">
                                                <?php
                                                $department = [];
                                                if(!empty($profile->EmploymentDetails->Outlet->outlet)) $department[] = $profile->EmploymentDetails->Outlet->outlet;
                                                if(!empty($profile->EmploymentDetails->Mall->mall)) $department[] = $profile->EmploymentDetails->Mall->mall;
                                                if(!empty($profile->EmploymentDetails->Area->area)) $department[] = $profile->EmploymentDetails->Area->area;
                                                if(!empty($profile->EmploymentDetails->Province->province)) $department[] = $profile->EmploymentDetails->Province->province;
                                                echo implode(' - ', $department);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="110">City/Province</td>
                                            <td>@if(!empty($profile->EmploymentDetails->Province->province)) {{ strtoupper($profile->EmploymentDetails->Province->province) }} @endif</td>
                                            <td>Mall</td>
                                            <td colspan="3">@if(!empty($profile->EmploymentDetails->Mall->mall)) {{ strtoupper($profile->EmploymentDetails->Mall->mall) }} @endif</td>
                                        </tr>
                                        <tr>
                                            <td width="110">Area</td>
                                            <td>@if(!empty($profile->EmploymentDetails->Area->area)) {{ strtoupper($profile->EmploymentDetails->Area->area) }} @endif</td>
                                            <td>Outlet</td>
                                            <td colspan="3">@if(!empty($profile->EmploymentDetails->Outlet->outlet)) {{ strtoupper($profile->EmploymentDetails->Outlet->outlet) }} @endif</td>
                                        </tr>
                                        {{--@endif--}}

                                        @if(in_array($profile->employmentdetails->position_id, [1, 2]))
                                            <tr>
                                                <td width="110">Coordinator</td>
                                                <td colspan="5">{{ !empty($profile->EmploymentDetails->Coordinator) ? strtoupper($profile->EmploymentDetails->Coordinator->name()) : '' }}</td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <td width="110">SSS No.</td>
                                            <td colspan="5">{{ $profile->Compensation->sss }}</td>
                                        </tr>
                                        <tr>
                                            <td width="110">HDMF No.</td>
                                            <td colspan="5">{{ $profile->Compensation->hdmf }}</td>
                                        </tr>
                                        <tr>
                                            <td width="110">PHIC No.</td>
                                            <td colspan="5">{{ $profile->Compensation->philhealth }}</td>
                                        </tr>
                                        <tr>
                                            <td width="110">Tax ID Number</td>
                                            <td colspan="5">{{ $profile->Compensation->tin }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#users-dt').DataTable({
                "pageLength": 50

            });
        })
    </script>
@endsection

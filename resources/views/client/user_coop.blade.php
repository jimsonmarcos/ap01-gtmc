@extends('layouts.client_app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ url("client/user-profiles") }}"><i class="fa fa-arrow-left"></i> Back to User Profiles</a>
                                    <br>
                                    <br>
                                </div>
                                
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url("client/user-profile/{$profile->id_number}") }}">Personal Information </a>
                                        </li>
                                        @if($profile->User->role != 'Member')
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ url("client/user-profile/{$profile->id_number}/employment-details") }}">Employment Details</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ url("client/user-profile/{$profile->id_number}/compensations-leave-credits") }}">Compensation and Leave Credits</a>
                                            </li>
                                        @endif
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ url("client/user-profile/{$profile->id_number}/loans-shares") }}">Loans and Shares</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12">
                                    <br>
                                    <br>
                                    <p class="text-right">Membership Status: <strong class="@if($profile->status == 'Active') text-success @else text-danger @endif">{{ $profile->status }}</strong></p>

                                    <div class="row no-padding">
                                        <div class="col-md-4">
                                            <h4>Cooperative</h4>
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>Membership Type</td>
                                                    <td>{{ $profile->membership_type }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Cost Center</td>
                                                    <td>{{ !empty($profile->CostCenter->cost_center) ? $profile->CostCenter->cost_center : 'N/A' }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Contribution</td>
                                                    <td>@if(!empty($profile->coop_share)) {{ number_format($profile->coop_share, 2) }} @else 0 @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Membership Fee</td>
                                                    <td>{{ number_format(\App\OtherPayments::where(['user_id' => $profile->user_id, 'transaction_type' => 'MF'])->sum('total_amount_paid'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Notarial Fee</td>
                                                    <td>{{ number_format(\App\OtherPayments::where(['user_id' => $profile->user_id, 'transaction_type' => 'NF'])->sum('total_amount_paid'), 2) }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-4">
                                            <h4>Company</h4>
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>Coop Share</td>
                                                    <td>{{ number_format(\App\OtherPayments::where(['user_id' => $profile->user_id, 'transaction_type' => 'COOP SHARE'])->sum('total_amount_paid'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Coop Savings</td>
                                                    <td>{{ number_format(\App\Loans::where(['user_id' => $profile->user_id, 'releasing_status' => 'Released'])->sum('savings'), 2) }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-4">
                                            <h4>Government</h4>
                                            <table class="table">
                                                <?php $payrollSummary = \App\PayrollSummary::where('user_id', $profile->user_id); ?>
                                                <tbody>
                                                <tr>
                                                    <td class="text-right">SSS EE & ER</td>
                                                    <td class="text-right">{{ number_format($payrollSummary->sum('sss_ee') + $payrollSummary->sum('sss_er'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">HDMF EE & EE</td>
                                                    <td class="text-right">{{ number_format($payrollSummary->sum('hdmf_ee') + $payrollSummary->sum('hdmf_er'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">PhilHealth EE & EE</td>
                                                    <td class="text-right">{{ number_format($payrollSummary->sum('philhealth_ee') + $payrollSummary->sum('philhealth_er'), 2) }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <table class="table table-striped table-sm" id="beneficiaries">
                                            <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Middle Name</th>
                                                <th>Last Name</th>
                                                <th>Birth Date</th>
                                                <th>Relationship to Member</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($profile->Beneficiaries->count() > 0)
                                                @foreach($profile->Beneficiaries as $beneficiary)
                                                    <tr>
                                                        <td>{{ $beneficiary->first_name }}</td>
                                                        <td>{{ $beneficiary->middle_name }}</td>
                                                        <td>{{ $beneficiary->last_name }}</td>
                                                        <td>{{ $beneficiary->birthdate() }}</td>
                                                        <td>{{ $beneficiary->MemberRelationship->title }}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="5">No Beneficiaries yet.</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>

                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#users-dt').DataTable({
                "pageLength": 50

            });
        })
    </script>
@endsection

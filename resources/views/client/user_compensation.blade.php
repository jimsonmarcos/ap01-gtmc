@extends('layouts.client_app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <a href="{{ url("client/user-profiles") }}"><i class="fa fa-arrow-left"></i> Back to User Profiles</a>
                                    <br>
                                    <br>
                                </div>

                                <div class="col-md-12">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url("client/user-profile/{$profile->id_number}") }}">Personal Information </a>
                                        </li>
                                        @if($profile->User->role != 'Member')
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ url("client/user-profile/{$profile->id_number}/employment-details") }}">Employment Details</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="{{ url("client/user-profile/{$profile->id_number}/compensations-leave-credits") }}">Compensation and Leave Credits</a>
                                            </li>
                                        @endif
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url("client/user-profile/{$profile->id_number}/loans-shares") }}">Loans and Shares</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12">
                                    <br>

                                    <h3>Payroll Details</h3>
                                    <div class="row no-padding-bottom">
                                        <div class="col-md-3">
                                            <h4>Compute</h4>
                                            <ul>
                                                <li class="text-{{ $profile->Compensation->compute_sss == 'Y' ? 'success' : 'danger' }}" style="list-style: none;"><span class="fa {{ $profile->Compensation->compute_sss == 'Y' ? 'fa-check' : 'fa-close' }}"></span> SSS No.</li>
                                                <li class="text-{{ $profile->Compensation->compute_hdmf == 'Y' ? 'success' : 'danger' }}" style="list-style: none;"><span class="fa {{ $profile->Compensation->compute_hdmf == 'Y' ? 'fa-check' : 'fa-close' }}"></span> HDMF No.</li>
                                                <li class="text-{{ $profile->Compensation->compute_philhealth == 'Y' ? 'success' : 'danger' }}" style="list-style: none;"><span class="fa {{ $profile->Compensation->compute_philhealth == 'Y' ? 'fa-check' : 'fa-close' }}"></span> PhilHealth No.</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <br>
                                            <br>


                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>Client</td>
                                                    <td>@if(!empty(strtoupper($profile->EmploymentDetails->Principal->company_code))) {{ strtoupper($profile->EmploymentDetails->Principal->company_code) }} @endif</td>
                                                </tr>
                                                @if(!empty($profile->EmploymentDetails->Principal->parent_company_id))
                                                    <tr>
                                                        <td>Parent Client</td>
                                                        <td>@if(!empty($profile->EmploymentDetails->Principal->ParentCompany->company_code)) {{ strtoupper($profile->EmploymentDetails->Principal->ParentCompany->company_code) }} @endif</td>
                                                    </tr>
                                                @endif
                                                <tr>
                                                    <td>Payroll Group</td>
                                                    <td>@if(!empty($profile->Compensation->PayrollGroup->group_name)) {{ strtoupper($profile->Compensation->PayrollGroup->group_name) }} @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Mode of Transfer</td>
                                                    <td>@if(!empty($profile->Compensation->TransferMode->transfer_mode)) {{ strtoupper($profile->Compensation->TransferMode->transfer_mode) }} @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Transfer Type</td>
                                                    <td>@if(!empty($profile->Compensation->TransferMode->transfer_type)) {{ strtoupper($profile->Compensation->TransferMode->transfer_type) }} @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Bank</td>
                                                    <td>@if(!empty($profile->Compensation->TransferMode->bank)) {{ strtoupper($profile->Compensation->TransferMode->bank) }} @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Mobile Number</td>
                                                    <td>{{ strtoupper($profile->Compensation->mobile_number) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Account Number</td>
                                                    <td>{{ strtoupper($profile->Compensation->account_number) }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <table class="table" style="max-width: 600px;">
                                        <tbody>
                                        <tr>
                                            <td style="width: 160px;">Tax Type</td>
                                            <td>
                                                {{ !empty($profile->Compensation->tax_type_id) ? $profile->Compensation->TaxType->title : '' }}
                                                @if($profile->Compensation->deduct_withholding == 'N')
                                                    <br>
                                                    <small class="text-success"><span class="fa fa-check"></span>
                                                        Minimum Wage Earner
                                                    </small>
                                                @else
                                                    <br>
                                                    <small class="text-success"><span class="fa fa-check"></span>
                                                        Deduct Withholding Tax
                                                    </small>
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <hr>

                                    <h3>Rate Details</h3>
                                    <table class="table" style="max-width: 600px;">
                                        <tbody>
                                        <tr>
                                            <td width="110">Rate Type</td>
                                            <td width="">{{ $profile->Compensation->rate_type }}</td>
                                            <td width="130">Monthly Rate</td>
                                            <td width="">
                                                @if($profile->Compensation->rate_type == 'Monthly')
                                                    {{ number_format($profile->Compensation->monthly_rate, 2) }}
                                                @else
                                                    @if($profile->Compensation->daily_category == 'Fixed')
                                                        {{ number_format($profile->Compensation->daily_rate * 26, 2) }}
                                                    @else
                                                        {{ !empty($profile->Compensation->daily_rate_id) ? number_format($profile->Compensation->DailyRate->rate * 26, 2) : '' }}
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="110">Daily Rate <br />Category</td>
                                            <td width="">{{ $profile->Compensation->daily_category }}</td>
                                            <td width="130">Daily Rate</td>
                                            <td width="">
                                                @if($profile->Compensation->daily_category == 'Fixed')
                                                    {{ number_format($profile->Compensation->daily_rate, 2) }}
                                                @else
                                                    {{ !empty($profile->Compensation->daily_rate_id) ? number_format($profile->Compensation->DailyRate->rate, 2) : '' }}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="110">Location</td>
                                            <td width="">{{ !empty($profile->Compensation->daily_rate_id) ? $profile->Compensation->DailyRate->location : '' }}</td>
                                            <td width="130">Hourly Rate</td>
                                            <td width="">
                                                @if($profile->Compensation->rate_type == 'Daily')
                                                    {{ number_format($profile->Compensation->daily_rate / 8, 2) }}
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <hr>

                                    <div style="max-width: 600px;">
                                    </div>
                                    <h4>Allowances</h4>
                                    <table class="table" style="max-width: 600px;">
                                        <thead>
                                        <tr>
                                            <th>Allowance</th>
                                            <th>Taxable</th>
                                            <th>Amount</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($profile->Compensation->daily_category == 'Location')
                                            <tr>
                                                <td>ECOLA</td>
                                                <td>Taxable</td>
                                                <td>{{ !empty($profile->Compensation->DailyRate->ecola) ? number_format($profile->Compensation->DailyRate->ecola, 2) : '' }}</td>
                                                <td></td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <td>FIXED ALLOWANCE</td>
                                            <td>Non-Taxable</td>
                                            <td>{{ number_format($profile->Compensation->fixed_allowance, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>DE MINIMIS</td>
                                            <td>Non-Taxable</td>
                                            <td>{{ number_format($profile->Compensation->de_minimis, 2) }}</td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <hr>

                                    <h3>Leave Credits</h3>
                                    <table class="table" style="max-width: 600px;">
                                        <tbody>
                                        <tr>
                                            <td width="40%">YTD Leave Credits</td>
                                            <td width="60">Days</td>
                                            <td>@if(\Carbon\Carbon::createFromFormat('Y-m-d', $profile->employmentdetails->hire_date)->age > 0) 5 @else 0 @endif</td>
                                            <td width="60">Hrs</td>
                                            <td>
                                                @if(\Carbon\Carbon::createFromFormat('Y-m-d', $profile->employmentdetails->hire_date)->age > 0) {{ 5 * 8 }} @else 0 @endif
                                            <!-- {{  floor(floor(((12 - date('m', strtotime($profile->employmentdetails->hire_date))) / 12) * 5) + ((date('Y') - date('Y', strtotime($profile->employmentdetails->hire_date))) * 5)) * 8 }} -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>YTD Accumulated Leave</td>
                                            <td>Days</td>
                                            <td>{{ $leaveHistory->where('status', 'Paid')->sum('days') }}</td>
                                            <td>Hrs</td>
                                            <td>{{ $leaveHistory->where('status', 'Paid')->sum('hours') }}</td>
                                        </tr>
                                        <tr>
                                            <td>YTD Leave Balance</td>
                                            <td>Days</td>
                                            <td>@if(\Carbon\Carbon::createFromFormat('Y-m-d', $profile->employmentdetails->hire_date)->age > 0 && $leaveHistory->where('status', 'Paid')->sum('days') < 5) {{ 5 - $leaveHistory->where('status', 'Paid')->sum('days') }} @else 0 @endif</td>
                                            <td>Hrs</td>
                                            <td>@if(\Carbon\Carbon::createFromFormat('Y-m-d', $profile->employmentdetails->hire_date)->age > 0 && $leaveHistory->where('status', 'Paid')->sum('hours') < 40) {{ (5 * 8) - $leaveHistory->where('status', 'Paid')->sum('hours') }} @else 0 @endif</td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <h4>Leave History</h4>

                                    <div style="max-width: 600px; margin-top: 10px;">
                                        <table id="leaveHistory" class="table">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Leave Type</th>
                                                <th>Days</th>
                                                <th>Hrs</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($leaveHistory->count() > 0)
                                            @foreach($leaveHistory as $leave)
                                                <tr>
                                                    <td>{{ date('m/d/Y', strtotime($leave->payroll_date)) }}</td>
                                                    <td>{{ $leave->leave_type }}</td>
                                                    <td>{{ $leave->day }}</td>
                                                    <td>{{ $leave->hours }}</td>
                                                    <td>{{ $leave->status }}</td>
                                                </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td colspan="5">No leave yet.</td>
                                            </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#users-dt').DataTable({
                "pageLength": 50

            });
        })
    </script>
@endsection

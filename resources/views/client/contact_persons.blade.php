@extends('layouts.client_app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Complete Name</th>
                                            <th>Position</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Office Number</th>
                                            <th>Local Number</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($client->ContactPersons->count() > 0)
                                            @foreach($client->ContactPersons as $contactPerson)
                                                <tr>
                                                    <td>{{ $contactPerson->name() }}</td>
                                                    <td>{{ $contactPerson->Position->title }}</td>
                                                    <td>{{ $contactPerson->email }}</td>
                                                    <td>
                                                        {{ $contactPerson->mobile1 }}
                                                        @if(!empty($contactPerson->mobile2)) <br> {{ $contactPerson->mobile2 }} @endif
                                                    </td>
                                                    <td>{{ $contactPerson->office_number }}</td>
                                                    <td>{{ $contactPerson->location }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5">No contact persons yet.</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#dt').DataTable({

            });
        })
    </script>
@endsection

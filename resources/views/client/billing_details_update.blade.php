@extends('layouts.client_app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Update Billing Details</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <form action="#" method="POST">
                                    {{ csrf_field()  }}
                                    <div class="form-group">
                                        <label class="col-xs-12" for="admin_type">Admin Type</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="admin_type" name="admin_type" size="1">
                                                <option value="1">Admin Type</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="admin_fees_number">Admin Fees #</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="admin_fees_number" name="admin_fees_number" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="admin_fees">Admin Fees</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="admin_fees" name="admin_fees" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="sss_number">SSS Number</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="sss_number" name="sss_number" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="hdmf_number">HDMF Number</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="hdmf_number" name="hdmf_number" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="philhealth">PhilHealth</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="philhealth" name="philhealth" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="tin">TIN</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="tin" name="tin" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12 text-right">
                                            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-check push-5-r"></i> Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

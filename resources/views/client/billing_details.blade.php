@extends('layouts.client_app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Billing</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                {{--<div class="col-md-12 text-right">--}}
                                    {{--<a class="btn btn-minw btn-warning" href="#">Edit</a>--}}
                                {{--</div>--}}

                                <div class="col-md-3">
                                    <h3 class="block-title">Company Name</h3>
                                    <h3 class="block-title">Company Code</h3>
                                    <h3 class="block-title">Company Address</h3>
                                    <h3 class="block-title">Trunkline</h3>
                                    <h3 class="block-title">Fax Number</h3>
                                </div>

                                <div class="col-md-9">
                                    <h3 class="block-title">{{ $user->Company->company_name }}</h3>
                                    <h3 class="block-title">{{ $user->Company->company_code ? $user->Company->company_code : 'N/A' }}</h3>
                                    <h3 class="block-title">{{ $user->Company->address ? $user->Company->address : 'N/A' }}</h3>
                                    <h3 class="block-title">{{ $user->Company->trunk_line ? $user->Company->trunk_line : 'N/A' }}</h3>
                                    <h3 class="block-title">{{ $user->Company->fax ? $user->Company->fax : 'N/A' }}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="fa fa-users"></span> Billing Details</a>
                                    </li>
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link" href="{{ url('/admin/transactions/invoices')  }}"><span class="fa fa-user-plus"></span> Invoices</a>--}}
                                    {{--</li>--}}
                                </ul>

                                <br>

                                <div class="col-md-12 text-right">
                                    <a class="btn btn-minw btn-warning" href="{{ route('client_billing_details_update') }}">Update Details</a>
                                </div>

                                <div class="col-md-3">
                                    <h3 class="block-title">Admin Type</h3>
                                    <h3 class="block-title">Admin Fee #</h3>
                                    <h3 class="block-title">Admin Fees</h3>
                                    <h3 class="block-title">SSS Number</h3>
                                    <h3 class="block-title">HDMF Number</h3>
                                    <h3 class="block-title">PhilHealth</h3>
                                    <h3 class="block-title">TIN</h3>
                                </div>

                                <div class="col-md-9">
                                    <h3 class="block-title">Admin Type</h3>
                                    <h3 class="block-title">Admin Fee #</h3>
                                    <h3 class="block-title">Admin Fees</h3>
                                    <h3 class="block-title">SSS Number</h3>
                                    <h3 class="block-title">HDMF Number</h3>
                                    <h3 class="block-title">PhilHealth</h3>
                                    <h3 class="block-title">TIN</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

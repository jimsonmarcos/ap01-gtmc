@extends('layouts.client_app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Add Payroll Groups</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <form action="#" method="POST">
                                    {{ csrf_field()  }}
                                    <div class="form-group">
                                        <label class="col-xs-12" for="payroll_group_name">Payroll Group Name</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="payroll_group_name" name="payroll_group_name" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Start Date</label>
                                        <div id="joinDate" class="input-group btn-mmddyyyy">
                                            <input type="text" class="form-control" name="start_date" placeholder="mm/dd/yyyy" required="">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">End Date</label>
                                        <div id="joinDate" class="input-group btn-mmddyyyy">
                                            <input type="text" class="form-control" name="end_date" placeholder="mm/dd/yyyy" required="">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12 text-right">
                                            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-check push-5-r"></i> Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

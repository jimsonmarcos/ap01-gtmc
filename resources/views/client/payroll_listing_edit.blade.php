@extends('layouts.client_app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            <form action="" method="POST">
                                {{ csrf_field() }}

                                <div class="row">

                                    <div class="col-md-12">

                                        <a href="{{ url("/client/payroll-listings") }}"><i class="fa fa-arrow-left"></i> Back to Payroll Listing</a>

                                        <br>
                                        <br>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-control-label">Mode of Transfer</label>
                                                <select id="comp-transfermode" name="" class="form-control">
                                                    @foreach($transferModes as $transferMode)
                                                        <option @if(!empty($profile->Compensation->TransferMode->transfer_mode) && $profile->Compensation->TransferMode->transfer_mode == $transferMode->transfer_mode) selected @endif>{{ $transferMode->transfer_mode }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-control-label">Transfer Type</label>
                                                <select name="" id="comp-transfertype" class="form-control">
                                                    @foreach($transferTypes as $transferType)
                                                        <option @if(!empty($profile->Compensation->TransferMode->transfer_type) && $profile->Compensation->TransferMode->transfer_type == $transferType->transfer_type) selected @endif>{{ $transferType->transfer_type }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-control-label">Bank</label>
                                                <select class="form-control" name="transfer_mode_id" id="comp-bank">
                                                    @foreach($banks as $bank)
                                                        <option value="{{ $bank->id }}" @if(!empty($profile->Compensation->TransferMode->bank) && $profile->Compensation->TransferMode->bank == $bank->bank) selected @endif>{{ $bank->bank }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-control-label">Mobile Number</label>
                                                <input id="comp-mobilenumber" type="text" name="mobile_number" class="form-control" maxlength="11" value="{{ $profile->Compensation->mobile_number }}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-control-label">Account Number</label>
                                                <input type="text" id="accountNumber" name="account_number" class="form-control" value="{{ $profile->Compensation->account_number }}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                            </div>
                                        </div>

                                        @if($profile->EmploymentDetails->principal_id == 8 && in_array($profile->EmploymentDetails->position_id, [1, 2]))
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Coordinator</label>
                                                    <select class="form-control" name="coordinator_id">
                                                        <option value=""></option>
                                                        @foreach($coordinators as $coordinator)
                                                            <option value="{{ $coordinator->user_id }}" @if($profile->EmploymentDetails->coordinator_id == $coordinator->user_id) selected @endif>{{ $coordinator->Profile->name() }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        @endif

                                    </div>

                                    <div class="col-md-12">
                                        <br>
                                        <div class="col-xs-12">
                                            <button class="btn btn-primary"><i class="fa fa-save"></i> Submit</button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(document).on('change', '#comp-transfermode', function(e) {
                e.preventDefault();

                if($(this).find(':selected').val() == 'BANK DEPOSIT' || $("#comp-transfermode").find(':selected').text() == 'BDO' || $("#comp-transfermode").find(':selected').text() == 'METROBANK' || $("#comp-transfermode").find(':selected').text() == 'UNIONBANK') {
                    $("#accountNumber").attr({ 'required' : true });
                } else {
                    $("#accountNumber").attr({ 'required' : false });
                }

                if($("#comp-transfermode").find(':selected').text() == 'GCASH') {
                    $("#comp-mobilenumber").prop('required', true);
                } else {
                    $("#comp-mobilenumber").prop('required', false);
                }

                $("#comp-transfertype, #comp-bank").find('option').remove();
                $("#comp-transfertype, #comp-bank").attr('disabled', 'disabled');
                $("#comp-transfertype").append("<option>Loading..</option>");

                var data = {
                    _token : '{{ csrf_token() }}',
                    transfer_mode : $(this).find(':selected').text()
                };
//               console.log($(this).find(':selected').text());
                $.post('{{ url("api/transfer-mode/types") }}', data, function(response) {
//                  console.log(response);
                    var transfer_types = $.parseJSON(response);
                    var option = '';
                    $.each(transfer_types, function(index, json) {
                        option = option + '<option>'+ json.transfer_type +'</option>';
                    });

                    $("#comp-transfertype > option").remove();
                    $("#comp-transfertype").append(option);
                    $("#comp-transfertype").removeAttr('disabled');

                    setTimeout(function() {
                        reloadTransferModes();
                    }, 300);
                });
            });

            $(document).on('change', '#comp-transfertype', function(e) {
                e.preventDefault();

                $("#comp-bank").find('option').remove();
                $("#comp-bank").attr('disabled', 'disabled');
                $("#comp-bank").append("<option>Loading..</option>");

                var data = {
                    _token : '{{ csrf_token() }}',
                    transfer_mode : $('#comp-transfermode').find(':selected').text(),
                    transfer_type : $(this).find(':selected').text()
                };

                $.post('{{ url("api/transfer-mode/banks") }}', data, function(response) {

                    var banks = $.parseJSON(response);
                    var option = '';
                    $.each(banks, function(index, json) {
                        option = option + '<option>'+ json.bank +'</option>';
                    });

                    $("#comp-bank > option").remove();
                    $("#comp-bank").append(option);
                    $("#comp-bank").removeAttr('disabled');

                    setTimeout(function() {
                        reloadTransferModes();
                    }, 300);
                });
            });

            reloadTransferModes();

            function reloadTransferModes() {
                if ($("#comp-transfertype option").length > 0 && $("#comp-bank option").length == 0) {

                    if ($('#comp-transfertype').find(':selected').text() != 'Loading..') {
                        $("#comp-bank").find('option').remove();
                        $("#comp-bank").attr('disabled', 'disabled');
                        $("#comp-bank").append("<option>Loading..</option>");

                        var data = {
                            _token : '{{ csrf_token() }}',
                            transfer_mode : $('#comp-transfermode').find(':selected').text(),
                            transfer_type : $('#comp-transfertype').find(':selected').text()
                        };

                        $.post('{{ url("api/transfer-mode/banks") }}', data, function(response) {

                            var banks = $.parseJSON(response);
                            var option = '';
                            $.each(banks, function(index, json) {
                                option = option + '<option value="'+ json.id +'">'+ json.bank +'</option>';
                            });

                            $("#comp-bank > option").remove();
                            $("#comp-bank").append(option);
                            $("#comp-bank").removeAttr('disabled');

                            setTimeout(function() {
                                reloadTransferModes();
                            }, 300);
                        });
                    }
                }
            }
        })
    </script>
@endsection

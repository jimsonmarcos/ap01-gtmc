@extends('layouts.client_app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                    <div class="col-md-12">
                                        <br />

                                        <table id="users-dt" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Payroll Code</th>
                                                <th>Company Code</th>
                                                <th>Payroll Period</th>
                                                <th>Payroll Date</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($client->PayrollCycles() as $payrollCycle)
                                                <tr>
                                                    <td><a href="{{ url("print/payslips/{$payrollCycle->id}") }}" target="_blank">{{ $payrollCycle->payroll_code }}</a></td>
                                                    <td>{{ $client->company_code }}</td>
                                                    <td>{{ $payrollCycle->payroll_period }}</td>
                                                    <td>{{ date('m/d/Y', strtotime($payrollCycle->payroll_date)) }}</td>
                                                    <td>{{ $payrollCycle->status }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#users-dt').DataTable({
                "pageLength": 50,
                "order": [[ 0, "desc" ]]

            });
        })
    </script>
@endsection

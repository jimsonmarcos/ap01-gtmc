@extends('layouts.client_app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Add Cost Center</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <form action="#" method="POST">
                                    {{ csrf_field()  }}
                                    <div class="form-group">
                                        <label class="col-xs-12" for="cost_center">Cost Center</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="cost_center" name="cost_center" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="description">Description</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="description" name="description" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12 text-right">
                                            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-check push-5-r"></i> Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

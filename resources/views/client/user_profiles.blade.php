@extends('layouts.client_app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link @if($status == 'Active') active @endif" href="{{ url("client/user-profiles/Active") }}"><span class="fa fa-users"></span> Active Users <span class="badge badge-primary">{{ $usersActive }}</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if($status == 'Inactive') active @endif" href="{{ url("client/user-profiles/Inactive") }}"><span class="fa fa-users"></span> Inactive Users <span class="badge badge-primary"> {{ $usersInactive }}</span></a>
                                        </li>
                                    </ul>
                                    <br />

                                    <table id="users-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Complete Name</th>
                                            <th>ID Number</th>
                                            <th>Membership Category</th>
                                            <th>Gender</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($profiles as $profile)
                                            <tr>
                                                <td>{{ "{$profile->last_name}, {$profile->first_name}" }}</td>
                                                <td>{{ $profile->id_number }}</td>
                                                <td>{{ $profile->membership_category }}</td>
                                                <td>{{ $profile->gender }}</td>
                                                <td><a href="{{ url("/client/user-profile/{$profile->id_number}") }}" class="btn btn-primary"><i class="fa fa-user"> VIEW</i></a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#users-dt').DataTable({
                "pageLength": 100

            });
        })
    </script>
@endsection

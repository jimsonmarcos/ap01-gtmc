@extends('layouts.client_app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                    <div class="col-md-12">
                                        <br />

                                        <table id="users-dt" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Invoice Number</th>
                                                <th>Invoice Date</th>
                                                <th>Principal</th>
                                                <th>Contact Email</th>
                                                <th>Billing Amount</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($client->Invoices as $invoice)
                                                <tr>
                                                    <td>{{ $invoice->invoice_number }}</td>
                                                    <td>{{ date('m/d/Y', strtotime($invoice->invoice_date)) }}</td>
                                                    <td>{{ $client->company_code }}</td>
                                                    <td>{{ !empty($invoice->ContactPerson->email) ? $invoice->ContactPerson->email : '' }}</td>
                                                    <td>{{ number_format($invoice->total_amount, 2) }}</td>
                                                    <td>{{ $invoice->status }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#users-dt').DataTable({
                "pageLength": 20,
                "order": [[ 0, "desc" ]]

            });
        })
    </script>
@endsection

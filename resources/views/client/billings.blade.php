@extends('layouts.client_app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <br />
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Admin Fee Type</label>
                                            <select name="admin_fee_type" class="form-control" disabled>
                                                <option value="">No selected type yet</option>
                                                <option value="Percentage" @if($client->admin_fee_type == 'Percentage') selected @endif>Percentage</option>
                                                <option value="Fixed" @if($client->admin_fee_type == 'Fixed') selected @endif>Fixed</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Admin Fee %</label>
                                            <input id="searchEmail" type="text" class="form-control" value="@if($client->admin_fee_type == 'Percentage') {{ $client->admin_fee_percentage }}% @endif" placeholder="" disabled />
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Admin Fee</label>
                                            <input id="searchEmail" type="text" class="form-control" value="@if($client->admin_fee_type == 'Percentage') {{ number_format($client->admin_fee, 2) }} @endif" placeholder="" disabled />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

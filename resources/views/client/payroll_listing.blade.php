@extends('layouts.client_app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                    <div class="col-md-12">

                                        <form action="" id="payrollListingForm" method="GET">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">Payroll Group</label>
                                                        <select name="payroll_group_id" id="payrollGroup" class="form-control">
                                                            <option value="">All</option>
                                                            @foreach($client->PayrollGroups as $payrollGroup)
                                                                <option value="{{ $payrollGroup->id }}" @if($payrollGroupId == $payrollGroup->id) selected @endif>{{ $payrollGroup->group_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                {{--<div class="col-md-3">--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<label for="">Coordinator</label>--}}
                                                        {{--<select name="" id="" class="form-control">--}}
                                                            {{--<option value="">All</option>--}}
                                                            {{--@foreach($client->Coordinators as $coordinator)--}}
                                                                {{--<option value="{{ $coordinator->user_id }}">{{ $payrollGroup->group_name }}</option>--}}
                                                            {{--@endforeach--}}
                                                        {{--</select>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            </div>
                                        </form>

                                        <br>
                                    </div>
                                    <div class="col-md-12">
                                        <br />

                                        <table id="users-dt" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>GTMC ID Number</th>
                                                <th>Employee Name</th>
                                                <th>Mode of Transfer</th>
                                                <th>Transfer Type</th>
                                                <th>Bank</th>
                                                <th>Mobile Number</th>
                                                <th>Account Number</th>
                                                <th>Coordinator</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($profiles as $profile)
                                                <tr>
                                                    <td>{{ $profile->id_number }}</td>
                                                    <td>{{ "{$profile->last_name}, $profile->first_name" }}</td>
                                                    <td>@if(!empty($profile->Compensation->TransferMode)) {{ ($profile->Compensation->TransferMode->transfer_mode == 'PICK UP') ? 'PICK UP' : 'DEPOSIT' }} @endif</td>
                                                    <td>@if(!empty($profile->Compensation->TransferMode)) {{ $profile->Compensation->TransferMode->transfer_type }} @endif</td>
                                                    <td>@if(!empty($profile->Compensation->TransferMode)) {{ ($profile->Compensation->TransferMode->transfer_mode != 'PICK UP') ? $profile->Compensation->TransferMode->transfer_mode : '' }} @endif</td>
                                                    <td>{{ $profile->Compensation->mobile_number }}</td>
                                                    <td>{{ $profile->Compensation->account_number }}</td>
                                                    <td>@if(!empty($profile->EmploymentDetails->Coordinator)) {{ $profile->EmploymentDetails->Coordinator->name() }} @endif</td>
                                                    <td><a href="{{ url("/client/payroll-listings/edit/{$profile->id_number}") }}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#users-dt').DataTable({
                "lengthMenu": [[ 20, 50, 100, 200, -1], [20, 50, 100, 200, 'All']],
                "pageLength": 20,
                "order": [[ 0, "desc" ]]
            });

            $("#payrollGroup").change(function() {
                $("#payrollListingForm").submit();
            });
        })
    </script>
@endsection

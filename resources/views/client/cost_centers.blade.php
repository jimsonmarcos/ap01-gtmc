@extends('layouts.client_app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <br>

                                    @if(empty($client->parent_company_id))
                                        <h2>Payroll Groups</h2>
                                        <br>
                                        <br>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Payroll Group</th>
                                                <th>Start Date</th>
                                                <th>Payroll Date A</th>
                                                <th>Payroll Date B</th>
                                                <th>Payroll Group Def.</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($client->PayrollGroups->count() > 0)
                                                @foreach($client->PayrollGroups as $payrollGroup)
                                                    <tr>
                                                        <td>{{ $payrollGroup->group_name }}</td>
                                                        <td>{{ $payrollGroup->start_date_a }}</td>
                                                        <td>{{ $payrollGroup->payroll_date_a }}</td>
                                                        <td>{{ $payrollGroup->payroll_date_b }}</td>
                                                        <td>{{ $payrollGroup->group_definition }}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6">No Payroll Groups yet.</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>

                                        <br>
                                        <hr>
                                        <br>

                                    @else

                                        <h2>Payroll Groups</h2>

                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Payroll Group</th>
                                                <th>Start Date</th>
                                                <th>Payroll Date A</th>
                                                <th>Payroll Date B</th>
                                                <th>Payroll Group Def.</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($client->ParentCompany->PayrollGroups->count() > 0)
                                                @foreach($client->ParentCompany->PayrollGroups as $payrollGroup)
                                                    <tr>
                                                        <td>{{ $payrollGroup->group_name }}</td>
                                                        <td>{{ $payrollGroup->start_date_a }}</td>
                                                        <td>{{ $payrollGroup->payroll_date_a }}</td>
                                                        <td>{{ $payrollGroup->payroll_date_b }}</td>
                                                        <td>{{ $payrollGroup->group_definition }}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6">No Payroll Groups yet.</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>

                                    @endif

                                    <h2>Cost Center</h2>
                                    <br>
                                    <br>
                                    <table class="table" style="max-width: 600px;">
                                        <thead>
                                        <tr>
                                            <th width="150">Cost Center</th>
                                            <th>Group Definition</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($client->CostCenters->count() > 0)
                                            @foreach($client->CostCenters as $costCenter)
                                                <tr>
                                                    <td>{{ $costCenter->cost_center }}</td>
                                                    <td>{{ $costCenter->group_definition }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="2">No Cost Centers yet.</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#dt').DataTable({

            });
        })
    </script>
@endsection

@extends('layouts.client_app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Add Contact Persons</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <form action="#" method="POST">
                                    {{ csrf_field()  }}
                                    <div class="col-md-12">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="first_name">First Name</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="first_name" name="first_name" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="middle_name">Middle Name</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="middle_name" name="middle_name" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="last_name">Last Name</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="last_name" name="last_name" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="position">Position</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="position" name="position" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="mobile_1">Mobile 1</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="mobile_1" name="mobile_1" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="mobile_2">Mobile 2</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="mobile_2" name="mobile_2" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="office_number">Office Number</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="office_number" name="office_number" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="local">Local</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="local" name="local" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12 text-right">
                                            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-check push-5-r"></i> Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

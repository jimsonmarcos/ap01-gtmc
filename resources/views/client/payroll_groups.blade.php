@extends('layouts.client_app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll Groups</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                {{--<div class="col-md-12 text-right">--}}
                                    {{--<a class="btn btn-minw btn-warning" href="#">Edit</a>--}}
                                {{--</div>--}}

                                <div class="col-md-3">
                                    <h3 class="block-title">Company Name</h3>
                                    <h3 class="block-title">Company Code</h3>
                                    <h3 class="block-title">Company Address</h3>
                                    <h3 class="block-title">Trunkline</h3>
                                    <h3 class="block-title">Fax Number</h3>
                                </div>

                                <div class="col-md-9">
                                    <h3 class="block-title">{{ $user->Company->company_name }}</h3>
                                    <h3 class="block-title">{{ $user->Company->company_code ? $user->Company->company_code : 'N/A' }}</h3>
                                    <h3 class="block-title">{{ $user->Company->address ? $user->Company->address : 'N/A' }}</h3>
                                    <h3 class="block-title">{{ $user->Company->trunk_line ? $user->Company->trunk_line : 'N/A' }}</h3>
                                    <h3 class="block-title">{{ $user->Company->fax ? $user->Company->fax : 'N/A' }}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="fa fa-users"></span> Payroll Groups</a>
                                    </li>
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link" href="{{ route('client_payroll_groups_add')  }}"><span class="fa fa-user-plus"></span> Add Payroll Groups</a>--}}
                                    {{--</li>--}}
                                </ul>

                                <br>

                                <table id="dt" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Group Name</th>
                                        <th>Start Date</th>
                                        <th>Payroll Date A</th>
                                        <th>Payroll Date B</th>
                                        <th>Definition</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($user->Company->PayrollGroups as $payrollGroup)
                                            <tr>
                                                <td>{{ $payrollGroup->group_name }}</td>
                                                <td>{{ $payrollGroup->start_date_a }}</td>
                                                <td>{{ $payrollGroup->payroll_date_a }}</td>
                                                <td>{{ $payrollGroup->payroll_date_b }}</td>
                                                <td>{{ $payrollGroup->group_definition }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#dt').DataTable({

            });
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Settings</li>
        <li><a class="link-effect" href="{{ url('settings/change-password')}}">Change Password</a></li>
    </ol>
     <!-- Dashboard Counts Section-->
     <section class="dashboard-counts no-padding-bottom">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-sm-12">
                     <div class="card">
                         <div class="card-body">
                             <div class="row">
                                 <div class="col-md-4 col-md-offset-4">
                                     <br>

                                     <form action="" method="POST" enctype="multipart/form-data">
                                         {{ csrf_field() }}

                                         {{-- This must be select2 --}}
                                         <div class="form-group">
                                             <label class="form-control-label">Name</label>
                                             <input id="name" type="text" class="form-control" name="name" placeholder="">
                                         </div>

                                         <div class="form-group">
                                             <div class="col-xs-12 text-center">
                                                 <br />
                                                 <button class="btn btn-sm btn-danger"><i class="fa fa-save"></i> RESET PASSWORD</button>
                                             </div>
                                         </div>
                                     </form>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </section>
@endsection

@section('extendedscript')
<script>
    $(document).ready(function() {

    })
</script>
@endsection

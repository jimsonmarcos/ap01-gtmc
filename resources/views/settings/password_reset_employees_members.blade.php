@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{ url('/admin/manage-admin/users') }}"><i class="fa fa-arrow-left"></i> Back to User Admin</a>
        </div>
    </div>
    <ol class="breadcrumb push-10-t">
        <li>Settings</li>
        <li><a class="link-effect" href="{{ url('settings/change-password')}}">Change Password</a></li>
    </ol>
     <!-- Dashboard Counts Section-->
     <section class="dashboard-counts no-padding-bottom">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-sm-12">
                     <div class="card">
                         <div class="card-body">
                             <div class="row">
                                 <div class="col-md-4 col-md-offset-4">
                                     <br>

                                     <div class="form-group">
                                         <label class="form-control-label">Select a person</label>
                                         <select name="admin" id="selectUserAccess" class="form-control">
                                             <option value="" disabled @if(empty($userAccess)) selected @endif>Select</option>
                                             <option value="Admin" @if(!empty($userAccess) && $userAccess == 'Admin') selected @endif>Admin</option>
                                             <option value="Employee" @if(!empty($userAccess) && $userAccess == 'Employee') selected @endif>Employee</option>
                                             <option value="Member" @if(!empty($userAccess) && $userAccess == 'Member') selected @endif>Member</option>
                                         </select>
                                     </div>

                                     @if(!empty($userAccess))
                                         <form action="" method="POST" enctype="multipart/form-data">
                                             {{ csrf_field() }}

                                             <div class="form-group">
                                                 <label class="form-control-label">Name</label>
                                                 <select name="id" id="users" class="form-control">
                                                     @foreach($users as $user)
{{--                                                         @if(!empty($user->Profile))--}}
                                                            <option value="{{ $user->id }}">{{ !empty($user->Profile) ? $user->Profile->name() : $user->name }}</option>
                                                         {{--@endif--}}
                                                     @endforeach
                                                 </select>
                                             </div>

                                             <div class="form-group">
                                                 <div class="col-xs-12 text-center">
                                                     <br />
                                                     <button class="btn btn-sm btn-danger"><i class="fa fa-save"></i> RESET PASSWORD</button>
                                                 </div>
                                             </div>
                                         </form>
                                     @endif

                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </section>
@endsection

@section('extendedscript')
<script>
    $(document).ready(function() {
        var base = "{{ url('/admin/super/employees-members/password_reset') }}";

        $("#selectUserAccess").change(function (e) {
            location.href = base + "/" + $(this).find(':selected').val();
        })

        $('#users').select2();

        $("#users").on("select2:select", function (evt) {
            var data = evt.params.data;
            var address = data.element.dataset.address;
            if(address != '') {
                $("#payee_address").val(address);
            }
        });
    })
</script>
@endsection

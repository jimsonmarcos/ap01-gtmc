@extends($layout)

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Settings</li>
        <li><a class="link-effect" href="{{ url('settings/change-password')}}">Change Password</a></li>
    </ol>
     <!-- Dashboard Counts Section-->
     <section class="dashboard-counts no-padding-bottom">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-sm-12">
                     <div class="card">
                         <div class="card-body">
                             <div class="row">
                                 <div class="col-md-4 col-md-offset-4">
                                     <br>

                                     <form action="" method="POST" enctype="multipart/form-data">
                                         {{ csrf_field() }}

                                         {{--<div class="form-group">--}}
                                             {{--<label class="col-xs-12" for="old_password">Old Password</label>--}}
                                             {{--<div class="col-xs-12">--}}
                                                 {{--<input class="form-control" type="password" id="old_password" name="old_password" placeholder="" minlength="4" required="">--}}
                                             {{--</div>--}}
                                         {{--</div>--}}

                                         <div class="form-group">
                                             <label class="col-xs-12" for="new_password">New Password</label>
                                             <div class="col-xs-12">
                                                 <input class="form-control" type="password" id="new_password" name="new_password" placeholder="" minlength="6" required="">
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <label class="col-xs-12" for="confirm_password">Confirm New Password</label>
                                             <div class="col-xs-12">
                                                 <input class="form-control" type="password" id="confirm_password" name="confirm_password" placeholder="" minlength="6" required="">
                                             </div>
                                         </div>

                                         <div class="form-group">
                                             <div class="col-xs-12 text-center">
                                                 <br />
                                                 <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> SUBMIT</button>
                                             </div>
                                         </div>
                                     </form>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </section>
@endsection

@section('extendedscript')
<script>
    $(document).ready(function() {

    })
</script>
@endsection

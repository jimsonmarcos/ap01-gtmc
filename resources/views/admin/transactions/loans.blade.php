@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('loans')}}">Loans</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="{{ route('loans_request') }}" class="btn btn-primary">Request <span class="fa fa-paperclip" style="font-size: 0.8em;"></span></a>
                                    <a href="{{ route('loans_voucher') }}" class="btn btn-primary">Voucher <span class="fa fa-ticket" style="font-size: 0.8em;"></span></a>
                                </div>

                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/transactions/loans/status/Request') }}"><span class="badge badge-info">{{ \App\Loans::all()->count() }}</span> Request</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/transactions/loans/status/Approved') }}"><span class="badge badge-success">{{ \App\Loans::where('status', 'Approved')->count() }}</span> Approved</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/transactions/loans/status/Cancelled') }}"><span class="badge badge-danger">{{ \App\Loans::where('status', 'Cancelled')->count() }}</span> Cancelled</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/transactions/loans/status/Pending') }}"><span class="badge badge-warning">{{ \App\Loans::where('status', 'Pending')->count() }}</span> Pending</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/transactions/loans/status/Ongoing') }}"><span class="badge badge-warning">{{ \App\Loans::where(['status' => 'Approved', 'releasing_status' => 'Released'])->join(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id', 'INNER')->whereRaw("((COALESCE(granted_loan_amount, 0) - COALESCE(total_amount_paid, 0)) > 0)")->count() }}</span> Ongoing</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/transactions/loans/status/Completed') }}"><span class="badge badge-warning">{{ \App\Loans::where(['status' => 'Approved', 'releasing_status' => 'Released'])->leftJoin(\Illuminate\Support\Facades\DB::raw("(SELECT loan_id, SUM(total_amount_paid) as total_amount_paid FROM loan_payments GROUP BY loan_id) loan_payments"), 'loan_payments.loan_id', '=', 'loans.id')->whereRaw("((granted_loan_amount - COALESCE(total_amount_paid, 0)) <= 0)")->count() }}</span> Completed</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 hidden">
                                    <div class="form-group">
                                        <label class="col-xs-12" for="column_name">Column Name</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="column_name" name="column_name" size="1">
                                                <option value="CLA NAME">CLA Name</option>
                                                <option value="EMPLOYEE ID NUMBER">Employee ID Number</option>
                                                <option value="EMPLOYEE NAME">COMPLETE NAME</option>
                                                <option value="COST CENTER">Cost Center</option>
                                                <option value="PRINICIPAL">Principal</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 hidden">
                                    <div class="form-group">
                                        <label class="col-xs-12" for="loan_status">Loan Status</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="loan_status" name="loan_status" size="1">
                                                <option value="PENDING">Pending</option>
                                                <option value="APPROVED">Approved</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-md-offset-8 push-20-t">
                                    <div class="form-group">
                                        <label class="col-sm-12 form-control-label hidden">Column Name: </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="searchQuery" />
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary" id="searchButton"><i class="fa fa-search" aria-hidden="true"></i> </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <table id="loans" class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>CLA Number</th>
                                                    <th>ID Number</th>
                                                    <th>Complete Name</th>
                                                    <th>Cost Center</th>
                                                    <th>Loan Type</th>
                                                    <th>Overdue Amount</th>
                                                    <th>Outstanding Balance</th>
                                                    <th>Granted Loan Amount</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>
                                                    <th class="hidden"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {{--<tr>--}}
                                                    {{--<td>1234</td>--}}
                                                    {{--<td>1234</td>--}}
                                                    {{--<td>1234</td>--}}
                                                    {{--<td>1234</td>--}}
                                                    {{--<td>1234</td>--}}
                                                    {{--<td>1234</td>--}}
                                                    {{--<td>1234</td>--}}
                                                    {{--<td class="text-center">--}}
                                                        {{--<a href="{{ route('loans_details') }}" class="btn btn-info btn-xs">UPDATE</a>--}}
                                                        {{--<a href="{{ route('disbursed_loans') }}" class="btn btn-success btn-xs">RELEASE</a>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            var table = $('#loans').DataTable({
                "order": [[ 0, "desc" ]],
                "pageLength": 20,
                "processing": true,
                "serverSide": true,
                "bFilter": false,
                "searching": true,
                "dom": "ltip",  // Remove global search box
                "ajax":{
                    "url": "{{ url('api/datatables/loans/'.$status) }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "cla_number" },
                    { "data": "id_number" },
                    { "data": "name" },
                    { "data": "cost_center" },
                    { "data": "loan_type" },
                    { "data": "total_overdue_amount" },
                    { "data": "outstanding_balance" },
                    { "data": "granted_loan_amount" },
                    { "data": "status" },
                    { "data": "action" },
                    { "data": "principal"}
                ]
            });

            $('#searchButton').on( 'click', function () {
                var q = $("#searchQuery").val();
                table.search( q ).draw();
            });

            $('#searchQuery').on('keypress', function (e) {
                if(e.which === 13){

                    var q = $("#searchQuery").val();
                    table.search( q ).draw();
                }
            });
        })
    </script>
@endsection

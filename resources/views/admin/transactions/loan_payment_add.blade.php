@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ url('admin/transactions/loans')}}">Loans</a></li>
        <li><a class="link-effect" href="{{ url("admin/transactions/loans/{$loan->id}")}}">Loan Details</a></li>
        <li><a class="link-effect" href="{{ url("admin/transactions/loans/{$loan->id}/payments")}}">Loan Payments</a></li>
        <li>Add Payment</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10">



                    <div class="row">
                        <form id="form-deposit" action="" class="form-horizontal push-10-t push-10" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="push-20 push-10-t">
                                <a href="{{ url("admin/transactions/loans/{$loan->id}/payments")}}"><i class="fa fa-chevron-left"></i> Back to Payments</a>
                            </div>



                            <div class="col-md-4 col-md-offset-2">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="transaction_type">Transaction Type</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="transaction_type" name="transaction_type" disabled>
                                                    @if($loan->is_voucher == 'N')
                                                        <option value="coop_loan">COOP LOAN</option>
                                                    @else
                                                        <option value="loan_voucher">LOAN VOUCHER</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="cla_number">CLA Number</label>
                                                <input class="form-control" type="text" id="cla_number" name="cla_number" value="{{$loan->cla_number}}" disabled />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="" for="pay_period">Pay Period</label>
                                                <input type="text" class="form-control" name="pay_period" value="{{$currentPayment->pay_period}}" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="" for="amount">Amount</label>
                                                <input type="number" class="form-control" name="amount" id="amount" value="{{$currentPayment->amortization}}" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="additional_payment">Additional Payment</label>
                                                <input class="form-control" type="number" id="additionalPayment" name="additional_payment"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="bank_account">Bank Account</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="bank_id" name="bank_id" disabled>
                                                    @foreach(\App\Banks::orderBy('bank_account')->get() as $bank)
                                                        <option value="{{ $bank->id }}">{{ $bank->bank_account }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="ar_number">AR Number</label>
                                                <input class="form-control" type="number" id="ar_number" name="ar_number" value="{{ $arNumber }}" required/>
                                            </div>
                                        </div>
                                    </div>







                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="employee_members_name">Employee/Members Name</label>
                                                <input class="form-control" type="text" id="employee_members_name" name="" value="{{$loan->User->name()}}" disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="payment_method">Payment Method</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="payment_method" name="payment_method" required>
                                                    <option value="Cash">CASH</option>
                                                    <option value="Deposit">DEPOSIT</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="form-control-label">Payment Date</label>
                                                <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                    <input type="text" class="form-control" name="payment_date" value="{{ date('m/d/Y') }}" placeholder="mm/dd/yyyy" required>
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="interest"> Interest</label>
                                                <input class="form-control" type="text" id="interest" name="interest" value="{{$currentPayment->lapsed_interest}}" readonly />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="amount_paid"> Total Amount Paid</label>
                                                <input class="form-control" type="number" id="totalAmountPaid" name="total_amount_paid" value="0.00" readonly />
                                                {{--onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')"--}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="reference_number">Reference Number</label>
                                                <input class="form-control" type="text" id="reference_number" name="reference_number" disabled />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="loan_installment_code">Remarks</label>
                                                <textarea name="remarks" rows="5"
                                                          class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>




                                </div>
                            </div>

                            <div class="col-md-12"></div>

                            <div class="col-md-10 text-right">
                                <button class="btn btn-success push-5-r push-10" onclick="return confirm('Are you sure you want to add this payment?')"><i class="fa fa-check"></i> Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(document).on('change', "#payment_method", function() {
                if($(this).find(':selected').val() == 'Deposit') {
                    $("#bank_id, #reference_number").removeAttr('disabled').attr('required', true);
                } else {
                    $("#bank_id, #reference_number").removeAttr('required').attr('disabled', true);
                }
            });

            updateTotalAmountPaid();

            $("#additionalPayment").on('keypress keyup change', function() {
                updateTotalAmountPaid();
            });

            function updateTotalAmountPaid()
            {
                var amount = parseFloat($("#amount").val()) || 0;
                var interest = parseFloat($("#interest").val()) || 0;
                var additionalPayment = parseFloat($("#additionalPayment").val()) || 0;
                $("#totalAmountPaid").val( (amount + interest + additionalPayment).toFixed(2) );
            }
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('invoices')}}">Invoices</a></li>
    </ol>
     <!-- Dashboard Counts Section-->
     <section class="dashboard-counts no-padding-bottom">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-sm-12">
                     <div class="card">
                         <div class="card-body">
                             <div class="row">
                                 <br />
                                 {{--<div class="col-md-12">--}}
                                     {{--<div class="row">--}}
                                         {{--<div class="col-md-6"></div>--}}
                                         {{--<div class="col-md-6">--}}
                                             {{--<div class="form-group">--}}
                                                 {{--<label class="col-xs-12" for="search">Search</label>--}}
                                                 {{--<div class="col-xs-12">--}}
                                                     {{--<div class="input-group">--}}
                                                         {{--<input class="form-control" type="text" id="search" name="search" />--}}
                                                         {{--<span class="input-group-addon"><i class="fa fa-search"></i></span>--}}
                                                     {{--</div>--}}
                                                 {{--</div>--}}
                                             {{--</div>--}}
                                         {{--</div>--}}
                                     {{--</div>--}}
                                     {{--<br />--}}
                                 {{--</div>--}}

                                 <div class="col-md-12">
                                     <div class="row">
                                         <div class="col-md-6">
                                             <a href="{{ route('invoices_create') }}" class="btn btn-primary">Create Invoice <span class="fa fa-plus" style="font-size: 0.8em;"></span></a>
                                             {{--<a href="{{ route('invoices_item') }}" class="btn btn-primary">Items <span class="fa fa-list" style="font-size: 0.8em;"></span></a>--}}
                                         </div>

                                         <div class="col-md-6 text-right">
                                             <!-- Trigger the modal with a button -->
                                             {{--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">--}}
                                                 {{--Advanced Search--}}
                                             {{--</button>--}}
                                             <div class="modal fade" id="myModal" role="dialog">
                                                 <div class="modal-dialog">
                                                     <!-- Modal content-->
                                                     <div class="modal-content">
                                                         <div class="modal-header">
                                                             <h4 class="modal-title">Advanced Search</h4>
                                                         </div>
                                                         <div class="modal-body text-left">
                                                             <div class="row">
                                                                 <div class="col-md-6">
                                                                     <div class="form-group">
                                                                         <div class="col-sm-9">
                                                                             <div class="form-material floating">
                                                                                 <select class="form-control" id="column_name" name="column_name" size="1">
                                                                                     <option></option>
                                                                                     <option value="1">Option #1</option>
                                                                                     <option value="2">Option #2</option>
                                                                                     <option value="3">Option #3</option>
                                                                                 </select>
                                                                                 <label for="column_name">Column Name</label>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                 <div class="col-md-6">
                                                                     <div class="form-group">
                                                                         <div class="col-xs-12">
                                                                             <div class="form-material floating">
                                                                                 <input class="form-control" type="text" id="value_view" name="value_view" required disabled>
                                                                                 <label for="value_view">Value you want to view</label>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                 <div class="col-md-6">
                                                                     <div class="form-group">
                                                                         <div class="col-sm-9">
                                                                             <div class="form-material floating">
                                                                                 <select class="form-control" id="column_date" name="column_date" size="1">
                                                                                     <option></option>
                                                                                     <option value="1">Option #1</option>
                                                                                     <option value="2">Option #2</option>
                                                                                     <option value="3">Option #3</option>
                                                                                 </select>
                                                                                 <label for="column_date">Column Date</label>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                 <div class="col-md-6">
                                                                     <div class="form-group">
                                                                         <label class="col-md-4 control-label" for="example-daterange1">Date Range</label>
                                                                         <div class="col-md-8">
                                                                             <div class="input-daterange input-group" data-date-format="mm/dd/yyyy">
                                                                                 <input class="form-control" type="text" id="example-daterange1" name="example-daterange1" placeholder="From">
                                                                                 <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                                                                 <input class="form-control" type="text" id="example-daterange2" name="example-daterange2" placeholder="To">
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="modal-footer">
                                                             <button type="button" class="btn btn-success" data-dismiss="modal">View</button>
                                                             <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                     <br />
                                 </div>



                                 <div class="col-md-6">
                                     <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                                         <li class="nav-item">
                                             <a class="nav-link @if($status == 'All') active @endif" href="{{ url('admin/transactions/invoices/status/All') }}">All</a>
                                         </li>
                                         <li class="nav-item">
                                             <a class="nav-link @if($status == 'Draft') active @endif" href="{{ url('admin/transactions/invoices/status/Draft') }}">Draft</a>
                                         </li>
                                         <li class="nav-item">
                                             <a class="nav-link @if($status == 'Unpaid') active @endif" href="{{ url('admin/transactions/invoices/status/Unpaid') }}">Unpaid</a>
                                         </li>
                                         <li class="nav-item">
                                             <a class="nav-link @if($status == 'Paid') active @endif" href="{{ url('admin/transactions/invoices/status/Paid') }}">Paid</a>
                                         </li>
                                     </ul>
                                 </div>

                                 <div class="col-md-12 push-10-t">
                                     <table id="invoices" class="table">
                                         <thead>
                                            <tr>
                                                <th class="text-center">INVOICE NUMBER</th>
                                                <th class="text-center">INVOICE DATE</th>
                                                <th class="text-center">PRINCIPAL</th>
                                                <th class="text-center">CONTACT EMAIL</th>
                                                <th class="text-center">BILLING AMOUNT</th>
                                                <th class="text-center">STATUS</th>
                                                <th class="text-center">ACTION</th>
                                            </tr>
                                         </thead>
                                         <tbody>
                                            @foreach($invoices as $invoice)
                                            <tr>
                                                <td>{{ $invoice->invoice_number }}</td>
                                                <td>{{ date('m/d/Y', strtotime($invoice->invoice_date)) }}</td>
                                                <td>{{ $invoice->Company->company_code }}</td>
                                                <td>{{ !empty($invoice->ContactPerson->email) ? $invoice->ContactPerson->email : '' }}</td>
                                                <td>{{ number_format($invoice->Items->sum('billing_amount'), 2) }}</td>
                                                <td>{{ $invoice->status }}</td>
                                                <td class="text-center">
                                                    @if($invoice->status == 'Draft')
                                                        <a href="#" class="btn btn-sm btn-primary sendInvoice" data-url="{{ url("/admin/transactions/invoices/send/{$invoice->id}") }}" data-ivn="{{ $invoice->invoice_number }}"><i class="fa fa-paper-plane"></i> Send</a>
                                                        <a href="{{ url("/admin/transactions/invoices/edit/{$invoice->id}") }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                                        <a href="" class="btn btn-sm btn-danger deleteInvoice" data-url="{{ url("/admin/transactions/invoices/delete/{$invoice->id}") }}" data-ivn="{{ $invoice->invoice_number }}"><i class="fa fa-paper-plane"></i> Delete</a>
                                                    @elseif($invoice->status == 'Unpaid')
                                                        <a href="{{ url("/admin/transactions/invoices/view/{$invoice->id}") }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a>
                                                        <a href="{{ url("/admin/transactions/invoices/edit/{$invoice->id}") }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                                        <a href="{{ url("admin/accounting/cash_receipt/create?source=invoice&id={$invoice->id}") }}" class="btn btn-sm btn-primary"><i class="fa fa-money"></i> Pay</a>
                                                        <a href="{{ url("/admin/transactions/invoices/print/{$invoice->id}") }}" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-print"></i> Print</a>
                                                    @elseif($invoice->status == 'Paid')
                                                        <a href="{{ url("/admin/transactions/invoices/view/{$invoice->id}") }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a>
                                                        <a href="{{ url("/admin/transactions/invoices/print/{$invoice->id}") }}" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-print"></i> Print</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                         </tbody>
                                     </table>
                                 </div>

                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </section>

    <div id="deleteInvoice" tabindex="-1" role="dialog" aria-labelledby="deleteInvoice" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete Invoice</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete the invoice #: <strong><span id="invoiceNumber"></span></strong>?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>

    <div id="sendInvoice" tabindex="-1" role="dialog" aria-labelledby="sendInvoice" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Send Invoice</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to send the invoice #: <strong><span id="invoiceNumber"></span></strong>?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-success" style="color: #FFF;"><span class="fa fa-paper-plane"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extendedscript')
<script>
    $(document).ready(function() {
        $('#invoices').dataTable({
            "pageLength": 100,
            /* Disable initial sort */
            "aaSorting": []
        });

        $(document).on('click', '.sendInvoice', function() {
           event.preventDefault();

            var url = $(this).attr('data-url');
            var id = $(this).attr('data-ivn');

            $("#sendInvoice").find('#invoiceNumber').text(id);
            $("#sendInvoice").find('#url').attr('href', url);
            $("#sendInvoice").modal('show');
        });

        $(document).on('click', '.deleteInvoice', function() {
           event.preventDefault();

            var url = $(this).attr('data-url');
            var id = $(this).attr('data-ivn');

            $("#deleteInvoice").find('#invoiceNumber').text(id);
            $("#deleteInvoice").find('#url').attr('href', url);
            $("#deleteInvoice").modal('show');
        });
    })
</script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10">
        <li>Transactions</li>
        <li><a href="{{ route('loans')}}">Loans</a></li>
        <li>Details</li>
    </ol>

                <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs" id="" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ url("admin/transactions/loans/{$loan->id}") }}"><i class="fa fa-info-circle"></i> Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/loans/{$loan->id}/loan-amortization-schedule") }}"><i class="fa fa-calendar"></i> Loan Amortization Schedule</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/loans/{$loan->id}/payment-schedule") }}"><i class="fa fa-calendar"></i> Payment Schedule</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/loans/{$loan->id}/payments") }}"><i class="fa fa-money"></i> Payments</a>
                        </li>
                    </ul>
                </div>

                <div class="col-sm-12 push-10-t">
                    <div class="row">

                        <div class="col-md-5">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>Complete Name</th>
                                        <td>{{ $loan->User->Profile->name() }}</td>
                                    </tr>
                                    <tr>
                                        <th>ID Number</th>
                                        <td>{{ $loan->User->Profile->id_number }}</td>
                                    </tr>
                                    <tr>
                                        <th>Cost Center</th>
                                        <td>{{ $loan->User->Profile->CostCenter->cost_center }}</td>
                                    </tr>
                                    <tr>
                                        <th>CLA Number</th>
                                        <td>{{ $loan->cla_number }}</td>
                                    </tr>
                                    <tr>
                                        <th>Date Request</th>
                                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $loan->date_request)->format('m/d/Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Loan Type</th>
                                        <td>@if($loan->is_voucher == 'N') {{ $loan->LoanType->type }} @else {{ $loan->loan_type }} @endif</td>
                                    </tr>
                                    <tr>
                                        <th>Month to Pay</th>
                                        <td>{{ $loan->months_to_pay }}</td>
                                    </tr>
                                    <tr>
                                        <th>CV Number</th>
                                        <td>@if(!empty($loan->check_voucher_id)) {{ $loan->CheckVoucher->cv_number }} @else {{ $loan->cv_number }} @endif</td>
                                    </tr>
                                    <tr>
                                        <th>CV Date</th>
                                        <td>@if(!empty($loan->check_voucher_id)) {{ date('m/d/Y', strtotime($loan->CheckVoucher->check_date)) }} @elseif(!empty($loan->check_date)) {{ date('m/d/Y', strtotime($loan->check_date)) }} @endif</td>
                                    </tr>
                                    <tr>
                                        <th>Cheque Number</th>
                                        <td>@if(!empty($loan->check_voucher_id)) {{ $loan->CheckVoucher->check_number }} @else {{ $loan->check_number }} @endif</td>
                                    </tr>
                                    <tr>
                                        <th>Effective Year/Month</th>
                                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $loan->effective_date)->format('F Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Remarks</th>
                                        <td>{{ $loan->remarks }}</td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-5">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th style="width: 200px">Request Amount</th>
                                        <td>{{ number_format($loan->request_amount, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Service Charge %</th>
                                        <td>{{ $loan->LoanType->service_charge }}%</td>
                                    </tr>
                                    <tr>
                                        <th>Service Charge</th>
                                        <td>{{ number_format($loan->service_charge, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Savings %</th>
                                        <td>{{ $loan->LoanType->savings }}%</td>
                                    </tr>
                                    <tr>
                                        <th>Savings</th>
                                        <td>{{ number_format($loan->savings, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Loan Interest %</th>
                                        <td>{{ $loan->LoanType->interest }}%</td>
                                    </tr>
                                    <tr>
                                        <th>Interest</th>
                                        <td>{{ number_format($loan->interest, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Overall Interest</th>
                                        <td>{{ number_format($loan->overall_interest, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <th>CLA Number Reference</th>
                                        <td>{{ $loan->cla_number_reference }}</td>
                                    </tr>
                                    <tr>
                                        <th>Additional Deduction</th>
                                        <td>{{ number_format($loan->additional_deduction, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Amortization</th>
                                        <td>{{ number_format($loan->amortization, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Granted Loan Amount</th>
                                        <td>{{ number_format($loan->granted_loan_amount, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Disbursed Amount</th>
                                        <td>{{ number_format($loan->disbursed_amount, 2) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection

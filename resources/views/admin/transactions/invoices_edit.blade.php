@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('invoices')}}">Invoices</a></li>
        <li>Create</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <br />
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="company_code">Company Code</label>
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control" value="{{ $invoice->Company->company_code }}" disabled>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="bill_to">Bill To</label>
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control" value="{{ !empty($invoice->ContactPerson) ? $invoice->ContactPerson->complete_name() : '' }}" disabled>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="company_name">Company Name</label>
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control" value="{{ $invoice->Company->company_name }}" disabled>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="company_address">Company Address</label>
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control" value="{{ $invoice->Company->company_address }}" disabled>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label class="form-control-label">Invoice Date</label>
                                                        <div id="invoice_date" class="input-group btn-mmddyyyy ">
                                                            <input type="text" class="form-control" id="" name="invoice_date" value="{{ date('m/d/Y', strtotime($invoice->invoice_date)) }}" placeholder="mm/dd/yyyy" required>
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        </div>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="terms">Terms</label>
                                                    <div class="col-xs-12">
                                                        <select class="form-control" id="terms" name="invoice_terms_id" size="1" required>
                                                            @foreach(\App\InvoiceTerms::orderBy('days')->get() as $terms)
                                                                <option value="{{ $terms->id }}" data-days="{{ $terms->days }}" @if($invoice->invoice_terms_id == $terms->id) selected @endif>{{ $terms->terms }}</option>
                                                            @endforeach
                                                        </select>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label class="form-control-label">Due Date</label>
                                                        <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                            <input type="text" class="form-control" id="due_date" name="due_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y', strtotime($invoice->invoice_date)) }}" required readonly>
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        </div>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="reference">Reference</label>
                                                    <div class="col-xs-12">
                                                        <select name="reference" id="reference" class="form-control">
                                                            @if(!empty(\App\Companies::find($invoice->company_id)->PayrollCycles()))
                                                                @foreach(\App\Companies::find($invoice->company_id)->PayrollCycles() as $payrollCycle)
                                                                    <option value="{{ $payrollCycle->payroll_code }}" @if($payrollCycle->payroll_code == $invoice->reference) selected @endif>{{ $payrollCycle->payroll_code }}</option>
                                                                @endforeach
                                                            @else
                                                                <option value="">No payroll codes found.</option>
                                                            @endif
                                                        </select>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 push-20-t push-20">
                                    <table id="invoiceItems" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="text-center" width="300">DESCRIPTION</th>
                                                <th class="text-center" width="80">HEAD COUNT</th>
                                                <th class="text-center" width="80">HOURS</th>
                                                <th class="text-center" width="150">AMOUNT</th>
                                                <th class="text-center">TAX</th>
                                                <th class="text-center" width="150">TAX AMOUNT</th>
                                                <th class="text-center">BILLING AMOUNT</th>
                                            </tr>

                                        </thead>
                                        <tbody>

                                        @foreach($invoice->Items as $item)
                                            <tr>
                                                <td><button class="btn btn-sm btn-danger remove-item"><i class="fa fa-remove"></i></button></td>
                                                <td><textarea class="form-control" name="existing_items[{{ $item->id }}][description]" rows="3" style="max-width: 300px;">{{ $item->description }}</textarea></td>
                                                <td><input type="text" class="form-control" name="existing_items[{{ $item->id }}][head_count]" value="{{ $item->head_count }}"></td>
                                                <td><input type="text" class="form-control" name="existing_items[{{ $item->id }}][hours]" {{ $item->hours }}></td>
                                                <td><input type="text" class="form-control amount" name="existing_items[{{ $item->id }}][amount]" value="{{ $item->amount }}"></td>
                                                <td>
                                                    <select name="existing_items[{{ $item->id }}][business_tax_type_id]" id="" class="form-control business-tax">
                                                        @foreach(\App\BusinessTaxTypes::all() as $t)
                                                            <option value="{{ $t->id }}" data-percent='{{ $t->percent }}' @if($item->business_tax_type_id == $t->id) selected @endif>{{ $t->tax }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="text" class="form-control tax-amount" name="existing_items[{{ $item->id }}][tax_amount]" value="{{ $item->tax_amount }}" readonly></td>
                                                <td><input type="text" class="form-control billing-amount" name="existing_items[{{ $item->id }}][billing_amount]" value="{{ $item->billing_amount }}" readonly></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                {{--<div class="col-md-12">--}}
                                    {{--<h6>Enter detailed Description <a href="{{ route('invoices_create_description') }}">here</a>. Optional</h6>--}}
                                {{--</div>--}}

                                <div class="col-md-12 text-right">
{{--                                    <a style="float: left" href="{{ route('add_invoice_item') }}" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add item</a>--}}

                                    <!-- Hidden on July 2, 2018 -->
                                    {{--<button id="addInvoiceItem" class="btn btn-primary" style="float: left"><i class="fa fa-plus" aria-hidden="true"></i> Add item</button>--}}

                                    <h3>Total Amount:
                                        <span id="totalAmountTxt" style="border: 1px blue solid; padding: 1em; border-radius: 10px; ">{{ number_format($invoice->total_amount, 2) }} PHP</span>
                                        <input type="hidden" id="totalAmount" name="total_amount" value="{{ $invoice->total_amount }}">
                                    </h3>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <br />
                                        <div class="form-group">
                                            <label class="col-xs-12" for="">Attach Files</label>
                                            <div class="col-xs-12">
                                                <input type="file" id="" name="files[]" multiple="">
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <br>
                                            @foreach($invoice->Files as $file)
                                                <div style="margin-bottom: 10px;">
                                                    <input type="hidden" name="existing_files[]" value="{{ $file->id }}" />
                                                    <div><a href="{{ url("/admin/transactions/invoices/file/{$file->id}") }}" download><i class="fa fa-download"></i> {{ $file->file_name }}</a> <button class="btn btn-sm btn-danger remove-file"><i class="fa fa-remove"></i></button></div>
                                                </div>
                                            @endforeach
                                            <br>
                                        </div>
                                        <br />
                                        <br />
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="note_to_recipient">Note To Recipient</label>
                                                <textarea class="form-control input-lg"
                                                          id="note_to_recipient"
                                                          name="note_to_recipient" rows="10"
                                                          placeholder="Note To Recipient">{{ $invoice->note_to_recipient }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="terms_and_conditions">Terms and Conditions</label>
                                                <textarea class="form-control input-lg"
                                                          id="terms_and_conditions"
                                                          name="terms_and_conditions" rows="10"
                                                          placeholder="Terms and Conditions">{{ $invoice->terms_and_conditions }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="memo">Memo</label>
                                                <textarea class="form-control input-lg"
                                                          id="memo"
                                                          name="memo" rows="10">{{ $invoice->memo }}</textarea>
                                            </div>
                                        </div>
                                        <br />
                                    </div>

                                    <div class="col-md-12 push-20-t">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-5">
                                                    <div class="form-group">
                                                        <label for="">Prepared By</label>
                                                        <select name="prepared_by" id="" class="form-control" disabled>
                                                            <option value="">{{ $invoice->PreparedBy->Profile->namefl() }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-sm-6 col-md-5">
                                                    <div class="form-group">
                                                        <label for="">Approved By</label>
                                                        <select name="approved_by" id="" class="form-control">
                                                            @foreach(\App\User::whereIn('id', \App\Signatories::pluck('user_id'))->get() as $signatory)
                                                                <option value="{{ $signatory->id }}" @if($signatory->id == $invoice->approved_by) selected @endif>{{ $signatory->Profile->namefl() }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <br /><br />
                                        <a href="{{ url("/admin/transactions/invoices/status/Draft") }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back to Invoices</a>
                                        <button href="#" class="btn btn-success" ><i class="fa fa-save" aria-hidden="true"></i> Confirm Edit</button>
                                    </div>
                                </div>

                                {{--<div class="col-md-12 text-right">--}}
                                    {{--<br /><br />--}}
                                    {{--<button class="btn btn-minw btn-danger" type="button"><i class="fa fa-times"></i> Cancel</button>--}}
                                    {{--<button class="btn btn-minw btn-success" type="button"><i class="fa fa-check"></i> Submit</button>--}}
                                {{--</div>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

            $("#company_id").change(function() {
                var company = $.parseJSON($(this).find(':selected').attr('data-company'));
                $("#contact_person_id").find('option').remove();

                if(company.contactpersons.length > 0) {
                    $.each(company.contactpersons, function(i, v) {
                        $("#contact_person_id").append('<option value="'+ v.id +'">'+ v.first_name +' '+ v.last_name +'</option>');
                    });
                }

                $("#company_name").val(company.company_name);
                $("#company_address").val(company.address);
            });

            $(document).on('dp.change', "#invoice_date", function(e) {
                $("#invoice_date").attr('data-date', moment(e.date._d).format('YYYY-MM-DD'));
                refreshDueDate();
            });

            $("#terms").change(function() {
                refreshDueDate();
            });

            function refreshDueDate() {
                $("#due_date").val(moment($("#invoice_date").attr('data-date')).add($("#terms").find(':selected').attr('data-days'), 'days').format("MM/DD/YYYY"));
            }

            $(document).on('click', '.remove-item', function() {
               event.preventDefault();

               $(this).closest('tr').remove();
            });

            var x = 1;

            $(document).on('click', '#addInvoiceItem', function() {
                event.preventDefault();

                var itemTemplate = "<tr>" +
                    "<td><button class='btn btn-sm btn-danger remove-item'><i class='fa fa-remove'></i></button></td>" +
                    "<td><textarea class='form-control' name='items["+ x +"][description]' rows='3' style='max-width: 329px;'></textarea></td>" +
                    "<td><input type='text' class='form-control' name='items["+ x +"][head_count]'></td>" +
                    "<td><input type='text' class='form-control' name='items["+ x +"][hours]'></td>" +
                    "<td><input type='text' class='form-control amount' name='items["+ x +"][amount]'></td>" +
                    "<td><select name='items["+ x +"][business_tax_type_id]' id='' class='form-control business-tax'>" +
                        @foreach(\App\BusinessTaxTypes::all() as $t)
                            "<option value='{{ $t->id }}' data-percent='{{ $t->percent }}'>{{ $t->tax }}</option>" +
                        @endforeach
                    "</select>" +
                    "</td>" +
                    "<td><input type='text' class='form-control tax-amount' name='items["+ x +"][tax_amount]' readonly></td>" +
                    "<td><input type='text' class='form-control billing-amount' name='items["+ x +"][billing_amount]' readonly></td>" +
                    "</tr>";

                $("#invoiceItems").find('tbody').append(itemTemplate);

                x++;
            });

            $(document).on('keyup', '.amount', function() {
                var amount = parseFloat($(this).val()).toFixed(2) || 0;
                if($(this).val().length == 0) {
                    amount = 0;
                }

                var percent = parseFloat($(this).closest('tr').find('.business-tax').find(':selected').attr('data-percent')) / 100;

                var taxAmount = (parseFloat(amount) * percent).toFixed(2);

                $(this).closest('tr').find('.tax-amount').val(taxAmount);

                var billingAmount = (parseFloat(taxAmount) + parseFloat(amount)).toFixed(2);


                $(this).closest('tr').find('.billing-amount').val(billingAmount);

                updateTotalBillingAmount();
            });

            $(document).on('change', '.business-tax', function() {
                var amount = parseFloat($(this).closest('tr').find('.amount').val()).toFixed(2) || 0;
                if($(this).val().length == 0) {
                    amount = 0;
                }

                var percent = parseFloat($(this).closest('tr').find('.business-tax').find(':selected').attr('data-percent')) / 100;

                var taxAmount = (parseFloat(amount) * percent).toFixed(2);

                $(this).closest('tr').find('.tax-amount').val(taxAmount);

                var billingAmount = (parseFloat(taxAmount) + parseFloat(amount)).toFixed(2);


                $(this).closest('tr').find('.billing-amount').val(billingAmount);

                updateTotalBillingAmount();
            });

            function updateTotalBillingAmount() {
                var totalBillingAmount = 0;

                $("#invoiceItems").find('tbody > tr').each(function() {
                    var amount = parseFloat($(this).find('.amount').val()).toFixed(2) || 0;
                    if($(this).find('.amount').val().length == 0) {
                        amount = 0;
                    }

                    var percent = parseFloat($(this).find('.business-tax').find(':selected').attr('data-percent')) / 100;

                    var taxAmount = (parseFloat(amount) * percent).toFixed(2);

                    $(this).find('.tax-amount').val(taxAmount);

                    var billingAmount = (parseFloat(taxAmount) + parseFloat(amount));

                    totalBillingAmount += billingAmount;

                    $("#totalAmountTxt").text(totalBillingAmount.toFixed(2));
                    $("#totalAmount").val(totalBillingAmount.toFixed(2));
                });
            }

            $(document).on('click', '.remove-file', function() {
                event.preventDefault();

                $(this).parent().parent().remove();
            });

            // updateReference();

            // function updateReference()
            // {
            //     var payrollCycles = $("#company_id").find(':selected').data('payrollcycles');
            //     $("#reference").find('option').remove();
            //
            //     if(payrollCycles.length > 0) {
            //         $.each(payrollCycles, function(index, payrollCycle) {
            //             $("#reference").append('<option value="'+ payrollCycle.payroll_code +'">'+ payrollCycle.payroll_code +'</option>');
            //         });
            //     } else {
            //         $("#reference").append('<option value="">No payroll codes found.</option>');
            //     }
            // }
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10">
        <li>Transactions</li>
        <li><a href="{{ route('loans')}}">Loans</a></li>
        <li><a class="link-effect" href="{{ route('loans_details') }}">Details</a></li>
    </ol>

                <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="cla_number">CLA Number</label>
                                            <div><strong>{{ $loan->cla_number }}</strong></div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="employee_name">Complete Name</label>
                                            <div><strong>{{ $loan->User->Profile->name() }}</strong></div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="employee_id">ID Number</label>
                                            <div><strong>{{ $loan->User->Profile->id_number }}</strong></div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="cost_center">Cost Center</label>
                                            <div><strong>{{ $loan->User->Profile->CostCenter->cost_center }}</strong></div>
                                        </div>
                                    </div>

                                    <div class="col-md-12"><hr /></div>


                                    @if($loan->status == 'Cancelled')
                                        <div class="col-xs-12">
                                            <p class="text-danger"><strong>NOTE: This Loan is cancelled</strong></p>
                                        </div>
                                    @endif

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="">
                                                <label class="form-control-label">Date Request</label>
                                                <div id="joinDate" class="input-group btn-mmddyyyy">
                                                    <input type="text" class="form-control" name="date_request" placeholder="mm/dd/yyyy" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $loan->date_request)->format('m/d/Y') }}" required disabled>
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="employee_id">Loan Type</label>
                                            <select name="loan_type_id" id="loanType" class="form-control" readonly>
                                                @foreach(\App\LoanTypes::orderBy('type')->get() as $loanType)
                                                    <option value="{{ $loanType->id }}"
                                                            data-amount="{{ $loanType->amount }}"
                                                            data-interest="{{ $loanType->interest * .01 }}"
                                                            data-servicecharge="{{ $loanType->service_charge * .01 }}"
                                                            data-savings="{{ $loanType->savings * .01 }}"
                                                            @if($loan->LoanType->id == $loanType->id) selected @endif>{{ $loanType->type }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="months_to_pay">Month to Pay</label>
                                            <select name="months_to_pay" id="months_to_pay" class="form-control" readonly>
                                                @for($x = 1; $x < 25; $x++)
                                                    <option value="{{ $x }}" @if($loan->months_to_pay == $x) selected @endif>{{ $x }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="amount_paid">Request Amount</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">₱</span>
                                                <input class="form-control" type="text" id="request_amount" name="request_amount" value="{{ $loan->request_amount }}" required readonly>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Service Charge %</label>
                                            <div class="input-group">
                                                <input class="form-control text-right" type="text" id="service_charge_p" value="{{ $loan->LoanType->service_charge }}" readonly>
                                                <span class="input-group-addon">%</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="service_charge">Service Charge</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">₱</span>
                                                <input class="form-control" type="text" id="service_charge" name="service_charge" value="{{ $loan->service_charge }}" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="savings_%">Savings %</label>
                                            <div class="input-group text-right">
                                                <input class="form-control" type="text" id="savings_p" value="{{ $loan->LoanType->savings }}" readonly>
                                                <span class="input-group-addon">%</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="savings">Savings</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">₱</span>
                                                <input class="form-control" type="text" id="savings" name="savings" value="{{ $loan->savings }}" readonly>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="loan_interest_%">Loan Interest %</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">%</span>
                                                <input class="form-control" type="text" id="interest_p" value="{{ $loan->LoanType->interest }}" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="interest">Interest</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">₱</span>
                                                <input class="form-control" type="text" id="interest" name="interest" value="{{ $loan->interest }}" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="overall_interest">Overall Interest</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">₱</span>
                                                <input class="form-control" type="text" id="overall_interest" name="overall_interest" value="{{ $loan->overall_interest }}" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">

                                        <div class="form-group">
                                            <label for="cla_number_reference">CLA Number Reference</label>
                                            <input class="form-control" type="text" id="cla_number_reference" name="cla_number_reference" value="{{ $loan->cla_number_reference }}" disabled>
                                        </div>
                                    </div>

                                    <div class="col-md-3">

                                        <div class="form-group">
                                            <label for="additional_deduction">Additional Deduction</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">₱</span>
                                                <input class="form-control" type="text" id="additional_deduction" name="additional_deduction" value="{{ $loan->additional_deduction }}" disabled>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="amortization">Amortization</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" id="amortization" name="amortization" value="{{ $loan->amortization }}" readonly>
                                                <span class="input-group-addon">₱</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="granted_loan_amount">Granted Loan Amount</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">₱</span>
                                                <input class="form-control" type="text" id="granted_loan_amount" name="granted_loan_amount" value="{{ $loan->granted_loan_amount }}" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="disbursed_amount">Disbursed Amount</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">₱</span>
                                                <input class="form-control" type="text" id="disbursed_amount" name="disbursed_amount" value="{{ $loan->disbursed_amount }}" readonly>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                @if($loan->status != 'Cancelled')
                                <div class="row">
                                    <div><hr></div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Action</label>
                                            <select class="form-control" name="status" id="action">
                                                @if($loan->status == 'Cancelled')
                                                    <option value="">Cancelled</option>
                                                @else
                                                <option value="Pending">Pending</option>
                                                <option value="Approved">Approve</option>
                                                <option value="Cancelled">Cancel</option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">CV Number</label>
                                            <input type="text" class="form-control" id="cvNumber" value="{{ $loan->cv_number }}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label for="">CV Date</label>
                                            <input type="text" class="form-control" id="cvDate" value="{{ date('m/d/Y', strtotime($loan->cv_date)) }}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Cheque Number</label>
                                            <input type="text" class="form-control" id="cheque" value="{{ $loan->check_number }}" disabled>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Effective Month</label>
                                            <select name="effective_month" id="effectiveMonth" class="form-control" disabled>
                                                @for($x = 1; $x < 13; $x++)
                                                    <option value="{{ $x }}" @if(date('m') + 1 > $x) disabled @endif>{{ $x }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Effective Year</label>
                                            <input type="text" id="effectiveYear" class="form-control" name="effective_year" value="@if(date('m') == 12) {{date('Y') + 1}} @else {{date('Y')}} @endif" onkeypress='return event.charCode >= 48 && event.charCode <= 57' disabled>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Remarks</label>
                                            <textarea id="remarks" name="remarks" rows="3"
                                                      class="form-control" disabled></textarea>
                                        </div>
                                    </div>
                                    </div>


                                <div class="col-md-12 text-right">
                                    <br><br>
                                    <a href="{{ url('/admin/transactions/loans') }}" class="btn btn-danger">Cancel <span class="fa fa-ban" style="font-size: 0.8em;"></span></a>
                                    <a href="#" id="editLoan" class="btn btn-primary">Edit <span class="fa fa-edit" style="font-size: 0.8em;"></span></a>
                                    <button class="btn btn-success">Update <span class="fa fa-check" style="font-size: 0.8em;"></span></button>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

            $("#editLoan").click(function(e) {
                event.preventDefault();
                $(this).toggleClass('is-edit');

                if($(this).hasClass('is-edit') == true) {
                    $("#loanType, #months_to_pay, #request_amount, #additional_deduction").removeAttr('readonly');
                } else {
                    $("#loanType, #months_to_pay, #request_amount, #additional_deduction").attr('readonly', 'readonly');
                }
            });

            $("#request_amount, #loanType, #additional_deduction, #months_to_pay ").on('keyup change', function() {
                var loanType = $("#loanType").find(':selected');

                if(loanType.val() == 4) {
                    $("#request_amount").attr('readonly', true);
                    $("#request_amount").val(loanType.attr('data-amount'));
                } else {
                    $("#request_amount").removeAttr('readonly');
                }

                $("#service_charge_p").val( (parseFloat(loanType.attr('data-servicecharge')) * 100).toFixed(2) );
                $("#savings_p").val( (parseFloat(loanType.attr('data-savings')) * 100).toFixed(2) );
                $("#interest_p").val( (parseFloat(loanType.attr('data-interest')) * 100).toFixed(2) );

                var requestAmount = parseFloat($("#request_amount").val()) || 0;

                var additionalDeduction = parseFloat($("#additional_deduction").val()) || 0;
                var monthsToPay = parseFloat($("#months_to_pay").find(':selected').text());
                var serviceCharge = parseFloat(loanType.attr('data-servicecharge'));
                var savings = parseFloat(loanType.attr('data-savings'));
                var interest = parseFloat(loanType.attr('data-interest'));

                $("#service_charge").val( (requestAmount * serviceCharge).toFixed(2) );
                $("#savings").val( (requestAmount * savings).toFixed(2) );
                $("#interest").val( (requestAmount * interest).toFixed(2) );
                $("#overall_interest").val( ((requestAmount * interest) * monthsToPay).toFixed(2) );
                $("#granted_loan_amount").val( (((requestAmount * interest) * monthsToPay) + requestAmount).toFixed(2) );
                $("#amortization").val( (((((requestAmount * interest) * monthsToPay) + requestAmount) / monthsToPay) / 2).toFixed(2) );

                $("#disbursed_amount").val( (requestAmount - (requestAmount * serviceCharge) - (requestAmount * savings) - additionalDeduction).toFixed(2) );
            });

            $(document).on('change', '#action', function() {
                if($(this).find(':selected').text() == 'Approve') {
                    // $("#cvNumber").removeAttr('disabled').val('');
//                    $("#cvDate").removeAttr('disabled').val('');
//                    $("#cheque").removeAttr('disabled').val('');
                    $("#effectiveMonth").removeAttr('disabled');
                    $("#effectiveYear").removeAttr('disabled').attr('readonly', true).val('{{ date('Y') }}');
                    $("#remarks").removeAttr('disabled').val('');
                } else {
                    // $("#cvNumber").attr('disabled', true).val('');
                    // $("#cvDate").attr('disabled', true).val('');
                    // $("#cheque").attr('disabled', true).val('');
                    $("#effectiveMonth").attr('disabled', true);
                    $("#effectiveYear").attr('disabled', true).val('{{ date('Y') }}');
                    $("#remarks").attr('disabled', true).val('');
                }
            });

            // $(document).on('change', '#cvNumber', function() {
            //    if($(this).find(':selected').val() == '') {
            //        console.log('Fail');
            //        $("#cvDate").val('');
            //        $("#cheque").val('');
            //    } else {
            //        console.log('Success');
            //        $("#cvDate").val($(this).find(':selected').attr('data-cvdate'));
            //        $("#cheque").val($(this).find(':selected').attr('data-checknumber'));
            //    }
            // });
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10">
        <li><a class="nav-link" href="{{ url("admin/dashboard") }}"><i class="fa fa-home"></i> Dashboard</a></li>
        <li>Loan Lapses</li>
    </ol>

                <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12 push-10-t">
                    <div class="row">

                        <div class="col-md-12">
                            <table class="table dt table-bordered table-condensed">
                                <thead>
                                    <tr>
                                        <th class="text-center">CLA NUMBER</th>
                                        <th class="text-center">NAME</th>
                                        <th class="text-center">LOAN TYPE</th>
                                        <th class="text-center">AMORTIZATION</th>
                                        <th class="text-center">LAPSE COUNT</th>
                                        <th class="text-center">TOTAL AMOUNT DUE</th>
                                        <th class="text-center">ACTION</th>
                                    </tr>
                                </thead>


                                <tbody>

                                <?php $loanType = \App\LoanTypes::all(); ?>
                                @foreach($paymentSchedules as $paymentSchedule)

                                    <tr>
                                        <th class="text-center">{{ $paymentSchedule->cla_number }}</th>
                                        <th class="">{{ $paymentSchedule->name }}</th>
                                        <th class="">{{ !empty($paymentSchedule->loan_type_id) ? $loanType->where('id', $paymentSchedule->loan_type_id)->first()['type'] : "{$paymentSchedule->loan_type} LOAN" }}</th>
                                        <td class="text-center">{{ $paymentSchedule->amortization }}</td>
                                        <td class="text-center">{{ $paymentSchedule->lapse_count }}</td>
                                        <td class="text-right">{{ number_format($paymentSchedule->total_amount_due, 2) }}</td>
                                        <th class="text-center">
                                            <a href="{{ url("/admin/transactions/loans/{$paymentSchedule->id}") }}" class="btn btn-primary">VIEW</a>
                                        </th>
                                    </tr>
                                @endforeach
                                </tbody>



                            </table>
                        </div>

                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('.dt').DataTable({
                "sorting": [],
                "pageLength": 25,
                order: [[ 4, 'desc' ]]
            });
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('invoices')}}">Invoices</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6"></div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="search">Search</label>
                                                <div class="col-xs-12">
                                                    <div class="input-group">
                                                        <input class="form-control" type="text" id="search" name="search" />
                                                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                </div>

                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="{{ route('invoices_create') }}" class="btn btn-primary">Create Invoice <span class="fa fa-plus" style="font-size: 0.8em;"></span></a>
                                            <a href="{{ route('invoices_item') }}" class="btn btn-primary">Items <span class="fa fa-list" style="font-size: 0.8em;"></span></a>
                                        </div>

                                        <div class="col-md-6 text-right">
                                            <!-- Trigger the modal with a button -->
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">
                                                Advanced Search
                                            </button>
                                            <div class="modal fade" id="myModal" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Advanced Search</h4>
                                                        </div>
                                                        <div class="modal-body text-left">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-9">
                                                                            <div class="form-material floating">
                                                                                <select class="form-control" id="column_name" name="column_name" size="1">
                                                                                    <option></option>
                                                                                    <option value="1">Option #1</option>
                                                                                    <option value="2">Option #2</option>
                                                                                    <option value="3">Option #3</option>
                                                                                </select>
                                                                                <label for="column_name">Column Name</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <div class="col-xs-12">
                                                                            <div class="form-material floating">
                                                                                <input class="form-control" type="text" id="value_view" name="value_view" required disabled>
                                                                                <label for="value_view">Value you want to view</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-9">
                                                                            <div class="form-material floating">
                                                                                <select class="form-control" id="column_date" name="column_date" size="1">
                                                                                    <option></option>
                                                                                    <option value="1">Option #1</option>
                                                                                    <option value="2">Option #2</option>
                                                                                    <option value="3">Option #3</option>
                                                                                </select>
                                                                                <label for="column_date">Column Date</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 control-label" for="example-daterange1">Date Range</label>
                                                                        <div class="col-md-8">
                                                                            <div class="input-daterange input-group" data-date-format="mm/dd/yyyy">
                                                                                <input class="form-control" type="text" id="example-daterange1" name="example-daterange1" placeholder="From">
                                                                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                                                                <input class="form-control" type="text" id="example-daterange2" name="example-daterange2" placeholder="To">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-success" data-dismiss="modal">View</button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                </div>



                                <div class="col-md-6">
                                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link " href="{{ url('admin/transactions/invoices/status/All') }}">All</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ url('admin/transactions/invoices/status/Draft') }}">Draft</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/transactions/invoices/status/Unpaid') }}">Unpaid</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/transactions/invoices/status/Paid') }}">Paid</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12">
                                    <div class="block">
                                        <div class="block-content">
                                            <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="dataTables_length" id="DataTables_Table_1_length"><label>
                                                                <select name="DataTables_Table_1_length"
                                                                        aria-controls="DataTables_Table_1"
                                                                        class="form-control">
                                                                    <option value="5">5</option>
                                                                    <option value="10">10</option>
                                                                    <option value="15">15</option>
                                                                    <option value="20">20</option>
                                                                </select>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div id="DataTables_Table_1_filter" class="dataTables_filter">
                                                            <label>Search:<input type="search" class="form-control" placeholder="" aria-controls="DataTables_Table_1"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <table class="table table-bordered table-striped js-dataTable-full dataTable no-footer"
                                                               id="DataTables_Table_1" role="grid" aria-describedby="DataTables_Table_1_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="text-center sorting_asc"
                                                                    tabindex="0"
                                                                    aria-controls="DataTables_Table_1"
                                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                                    aria-label=": activate to sort column descending"></th>

                                                                <th class="sorting"
                                                                    tabindex="0"
                                                                    aria-controls="DataTables_Table_1"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label="Name: activate to sort column ascending">Invoice Number</th>

                                                                <th class="hidden-xs sorting"
                                                                    tabindex="0" aria-controls="DataTables_Table_1"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label="Email: activate to sort column ascending">Invoice Date</th>

                                                                <th class="hidden-xs sorting" tabindex="0" aria-controls="DataTables_Table_1"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label="Access: activate to sort column ascending">Principal</th>

                                                                <th class="text-center sorting_disabled" rowspan="1"
                                                                    colspan="1" aria-label="Actions">Contact Email</th>

                                                                <th class="text-center sorting_disabled" rowspan="1"
                                                                    colspan="1" aria-label="Actions">Billing Amount</th>

                                                                <th class="text-center sorting_disabled" rowspan="1"
                                                                    colspan="1" aria-label="Actions">Status</th>

                                                                <th class="text-center sorting_disabled" rowspan="1"
                                                                    colspan="1" aria-label="Actions">Action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row" class="odd">
                                                                <td class="text-center sorting_1">
                                                                    1
                                                                </td>
                                                                <td class="font-w600"></td>
                                                                <td class="font-w600"></td>
                                                                <td class="font-w600">
                                                                    <span class="label label-danger">YES</span>
                                                                </td>
                                                                <td class="font-w600"></td>
                                                                <td class="font-w600"></td>
                                                                <td class="font-w600"></td>
                                                                <td class="font-w600">
                                                                    <a href="#" class="btn btn-primary">Update</a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="dataTables_info" id="DataTables_Table_1_info"
                                                             role="status" aria-live="polite">Showing <strong>1</strong>-<strong>10</strong> of <strong>40</strong>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="dataTables_paginate paging_simple_numbers"
                                                             id="DataTables_Table_1_paginate">
                                                            <ul class="pagination">
                                                                <li class="paginate_button previous disabled"
                                                                    aria-controls="DataTables_Table_1" tabindex="0"
                                                                    id="DataTables_Table_1_previous">
                                                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                                                </li>

                                                                <li class="paginate_button active" aria-controls="DataTables_Table_1"
                                                                    tabindex="0">
                                                                    <a href="#">1</a>
                                                                </li>

                                                                <li class="paginate_button " aria-controls="DataTables_Table_1"
                                                                    tabindex="0">
                                                                    <a href="#">2</a>
                                                                </li>

                                                                <li class="paginate_button " aria-controls="DataTables_Table_1"
                                                                    tabindex="0">
                                                                    <a href="#">3</a>
                                                                </li>

                                                                <li class="paginate_button " aria-controls="DataTables_Table_1"
                                                                    tabindex="0">
                                                                    <a href="#">4</a>
                                                                </li>

                                                                <li class="paginate_button next" aria-controls="DataTables_Table_1"
                                                                    tabindex="0" id="DataTables_Table_1_next">
                                                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('loans')}}">Loans</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="{{ route('loans_request') }}" class="btn btn-primary">Request <span class="fa fa-paperclip" style="font-size: 0.8em;"></span></a>
                                    <a href="{{ route('loans_voucher') }}" class="btn btn-primary">Voucher <span class="fa fa-ticket" style="font-size: 0.8em;"></span></a>
                                </div>

                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('deposit_table') }}"><span class="badge badge-info">1</span> Request</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('withdraw_table') }}"><span class="badge badge-success">1</span> Approved</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('withdraw_table') }}"><span class="badge badge-danger">1</span> Reject</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('withdraw_table') }}"><span class="badge badge-warning">1</span> Pending</a>
                                        </li>
                                    </ul>
                                </div>


                                <div class="col-md-3 text-center">
                                    <h6>Request</h6>
                                    <h3>1</h3>
                                </div>
                                <div class="col-md-3 text-center">
                                    <h6>Approved</h6>
                                    <h3>1</h3>
                                </div>
                                <div class="col-md-3 text-center">
                                    <h6>Rejected</h6>
                                    <h3>1</h3>
                                </div>
                                <div class="col-md-3 text-center">
                                    <h6>Pending</h6>
                                    <h3>1</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-sm-12 form-control-label">Column Name: </label>
                                        <div class="col-sm-12">
                                            <select name="column_name" class="form-control" required>
                                                <option>option 1</option>
                                                <option>option 2</option>
                                                <option>option 3</option>
                                                <option>option 4</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-sm-12 form-control-label">Column Name: </label>
                                        <div class="col-sm-12">
                                            <select name="column_name" class="form-control" required>
                                                <option>Pending</option>
                                                <option>Approved</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-sm-12 form-control-label">Column Name: </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" />
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Actions</th>
                                                    <th>CLA Number</th>
                                                    <th>Employee ID Number</th>
                                                    <th>Employee Name</th>
                                                    <th>Cost Center</th>
                                                    <th>Loan Type</th>
                                                    <th>Granted Loan Amount</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                for ($x=0; $x<5; $x++){ ?>
                                                <tr>
                                                    <td class="text-center">
                                                        <a href="http://localhost:8000/admin/transactions/loans/{id}/details"><span class="fa fa-pencil" style="font-size: 0.8em;"></span></a>
                                                        &nbsp;
                                                        <a href="/admin/transactions/loans/{id}/disbursed_loan"><span class="fa fa-check" style="font-size: 0.8em;"></span></a>
                                                    </td>
                                                    <td>1234</td>
                                                    <td>1234</td>
                                                    <td>1234</td>
                                                    <td>1234</td>
                                                    <td>1234</td>
                                                    <td>1234</td>
                                                    <td>1234</td>
                                                </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

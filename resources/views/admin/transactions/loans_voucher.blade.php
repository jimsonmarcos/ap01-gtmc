@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a href="{{ route('loans')}}">Loans</a></li>
        <li><a class="link-effect" href="{{ route('loans_voucher') }}">Vouchers</a></li>
    </ol>

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <br />
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="employee_id">ID Number</label>
                                            <div class="">
                                                <select class="form-control" id="employeeID" name="" style="width: 100%;">

                                                </select>
                                            </div>
                                            @if(empty($user))
                                                <small>Search a user first to proceed.</small>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                @if(!empty($user))
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="employee_id">Employee ID</label>
                                            <div class="">
                                                <input class="form-control" type="text" id="employee_name" value="{{$user->Profile->id_number}}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="employee_name">Employee Name</label>
                                            <input class="form-control" type="text" id="employee_name" value="{{$user->name()}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <hr />
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Loan Request</label>
                                            <div>
                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                    <input type="radio" name="request_type" class="requestType" value="New" checked><span></span> New
                                                </label>
                                                <label class="css-input css-radio css-radio-primary">
                                                    <input type="radio" name="request_type" class="requestType" value="Existing"><span></span> Existing
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="loan_type">Loan Type</label>
                                            <select class="form-control" id="loan_type" name="loan_type">
                                                <option value="SSS">SSS LOAN</option>
                                                <option value="HDMF">HDMF LOAN</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="form-control-label">Loan Date</label>
                                            <div id="joinDate" class="input-group btn-mmddyyyy">
                                                <input type="text" class="form-control" name="date_request" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y') }}" required>
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3"><div class="form-group">
                                            <label for="">Voucher Number</label>
                                            <input type="text" class="form-control" name="cv_number" value="CV-">
                                        </div>
                                    </div>

                                    <div class="col-md-12">&nbsp;</div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="amount_paid">Loan Amount</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">P</span>
                                                <input class="form-control" type="text" id="loan_amount" name="loan_amount" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="amount_paid">Disbursed Amount</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">P</span>
                                                    <input class="form-control" type="text" id="disbursed_amount" name="disbursed_amount" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="amount_paid">Amortization</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">P</span>
                                                <input class="form-control" type="text" id="amortization" name="amortization" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12"><br /></div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="amount_paid">Beginning Balance</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">P</span>
                                                <input class="form-control" type="text" id="beginning_balance" name="beginning_balance" value="0.00" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="effective_month">Months to Pay</label>
                                            <select class="form-control" id="months_to_pay" name="months_to_pay">
                                                @for($x = 1; $x < 25; $x++)
                                                    <option value="{{ $x }}" @if($x == 24) selected @endif>{{ $x }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="effective_year">Effective Year (YYYY)</label>
                                            <input type="text" class="form-control" name="effective_year" value="{{ date('Y') }}"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' minlength="4" required />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="effective_month">Effective Month</label>
                                            <select class="form-control" id="effective_month" name="effective_month">
                                                @for($x = 1; $x < 13; $x++)
                                                    <option value="{{ $x }}" @if(date('m') + 1 > $x) disabled @endif>{{ $x }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12"></div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="remarks">Enter your Remarks</label>
                                            <textarea class="form-control"
                                                      id="remarks"
                                                      name="remarks"
                                                      rows="3"
                                                      placeholder="Remarks"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-right">
                                    <br><br>
                                    <a href="{{ url('admin/transactions/loans') }}" class="btn btn-danger">Cancel <span class="fa fa-ban" style="font-size: 0.8em;"></span></a>
                                    <button href="#" class="btn btn-success" onclick="return confirm('Are you sure you want to create this loan voucher?')">Submit <span class="fa fa-save" style="font-size: 0.8em;"></span></button>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(document).on('change', ".requestType", function() {
               if($(".requestType:checked").val() == 'New') {
                   $("#beginning_balance").prop('readonly', true);
               } else {
                   $("#beginning_balance").prop('readonly', false);
               }
            });

            $("#employeeID").select2({
                ajax: {
                    url: "{{ url('api/users/employees/Active') }}",
                    dataType: 'json',
                    delay: 300,
                    data: function (params) {
                        return {
                            q: params.term, // search term
//                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'Search User',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            function formatRepo (user) {
                if (user.loading) {
                    return user.name;
                }
//                console.log(user);

                var markup = "<span data-fname='"+ user.first_name +"' data-mname='"+ user.middle_name +"' data-lname='"+ user.last_name +"' data-email='"+ user.email+"' data-company='"+ user.company_code+"'>"+ user.id_number +"</span>"

                return markup;
            }

            function formatRepoSelection (user) {
                return user.name;
            }


            $("#employeeID").on("select2:select", function (evt) {
                console.log(evt.params.data);
                var user = evt.params.data;

                location.href = "{{url("/admin/transactions/loans/voucher?user_id=")}}" + user.user_id;

                // Commented on July 2, 2018
                // $("#employee_name").val(user.name);
                // $("#cost_center").val(user.cost_center);

//                $("#searchEmail").attr('value', user.email);
//                $("#searchCompanyCode").attr('value', user.principal);
//                $("#searchUserId").attr('value', user.user_id);
//                var adminusername = 'admin.' + user.first_name.substring(0, 1).toLowerCase() + user.middle_name.substring(0, 1).toLowerCase() + user.last_name.trim().toLowerCase().replace(' ', '_');
//                $("#searchUsername").attr('value', adminusername);
//                $("#adminUsername").text(adminusername);
            });

            $(document).on('keyup change', '#amortization, #months_to_pay', function() {
                updateBeginningBalance();
            });

            function updateBeginningBalance() {
                if($(".requestType:checked").val() == 'New') {
                    var amortization = parseFloat($("#amortization").val().replace(/\,/g, '')) || 0;
                    var months = parseInt($("#months_to_pay").find(':selected').val());

                    var beginningBalance = amortization * months;
                    console.log(amortization + ' ' + months + ' ' + beginningBalance);
                    $("#beginning_balance").val(beginningBalance.numberFormat(2));
                }
            }
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10">



                    <div class="row">
                        <form id="form-deposit" action="" class="form-horizontal push-10-t push-10" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="push-20 push-10-t">
                                <a href="{{ url("/admin/transactions/payments")}}"><i class="fa fa-chevron-left"></i> Back to Payments</a>
                            </div>

                            <div class="col-md-4 col-md-offset-2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="employee_id">ID Number</label>
                                                <div class="">
                                                    <select class="form-control" id="employeeID" name="" style="width: 100%;">

                                                    </select>
                                                </div>
                                                @if(empty($user))
                                                    <small>Search a user first to proceed.</small>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if(!empty($user))
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="payment_method">Transaction Type</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="transaction_type" name="transaction_type" required>
                                                    <option value="" disabled selected>Select Transaction Type</option>
                                                    <option value="COOP SHARE" @if(!empty($transaction_type) && $transaction_type == 'COOP SHARE') selected @endif>COOP SHARE</option>
                                                    <option value="NF" @if(!empty($transaction_type) && $transaction_type == 'NF') selected @endif>NOTARIAL FEE</option>
                                                    <option value="MF" @if(!empty($transaction_type) && $transaction_type == 'MF') selected @endif>MEMBERSHIP FEE</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="col-xs-12"></div>

                            <div class="col-md-4 col-md-offset-2">

                                <div class="row">

                                    @if(!empty($user))
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="employee_members_name">Employee/Members Name</label>
                                                <input class="form-control" type="text" id="employee_members_name" name="employee_members_name" value="{{ $user->Profile->namelfm() }}" disabled/>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    @if(!empty($user) && !empty($transaction_type))
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="" for="pay_period">Pay Period</label>
                                                <select name="pay_period" id="pay_period" class="form-control">
                                                    @if(!\App\OtherPayments::where(['user_id' => $user->id, 'pay_period' => date('Ym').'A', 'transaction_type' => $transaction_type])->exists())
                                                    <option value="{{ date('Ym') }}A" data-date="@if($user->role == 'Employee'){{ date('Y-m-').$user->Profile->Compensation->PayrollGroup->payroll_date_a }} @else {{ date('Y-m-15') }} @endif">{{ date('Ym') }}A</option>
                                                    @endif
                                                    @if(!\App\OtherPayments::where(['user_id' => $user->id, 'pay_period' => date('Ym').'B', 'transaction_type' => $transaction_type])->exists())
                                                    <option value="{{ date('Ym') }}B" data-date="@if($user->role == 'Employee'){{ date('Y-m-').$user->Profile->Compensation->PayrollGroup->payroll_date_b }} @else {{ date('Y-m-t') }} @endif">{{ date('Ym') }}B</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="" for="amount">Amount</label>
                                                <input type="text" class="form-control" id="amount" name="amount" value="@if($transaction_type == 'COOP SHARE'){{$user->Profile->coop_share}} @elseif($transaction_type == 'NF'){{\App\Keyval::where('key', 'notarial_fee')->first()->value}}@elseif($transaction_type == 'MF'){{\App\Keyval::where('key', 'membership_fee')->first()->value}}@endif" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="additional_payment">Additional Payment</label>
                                                <input class="form-control" type="number" id="additionalPayment" name="additional_payment"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="bank_account">Bank Account</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="bank_id" name="bank_id" disabled>
                                                    @foreach(\App\Banks::orderBy('bank_account')->get() as $bank)
                                                        <option value="{{ $bank->id }}">{{ $bank->bank_account }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="reference_number">Reference Number</label>
                                                <input class="form-control" type="text" id="reference_number" name="reference_number" disabled />
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="ar_number">AR Number</label>
                                                <input class="form-control" type="number" id="ar_number" name="ar_number" value="{{ $arNumber }}" required/>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="payment_method">Payment Method</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="payment_method" name="payment_method" required>
                                                    <option value="Cash">CASH</option>
                                                    <option value="Deposit">DEPOSIT</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="form-control-label">Payment Date</label>
                                                <div id="payment_date_dp" class="input-group btn-mmddyyyy ">
                                                    <input type="text" class="form-control" id="payment_date" name="payment_date" value="{{ date('m/d/Y') }}" placeholder="mm/dd/yyyy" required>
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="interest"> Interest</label>
                                                <input class="form-control" type="text" id="interest" name="interest" @if(in_array($transaction_type, ['NF', 'MF'])) disabled @else readonly @endif  />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="amount_paid"> Total Amount Paid</label>
                                                <input class="form-control" type="text" id="total_amount_paid" name="total_amount_paid" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" readonly />
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="loan_installment_code">Remarks</label>
                                                <textarea name="remarks" rows="5"
                                                          class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>

                            <div class="col-md-12"></div>

                            <div class="col-md-10 text-right">
                                <button class="btn btn-warning push-5-r push-10" type="reset"><i class="fa fa-refresh"></i> Reset</button>
                                <button class="btn btn-success push-5-r push-10" onclick="return confirm('Are you sure you want to add this payment?')"><i class="fa fa-check"></i> Submit</button>
                            </div>
                            @endif
                        </form>

                        <input type="hidden" id="coop_share_interest" value="{{ \App\Keyval::where('key', 'coop_share_interest')->first()->value }}">
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

            @if(!empty($user))
            var user = $.parseJSON('{!! $user->toJson() !!}');
            console.log(user);
            @endif

            $("#employeeID").select2({
                placeholder: 'Search GTMC User here..',
                ajax: {
                    url: "{{ url('api/users/all') }}",
                    dataType: 'json',
                    delay: 300,
                    data: function (params) {
                        return {
                            q: params.term, // search term
//                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            function formatRepo (user) {
                if (user.loading) {
                    return user.name;
                }
//                console.log(user);

                var markup = "<span data-fname='"+ user.first_name +"' data-mname='"+ user.middle_name +"' data-lname='"+ user.last_name +"' data-email='"+ user.email+"' data-company='"+ user.company_code+"'>"+ user.id_number +"</span>"

                return markup;
            }

            function formatRepoSelection (user) {
                return user.id_number;
            }


            $("#employeeID").on("select2:select", function (evt) {
                var user = evt.params.data;
                console.log(user);

                location.href = "{{url("/admin/transactions/payments/add?user_id=")}}" + user.user_id;
            });


            @if(!empty($user))
            $("#transaction_type").on("change", function () {
                var transactionType = $("#transaction_type").val();

                location.href = "{{url("/admin/transactions/payments/add?user_id={$user->id}")}}&transaction_type=" + transactionType;
            });

            $(document).on('change', "#payment_method", function() {
                if($(this).find(':selected').val() == 'Deposit') {
                    $("#bank_id, #reference_number").removeAttr('disabled').attr('required', true);
                } else {
                    $("#bank_id, #reference_number").removeAttr('required').attr('disabled', true);
                }
            });


            updateTotalAmountPaid();

            $("#amount, #interest").on('keypress keyup change', function() {
                updateTotalAmountPaid();
            });

            $(document).on('dp.change', '#payment_date_dp', function() {
                updateTotalAmountPaid();
            });

//            $("#paymentDate").val();

            $("#additionalPayment").on('keypress keyup change', function() {
                updateTotalAmountPaid();
            });

            function updateTotalAmountPaid()
            {
                var amount = parseFloat($("#amount").val()) || 0;
                var interest = parseFloat($("#interest").val()) || 0;

                if($("#transaction_type").val() == 'COOP SHARE') {
                    var payment_date = moment($("#payment_date").val()).add(5, 'd').format("YYYY-MM-DD");
                    var pay_period = moment($("#pay_period").find(':selected').attr('data-date')).format("YYYY-MM-DD");
//                    console.log(payment_date);
//                    console.log(pay_period);
                    if(payment_date > pay_period) {
                        $("#interest").val(amount * (parseFloat($("#coop_share_interest").val() * .01)));
                    } else {
                        $("#interest").val(0);
                    }
                }
                var interest = parseFloat($("#interest").val()) || 0;


                var additionalPayment = parseFloat($("#additionalPayment").val()) || 0;
                $("#total_amount_paid").val( (amount + interest + additionalPayment).toFixed(2) );
            }
            @endif
        })
    </script>
@endsection

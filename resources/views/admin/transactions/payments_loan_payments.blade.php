@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('payments')}}">Payment</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <br />
                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#"> Loan Payments</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('withdrawals') }}"> Withdrawals</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('coop_shares') }}"> Coop Shares</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('notarial_fee') }}"> Notarial Fee</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('membership_fee') }}"> Membership Fee</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/payments/add") }}"><i class="fa fa-money"></i> Add Payment</a>
                        </li>
                    </ul>
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            <h4>Unpaid Loans</h4>
                            <br>

                            {{--<div class="text-right">--}}
                                {{--<a href="{{ route('transactions_loan_add_payment') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Loan Payment</a>--}}
                            {{--</div>--}}


                            <table id="unpaidLoans" class="table">
                                <thead>
                                    <tr class="text-center">
                                        <th>CLA Number</th>
                                        <th>ID Number</th>
                                        <th>Employee Name</th>
                                        <th>Loan Type</th>
                                        {{--<th>Due Date</th>--}}
                                        <th>Amortization</th>
                                        <th>Outstanding Amount</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            var table = $('#unpaidLoans').DataTable({
                "pageLength": 100,
                "processing": true,
                "serverSide": true,
                // "bFilter": false,
                searchDelay: 350,
                "searching": true,
                // "dom": "ltip",  // Remove global search box
                "ajax":{
                    "url": "{{ url('api/datatables/unpaid-loans/') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "cla_number" },
                    { "data": "id_number" },
                    { "data": "name" },
                    { "data": "loan_type" },
                    // { "data": "due_date" },
                    { "data": "amortization" },
                    { "data": "outstanding_amount" },
                    { "data": "action" }
                ]

            });
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('invoices')}}">Invoices</a></li>
        <li><a class="link-effect" href="{{ route('invoices_item')}}">Item</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-6">
                                    <a href="{{ route('invoices_create_item') }}" class="btn btn-primary">Add Item <span class="fa fa-plus" style="font-size: 0.8em;"></span></a>
                                </div>
                                <div class="col-md-12">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Action</th>
                                            <th>Item Name</th>
                                            <th>Company Code</th>
                                            <th>Payroll Cycle</th>
                                            <th>Head Count</th>
                                            <th>Total Hours</th>
                                            <th>Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach(\App\InvoiceItemList::whereNotIn('id', \App\InvoiceItems::all()->pluck('invoice_item_id'))->orderByDesc('id')->get() as $invoiceItem)
                                        <tr>
                                            <td class="text-center">
                                                <a href="#"><span class="fa fa-pencil" style="font-size: 0.8em;"></span></a>
                                                &nbsp;
                                                <a href="#"><span class="fa fa-print" style="font-size: 0.8em;"></span></a>
                                            </td>
                                            <td>{{ $invoiceItem->item_name }}</td>
                                            <td>{{ $invoiceItem->Company->company_code }}</td>
                                            <td>{{ $invoiceItem->PayrollCycle->payroll_code }}</td>
                                            <td>{{ $invoiceItem->total_head_count }}</td>
                                            <td>{{ $invoiceItem->total_hours }}</td>
                                            <td>{{ $invoiceItem->billing_amount }}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

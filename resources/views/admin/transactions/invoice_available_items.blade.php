@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('invoices')}}">Invoices</a></li>
        <li><a class="link-effect" href="{{ route('invoices_item')}}">Item</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ url('/admin/transactions/invoices/create_invoice') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <br />
                                    <div class="col-md-12">
                                        <table id="invoiceItems" class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th><input id="selectAll" type="checkbox"></th>
                                                <th>Item Name</th>
                                                <th>Company Code</th>
                                                <th>Payroll Cycle</th>
                                                <th>Head Count</th>
                                                <th>Total Hours</th>
                                                <th>Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach(\App\InvoiceItemList::whereNotIn('id', \App\InvoiceItems::all()->pluck('invoice_item_id'))->orderByDesc('id')->get() as $invoiceItem)
                                                <tr>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="invoice_item[{{$invoiceItem->id}}]">
                                                    </td>
                                                    <td>{{ $invoiceItem->item_name }}</td>
                                                    <td>{{ $invoiceItem->Company->company_code }}</td>
                                                    <td>{{ $invoiceItem->PayrollCycle->payroll_code }}</td>
                                                    <td>{{ $invoiceItem->total_head_count }}</td>
                                                    <td>{{ $invoiceItem->total_hours }}</td>
                                                    <td>{{ $invoiceItem->billing_amount }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary"><i class="fa fa-save"></i> SUBMIT</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#selectAll").click(function(e) {

                $('#invoiceItems tbody input:checkbox').not(this).prop('checked', this.checked);

            });
        })
    </script>
@endsection

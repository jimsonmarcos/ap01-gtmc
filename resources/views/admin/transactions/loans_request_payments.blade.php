@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10">
        <li>Transactions</li>
        <li><a href="{{ route('loans')}}">Loans</a></li>
        <li>Details</li>
    </ol>

                <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs" id="" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/loans/{$loan->id}") }}"><i class="fa fa-info-circle"></i> Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/loans/{$loan->id}/loan-amortization-schedule") }}"><i class="fa fa-calendar"></i> Loan Amortization Schedule</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/loans/{$loan->id}/payment-schedule") }}"><i class="fa fa-calendar"></i> Payment Schedule</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ url("admin/transactions/loans/{$loan->id}/payments") }}"><i class="fa fa-money"></i> Payments</a>
                        </li>
                    </ul>
                </div>

                <div class="col-sm-12">
                    <div class="row">

                        <div class="col-md-10 col-md-offset-1">
                            <?php $paymentSchedules = \App\LoanPaymentSchedules::where(['loan_id' => $loan->id]); ?>
                            @if((($paymentSchedules->sum('amortization') + $paymentSchedules->sum('lapsed_interest')) - ($payments->sum('total_amount_paid'))) > 0)
                            <div class="text-right push-10 push-10-t">
                                <a href="{{ url("admin/transactions/loans/{$loan->id}/payments/add") }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Payment</a>
                            </div>
                            @endif

                            @if($payments->count() > 0)
                                    <br>
                            <table class="table table-bordered" style="width: 300px;">
                                <tr>
                                    <th class="text-center" width="170">TOTAL AMOUNT PAID</th>
                                    <th class="text-right">{{ number_format($payments->sum('total_amount_paid'), 2) }}</th>
                                </tr>
                            </table>

                            <br>
                            <table class="table table-bordered push-10-t">
                                <thead>
                                    <tr>
                                        <th>TRANSACTION CODE</th>
                                        <th>AR NUMBER</th>
                                        <th>PAY PERIOD</th>
                                        <th>DUE DATE</th>
                                        <th>PAYMENT DATE</th>
                                        <th>OVERDUE AMOUNT</th>
                                        <th>AMOUNT PAID</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($payments as $payment)
                                        <?php $paymentSchedule = \App\LoanPaymentSchedules::where(['loan_id' => $payment->loan_id, 'pay_period' => $payment->pay_period])->first() ?>

                                        <tr>
                                            <th>{{ $payment->transaction_id }}</th>
                                            <th>{{ $payment->ar_number }}</th>
                                            <td>{{ $payment->pay_period }}</td>
                                            <td>{{ date('m/d/Y', strtotime($paymentSchedule->due_date)) }}</td>
                                            <td>{{ date('m/d/Y', strtotime($payment->payment_date)) }}</td>
                                            <td class="text-right">{{ number_format($paymentSchedule->amortization + $paymentSchedule->lapsed_interest, 2) }}</td>
                                            <td class="text-right">{{ number_format($payment->total_amount_paid, 2) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                            <p>No payments made yet.</p>
                            @endif
                        </div>

                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection

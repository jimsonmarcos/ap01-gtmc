@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10">
        <li>Transactions</li>
        <li><a href="{{ route('loans')}}">Loans</a></li>
        <li>Details</li>
    </ol>

                <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs" id="" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ url("admin/transactions/loans/{$loan->id}") }}"><i class="fa fa-info-circle"></i> Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/loans/{$loan->id}/payment-schedule") }}"><i class="fa fa-calendar"></i> Payment Schedule</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/loans/{$loan->id}/loan-amortization-schedule") }}"><i class="fa fa-calendar"></i> Loan Amortization Schedule</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/loans/{$loan->id}/payments") }}"><i class="fa fa-money"></i> Payments</a>
                        </li>
                    </ul>
                </div>

                <div class="col-sm-12 push-10-t">
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>Complete Name</th>
                                        <td>{{ $loan->User->Profile->name() }}</td>
                                    </tr>
                                    <tr>
                                        <th>ID Number</th>
                                        <td>{{ $loan->User->Profile->id_number }}</td>
                                    </tr>
                                    <tr>
                                        <th>Cost Center</th>
                                        <td>{{ $loan->User->Profile->CostCenter->cost_center }}</td>
                                    </tr>
                                    <tr>
                                        <th>CLA Number</th>
                                        <td>{{ $loan->cla_number }}</td>
                                    </tr>
                                    <tr>
                                        <th>Date Request</th>
                                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $loan->date_request)->format('m/d/Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Loan Type</th>
                                        <td>{{ $loan->loan_type }} </td>
                                    </tr>
                                    <tr>
                                        <th>CV Number</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>CV Date</th>
                                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $loan->date_request)->format('m/d/Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Cheque Number</th>
                                        <td>{{ $loan->cv_number }}</td>
                                    </tr>
                                    <tr>
                                        <th>Remarks</th>
                                        <td>{{ $loan->remarks }}</td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-4">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>Month to Pay</th>
                                        <td>{{ $loan->months_to_pay }}</td>
                                    </tr>
                                    <tr>
                                        <th>Effective Year/Month</th>
                                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $loan->effective_date)->format('F Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Loan Amount</th>
                                        <td>{{ number_format($loan->granted_loan_amount, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Disbursed Amount</th>
                                        <td>{{ number_format($loan->disbursed_amount, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Amortization</th>
                                        <td>{{ number_format($loan->amortization, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Beginning Balance</th>
                                        <td>{{ number_format($loan->beginning_balance, 2) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection

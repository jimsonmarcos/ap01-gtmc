@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('payments')}}">Payment</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <br />
                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link " href="{{ url('/admin/transactions/payments') }}"> Loan Payments</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ route('withdrawals') }}"> Withdrawals</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('coop_shares') }}"> Coop Shares</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('notarial_fee') }}"> Notarial Fee</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('membership_fee') }}"> Membership Fee</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/payments/add") }}"><i class="fa fa-money"></i> Add Payment</a>
                        </li>
                    </ul>
                    <div class="row">
                        <div class="col-md-12 push-20-t">

                            <h4>Recent Withdrawals</h4>

                            {{--<div class="text-right">--}}
                                {{--<a href="{{ route('withdrawals_add') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Withdrawal</a>--}}
                            {{--</div>--}}

                            <table class="table">

                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

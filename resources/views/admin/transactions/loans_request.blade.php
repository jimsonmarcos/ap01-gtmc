@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a href="{{ route('loans')}}">Loans</a></li>
        <li><a class="link-effect" href="{{ route('loans_request') }}">Request</a></li>
    </ol>
     <!-- Dashboard Counts Section-->
     <section class="dashboard-counts no-padding-bottom">
         <div class="container-fluid">
             <div class="row">
                 <?php $totalOutstandingBalance = 0; ?>
                 @if(!empty($unpaidLoans))
                 <div class="row">
                     <div class="col-md-12">
                         <div class="block">
                             <div class="block-content">
                                 <table class="table datatable-unpaid-loans">
                                     <thead>
                                        <tr>
                                            <th></th>
                                            <th width="85">LOAN CODE</th>
                                            <th>MONTHS TO PAY</th>
                                            <th>REQUEST AMOUNT</th>
                                            <th>INTEREST</th>
                                            <th>GRANTED LOAN AMOUNT</th>
                                            <th>AMORTIZATION</th>
                                            <th>LOAN PAYMENTS</th>
                                            <th>LOAN PAYMENTS LEFT</th>
                                            <th>OUTSTANDING BALANCE</th>
                                        </tr>
                                     </thead>
                                     <tbody>
                                     <?php $x = 1; ?>
                                     @foreach($unpaidLoans as $unpaidLoan)
                                     <?php
                                         $totalPayment = $unpaidLoan->total_amount_paid;
                                         $paymentSchedules = \App\LoanPaymentSchedules::where('loan_id', $unpaidLoan->id)->get();
                                         $paymentsLeft = ($unpaidLoan->months_to_pay * 2) - $unpaidLoan->Payments->count();
                                     ?>
                                        <tr>
                                            <?php /* <td><input type="radio" class="existingLoan" name="existing_loan" value="{{$unpaidLoan->cla_number}}" data-outstanding-balance='{{($paymentSchedules->sum('amortization') + $paymentSchedules->sum('lapsed_interest')) - $totalPayment}}' @if($unpaidLoans->count() > 2) disabled @endif @if(($unpaidLoan->months_to_pay * 2 ) - $unpaidLoan->Payments->count() > 2) disabled @else @if($unpaidLoans->count() == 1) checked @endif @endif></td> */?>
                                            <td><input type="radio" class="existingLoan" name="existing_loan" value="{{$unpaidLoan->cla_number}}" data-outstanding-balance='{{($paymentSchedules->sum('amortization') + $paymentSchedules->sum('lapsed_interest')) - $totalPayment}}' @if($unpaidLoans->count() > 2)  @endif @if(($unpaidLoan->months_to_pay * 2 ) - $unpaidLoan->Payments->count() > 2)  @else @if($unpaidLoans->count() == 1) checked @endif @endif></td>
                                            <td>{{ $unpaidLoan->cla_number }}</td>
                                            <td>{{ $unpaidLoan->months_to_pay }}</td>
                                            <td>@if($unpaidLoan->is_voucher == 'N') {{ number_format($unpaidLoan->request_amount, 2) }} @else {{ number_format($unpaidLoan->granted_loan_amount, 2) }} @endif</td>
                                            <td>{{ $unpaidLoan->interest }}</td>
                                            <td>{{ $unpaidLoan->granted_loan_amount }}</td>
                                            <td>{{ $unpaidLoan->amortization }}</td>
                                            <td>{{ number_format($totalPayment, 2) }}</td>
                                            <td>{{ $paymentsLeft }}</td>
                                            <td>{{ number_format($unpaidLoan->amortization * $paymentsLeft, 2) }}</td>
                                        </tr>
                                     </tbody>
                                     <?php
                                     $x++;
                                     $totalOutstandingBalance += $unpaidLoan->amortization * $paymentsLeft;
                                     ?>
                                     @endforeach
                                 </table>
                             </div>
                         </div>
                     </div>
                 </div>
                 @endif



                 @if(!empty($user))
                 <?php
                 $payrollCoopShare = \App\PayrollSummary::where('user_id', $user->id)->sum('coop_share');
                 $capitalShare = \App\OtherPayments::where('user_id', $user->id)->where('transaction_type', 'COOP SHARE')->get()->sum('total_amount_paid');
                 $capitalShare += $payrollCoopShare;
                 $loanablePercentage = (float) \App\Keyval::where('key', 'loanable_percentage')->first()->value;
                 ?>
                 <div class="row">

                     <div class="col-md-12">
                         <h2 class="content-heading">Checking Loanable Amount</h2>
                     </div>

                     <div class="col-md-6">
                         <div class="form-group">
                             <div class="col-xs-12">
                                 <label for="capital_shares">Capital Share</label>
                                 <input class="form-control" type="text" id="capital_shares" value="{{ number_format($capitalShare, 2) }}"  disabled />
                             </div>
                         </div>
                     </div>

                     <div class="col-md-6">
                         <div class="form-group">
                             <div class="col-xs-12">
                                 <label for="existing_loans">Existing Loans</label>
                                 <input class="form-control" type="text" id="existing_loans" value="{{ number_format($totalOutstandingBalance, 2) }}" disabled />
                             </div>
                         </div>
                     </div>

                     <div class="col-md-6">
                         <div class="form-group">
                             <div class="col-xs-12">
                                 <label for="loanable_%">Loanable %</label>
                                 <input class="form-control" type="text" id="loanable_%" value="{{ $loanablePercentage }}%" disabled />
                             </div>
                         </div>
                     </div>

                     <div class="col-md-6">
                         <div class="form-group">
                             <div class="col-xs-12">
                                 <label for="loanable_amount">Loanable Amount</label>
                                 <input class="form-control" type="text" id="loanable_amount" value="{{ number_format(($capitalShare - $totalOutstandingBalance) * ($loanablePercentage * .01), 2) }}" disabled />
                             </div>
                         </div>
                     </div>

                     <div class="col-md-12"><div class="col-md-12"><hr></div></div>
                 </div>

                 @endif



                 <div class="col-sm-12">
                     <div class="card">
                         <div class="card-body">
                             <form action="" method="POST" enctype="multipart/form-data">
                                 {{ csrf_field() }}

                                 <br />
                                 <div class="row">
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="employee_id">ID Number</label>
                                             <div class="">
                                                 <select class="form-control" id="employeeID" name="" style="width: 100%;">

                                                 </select>
                                             </div>
                                             @if(empty($user))
                                                 <small>Search a user first to proceed.</small>
                                             @endif
                                         </div>
                                     </div>
                                 </div>

                                 @if(!empty($user))
                                 <div class="row">
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="employee_id">ID Number</label>
                                             <input class="form-control" type="text" id="employee_name" value="{{$user->Profile->id_number}}" disabled>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="employee_name">Complete Name</label>
                                             <input class="form-control" type="text" id="employee_name" value="{{$user->name()}}" disabled>
                                         </div>
                                     </div>
                                     {{--<div class="col-md-3">--}}
                                         {{--<div class="form-group">--}}
                                             {{--<label for="cla_number">CLA Number</label>--}}
                                             {{--<input class="form-control" type="text" id="cla_number" name="">--}}
                                         {{--</div>--}}
                                     {{--</div>--}}
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="cost_center">Cost Center</label>
                                             <input class="form-control" type="text" id="cost_center" value="{{$user->Profile->CostCenter->cost_center}}" disabled>
                                         </div>
                                     </div>
                                     <div class="col-md-12">
                                         <hr />
                                     </div>

                                     <div class="col-md-12" style="display: none;">
                                         <table class="table table-striped">
                                             <thead>
                                                <tr>
                                                    <th>Loan Code</th>
                                                    <th>Months To Pay</th>
                                                    <th>Request Amount</th>
                                                    <th>Interest</th>
                                                    <th>Granted Loan Amount</th>
                                                    <th>Amortization</th>
                                                    <th>Loan Payments</th>
                                                    <th>Outstanding Balance</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                                <tr>

                                                </tr>
                                             </tbody>
                                         </table>
                                     </div>
                                 </div>
                                 <div class="row">
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <div class="">
                                                 <label class="form-control-label">Date Request</label>
                                                 <div id="joinDate" class="input-group btn-mmddyyyy">
                                                     <input type="text" class="form-control" name="date_request" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y') }}" required>
                                                     <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="employee_id">Loan Type</label>
                                             <select name="loan_type_id" id="loanType" class="form-control">
                                                 @foreach(\App\LoanTypes::orderBy('type')->get() as $loanType)
                                                     <option value="{{ $loanType->id }}"
                                                             data-amount="{{ $loanType->amount }}"
                                                             data-interest="{{ $loanType->interest * .01 }}"
                                                             data-servicecharge="{{ $loanType->service_charge * .01 }}"
                                                             data-savings="{{ $loanType->savings * .01 }}">{{ $loanType->type }}</option>
                                                 @endforeach
                                             </select>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="months_to_pay">Month to Pay</label>
                                             <select name="months_to_pay" id="months_to_pay" class="form-control">
                                             @for($x = 1; $x < 13; $x++)
                                                     <option value="{{ $x }}">{{ $x }}</option>
                                                 @endfor
                                             </select>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="amount_paid">Request Amount</label>
                                             <div class="input-group">
                                                 <span class="input-group-addon">₱</span>
                                                 <input class="form-control" type="text" id="request_amount" name="request_amount" required>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="">Service Charge %</label>
                                             <div class="input-group">
                                                 <input class="form-control text-right" type="text" id="service_charge_p" name="service_charge_percent" value="{{ \App\LoanTypes::orderBy('type')->get()->first()->service_charge }}" readonly>
                                                 <span class="input-group-addon">%</span>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="service_charge">Service Charge</label>
                                             <div class="input-group">
                                                 <span class="input-group-addon">₱</span>
                                                 <input class="form-control" type="text" id="service_charge" name="service_charge" value="0" readonly>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="savings_%">Savings %</label>
                                             <div class="input-group">
                                                 <input class="form-control text-right" type="text" id="savings_p" name="savings_percent" value="{{ \App\LoanTypes::orderBy('type')->get()->first()->savings }}" readonly>
                                                 <span class="input-group-addon">%</span>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="savings">Savings</label>
                                             <div class="input-group">
                                                 <span class="input-group-addon">₱</span>
                                                 <input class="form-control" type="text" id="savings" name="savings" value="0" readonly>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="loan_interest_%">Loan Interest %</label>
                                             <div class="input-group">
                                                 <input class="form-control text-right" type="text" id="interest_p" name="interest_percent" value="{{ \App\LoanTypes::orderBy('type')->get()->first()->interest }}" readonly>
                                                 <span class="input-group-addon">%</span>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="interest">Interest</label>
                                             <div class="input-group">
                                                 <span class="input-group-addon">₱</span>
                                                 <input class="form-control" type="text" id="interest" name="interest" value="0" readonly>
                                             </div>
                                         </div>
                                     </div>

                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="overall_interest">Overall Interest</label>
                                             <div class="input-group">
                                                 <span class="input-group-addon">₱</span>
                                                 <input class="form-control" type="text" id="overall_interest" name="overall_interest" value="0" readonly>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="additional_deduction">CLA Number Reference</label>
                                             <?php // @if(!empty($unpaidLoans) && $unpaidLoans->count() == 1){{$unpaidLoan->cla_number}}@endif ?>
                                             <input type="text" class="form-control" name="cla_number_reference" id="claNumberReference" value="" readonly>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="additional_deduction">Additional Deduction</label>
                                             <div class="input-group">
                                                 <span class="input-group-addon">₱</span>
                                                 <?php // @if(!empty($unpaidLoans) && $unpaidLoans->count() == 1){{$unpaidLoan->granted_loan_amount - $totalPayment}}@endif ?>
                                                 <input class="form-control" type="text" id="additional_deduction" name="additional_deduction" value="" readonly>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="amortization">Amortization</label>
                                             <div class="input-group">
                                                 <span class="input-group-addon">₱</span>
                                                 <input class="form-control" type="text" id="amortization" name="amortization" value="0" readonly>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="granted_loan_amount">Granted Loan Amount</label>
                                             <div class="input-group">
                                                 <span class="input-group-addon">₱</span>
                                                 <input class="form-control" type="text" id="granted_loan_amount" name="granted_loan_amount" value="0" readonly>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-md-3">
                                         <div class="form-group">
                                             <label for="disbursed_amount">Disbursed Amount</label>
                                             <div class="input-group">
                                                 <span class="input-group-addon">₱</span>
                                                 <input class="form-control" type="text" id="disbursed_amount" name="disbursed_amount" value="0" readonly>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="col-md-12 text-right">
                                     <br><br>
                                     <a href="{{ url('/admin/transactions/loans') }}" class="btn btn-danger">Cancel <span class="fa fa-ban" style="font-size: 0.8em;"></span></a>
                                     <button id="submitLoanRequest" href="#" class="btn btn-success" onclick="return confirm('Are you sure you want to create this loan?')">Create <span class="fa fa-check" style="font-size: 0.8em;"></span></button>
                                 </div>
                                 @endif
                             </form>


                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#employeeID").select2({
                placeholder: {
                    id: '-1', // the value of the option
                    id_number: @if(!empty($user)) '{{ $user->Profile->id_number }}' @else "Search user here..." @endif
                },
                ajax: {
                    url: "{{ url('api/users/all/Active') }}",
                    dataType: 'json',
                    delay: 300,
                    data: function (params) {
                        return {
                            q: params.term, // search term
//                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            function formatRepo (user) {
                if (user.loading) {
                    return user.name;
                }
//                console.log(user);

                var markup = "<span data-fname='"+ user.first_name +"' data-mname='"+ user.middle_name +"' data-lname='"+ user.last_name +"' data-email='"+ user.email+"' data-company='"+ user.company_code+"'>"+ user.id_number +"</span>"

                return markup;
            }

            function formatRepoSelection (user) {
                return user.id_number;
            }


            $("#employeeID").on("select2:select", function (evt) {
//                console.log(evt.params.data);
                var user = evt.params.data;
//                $("#employee_name").val(user.name);
//                $("#cost_center").val(user.cost_center);
                console.log(user);
                location.href = "{{url("/admin/transactions/loans/request?user_id=")}}" + user.user_id;

//                $("#searchEmail").attr('value', user.email);
//                $("#searchCompanyCode").attr('value', user.principal);
//                $("#searchUserId").attr('value', user.user_id);
//                var adminusername = 'admin.' + user.first_name.substring(0, 1).toLowerCase() + user.middle_name.substring(0, 1).toLowerCase() + user.last_name.trim().toLowerCase().replace(' ', '_');
//                $("#searchUsername").attr('value', adminusername);
//                $("#adminUsername").text(adminusername);
            });



            $("#request_amount, #loanType, #additional_deduction, #months_to_pay ").on('keyup change', function() {
                var loanType = $("#loanType").find(':selected');

                if(loanType.val() == 4) {
                    $("#request_amount").attr('readonly', true);
                    $("#request_amount").val(loanType.attr('data-amount'));
                } else {
                    $("#request_amount").removeAttr('readonly');
                }

                $("#service_charge_p").val( (parseFloat(loanType.attr('data-servicecharge')) * 100).toFixed(2) );
                $("#savings_p").val( (parseFloat(loanType.attr('data-savings')) * 100).toFixed(2) );
                $("#interest_p").val( (parseFloat(loanType.attr('data-interest')) * 100).toFixed(2) );

                var requestAmount = parseFloat($("#request_amount").val()) || 0;

                var additionalDeduction = parseFloat($("#additional_deduction").val()) || 0;
                var monthsToPay = parseFloat($("#months_to_pay").find(':selected').text());
                var serviceCharge = parseFloat(loanType.attr('data-servicecharge'));
                var savings = parseFloat(loanType.attr('data-savings'));
                var interest = parseFloat(loanType.attr('data-interest'));

                $("#service_charge").val( (requestAmount * serviceCharge).toFixed(2) );
                $("#savings").val( (requestAmount * savings).toFixed(2) );
                $("#interest").val( (requestAmount * interest).toFixed(2) );
                $("#overall_interest").val( ((requestAmount * interest) * monthsToPay).toFixed(2) );
                $("#granted_loan_amount").val( (((requestAmount * interest) * monthsToPay) + requestAmount).toFixed(2) );
                $("#amortization").val( (((((requestAmount * interest) * monthsToPay) + requestAmount) / monthsToPay) / 2).toFixed(2) );

                $("#disbursed_amount").val( (requestAmount - (requestAmount * serviceCharge) - (requestAmount * savings) - additionalDeduction).toFixed(2) );

                var loanableAmount = parseFloat($("#loanable_amount").val().replace(/\,/g, ''));

                if(requestAmount > loanableAmount) {
                    $("#submitLoanRequest").attr('disabled', true);
                } else {
                    $("#submitLoanRequest").attr('disabled', false);
                }


                $("#loanable_amount").val().replace(/\,/g, '');
                console.log();
            });

            $(document).on('click', ".existingLoan", function(e) {
//                console.log('Wow!');
                $("#additional_deduction").val(parseFloat($(this).attr('data-outstanding-balance')).toFixed(2));
                $("#claNumberReference").val($(this).val());
            });

            $(".datatable-unpaid-loans").dataTable({
                bLengthChange: false,
                sorting: false,
                searching: false,
                paging: false
            });
        })
    </script>
@endsection

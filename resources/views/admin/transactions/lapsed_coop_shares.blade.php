@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10">
        <li><a class="nav-link" href="{{ url("admin/dashboard") }}"><i class="fa fa-home"></i> Dashboard</a></li>
        <li>COOP Share Lapses</li>
    </ol>

                <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12 push-10-t">
                    <div class="row">

                        <div class="col-md-12">
                            <table class="table dt table-bordered table-condensed">
                                <thead>
                                    <tr>
                                        <th class="">PAYROLL PERIOD</th>
                                        <th class="">NAME</th>
                                        <th class="">ID NUMBER</th>
                                        <th>CONTRIBUTION PER PAYDAY</th>
                                    </tr>
                                </thead>


                                <tbody>

                                <?php $loanType = \App\LoanTypes::all(); ?>
                                @foreach($paymentSchedules as $paymentSchedule)

                                    <tr>
                                        <th class="">{{ $paymentSchedule->payroll_period }}</th>
                                        <th class="">{{ "{$paymentSchedule->first_name} {$paymentSchedule->last_name}" }}</th>
                                        <th class="">{{ "{$paymentSchedule->id_number}" }}</th>
                                        <th class="text-right">{{ number_format($paymentSchedule->coop_share, 2) }}</th>
                                    </tr>
                                @endforeach
                                </tbody>



                            </table>
                        </div>

                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('.dt').DataTable({
                "sorting": [],
                "pageLength": 25,
                order: [[ 0, 'desc' ]]
            });
        })
    </script>
@endsection

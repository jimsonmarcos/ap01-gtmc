@extends('layouts.app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10">



                    <div class="row">
                        <form id="form-deposit" action="" class="form-horizontal push-10-t push-10" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="push-20 push-10-t">
                                <a href="{{ url()->previous() }}"><i class="fa fa-chevron-left"></i> Back to Payments</a>
                            </div>

                            <div class="col-xs-12"></div>

                            <div class="col-md-4 col-md-offset-2">

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="employee_members_name">Employee/Members Name</label>
                                                <input class="form-control" type="text" id="" name="" value="{{ $payment->Profile->namelfm() }}" disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="" for="pay_period">Pay Period</label>
                                                <input class="form-control" type="text" id="" name="" value="{{ $payment->pay_period }}" disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="" for="amount">Amount</label>
                                                <input class="form-control" type="text" id="" name="" value="{{ number_format($payment->amount, 2) }}" disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="additional_payment">Additional Payment</label>
                                                <input class="form-control" type="text" id="" name="" value="{{ number_format($payment->additional_payment, 2) }}" disabled/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="bank_account">Bank Account</label>
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" id="" name="" value="@if(!empty($payment->bank_id)) {{ $payment->Bank->bank_account }}@endif"  disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="reference_number">Reference Number</label>
                                                <input class="form-control" type="text" id="" name="" value="{{ $payment->reference_number }}" disabled/>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="payment_method">Payment Method</label>
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" id="" name="" value="{{ $payment->payment_method }}" disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="form-control-label">Payment Date</label>
                                                <input class="form-control" type="text" id="" name="" value="{{ date('m/d/Y', strtotime($payment->payment_date)) }}" disabled/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="interest"> Interest</label>
                                                <input class="form-control" type="text" id="" name="" value="{{ number_format($payment->interest, 2) }}" disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="amount_paid"> Total Amount Paid</label>
                                                <input class="form-control" type="text" id="" name="" value="{{ number_format($payment->total_amount_paid, 2) }}" disabled/>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="loan_installment_code">Remarks</label>
                                                <input class="form-control" type="text" id="" name="" value="{{ $payment->remarks }}" disabled/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="ar_number">AR Number</label>
                                                <input class="form-control" type="text" id="" name="" value="{{ $payment->ar_number }}" disabled/>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>

    </script>
@endsection

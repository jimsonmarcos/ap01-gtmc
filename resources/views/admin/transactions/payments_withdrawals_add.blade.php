@extends('layouts.app')

@section('content')
    <a href="{{ url('/admin/transactions/payments/withdrawals') }}"><i class="fa fa-chevron-left"></i> Back to Withdrawals</a>
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('payments')}}">Payment</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <br />
                    {{--<ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">--}}
                        {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="{{ route('payments') }}"> Loan Payments</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="{{ route('coop_shares') }}"> Coop Shares</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item">--}}
                            {{--<a class="nav-link active" href="{{ route('withdrawals') }}"> Withdrawals</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                    <div class="row">
                        <form id="form-withdraw" action="" class="form-horizontal push-10-t push-10" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="col-md-12">
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                            <label class="" for="cv_number">CV Number</label>
                                            <select name="check_voucher_id" id="check_voucher_id" class="form-control ">
                                                <option value=""></option>
                                                @foreach(\App\CheckVouchers::where('is_updated', 'Y')->where('is_used', 'N')->orderBy('cv_number')->get() as $checkVoucher)
                                                    <option value="{{ $checkVoucher->id }}" data-cvdate="{{ date('m/d/Y', strtotime($checkVoucher->check_date)) }}" data-checknumber="{{ $checkVoucher->check_number }}">{{ $checkVoucher->cv_number }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="amount_withdrawn">Amount Withdrawn</label>
                                                <input class="form-control"
                                                       type="text"
                                                       id="amount"
                                                       name="amount" required disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="form-control-label">Payment Date</label>
                                                <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                    <input type="text" class="form-control" name="payment_date" placeholder="mm/dd/yyyy" required disabled="">
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="form-control-label">Cheque Date</label>
                                                <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                    <input type="text" class="form-control" name="cheque_date" placeholder="mm/dd/yyyy" required disabled="">
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-12" for="releasing_status">Releasing Status</label>
                                    <div class="col-xs-12">
                                        <select class="form-control" id="releasing_status" name="releasing_status" size="1" required>
                                            <option value="RELEASED">Released</option>
                                            <option value="UNRELEASED">Unreleased</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label class="form-control-label">Released Date</label>
                                        <div id="joinDate" class="input-group btn-mmddyyyy ">
                                            <input type="text" class="form-control" name="released_date" placeholder="mm/dd/yyyy" required>
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label for="remarks">Remarks</label>
                                        <textarea class="form-control input-lg"
                                                  id="remarks" name="remarks"
                                                  rows="5"
                                                  placeholder="Remarks"></textarea>
                                    </div>
                                </div>
                            </div>

                            <table class="table hidden">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>CV NUMBER</th>
                                        <th>EMPLOYEE NAME</th>
                                        <th>COST CENTER</th>
                                        <th>AMORTIZATION</th>
                                        <th>CV DATE</th>
                                        <th>CHEQUE NUMBER</th>
                                    </tr>
                                </thead>
                            </table>

                            <div class="col-md-12 text-right">
                                <button class="btn btn-danger push-5-r push-10"><i class="fa fa-times"></i> Cancel</button>
                                <button class="btn btn-success push-5-r push-10"><i class="fa fa-check"></i> Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

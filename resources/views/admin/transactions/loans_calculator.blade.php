@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('loans_calculator')}}">Calculator</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <br />

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="employee_id">Employee ID</label>
                                            <div class="col-xs-12">
                                                <div class="input-group">
                                                    <input class="form-control" type="text" id="employee_id" name="employee_id" required />
                                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="employee_name">Employee Name</label>
                                                <input class="form-control" type="text" id="employee_name" name="employee_name" required disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <hr />
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="block">
                                            <div class="block-content">
                                                <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_length" id="DataTables_Table_1_length"><label>
                                                                    <select name="DataTables_Table_1_length"
                                                                            aria-controls="DataTables_Table_1"
                                                                            class="form-control">
                                                                        <option value="5">5</option>
                                                                        <option value="10">10</option>
                                                                        <option value="15">15</option>
                                                                        <option value="20">20</option>
                                                                    </select>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div id="DataTables_Table_1_filter" class="dataTables_filter">
                                                                <label>Search:<input type="search" class="form-control" placeholder="" aria-controls="DataTables_Table_1"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <table class="table table-bordered table-striped js-dataTable-full dataTable no-footer"
                                                                   id="DataTables_Table_1" role="grid" aria-describedby="DataTables_Table_1_info">
                                                                <thead>
                                                                <tr role="row">
                                                                    <th class="text-center sorting_asc"
                                                                        tabindex="0"
                                                                        aria-controls="DataTables_Table_1"
                                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                                        aria-label=": activate to sort column descending"></th>

                                                                    <th class="sorting"
                                                                        tabindex="0"
                                                                        aria-controls="DataTables_Table_1"
                                                                        rowspan="1" colspan="1"
                                                                        aria-label="Name: activate to sort column ascending">Loan Code</th>

                                                                    <th class="hidden-xs sorting"
                                                                        tabindex="0" aria-controls="DataTables_Table_1"
                                                                        rowspan="1" colspan="1"
                                                                        aria-label="Email: activate to sort column ascending">Months To Pay</th>

                                                                    <th class="hidden-xs sorting" tabindex="0" aria-controls="DataTables_Table_1"
                                                                        rowspan="1" colspan="1"
                                                                        aria-label="Access: activate to sort column ascending">Request Amount</th>

                                                                    <th class="text-center sorting_disabled" rowspan="1"
                                                                        colspan="1" aria-label="Actions">Interest</th>

                                                                    <th class="text-center sorting_disabled" rowspan="1"
                                                                        colspan="1" aria-label="Actions">Granted Loan Amount</th>

                                                                    <th class="text-center sorting_disabled" rowspan="1"
                                                                        colspan="1" aria-label="Actions">Amortization</th>

                                                                    <th class="text-center sorting_disabled" rowspan="1"
                                                                        colspan="1" aria-label="Actions">Loan Payments</th>

                                                                    <th class="text-center sorting_disabled" rowspan="1"
                                                                        colspan="1" aria-label="Actions">Outstanding Balance</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr role="row" class="odd">
                                                                    <td class="text-center sorting_1">
                                                                        1
                                                                    </td>
                                                                    <td class="font-w600"></td>
                                                                    <td class="font-w600"></td>
                                                                    <td class="font-w600">
                                                                        <span class="label label-danger">YES</span>
                                                                    </td>
                                                                    <td class="font-w600"></td>
                                                                    <td class="font-w600"></td>
                                                                    <td class="font-w600"></td>
                                                                    <td class="font-w600"></td>
                                                                    <td class="font-w600">
                                                                        <a href="#" class="btn btn-primary">Update</a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_info" id="DataTables_Table_1_info"
                                                                 role="status" aria-live="polite">Showing <strong>1</strong>-<strong>10</strong> of <strong>40</strong>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_paginate paging_simple_numbers"
                                                                 id="DataTables_Table_1_paginate">
                                                                <ul class="pagination">
                                                                    <li class="paginate_button previous disabled"
                                                                        aria-controls="DataTables_Table_1" tabindex="0"
                                                                        id="DataTables_Table_1_previous">
                                                                        <a href="#"><i class="fa fa-angle-left"></i></a>
                                                                    </li>

                                                                    <li class="paginate_button active" aria-controls="DataTables_Table_1"
                                                                        tabindex="0">
                                                                        <a href="#">1</a>
                                                                    </li>

                                                                    <li class="paginate_button " aria-controls="DataTables_Table_1"
                                                                        tabindex="0">
                                                                        <a href="#">2</a>
                                                                    </li>

                                                                    <li class="paginate_button " aria-controls="DataTables_Table_1"
                                                                        tabindex="0">
                                                                        <a href="#">3</a>
                                                                    </li>

                                                                    <li class="paginate_button " aria-controls="DataTables_Table_1"
                                                                        tabindex="0">
                                                                        <a href="#">4</a>
                                                                    </li>

                                                                    <li class="paginate_button next" aria-controls="DataTables_Table_1"
                                                                        tabindex="0" id="DataTables_Table_1_next">
                                                                        <a href="#"><i class="fa fa-angle-right"></i></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 text-right">
                                    <span>Total Outstanding Balance: <h6>0.00</h6></span>
                                </div>

                                <div class="row">

                                    <div class="col-md-12">
                                        <h2 class="content-heading">Checking Loanable Amount</h2>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="capital_shares">Capital Share</label>
                                                <input class="form-control" type="text" id="capital_shares" name="capital_shares" required disabled />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="existing_loans">Existing Loans</label>
                                                <input class="form-control" type="text" id="existing_loans" name="existing_loans" required disabled />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="loanable_%">Loanable %</label>
                                                <input class="form-control" type="text" id="loanable_%" name="loanable_%" required disabled />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="loanable_amount">Loanable Amount</label>
                                                <input class="form-control" type="text" id="loanable_amount" name="loanable_amount" required disabled />
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-12">
                                        <h2 class="content-heading">Calculate Loan</h2>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="months_to_pay">Months To Pay</label>
                                                    <div class="col-xs-12">
                                                        <select class="form-control" id="months_to_pay" name="months_to_pay" size="1" required>
                                                            <option value="APPLIANCE">Appliance Loan</option>
                                                            <option value="COOP">Coop Loan</option>
                                                            <option value="EDUCATIONAL">Educational Loan</option>
                                                            <option value="EMERGENCY">Emergency Loan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="loan_type">Loan Type</label>
                                                    <div class="col-xs-12">
                                                        <select class="form-control" id="loan_type" name="loan_type" size="1" required>
                                                            @for($x=1; $x<13; $x++)
                                                                <option value="{{ $x }}">{{ $x }}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="request_amount">Request Amount</label>
                                                        <input class="form-control" type="text" id="request_amount" name="request_amount" required disabled />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="loan_interest_%">Loan Interest %</label>
                                                        <input class="form-control" type="text" id="loan_interest_%" name="loan_interest_%" required disabled />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="interest_%">Interest %</label>
                                                        <input class="form-control" type="text" id="interest_%" name="interest_%" required disabled />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="overall_interest">Overall Interest</label>
                                                        <input class="form-control" type="text" id="overall_interest" name="overall_interest" required disabled />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="granted_loan_amount">Granted Loan Amount</label>
                                                        <input class="form-control" type="text" id="granted_loan_amount" name="granted_loan_amount" required disabled />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="amortization">Amortization</label>
                                                        <input class="form-control" type="text" id="amortization" name="amortization" required disabled />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="service_charge_%">Service Charge %</label>
                                                        <input class="form-control" type="text" id="service_charge_%" name="service_charge_%" required disabled />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="service_charge">Service Charge</label>
                                                        <input class="form-control" type="text" id="service_charge" name="service_charge" required disabled />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="savings_%">Savings %</label>
                                                        <input class="form-control" type="text" id="savings_%" name="savings_%" required disabled />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="savings">Savings</label>
                                                        <input class="form-control" type="text" id="savings" name="savings" required disabled />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="additional_deduction">Additional Deduction</label>
                                                        <input class="form-control" type="text" id="additional_deduction" name="additional_deduction" required disabled />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="disbursed_amount">Disbursed Amount</label>
                                                        <input class="form-control" type="text" id="disbursed_amount" name="disbursed_amount" required disabled />
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 text-right">
                                    {{--<button class="btn btn-primary push-5-r push-10" type="button"><i class="fa fa-calculator"></i> Calculate</button>--}}
                                    <button class="btn btn-primary push-5-r push-10" type="button"><i class="fa fa-telegram"></i> Request</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
<script>
    $(document).ready(function() {

    })
</script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10">
        <li>Transactions</li>
        <li><a href="{{ route('loans')}}">Loans</a></li>
        <li>Details</li>
    </ol>

                <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs" id="" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/loans/{$loan->id}") }}"><i class="fa fa-info-circle"></i> Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ url("admin/transactions/loans/{$loan->id}/loan-amortization-schedule") }}"><i class="fa fa-calendar"></i> Loan Amortization Schedule</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/loans/{$loan->id}/payment-schedule") }}"><i class="fa fa-calendar"></i> Payment Schedule</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("admin/transactions/loans/{$loan->id}/payments") }}"><i class="fa fa-money"></i> Payments</a>
                        </li>
                    </ul>
                </div>

                <div class="col-sm-12 push-10-t">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-right">
                                <a href="{{ url("/print/loan/loan-amortization-schedule/{$loan->id}") }}" class="btn btn-primary btn-sm" target="_blank"><i class="fa fa-print"></i> Print Loan Amortization Schedule</a>
                            </div>
                            <br>
                        </div>

                        <div class="col-md-12">
                            <table class="table table-bordered table-condensed">
                                <thead>
                                    <tr>
                                        <th class="text-center">MONTH</th>
                                        <th class="text-center">PAY PERIOD</th>
                                        <th class="text-center">DUE DATE</th>
                                        <th class="text-center">AMORTIZATION</th>
                                        <th class="text-center">OUTSTANDING BALANCE</th>
                                    </tr>
                                </thead>


                                <tbody>
                                <?php
                                $outstandingBalance = $loan->is_voucher == 'N' ? $loan->granted_loan_amount : $loan->beginning_balance;
                                ?>

                                <tr>
                                    <th class="text-right" colspan="4"></th>
                                    <th class="text-right">{{ number_format($outstandingBalance, 2) }}</th>
                                </tr>


                                @foreach($paymentSchedules as $paymentSchedule)
                                    <?php
                                    $payment = \App\LoanPayments::where('loan_id', $paymentSchedule->loan_id)->where('pay_period', $paymentSchedule->pay_period)->first();
                                    $amountPaid = !empty($payment) ? number_format($payment->amount_paid, 2) : number_format(0, 2);

                                    $outstandingBalance -= round($paymentSchedule->amortization + $paymentSchedule->lapsed_interest, 2);
                                    ?>
                                    @if($loop->last)
                                        @if(floor($outstandingBalance) == 0)
                                            <?php $outstandingBalance = 0; ?>
                                        @endif
                                    @endif

                                    <tr>
                                        <th class="text-center">{{ $paymentSchedule->month }}</th>
                                        <td class="text-center">{{ $paymentSchedule->pay_period }}</td>
                                        <td class="text-center">{{ $paymentSchedule->due_date }}</td>
                                        <td class="text-right">{{ number_format($paymentSchedule->amortization, 2) }}</td>
                                        <th class="text-right">{{ $outstandingBalance > 0 ? number_format($outstandingBalance, 2) : '-' }}</th>
                                    </tr>
                                @endforeach
                                </tbody>



                            </table>
                        </div>

                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('invoices')}}">Invoices</a></li>
        <li><a class="link-effect" href="{{ route('invoices_create_description')}}">Description</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <br />
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="example-file-multiple-input">Attach Files</label>
                                            <div class="col-xs-12">
                                                <input type="file" id="example-file-multiple-input" name="example-file-multiple-input" multiple="">
                                            </div>
                                        </div>
                                        <br />
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="note_to_recipient">Note To Recipient</label>
                                                <textarea class="form-control input-lg"
                                                          id="note_to_recipient"
                                                          name="note_to_recipient" rows="10"
                                                          placeholder="Note To Recipient"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="terms_and_conditions">Terms and Conditions</label>
                                                <textarea class="form-control input-lg"
                                                          id="terms_and_conditions"
                                                          name="terms_and_conditions" rows="10"
                                                          placeholder="Terms and Conditions"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="memo">Memo</label>
                                                <textarea class="form-control input-lg"
                                                          id="memo"
                                                          name="memo" rows="10"></textarea>
                                            </div>
                                        </div>
                                        <br />
                                    </div>

                                    <div class="col-md-6">
                                        <h6>Prepared by: &nbsp; <span>NAME HERE</span></h6>
                                    </div>

                                    <div class="col-md-6">
                                        <h6>Approved by: &nbsp; <span>NAME HERE and POSITION</span></h6>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <br /><br />
                                        <a href="{{ route('invoices_preview') }}" class="btn btn-info"><i class="fa fa-print" aria-hidden="true"></i> Preview</a>
                                        <a href="#" class="btn btn-success"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a>
                                        <a href="#" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save as Draft</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('invoices')}}">Invoices</a></li>
        <li><a class="link-effect" href="{{ route('invoices_item')}}">Item</a></li>
        <li><a class="link-effect" href="{{ route('invoices_tax')}}">Tax</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <br />

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="tax_rate_name">Tax Rate Name</label>
                                                <input class="form-control input-lg" type="text" id="tax_rate_name" name="tax_rate_name" required />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="rate">Tax Rate Name</label>
                                                <input class="form-control input-lg" type="text" id="rate" name="rate" required />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <br><br>
                                        <a href="#" class="btn btn-danger">Cancel <span class="fa fa-ban" style="font-size: 0.8em;"></span></a>
                                        <a href="#" class="btn btn-success">Add <span class="fa fa-check" style="font-size: 0.8em;"></span></a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

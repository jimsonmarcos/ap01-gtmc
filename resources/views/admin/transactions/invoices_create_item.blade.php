@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('invoices')}}">Invoices</a></li>
        <li><a class="link-effect" href="{{ route('invoices_item')}}">item</a></li>
        <li><a class="link-effect" href="{{ route('invoices_create_item')}}">Create</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="col-md-12">
                                    <br />
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label for="item_name">Item Name</label>
                                                    <input class="form-control" type="text" id="item_name" name="item_name" required />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="company_code">Company Code</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="company_id" name="company_id" size="1" required>
                                                        @foreach(\App\Companies::with('payrollgroups.payrollcycles')->orderBy('company_code')->get() as $company)
                                                            <option value="{{ $company->id }}" data-company="{{ $company->toJson() }}">{{ $company->company_code }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="payroll_group">Payroll Group</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="payroll_group_id" name="payroll_group_id" size="1" required>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="payroll_cycle">Payroll Cycle</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="payroll_cycle_id" name="payroll_cycle_id" size="1" required>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <h2 class="content-heading">Item Details</h2>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label for="total_head_count">Total Head Count</label>
                                                    <input class="form-control" type="text" id="total_head_count" name="total_head_count" value="0" required readonly/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label for="total_number_hours">Total # of Hours</label>
                                                    <input class="form-control" type="text" id="total_number_hours" name="total_number_hours" value="0" required readonly/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label for="billing_amount">Billing Amount</label>
                                                    <input class="form-control" type="text" id="billing_amount" name="billing_amount" value="0" required readonly/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="tax_type">Tax Type</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="business_tax_type_id" name="business_tax_type_id" size="1" required>
                                                        @foreach(\App\BusinessTaxTypes::all() as $taxType)
                                                            <option value="{{ $taxType->id }}" data-percentage="{{ $tax }}">{{ $taxType->tax }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <h2 class="content-heading">Description</h2>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <textarea class="form-control input-lg"
                                                              id="description"
                                                              name="description"
                                                              rows="10"
                                                              placeholder="Description" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 text-right">
                                    <br><br>
                                    <a href="#" class="btn btn-danger" onclick="history.back();">Cancel <span class="fa fa-ban" style="font-size: 0.8em;"></span></a>
                                    <button href="#" class="btn btn-success">Add <span class="fa fa-check" style="font-size: 0.8em;"></span></button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            function initPayrollGroups() {
                var data = $.parseJSON($("#company_id").find('option').first().attr('data-company'));
                $.each(data.payrollgroups, function(i, v) {
                   $("#payroll_group_id").append("<option value='"+ v.id +"' data-payrollcycles='"+ JSON.stringify(v.payrollcycles) +"'>"+ v.group_name +"</option>");

                });

                var payrollGroups = $.parseJSON($("#payroll_group_id").find('option').first().attr('data-payrollcycles'));
//                console.log(pg);
                if(payrollGroups.length > 0) {
                    $.each(payrollGroups, function(i, pg) {
                        $("#payroll_cycle_id").append('<option value="'+ pg.id +'">'+ pg.payroll_code +'</option>');
                    });
                }



                recalculateItemDetails();
            }

            initPayrollGroups();

            $("#company_id").change(function() {
                var data = $.parseJSON($("#company_id").find(':selected').attr('data-company'));
                $("#payroll_group_id").find('option').remove();
                $("#payroll_cycle_id").find('option').remove();
                $.each(data.payrollgroups, function(i, v) {
                    $("#payroll_group_id").append("<option value='"+ v.id +"' data-payrollcycles='"+ JSON.stringify(v.payrollcycles) +"'>"+ v.group_name +"</option>");
                });

                var payrollGroups = $.parseJSON($("#payroll_group_id").find('option').first().attr('data-payrollcycles'));
                console.log(payrollGroups);
                if(payrollGroups.length > 0) {
                    $.each(payrollGroups, function(i, pg) {
                        $("#payroll_cycle_id").append('<option value="'+ pg.id +'">'+ pg.payroll_code +'</option>');
                    });
                }


                recalculateItemDetails();
                refreshDescription();
            });

            $(document).on('change', "#payroll_group_id", function() {
                $("#payroll_cycle_id").find('option').remove();
                var payrollGroups = $.parseJSON($(this).find(':selected').attr('data-payrollcycles'));
                if(payrollGroups.length > 0) {
                    $.each(payrollGroups, function(i, pg) {
                        $("#payroll_cycle_id").append('<option value="'+ pg.id +'">'+ pg.payroll_code +'</option>');
                    });
                }

                refreshDescription();
            });

            function refreshDescription() {
                $("#description").val($("#payroll_group_id").find(':selected').text() + ' - ' + $("#payroll_cycle_id").find(':selected').text());
            }

            function recalculateItemDetails() {
//                console.log($("#payroll_cycle_id").find(':selected').val());
                $("#total_head_count").val('0');
                $("#total_number_hours").val('0');
                $("#billing_amount").val('0');
                if($("#payroll_cycle_id").find(':selected').val() != undefined) {
                    $.get('{{ url('api/payroll-for-invoice-item') }}/' + $("#payroll_cycle_id").find(':selected').first().val(), function(result) {
                        console.log(result);
                        result = $.parseJSON(result);
                        $("#total_head_count").val(result.total_head_count);
                        $("#total_number_hours").val(result.total_hours);
                        $("#billing_amount").val(result.billing_amount);
                    });
                }
            }

            $(document).on('change', "#payroll_cycle_id", function() {
                recalculateItemDetails();
            });
        })
    </script>
@endsection

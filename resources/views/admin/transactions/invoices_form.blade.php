@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('invoices')}}">Invoices</a></li>
        <li><a class="link-effect" href="{{ route('invoices_create')}}">Create</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('invoices_create_submit') }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <br />
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="company_code">Company Code</label>
                                                    <div class="col-xs-12">
                                                        <select class="form-control" id="company_id" name="company_id" size="1" required>
                                                            @foreach(\App\Companies::with('contactpersons')->orderBy('company_code')->get() as $company)
                                                                <option value="{{ $company->id }}" data-company='{{ $company->toJson() }}' data-contactpersons='{{ $company->contactpersons->toJson() }}' data-payrollcycles="{{ $company->PayrollCycles() }}">{{ $company->company_code }}</option>
                                                            @endforeach
                                                        </select>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="bill_to">Bill To</label>
                                                    <div class="col-xs-12">
                                                        <select class="form-control" id="contact_person_id" name="contact_person_id" size="1" disabled>

                                                        </select>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="company_name">Company Name</label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" type="text" id="company_name" name="company_name" required readonly/>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="company_address">Company Address</label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" type="text" id="company_address" name="company_address" required readonly/>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label class="form-control-label">Invoice Date</label>
                                                        <div id="invoice_date" class="input-group btn-mmddyyyy ">
                                                            <input type="text" class="form-control" id="" name="invoice_date" placeholder="mm/dd/yyyy" required>
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        </div>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="terms">Terms</label>
                                                    <div class="col-xs-12">
                                                        <select class="form-control" id="terms" name="invoice_terms_id" size="1" required>
                                                            @foreach(\App\InvoiceTerms::orderBy('days')->get() as $terms)
                                                                <option value="{{ $terms->id }}" data-days="{{ $terms->days }}">{{ $terms->terms }}</option>
                                                            @endforeach
                                                        </select>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label class="form-control-label">Due Date</label>
                                                        <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                            <input type="text" class="form-control" id="due_date" name="due_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y') }}" required readonly>
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        </div>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="reference">Reference</label>
                                                    <div class="col-xs-12">
                                                        <select name="reference" id="reference" class="form-control">
                                                            {{--<option value="">No payroll codes found.</option>--}}
                                                        </select>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 push-20-t push-20">
                                    <table id="invoiceItems" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                {{--<th></th>--}}
                                                <th class="text-center" width="300">DESCRIPTION</th>
                                                <th class="text-center" width="80">HEAD COUNT</th>
                                                <th class="text-center" width="80">HOURS</th>
                                                <th class="text-center" width="150">AMOUNT</th>
                                                <th class="text-center">TAX</th>
                                                <th class="text-center" width="150">TAX AMOUNT</th>
                                                <th class="text-center">BILLING AMOUNT</th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            <tr>
                                                {{--<td><button class="btn btn-sm btn-danger remove-item"><i class="fa fa-remove"></i></button></td>--}}
                                                <td><textarea class="form-control" name="items[0][description]" rows="3" style="max-width: 300px;" required></textarea></td>
                                                <td><input type="text" class="form-control" name="items[0][head_count]"></td>
                                                <td><input type="text" class="form-control" name="items[0][hours]"></td>
                                                <td><input type="text" class="form-control amount" name="items[0][amount]" required></td>
                                                <td>
                                                    <select name="items[0][business_tax_type_id]" id="" class="form-control business-tax">
                                                        @foreach(\App\BusinessTaxTypes::all() as $t)
                                                            <option value="{{ $t->id }}" data-percent='{{ $t->percent }}'>{{ $t->tax }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="text" class="form-control tax-amount" name="items[0][tax_amount]" readonly></td>
                                                <td><input type="text" class="form-control billing-amount" name="items[0][billing_amount]" readonly></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                {{--<div class="col-md-12">--}}
                                    {{--<h6>Enter detailed Description <a href="{{ route('invoices_create_description') }}">here</a>. Optional</h6>--}}
                                {{--</div>--}}

                                <div class="col-md-12 text-right">
{{--                                    <a style="float: left" href="{{ route('add_invoice_item') }}" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add item</a>--}}

                                    <!-- Hidden on July 2, 2018 -->
                                    {{--<button id="addInvoiceItem" class="btn btn-primary" style="float: left"><i class="fa fa-plus" aria-hidden="true"></i> Add item</button>--}}

                                    <h3>Total Amount:
                                        <span id="totalAmountTxt" style="border: 1px blue solid; padding: 1em; border-radius: 10px; ">0.00 PHP</span>
                                        <input type="hidden" id="totalAmount" name="total_amount" value="0.00">
                                    </h3>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <br />
                                        <div class="form-group">
                                            <label class="col-xs-12" for="">Attach Files</label>
                                            <div class="col-xs-12">
                                                <input type="file" id="" name="files[]" multiple="">
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="note_to_recipient">Note To Recipient</label>
                                                <textarea class="form-control input-lg"
                                                          id="note_to_recipient"
                                                          name="note_to_recipient" rows="10"
                                                          placeholder="Note To Recipient"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="terms_and_conditions">Terms and Conditions</label>
                                                <textarea class="form-control input-lg"
                                                          id="terms_and_conditions"
                                                          name="terms_and_conditions" rows="10"
                                                          placeholder="Terms and Conditions"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="memo">Memo</label>
                                                <textarea class="form-control input-lg"
                                                          id="memo"
                                                          name="memo" rows="10"></textarea>
                                            </div>
                                        </div>
                                        <br />
                                    </div>

                                    <div class="col-md-12 push-20-t">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-5">
                                                    <div class="form-group">
                                                        <label for="">Prepared By</label>
                                                        <select name="prepared_by" id="" class="form-control" disabled>
                                                            <option value="">{{ $me->Profile->namefl() }}</option>
                                                            {{--@foreach(\App\User::whereIn('id', \App\Signatories::pluck('user_id'))->get() as $signatory)--}}
                                                                {{--<option value="{{ $signatory->id }}">{{ $signatory->name() }}</option>--}}
                                                            {{--@endforeach--}}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <input type="hidden" name="prepared_by" value="{{ \Illuminate\Support\Facades\Auth::user()->id }}">


                                            <div class="row">
                                                <div class="col-sm-6 col-md-5">
                                                    <div class="form-group">
                                                        <label for="">Approved By</label>
                                                        <select name="approved_by" id="" class="form-control">
                                                            @foreach(\App\User::whereIn('id', \App\Signatories::pluck('user_id'))->get() as $signatory)
                                                                <option value="{{ $signatory->id }}">{{ $signatory->Profile->namefl() }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <br /><br />
{{--                                        <a href="{{ route('invoices_preview') }}" class="btn btn-info"><i class="fa fa-print" aria-hidden="true"></i> Preview</a>--}}
                                        <button href="#"  class="btn btn-success" name="status" value="Unpaid" onclick="return confirm('Are you sure you want to send this invoice?');"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</button>
                                        <button href="#" class="btn btn-primary" name="status" value="Draft"><i class="fa fa-floppy-o" aria-hidden="true"></i> Draft</button>
                                    </div>
                                </div>

                                {{--<div class="col-md-12 text-right">--}}
                                    {{--<br /><br />--}}
                                    {{--<button class="btn btn-minw btn-danger" type="button"><i class="fa fa-times"></i> Cancel</button>--}}
                                    {{--<button class="btn btn-minw btn-success" type="button"><i class="fa fa-check"></i> Submit</button>--}}
                                {{--</div>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

            function initInvoice() {
                var company = $.parseJSON($("#company_id > option").first().attr('data-company'));

                if(company.contactpersons.length > 0) {
                   $.each(company.contactpersons, function(i, v) {
                       $("#contact_person_id").append('<option value="'+ v.id +'">'+ v.first_name +' '+ v.last_name +'</option>');
                   });
                }

                $("#company_name").val(company.company_name);
                $("#company_address").val(company.address);
            }

            initInvoice();

            $("#company_id").change(function() {
                var company = $.parseJSON($(this).find(':selected').attr('data-company'));
                $("#contact_person_id").find('option').remove();

                if(company.contactpersons.length > 0) {
                    $.each(company.contactpersons, function(i, v) {
                        $("#contact_person_id").append('<option value="'+ v.id +'">'+ v.first_name +' '+ v.last_name +'</option>');
                    });
                }

                $("#company_name").val(company.company_name);
                $("#company_address").val(company.address);

                updateReference();
            });

            $(document).on('dp.change', "#invoice_date", function(e) {
                $("#invoice_date").attr('data-date', moment(e.date._d).format('YYYY-MM-DD'));
                refreshDueDate();
            });

            $("#terms").change(function() {
                refreshDueDate();
            });

            function refreshDueDate() {
                $("#due_date").val(moment($("#invoice_date").attr('data-date')).add($("#terms").find(':selected').attr('data-days'), 'days').format("MM/DD/YYYY"));
            }

            $(document).on('click', '.remove-item', function() {
               event.preventDefault();

               $(this).closest('tr').remove();
            });

            var x = 1;

            $(document).on('click', '#addInvoiceItem', function() {
                event.preventDefault();

                var itemTemplate = "<tr>" +
                    "<td><button class='btn btn-sm btn-danger remove-item'><i class='fa fa-remove'></i></button></td>" +
                    "<td><textarea class='form-control' name='items["+ x +"][description]' rows='3' style='max-width: 329px;' required></textarea></td>" +
                    "<td><input type='text' class='form-control' name='items["+ x +"][head_count]'></td>" +
                    "<td><input type='text' class='form-control' name='items["+ x +"][hours]'></td>" +
                    "<td><input type='text' class='form-control amount' name='items["+ x +"][amount]' required></td>" +
                    "<td><select name='items["+ x +"][business_tax_type_id]' id='' class='form-control business-tax'>" +
                        @foreach(\App\BusinessTaxTypes::all() as $t)
                            "<option value='{{ $t->id }}' data-percent='{{ $t->percent }}'>{{ $t->tax }}</option>" +
                        @endforeach
                    "</select>" +
                    "</td>" +
                    "<td><input type='text' class='form-control tax-amount' name='items["+ x +"][tax_amount]' readonly></td>" +
                    "<td><input type='text' class='form-control billing-amount' name='items["+ x +"][billing_amount]' readonly></td>" +
                    "</tr>";

                $("#invoiceItems").find('tbody').append(itemTemplate);

                x++;
            });

            $(document).on('keyup', '.amount', function() {
                var amount = parseFloat($(this).val().replace(/\,/g,'')).toFixed(2) || 0;
                if($(this).val().replace(/\,/g,'').length == 0) {
                    amount = 0;
                }

                var percent = parseFloat($(this).closest('tr').find('.business-tax').find(':selected').attr('data-percent')) / 100;

                var taxAmount = (parseFloat(amount) * percent);



                var billingAmount = (parseFloat(taxAmount) + parseFloat(amount)).numberFormat(2);

                $(this).closest('tr').find('.tax-amount').val(taxAmount.numberFormat(2));
                $(this).closest('tr').find('.billing-amount').val(billingAmount);

                updateTotalBillingAmount();
            });

            $(document).on('change', '.business-tax', function() {
                var amount = parseFloat($(this).closest('tr').find('.amount').val().replace(/\,/g,'')).toFixed(2) || 0;
                if($(this).val().replace(/\,/g,'').length == 0) {
                    amount = 0;
                }

                var percent = parseFloat($(this).closest('tr').find('.business-tax').find(':selected').attr('data-percent')) / 100;

                var taxAmount = (parseFloat(amount) * percent);




                var billingAmount = (parseFloat(taxAmount) + parseFloat(amount)).numberFormat(2);


                $(this).closest('tr').find('.tax-amount').val(taxAmount.numberFormat(2));
                $(this).closest('tr').find('.billing-amount').val(billingAmount);

                updateTotalBillingAmount();
            });

            function updateTotalBillingAmount() {
                var totalBillingAmount = 0;

                $("#invoiceItems").find('tbody > tr').each(function() {
                    var amount = parseFloat($(this).find('.amount').val().replace(/\,/g,'')).toFixed(2) || 0;
                    if($(this).find('.amount').val().replace(/\,/g,'').length == 0) {
                        amount = 0;
                    }

                    var percent = parseFloat($(this).find('.business-tax').find(':selected').attr('data-percent')) / 100;

                    var taxAmount = (parseFloat(amount) * percent);


                    var billingAmount = (parseFloat(taxAmount) + parseFloat(amount));

                    totalBillingAmount += billingAmount;

                    $(this).find('.tax-amount').val(taxAmount.numberFormat(2));

                    $("#totalAmountTxt").text(totalBillingAmount.numberFormat(2));
                    $("#totalAmount").val(totalBillingAmount.numberFormat(2));
                });
            }

            var data = [];

            $("#reference").select2({
                data: data
            });

            updateReference();

            function updateReference()
            {
                var payrollCycles = $("#company_id").find(':selected').data('payrollcycles');


                if(payrollCycles.length > 0) {
                    var data = $.map(payrollCycles, function (obj) {
                        obj.id = obj.payroll_code; // replace pk with your identifier
                        obj.text = obj.payroll_code; // replace pk with your identifier

                        return obj;
                    });
                    // $.each(payrollCycles, function(index, payrollCycle) {
                    //     $("#reference").append('<option value="'+ payrollCycle.payroll_code +'">'+ payrollCycle.payroll_code +'</option>');
                    // });
                } else {
                    var data = [];
                    // $("#reference").append('<option value="">No payroll codes found.</option>');
                }

                console.log(data);

                $("#reference").empty();
                $('#reference').select2({
                    data: data
                });
            }

        })
    </script>

    <style>
        .select2-container {
            width: 100% !important;
        }
    </style>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('invoices')}}">Invoices</a></li>
        <li><a class="link-effect" href="{{ route('invoices_preview')}}">Preview</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6 text-right">
                <a href="#" class="btn-success btn">
                    <i class="fa fa-paper-plane" aria-hidden="true"></i> SEND
                </a>
                <a href="#" class="btn-warning btn">
                    <i class="fa fa-pencil" aria-hidden="true"></i> EDIT
                </a>
                <a href="#" class="btn-primary btn">
                    <i class="fa fa-print" aria-hidden="true"></i> PRINT
                </a>
            </div>
            <div class="col-md-6"></div>
            <div class="col-md-6 text-right">
                <br />
                <h3>BILLING INVOICE</h3>
            </div>
            <div class="col-md-6">
                {{--THIS IS IMAGE --}}
                <figure class="figure">
                    <img src="http://bulma.io/images/placeholders/128x128.png" class="figure-img img-fluid rounded">
                </figure>
                <br />
                <div class="row">
                    <div class="col-md-6">
                        <p>Company Name: </p>
                        <p>Company Address: </p>
                        <p>Tax Type: </p>
                        <p>TIN: </p>
                    </div>
                    <div class="col-md-6">
                        <p>Value company name</p>
                        <p>Value company address</p>
                        <p>Value tax type</p>
                        <p>Value TIN</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <p>Invoice #: </p>
                        <p>Invoice Date: </p>
                        <p>Due Date: </p>
                        <p>Reference</p>
                        <br />
                        <p>Amount Due: </p>
                    </div>
                    <div class="col-md-6">
                        <p>Value invoice #</p>
                        <p>Value invoice date</p>
                        <p>Value due date</p>
                        <p>Value references</p>
                        <br />
                        <p>VALUE amount due</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h6>Invoice To: </h6>
                    </div>
                    <div class="col-md-3">
                        <p>Company Name: </p>
                        <p>Company Address 1: </p>
                        <p>Company Address 2: </p>
                    </div>
                    <div class="col-md-9">
                        <p>value company name.</p>
                        <p>value company address 1.</p>
                        <p>value company address 2.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h6>Terms</h6>
                        <p>Due in <span>VALUE HERE</span> Days.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-8 text-center">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Item</th>
                        <th>Head Count</th>
                        <th>Hours</th>
                        <th>Amount</th>
                        <th>Tax</th>
                        <th>Tax Amount</th>
                        <th>Billing Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                    </tr>
                    </tbody>
                </table>
                <h6 style="text-align: left;">Enter detailed Description <a href="{{ url ('/admin/transactions/invoices/create_invoice/description') }}">here</a>. Optional</h6>
                <h6 style="text-align: right">Sub Total: <span style="padding: 1em; border-radius: 10px">0.00 PHP</span></h6>
                <h6 style="text-align: right">Total: <span style="padding: 1em; border-radius: 10px">0.00 PHP</span></h6>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-6">
                <h6>NOTES: </h6>
                <p>Notes Here</p>
            </div>
            <div class="col-md-6">
                <h6>Terms and Conditions: </h6>
                <p>Terms and conditions here.</p>
            </div>
            <div class="col-md-6">
                <h6>Prepared By: </h6>
                <p>Name</p>
                <p>Name</p>
            </div>
            <div class="col-md-6">
                <h6>Approved By: </h6>
                <p>Name</p>
                <p>Name</p>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

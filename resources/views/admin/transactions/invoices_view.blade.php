@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Transactions</li>
        <li><a class="link-effect" href="{{ route('invoices')}}">Invoices</a></li>
        <li>View</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">

                                <div class="row">
                                    <br />
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="company_code">Company Code</label>
                                                    <div class="col-xs-12">
                                                        <p><strong>{{ $invoice->Company->company_code }}</strong></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="bill_to">Bill To</label>
                                                    <div class="col-xs-12">
                                                        <p><strong>{{ !empty($invoice->ContactPerson) ? $invoice->ContactPerson->complete_name(): '' }}</strong></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="company_name">Company Name</label>
                                                    <div class="col-xs-12">
                                                        <p><strong>{{ $invoice->Company->company_name }}</strong></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="company_address">Company Address</label>
                                                    <div class="col-xs-12">
                                                        <p><strong>{{ $invoice->Company->company_address }}</strong></p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label class="form-control-label">Invoice Date</label>
                                                            <p><strong>{{ date('m/d/Y', strtotime($invoice->invoice_date)) }}</strong></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="terms">Terms</label>
                                                    <div class="col-xs-12">
                                                        <p><strong>{{ $invoice->Terms->terms }}</strong></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label class="form-control-label">Due Date</label>
                                                        <p><strong>{{ date('m/d/Y', strtotime($invoice->due_date)) }}</strong></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="reference">Reference</label>
                                                    <div class="col-xs-12">
                                                        <p><strong>{{ $invoice->reference }}</strong></p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 push-20-t push-20">
                                    <table id="invoiceItems" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center" width="300">DESCRIPTION</th>
                                                <th class="text-center" width="80">HEAD COUNT</th>
                                                <th class="text-center" width="80">HOURS</th>
                                                <th class="text-center" width="150">AMOUNT</th>
                                                <th class="text-center">TAX</th>
                                                <th class="text-center" width="150">TAX AMOUNT</th>
                                                <th class="text-center">BILLING AMOUNT</th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            @foreach($invoice->Items as $item)
                                                <tr>
                                                    <td>{{ $item->description }}</td>
                                                    <td>{{ $item->head_count }}</td>
                                                    <td>{{ $item->hours }}</td>
                                                    <td>{{ number_format($item->amount, 2) }}</td>
                                                    <td>{{ $item->BusinessTaxType->tax }}</td>
                                                    <td>{{ number_format($item->tax_amount, 2) }}</td>
                                                    <td>{{ number_format($item->billing_amount, 2) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-12 text-right">
                                    <h3>Total Amount:
                                        <span id="totalAmountTxt" style="border: 1px blue solid; padding: 1em; border-radius: 10px; ">{{ number_format($invoice->Items->sum('billing_amount', 2), 2) }} PHP</span>
                                    </h3>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <br />
                                        <div class="form-group">
                                            <label class="col-xs-12" for="">Attach Files</label>
                                            <div class="col-xs-12">
                                                @foreach($invoice->Files as $file)
                                                    <div><a href="{{ url("/admin/transactions/invoices/file/{$file->id}") }}" download><i class="fa fa-download"></i> {{ $file->file_name }}</a></div>
                                                @endforeach
                                                    <br>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="note_to_recipient">Note To Recipient</label>
                                                <p>{{ $invoice->note_to_recipient }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="terms_and_conditions">Terms and Conditions</label>
                                                <p>{{ $invoice->terms_and_conditions }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="memo">Memo</label>
                                                <p>{{ $invoice->memo }}</p>
                                            </div>
                                        </div>
                                        <br />
                                    </div>

                                    <div class="col-md-12 push-20-t">
                                        <div class="col-md-12">

                                            <div class="row">
                                                <div class="col-sm-6 col-md-5">
                                                    <div class="form-group">
                                                        <label for="">Prepared By</label>
                                                        <p><strong>{{ $invoice->PreparedBy->Profile->namefl() }}</strong></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6 col-md-5">
                                                    <div class="form-group">
                                                        <label for="">Approved By</label>
                                                        <p><strong>{{ $invoice->ApprovedBy->Profile->namefl() }}</strong></p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <br /><br />
                                        <a href="{{ route('invoices') }}" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to Invoices</a>
{{--                                        <a href="{{ route('invoices_preview') }}" class="btn btn-info"><i class="fa fa-print" aria-hidden="true"></i> Preview</a>--}}
                                        {{--<button href="#" class="btn btn-success" name="status" value="Unpaid"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</button>--}}
                                        {{--<button href="#" class="btn btn-primary" name="status" value="Draft"><i class="fa fa-floppy-o" aria-hidden="true"></i> Draft</button>--}}
                                    </div>
                                </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection


@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Dashboard</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-6" style="box-shadow: -3px 0px grey">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6" style="padding-bottom: 1.5em;">
                                <h2 class="content-heading" style="color: #000000">Members</h2>
                                <span>Total Headcount</span>
                            </div>

                            <div class="col-md-6">
                                <h2 class="text-green" style="padding-top: 2em; color: blue;">{{ \App\User::where('role', 'Member')->count() }}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6" style="padding-bottom: 1.5em;">
                                <h2 class="content-heading" style="color: #000000">Associate</h2>
                                <span>Total Headcount</span>
                            </div>

                            <div class="col-md-6">
                                <h2 class="text-green" style="padding-top: 2em; color: lightblue">{{ \App\Profiles::where('membership_type', 'Associate')->count() }}</h2>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="col-md-6">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6" style="padding-bottom: 1.5em;">
                                <h2 class="content-heading" style="color: #000000">Regular</h2>
                                <span>Total Headcount</span>
                            </div>

                            <div class="col-md-6">
                                <h2 class="text-green" style="padding-top: 2em; color: lightcoral">{{ \App\Profiles::where('membership_type', 'Regular')->count() }}</h2>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6" style="padding-bottom: 1.5em;">
                                <h2 class="content-heading" style="color: #000000">Clients</h2>
                                <span>Total Headcount</span>
                            </div>

                            <div class="col-md-6">
                                <h2 class="text-green" style="padding-top: 2em; color: green">{{ \App\Companies::where('status', 'Active')->count() }}</h2>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-4">
                    <!-- First Table -->
                    <table id="loans" class="table table-striped">
                        <thead>
                        <tr>
                            <th>Principal</th>
                            <th>Employees</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $x = 0; ?>
                            @foreach(\App\Companies::where('status', 'Active')->orderBy('company_name')->get() as $company)
                            <tr>
                                <td>{{ $company->company_code }}</td>
                                <td>{{ $company->Employees->count() }}</td>
                            </tr>
                            <?php $x += $company->Employees->count(); ?>
                            @endforeach
                        <tr>
                            <td><strong>Grand Total</strong></td>
                            <td>{{ $x }}</td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- End First Table -->
                </div>

                    <!-- Second Table -->
                <div class="col-md-4">
                    <table id="loans" class="table table-striped">
                        <thead>
                        <tr>
                            <th>Payroll Group</th>
                            <th>Head Count</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $x = 0; ?>
                            @foreach(\App\PayrollGroups::with(['Company' => function($query) {
                                $query->where('status', 'Active');
                            }])->orderBy('group_name')->get() as $payrollGroup)
                                @if(!empty($payrollGroup->Company) && $payrollGroup->Company->status == 'Active')
                                    <tr>
                                        <td>{{ $payrollGroup->group_name }}</td>
                                        <td>{{ $payrollGroup->Members->count() }}</td>
                                    </tr>
                                    <?php $x += $payrollGroup->Members->count(); ?>
                                @endif
                            @endforeach
                            <tr>
                                <td><strong>Grand Total</strong></td>
                                <td>{{ $x }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                    <!-- End Second Table -->

                    <!-- Third Table -->
                {{--<div class="col-md-3">--}}
                    {{--<table id="loans" class="table table-striped">--}}
                        {{--<thead>--}}
                        {{--<tr>--}}
                            {{--<th>Principal</th>--}}
                            {{--<th>Members</th>--}}
                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                            {{--<td>GIDCI</td>--}}
                            {{--<td>102</td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<td>GIDCI-GIANT</td>--}}
                            {{--<td>8</td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<td>GIDCI-GTI</td>--}}
                            {{--<td>10</td>--}}
                        {{--</tr>--}}

                        {{--<tr>--}}
                            {{--<td><strong>Grand Total</strong></td>--}}
                            {{--<td>120</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                    {{--</table>--}}
                {{--</div>--}}
                    <!-- End Third Table -->

                    <!-- Fourth Table -->
                <div class="col-md-4">
                    <table id="loans" class="table table-striped">
                        <thead>
                        <tr>
                            <th>Cost Center</th>
                            <th>Head Count</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $x = 0; ?>
                            @foreach(\App\CostCenters::orderBy('cost_center')->get() as $costCenter)
                                @if(!empty($costCenter->Company) && $costCenter->Company->status == 'Active')
                                    <tr>
                                        <td>{{ $costCenter->cost_center }}</td>
                                        <td>{{ $costCenter->Members->count() }}</td>
                                    </tr>
                                    <?php $x += $costCenter->Members->count(); ?>
                                @endif
                            @endforeach
                            <tr>
                                <td><strong>Grand Total</strong></td>
                                <td>{{ $x }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                    <!-- End Fourth Table -->

                <div class="col-md-12">
                    <hr />
                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="content-heading">Birthday Celebrants of the Month</h2>
                            <table id="birthday" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Principal</th>
                                    <th>Full Name</th>
                                    <th>Birthday</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach(\App\Profiles::where('status', 'Active')->whereRaw("MONTH(birthday) = '". date('m') ."' AND (birthday > '1910-01-01')")->orderByRaw('DAYOFMONTH(birthday) ASC, last_name ASC')->get() as $profile)
                                    <tr>
                                        <td>@if(!empty($profile->EmploymentDetails->Principal->company_code)) {{ $profile->EmploymentDetails->Principal->company_code }} @endif</td>
                                        <td>@if(!empty($profile->first_name)) {{ $profile->first_name }} @endif, @if(!empty($profile->last_name)) {{ $profile->last_name }} @endif</td>
                                        <td>{{ date('d/m/Y', strtotime($profile->birthday)) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{--<div class="col-md-6">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<h2 class="content-heading">Announcements</h2>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6 text-right">--}}
                                    {{--<a href="#" class="btn btn-primary"><i class="fa fa-plus"></i> ADD</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<table id="birthday" class="table table-striped">--}}
                                {{--<thead>--}}
                                {{--<tr>--}}
                                    {{--<th>Date Published</th>--}}
                                    {{--<th>Posted By</th>--}}
                                    {{--<th>Announcement</th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody>--}}
                                {{--<tr>--}}
                                    {{--<td>OPPO</td>--}}
                                    {{--<td>ZHERINA, CRUZ</td>--}}
                                    {{--<td>16/01/1992</td>--}}
                                {{--</tr>--}}
                                {{--</tbody>--}}
                            {{--</table>--}}
                        {{--</div>--}}

                    </div>
                </div>

                {{--<div class="col-md-12">--}}
                    {{--<h2 class="content-heading">Activity</h2>--}}
                    {{--<table class="table">--}}
                        {{--<thead>--}}
                            {{--<tr>--}}
                                {{--<th>Announcement</th>--}}
                                {{--<th>Peron</th>--}}
                                {{--<th>Date</th>--}}
                            {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}
                            {{--<tr>--}}
                                {{--<td>New Employee Added</td>--}}
                                {{--<td>adminx</td>--}}
                                {{--<td>2018-02-01 12:089pm</td>--}}
                            {{--</tr>--}}
                        {{--</tbody>--}}

                    {{--</table>--}}
                {{--</div>--}}
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')

@endsection

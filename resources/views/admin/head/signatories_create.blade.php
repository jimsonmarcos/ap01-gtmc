@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Signatories</li>
        <li><a class="link-effect" href="{{ route('head_signatories_create') }}">New Signatory</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <br />
                                    <form class="form" action="" method="POST">
                                        {{ csrf_field() }}


                                        <div class="form-group">
                                            <label class="col-xs-12" for="contact_type">Contact Type</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="contact_type" name="contact_type" size="1">
                                                    <option value="Employee" @if($role == 'Employee') selected @endif>Employee</option>
                                                    <option value="Member" @if($role == 'Member') selected @endif>Member</option>
                                                    {{--<option value="ClientContacts">Client Contacts</option>--}}
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <br>
                                                <label class="" for="login1-username">Contact Person</label>
                                                <div class="">
                                                    <select name="user_id" id="" class="form-control">
                                                        @foreach($users as $user)
                                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <br>
                                            </div>

                                        </div>



                                        @if($role == 'Member')
                                            <div class="form-group" style="margin-top: 10px;">
                                                <label class="col-xs-12" for="login1-username">Position</label>
                                                <div class="col-xs-12">
                                                    <div class="">
                                                        <select name="position_id" id="" class="form-control">
                                                            @foreach(\App\Positions::all() as $position)
                                                                <option value="{{ $position->id }}">{{ $position->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        <div class="form-group">
                                            <div class="col-xs-12 text-right">
                                                <br />
                                                <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-save push-5-r"></i> SUBMIT</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#contact_type").change(function() {
               var role = $(this).val();

               location.href = "{{ route('head_signatories_create') }}/" + role;
            });
        })
    </script>
@endsection

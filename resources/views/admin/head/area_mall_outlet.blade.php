@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Area Mall Outlet</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="fa fa-users"></span> List of Area Mall Outlet</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('head_area_mall_outlet_create') }}"><span class="fa fa-user-plus"></span> New Area Mall Outlet</a>
                                    </li>
                                </ul>

                                <br />

                                <table id="departments" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>City/Province</th>
                                        <th>Area</th>
                                        <th>Mall</th>
                                        <th>Outlet</th>
                                        <th>Department</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(\App\Departments::orderBy('province')->get() as $department)
                                            <tr>
                                                <td>{{ $department->province }}</td>
                                                <td>{{ $department->area }}</td>
                                                <td>{{ $department->mall }}</td>
                                                <td>{{ $department->outlet }}</td>
                                                <td>{{ $department->department }}</td>
                                                <td>
                                                    <a href="#" class="btn btn-sm btn-primary editDepartment" data-id="{{ $department->id }}"><i class="fa fa-edit"></i> EDIT</a>
                                                    <a href="#" class="btn btn-sm btn-danger deleteDepartment" data-id="{{ $department->id }}"><i class="fa fa-remove"></i> DELETE</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="deleteDepartment" tabindex="-1" role="dialog" aria-labelledby="deleteDepartment" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete Department</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Department?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#departments').DataTable();

            $(document).on('click', '.editDepartment', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/department-edit") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');

                    }
                });
            });

            $(document).on('click', '.deleteDepartment', function(e) {
                e.preventDefault();

                var url = "{{ url("admin/super/department/delete") }}/" + $(this).attr('data-id');

                $("#deleteDepartment").find('#url').attr('href', url);
                $("#deleteDepartment").modal('show');
            });
        })
    </script>
@endsection

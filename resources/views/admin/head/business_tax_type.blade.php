@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Business Tax Type</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="fa fa-users"></span> List of Business Tax Type</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('head_business_tax_type_create') }}"><span class="fa fa-user-plus"></span> New Business Tax Type</a>
                                    </li>
                                </ul>

                                <br>

                                <table id="users-dt" class="table table-bordered table-striped" style="max-width: 500px;">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="200">Business Tax Type</th>
                                        <th class="text-center">Rate</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(\App\BusinessTaxTypes::all() as $tax)
                                            <tr>
                                                <td class="text-center">{{ $tax->tax }}</td>
                                                <td class="text-center">{{ $tax->percent }}%</td>
                                                <td class="text-right">
                                                    <a href="#" class="btn btn-sm btn-primary editBusinessTaxType" data-id="{{ $tax->id }}"><i class="fa fa-edit"></i> EDIT</a>
                                                    {{--<a href="#" class="btn btn-sm btn-danger deleteBusinessTaxType" data-id="{{ $tax->id }}"><i class="fa fa-remove"></i> DELETE</a>--}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="deleteBusinessTaxType" tabindex="-1" role="dialog" aria-labelledby="deleteBusinessTaxType" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete Business Tax Type</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Business Tax Type?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#civilStatus').DataTable();

            $(document).on('click', '.editBusinessTaxType', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/business-tax-type-edit") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');

                    }
                });
            });

            $(document).on('click', '.deleteBusinessTaxType', function(e) {
                e.preventDefault();

                var url = "{{ url("admin/super/business-tax-type/delete") }}/" + $(this).attr('data-id');

                $("#deleteBusinessTaxType").find('#url').attr('href', url);
                $("#deleteBusinessTaxType").modal('show');
            });
        })
    </script>
@endsection

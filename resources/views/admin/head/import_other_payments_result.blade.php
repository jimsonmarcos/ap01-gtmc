@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li><a href="{{ route('import_other_payments') }}" class="link-effect">Import Other Payments</a></li>
        <li>Import Other Payments Result</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-12 text-center">
                                    <div class="block">
                                        <div class="block-content block-content-full">

                                            <a href="{{ route('import_other_payments') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back to Import</a>

                                            <br>
                                            <br>

                                            @if(!empty($valid))
                                            <h4>Valid Payments</h4>

                                            <table class="table table-bordered table-condensed">
                                                <thead>
                                                    <tr>
                                                        <th>GTMC ID Number</th>
                                                        <th>Username</th>
                                                        <th>Transaction Type</th>
                                                        <th>Payment Method</th>
                                                        <th>Pay Period</th>
                                                        <th>AR Number</th>
                                                        <th>Payment Date</th>
                                                        <th>Amount</th>
                                                        <th>Total Amount Paid</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($valid as $payment)
                                                    <tr>
                                                        <td>{{ $payment->User->Profile->id_number }}</td>
                                                        <td>{{ $payment->User->username }}</td>
                                                        <td>{{ $payment->transaction_type }}</td>
                                                        <td>{{ $payment->payment_method }}</td>
                                                        <td>{{ $payment->pay_period }}</td>
                                                        <td>{{ $payment->ar_number }}</td>
                                                        <td>{{ $payment->payment_date }}</td>
                                                        <td>{{ number_format($payment->amount, 2) }}</td>
                                                        <td>{{ number_format($payment->total_amount_paid, 2) }}</td>
                                                    </tr>
                                                </tbody>
                                                @endforeach
                                            </table>

                                            @endif

                                            @if(!empty($invalid))
                                                <br>
                                                <h4>Invalid Users</h4>

                                                @foreach($invalid as $payment)
                                                    <tr>
                                                        <td></td>
                                                        <td>{{ $payment->username }}</td>
                                                        <td>{{ $payment->code }}</td>
                                                        <td>{{ $payment->payment_method }}</td>
                                                        <td>{{ $payment->pay_period }}</td>
                                                        <td></td>
                                                        <td>{{ date('m/d/Y', strtotime($payment->date)) }}</td>
                                                        <td>{{ number_format($payment->amount, 2) }}</td>
                                                        <td>{{ number_format($payment->total_amount_paid, 2) }}</td>
                                                    </tr>
                                                    </tbody>
                                                @endforeach
                                            @endif


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
   
@endsection

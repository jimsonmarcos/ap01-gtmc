@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Area</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="fa fa-users"></span> List of Area</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('head_area_create') }}"><span class="fa fa-user-plus"></span> New Area</a>
                                    </li>
                                </ul>

                                <br />

                                <table id="civilStatus" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Province</th>
                                        <th>Municipality</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(\App\CivilStatus::orderBy('title')->get() as $civilStatus)
                                        <tr>
                                            <td>{{ $civilStatus->title }}</td>
                                            <td></td>
                                            <td>
                                                <a href="#" class="btn btn-sm btn-primary editCivilStatus" data-id="{{ $civilStatus->id }}"><i class="fa fa-edit"></i> EDIT</a>
                                                <a href="#" class="btn btn-sm btn-danger deleteCivilStatus" data-id="{{ $civilStatus->id }}"><i class="fa fa-remove"></i> DELETE</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="deleteCivilStatus" tabindex="-1" role="dialog" aria-labelledby="deletePosition" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete Civil Status</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Civil Status?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#civilStatus').DataTable();

            $(document).on('click', '.editCivilStatus', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/civil-status-edit") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');

                    }
                });
            });

            $(document).on('click', '.deleteCivilStatus', function(e) {
                e.preventDefault();

                var url = "{{ url("admin/super/civil-status/delete") }}/" + $(this).attr('data-id');

                $("#deleteCivilStatus").find('#url').attr('href', url);
                $("#deleteCivilStatus").modal('show');
            });
        })
    </script>
@endsection

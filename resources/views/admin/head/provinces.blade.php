@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>City/Provinces</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                {{--<div class="text-right">--}}
                                    {{--<a class="nav-link addProvince btn btn-primary" href="#"><span class="fa fa-map-marker"></span> New City/Provinces</a>--}}
                                {{--</div>--}}
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="fa fa-map-marker"></span> City/Provinces</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('/admin/super/areas') }}"><span class="fa fa-map-marker"></span> Areas</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('/admin/super/malls') }}"><span class="fa fa-map-marker"></span> Malls</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('/admin/super/outlets') }}"><span class="fa fa-map-marker"></span> Outlets</a>
                                    </li>
                                </ul>

                                <br />

                                <table id="provinces" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>City/Province</th>
                                        {{--<th>Action</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(\App\Provinces::orderBy('province')->get() as $province)
                                        <tr>
                                            <td>{{ $province->province }}</td>
                                            {{--<td>--}}
                                                {{--<a href="#" class="btn btn-sm btn-primary editProvince" data-id="{{ $province->id }}"><i class="fa fa-edit"></i> EDIT</a>--}}
                                                {{--<a href="#" class="btn btn-sm btn-danger deleteProvince" data-id="{{ $province->id }}"><i class="fa fa-remove"></i> DELETE</a>--}}
                                            {{--</td>--}}
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="deleteProvince" tabindex="-1" role="dialog" aria-labelledby="deletePosition" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete City/Province</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this City/Province?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>

    <div id="addProvince" tabindex="-1" role="dialog" aria-labelledby="addProvince" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <form action="" method="POST">
            {{ csrf_field() }}

            <div role="document" class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="exampleModalLabel" class="modal-title">Add City/Province</h4>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">City/Province</label>
                            <input type="text" class="form-control" name="province">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                        <button class="btn btn-primary" style="color: #FFF;"><span class="fa fa-save"></span> Confirm</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#provinces').DataTable();

            $(document).on('click', '.editProvince', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/province-edit") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');

                    }
                });
            });

            $(document).on('click', '.deleteProvince', function(e) {
                e.preventDefault();

                var url = "{{ url("admin/super/province/delete") }}/" + $(this).attr('data-id');

                $("#deleteProvince").find('#url').attr('href', url);
                $("#deleteProvince").modal('show');
            });

            $(document).on('click', '.addProvince', function(e) {
                e.preventDefault();

                $("#addProvince").modal('show');
            });
        })
    </script>
@endsection

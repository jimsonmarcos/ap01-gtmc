@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Account</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="block block-themed">

                                    <div class="block-header bg-primary">
                                        <h3 class="block-title">Account</h3>
                                    </div>

                                    <div class="block-content">
                                        <form class="form-horizontal push-5-t" action="" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}

                                            <div class="form-group">
                                                <label class="col-xs-12" for="account_name">Account Logo</label>
                                                <div class="col-xs-12">
                                                    <?php $logo = \App\Keyval::where('key', 'account_logo')->first()->value; ?>
                                                    @if(empty($logo))
                                                        <img src="{{ url("img/logo-placeholder.jpg") }}" alt="" style="max-width: 215px; max-height: 215px">
                                                    @else
                                                        <img src="{{ url($logo) }}" alt="" style="max-width: 215px; max-height: 215px;">
                                                    @endif
                                                    <div>
                                                        <input type="file" name="account_logo" accept="image/jpeg, image/png">
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="account_name">Account Name</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="account_name"
                                                           name="account_name" placeholder="Account Name" value="{{ \App\Keyval::where('key', 'account_name')->first()->value }}" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="account_code">Account Code</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="account_code"
                                                           name="account_code" placeholder="Account Code" value="{{ \App\Keyval::where('key', 'account_code')->first()->value }}" required
                                                           oninput="this.value = this.value.toUpperCase().replace(/[^0-9A-Z]/g, '');">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="address">Address</label>
                                                <div class="col-xs-12">
                                                    <textarea class="form-control" id="address" name="address" rows="7" placeholder="Address" required>{{ \App\Keyval::where('key', 'address')->first()->value }}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="email_address">Email Address</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="email" id="email_address" name="email_address" placeholder="Email address" value="{{ \App\Keyval::where('key', 'email_address')->first()->value }}" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="contact_number">Contact Number</label>
                                                <div class="col-xs-12">
                                                    <input class="mask-telephone form-control" type="text" id="contact_number" name="contact_number" placeholder="" value="{{ \App\Keyval::where('key', 'contact_number')->first()->value }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="sss_number">SSS Number</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="sss_number" name="sss_number" placeholder="Your 12 Digit SSS number" value="{{ \App\Keyval::where('key', 'sss_number')->first()->value }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="hdmf_number">HDMF Number</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="hdmf_number" name="hdmf_number" placeholder="Your 12 Digit HDMF number" value="{{ \App\Keyval::where('key', 'hdmf_number')->first()->value }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="philhealth_number">PhilHealth Number</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="philhealth_number" name="philhealth_number" placeholder="Your 12 Digit Philhealth number" value="{{ \App\Keyval::where('key', 'philhealth_number')->first()->value }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="tin_number">Tax ID Number</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="tin_number" name="tin_number" placeholder="Your 12 Digit TIN number" value="{{ \App\Keyval::where('key', 'tin_number')->first()->value }}" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="tin_number">Business Tax Type</label>
                                                <div class="col-xs-12">
                                                    <select name="business_tax_type" id="" class="form-control" required>
                                                        <option value=""></option>
                                                        @foreach(\App\BusinessTaxTypes::all() as $t)
                                                            <option value="{{ $t->tax }}" @if(\App\Keyval::where('key', 'business_tax_type')->first()->value == $t->tax) selected @endif>{{ $t->tax }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12 text-right">
                                                    <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-check push-5-r"></i> Update</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')

@endsection

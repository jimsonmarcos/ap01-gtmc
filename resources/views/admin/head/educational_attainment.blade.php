@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Educational Attainment</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="fa fa-users"></span> List of Educational Attianment</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('head_educational_attainment_create') }}"><span class="fa fa-user-plus"></span> New Educational Attainment</a>
                                    </li>
                                </ul>

                                <br />

                                <table id="educationalAttainments" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Position</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(\App\EducationalAttainments::orderBy('title')->get() as $educationalAttainment)
                                        <tr>
                                            <td>{{ $educationalAttainment->title }}</td>
                                            <td>
                                                <a href="#" class="btn btn-sm btn-primary editEducationalAttainment" data-id="{{ $educationalAttainment->id }}"><i class="fa fa-edit"></i> EDIT</a>
                                                <a href="#" class="btn btn-sm btn-danger deleteEducationalAttainment" data-id="{{ $educationalAttainment->id }}"><i class="fa fa-remove"></i> DELETE</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="deleteEducationalAttainment" tabindex="-1" role="dialog" aria-labelledby="deleteEducationalAttainment" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete Educational Attainment</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Educational Attainment?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#educationalAttainments').DataTable();

            $(document).on('click', '.editEducationalAttainment', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/educational-attainment-edit") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');

                    }
                });
            });

            $(document).on('click', '.deleteEducationalAttainment', function(e) {
                e.preventDefault();

                var url = "{{ url("admin/super/educational-attainment/delete") }}/" + $(this).attr('data-id');

                $("#deleteEducationalAttainment").find('#url').attr('href', url);
                $("#deleteEducationalAttainment").modal('show');
            });
        })
    </script>
@endsection

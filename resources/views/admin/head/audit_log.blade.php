@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Audit Log</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <br />

                                <table id="auditLog" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Username</th>
                                        <th>Category</th>
                                        <th>Action</th>
                                        <th>Description</th>
                                        <th>Date/Time</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#auditLog').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url": "{{ url("admin/super/audit-log/datatables") }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "username" },
                    { "data": "category" },
                    { "data": "action" },
                    { "data": "description" },
                    { "data": "created_at" }
                ],
                "pageLength": 100,
                searchDelay: 350,
                "order": [[ 4, "desc" ]]
            });

        })
    </script>
@endsection

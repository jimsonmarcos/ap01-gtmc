@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Bank</li>
        <li><a class="link-effect" href="{{ route('head_bank_create')}}">New Bank</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <br />
                                    <form action="" method="POST">
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label class="col-xs-12" for="bank_name">Bank</label>
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" id="bank_name" name="bank_name" placeholder="Bank Name" required />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-xs-12" for="bank_code">Bank Code</label>
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" id="bank_code" name="bank_code" placeholder="Bank Code" required />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-xs-12" for="bank_full_name">Bank Full Name</label>
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" id="bank_full_name" name="bank_full_name" placeholder="Bank Full Name" required />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-12 text-right">
                                                <br />
                                                <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-save push-5-r"></i> SUBMIT</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Business Tax Type</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('head_business_tax_type') }}"><span class="fa fa-users"></span> List of Business Tax Type</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link active"><span class="fa fa-user-plus"></span> New Business Tax Type</a>
                                    </li>
                                </ul>

                                <br>

                                <div class="col-md-4 col-md-offset-4">
                                    <br />
                                    <form action="" method="POST">
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label class="col-xs-12" for="login1-username">Business Tax Type</label>
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" id="tax" name="tax" placeholder="Business Tax Type here" required />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-xs-12" for="login1-username">Percent</label>
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" id="percent" name="percent" placeholder="Percent here" required />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-12 text-right">
                                                <br />
                                                <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-save push-5-r"></i> SUBMIT</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
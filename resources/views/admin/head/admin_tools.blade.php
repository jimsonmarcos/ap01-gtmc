@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Admin Tools</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="fa fa-users"></span> User Admins</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('admin/manage-admin/users/add') }}"><span class="fa fa-user-plus"></span> New Admin</a>
                                    </li>
                                </ul>

                                <br>

                                <table id="users-dt" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Complete Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Company Code</th>
                                        <th>Admin Type</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#users-dt').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url": "{{ url('api/datatables/admin/users') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "name" },
                    { "data": "username" },
                    { "data": "email" },
                    { "data": "company_code" },
                    { "data": "role" },
                    { "data": "action" }
                ]
            });
        })
    </script>
@endsection

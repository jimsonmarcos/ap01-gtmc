@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Position</li>
        <li><a class="link-effect" href="{{ route('head_position_create')}}">New Position</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <br />
                                    <form action="" method="POST">
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label class="col-xs-12" for="login1-username">Position</label>
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" id="position" name="title" placeholder="Position name here"  oninput="this.value = this.value.toUpperCase()" required />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-12 text-right">
                                                <br />
                                                <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-save push-5-r"></i> SUBMIT</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

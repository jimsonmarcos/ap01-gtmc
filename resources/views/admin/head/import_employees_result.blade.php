@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li><a href="{{ route('import_employees') }}" class="link-effect">Import Employees</a></li>
        <li>Import Employees Result</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-12 text-center">
                                    <div class="block">
                                        <div class="block-content block-content-full">

                                            <a href="{{ route('import_employees') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back to Import</a>

                                            <br>
                                            <br>

                                            @if(!empty($validUsers))
                                            <h4>Valid Users</h4>

                                            <table class="table table-bordered table-condensed">
                                                <thead>
                                                    <tr>
                                                        <th>GTMC ID Number</th>
                                                        <th>Username</th>
                                                        <th>First Name</th>
                                                        <th>Middle Name</th>
                                                        <th>Last Name</th>
                                                        <th>Email</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($validUsers as $user)
                                                    <tr>
                                                        <td>{{ $user->Profile->id_number }}</td>
                                                        <td>{{ $user->username }}</td>
                                                        <td>{{ $user->Profile->first_name }}</td>
                                                        <td>{{ $user->Profile->middle_name }}</td>
                                                        <td>{{ $user->Profile->last_name }}</td>
                                                        <td>{{ $user->Profile->email }}</td>
                                                    </tr>
                                                </tbody>
                                                @endforeach
                                            </table>

                                            @endif

                                            @if(!empty($invalidUsers))
                                                <br>
                                                <h4>Invalid Users</h4>

                                                @foreach($invalidUsers as $user)
                                                    <tr>
                                                        <td>{{ $user->gtmc_id_number }}</td>
                                                        <td>{{ $user->username }}</td>
                                                        <td>{{ $user->first_name }}</td>
                                                        <td>{{ $user->middle_name }}</td>
                                                        <td>{{ $user->last_name }}</td>
                                                        <td>{{ $user->email }}</td>
                                                    </tr>
                                                    </tbody>
                                                @endforeach
                                            @endif


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
   
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <br />
                                    <form action="" method="POST">
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label class="col-xs-12" for="notarialFee">Notarial Fee</label>
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" id="notarialFee" name="notarial_fee" value="{{ \App\Keyval::where('key', 'notarial_fee')->first()->value }}" placeholder="" required />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-12 text-right">
                                                <br />
                                                <a class="btn btn-sm btn-primary" onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left push-5-r"></i> CANCEL</a>
                                                <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-save push-5-r"></i> SUBMIT</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link @if($nav == 'income') active @endif" href="{{ url('/admin/super/income-deduction-items') }}"><span class="fa fa-users"></span> Income Items</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link @if($nav == 'deductions') active @endif" href="{{ url('/admin/super/income-deduction-items/deductions') }}"><span class="fa fa-users"></span> Deduction Items</a>
                                    </li>
                                </ul>

                                <br>

                                <div>
                                    <h4>Add {{ $t }} item</h4>
                                    <br>

                                    <div class="col-md-6">
                                        <div class="row">
                                            <form action="" method="POST">
                                                {{ csrf_field() }}
                                                <input class="form-control" type="hidden" id="" name="type" value="{{ $t }}"  />

                                                <div class="form-group col-md-6">
                                                    <label class="col-xs-12" for="">Item</label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" type="text" id="" name="item" placeholder="Item name here" required />
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <div class="col-xs-12">
                                                        <br />
                                                        <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-save push-5-r"></i> SUBMIT</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <table id="positions" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($items as $item)
                                            <tr>
                                                <td>{{ $item->item }}</td>
                                                <td>
                                                    <a href="#" class="btn btn-sm btn-primary editIncomeDeductionItem" data-id="{{ $item->id }}"><i class="fa fa-edit"></i> EDIT</a>
                                                    <a href="#" class="btn btn-sm btn-danger deleteIncomeDeductionItem" data-id="{{ $item->id }}"><i class="fa fa-remove"></i> DELETE</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="deleteIncomeDeductionItem" tabindex="-1" role="dialog" aria-labelledby="IncomeDeductionItem" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete {{ $t }}</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this {{ $t }} item?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#positions').DataTable();

            $(document).on('click', '.editIncomeDeductionItem', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/income-deduction-item-edit") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');

                    }
                });
            });

            $(document).on('click', '.deleteIncomeDeductionItem', function(e) {
                e.preventDefault();

                var url = "{{ url("admin/super/income-deduction-item/delete") }}/" + $(this).attr('data-id');

                $("#deleteIncomeDeductionItem").find('#url').attr('href', url);
                $("#deleteIncomeDeductionItem").modal('show');
            });
        })
    </script>
@endsection

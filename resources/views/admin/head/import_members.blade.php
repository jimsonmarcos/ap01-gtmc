@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Import Members</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-12 text-center">
                                    <div class="block">
                                        <div class="block-content block-content-full">
                                            <form action="" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}

                                                <div class="form-group">
                                                    <div class="col-xs-12 text-center">
                                                        <input type="file" id="" name="import_file" style="display: inline-block">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-xs-12 text-center">
                                                        <br />
                                                        <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-save push-5-r"></i> SUBMIT</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
   
@endsection

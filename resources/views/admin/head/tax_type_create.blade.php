@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Tax Type</li>
        <li><a class="link-effect" href="{{ route('head_tax_type_create')}}">New</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <form action="" method="POST">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label class="col-xs-12" for="tax_schedule">Tax Schedule</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="tax_schedule" name="tax_schedule" size="1">
                                                <option value="semi-monthly">Semi-Monthly</option>
                                            </select>
                                            <br/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="tax_type">Tax Type</label>
                                        <div class="col-xs-12">

                                            <select class="form-control" id="tax_type" name="tax_type" size="1">
                                                <option value="S">S</option>
                                                <option value="S1">S1</option>
                                                <option value="S2">S2</option>
                                                <option value="S3">S3</option>
                                                <option value="S4">S4</option>
                                                <option value="M">M</option>
                                                <option value="M1">M1</option>
                                                <option value="M2">M2</option>
                                                <option value="M3">M3</option>
                                                <option value="M4">M4</option>
                                            </select>
                                            <br/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="base_rate">Base Rate</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="base_rate" name="base_rate" required />
                                            <br />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-xs-12" for="tax_status">Tax Status</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="tax_status" name="tax_status" required />
                                            <br />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="exemption">Exemption</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="exemption" name="exemption" required />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12 text-right">
                                            <br />
                                            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-arrow-right push-5-r"></i> Submit</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

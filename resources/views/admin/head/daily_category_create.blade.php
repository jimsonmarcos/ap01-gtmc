@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Daily Category</li>
        <li><a class="link-effect" href="{{ route('head_area_mall_outlet_create')}}">New</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <br />
                                    <form action="" method="POST">
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label class="col-xs-12" for="category">Category</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="category" name="category" size="1">
                                                    <option value="LOCATION">LOCATION</option>
                                                    <option value="REGION">REGION</option>
                                                </select>
                                                <br/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-xs-12" for="location">Location</label>
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control" id="location" name="location">
                                                <br/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-xs-12" for="rate">Rate</label>
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control" id="rate" name="rate">
                                                <br/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-xs-12" for="location">ECOLA</label>
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control" id="ecola" name="ecola">
                                                <br/>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-xs-12 text-right">
                                                <br />
                                                <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-arrow-right push-5-r"></i> Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

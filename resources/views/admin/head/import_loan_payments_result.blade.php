@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li><a href="{{ route('import_loan_payments') }}" class="link-effect">Import Loan Payments</a></li>
        <li>Import Loan Payments Result</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-12 text-center">
                                    <div class="block">
                                        <div class="block-content block-content-full">

                                            <a href="{{ route('import_loans') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back to Import</a>

                                            <br>
                                            <br>

                                            @if(!empty($valid))
                                            <h4>Valid Payments</h4>

                                            <table class="table table-bordered table-condensed">
                                                <thead>
                                                    <tr>
                                                        <th>GTMC ID Number</th>
                                                        <th>Username</th>
                                                        <th>Payment Method</th>
                                                        <th>Code</th>
                                                        <th>Pay Period</th>
                                                        <th>CLA Number</th>
                                                        <th>AR Number</th>
                                                        <th>Date</th>
                                                        <th>Amount</th>
                                                        <th>Interest</th>
                                                        <th>Additional Payment</th>
                                                        <th>Total Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($valid as $payment)
                                                    <tr>
                                                        <td>{{ $payment->Loan->User->Profile->id_number }}</td>
                                                        <td>{{ $payment->Loan->User->username }}</td>
                                                        <td>{{ $payment->payment_method }}</td>
                                                        <td>{{ $payment->Loan->is_voucher == 'Y' ? $payment->Loan->loan_type : $payment->Loan->LoanType->type }}</td>
                                                        <td>{{ $payment->pay_period }}</td>
                                                        <td>{{ $payment->Loan->cla_number }}</td>
                                                        <td>{{ $payment->ar_number }}</td>
                                                        <td>{{ date('m/d/Y', strtotime($payment->payment_date)) }}</td>
                                                        <td>{{ number_format($payment->amount, 2) }}</td>
                                                        <td>{{ number_format($payment->interest, 2) }}</td>
                                                        <td>{{ number_format($payment->additional_payment, 2) }}</td>
                                                        <td>{{ number_format($payment->total_amount, 2) }}</td>
                                                    </tr>
                                                </tbody>
                                                @endforeach
                                            </table>

                                            @endif

                                            @if(!empty($invalid))
                                                <br>
                                                <h4>Invalid Users</h4>

                                                @foreach($invalid as $loan)
                                                    <tr>
                                                        <td>{{ $loan->gtmc_id_number }}</td>
                                                        <td>{{ $loan->username }}</td>
                                                        <td>{{ $loan->cv_number }}</td>
                                                        <td>{{ date('m/d/Y', strtotime($loan->cv_date)) }}</td>
                                                        <td>{{ date('m/d/Y', strtotime($loan->date_request)) }}</td>
                                                        <td>{{ number_format($loan->request_amount, 2) }}</td>
                                                        <td>{{ number_format($loan->interest, 2) }}</td>
                                                        <td>{{ number_format($loan->granted_loan_amount, 2) }}</td>
                                                        <td>{{ number_format($loan->disbursed_amount, 2) }}</td>
                                                    </tr>
                                                    </tr>
                                                    </tbody>
                                                @endforeach
                                            @endif


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
   
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Bank</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="fa fa-users"></span> List of Bank</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('head_bank_create') }}"><span class="fa fa-user-plus"></span> New Bank</a>
                                    </li>
                                </ul>

                                <br>

                                <table id="bank" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Bank</th>
                                        <th>Bank Code</th>
                                        <th>Bank Full Name</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($bank as $bank)
                                        <tr>
                                            <td>{{ $bank->bank }}</td>
                                            <td>{{ $bank->bank_code }}</td>
                                            <td>{{ $bank->bank_full_name }}</td>
                                            <td>
                                                <a href="#" class="btn btn-sm btn-primary editBank" data-id="{{ $bank->id }}"><i class="fa fa-edit"></i> EDIT</a>
                                                <a href="#" class="btn btn-sm btn-danger deleteBank" data-id="{{ $bank->id }}"><i class="fa fa-remove"></i> DELETE</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="deletePosition" tabindex="-1" role="dialog" aria-labelledby="deletePosition" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete Bank</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Bank?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extendedscript')

    <script>
        $(document).ready(function() {
            $('#bank').DataTable();

            $(document).on('click', '.editBank', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/bank-edit") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');

                    }
                });
            });

            $(document).on('click', '.deleteBank', function(e) {
                e.preventDefault();

                var url = "{{ url("admin/super/bank/delete") }}/" + $(this).attr('data-id');

                $("#deletePosition").find('#url').attr('href', url);
                $("#deletePosition").modal('show');
            });
        })
    </script>
@endsection

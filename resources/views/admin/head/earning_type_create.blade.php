@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Earning Type</li>
        <li><a class="link-effect" href="{{ route('head_earning_type_create')}}">New</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <form action="" method="POST">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label class="col-xs-12" for="category">Category</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="category" name="category" size="1">
                                                <option value="allowance">Allowance</option>
                                                <option value="income">Income</option>
                                                <option value="adjustment">Adjustment</option>
                                                <option value="bonus">Bonus</option>
                                            </select>
                                            <br/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="code">Code</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="code" name="code" required />
                                            <br />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="tag">Tag</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="tag" name="tag" required />
                                            <br />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-xs-12" for="category">Taxable</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="category" name="category" size="1">
                                                <option value="tx">Tx</option>
                                                <option value="ntx">Ntx</option>
                                            </select>
                                            <br/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12 text-right">
                                            <br />
                                            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-arrow-right push-5-r"></i> Submit</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

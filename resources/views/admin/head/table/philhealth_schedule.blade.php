@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>PhilHealth Schedule</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br>

                                <div class="col-md-12 text-right">
                                    <button id="addPhilHealthContribution" class="btn btn-success"><i class="fa fa-plus"></i> Add Salary Bracket</button>
                                    <a class="btn btn-minw btn-primary" href="{{ route('head_philhealth_schedule_update') }}">
                                        Data
                                    </a>
                                    <a class="btn btn-minw btn-primary" href="#">
                                        Print
                                    </a>
                                </div>

                                <div class="col-md-12"><br /></div>

                                <table id="users-dt" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Salary Bracket</th>
                                        <th class="text-center">Salary Range</th>
                                        <th class="text-center">Salary Base</th>
                                        <th class="text-center">Premium Rate</th>
                                        {{--<th class="text-center">Total Monthly Premium</th>--}}
                                        {{--<th class="text-center">Employer Share</th>--}}
                                        {{--<th class="text-center">Employee Share</th>--}}
                                        <th class="text-center">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $x = 1; ?>
                                    @foreach($PhilHealthContributions as $philhealth)
                                        <tr class="text-center">
                                            <td>{{ $x }}</td>
                                            <td>{{ number_format($philhealth->salary_range_start, 2) }} - {{ number_format($philhealth->salary_range_end, 2) }}</td>
                                            <td>{{ number_format($philhealth->salary_base, 2) }}</td>
                                            <td>{{ number_format($philhealth->premium_rate, 2) }}</td>
                                            {{--<td>{{ number_format($philhealth->total_monthly_premium, 2) }}</td>--}}
                                            {{--<td>{{ number_format($philhealth->employer_share, 2) }}</td>--}}
                                            {{--<td>{{ number_format($philhealth->employee_share, 2) }}</td>--}}
                                            <td>
                                                <a href="#" class="btn btn-sm btn-primary editPhilHealthContribution" data-id="{{ $philhealth->id }}"><i class="fa fa-edit"></i> EDIT</a>
                                                <a href="#" class="btn btn-sm btn-danger deletePhilHealthContribution" data-id="{{ $philhealth->id }}"><i class="fa fa-remove"></i> DELETE</a>
                                            </td>
                                        </tr>
                                        <?php $x++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="deletePhilHealthContribution" tabindex="-1" role="dialog" aria-labelledby="deletePhilHealthContribution" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete PhilHealth Salary Bracket</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this PhilHealth Salary Bracket?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>

    <div id="addPhilHealthContributionModal" tabindex="-1" role="dialog" aria-labelledby="addPhilHealthContributionModal" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <form action="{{ route('philhealth_schedule_create') }}" method="POST">
                {{ csrf_field() }}

                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="exampleModalLabel" class="modal-title">Add SSS Salary Bracket</h4>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6" id="">
                                <div class="form-group">
                                    <label class="form-control-label">Salary Range Start</label>
                                    <input type="text" class="form-control" name="salary_range_start" value="" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="">
                                <div class="form-group">
                                    <label class="form-control-label">Salary Range End</label>
                                    <input type="text" class="form-control" name="salary_range_end" value="" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="">
                                <div class="form-group">
                                    <label class="form-control-label">Salary Base</label>
                                    <input type="text" class="form-control" name="salary_base" value="" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="">
                                <div class="form-group">
                                    <label class="form-control-label">Total Monthly Premium</label>
                                    <input type="text" class="form-control" name="total_monthly_premium" value="" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="">
                                <div class="form-group">
                                    <label class="form-control-label">Employer Share</label>
                                    <input type="text" class="form-control" name="employee_share" value="" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="">
                                <div class="form-group">
                                    <label class="form-control-label">Employee Share</label>
                                    <input type="text" class="form-control" name="employer_share" value="" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="">
                                <div class="form-group">
                                    <label class="form-control-label">Premium Rate %</label>
                                    <input type="text" class="form-control" name="premium_rate" value="" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                        <button class="btn btn-success" style="color: #FFF;"><span class="fa fa-save"></span> Confirm</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#civilStatus').DataTable();

            $(document).on('click', '.editPhilHealthContribution', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/philhealth-contribution-edit") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');

                    }
                });
            });

            $(document).on('click', '.deletePhilHealthContribution', function(e) {
                e.preventDefault();

                var url = "{{ url("admin/super/philhealth_schedule/delete") }}/" + $(this).attr('data-id');

                $("#deletePhilHealthContribution").find('#url').attr('href', url);
                $("#deletePhilHealthContribution").modal('show');
            });

            $(document).on('click', '#addPhilHealthContribution', function(e) {
                e.preventDefault();

                $("#addPhilHealthContributionModal").modal('show');
            });
        })
    </script>
@endsection
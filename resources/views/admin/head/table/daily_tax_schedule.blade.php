@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Tax Schedule</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link " href="{{ route('head_monthly_tax_schedule') }}"><span class="fa fa-users"></span> Monthly Tax Schedule</a>--}}
                                    {{--</li>--}}
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('head_semi_monthly_tax_schedule') }}"><span class="fa fa-users"></span> Semi Monthly Tax Schedule</a>
                                    </li>
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link " href="{{ route('head_weekly_tax_schedule') }}"><span class="fa fa-users"></span> Weekly Tax Schedule</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link active" href="#"><span class="fa fa-users"></span> Daily Tax Schedule</a>--}}
                                    {{--</li>--}}
                                </ul>

                                <br>

                                <div class="col-md-12 text-right">
                                    {{--<a class="btn btn-minw btn-primary" href="{{ route('head_daily_tax_schedule_update') }}">--}}
                                        {{--Data--}}
                                    {{--</a>--}}
                                    <button class="btn btn-minw btn-primary">
                                        <i class="fa fa-print"></i> Print
                                    </button>
                                </div>

                                <div class="col-md-12"><br /></div>

                                <table id="users-dt" class="table table-bordered ">
                                    <thead>
                                    <tr>
                                        <th colspan="10">Daily</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Exemption</td>
                                        <td></td>
                                        <td class="text-right">-</td>
                                        <td class="text-right">-</td>
                                        <td class="text-right">1.65</td>
                                        <td class="text-right">8.25</td>
                                        <td class="text-right">28.05</td>
                                        <td class="text-right">74.26</td>
                                        <td class="text-right">165.02</td>
                                        <td class="text-right">412.54</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td></td>
                                        <td class="text-center">0%</td>
                                        <td class="text-center">5%</td>
                                        <td class="text-center">10%</td>
                                        <td class="text-center">15%</td>
                                        <td class="text-center">20%</td>
                                        <td class="text-center">25%</td>
                                        <td class="text-center">30%</td>
                                        <td class="text-center">32%</td>
                                    </tr>
                                    <tr>
                                        <td colspan="10"></td>
                                    </tr>
                                    <tr>
                                        <th>Z</th>
                                        <th class="text-right">-</th>
                                        <th class="text-right">1</th>
                                        <th class="text-right">-</th>
                                        <th class="text-right">33</th>
                                        <th class="text-right">99</th>
                                        <th class="text-right">231</th>
                                        <th class="text-right">462</th>
                                        <th class="text-right">825</th>
                                        <th class="text-right">1,650</th>
                                    </tr>
                                    <tr>
                                        <th>S/ME</th>
                                        <th class="text-right">50.00</th>
                                        <th class="text-right">1</th>
                                        <th class="text-right">165</th>
                                        <th class="text-right">198</th>
                                        <th class="text-right">264</th>
                                        <th class="text-right">396</th>
                                        <th class="text-right">627</th>
                                        <th class="text-right">990</th>
                                        <th class="text-right">1815</th>
                                    </tr>
                                    <tr>
                                        <th>S1/ME1</th>
                                        <th class="text-right">75.00</th>
                                        <th class="text-right">1</th>
                                        <th class="text-right">248</th>
                                        <th class="text-right">281</th>
                                        <th class="text-right">347</th>
                                        <th class="text-right">479</th>
                                        <th class="text-right">710</th>
                                        <th class="text-right">1,073</th>
                                        <th class="text-right">1,898</th>
                                    </tr>
                                    <tr>
                                        <th>S2/ME2</th>
                                        <th class="text-right">100.00</th>
                                        <th class="text-right">1</th>
                                        <th class="text-right">330</th>
                                        <th class="text-right">363</th>
                                        <th class="text-right">429</th>
                                        <th class="text-right">561</th>
                                        <th class="text-right">792</th>
                                        <th class="text-right">1,155</th>
                                        <th class="text-right">1,980</th>
                                    </tr>
                                    <tr>
                                        <th>S3/ME3</th>
                                        <th class="text-right">125.00</th>
                                        <th class="text-right">1</th>
                                        <th class="text-right">413</th>
                                        <th class="text-right">446</th>
                                        <th class="text-right">512</th>
                                        <th class="text-right">644</th>
                                        <th class="text-right">875</th>
                                        <th class="text-right">1,238</th>
                                        <th class="text-right">2,063</th>
                                    </tr>
                                    <tr>
                                        <th>S4/ME4</th>
                                        <th class="text-right">150.00</th>
                                        <th class="text-right">1</th>
                                        <th class="text-right">495</th>
                                        <th class="text-right">528</th>
                                        <th class="text-right">594</th>
                                        <th class="text-right">726</th>
                                        <th class="text-right">957</th>
                                        <th class="text-right">1320</th>
                                        <th class="text-right">2145</th>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')

@endsection

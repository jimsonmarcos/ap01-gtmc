@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Tax Schedule</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link " href="{{ route('head_monthly_tax_schedule') }}"><span class="fa fa-users"></span> Monthly Tax Schedule</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('head_semi_monthly_tax_schedule') }}"><span class="fa fa-users"></span> Semi Monthly Tax Schedule</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="fa fa-users"></span> Weekly Tax Schedule</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('head_daily_tax_schedule') }}"><span class="fa fa-users"></span> Daily Tax Schedule</a>
                                    </li>
                                </ul>

                                <br>

                                <div class="col-md-12 text-right">
                                    {{--<a class="btn btn-minw btn-primary" href="{{ route('head_daily_tax_schedule_update') }}">--}}
                                    {{--Data--}}
                                    {{--</a>--}}
                                    <button class="btn btn-minw btn-primary">
                                        <i class="fa fa-print"></i> Print
                                    </button>
                                </div>

                                <div class="col-md-12"><br /></div>

                                <table id="users-dt" class="table table-bordered ">
                                    <thead>
                                    <tr>
                                        <th colspan="10">Weekly</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Exemption</td>
                                        <td></td>
                                        <td class="text-right">-</td>
                                        <td class="text-right">-</td>
                                        <td class="text-right">9.62</td>
                                        <td class="text-right">48.08</td>
                                        <td class="text-right">163.46</td>
                                        <td class="text-right">432.09</td>
                                        <td class="text-right">961.54</td>
                                        <td class="text-right">2,403.85</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td></td>
                                        <td class="text-center">0%</td>
                                        <td class="text-center">5%</td>
                                        <td class="text-center">10%</td>
                                        <td class="text-center">15%</td>
                                        <td class="text-center">20%</td>
                                        <td class="text-center">25%</td>
                                        <td class="text-center">30%</td>
                                        <td class="text-center">32%</td>
                                    </tr>
                                    <tr>
                                        <td colspan="10"></td>
                                    </tr>
                                    <tr>
                                        <th>Z</th>
                                        <th class="text-right">-</th>
                                        <th class="text-right">1</th>
                                        <th class="text-right">-</th>
                                        <th class="text-right">192</th>
                                        <th class="text-right">577</th>
                                        <th class="text-right">1,346</th>
                                        <th class="text-right">2,692</th>
                                        <th class="text-right">4,808</th>
                                        <th class="text-right">9,615</th>
                                    </tr>
                                    <tr>
                                        <th>S/ME</th>
                                        <th class="text-right">50.00</th>
                                        <th class="text-right">1</th>
                                        <th class="text-right">962</th>
                                        <th class="text-right">1,154</th>
                                        <th class="text-right">1,538</th>
                                        <th class="text-right">2,308</th>
                                        <th class="text-right">3,654</th>
                                        <th class="text-right">5,769</th>
                                        <th class="text-right">10,577</th>
                                    </tr>
                                    <tr>
                                        <th>S1/ME1</th>
                                        <th class="text-right">75.00</th>
                                        <th class="text-right">1</th>
                                        <th class="text-right">1,442</th>
                                        <th class="text-right">1,635</th>
                                        <th class="text-right">2,019</th>
                                        <th class="text-right">2,788</th>
                                        <th class="text-right">4,135</th>
                                        <th class="text-right">6,250</th>
                                        <th class="text-right">11,058</th>
                                    </tr>
                                    <tr>
                                        <th>S2/ME2</th>
                                        <th class="text-right">100.00</th>
                                        <th class="text-right">1</th>
                                        <th class="text-right">1,923</th>
                                        <th class="text-right">2,115</th>
                                        <th class="text-right">2,500</th>
                                        <th class="text-right">3,269</th>
                                        <th class="text-right">4,615</th>
                                        <th class="text-right">6,731</th>
                                        <th class="text-right">11,538</th>
                                    </tr>
                                    <tr>
                                        <th>S3/ME3</th>
                                        <th class="text-right">125.00</th>
                                        <th class="text-right">1</th>
                                        <th class="text-right">2,404</th>
                                        <th class="text-right">2,596</th>
                                        <th class="text-right">2,981</th>
                                        <th class="text-right">3,750</th>
                                        <th class="text-right">5,096</th>
                                        <th class="text-right">7,212</th>
                                        <th class="text-right">12,019</th>
                                    </tr>
                                    <tr>
                                        <th>S4/ME4</th>
                                        <th class="text-right">150.00</th>
                                        <th class="text-right">1</th>
                                        <th class="text-right">2,885</th>
                                        <th class="text-right">3,077</th>
                                        <th class="text-right">3,462</th>
                                        <th class="text-right">4,231</th>
                                        <th class="text-right">5,577</th>
                                        <th class="text-right">7,692</th>
                                        <th class="text-right">12,500</th>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')

@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Tax Schedule</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link active" href="#"><span class="fa fa-users"></span> Monthly Tax Schedule</a>--}}
                                    {{--</li>--}}
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('head_semi_monthly_tax_schedule') }}"><span class="fa fa-users"></span> Semi Monthly Tax Schedule</a>
                                    </li>
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link" href="{{ route('head_weekly_tax_schedule') }}"><span class="fa fa-users"></span> Weekly Tax Schedule</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link" href="{{ route('head_daily_tax_schedule') }}"><span class="fa fa-users"></span> Daily Tax Schedule</a>--}}
                                    {{--</li>--}}
                                </ul>

                                <br>

                                <div class="col-md-12 text-right">
                                    {{--<a class="btn btn-minw btn-primary" href="{{ route('head_daily_tax_schedule_update') }}">--}}
                                    {{--Data--}}
                                    {{--</a>--}}
                                    <button class="btn btn-minw btn-primary">
                                        <i class="fa fa-print"></i> Print
                                    </button>
                                </div>

                                <div class="col-md-12"><br /></div>

                                <table id="users-dt" class="table table-bordered ">
                                    <thead>
                                        <tr>
                                            <th colspan="10">Monthly</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Exemption</td>
                                            <td></td>
                                            <td class="text-right">-</td>
                                            <td class="text-right">-</td>
                                            <td class="text-right">41.67</td>
                                            <td class="text-right">208.33</td>
                                            <td class="text-right">708.33</td>
                                            <td class="text-right">1,875.00</td>
                                            <td class="text-right">4,166.67</td>
                                            <td class="text-right">10,416.67</td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td></td>
                                            <td class="text-center">0%</td>
                                            <td class="text-center">5%</td>
                                            <td class="text-center">10%</td>
                                            <td class="text-center">15%</td>
                                            <td class="text-center">20%</td>
                                            <td class="text-center">25%</td>
                                            <td class="text-center">30%</td>
                                            <td class="text-center">32%</td>
                                        </tr>
                                        <tr>
                                            <td colspan="10"></td>
                                        </tr>
                                        <tr>
                                            <th>Z</th>
                                            <th class="text-right">-</th>
                                            <th class="text-right">1</th>
                                            <th class="text-right">-</th>
                                            <th class="text-right">833</th>
                                            <th class="text-right">2,500</th>
                                            <th class="text-right">5,833</th>
                                            <th class="text-right">11,667</th>
                                            <th class="text-right">20,833</th>
                                            <th class="text-right">41,667</th>
                                        </tr>
                                        <tr>
                                            <th>S/ME</th>
                                            <th class="text-right">50.00</th>
                                            <th class="text-right">1</th>
                                            <th class="text-right">4,167</th>
                                            <th class="text-right">5,000</th>
                                            <th class="text-right">6,667</th>
                                            <th class="text-right">10,000</th>
                                            <th class="text-right">15,833</th>
                                            <th class="text-right">25,000</th>
                                            <th class="text-right">45,833</th>
                                        </tr>
                                        <tr>
                                            <th>S1/ME1</th>
                                            <th class="text-right">75.00</th>
                                            <th class="text-right">1</th>
                                            <th class="text-right">6,250</th>
                                            <th class="text-right">7,083</th>
                                            <th class="text-right">8,750</th>
                                            <th class="text-right">12,083</th>
                                            <th class="text-right">17,917</th>
                                            <th class="text-right">27,083</th>
                                            <th class="text-right">47,917</th>
                                        </tr>
                                        <tr>
                                            <th>S2/ME2</th>
                                            <th class="text-right">100</th>
                                            <th class="text-right">1</th>
                                            <th class="text-right">8,333</th>
                                            <th class="text-right">9,167</th>
                                            <th class="text-right">10,833</th>
                                            <th class="text-right">14,167</th>
                                            <th class="text-right">20,000</th>
                                            <th class="text-right">29,167</th>
                                            <th class="text-right">50,000</th>
                                        </tr>
                                        <tr>
                                            <th>S3/ME3</th>
                                            <th class="text-right">125.00</th>
                                            <th class="text-right">1</th>
                                            <th class="text-right">10,417</th>
                                            <th class="text-right">11,250</th>
                                            <th class="text-right">12,917</th>
                                            <th class="text-right">16,250</th>
                                            <th class="text-right">22,083</th>
                                            <th class="text-right">31,250</th>
                                            <th class="text-right">52,083</th>
                                        </tr>
                                        <tr>
                                            <th>S4/ME4</th>
                                            <th class="text-right">150.00</th>
                                            <th class="text-right">1</th>
                                            <th class="text-right">12,500</th>
                                            <th class="text-right">13,333</th>
                                            <th class="text-right">15,000</th>
                                            <th class="text-right">18,333</th>
                                            <th class="text-right">24,167</th>
                                            <th class="text-right">33,333</th>
                                            <th class="text-right">54,167</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')

@endsection

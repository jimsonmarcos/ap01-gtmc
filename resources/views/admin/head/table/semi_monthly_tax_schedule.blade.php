@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Tax Schedule</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link " href="{{ route('head_monthly_tax_schedule') }}"><span class="fa fa-users"></span> Monthly Tax Schedule</a>--}}
                                    {{--</li>--}}
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="fa fa-users"></span> Semi Monthly Tax Schedule</a>
                                    </li>
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link" href="{{ route('head_weekly_tax_schedule') }}"><span class="fa fa-users"></span> Weekly Tax Schedule</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link" href="{{ route('head_daily_tax_schedule') }}"><span class="fa fa-users"></span> Daily Tax Schedule</a>--}}
                                    {{--</li>--}}
                                </ul>

                                <br>

                                <div class="col-md-12 text-right">
                                    {{--<a class="btn btn-minw btn-primary" href="{{ route('head_daily_tax_schedule_update') }}">--}}
                                    {{--Data--}}
                                    {{--</a>--}}
                                    <button class="btn btn-minw btn-primary">
                                        <i class="fa fa-print"></i> Print
                                    </button>
                                </div>

                                <div class="col-md-12"><br /></div>

                                <table id="users-dt" class="table table-bordered ">
                                    <thead>
                                    <tr>
                                        <th colspan="8">Semi-Monthly</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Exemption</td>
                                        <td></td>
                                        <td class="text-right">-</td>
                                        <td class="text-right">-</td>
                                        <td class="text-right">1,250.00</td>
                                        <td class="text-right">5,416.67</td>
                                        <td class="text-right">20,416.67</td>
                                        <td class="text-right">100,416.67</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td></td>
                                        <td class="text-center">0%</td>
                                        <td class="text-center">20%</td>
                                        <td class="text-center">25%</td>
                                        <td class="text-center">30%</td>
                                        <td class="text-center">32%</td>
                                        <td class="text-center">35%</td>
                                    </tr>
                                    <tr>
                                        <td colspan="10"></td>
                                    </tr>
                                    <tr>
                                        <th>Salary Base</th>
                                        <th class="text-right">-</th>
                                        <th class="text-right">1</th>
                                        <th class="text-right">10,417.00</th>
                                        <th class="text-right">16,667.00</th>
                                        <th class="text-right">33,333.00</th>
                                        <th class="text-right">83,333.00</th>
                                        <th class="text-right">333,333.00</th>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')

@endsection

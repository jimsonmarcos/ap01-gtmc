@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Premium Rate</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                           <div class="row">

                               <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                   <br />
                                   <li class="nav-item">
                                       <a class="nav-link " href="{{ url('admin/super/premium-rate') }}"><span class="fa fa-users"></span> Premium Rate</a>
                                   </li>
                                   <li class="nav-item">
                                       <a class="nav-link active" href="#"><span class="fa fa-user-plus"></span> New Premium Rate</a>
                                   </li>
                               </ul>

                               <div class="col-md-12"><br /></div>

                               <form action="" method="POST">
                                   {{ csrf_field() }}

                                   <div class="form-group">
                                       <label class="col-xs-12" for="category">Tag</label>
                                       <div class="col-xs-12">
                                           <select class="form-control" id="tag" name="tag" size="1">
                                               <option value="OT">OT</option>
                                               <option value="RD">RD</option>
                                               <option value="SPE">SPE</option>
                                               <option value="SPERD">SPERD</option>
                                               <option value="LEG">LEG</option>
                                               <option value="LEGRD">LEGRD</option>
                                               <option value="ND">ND</option>
                                           </select>
                                           <br/>
                                       </div>
                                   </div>


                                   <div class="form-group">
                                       <label class="col-xs-12" for="ot">Tag</label>
                                       <div class="col-xs-12">
                                           <input class="form-control" type="text" id="ot" name="ot" required />
                                           <br />
                                       </div>
                                   </div>

                                   <div class="form-group">
                                       <label class="col-xs-12" for="premium">Premium</label>
                                       <div class="col-xs-12">
                                           <input class="form-control" type="text" id="premium" name="premium" required />
                                           <br />
                                       </div>
                                   </div>

                                   <div class="form-group">
                                       <div class="col-xs-12 text-right">
                                           <br />
                                           <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-arrow-right push-5-r"></i> Submit</button>
                                       </div>
                                   </div>

                               </form>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="deleteHDMFContribution" tabindex="-1" role="dialog" aria-labelledby="deleteHDMFContribution" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete HDMF Salary Bracket</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this HDMF Salary Bracket?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#civilStatus').DataTable();

            $(document).on('click', '.editHDMFContribution', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/hdmf-contribution-edit") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');

                    }
                });
            });

            $(document).on('click', '.deleteHDMFContribution', function(e) {
                e.preventDefault();

                var url = "{{ url("admin/super/hdmf_schedule/delete") }}/" + $(this).attr('data-id');

                $("#deleteHDMFContribution").find('#url').attr('href', url);
                $("#deleteHDMFContribution").modal('show');
            });

            $(document).on('click', '#addHDMFContribution', function(e) {
                e.preventDefault();

                $("#addHDMFContributionModal").modal('show');
            });
        })
    </script>
@endsection
@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>SSS Schedule</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br>

                                <div class="col-md-12 text-right">
                                    <button id="addSSSContribution" class="btn btn-success"><i class="fa fa-plus"></i> Add Salary Bracket</button>
                                    <a class="btn btn-minw btn-primary" href="{{ route('head_sss_schedule_update') }}">
                                        Data
                                    </a>
                                    <a class="btn btn-minw btn-primary" href="#">
                                        <i class="fa fa-print"></i> Print
                                    </a>
                                </div>

                                <div class="col-md-12"><br /></div>

                                <table id="users-dt" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Salary Bracket</th>
                                        <th class="text-center">Range of Compensation</th>
                                        <th class="text-center">Monthly Salary Credit</th>
                                        <th class="text-center">ER</th>
                                        <th class="text-center">EE</th>
                                        <th class="text-center">EC</th>
                                        <th class="text-center">Total</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $x = 1; ?>
                                        @foreach($SSSContributions as $sss)
                                            <tr class="text-center">
                                                <td>{{ $x }}</td>
                                                <td>{{ number_format($sss->compensation_range_start, 2) }} - {{ number_format($sss->compensation_range_end, 2) }}</td>
                                                <td>{{ number_format($sss->monthly_salary_credit, 2) }}</td>
                                                <td>{{ number_format($sss->er, 2) }}</td>
                                                <td>{{ number_format($sss->ee, 2) }}</td>
                                                <td>{{ number_format($sss->ec, 2) }}</td>
                                                <td>{{ number_format($sss->total, 2) }}</td>
                                                <td>
                                                    <a href="#" class="btn btn-sm btn-primary editSSSContribution" data-id="{{ $sss->id }}"><i class="fa fa-edit"></i> EDIT</a>
                                                    <a href="#" class="btn btn-sm btn-danger deleteSSSContribution" data-id="{{ $sss->id }}"><i class="fa fa-remove"></i> DELETE</a>
                                                </td>
                                            </tr>
                                            <?php $x++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="deleteSSSContribution" tabindex="-1" role="dialog" aria-labelledby="deleteSSSContribution" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete SSS Salary Bracket</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this SSS Salary Bracket?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>

    <div id="addSSSContributionModal" tabindex="-1" role="dialog" aria-labelledby="addSSSContributionModal" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <form action="{{ route('sss_schedule_create') }}" method="POST">
            {{ csrf_field() }}

                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="exampleModalLabel" class="modal-title">Add SSS Salary Bracket</h4>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6" id="">
                                <div class="form-group">
                                    <label class="form-control-label">Compensation Range Start</label>
                                    <input type="text" class="form-control" name="compensation_range_start" value="" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="">
                                <div class="form-group">
                                    <label class="form-control-label">Compensation Range End</label>
                                    <input type="text" class="form-control" name="compensation_range_end" value="" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="">
                                <div class="form-group">
                                    <label class="form-control-label">Monthly Salary Credit</label>
                                    <input type="text" class="form-control" name="monthly_salary_credit" value="" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="">
                                <div class="form-group">
                                    <label class="form-control-label">ER</label>
                                    <input type="text" class="form-control" name="er" value="" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="">
                                <div class="form-group">
                                    <label class="form-control-label">EE</label>
                                    <input type="text" class="form-control" name="ee" value="" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="">
                                <div class="form-group">
                                    <label class="form-control-label">Total</label>
                                    <input type="text" class="form-control" name="total" value="" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                        <button class="btn btn-success" style="color: #FFF;"><span class="fa fa-save"></span> Confirm</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#civilStatus').DataTable();

            $(document).on('click', '.editSSSContribution', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/sss-contribution-edit") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');

                    }
                });
            });

            $(document).on('click', '.deleteSSSContribution', function(e) {
                e.preventDefault();

                var url = "{{ url("admin/super/sss_schedule/delete") }}/" + $(this).attr('data-id');

                $("#deleteSSSContribution").find('#url').attr('href', url);
                $("#deleteSSSContribution").modal('show');
            });

            $(document).on('click', '#addSSSContribution', function(e) {
                e.preventDefault();

                $("#addSSSContributionModal").modal('show');
            });
        })
    </script>
@endsection

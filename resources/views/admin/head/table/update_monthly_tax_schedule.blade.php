@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Tax Schedule</li>
        <li><a class="link-effect" href="{{ route('head_monthly_tax_schedule_update')}}">Update Monthly Tax Schedule</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-12 text-center">
                                    <h2 class="content-heading">Monthly Tax Schedule </h2>
                                    <p>*upload .xls, .xlsx, .csv</p>
                                    <div class="block">
                                        <div class="block-content block-content-full">
                                            <form class="dropzone dz-clickable" action="base_forms_pickers_more.php">
                                                <div class="dz-default dz-message"><span>Drop files here to upload</span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div class="block">
                                        <div class="block-content block-content-full">
                                            <form action="" method="POST">


                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="file" id="monthly_tax_schedule" name="monthly_tax_schedule">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-12 text-right">
                                                            <br />
                                                            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-save push-5-r"></i> SUBMIT</button>
                                                        </div>
                                                    </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
   
@endsection

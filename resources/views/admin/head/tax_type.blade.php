@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Tax Type</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="fa fa-users"></span> List of Tax Type</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('head_tax_type_create') }}"><span class="fa fa-user-plus"></span> New Tax Type</a>
                                    </li>
                                </ul>

                                <table id="users-dt" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Tax Schedule</th>
                                        <th>Tax Type</th>
                                        <th>Base Rate</th>
                                        <th>Tax Status</th>
                                        <th>Exemption</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <td>SEMI-MONTHLY</td>
                                    <td>S</td>
                                    <td>1</td>
                                    <td>0</td>
                                    <td>-</td>
                                    <td>
                                        <a href="#">Edit</a> :: <a href="#">Delete</a>
                                    </td>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Admin Tools</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <!-- <div class="col-md-12">
                                    <h2 class="content-heading">Email</h2>

                                    <div class="row">
                                        <div class="col-md-3 text-center">
                                            <a href="#">
                                                <i class="fa fa-envelope fa-2x" aria-hidden="true"></i>
                                                <p>Email Templates</p>
                                            </a>
                                        </div>
                                    </div>

                                </div> -->

                                <div class="col-md-12">
                                    <h2 class="content-heading">Custom Items</h2>

                                    <div class="row">

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_position') }}">
                                                <i class="fa fa-id-card fa-2x" aria-hidden="true"></i>
                                                <p>Position</p>
                                            </a>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_relationship') }}">
                                                <i class="fa fa-users fa-2x" aria-hidden="true"></i>
                                                <p>Relationships</p>
                                            </a>
                                        </div>

                                        {{--<div class="col-md-3 text-center">--}}
                                            {{--<a href="{{ route('head_provinces') }}">--}}
                                                {{--<i class="fa fa-building fa-2x" aria-hidden="true"></i>--}}
                                                {{--<p>Provinces</p>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_daily_category') }}">
                                                <i class="fa fa-location-arrow fa-2x" aria-hidden="true"></i>
                                                <p>Rate Location</p>
                                            </a>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_bank') }}">
                                                <i class="fa fa-university fa-2x" aria-hidden="true"></i>
                                                <p>Banks</p>
                                            </a>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_business_tax_type') }}">
                                                <i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>
                                                <p>Business Tax Type</p>
                                            </a>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_loan_type') }}">
                                                <i class="fa fa-money fa-2x" aria-hidden="true"></i>
                                                <p>Loan Type</p>
                                            </a>
                                        </div>

                                        {{--<div class="col-md-3 text-center">--}}
                                            {{--<a href="{{ route('head_earning_type') }}">--}}
                                                {{--<i class="fa fa-money fa-2x" aria-hidden="true"></i>--}}
                                                {{--<p>Income Type</p>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}

                                        {{--<div class="col-md-3 text-center">--}}
                                            {{--<a href="{{ route('head_deduction_type') }}">--}}
                                                {{--<i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>--}}
                                                {{--<p>Deduction Type</p>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}

                                        {{--<div class="col-md-3 text-center">--}}
                                            {{--<a href="{{ route('head_malls') }}">--}}
                                                {{--<i class="fa fa-building fa-2x" aria-hidden="true"></i>--}}
                                                {{--<p>Mall</p>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}

                                        {{--<div class="col-md-3 text-center">--}}
                                            {{--<a href="{{ route('head_outlets') }}">--}}
                                                {{--<i class="fa fa-building fa-2x" aria-hidden="true"></i>--}}
                                                {{--<p>Store Outlet</p>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}

                                        {{--<div class="col-md-3 text-center">--}}
                                            {{--<a href="{{ route('head_areas') }}">--}}
                                                {{--<i class="fa fa-building fa-2x" aria-hidden="true"></i>--}}
                                                {{--<p>Area</p>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_provinces') }}">
                                                <i class="fa fa-building fa-2x" aria-hidden="true"></i>
                                                <p>Department</p>
                                            </a>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_coop_share_interest') }}">
                                                <i class="fa fa-percent fa-2x" aria-hidden="true"></i>
                                                <p>COOP Share Interest</p>
                                            </a>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_loanalable_percentage') }}">
                                                <i class="fa fa-percent fa-2x" aria-hidden="true"></i>
                                                <p>Loanable Percentage</p>
                                            </a>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_membership_fee') }}">
                                                <i class="fa fa-money fa-2x" aria-hidden="true"></i>
                                                <p>Membership Fee</p>
                                            </a>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_notarial_fee') }}">
                                                <i class="fa fa-money fa-2x" aria-hidden="true"></i>
                                                <p>Notarial Fee</p>
                                            </a>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_income_deduction_items') }}">
                                                <i class="fa fa-money fa-2x" aria-hidden="true"></i>
                                                <p>Income/Deduction items</p>
                                            </a>
                                        </div>


                                    </div>

                                </div>


                                <div class="col-md-12">
                                    <h2 class="content-heading">Table</h2>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ route('head_semi_monthly_tax_schedule') }}">
                                            <i class="fa  fa-calendar fa-2x" aria-hidden="true"></i>
                                            <p>Tax Schedule</p>
                                        </a>
                                    </div>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ route('head_sss_schedule') }}">
                                            <i class="fa fa-building fa-2x" aria-hidden="true"></i>
                                            <p>SSS</p>
                                        </a>
                                    </div>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ route('head_hdmf_schedule') }}">
                                            <i class="fa fa-building fa-2x" aria-hidden="true"></i>
                                            <p>HDMF</p>
                                        </a>
                                    </div>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ route('head_philhealth_schedule') }}">
                                            <i class="fa fa-building fa-2x" aria-hidden="true"></i>
                                            <p>PhilHealth</p>
                                        </a>
                                    </div>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ route('head_premium_rate') }}">
                                            <i class="fa fa-building fa-2x" aria-hidden="true"></i>
                                            <p>Premium Rate</p>
                                        </a>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <h2 class="content-heading">Data Import</h2>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ url('admin/super/import/employees') }}">
                                            <i class="fa fa-upload fa-2x" aria-hidden="true"></i>
                                            <p>Import Employees</p>
                                        </a>
                                    </div>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ url('admin/super/import/members') }}">
                                            <i class="fa fa-upload fa-2x" aria-hidden="true"></i>
                                            <p>Import Members</p>
                                        </a>
                                    </div>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ url('admin/super/import/other-payments') }}">
                                            <i class="fa fa-upload fa-2x" aria-hidden="true"></i>
                                            <p>Import Other Payments</p>
                                        </a>
                                    </div>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ url('admin/super/import/loans') }}">
                                            <i class="fa fa-upload fa-2x" aria-hidden="true"></i>
                                            <p>Import Loans</p>
                                        </a>
                                    </div>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ url('admin/super/import/loan-payments') }}">
                                            <i class="fa fa-upload fa-2x" aria-hidden="true"></i>
                                            <p>Import Loan Payments</p>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <h2 class="content-heading">General Settings</h2>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ url('admin/manage-admin/users') }}">
                                            <i class="fa fa-user fa-2x" aria-hidden="true"></i>
                                            <p>User Admin</p>
                                        </a>
                                    </div>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ url('admin/manage-admin/clients') }}">
                                            <i class="fa fa-user fa-2x" aria-hidden="true"></i>
                                            <p>Client Admin</p>
                                        </a>
                                    </div>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ route('head_signatories') }}">
                                            <i class="fa fa-pencil fa-2x" aria-hidden="true"></i>
                                            <p>Signatories</p>
                                        </a>
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <h2 class="content-heading">Account Settings</h2>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ route('head_account') }}">
                                            <i class="fa fa-cog fa-2x" aria-hidden="true"></i>
                                            <p>Account</p>
                                        </a>
                                    </div>

                                    <div class="col-md-3 text-center">
                                        <a href="{{ route('head_audit_log') }}">
                                            <i class="fa fa-list fa-2x" aria-hidden="true"></i>
                                            <p>Audit Log</p>
                                        </a>
                                    </div>
                                </div>

                                <!-- <div class="col-md-12">
                                    <h2 class="content-heading">Super Admin</h2>

                                    <div class="row">

                                        {{--<div class="col-md-3 text-center">
                                            <a href="{{ route('head_membership_type') }}">
                                                <i class="fa fa-user-circle fa-2x" aria-hidden="true"></i>
                                                <p>Membership Type</p>
                                            </a>
                                        </div>
                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_membership_category') }}">
                                                <i class="fa fa-user-circle fa-2x" aria-hidden="true"></i>
                                                <p>Membership Category</p>
                                            </a>
                                        </div>--}}



                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_civil_status') }}">
                                                <i class="fa fa-user-circle fa-2x" aria-hidden="true"></i>
                                                <p>Civil Status</p>
                                            </a>
                                        </div>


                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_mode_of_transfer') }}">
                                                <i class="fa fa-paper-plane fa-2x" aria-hidden="true"></i>
                                                <p>Mode of Transfer</p>
                                            </a>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_educational_attainment') }}">
                                                <i class="fa fa-university fa-2x" aria-hidden="true"></i>
                                                <p>Educational Attainment</p>
                                            </a>
                                        </div> -->

                                        {{--<div class="col-md-3 text-center">--}}
                                            {{--<a href="{{ route('head_income_category') }}">--}}
                                                {{--<i class="fa fa-money fa-2x" aria-hidden="true"></i>--}}
                                                {{--<p>Income Category</p>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}

                                        {{--<div class="col-md-3 text-center">--}}
                                            {{--<a href="{{ route('head_deduction_category') }}">--}}
                                                {{--<i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>--}}
                                                {{--<p>Deduction Category</p>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}



                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')

@endsection

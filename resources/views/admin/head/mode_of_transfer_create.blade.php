@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Mode of Transfer</li>
        <li><a class="link-effect" href="{{ route('head_mode_of_transfer')}}">New</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <br />
                                    <form action="" method="POST">
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label class="col-xs-12" for="mode_of_transfer">Mode of Transfer</label>
                                            <div class="col-xs-12">
                                                <select name="transfer_mode" id="" class="form-control">
                                                    <option value="BANK DEPOSIT">BANK DEPOSIT</option>
                                                    <option value="BDO">BDO</option>
                                                    <option value="COURIER">COURIER</option>
                                                    <option value="GCASH">GCASH</option>
                                                    <option value="HAND OVER">HAND OVER</option>
                                                    <option value="METROBANK">METROBANK</option>
                                                    <option value="PICK UP">PICK UP</option>
                                                    <option value="UNIONBANK">UNIONBANK</option>
                                                </select>
                                                <br/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-xs-12" for="transfer_type">Transfer Type</label>
                                            <div class="col-xs-12">
                                                <select name="transfer_type" id="" class="form-control">
                                                    <option value="ATM">ATM</option>
                                                    <option value="CASH">CASH</option>
                                                </select>
                                                <br/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-xs-12" for="transfer_type">Bank Deposit</label>
                                            <div class="col-xs-12">
                                                <select name="transfer_type" id="" class="form-control">
                                                    <option value="ATM">YES</option>
                                                    <option value="CASH">NO</option>
                                                </select>
                                                <br/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-xs-12" for="bank">Bank</label>
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control" name="bank" value="">
                                                <br/>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-xs-12 text-right">
                                                <br />
                                                <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-arrow-right push-5-r"></i> Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

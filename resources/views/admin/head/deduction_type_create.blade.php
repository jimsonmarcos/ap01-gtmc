@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Deduction Type</li>
        <li><a class="link-effect" href="{{ route('head_deduction_type_create')}}">New</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-6 col-md-offset-3">
                                    <form action="" method="POST">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label class="col-xs-12" for="category">Deduction</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="category" name="category" size="1">
                                                <option value="allowance">Allowance</option>
                                                <option value="income">Income</option>
                                                <option value="adjustment">Adjustment</option>
                                                <option value="bonus">Bonus</option>
                                            </select>
                                            <br/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="code">Code</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="code" name="code" required />
                                            <br />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="tag">Tag</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="tag" name="tag" required />
                                            <br />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="cycle">Cycle</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="cycle" name="cycle" size="1">
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="AB">AB</option>
                                            </select>
                                            <br/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="fixed_amount">Fixed Amount</label>
                                        <div class="col-xs-12">
                                                <select class="form-control" id="fixed_amount" name="fixed_amount" size="1">
                                                    <option value="Yes">Yes</option>
                                                    <option value="No" selected>No</option>
                                                </select>
                                            <br/>
                                        </div>
                                    </div>

                                    {{--Will Be enabled if Fixed amount is no--}}
                                    <div class="form-group">
                                        <label class="col-xs-12" for="amount">Amount</label>
                                        {{-- @if (fixed_amount == YES )--}}
                                        {{--<div class="col-xs-12">--}}
                                            {{--<input class="form-control" type="text" id="amount" name="amount"/>--}}
                                            {{--<br />--}}
                                        {{--</div>--}}
                                        {{-- @else --}}
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="amount" name="amount" disabled/>
                                            <br />
                                        </div>
                                        {{--@endif--}}
                                    </div>


                                    <div class="form-group">
                                        <div class="col-xs-12 text-right">
                                            <br />
                                            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-arrow-right push-5-r"></i> Submit</button>
                                        </div>
                                    </div>

                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

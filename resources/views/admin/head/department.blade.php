@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Admin Tools</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <h2 class="content-heading">Custom Items</h2>

                                    <div class="row">


                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_provinces') }}">
                                                <i class="fa fa-building fa-2x" aria-hidden="true"></i>
                                                <p>Provinces</p>
                                            </a>
                                        </div>


                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_malls') }}">
                                                <i class="fa fa-building fa-2x" aria-hidden="true"></i>
                                                <p>Mall</p>
                                            </a>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_outlets') }}">
                                                <i class="fa fa-building fa-2x" aria-hidden="true"></i>
                                                <p>Store Outlet</p>
                                            </a>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('head_areas') }}">
                                                <i class="fa fa-building fa-2x" aria-hidden="true"></i>
                                                <p>Area</p>
                                            </a>
                                        </div>


                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')

@endsection

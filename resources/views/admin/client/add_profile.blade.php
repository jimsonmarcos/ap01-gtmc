@extends('layouts.app')

@section('content')


                <!-- Dashboard Counts Section-->
                <section class="dashboard-counts no-padding-bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="" class="form" method="POST" enctype="multipart/form-data">
                                            {{ csrf_field() }}

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a href="{{ url('/admin/clients') }}"><strong><i class="fa fa-arrow-left"></i> Back to Clients</strong></a>
                                                </div>


                                                <div class="col-md-12 text-center">
                                                    <h1>Client Information</h1>
                                                    <hr>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Client ID</label>
                                                        <input type="text" class="form-control" name="client_id" value="C{{ \App\Companies::all()->max('series') + 1 }}" readonly>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Company Code</label>
                                                        <input type="text" id="companyCode" class="form-control" name="company_code" value="{{ old('company_code') }}" maxlength="15" oninput="this.value = this.value.toUpperCase().trim()" required>
                                                    </div>
                                                </div>

                                                <div id="parentCompany" class="col-md-2" style="display: none;">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Parent Company</label>
                                                        <select name="parent_company_id" id=""
                                                                class="form-control">
                                                            <option value=""></option>
                                                            @foreach(\App\Companies::where('status', 'Active')->orderBy('company_code')->get() as $company)
                                                                <option value="{{ $company->id }}">{{ $company->company_code }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div>
                                                            <input id="hasParentCompany" type="checkbox" value="" name="">
                                                            <label for="hasParentCompany">Connect to Parent Company</label>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Company Name</label>
                                                        <input type="text" class="form-control" name="company_name" value="{{ old('company_name') }}" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"></div>

                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Company Address</label>
                                                        <textarea name="address" id="" rows="3"
                                                                  class="form-control" placeholder="Blk no., Street, Barangay, City or State , Country" required>{{ old('address') }}</textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"></div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Trunk Line</label>
                                                        <input type="text" class="mask-telephone form-control" name="trunk_line" value="{{ old('trunk_line') }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Fax Number</label>
                                                        <input type="text" class="mask-telephone form-control" name="fax" value="{{ old('fax') }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-12"></div>

                                                <div class="col-md-4">
                                                    {{--<div class="form-group">--}}
                                                    {{--<label class="form-control-label">SSS</label>--}}
                                                    {{--<input type="text" class="form-control" name="sss">--}}
                                                    {{--</div>--}}
                                                    {{--<div class="form-group">--}}
                                                    {{--<label class="form-control-label">HDMF</label>--}}
                                                    {{--<input type="text" class="form-control" name="hdmf">--}}
                                                    {{--</div>--}}
                                                    {{--<div class="form-group">--}}
                                                    {{--<label class="form-control-label">PhilHealth</label>--}}
                                                    {{--<input type="text" class="form-control" name="philhealth">--}}
                                                    {{--</div>--}}
                                                    <div class="form-group">
                                                        <label class="form-control-label">TIN</label>
                                                        <input type="text" class="form-control" name="tin">
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Effectivity Date</label>
                                                        <div class='input-group btn-mmddyyyy'>
                                                            <input type='text' class="form-control" name="client_start_date" value="{{ old('client_start_date') }}" placeholder="mm/dd/yyyy" required />
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>



                                                <br>

                                                <div class="col-md-12 text-right">
                                                    <button href="#" class="btn btn-primary">Next <span class="fa fa-chevron-right" style="font-size: 0.8em;"></span></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#hasParentCompany").change(function() {
                const isChecked = $(this).is(':checked');

                if(isChecked === true) {
                    $("#parentCompany").slideDown().find('select').prop('required', true);
                } else {
                    $("#companyCode").val('');
                    $("#parentCompany").prop('checked', true).slideUp().find('select').prop('required', false);
                    $("#parentCompany option").first().prop('selected', true);
                }
            });

            // $("#parentCompany select").change(function() {
            //     var companyCode = $(this).find(":selected").text();
            //     console.log(companyCode);
            //
            //     if(companyCode.length > 0) {
            //         $("#companyCode").val(companyCode + ' - ');
            //     } else {
            //         $("#companyCode").val('');
            //     }
            // });
        });
    </script>
@endsection

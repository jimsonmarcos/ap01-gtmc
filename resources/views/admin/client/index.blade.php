@extends('layouts.app')

@section('content')



                <ul class="breadcrumb">
                    <div class="container-fluid">
                        <li class="breadcrumb-item active">Client Profile</li>
                    </div>
                </ul>
                <!-- Dashboard Counts Section-->
                <section class="dashboard-counts no-padding-bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <br />
                                            <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#"><span class="fa fa-users"></span> Client Profile</a>
                                                </li>

                                                {{--<li class="nav-item">--}}
                                                    {{--<a class="nav-link" href="{{ url('admin/manage-admin/clients') }}"><span class="fa fa-user-plus"></span> Manage Admin</a>--}}
                                                {{--</li>--}}

                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url('admin/clients/add') }}"><span class="fa fa-user-plus"></span> New Client</a>
                                                </li>
                                            </ul>
                                            <br />
                                            <table id="clients-dt" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Company Code</th>
                                                    <th>Company Name</th>
                                                    <th>Client ID Number</th>
                                                    <th>Client Status</th>
                                                    {{--<th>Total Billing Amount</th>--}}
                                                    {{--<th>Pending Billing Amount</th>--}}

                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#clients-dt').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url": "{{ url('api/datatables/clients') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "action" },
                    { "data": "company_code" },
                    { "data": "company_name" },
                    { "data": "client_id" },
                    { "data": "status" },
                    // { "data": "total_billing_amount" },
                    // { "data": "pending_billing_amount" },
                ],
                "order": [[ 1, "asc" ]],
                columnDefs: [ { orderable: false, targets: [0] }]

            });
        })
    </script>
@endsection

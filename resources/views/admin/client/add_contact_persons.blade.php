@extends('layouts.app')

@section('content')

            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <form action="" class="form" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="#" onclick="history.back()"><strong><i class="fa fa-arrow-left"></i> Back to Client Profile</strong></a>
                                            </div>

                                            <div class="col-md-12 text-center">
                                                <h1>Contact Persons</h1>
                                                <hr>
                                            </div>

                                            <div class="col-md-4" id="cp-firstName">
                                                <div class="form-group">
                                                    <label class="form-control-label">First Name</label>
                                                    <input type="text" class="form-control" name="">
                                                </div>
                                            </div>

                                            <div class="col-md-4" id="cp-middleName">
                                                <div class="form-group">
                                                    <label class="form-control-label">Middle Name</label>
                                                    <input type="text" class="form-control" name="">
                                                </div>
                                            </div>

                                            <div class="col-md-4" id="cp-lastName">
                                                <div class="form-group">
                                                    <label class="form-control-label">Last Name</label>
                                                    <input type="text" class="form-control" name="">
                                                </div>
                                            </div>

                                            <div class="col-md-4" id="cp-position">
                                                <div class="form-group">
                                                    <label class="form-control-label">Position</label>
                                                    <select name="" id="select-positions" class="form-control">
                                                        @foreach(\App\Positions::orderBy('title')->get() as $position)
                                                            <option value="{{ $position->id }}">{{ $position->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4" id="cp-email">
                                                <div class="form-group">
                                                    <label class="form-control-label">Email Address</label>
                                                    <input type="text" class="form-control" name="">
                                                </div>
                                            </div>

                                            <div class="col-md-12"></div>

                                            <div class="col-md-4" id="cp-mobile">
                                                <div class="form-group">
                                                    <label class="form-control-label">Mobile</label>
                                                    <input type="text" class="mask-cellphone form-control" name="" placeholder="mobile 1">
                                                </div>
                                            </div>

                                            <div class="col-md-4" id="cp-mobile">
                                                <div class="form-group">
                                                    <label class="form-control-label"></label>
                                                    <input type="text" class="mask-cellphone form-control" name="" placeholder="mobile 2" style="margin-top: 5px;">
                                                </div>
                                            </div>

                                            <div class="col-md-12"></div>

                                            <div class="col-md-4" id="cp-office">
                                                <div class="form-group">
                                                    <label class="form-control-label">Office Number</label>
                                                    <input type="text" class="mask-telephone form-control" name="">
                                                </div>
                                            </div>

                                            <div class="col-md-4" id="cp-local-number">
                                                <div class="form-group">
                                                    <label class="form-control-label">Local Number</label>
                                                    <input type="text" class="form-control" name="" oninput="this.value = this.value.replace(/[^0-9]/g, '')" maxlength="7">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <br>
                                                <a href="#" id="addContactPerson" class="btn btn-primary pull-right"><span class="fa fa-plus"></span> Add</a>
                                                <button type="reset" class="btn btn-warning pull-right" style="margin-right: 5px;"><span class="fa fa-refresh"></span> Reset</button>
                                            </div>
                                        </div>
                                    </form>



                                    <form action="" class="form" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <hr>

                                            <div class="col-md-12">
                                                <table class="table" id="tbl-contact-persons">
                                                    <thead>
                                                    <tr>
                                                        <th>First Name</th>
                                                        <th>Middle Name</th>
                                                        <th>Last Name</th>
                                                        <th>Position</th>
                                                        <th>Email</th>
                                                        <th>Mobile</th>
                                                        <th>Office Number</th>
                                                        <th>Local Number</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>

                                                    <tbody>
                                                    @if(session('client.contactPersons'))
                                                        @php $k = 0; @endphp
                                                        @foreach(session('client.contactPersons') as $cp)
                                                            <tr>
                                                                <td>{{ $cp['first_name'] }}</td>
                                                                <td>{{ $cp['middle_name'] }}</td>
                                                                <td>{{ $cp['last_name'] }}</td>
                                                                <td>{{ \App\Positions::find($cp['position_id'])->title }}</td>
                                                                <td>{{ $cp['email'] }}</td>
                                                                <td>{{ $cp['mobile1'] }} <br> {{ $cp['mobile2'] }}</td>
                                                                <td>{{ $cp['office_number'] }}</td>
                                                                <td>{{ $cp['local_number'] }}</td>
                                                                <td>
                                                                    <button class='btn btn-sm btn-danger removeContactPerson'><span class='fa fa-remove'></span> Remove</button>
                                                                    <input type='hidden' name='contact_persons[{{ $k }}][first_name]' value='{{ $cp['first_name'] }}' />
                                                                    <input type='hidden' name='contact_persons[{{ $k }}][middle_name]' value='{{ $cp['middle_name'] }}' />
                                                                    <input type='hidden' name='contact_persons[{{ $k }}][last_name]' value='{{ $cp['last_name'] }}' />
                                                                    <input type='hidden' name='contact_persons[{{ $k }}][position_id]' value='{{ $cp['position_id'] }}' />
                                                                    <input type='hidden' name='contact_persons[{{ $k }}][email]' value='{{ $cp['email'] }}' />
                                                                    <input type='hidden' name='contact_persons[{{ $k }}][mobile1]' value='{{ $cp['mobile1'] }}' />
                                                                    <input type='hidden' name='contact_persons[{{ $k }}][mobile2]' value='{{ $cp['mobile2'] }}' />
                                                                    <input type='hidden' name='contact_persons[{{ $k }}][office_number]' value='{{ $cp['office_number'] }}' />
                                                                    <input type='hidden' name='contact_persons[{{ $k }}][local_number]' value='{{ $cp['local_number'] }}' />
                                                                </td>
                                                            </tr>
                                                            @php $k++; @endphp
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>



                                            <div class="col-md-12 text-right">
                                                <br>
                                                <br>
                                                <br>

                                                <button href="#" class="btn btn-primary">Next <span class="fa fa-chevron-right" style="font-size: 0.8em;"></span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            var x = @if(session('client.contactPersons')) {{ count(session('client.contactPersons')) + 1 }} @else 0 @endif;

            $(document).on('click', "#addContactPerson", function(e) {
                e.preventDefault();
                var firstname = $("#cp-firstName input").val();
                var middlename = $("#cp-middleName input").val();
                var lastname = $("#cp-lastName input").val();
                var position = $("#cp-position").find(':selected');
                var email = $("#cp-email input").val();
                var mobile1 = $("#cp-mobile input").first().val();
                var mobile2 = $("#cp-mobile input").last().val();
                var office = $("#cp-office input").val();
                var localnumber = $("#cp-local-number input").val();
                var hasError = 0;

                if(firstname.trim().length === 0) {
                    $("#cp-firstName input").focus();
                    alert('First name is required');
                    return false;
                }

                if(lastname.trim().length === 0) {
                    $("#cp-lastName input").focus();
                    alert('Last name is required');
                    return false;
                }

                if(position.text().trim().length === 0) {
                    alert('Position is required');
                    return false;
                }

                if(email.trim().length === 0) {
                    $("#cp-email input").focus();
                    alert('Email is required');
                    return false;
                } else {
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if(re.test(email.trim()) === false) {
                        $("#cp-email input").focus();
                        alert('Email format invalid');
                        return false;
                    }
                }

                // if(office.length > 0 && /\([0-9]{2}\) [0-9]{3}-[0-9]{4}/.test(office) === false) {
                //     alert('Invalid Office Number format.');
                //     hasError++;
                // }
                //
                // if(localnumber.length > 0 && /\([0-9]{2}\) [0-9]{3}-[0-9]{4}/.test(localnumber) === false) {
                //     alert('Invalid Local Number format.');
                //     hasError++;
                // }


                if(hasError === 0) {
                    var template = "<tr>";
                    template = template + "<td>"+ firstname +"</td>";
                    template = template + "<td>"+ middlename +"</td>";
                    template = template + "<td>"+ lastname +"</td>";
                    template = template + "<td>"+ position.text() +"</td>";
                    template = template + "<td>"+ email +"</td>";
                    template = template + "<td>"+ mobile1 +"<br>"+ mobile2 +"</td>";
                    template = template + "<td>"+ office +"</td>";
                    template = template + "<td>"+ localnumber +"</td>";

                    template = template + "<td><button class='btn btn-sm btn-danger removeContactPerson'><span class='fa fa-remove'></span> Remove</button>";
                    template = template + "<input type='hidden' name='contact_persons["+ x +"][position_id]' value='"+ position.val() +"' />";
                    template = template + "<input type='hidden' name='contact_persons["+ x +"][first_name]' value='"+ firstname +"' />";
                    template = template + "<input type='hidden' name='contact_persons["+ x +"][middle_name]' value='"+ middlename +"' />";
                    template = template + "<input type='hidden' name='contact_persons["+ x +"][last_name]' value='"+ lastname +"' />";
                    template = template + "<input type='hidden' name='contact_persons["+ x +"][email]' value='"+ email +"' />";
                    template = template + "<input type='hidden' name='contact_persons["+ x +"][mobile1]' value='"+ mobile1 +"' />";
                    template = template + "<input type='hidden' name='contact_persons["+ x +"][mobile2]' value='"+ mobile2 +"' />";
                    template = template + "<input type='hidden' name='contact_persons["+ x +"][office_number]' value='"+ office +"' />";
                    template = template + "<input type='hidden' name='contact_persons["+ x +"][local_number]' value='"+ localnumber +"' />";
                    template = template + "</td></tr>";

//                console.log(template);

                    $("#tbl-contact-persons > tbody").append(template);

                    $("button[type=reset]").click();
                    x++;
                }

            });


            $(document).on('click', ".removeContactPerson", function(e) {
                e.preventDefault();
                $(this).closest('tr').remove();
            });



        })
    </script>
@endsection

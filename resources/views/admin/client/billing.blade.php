@extends('layouts.app')

@section('content')



                <ul class="breadcrumb">
                    <div class="container-fluid">
                        <div class="push-10"><a href="{{ url('admin/clients') }}"><i class="fa fa-chevron-left"></i> Go back to Clients</a></div>

                        <li class="breadcrumb-item active">Client Billing</li>
                    </div>
                </ul>
                <!-- Dashboard Counts Section-->
                <section class="dashboard-counts no-padding-bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h1><strong>{{ $client->company_name }}</strong></h1>
                                                <h4>{{ $client->company_code }}</h4>
                                            </div>

                                            <div class="col-md-12">
                                                <hr>
                                                <ul class="nav nav-tabs">
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/client/{$id}") }}">Client Information</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/client/{$id}/contact-persons") }}">Contact Persons</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/client/{$id}/cost-center") }}">Cost Center</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#">Billings</a>
                                                    </li>
                                                    {{--<li class="nav-item">--}}
                                                        {{--<a class="nav-link" href="{{ url("admin/client/{$id}/user-accounts") }}">User Accounts</a>--}}
                                                    {{--</li>--}}
                                                </ul>
                                            </div>

                                            <div class="col-md-12">
                                                <br />
                                                <form action="" method="POST">
                                                    {{ csrf_field() }}

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Admin Fee Type</label>
                                                            <select id="adminFeeType" name="admin_fee_type" class="form-control">
                                                                <option value="Percentage" @if($client->admin_fee_type == 'Percentage') selected @endif>Percentage</option>
                                                                <option value="Fixed" @if($client->admin_fee_type == 'Fixed') selected @endif>Fixed</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Admin Fee %</label>
                                                            <input id="adminFeePercentage" type="text" class="form-control" name="admin_fee_percentage" value="@if($client->admin_fee_type == 'Percentage'){{ $client->admin_fee_percentage }}@endif" placeholder="" @if($client->admin_fee_type == 'Fixed') readonly @else required @endif oninput="this.value = this.value.replace(/[^0-9.]/g, '')" />
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Admin Fee</label>
                                                            <input id="adminFee" type="text" class="form-control" name="admin_fee" value="@if(!empty($client->admin_fee)){{ number_format($client->admin_fee, 2) }}@endif" placeholder="" @if($client->admin_fee_type == 'Percentage') readonly @else required  @endif oninput="this.value = this.value.replace(/[^0-9.]/g, '')" />
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <br />
                                                        <button class="btn btn-success"><i class="fa fa-check"></i> SAVE</button>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="col-md-6">
                                                <br>

                                                <hr>

                                                <h3>Admin Breakdown</h3>

                                                <div>
                                                    <a href="#" id="editAdminBreakdown" class="btn btn-primary btn-sm pull-right"><span class="fa fa-plus"></span> Edit</a>
                                                    <br>
                                                    <br>
                                                </div>


                                                <div>
                                                    @if(!empty($client->admin_breakdown))
                                                    <table class="table">
                                                        <tbody>
                                                        @foreach(explode(';', $client->admin_breakdown) as $bd)
                                                            <tr>
                                                                <td><strong>{{ $bd }}</strong></td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                    @else
                                                    <p>No selected Admin Breakdown yet.</p>
                                                    @endif
                                                </div>
                                            </div>



                                            @if($me->role != 'AdminHR')
                                            <div class="col-md-12">

                                                <hr>

                                                <h3>Billing History</h3>
                                                <br>

                                                <table id="clients-dt" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Company Code</th>
                                                            <th>Billing Date</th>
                                                            <th>Invoice</th>
                                                            <th>Billing Amount</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($client->Invoices->count() > 0)
                                                    @foreach($client->Invoices as $invoice)
                                                        <tr>
                                                            <td>{{ $client->company_code }}</td>
                                                            <td>{{ date('m/d/Y', strtotime($invoice->invoice_date)) }}</td>
                                                            <td>{{ $invoice->invoice_number }}</td>
                                                            <td>{{ number_format($invoice->total_amount, 2) }}</td>
                                                            <td>{{ $invoice->status }}</td>
                                                        </tr>
                                                    @endforeach
                                                    @else
                                                        <tr>
                                                            <td>No Billing invoice yet.</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    @endif
                                                    </tbody>

                                                </table>
                                            </div>
                                            @endif



                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection


@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#clients-dt').DataTable({
                "pageLength": 50,
                "order": [[ 2, "desc" ]]
            });

            $("#adminFeeType").change(function() {
                if($(this).find(":selected").val() == 'Percentage') {
                    $("#adminFee").val('').prop('readonly', true).prop('required', false);
                    $("#adminFeePercentage").val('').prop('readonly', false).prop('required', true);
                } else {
                    $("#adminFee").val('').prop('readonly', false).prop('required', true);
                    $("#adminFeePercentage").val('').prop('readonly', true).prop('required', false);
                }
            });

            $(document).on('keyup change', "#adminFeePercentage", function() {
                var percentage = $(this).val();
                adminFee = '';
                if(percentage.length > 0) {
                    var adminFee = parseFloat(percentage) / 100;
                }

                $("#adminFee").val(adminFee);
            });

            $(document).on('click', '#editAdminBreakdown', function(e) {
                e.preventDefault();
                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load("{{ url("modal/admin-breakdown/$id") }}", function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $('#mt').multiSelect();
                        $("#ajaxPopup").modal('show');
                    }
                });
            });
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')

                <ul class="breadcrumb">
                    <div class="container-fluid">
                        <div class="push-10"><a href="{{ url('admin/clients') }}"><i class="fa fa-chevron-left"></i> Go back to Clients</a></div>

                        <li class="breadcrumb-item">Client</li>
                        <li class="breadcrumb-item active">Profile</li>
                    </div>
                </ul>
                <!-- Dashboard Counts Section-->
                <section class="dashboard-counts no-padding-bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h1><strong>{{ $client->company_name }}</strong></h1>
                                                <h4>{{ $client->company_code }}</h4>
                                            </div>

                                            <div class="col-md-12">
                                                <hr>
                                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#">Client Information</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/client/{$id}/contact-persons") }}">Contact Persons</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/client/{$id}/cost-center") }}">Cost Center</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/client/{$id}/billings") }}">Billings</a>
                                                    </li>
                                                    {{--<li class="nav-item">--}}
                                                        {{--<a class="nav-link" href="{{ url("admin/client/{$id}/user-accounts") }}">User Accounts</a>--}}
                                                    {{--</li>--}}
                                                </ul>
                                            </div>

                                            <div class="col-md-9">

                                                <div style="margin: 10px 0;">
                                                    <a href="{{ url("admin/client/{$client->id}/edit") }}" class="btn btn-primary pull-right"><span class="fa fa-edit"></span> Edit</a>
                                                    @if($client->status == 'Active')
                                                        @if($client->client_id != 'C1')
                                                            <a href="#" class="btn btn-danger pull-right" data-toggle="modal" data-target="#deleteClientModal" style="margin-right: 5px;"><span class="fa fa-remove"></span> Deactivate</a>
                                                        @endif
                                                    @else
                                                        <a href="#" class="btn btn-success pull-right" data-toggle="modal" data-target="#deleteClientModal" style="margin-right: 5px;"><span class="fa fa-check"></span> Activate</a>
                                                    @endif
                                                    <br>
                                                    <br>
                                                </div>

                                                <table class="table" style="max-width: 600px;">
                                                    <tr>
                                                        <td width="150">Client ID</td>
                                                        <td>{{ $client->client_id }}</td>
                                                        <td>Company Code</td>
                                                        <td>{{ $client->company_code }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="150">Client Status</td>
                                                        <td colspan="3" class="@if($client->status == 'Active') text-success @else text-danger @endif">{{ $client->status }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="150">Company Name</td>
                                                        <td colspan="3">{{ $client->company_name }}</td>
                                                    </tr>
                                                    @if(!empty($client->parent_company_id))
                                                    <tr>
                                                        <td width="150">Parent Company</td>
                                                        <td colspan="3">{{ $client->ParentCompany->company_code }}</td>
                                                    </tr>
                                                    @endif
                                                    <tr>
                                                        <td width="150">Company Address</td>
                                                        <td colspan="3">{{ $client->address }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="150">Trunk Line</td>
                                                        <td>{{ $client->trunk_line }}</td>
                                                        <td>Fax Number</td>
                                                        <td>{{ $client->fax }}</td>
                                                    </tr>
                                                    {{--<tr>--}}
                                                    {{--<td width="150">SSS</td>--}}
                                                    {{--<td colspan="3">{{ $client->sss }}</td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                    {{--<td width="150">HDMF</td>--}}
                                                    {{--<td colspan="3">{{ $client->hdmf }}</td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                    {{--<td>PhilHealth</td>--}}
                                                    {{--<td colspan="3">{{ $client->philhealth }}</td>--}}
                                                    {{--</tr>--}}
                                                    <tr>
                                                        <td>TIN</td>
                                                        <td colspan="3">{{ $client->tin }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="150">Effectivity Date</td>
                                                        <td colspan="3">{{ date('m/d/Y', strtotime($client->client_start_date)) }}</td>
                                                    </tr>

                                                </table>
                                            </div>

                                            <?php /*
                                            <div class="col-md-3">
                                                <div class="" style="padding: 5px;">
                                                    @if(empty($client->logo_path))
                                                        <img src="{{ url("img/logo-placeholder.jpg") }}" alt="" style="max-width: 100%;">
                                                    @else
                                                        <img src="{{ url($client->logo_path) }}" alt="" style="max-width: 100%;">
                                                    @endif
                                                </div>
                                                <div class="text-center push-5-t">
                                                    <button id="upload-photo" class="btn btn-default"><i class="fa fa-upload"></i> Choose File</button>
                                                </div>

                                                <form class="hidden" action="{{ url("admin/client/{$id}/profile/upload-logo") }}" method="POST" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="file" id="file-upload-photo" name="logo" accept=".jpg, .jpeg, .png" />
                                                </form>
                                            </div>
                                            */ ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @if($client->status == 'Active')
        <div id="deleteClientModal" tabindex="-1" role="dialog" aria-labelledby="deleteClientModal" class="modal fade text-left" aria-hidden="true" style="display: none;">
            <div role="document" class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="exampleModalLabel" class="modal-title">Deactivate Client</h4>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to deactivate this client?</p>
                    </div>
                    <div class="modal-footer">
                        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                        <a href="{{ url("admin/client/{$client->id}/deactivate") }}" id="" type="button" class="btn btn-primary">Confirm</a>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div id="deleteClientModal" tabindex="-1" role="dialog" aria-labelledby="deleteClientModal" class="modal fade text-left" aria-hidden="true" style="display: none;">
            <div role="document" class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="exampleModalLabel" class="modal-title">Activate Client</h4>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to activate this client?</p>
                    </div>
                    <div class="modal-footer">
                        <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                        <a href="{{ url("admin/client/{$client->id}/activate") }}" id="" type="button" class="btn btn-primary">Confirm</a>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(document).on('click', "#upload-photo", function(e) {
                e.preventDefault();
//                console.log('clicked!');
                $("#file-upload-photo").trigger('click');
            })

            $(document).on('change', "#file-upload-photo", function () {
                $(this).closest('form').submit();
            });
        })
    </script>
@endsection

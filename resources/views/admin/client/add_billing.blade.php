@extends('layouts.app')

@section('content')


            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <form action="" class="form" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">

                                            <div class="col-md-12">
                                                <a href="#" onclick="history.back()"><strong><i class="fa fa-arrow-left"></i> Back to Client Cost Center</strong></a>
                                            </div>

                                            <div class="col-md-12 text-center">
                                                <h1>Admin Fee</h1>
                                                <hr>
                                            </div>

                                            <div class="col-md-4" id="cp-firstName">
                                                <div class="form-group">
                                                    <label class="form-control-label">Admin Fee Type</label>
                                                    <select name="admin_fee_type" id="adminFeeType" class="form-control">
                                                        <option value="Percentage">Percentage</option>
                                                        <option value="Fixed">Fixed</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-control-label">Admin Fee %</label>
                                                    <input id="adminFeePercentage" type="text" name="admin_fee_percentage" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, '')" required>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-control-label">Admin Fee</label>
                                                    <input id="adminFee" type="text" class="form-control" name="admin_fee" oninput="this.value = this.value.replace(/[^0-9.]/g, '')" readonly>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-md-offset-1">
                                                <div class="form-group">
                                                    <label for="disabledSelect">Admin Breakdown list</label>
                                                    <?php
                                                    $list = [
                                                        'BASIC',
                                                        'RDOT',
                                                        'SPE',
                                                        'SPERD',
                                                        'LEG',
                                                        'LEGRD',
                                                        'OT',
                                                        'ND',
                                                        'SIL',
                                                        'ECOLA',
                                                        'HECOLA',
                                                        '13TH MONTH',
                                                        'INCENTIVES',
                                                        'SSS ER',
                                                        'SSS EC',
                                                        'HDMF ER',
                                                        'PhilHealth ER'
                                                    ];
                                                    ?>
                                                    <select id="mt" class="form-control" name="admin_breakdown[]" multiple='multiple'>
                                                        @foreach($list as $l)
                                                            <option value="{{ $l }}">{{ $l }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>



                                            <div class="col-md-12 text-right">
                                                <br>
                                                <br>
                                                <br>

                                                <button id="confirmClientRegistration" class="btn btn-primary">Complete Registration <span class="fa fa-save" style="font-size: 0.8em;"></span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

            $("#adminFeeType").change(function() {
                if($(this).find(":selected").val() == 'Percentage') {
                    $("#adminFee").val('').prop({'readonly': true, 'required' : false});
                    $("#adminFeePercentage").val('').prop({'readonly' : false,'required' : true});
                } else {
                    $("#adminFee").val('').prop({'readonly': false, 'required' : true});
                    $("#adminFeePercentage").val('').prop({'readonly' : true,'required' : false});
                }
            });

            $(document).on('keyup change', "#adminFeePercentage", function() {
                var percentage = $(this).val();
                adminFee = '';
                if(percentage.length > 0) {
                    var adminFee = parseFloat(percentage) / 100;
                }

                $("#adminFee").val(adminFee);
            });

            $("#confirmClientRegistration").click(function(e) {
                if($("#adminFeeType").find(":selected").val() === 'Percentage') {
                    var adminFee = $("#adminFeePercentage").val() + '%';
                } else {
                    var adminFee = 'Php' + $("#adminFee").val();
                }

                var msg = 'Are you sure you want to proceed?\n\n' +
                    `Company Code: {{ !empty(session('client.profile')['parent_company_id']) ? \App\Companies::find(session('client.profile')['parent_company_id'])->company_code . '-' . session('client.profile')['company_code'] : session('client.profile')['company_code'] }} \n` +
                    `Company Name: {{ session('client.profile')['company_name'] }} \n` +
                    `Effectivity Date: {{ session('client.profile')['client_start_date'] }} \n` +
                    'Admin Fee Type: ' + $("#adminFeeType").find(":selected").val() + '\n' +
                    'Admin Fee: ' + adminFee
                    ;
                if(confirm(msg) === false) {
                    e.preventDefault();
                }
            });

            $('#mt').multiSelect();
        });
    </script>
@endsection

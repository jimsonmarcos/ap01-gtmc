@extends('layouts.app')

@section('content')

            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <form action="" class="form" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="#" onclick="history.back()"><strong><i class="fa fa-arrow-left"></i> Back to Client Contact Persons</strong></a>
                                            </div>

                                            <div class="col-md-12 text-center">
                                                <h1>Cost Center</h1>
                                                <hr>
                                            </div>



                                            <div class="col-md-12">
                                                <h3>Payroll Groups</h3>

                                                <a href="#" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#payrollGroupModal"><span class="fa fa-plus"></span> Add Payroll Group</a>
                                                <br>
                                                <br>

                                                <table class="table" id="tbl-payroll-groups">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Payroll Group</th>
                                                            <th class="text-center">Start Date A</th>
                                                            <th class="text-center">Payroll Date A</th>
                                                            <th class="text-center">Start Date B</th>
                                                            <th class="text-center">Payroll Date B</th>
                                                            <th style="width: 30%">PG Def.</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(session('client.payrollGroups'))
                                                        @php $k = 0; @endphp
                                                        @foreach(session('client.payrollGroups') as $pg)
                                                            <td>{{ $pg['group_name'] }}</td>
                                                            <td>{{ $pg['start_date_a'] }}</td>
                                                            <td>{{ $pg['payroll_date_a'] }}</td>
                                                            <td>{{ $pg['start_date_b'] }}</td>
                                                            <td>{{ $pg['payroll_date_b'] }}</td>
                                                            <td>{{ $pg['group_definition'] }}</td>
                                                            <td>
                                                                <button class='btn btn-sm btn-danger removePayrollGroup'><span class='fa fa-remove'></span> Remove</button>
                                                                <input type='hidden' name='payroll_groups[{{ $k }}][group_name]' value="{{ $pg['group_name'] }}" />
                                                                <input type='hidden' name='payroll_groups[{{ $k }}][group_definition]' value="{{ $pg['group_definition'] }}" />
                                                                <input type='hidden' name='payroll_groups[{{ $k }}][start_date_a]' value="{{ $pg['start_date_a'] }}" />
                                                                <input type='hidden' name='payroll_groups[{{ $k }}][payroll_date_a]' value="{{ $pg['payroll_date_a'] }}" />
                                                                <input type='hidden' name='payroll_groups[{{ $k }}][start_date_b]' value="{{ $pg['start_date_b'] }}" />
                                                                <input type='hidden' name='payroll_groups[{{ $k }}][payroll_date_b]' value="{{ $pg['payroll_date_b'] }}" />
                                                            </td>
                                                            @php $k++; @endphp
                                                        @endforeach
                                                    @else
                                                        @if(!empty(session('client.profile')['parent_company_id']))
                                                            @php $k = 0; @endphp
                                                            @foreach(\App\PayrollGroups::where('company_id')->get() as $pg)
                                                                <td>{{ $pg['group_name'] }}</td>
                                                                <td>{{ $pg['start_date_a'] }}</td>
                                                                <td>{{ $pg['payroll_date_a'] }}</td>
                                                                <td>{{ $pg['start_date_b'] }}</td>
                                                                <td>{{ $pg['payroll_date_b'] }}</td>
                                                                <td>{{ $pg['group_definition'] }}</td>
                                                                <td>
                                                                    <button class='btn btn-sm btn-danger removePayrollGroup'><span class='fa fa-remove'></span> Remove</button>
                                                                    <input type='hidden' name='payroll_groups[{{ $k }}][group_name]' value="{{ $pg['group_name'] }}" />
                                                                    <input type='hidden' name='payroll_groups[{{ $k }}][group_definition]' value="{{ $pg['group_definition'] }}" />
                                                                    <input type='hidden' name='payroll_groups[{{ $k }}][start_date_a]' value="{{ $pg['start_date_a'] }}" />
                                                                    <input type='hidden' name='payroll_groups[{{ $k }}][payroll_date_a]' value="{{ $pg['payroll_date_a'] }}" />
                                                                    <input type='hidden' name='payroll_groups[{{ $k }}][start_date_b]' value="{{ $pg['start_date_b'] }}" />
                                                                    <input type='hidden' name='payroll_groups[{{ $k }}][payroll_date_b]' value="{{ $pg['payroll_date_b'] }}" />
                                                                </td>
                                                            @endforeach
                                                            @php $k++; @endphp
                                                        @endif
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>



                                            <div class="col-md-12">
                                                <hr>
                                                <h3>Cost Center</h3>
                                                <br>
                                            </div>

                                                <div class="col-md-5">
                                                    <div class="form-horizontal">
                                                        <div class="form-group row" style="padding: 10px 15px;">
                                                            <label class="col-sm-4 form-control-label">Cost Center</label>
                                                            <div class="col-sm-8">
                                                                <input id="cc-cost-center" type="text" placeholder="" class="form-control" oninput="this.value = this.value.toUpperCase().replace(/[^A-Z_-]/g, '').replace(/(\..*)\./g, '$1');">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" style="padding: 10px 15px;">
                                                            <label class="col-sm-4 form-control-label">Group Definition</label>
                                                            <div class="col-sm-8">
                                                                <input id="cc-group-def" type="text" placeholder="" class="form-control">
                                                            </div>
                                                        </div>

                                                        <div class="">
                                                            <a href="#" id="btnAddCostCenter" class="btn btn-primary btn-sm"><span class="fa fa-plus"></span> Add Cost Center</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-7">
                                                    <table class="table" id="tbl-cost-center">
                                                        <thead>
                                                        <tr>
                                                            <th>Cost Center</th>
                                                            <th>Definition</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>

                                                        <tbody>
                                                            @if(session('client.costCenters'))
                                                                @php $k1 = 0; @endphp
                                                                @foreach(session('client.costCenters') as $cc)
                                                                    <tr>
                                                                        <td>{{ $cc['cost_center'] }}</td>
                                                                        <td>{{ $cc['group_definition'] }}</td>
                                                                        <td>
                                                                            <button class='btn btn-sm btn-danger removeCostCenter'><span class='fa fa-remove'></span> Remove</button>
                                                                            <input type='hidden' name='cost_centers[{{ $k1 }}][cost_center]' value="{{ $cc['cost_center'] }}" />
                                                                            <input type='hidden' name='cost_centers[{{ $k1 }}][group_definition]' value="{{ $cc['group_definition'] }}" />
                                                                        </td>
                                                                    </tr>
                                                                    @php $k1++; @endphp
                                                                @endforeach
                                                            @endif
                                                        </tbody>

                                                    </table>
                                                </div>



                                            <div class="col-md-12 text-right">
                                                <br>
                                                <br>
                                                <br>

                                                <button href="#" class="btn btn-primary">Next <span class="fa fa-chevron-right" style="font-size: 0.8em;"></span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div id="payrollGroupModal" tabindex="-1" role="dialog" aria-labelledby="payrollGroupModal" class="modal fade text-left" aria-hidden="true" style="display: none;">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="exampleModalLabel" class="modal-title">Add Payroll Group</h4>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form>

                    <div class="form-horizontal">
                        <div class="form-group row" style="padding: 10px 15px;">
                            <label class="col-sm-4 form-control-label">Payroll Group</label>
                            <div class="col-sm-8">
                                <input id="pg-payroll-group" type="text" placeholder="" class="form-control" oninput="this.value = this.value.toUpperCase().replace(/[^0-9A-Z_-]/g, '').replace(/(\..*)\./g, '$1');">
                            </div>
                        </div>
                        <div class="form-group row" style="padding: 10px 15px;">
                            <label class="col-sm-4 form-control-label">Group Definition</label>
                            <div class="col-sm-8">
                                <input id="pg-payroll-def" type="text" placeholder="" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <table class="table" style="max-width: 100%;">
                                <tr>
                                    <th></th>
                                    <th>Start Date</th>
                                    <th>Payroll Date</th>
                                </tr>

                                <tr>
                                    <th>A</th>
                                    <td>
                                        <select name="" id="pg-start-a" class="form-control" style="width: 75px;">
                                            @for($x = 1; $x <= 31; $x++)
                                                <option value="{{ $x }}">{{ $x }}</option>
                                            @endfor
                                        </select>
                                    <td>
                                        <select name="payroll_date_a" id="pg-payroll-a" class="form-control" style="width: 75px;">
                                            @for($x = 1; $x <= 31; $x++)
                                                <option value="{{ $x }}">{{ $x }}</option>
                                            @endfor
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <th>B</th>
                                    <td>
                                        <select name="" id="pg-start-b" class="form-control" style="width: 75px;">
                                            @for($x = 1; $x <= 31; $x++)
                                                <option value="{{ $x }}">{{ $x }}</option>
                                            @endfor
                                        </select>
                                    </td>
                                    <td>
                                        <select name="payroll_date_b" id="pg-payroll-b" class="form-control" style="width: 75px;">
                                            @for($x = 1; $x <= 31; $x++)
                                                <option value="{{ $x }}">{{ $x }}</option>
                                            @endfor
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="form-group">
                        <button id="btnResetPG" type="reset" class="btn btn-warning"><span class="fa fa-refresh"></span> Reset</button>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button id="btnCloseModalPG" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                <button id="btnAddPayrollGroup" type="button" class="btn btn-primary">Confirm</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            var x = @if(!empty($k)) {{ $k + 1 }} @else 0 @endif;

            $(document).on('click', "#btnAddPayrollGroup", function(e) {
                e.preventDefault();
                var payrollgroup = $("#pg-payroll-group").val().trim();
                var payrolldef = $("#pg-payroll-def").val();
                var startdate_a = $("#pg-start-a").find(':selected').val();
                var startdate_b = $("#pg-start-b").val();
                var payrolldate_a = $("#pg-payroll-a").find(':selected').val();
                var payrolldate_b = $("#pg-payroll-b").find(':selected').val();

                if(payrollgroup.length == 0) {
                    alert('Payroll Group must not be empty.');
                    $("#pg-payroll-group").focus();
                    return false;
                }


                var template = "<tr>";
                template = template + "<td>"+ payrollgroup +"</td>";
                template = template + "<td>"+ startdate_a +"</td>";
                template = template + "<td>"+ payrolldate_a +"</td>";
                template = template + "<td>"+ startdate_b +"</td>";
                template = template + "<td>"+ payrolldate_b +"</td>";
                template = template + "<td>"+ payrolldef +"</td>";

                template = template + "<td><button class='btn btn-sm btn-danger removePayrollGroup'><span class='fa fa-remove'></span> Remove</button>";
                template = template + "<input type='hidden' name='payroll_groups["+ x +"][group_name]' value='"+ payrollgroup +"' />";
                template = template + "<input type='hidden' name='payroll_groups["+ x +"][group_definition]' value='"+ payrolldef +"' />";
                template = template + "<input type='hidden' name='payroll_groups["+ x +"][start_date_a]' value='"+ startdate_a +"' />";
                template = template + "<input type='hidden' name='payroll_groups["+ x +"][payroll_date_a]' value='"+ payrolldate_a +"' />";
                template = template + "<input type='hidden' name='payroll_groups["+ x +"][start_date_b]' value='"+ startdate_b +"' />";
                template = template + "<input type='hidden' name='payroll_groups["+ x +"][payroll_date_b]' value='"+ payrolldate_b +"' />";
                template = template + "</td></tr>";

                $("#tbl-payroll-groups > tbody").append(template);

                $("#btnResetPG").click();
                $("#btnCloseModalPG").click();
                x++;
            });


            $(document).on('click', ".removePayrollGroup", function(e) {
                e.preventDefault();
                $(this).closest('tr').remove();
            });

            var y = @if(!empty($k1)) {{ $k1 + 1 }} @else 0 @endif;
            $(document).on('click', "#btnAddCostCenter", function(e) {
                e.preventDefault();
                var costcenter = $("#cc-cost-center").val();
                var groupdef = $("#cc-group-def").val();

                if(costcenter.trim().length === 0) {
                    alert('Cost center must not be empty');
                    return false;
                }


                var template = "<tr>";
                template = template + "<td>"+ costcenter +"</td>";
                template = template + "<td>"+ groupdef +"</td>";

                template = template + "<td><button class='btn btn-sm btn-danger removeCostCenter'><span class='fa fa-remove'></span> Remove</button>";
                template = template + "<input type='hidden' name='cost_centers["+ x +"][cost_center]' value='"+ costcenter +"' />";
                template = template + "<input type='hidden' name='cost_centers["+ x +"][group_definition]' value='"+ groupdef +"' />";
                template = template + "</td></tr>";

                $("#tbl-cost-center > tbody").append(template);

                $("#cc-cost-center").val('');
                $("#cc-group-def").val('');
                y++;
            });


            $(document).on('click', ".removeCostCenter", function(e) {
                e.preventDefault();
                $(this).closest('tr').remove();
            });
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')


                <ul class="breadcrumb">
                    <div class="container-fluid">
                        <div class="push-10"><a href="{{ url('admin/clients') }}"><i class="fa fa-chevron-left"></i> Go back to Clients</a></div>

                        <li class="breadcrumb-item">Client</li>
                        <li class="breadcrumb-item active">Cost Center</li>
                    </div>
                </ul>
                <!-- Dashboard Counts Section-->
                <section class="dashboard-counts no-padding-bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h1><strong>{{ $client->company_name }}</strong></h1>
                                                <h4>{{ $client->company_code }}</h4>
                                            </div>

                                            <div class="col-md-12">
                                                <hr>
                                                <ul class="nav nav-tabs">
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/client/{$id}") }}">Client Information</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/client/{$id}/contact-persons") }}">Contact Persons</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#">Cost Center</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/client/{$id}/billings") }}">Billings</a>
                                                    </li>
                                                    {{--<li class="nav-item">--}}
                                                        {{--<a class="nav-link" href="{{ url("admin/client/{$id}/user-accounts") }}">User Accounts</a>--}}
                                                    {{--</li>--}}
                                                </ul>
                                            </div>

                                            <div class="col-md-12">
                                                <br>

                                                @if(empty($client->parent_company_id))
                                                <h2>Payroll Groups</h2>
                                                <div>
                                                    <a href="#" id="addPayrollGroup" class="btn btn-primary btn-sm pull-right"><span class="fa fa-plus"></span> Add Payroll Group</a>
                                                    <br>
                                                    <br>
                                                </div>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Payroll Group</th>
                                                            <th>Start Date A</th>
                                                            <th>Payroll Date A</th>
                                                            <th>Start Date B</th>
                                                            <th>Payroll Date B</th>
                                                            <th>Payroll Group Def.</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if($payrollGroups->count() > 0)
                                                            @foreach($payrollGroups as $payrollGroup)
                                                                <tr>
                                                                    <td>{{ strtoupper($payrollGroup->group_name) }}</td>
                                                                    <td>{{ $payrollGroup->start_date_a }}</td>
                                                                    <td>{{ $payrollGroup->payroll_date_a }}</td>
                                                                    <td>{{ $payrollGroup->start_date_b }}</td>
                                                                    <td>{{ $payrollGroup->payroll_date_b }}</td>
                                                                    <td>{{ $payrollGroup->group_definition }}</td>
                                                                    <td width="100">
                                                                        <a href="#" class="btn btn-sm btn-primary editPayrollGroup" data-id="{{ $payrollGroup->id }}"><span class="fa fa-edit"></span></a>
                                                                        <a href="#" class="btn btn-sm btn-danger deletePayrollGroup" data-id="{{ $payrollGroup->id }}"><span class="fa fa-remove"></span></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @else
                                                            <tr>
                                                                <td colspan="6">No Payroll Groups yet.</td>
                                                            </tr>
                                                        @endif
                                                    </tbody>
                                                </table>

                                                <br>
                                                <hr>
                                                <br>

                                                @else

                                                <h2>Payroll Groups</h2>

                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>Payroll Group</th>
                                                        <th>Start Date</th>
                                                        <th>Payroll Date A</th>
                                                        <th>Payroll Date B</th>
                                                        <th>Payroll Group Def.</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($client->ParentCompany->PayrollGroups->count() > 0)
                                                        @foreach($client->ParentCompany->PayrollGroups as $payrollGroup)
                                                            <tr>
                                                                <td>{{ strtoupper($payrollGroup->group_name) }}</td>
                                                                <td>{{ $payrollGroup->start_date_a }}</td>
                                                                <td>{{ $payrollGroup->payroll_date_a }}</td>
                                                                <td>{{ $payrollGroup->payroll_date_b }}</td>
                                                                <td>{{ $payrollGroup->group_definition }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="6">No Payroll Groups yet.</td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>

                                                @endif

                                                <h2>Cost Center</h2>
                                                <div style="max-width: 600px;">
                                                    <a href="#" id="addCostCenter" class="btn btn-primary btn-sm pull-right"><span class="fa fa-plus"></span> Add Cost Center</a>
                                                    <br>
                                                    <br>
                                                </div>
                                                <table class="table" style="max-width: 600px;">
                                                    <thead>
                                                    <tr>
                                                        <th width="150">Cost Center</th>
                                                        <th>Group Definition</th>
                                                        <th width="200"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($costCenters->count() > 0)
                                                        @foreach($costCenters as $costCenter)
                                                            <tr>
                                                                <td>{{ strtoupper($costCenter->cost_center) }}</td>
                                                                <td>{{ $costCenter->group_definition }}</td>
                                                                <td>
                                                                    <a href="#" class="btn btn-sm btn-primary editCostCenter" data-id="{{ $costCenter->id }}"><span class="fa fa-edit"></span> </a>
                                                                    <a href="#" class="btn btn-sm btn-danger deleteCostCenter" data-id="{{ $costCenter->id }}"><span class="fa fa-remove"></span> </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="6">No Cost Centers yet.</td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div id="deletePayrollGroup" tabindex="-1" role="dialog" aria-labelledby="" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete Payroll Group</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Payroll Group?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>

    <div id="deleteCostCenter" tabindex="-1" role="dialog" aria-labelledby="" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete Cost Center</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Cost Center?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            // Payroll Group Modals
            $(document).on('click', '#addPayrollGroup', function(e) {
                e.preventDefault();
                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load("{{ url("modal/payroll-group-add/$id") }}", function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');
                    }
                });
            });

            $(document).on('click', '.editPayrollGroup', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/payroll-group-edit/{$id}") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');
                    }
                });
            });

            $(".deletePayrollGroup").click(function(e) {
                e.preventDefault();

                var url = "{{ url("admin/client/{$id}/payroll-group/delete") }}/";

                $("#deletePayrollGroup").find('#url').attr('href', url + $(this).attr('data-id'));
                $("#deletePayrollGroup").modal('show');
            });

            // Cost Center Modals
            $(document).on('click', '#addCostCenter', function(e) {
                e.preventDefault();
                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load("{{ url("modal/cost-center-add/$id") }}", function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');
                    }
                });
            });

            $(document).on('click', '.editCostCenter', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/cost-center-edit/{$id}") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');
                    }
                });
            });

            $(".deleteCostCenter").click(function(e) {
                e.preventDefault();

                var url = "{{ url("admin/client/{$id}/cost-center/delete") }}/";

                $("#deleteCostCenter").find('#url').attr('href', url + $(this).attr('data-id'));
                $("#deleteCostCenter").modal('show');
            });
        });
    </script>
@endsection

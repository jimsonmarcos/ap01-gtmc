@extends('layouts.app')

@section('content')

                <ul class="breadcrumb">
                    <div class="container-fluid">
                        <li class="breadcrumb-item">Client Profile</li>
                        <li class="breadcrumb-item active">Details</li>
                    </div>
                </ul>
                <!-- Dashboard Counts Section-->
                <section class="dashboard-counts no-padding-bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="" method="POST" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <h1>Edit Client Profile</h1>
                                                    <hr />
                                                </div>

                                                <div class="col-md-12">
                                                    <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                                        <li class="nav-item">
                                                            <a class="nav-link active" href="#">Client Information</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="{{ url("admin/client/{$id}/contact-persons") }}">Contact Persons</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="{{ url("admin/client/{$id}/cost-center") }}">Cost Center</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="{{ url("admin/client/{$id}/billings") }}">Billings</a>
                                                        </li>
                                                    </ul>

                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="ddclient_information" role="tabpanel">
                                                            <div class="row">
                                                                <br />
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Client ID</label>
                                                                        <input type="text" name="client_id" class="form-control" value="{{ $client->client_id }}" />
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Company Code</label>
                                                                        <input type="text" name="company_code" class="form-control" value="{{ $client->company_code }}" oninput="this.value = this.value.toUpperCase().trim()" maxlength="15" />
                                                                    </div>
                                                                </div>

                                                                <div id="parentCompany" class="col-md-4" @if(empty($client->parent_company_id)) style="display: none;" @endif>
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Parent Company</label>
                                                                        <select name="parent_company_id" id=""
                                                                                class="form-control">
                                                                            <option value=""></option>
                                                                            @foreach(\App\Companies::where('id', '!=', $client->id)->where('status', 'Active')->orderBy('company_code')->get() as $company)
                                                                                <option value="{{ $company->id }}" @if($client->parent_company_id == $company->id) selected @endif>{{ $company->company_code }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <div>
                                                                            <input id="hasParentCompany" type="checkbox" value="" name="" @if(!empty($client->parent_company_id)) checked @endif>
                                                                            <label for="hasParentCompany">Connect to Parent Company</label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Company Name</label>
                                                                        <input type="text" name="company_name" class="form-control" value="{{ $client->company_name }}" />
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Company Address</label>
                                                                        <textarea name="address" id="" rows="3"
                                                                                  class="form-control">{{ $client->company_address }}</textarea>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Trunk Link</label>
                                                                        <input type="text" name="trunk_line" class="mask-telephone form-control" value="{{ $client->trunk_line }}" />
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Fax Number</label>
                                                                        <input type="text" name="fax" class="mask-telephone form-control" value="{{ $client->fax }}" />
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12"></div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">TIN</label>
                                                                        <input type="text" name="tin" class="form-control" value="{{ $client->tin }}" />
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Effectivity Date</label>
                                                                        <div class='input-group btn-mmddyyyy'>
                                                                            <input type='text' class="form-control" name="client_start_date" value="{{ date('m/d/Y', strtotime($client->client_start_date)) }}" placeholder="mm/dd/yyyy" required />
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                {{--<div class="col-md-3">--}}
                                                                    {{--<div class="form-group">--}}
                                                                        {{--<label class="form-control-label">SSS</label>--}}
                                                                        {{--<input type="text" name="sss" class="form-control" value="{{ $client->sss }}" />--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}

                                                                {{--<div class="col-md-3">--}}
                                                                    {{--<div class="form-group">--}}
                                                                        {{--<label class="form-control-label">HDMF</label>--}}
                                                                        {{--<input type="text" name="hdmf" class="form-control" value="{{ $client->hdmf }}" />--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}

                                                                {{--<div class="col-md-3">--}}
                                                                    {{--<div class="form-group">--}}
                                                                        {{--<label class="form-control-label">PhilHealth</label>--}}
                                                                        {{--<input type="text" name="philhealth" class="form-control" value="{{ $client->philhealth }}" />--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}



                                                                <div class="col-md-12 text-right">
                                                                    <br><br>
                                                                    <a href="{{ url("admin/client/{$client->id}") }}" class="btn btn-danger"><span class="fa fa-chevron-left" style="font-size: 0.8em;"></span> Cancel</a>
                                                                    <button class="btn btn-primary"><span class="fa fa-save" style="font-size: 0.8em;"></span> Confirm</button>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                            </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#hasParentCompany").change(function() {
                const isChecked = $(this).is(':checked');

                if(isChecked === true) {
                    $("#parentCompany").slideDown();
                } else {
                    $("#parentCompany").prop('checked', true).slideUp();
                    $("#parentCompany option").first().prop('selected', true);
                }
            });
        });
    </script>
@endsection

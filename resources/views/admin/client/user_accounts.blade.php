@extends('layouts.app')

@section('content')

                <ul class="breadcrumb">
                    <div class="container-fluid">
                        <div class="push-10"><a href="{{ url('admin/clients') }}"><i class="fa fa-chevron-left"></i> Go back to Clients</a></div>

                        <li class="breadcrumb-item">Client</li>
                        <li class="breadcrumb-item active">Contact Persons</li>
                    </div>
                </ul>
                <!-- Dashboard Counts Section-->
                <section class="dashboard-counts no-padding-bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h1><strong>{{ $client->company_name }}</strong></h1>
                                                <h4>{{ $client->company_code }}</h4>
                                            </div>

                                            <div class="col-md-12">
                                                <hr>
                                                <ul class="nav nav-tabs">
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/client/{$id}") }}">Client Information</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/client/{$id}/contact-persons") }}">Contact Persons</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/client/{$id}/cost-center") }}">Cost Center</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/client/{$id}/billings") }}">Billings</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link active">User Accounts</a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="col-md-12">
                                                <br>
                                                <h2>User Accounts</h2>
                                                <div>
                                                    <a href="#" id="addContactPerson" class="btn btn-primary btn-sm pull-right"><span class="fa fa-plus"></span> Add User Account</a>
                                                    <br>
                                                    <br>
                                                </div>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Complete Name</th>
                                                            <th>Username</th>
                                                            <th>Email</th>
                                                            <th>Mobile</th>
                                                            <th>User Access</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if($client->UserAccounts()->count() > 0)
                                                            @foreach($client->UserAccounts() as $userAccount)
                                                                <tr>
                                                                    <td>{{ $contactPerson->name() }}</td>
                                                                    <td>{{ $contactPerson->Position->title }}</td>
                                                                    <td>{{ $contactPerson->email }}</td>
                                                                    <td>{{ $contactPerson->mobile1 }} @if(!empty($contactPerson->mobile2))
                                                                            <br> {{ $contactPerson->mobile2 }} @endif</td>
                                                                    <td>{{ $contactPerson->office_number }}</td>
                                                                    <td>{{ $contactPerson->location }}</td>
                                                                    <td></td>
                                                                    {{--<td width="100">--}}
                                                                        {{--<a href="#" class="btn btn-sm btn-primary editContactPerson" data-id="{{ $contactPerson->id }}" style="margin-right: 5px;"><span class="fa fa-edit"></span> </a>--}}

                                                                        {{--<a href="#" class="btn btn-sm btn-danger deleteContactPerson" data-id="{{ $contactPerson->id }}"><span class="fa fa-remove"></span> </a>--}}
                                                                    {{--</td>--}}
                                                                </tr>
                                                            @endforeach
                                                        @else
                                                            <tr>
                                                                <td colspan="6">No user accounts yet.</td>
                                                            </tr>
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

<div id="deleteContactPerson" tabindex="-1" role="dialog" aria-labelledby="" class="modal fade text-left" aria-hidden="true" style="display: none;">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="exampleModalLabel" class="modal-title">Delete Contact Person</h4>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Contact Person?</p>
            </div>
            <div class="modal-footer">
                <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
            </div>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(document).on('click', '#addContactPerson', function(e) {
                e.preventDefault();
                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load("{{ url("modal/contact-person-add/$id") }}", function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');
                    }
                });
            });

            $(document).on('click', '.editContactPerson', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/contact-person-edit/{$id}") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');
                    }
                });
            });

            $(".deleteContactPerson").click(function(e) {
                e.preventDefault();

                var url = "{{ url("admin/client/{$id}/contact-persons/delete") }}/";

                $("#deleteContactPerson").find('#url').attr('href', url + $(this).attr('data-id'));
                $("#deleteContactPerson").modal('show');
            });
        });
    </script>
@endsection

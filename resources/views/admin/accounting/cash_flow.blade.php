@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('cash_flow')}}">Cash Flow</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12 text-right">
                                    <br />
                                    <a href="{{ route('cash_flow_create') }}" class="btn btn-info push-5-r push-10">Create</a>
                                    <a href="{{ url('/admin/accounting/check_request/payee-names') }}" class="btn btn-info push-5-r push-10">Items</a>
                                </div>

                                <div class="col-md-12">
                                    <br />
                                    <div class="card">
                                        <div class="card-body">
                                            <table id="cash-flow" class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">CF Number</th>
                                                    <th class="text-center">Date</th>
                                                    <th class="text-center">Mode</th>
                                                    <th class="text-center">CV Number</th>
                                                    <th class="text-center">Particulars</th>
                                                    <th class="text-center">Total Amount</th>
                                                    <th class="text-center">Status</th>
                                                    <th width="115">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($cashFlows as $cashFlow)
                                                        <tr>
                                                            <td>{{ $cashFlow->cf_number }}</td>
                                                            <td>{{ date('m/d/Y', strtotime($cashFlow->entry_date)) }}</td>
                                                            <td>{{ $cashFlow->mode }}</td>
                                                            <td>{{ $cashFlow->cv_number }}</td>
                                                            <td>{{ $cashFlow->particulars }}</td>
                                                            <td>{{ $cashFlow->total_amount }}</td>
                                                            <td>{{ $cashFlow->status }}</td>
                                                            <td>
                                                                <a href="{{ url("/admin/accounting/cash_flow/edit/{$cashFlow->id}") }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="hidden" id="template">
            <table>
                <tr>
                    <td>
                        <select name="item[x][cash_flow_item_id]" id="" class="form-control">
                            @foreach(\App\CashFlowItems::orderBy('item')->get() as $item)
                                <option value="{{ $item->id }}" @if(!empty($chartOfAccount) && $chartOfAccount->id == 2) selected @endif>{{ $item->item }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <textarea name="item[x][description]" id="" rows="3" class="form-control"></textarea>
                    </td>
                    <td><input type="text" class="form-control amount" name="item[x][amount]" autocomplete="off" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required></td>
                    <td class="text-center"><button class="btn btn-xs btn-danger removeItem" type="button"><i class="fa fa-remove"></i></button></td>
                </tr>
            </table>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#cash-flow').dataTable({
                'pageLength': 100,
                "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ]
            });

            $("#mode").change(function(e) {
               if($(this).find(':checked').val() == 'Check') {
                   $("#check_date, #check_number").attr({'required' : true, 'disabled' : false})
               } else {
                   $("#check_date, #check_number").attr({'required' : false, 'disabled' : true})
               }
            });

            var x = 1;

            $(document).on('click', '#addNewItem', function(e) {
                var template = $("#template tr").clone();
                e.preventDefault();

                template.find('input, textarea').each(function() {
                    $(this).attr('name', $(this).attr('name').replace('[x]', '[' + x.toString() + ']'));
                });

                template.appendTo('#cashFlowItems tbody');
                x += 1;

                updateParticulars();
            });


            $(document).on('click', ".removeItem", function() {
                $(this).closest('tr').remove();
                updateTotalAmount();
                updateParticulars();
            })


            $(document).on('change, keyup', '#cashFlowItems .amount', function() {
                updateTotalAmount();
            });

            function updateTotalAmount() {
                var totalAmount = 0;

                $("#cashFlowItems .amount").each(function() {
                    var amount = parseFloat($(this).val()).toFixed(2);
                    if($(this).val().length == 0) {
                        amount = 0;
                    }
                    totalAmount += parseFloat(amount);
                });

                $("#totalAmount").text("P "+ commaSeparateNumber(totalAmount) +" PHP");
            }

            $(document).on('change', "#cashFlowItems .cfItem", function() {
                updateParticulars();
            });

            $(document).on('keyup', "#cashFlowItems .cfDesc, #payee_name", function() {
                updateParticulars();
            });

            function updateParticulars()
            {
                var items = '';
                var descriptions = '';
                var payeeName = $("#payee_name").val();

                $("#cashFlowItems .cfItem").each(function() {
                    if(items == '') {
                        items = $(this).find(':selected').text();
                    } else {
                        items = items + ', ' + $(this).find(':selected').text();
                    }
                });

                $("#cashFlowItems .cfDesc").each(function() {
                    if(descriptions == '') {
                        descriptions = $(this).val();
                    } else {
                        descriptions = descriptions + ', ' + $(this).val();
                    }
                });

                $("#particulars").val(items + ' ' + payeeName + ' ' + descriptions);
            }

            updateParticulars();
        })
    </script>
@endsection














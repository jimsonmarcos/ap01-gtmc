@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('check_request')}}">Check Request</a></li>
        <li><a class="link-effect" href="{{ route('payee_names')}}">Payee Name</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('payee_names') }}">Payee Name</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('cash_flow_items') }}">Cash Flow Items</a>
                                        </li>
                                    </ul>

                                    <br>
                                </div>

                                <form action="" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="payee_type">Payee Type</label>
                                            <select class="form-control" id="payee_type" name="payee_type" size="1" disabled>
                                                <option value="services">Services</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="payee_name">Payee Name</label>
                                            <input class="form-control" type="text" id="payee_name" name="payee_name" required />
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">&nbsp;</label>
                                            <div>
                                                <button class="btn btn-success push-5-r push-10"><i class="fa fa-save"></i> Submit</button>
                                            </div>
                                        </div>

                                    </div>

                                </form>

                                <div class="col-md-12">

                                    <table id="payee-names" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="100">Payee Type</th>
                                                <th>Payee Name</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach(\App\PayeeNames::orderBy('payee_name')->get() as $payeeName)
                                                <tr>
                                                    <td>Services</td>
                                                    <td>{{$payeeName->payee_name  }}</td>
                                                    <td>
                                                        <a href="#" class="btn btn-sm btn-primary editPayeeName" data-id="{{ $payeeName->id }}"><i class="fa fa-edit"></i> EDIT</a>
                                                        <a href="#" class="btn btn-sm btn-danger deletePayeeName" data-id="{{ $payeeName->id }}"><i class="fa fa-remove"></i> DELETE</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="deletePayeeName" tabindex="-1" role="dialog" aria-labelledby="deletePayeeName" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete Payee Name</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Payee Name?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#payee-names').DataTable();

            $(document).on('click', '.editPayeeName', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/payee-name-edit") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');

                    }
                });
            });

            $(document).on('click', '.deletePayeeName', function(e) {
                e.preventDefault();

                var url = "{{ url("admin/accounting/check_request/payee-names-delete") }}/" + $(this).attr('data-id');

                $("#deletePayeeName").find('#url').attr('href', url);
                $("#deletePayeeName").modal('show');
            });
        })


    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="#">Check Voucher</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <br />
                                    <table id="checkVouchers" class="table table-bordered table-striped table-condensed">
                                        <thead>
                                            <tr>
                                                <th>CV NUMBER</th>
                                                <th>PAYEE</th>
                                                <th>CHECK DATE</th>
                                                <th>CHECK NUMBER</th>
                                                <th>AMOUNT</th>
                                                <th width="180"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($items as $checkVoucher)
                                                <?php
                                                  $action = $checkVoucher->is_updated == 'Y' ? 'view' : 'update';
                                                ?>
                                                <tr>
                                                    <td>{{ $checkVoucher->cv_number }}</td>
                                                    <td>{{ $checkVoucher->payee_name }}</td>
                                                    <td>{{ date('m/d/Y', strtotime($checkVoucher->check_date)) }}</td>
                                                    <td>{{ $checkVoucher->check_number }}</td>
                                                    <td>{{ number_format($checkVoucher->total_amount, 2) }}</td>
                                                    <th class="">
                                                        <a href="{{ url("/admin/accounting/check_voucher/edit/{$checkVoucher->id}") }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a>
                                                        <a href="{{ url("/admin/accounting/cash_flow/edit/{$checkVoucher->id}") }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Edit</a>

                                                        @if(!empty($checkVoucher->approved_by))
                                                            <a href="{{ url("/admin/accounting/check_voucher/print/{$checkVoucher->id}") }}" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Print</a>
                                                        @endif
                                                    </th>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready( function() {
            $('#checkVouchers').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": []
            });
        })
    </script>
@endsection

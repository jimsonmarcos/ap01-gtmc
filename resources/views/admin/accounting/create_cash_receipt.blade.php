@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('cash_receipt')}}">Cash Receipt</a></li>
        <li><a class="link-effect" href="{{ route('create_cash_receipt')}}">Create Cash Receipt</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12 text-right">
                                    {{--<i class="fa fa-print fa-2x" aria-hidden="true"></i>--}}
                                    <br>
                                </div>

                                <form action="" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="">Mode</label>
                                                    <select name="mode" id="mode" class="form-control">
                                                        <option value="Cash">CASH</option>
                                                        <option value="Check">CHECK</option>
                                                        <option value="Credit">CREDIT</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="" for="">Check Date</label>
                                                    <div class='input-group btn-mmddyyyy'>
                                                            <input type='text' class="form-control" id="check_date" name="check_date" value="" placeholder="mm/dd/yyyy" disabled />
                                                            <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="">Check Number</label>
                                                    <input type="text" name="check_number" id="check_number" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' disabled/>
                                                </div> 
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="">Payor Type</label>
                                                    <select name="payor_type" id="payorType" class="form-control">
                                                        <option value="Member">MEMBER</option>
                                                        <option value="Employee">EMPLOYEE</option>
                                                        <option value="Client">CLIENT</option>
                                                        <option value="Services">SERVICES</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="" for="">OR Date</label>
                                                    <div class='input-group btn-mmddyyyy'>
                                                        <input type='text' class="form-control" name="or_date" value="{{ date('m/d/Y') }}" placeholder="mm/dd/yyyy" required />
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="">OR Number</label>
                                                    <input type="text" name="or_number" class="form-control" value="{{ $orNumber }}" onkeypress='return event.charCode >= 48 && event.charCode <= 57' />
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="">Billing Type</label>
                                                    <select name="billing_type" id="billingType" class="form-control">
                                                        <option value="Billing Invoice">BILLING INVOICE</option>
                                                        <option value="Non-Billing Invoice">NON-BILLING INVOICE</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="">Payor Name</label>
                                                    <select name="payor_name" id="payorName" class="form-control">

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Reference Number</label>
                                            <textarea name="reference_number" id="referenceNumber" cols="" rows="3"
                                                      class="form-control"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <h2 class="content-heading">Summary / Schedule of Expenditures</h2>
                                    </div>

                                    <div class="col-md-12">
                                        <table id="cashReceiptItems" class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>ITEM</th>
                                                    <th>INVOICE NUMBER</th>
                                                    <th>AMOUNT</th>
                                                    <th width="5"></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="">
                                                            <select name="item[0][item]" id="" class="form-control cashFlowItem">
                                                                @foreach(\App\CashFlowItems::where('payee_type', 'Member')->orderBy('item')->get() as $cashFlowItem)
                                                                    <option value="{{ $cashFlowItem->item }}">{{ $cashFlowItem->item }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="">
                                                            <select name="item[0][invoice_number]" id="" class="form-control invoiceNumber">
                                                                <option value="">Select Payor name first</option>
                                                                {{--@foreach(\App\Invoices::where('status', 'Unpaid')->orderByDesc('invoice_number')->get() as $invoice)--}}
                                                                    {{--<option value="{{ $invoice->invoice_number }}" data-amount="{{ $invoice->total_amount }}">{{ $invoice->invoice_number }}</option>--}}
                                                                {{--@endforeach--}}
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td><input type="text" class="form-control amount" name="item[0][amount]" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    <td class="text-center"><a class="btn btn-sm removeCashReceiptItem btn-danger"><i class="fa fa-remove"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <div><label for="">Description</label></div>
                                                        <textarea name="item[0][description]" class="form-control" id="" rows="3" placeholder="Enter detailed description here (Optional)"></textarea>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-6">
                                        <a href="#" id="addCashReceiptItem" class="btn btn-primary push-5-r push-10"><i class="fa fa-plus"></i> Add New Item</a>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Particulars</label>
                                            <textarea name="particulars" id="particulars" cols="" rows="3"
                                                      class="form-control">{{ \App\CashFlowItems::orderBy('item')->first()->item }}</textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="" style="display: inline-block; margin-right: 20px;">Total Amount</label>
                                                <span id="totalAmount" style="border: 1px blue solid; border-radius: 50px; padding: 10px 20px;font-weight: bold;">Php 0.00</span>
                                                <input type="hidden" name="total_amount" id="totalAmountTxt" value="0">
                                            </div>
                                        </div>
                                        <br>
                                    </div>

                                    {{-- Please change the textfield to its appropriate name--}}
                                    {{--<div class="col-md-12">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="text_field">TextField</label>--}}
                                            {{--<input type="text" name="text_field" class="form-control"/>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Bank</label>
                                            <select name="bank" id="" class="form-control">
                                                @foreach(\App\Banks::orderBy('bank_account')->get() as $bank)
                                                    <option value="{{ $bank->bank_account }}">{{ $bank->bank_account }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Remarks</label>
                                            <textarea name="remarks" id="" cols="" rows="3"
                                                      class="form-control"></textarea>
                                        </div>
                                    </div>


                                    <div class="col-md-12 text-right">
                                        <br />
                                        <a href="{{ url("/admin/accounting/cash_receipt") }}" class="btn btn-danger push-5-r push-10"><i class="fa fa-times"></i> Cancel</a>
                                        <button class="btn btn-success push-5-r push-10"><i class="fa fa-check"></i> Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="hidden" id="template">
        <table>
            <tr>
                <td>
                    <select name="item[x][item]" id="" class="cashFlowItem form-control">
                        @foreach(\App\CashFlowItems::where('payee_type', 'Member')->orderBy('item')->get() as $item)
                            <option value="{{ $item->item }}">{{ $item->item }}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="item[x][invoice_number]" id="" class="form-control invoiceNumber">
                        <option value="">Select Payor name first</option>
                    </select>
                </td>
                <td><input type="text" class="form-control amount" name="item[x][amount]" autocomplete="off" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required></td>
                <td class="text-center"><a class="btn btn-sm removeCashReceiptItem btn-danger"><i class="fa fa-remove"></i></a></td>
            </tr>
            <tr>
                <td colspan="4">
                    <div><label for="">Description</label></div>
                    <textarea name="item[x][description]" class="form-control" id="" rows="3" placeholder="Enter detailed description here (Optional)"></textarea>
                </td>
            </tr>
        </table>
    </div>

    <style>
        .select2-container {
            width: 100% !important;
        }
    </style>
@endsection

@section('extendedscript')
    <script>
        var cashFlowItems = [];
        cashFlowItems['Services'] = $.parseJSON('{!! \App\CashFlowItems::where("payee_type", "Services")->orderBy("item")->get()->toJson() !!}');
        cashFlowItems['Member'] = $.parseJSON('{!! \App\CashFlowItems::where("payee_type", "Member")->orderBy("item")->get()->toJson() !!}');
        cashFlowItems['Employee'] = $.parseJSON('{!! \App\CashFlowItems::where("payee_type", "Employee")->orderBy("item")->get()->toJson() !!}');
        cashFlowItems['Client'] = $.parseJSON('{!! \App\CashFlowItems::where("payee_type", "Client")->orderBy("item")->get()->toJson() !!}');

        $(document).ready(function() {

            $("#mode").change(function(e) {
                if($(this).find(':checked').val() == 'Check') {
                    $("#check_date, #check_number").prop({'required' : true, 'disabled' : false})
                } else {
                    $("#check_date, #check_number").prop({'required' : false, 'disabled' : true})
                }
            });

            updatePayorName();
            updateCashFlowItems($("#payorType").find(':selected').val());

            $("#payorType").change(function() {
                updateCashFlowItems($(this).find(':selected').val());
            });

            function updateCashFlowItems(payeeType) {
                // console.log(cashFlowItems[payeeType]);

                $(".cashFlowItem > option").remove();

                $.each(cashFlowItems[payeeType], function(index, value) {
                    $(".cashFlowItem").append('<option value="'+ value.item +'">'+ value.item +'</option>');

                });
            }

            $("#payorType, #billingType").change(function() {
                // $('#payorType').val(null);
                // $("span.select2").remove();
                updatePayorName();
            });

            function updatePayorName() {
                var billingType = $("#billingType").find(':selected').val();

                if(billingType == 'Billing Invoice') {
                    $("#payorType").find(':selected').removeAttr('selected');
                    $("#payorType > option").each(function() {
                       if($(this).val() == 'Client') {
                           $(this).prop('selected', true);
                           $("#payorType").prop('disabled', true);
                       }
                    });
                } else {
                    $("#payorType > option").each(function() {
                        if($(this).val() == 'Client') {
                            $(this).removeAttr('selected');
                        }
                    });
                    $("#payorType").prop('disabled', false);
                    // $("#payorType").find(':selected').removeAttr('selected');
                    // $("#payorType").find('option').first().attr('selected', true);
                }

                var payorType = $("#payorType").find(':selected').val();

                if(payorType == 'Member') {
                    var url = "{{ url('api/users/members') }}";
                } else if(payorType == 'Employee') {
                    var url = "{{ url('api/users/employees') }}";
                } else if(payorType == 'Client') {
                    var url = "{{ url('api/clients') }}";
                } else if(payorType == 'Services') {
                    var url = "{{ url('api/payee-names') }}";
                }



                $("#payorName").select2({
                    placeholder: {
                        id: '-1', // the value of the option
                        id_number: "Search Payor name here..."
                    },
                    ajax: {
                        url: url,
                        dataType: 'json',
                        delay: 300,
                        data: function (params) {
                            return {
                                q: params.term, // search term
//                            page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            // parse the results into the format expected by Select2
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data, except to indicate that infinite
                            // scrolling can be used
                            params.page = params.page || 1;

                            // console.log();

                            if(payorType == 'Member' || payorType == 'Employee') {
                                var items = data.items;
                                $.each(items, function(index, value) {
                                    // console.log(value);
                                    value.id = value.name;
                                });
                            } else if(payorType == 'Client') {
                                var items = data.items;
                                $.each(items, function(index, value) {
                                    // console.log(value);
                                    value.id = value.company_name;
                                    value.name = value.company_name;
                                });
                            } else if(payorType == 'Services') {
                                var items = data.items;
                                $.each(items, function(index, value) {
                                    // console.log(value);
                                    value.id = value.payee_name;
                                    value.name = value.payee_name;
                                });
                            }




                            return {
                                results: data.items,
                                pagination: {
                                    more: (params.page * 30) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                    minimumInputLength: 3,
                    templateResult: formatRepo,
                    templateSelection: formatRepoSelection
                });

                // console.log();

            }

            function formatRepo (user) {
                if (user.loading) {
                    return user.name;
                }
//                console.log(user);

                var markup = "<span>"+ user.name +"</span>"

                return markup;
            }

            function formatRepoSelection (user) {
                return user.name;
            }

            $(document).on('click', ".removeCashReceiptItem", function() {
                event.preventDefault();

                $(this).closest('tr').next().remove();
                $(this).closest('tr').remove();
                updateTotalAmount();
                updateParticulars();
            });

            {{--var x = 1;--}}
            {{--$(document).on('click', '#addCashReceiptItem', function() {--}}
                {{--event.preventDefault();--}}


                {{--var itemTemplate = "<tr>" +--}}
                    {{--"<td><select name='item["+ x +"][item]' id='' class='form-control cashFlowItem'>" +--}}
                    {{--@foreach(\App\CashFlowItems::where('payee_type', 'Member')->orderBy('item')->get() as $cashFlowItem)--}}
                    {{--"<option value='{{ $cashFlowItem->item }}'>{{ $cashFlowItem->item }}</option>" +--}}
                    {{--@endforeach--}}
                    {{--"</select></td>" +--}}
                    {{--"<td><select name='item["+ x +"][invoice_number]' id='' class='form-control invoiceNumber'><option value=\"\"></option>" +--}}
                    {{--@foreach(\App\Invoices::where('status', 'Unpaid')->orderByDesc('invoice_number')->get() as $invoice)--}}
                    {{--"<option value='{{ $invoice->invoice_number }}' data-amount='{{ $invoice->total_amount }}'>{{ $invoice->invoice_number }}</option>" +--}}
                    {{--@endforeach--}}
                    {{--"</select></td>" +--}}
                    {{--"<td><input type='text' class='form-control amount' name='item["+ x +"][amount]' oninput=\'this.value = this.value.replace(/[^0-9.,]/g, \"\").replace(/(\\..*)\\./g, \"$1\");\'></td>" +--}}
                    {{--"<td class='text-center'><a class='btn btn-sm removeCashReceiptItem btn-danger'><i class='fa fa-remove'></i></a></td>" +--}}
                    {{--"</tr><tr>" +--}}
                    {{--"<td colspan='4'><div><label for=''>Description</label></div>" +--}}
                    {{--"<textarea name='item["+ x +"][description]' class='form-control' id='' rows='3' placeholder='Enter detailed description here (Optional)'></textarea>" +--}}
                    {{--"</td></tr>";--}}



                {{--$("#cashReceiptItems").find('tbody').append(itemTemplate);--}}
                {{--updateParticulars();--}}

                {{--x++;--}}
            {{--});--}}

            var x = 1;

            $(document).on('click', '#addCashReceiptItem', function(e) {
                var template = $("#template tr").clone();
                e.preventDefault();

                template.find('input, textarea, select').each(function() {
                    $(this).attr('name', $(this).attr('name').replace('[x]', '[' + x.toString() + ']'));
                });

                template.appendTo('#cashReceiptItems tbody');
                x += 1;

                updateParticulars();
            });

            $(document).on('change', '.invoiceNumber', function() {
                var amount = parseFloat($(this).find(':selected').attr('data-amount') || 0);

                $(this).closest('tr').find('.amount').val(amount.numberFormat(2));
                updateTotalAmount();

                var x = 0;
                $("#cashReceiptItems .invoiceNumber").each(function() {
                    if(x == 0) {
                        $("#referenceNumber").val($(this).val());
                    } else {
                        $("#referenceNumber").val($("#referenceNumber").val() + ', ' + $(this).val());
                    }


                    x++;
                });

            });

            function updateTotalAmount() {
                var totalAmount = 0;
                var amountIntoWords = '';

                if($("#cashReceiptItems .amount").length > 0) {
                    $("#cashReceiptItems .amount").each(function() {
                        var amount = parseFloat($(this).val().replace(/\,/g, '')).toFixed(2) || 0;
                        if($(this).val().length == 0) {
                            amount = 0;
                        }
                        totalAmount += parseFloat(amount);
                    });
                }

                $("#totalAmount").text("Php "+ totalAmount.numberFormat(2));
                $("#totalAmountTxt").val(totalAmount);
            }

            $(document).on('change, keyup', '#cashReceiptItems .amount', function() {
                updateTotalAmount();
            });

            $(document).on('change', "#cashReceiptItems .cashFlowItem", function() {
                updateParticulars();
            });

            function updateParticulars() {
                $("#particulars").val('');

                var ix = 0;

                $("#cashReceiptItems .cashFlowItem").each(function() {
                    if(ix == 0) {
                        $("#particulars").val($(this).find(':selected').val());
                    } else {
                        var particulars = $("#particulars").val();
                        $("#particulars").val(particulars + ", " + $(this).find(':selected').val());
                    }

                    ix++;
                });

                var particulars = $("#particulars").val();
                var payorName = $("#payorName").val() || '';
                if(payorName != '') {
                    payorName = " - " + payorName;
                }
                $("#particulars").val(particulars + payorName);
            }

            $('#payorName').on('select2:select', function (evt) {
                updateParticulars();

                // .invoiceNumber

                var data = evt.params.data;
                // var address = data.element.dataset.address;
                // console.log(data.company_name);


                $.get('{{ url("api/cash-receipt/invoice-number") }}', { payor_name : data.company_name }, function(result) {
                    var json = $.parseJSON(result);

                    $(".invoiceNumber").find('option').remove();

                    if(json.length > 0) {
                        $(".invoiceNumber").append('<option>Select invoice number.</option>');

                        $.each(json, function(i, invoice) {
                            $(".invoiceNumber").append('<option value="'+ invoice.invoice_number +'" data-amount="'+ invoice.total_amount +'">'+ invoice.invoice_number +'</option>');
                        });
                    } else {
                        $(".invoiceNumber").append('<option>No invoice number found.</option>');
                    }
                });
            });
        });
    </script>
@endsection

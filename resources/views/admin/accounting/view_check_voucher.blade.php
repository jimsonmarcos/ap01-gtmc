@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('check_voucher')}}">Check Voucher</a></li>
        <li><a class="link-effect" href="">Update</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <br />
                                <form action="" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="col-md-6">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="payee_name">Payee Name</label>
                                                <div class="col-xs-12">
                                                    {{ $checkVoucher->payee_name }}
                                                </div>
                                            </div>
                                            <br />
                                        </div>

                                        <div class="col-md-12">
                                            <br />
                                            <div class="form-group">
                                                <label class="col-xs-12" for="payee_address">Payee Address</label>
                                                <div class="col-xs-12">
                                                    {{ $checkVoucher->payee_address }}
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="col-md-6">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="cv_number">CV Number</label>
                                                <div class="col-xs-12">
                                                    {{ $checkVoucher->cv_number }}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 push-10-t">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Check Date</label>
                                                    <div>{{ date('m/d/Y', strtotime($checkVoucher->check_date)) }}</div>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="check_number">Check Number</label>
                                                <div class="col-xs-12">
                                                    {{ $checkVoucher->check_number }}
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12 push-10-t">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="col-xs-12" for="particulars">Particulars</label>
                                                <div class="col-xs-12">
                                                    {{ $checkVoucher->particulars }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <br />
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="col-xs-12" for="amount">Amount</label>
                                                <div class="col-xs-12">
                                                    {{ number_format($checkVoucher->amount, 2) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <h2 class="content-heading">Entry</h2>
                                    </div>

                                    <div class="col-md-12">
                                        <br />
                                        <table id="checkVoucherItems" class="table table-striped table-bordered table-condensed">
                                            <thead>
                                                <tr>
                                                    <th>CHART OF ACCOUNT</th>
                                                    <th>TXN</th>
                                                    <th>DEBIT</th>
                                                    <th>CREDIT</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($checkVoucherItems as $item)
                                                <tr>
                                                    <td>{{ $item->ChartOfAccount->account_type }} - {{ $item->ChartOfAccount->account_title }}</td>
                                                    <td width="100">{{ $item->txn }}</td>
                                                    <td class="text-right">{{ $item->debit }}</td>
                                                    <td class="text-right">{{ $item->credit }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="2"></th>
                                                    <th id="totalDebitAmount" class="text-right">{{ number_format($item->sum('debit'), 2) }} PHP</th>
                                                    <th id="totalCreditAmount" class="text-right">{{ number_format($item->sum('credit'), 2) }} PHP</th>
                                                    <th></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>



                                    <div class="col-md-6">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="prepared_by">Prepared By</label>
                                                <div class="col-xs-12">
                                                    {{ $checkVoucher->PreparedBy->name }}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12"></div>

                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="approved_by">Approved By</label>
                                                <div class="col-xs-12">
                                                    {{ $checkVoucher->ApprovedBy->name }}
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <h2 class="content-heading">
                                            Recieved from {% Company Name %} the sum of <span id="amountIntoWords" data-amount="{{ $item->sum('credit') }}" ></span> PHP {{ number_format($item->sum('credit'), 2) }}
                                        </h2>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        {{--<a class="btn btn-danger push-5-r push-10" onclick="window.history.go(-1); return false;" type="button" ><i class="fa fa-chevron-left"></i> Back</a>--}}
                                        {{--<button class="btn btn-success push-5-r push-10"><i class="fa fa-check"></i> Update</button>--}}
                                        {{--<a href="#" class="btn btn-primary push-5-r push-10">Preview</a>--}}
                                    </div>
                                </form>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            var totalAmount = parseFloat($("#amountIntoWords").attr('data-amount'));
            var amountIntoWords = chunk(totalAmount)
                .map(inEnglish)
                .map(appendScale)
                .filter(isTruthy)
                .reverse()
                .join(" ");

            $("#amountIntoWords").text(amountIntoWords);
        })
    </script>
@endsection

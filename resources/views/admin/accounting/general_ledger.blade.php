@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('general_ledger')}}">General Ledger</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-sm-12">
                                    <br/>
                                    <form action="" method="GET">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="" for="date_from">Banks</label>
                                                <select name="bank" id="" class="form-control">
                                                    <option value="">All</option>
                                                    @foreach(\App\Banks::orderBy('bank_account')->get() as $bank)
                                                        <option value="{{ $bank->bank_account }}" @if($bank->bank_account == $b) selected @endif>{{ $bank->bank_account }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="" for="date_from">Date from</label>
                                                <div class='input-group btn-mmddyyyy'>
                                                    <input type='text' class="form-control" name="date_from" value="{{ date('m/d/Y', strtotime($date_from)) }}" placeholder="mm/dd/yyyy" required />
                                                    <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="" for="">Date To</label>
                                                <div class='input-group btn-mmddyyyy'>
                                                    <input type='text' class="form-control" name="date_to" value="{{ date('m/d/Y', strtotime($date_to)) }}" placeholder="mm/dd/yyyy" required />
                                                    <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-sm-3">
                                            <br />
                                            <button class="btn btn-primary push-5-r push-10">
                                                <i class="fa fa-arrow-right"></i> Extract
                                            </button>
                                        </div>
                                    </form>

                                    <div class="col-md-12 text-right">
                                        <br />
                                        <a href="{{ url("admin/accounting/general_ledger/download?". request()->getQueryString()) }}" class="btn btn-info push-10" ><i class="fa fa-download"></i> Download</a>
                                    </div>

                                </div>
                                {{-- Advanced Search uncomment if needed --}}
                                {{--<div class="col-md-12 text-right">--}}
                                    {{--<br />--}}
                                    {{--<!-- Trigger the modal with a button -->--}}
                                    {{--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">--}}
                                        {{--Advanced Search--}}
                                    {{--</button>--}}
                                    {{--<div class="modal fade" id="myModal" role="dialog">--}}
                                        {{--<div class="modal-dialog">--}}
                                            {{--<!-- Modal content-->--}}
                                            {{--<div class="modal-content">--}}
                                                {{--<div class="modal-header">--}}
                                                    {{--<h4 class="modal-title">Advanced Search</h4>--}}
                                                {{--</div>--}}
                                                {{--<div class="modal-body text-left">--}}
                                                    {{--<div class="row">--}}

                                                        {{--<div class="col-md-6">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label class="col-xs-12" for="column_name">Column Name</label>--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<select class="form-control" id="column_name" name="column_name" size="1">--}}
                                                                        {{--<option value="payee_type">Payee Type</option>--}}
                                                                        {{--<option value="payee_name">Payee Name</option>--}}
                                                                        {{--<option value="payee_address">Payee Address</option>--}}
                                                                        {{--<option value="crf_number">CRF Number</option>--}}
                                                                        {{--<option value="item">Item</option>--}}
                                                                        {{--<option value="reference_number">Reference Number</option>--}}
                                                                    {{--</select>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-6">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<label for="colum_name_text">Column Name Text</label>--}}
                                                                    {{--<input class="form-control" type="text" id="colum_name_text" name="colum_name_text" />--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-4">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label class="col-xs-12" for="column_date">Column Date</label>--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<select class="form-control" id="column_date" name="column_date" size="1">--}}
                                                                        {{--<option value="request_date">Request Date</option>--}}
                                                                    {{--</select>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-4">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<label class="form-control-label">Column Date From</label>--}}
                                                                    {{--<div id="joinDate" class="input-group btn-mmddyyyy ">--}}
                                                                        {{--<input type="text" class="form-control" name="column_date_from" placeholder="mm/dd/yyyy" required="">--}}
                                                                        {{--<span class="input-group-addon"><span class="fa fa-calendar"></span></span>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-4">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<label class="form-control-label">Column Date To</label>--}}
                                                                    {{--<div id="joinDate" class="input-group btn-mmddyyyy ">--}}
                                                                        {{--<input type="text" class="form-control" name="column_date_to" placeholder="mm/dd/yyyy" required="">--}}
                                                                        {{--<span class="input-group-addon"><span class="fa fa-calendar"></span></span>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="modal-footer">--}}
                                                    {{--<button type="button" class="btn btn-success" data-dismiss="modal">View</button>--}}
                                                    {{--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{-- End Advanced Search--}}

                                <div class="col-md-12">
                                    <div class="block">
                                        <div class="block-content">
                                            <table id="general_ledger" class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Bank</th>
                                                    <th>Account Type</th>
                                                    <th>Account Title</th>
                                                    <th>TXN</th>
                                                    <th>Debit Amount</th>
                                                    <th>Credit Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($items as $item)
                                                <tr>
                                                    <td>{{ date('m/d/Y', strtotime($item->date)) }}</td>
                                                    <td>{{ $item->bank }}</td>
                                                    <td>{{ $item->account_type }}</td>
                                                    <td>{{ $item->account_title }}</td>
                                                    <td>{{ $item->txn == 'Credit' ? 'CR' : 'DR' }}</td>
                                                    <td class="text-right">{{ number_format($item->debit, 2) }}</td>
                                                    <td class="text-right">{{ number_format($item->credit, 2) }}</td>
                                                </tr>
                                                @endforeach

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Total</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th class="text-right"></th>
                                                        <th class="text-right"></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            {{--var table = $('#loans').DataTable({--}}
                {{--"processing": true,--}}
                {{--"serverSide": true,--}}
                {{--"bFilter": false,--}}
                {{--"searching": true,--}}
                {{--"dom": "ltip",  // Remove global search box--}}
                {{--"ajax":{--}}
                    {{--"url": "{{ url('api/datatables/loans/'.$status) }}",--}}
                    {{--"dataType": "json",--}}
                    {{--"type": "POST",--}}
                    {{--"data":{ _token: "{{csrf_token()}}"}--}}
                {{--},--}}
                {{--"columns": [--}}
                    {{--{ "data": "cla_number" },--}}
                    {{--{ "data": "id_number" },--}}
                    {{--{ "data": "name" },--}}
                    {{--{ "data": "cost_center" },--}}
                    {{--{ "data": "loan_type" },--}}
                    {{--{ "data": "granted_loan_amount" },--}}
                    {{--{ "data": "status" },--}}
                    {{--{ "data": "action" },--}}
                    {{--{ "data": "principal"}--}}
                {{--]--}}

            {{--});--}}

            {{--$('#searchButton').on( 'click', function () {--}}
                {{--var q = $("#searchQuery").val();--}}
                {{--table.search( q ).draw();--}}
            {{--});--}}

            $("#general_ledger").dataTable({
                "order": [],
                'pageLength': 100,
                "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    totalDebit = api
                        .column( 5 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    totalCredit = api
                        .column( 6 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    // pageTotal = api
                    //     .column( 6, { page: 'current'} )
                    //     .data()
                    //     .reduce( function (a, b) {
                    //         return intVal(a) + intVal();
                    //     }, 0 );

                    // Update footer
                    $( api.column( 5 ).footer() ).html(
                        totalDebit.numberFormat(2)
                    );
                    $( api.column( 6 ).footer() ).html(
                        totalCredit.numberFormat(2)
                    );
                }
            });
        })
    </script>
@endsection

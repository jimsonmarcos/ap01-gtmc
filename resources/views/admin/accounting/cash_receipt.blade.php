@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('cash_receipt')}}">Cash Receipt</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12 text-right">
                                    <br />
                                    <a href="{{ route('create_cash_receipt') }}" class="btn btn-primary push-5-r push-10"><i class="fa fa-plus"></i> Create</a>
                                </div>

                                {{-- Advanced search uncomment if needed --}}
                                {{--<div class="col-md-12 text-right">--}}
                                    {{--<br />--}}
                                    {{--<!-- Trigger the modal with a button -->--}}
                                    {{--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">--}}
                                        {{--Advanced Search--}}
                                    {{--</button>--}}
                                    {{--<div class="modal fade" id="myModal" role="dialog">--}}
                                        {{--<div class="modal-dialog">--}}
                                            {{--<!-- Modal content-->--}}
                                            {{--<div class="modal-content">--}}
                                                {{--<div class="modal-header">--}}
                                                    {{--<h4 class="modal-title">Advanced Search</h4>--}}
                                                {{--</div>--}}
                                                {{--<div class="modal-body text-left">--}}
                                                    {{--<div class="row">--}}

                                                        {{--<div class="col-md-6">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label class="col-xs-12" for="column_name">Column Name</label>--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<select class="form-control" id="column_name" name="column_name" size="1">--}}
                                                                        {{--<option value="payee_type">Payee Type</option>--}}
                                                                        {{--<option value="payee_name">Payee Name</option>--}}
                                                                        {{--<option value="payee_address">Payee Address</option>--}}
                                                                        {{--<option value="crf_number">CRF Number</option>--}}
                                                                        {{--<option value="item">Item</option>--}}
                                                                        {{--<option value="reference_number">Reference Number</option>--}}
                                                                    {{--</select>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-6">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<label for="colum_name_text">Column Name Text</label>--}}
                                                                    {{--<input class="form-control" type="text" id="colum_name_text" name="colum_name_text" />--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-4">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label class="col-xs-12" for="column_date">Column Date</label>--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<select class="form-control" id="column_date" name="column_date" size="1">--}}
                                                                        {{--<option value="request_date">Request Date</option>--}}
                                                                    {{--</select>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-4">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<label class="form-control-label">Column Date From</label>--}}
                                                                    {{--<div id="joinDate" class="input-group btn-mmddyyyy ">--}}
                                                                        {{--<input type="text" class="form-control" name="column_date_from" placeholder="mm/dd/yyyy" required="">--}}
                                                                        {{--<span class="input-group-addon"><span class="fa fa-calendar"></span></span>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-4">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<label class="form-control-label">Column Date To</label>--}}
                                                                    {{--<div id="joinDate" class="input-group btn-mmddyyyy ">--}}
                                                                        {{--<input type="text" class="form-control" name="column_date_to" placeholder="mm/dd/yyyy" required="">--}}
                                                                        {{--<span class="input-group-addon"><span class="fa fa-calendar"></span></span>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="modal-footer">--}}
                                                    {{--<button type="button" class="btn btn-success" data-dismiss="modal">View</button>--}}
                                                    {{--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{-- End Advanced Search --}}

                                <div class="col-md-12">
                                    <div class="block">
                                        <div class="block-content">
                                            <table id="cash-reciept" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>OR Number</th>
                                                    <th>OR Date</th>
                                                    <th>Mode</th>
                                                    <th>Payor Name</th>
                                                    <th>Total Amount</th>
                                                    <th width="100">Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($cashReceipts as $cashReceipt)
                                                    <tr>
                                                        <td>{{ $cashReceipt->or_number }}</td>
                                                        <td>{{ $cashReceipt->or_date }}</td>
                                                        <td>{{ $cashReceipt->mode }}</td>
                                                        <td>{{ $cashReceipt->payor_name }}</td>
                                                        <th class="text-right">{{ number_format($cashReceipt->total_amount, 2) }}</th>
                                                        <td>
                                                            @if(\App\CashFlows::where(['source' => 'Cash Receipt', 'source_id' => $cashReceipt->id])->exists())
                                                                <a href="{{ url("/admin/accounting/cash_flow/edit/". \App\CashFlows::where(['source' => 'Cash Receipt', 'source_id' => $cashReceipt->id])->first()->id) }}" class="btn btn-sm btn-primary">CASH FLOW <i class="fa fa-arrow-right"></i></a>
                                                            @else
                                                                <a href="{{ url("/admin/accounting/cash_flow/create?source=cash_receipt&id={$cashReceipt->id}") }}" class="btn btn-sm btn-primary">CASH FLOW <i class="fa fa-arrow-right"></i></a>
                                                            @endif

                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#cash-reciept").dataTable({
                "order": [],
                'pageLength': 100,
                "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ]
            });
        })
    </script>
@endsection

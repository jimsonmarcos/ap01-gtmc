@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('check_request')}}">Check Request</a></li>
        <li><a class="link-effect" href="{{ route('cash_flow_items')}}">Add Cash Flow Items</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link " href="{{ route('payee_names') }}">Add Payee Name</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('cash_flow_items') }}">Add Cash Flow Items</a>
                                        </li>
                                    </ul>

                                    <br />

                                </div>

                                <form action="" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="payee_type">Payee Type</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="payee_type" name="payee_type" size="1" required>
                                                    <option value="Member">Member</option>
                                                    <option value="Employee">Employee</option>
                                                    <option value="Client">Client</option>
                                                    <option value="Services">Services</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="cash_flow_items">Cash Flow Items</label>
                                                <input class="form-control" type="text" id="cash_flow_items" name="item" required />
                                                <br />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="">&nbsp;</label>
                                                <div>
                                                    <button class="btn btn-success push-5-r push-10"><i class="fa fa-save"></i> Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </form>

                                <div class="col-md-12">
                                    <br />
                                    <br />

                                    <table id="cashFlowItems" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>PAYEE TYPE</th>
                                                <th>CASH FLOW ITEM</th>
                                                {{--<th>STATUS</th>--}}
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach(\App\CashFlowItems::orderByDesc('created_at')->get() as $cashFlowItem)
                                                <tr>
                                                    <td>{{ $cashFlowItem->payee_type }}</td>
                                                    <td>{{ $cashFlowItem->item }}</td>
                                                    {{--<td>{{ $cashFlowItem->status }}</td>--}}
                                                    <td>
                                                        {{--<a href="#" class="btn btn-sm btn-primary editCashFlowItem" data-id="{{ $cashFlowItem->id }}"><i class="fa fa-edit"></i> EDIT</a>--}}
                                                        <a href="#" class="btn btn-sm btn-danger deleteCashFlowItem" data-id="{{ $cashFlowItem->id }}"><i class="fa fa-remove"></i> DELETE</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="deleteCashFlowItem" tabindex="-1" role="dialog" aria-labelledby="deleteCashFlowItem" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Delete Cash Flow Item</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Cash Flow Item?</p>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#cashFlowItems').DataTable();

            $(document).on('click', '.editCashFlowItem', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/cash-flow-item-edit") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');

                    }
                });
            });

            $(document).on('click', '.deleteCashFlowItem', function(e) {
                e.preventDefault();

                var url = "{{ url("/admin/accounting/check_request/cash-flow-items-delete") }}/" + $(this).attr('data-id');

                $("#deleteCashFlowItem").find('#url').attr('href', url);
                $("#deleteCashFlowItem").modal('show');
            });
        });
    </script>
@endsection

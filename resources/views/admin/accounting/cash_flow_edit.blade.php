@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('cash_flow_create')}}">Cash Flow</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <br />
                                    <form action="" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        <div class="col-md-6">
                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="form-group">
                                                        <label class="col-xs-12" for="mode">Mode</label>
                                                        <div class="col-xs-12">
                                                            <select class="form-control" id="mode" name="mode">
                                                                <option value="Cash" @if($cashFlow->mode == 'Cash') selected @endif>CASH</option>
                                                                <option value="Check" @if($cashFlow->mode == 'Check') selected @endif>CHECK</option>
                                                                <option value="Credit" @if($cashFlow->mode == 'Credit') selected @endif>CREDIT</option>
                                                            </select>
                                                            <br />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label class="form-control-label">Check Date</label>
                                                            <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                                <input type="text" class="form-control" id="check_date" name="check_date" placeholder="mm/dd/yyyy" value="@if($cashFlow->mode == 'Check'){{ date('m/d/Y',strtotime($cashFlow->check_date)) }}@endif" @if($cashFlow->mode != 'Check') disabled @endif>
                                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                            </div>
                                                            <br />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-xs-12" for="check_number">Check Number</label>
                                                        <div class="col-xs-12">
                                                            <div class="input-group">
                                                                <input class="form-control" type="text" id="check_number" name="check_number" value="{{ $cashFlow->check_number }}" onkeypress='return event.charCode >= 48 && event.charCode <= 57' @if($cashFlow->mode != 'Check') disabled @endif>
                                                                <span class="input-group-btn">
                                                                <button id="searchCheckNumber" class="btn btn-default" type="button"><i class="fa fa-search"></i> Search</button>
                                                            </span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <br />
                                                </div>

                                                <div class="col-md-12">
                                                    <br />
                                                    <div class="form-group">
                                                        <label class="col-xs-12" for="payee_type">Payee Type</label>
                                                        <div class="col-xs-12">
                                                            <select name="payee_type" id="payeeType" class="form-control">
                                                                <option value="Member" @if($cashFlow->payee_type == 'Member') selected @endif>MEMBER</option>
                                                                <option value="Employee" @if($cashFlow->payee_type == 'Employee') selected @endif>EMPLOYEE</option>
                                                                <option value="Client" @if($cashFlow->payee_type == 'Client') selected @endif>CLIENT</option>
                                                                <option value="Services" @if($cashFlow->payee_type == 'Services') selected @endif>SERVICES</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label class="form-control-label">Entry Date</label>
                                                            <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                                <input type="text" class="form-control" name="entry_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y', strtotime($cashFlow->entry_date)) }}" required>
                                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <br />
                                                    <div class="form-group">
                                                        <label class="col-xs-12" for="cf_status">Status</label>
                                                        <div class="col-xs-12">
                                                            <select class="form-control" id="cf_status" name="status">
                                                                <option value="Deposit" @if($cashFlow->status == 'Deposit') selected @endif>Deposit</option>
                                                                <option value="Withdrawal" @if($cashFlow->status == 'Withdrawal') selected @endif>Withdrawal</option>
                                                                <option value="Uncleared" @if($cashFlow->status == 'Uncleared') selected @endif>Uncleared</option>
                                                                <option value="Cancelled" @if($cashFlow->status == 'Cancelled') selected @endif>Cancelled</option>
                                                                <option value="No Details" @if($cashFlow->status == 'No Details') selected @endif>No Details</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-12">
                                                    <br />
                                                    <div class="form-group">
                                                        <label class="col-xs-12" for="reference_number">Reference Number</label>
                                                        <div class="col-xs-12">
                                                            <input class="form-control" type="text" id="reference_number" name="reference_number" value="{{ $cashFlow->reference_number }}" disabled/>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-12">
                                                    <br />
                                                    <div class="form-group">
                                                        <label class="col-xs-12" for="payee_name">Payee Name</label>
                                                        <div class="col-xs-12">
                                                            <select name="payee_name" id="payeeName" class="form-control">

                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>

                                                {{--<div class="col-md-12">--}}
                                                    {{--<br />--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<label class="col-xs-12" for="payee_address">Payee Address</label>--}}
                                                        {{--<div class="col-xs-12">--}}
                                                            {{--<textarea name="payee_address" id="" rows="4"--}}
                                                                      {{--class="form-control"></textarea>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                            </div>
                                        </div>

                                        <div class="col-md-12 push-20-t">

                                            <table id="cashFlowItems" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" width="300">ITEM</th>
                                                        <th class="text-center">DESCRIPTION</th>
                                                        <th class="text-center" width="200">AMOUNT</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $x = 0; ?>
                                                    @foreach($cashFlow->Items as $cfItem)
                                                    <tr>
                                                        <td>
                                                            <select name="item[{{ $x }}][item]" id="" class="cfItem form-control">
                                                                @foreach(\App\CashFlowItems::where('payee_type', $cashFlow->payee_type)->orderBy('item')->get() as $item)
                                                                    <option value="{{ $item->item }}" @if($cfItem->item == $item->item) selected @endif>{{ $item->item }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <textarea name="item[{{ $x }}][description]" id="" rows="3" class="cfDesc form-control">{{ $cfItem->description }}</textarea>
                                                        </td>
                                                        <td><input type="text" class="form-control amount" name="item[{{ $x }}][amount]" value="{{ number_format($cfItem->amount, 2) }}" autocomplete="off" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required></td>
                                                        <td class="text-center"><button class="btn btn-xs btn-danger removeItem" type="button"><i class="fa fa-remove"></i></button></td>
                                                    </tr>
                                                    <?php $x++; ?>
                                                    @endforeach

                                                </tbody>
                                            </table>

                                        </div>

                                        <div class="col-md-12">
                                            <a href="#" id="addNewItem" class="btn btn-primary push-5-r push-10"><i class="fa fa-plus"></i> Add New Item</a>

                                            <a href="{{ url('/admin/accounting/check_request/cash-flow-items') }}" class="btn btn-info pull-right" target="_blank"><i class="fa fa-plus"></i> Add Cash Flow Item</a>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label for="particulars">Particulars</label>
                                                    <textarea class="form-control input-lg" id="particulars" name="particulars" rows="5" required>{{ $cashFlow->particulars }}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <br />
                                            <div class="row">

                                                <div class="col-md-4">
                                                    <div class="col-xs-12">
                                                        <label class="push-10-t"> Total Amount</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="col-xs-12 text-center">
                                                        <p id="totalAmount" style="border: 1px blue solid; border-radius: 50px; padding: 10px;font-weight: bold;">Php {{ number_format($cashFlow->total_amount, 2) }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>

                                        <div class="col-md-12">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="banks">Banks</label>
                                                    <div class="col-xs-12">
                                                        <select class="form-control" id="banks" name="bank">
                                                            @foreach(\App\Banks::orderBy('bank_account')->get() as $bank)
                                                                <option value="{{ $bank->bank_account }}" @if($bank->bank_account == $cashFlow->bank) selected @endif>{{ $bank->bank_account }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="bank_transaction_id">Bank Transaction ID</label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" type="text" id="bank_transaction_id" name="bank_transaction_id" value="{{ $cashFlow->bank_transaction_id }}" autocomplete="off" onkeypress='return event.charCode >= 48 && event.charCode <= 57' @if($cashFlow->status != 'Deposit') disabled @else required @endif  />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 push-5-t">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="cv_number">CV Number</label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" type="text" id="cv_number" name="cv_number" value="{{ $cashFlow->cv_number }}" autocomplete="off" @if($cashFlow->mode != 'Check') disabled @endif />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 push-5-t">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="cv_number">CV Reference (CV Number)</label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" type="text" id="cv_reference_number" name="cv_reference_number" value="{{ $cashFlow->cv_reference_number }}" autocomplete="off" readonly />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 push-5-t">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="cv_number">CV Reference (Check Number)</label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" type="text" id="cv_reference_check" name="cv_reference_check" value="{{ $cashFlow->cv_reference_check }}" autocomplete="off" readonly />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-12 push-5-t">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="col-xs-12" for="remarks">Remarks</label>
                                                    <div class="col-xs-12">
                                                        <textarea class="form-control input-lg" id="remarks" name="remarks" rows="5">{{ $cashFlow->remarks }}</textarea>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 push-20-t">
                                            <h3 class="content-heading">Entry</h3>

                                            <table id="cashFlowEntries" class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">Account Type</th>
                                                    <th class="text-center">Account Title</th>
                                                    <th class="text-center" width="150">TXN</th>
                                                    <th class="text-center" width="200">DEBIT</th>
                                                    <th class="text-center" width="200">CREDIT</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $x = 0; ?>
                                                @foreach($cashFlow->Entries as $entry)
                                                <tr>
                                                    <td>
                                                        <div class="">
                                                            <select name="entry[{{ $x }}][account_type]" id="" class="form-control accountType">
                                                                @foreach($accountTypes as $accountType)
                                                                    <option value="{{ $accountType->account_type }}" data-account-titles='{{ $accountType->AccountTitles }}' @if($accountType->account_type == $entry->account_type) selected @endif>{{ $accountType->account_type }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="">
                                                            <select name="entry[{{ $x }}][account_title]" id="" class="form-control accountTitle">
                                                                @foreach($accountTypes->where('account_type', $entry->account_type)->first()->AccountTitles as $accountTitle)
                                                                    <option value="{{ $accountTitle->account_title }}" @if($accountTitle->account_title == $entry->account_title) selected @endif>{{ $accountTitle->account_title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="">
                                                            <select name="entry[{{ $x }}][txn]" class="form-control txn">
                                                                <option value="Debit" @if($entry->txn == 'Debit') selected @endif>DR</option>
                                                                <option value="Credit" @if($entry->txn == 'Credit') selected @endif>CR</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td><input type="text" class="form-control debit" name="entry[{{ $x }}][debit]" value="{{ number_format($entry->debit, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" @if($entry->txn == 'Debit') required @else disabled @endif></td>
                                                    <td><input type="text" class="form-control credit" name="entry[{{ $x }}][credit]" value="{{ number_format($entry->credit, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" @if($entry->txn == 'Credit') required @else disabled @endif></td>
                                                    <td class="text-center"><a class="btn btn-sm removeItem btn-danger"><i class="fa fa-remove"></i></a></td>
                                                </tr>
                                                <?php $x++; ?>
                                                @endforeach

                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>Total</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th id="totalDebit">{{ number_format($cashFlow->Entries->sum('debit'), 2) }}</th>
                                                    <th id="totalCredit">{{ number_format($cashFlow->Entries->sum('credit'), 2) }}</th>
                                                    <th></th>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>

                                        <div class="col-md-12">
                                            <a href="#" id="addNewEntry" class="btn btn-primary push-5-r push-10"><i class="fa fa-plus"></i> Add New Item</a>
                                        </div>

                                        {{-- End --}}


                                        {{--<div class="col-md-12">--}}
                                            {{--<h2 class="content-heading text-center">--}}
                                                {{--Notes: Will also a automatic create CV Number depending on Bank and will save both in cash flow and or number--}}
                                            {{--</h2>--}}
                                        {{--</div>--}}

                                        <div class="col-md-12 text-right">
                                            {{--<button class="btn btn-warning" type="reset"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>--}}
                                            <a class="btn btn-primary" onclick="history.back()"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                                            <button class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> Submit</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="hidden" id="template">
            <table>
                <tr>
                    <td>
                        <select name="item[x][item]" id="" class="cfItem form-control">
                            @foreach(\App\CashFlowItems::where('payee_type', 'Member')->orderBy('item')->get() as $item)
                                <option value="{{ $item->item }}">{{ $item->item }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <textarea name="item[x][description]" id="" rows="3" class="form-control"></textarea>
                    </td>
                    <td><input type="text" class="form-control amount" name="item[x][amount]" autocomplete="off" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required></td>
                    <td class="text-center"><button class="btn btn-xs btn-danger removeItem" type="button"><i class="fa fa-remove"></i></button></td>
                </tr>
            </table>
        </div>
    </section>

    <div class="hidden" id="entriesTemplate">
        <table>
            <tr>
                <td>
                    <div class="">
                        <select name="entry[x][account_type]" id="" class="form-control accountType">
                            @foreach($accountTypes as $accountType)
                                <option value="{{ $accountType->account_type }}" data-account-titles='{{ $accountType->AccountTitles }}'>{{ $accountType->account_type }}</option>
                            @endforeach
                        </select>
                    </div>
                </td>
                <td>
                    <div class="">
                        <select name="entry[x][account_title]" id="" class="form-control accountTitle">
                            @foreach($accountTypes->first()->AccountTitles as $accountTitle)
                                <option value="{{ $accountTitle->account_title }}">{{ $accountTitle->account_title }}</option>
                            @endforeach
                        </select>
                    </div>
                </td>
                <td>
                    <div class="">
                        <select name="entry[x][txn]" class="form-control txn">
                            <option value="Debit">DR</option>
                            <option value="Credit">CR</option>
                        </select>
                    </div>
                </td>
                <td><input type="text" class="form-control debit" name="entry[x][debit]" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                <td><input type="text" class="form-control credit" name="entry[x][credit]" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" disabled></td>
                <td class="text-center"><a class="btn btn-sm removeItem btn-danger"><i class="fa fa-remove"></i></a></td>
            </tr>
        </table>
    </div>

    <style>
        .select2-container {
            width: 100% !important;
        }
    </style>
@endsection

@section('extendedscript')
    <script>

        var cashFlowItems = [];
        cashFlowItems['Services'] = $.parseJSON('{!! \App\CashFlowItems::where("payee_type", "Services")->orderBy("item")->get()->toJson() !!}');
        cashFlowItems['Member'] = $.parseJSON('{!! \App\CashFlowItems::where("payee_type", "Member")->orderBy("item")->get()->toJson() !!}');
        cashFlowItems['Employee'] = $.parseJSON('{!! \App\CashFlowItems::where("payee_type", "Employee")->orderBy("item")->get()->toJson() !!}');
        cashFlowItems['Client'] = $.parseJSON('{!! \App\CashFlowItems::where("payee_type", "Client")->orderBy("item")->get()->toJson() !!}');

        $(document).ready(function() {
            $('#cash-flow').dataTable({
                'pageLength': 100,
                "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ]
            });

            $("#mode").change(function(e) {
               if($(this).find(':checked').val() == 'Check') {
                   $("#check_date, #check_number").attr({'required' : true, 'disabled' : false})
               } else {
                   if($("#cf_status").find(':selected').val() == 'Deposit') {
                       $("#bank_transaction_id").attr({'required' : true, 'disabled' : false})
                   }

                   $("#check_date, #check_number").attr({'required' : false, 'disabled' : true})
               }

                updateCVNumber();
            });

            $("#cf_status").change(function() {
               if($(this).find(':selected').val() != 'Deposit') {
                   $("#bank_transaction_id").val('').attr({'required' : false, 'disabled' : true})
               }  else {
                   $("#bank_transaction_id").val('').attr({'required' : true, 'disabled' : false})
               }

                updateCVNumber();
            });

            $("#banks").change(function() {
                // updateCVNumber();
            });


            var x = {{ $cashFlow->Items->count() + 1 }};

            $(document).on('click', '#addNewItem', function(e) {
                var template = $("#template tr").clone();
                e.preventDefault();

                template.find('input, textarea, select').each(function() {
                    $(this).attr('name', $(this).attr('name').replace('[x]', '[' + x.toString() + ']'));
                });

                template.appendTo('#cashFlowItems tbody');
                x += 1;

                updateParticulars();
            });


            $(document).on('click', ".removeItem", function() {
                $(this).closest('tr').remove();
                updateTotalAmount();
                updateParticulars();
                updateTotalDRCR();
            })


            $(document).on('change, keyup', '#cashFlowItems .amount', function() {
                updateTotalAmount();
            });

            function updateTotalAmount() {
                var totalAmount = 0;

                $("#cashFlowItems .amount").each(function() {
                    var amount = parseFloat($(this).val()).toFixed(2);
                    if($(this).val().length == 0) {
                        amount = 0;
                    }
                    totalAmount += parseFloat(amount);
                });

                $("#totalAmount").text("Php "+ totalAmount.numberFormat(2));
            }

            $(document).on('change', "#cashFlowItems .cfItem", function() {
                updateParticulars();
            });

            $(document).on('keyup', "#cashFlowItems .cfDesc, #payee_name", function() {
                updateParticulars();
            });

            function updateParticulars()
            {
                if($("#mode").find(':selected').val() != 'Check') {
                    var items = '';
                    var descriptions = '';
                    var payeeName = $("#payee_name").val() || '';

                    $("#particulars").val('');

                    $("#cashFlowItems .cfItem").each(function() {
                        if(items == '') {
                            items = $(this).find(':selected').text();
                        } else {
                            items = items + ', ' + $(this).find(':selected').text();
                        }
                    });

                    $("#cashFlowItems .cfDesc").each(function() {
                        if(descriptions == '') {
                            descriptions = $(this).val() || '';
                        } else {
                            descriptions = descriptions + ', ' + $(this).val();
                        }
                    });

                    $("#particulars").val(items + ' ' + payeeName + ' ' + descriptions);
                }


            }

            // updateParticulars();


            function updateCashFlowItems(payeeType) {
                // console.log(cashFlowItems[payeeType]);

                $(".cfItem > option").remove();

                $.each(cashFlowItems[payeeType], function(index, value) {
                    $(".cfItem").append('<option value="'+ value.item +'">'+ value.item +'</option>');

                });
            }



            $(document).on('change', ".accountType", function() {
                var accountTitles = $.parseJSON($(this).find(':selected').attr('data-account-titles'));
                var tr = $(this).closest('tr');
                // console.log(accountTitles);


                $(this).closest('tr').find('.accountTitle > option').remove();
                $.each(accountTitles, function(index, value) {
                    tr.find(".accountTitle").append('<option value="'+ value.account_title +'">'+ value.account_title +'</option>');
                });
            });


            $(document).on('change', '.txn', function() {
                var txn = $(this).find(':selected').val();

                if(txn == 'Debit') {
                    $(this).closest('tr').find('.debit').val('').attr('disabled', false).attr('required', true);
                    $(this).closest('tr').find('.credit').val('').attr('disabled', true).attr('required', false);
                } else {
                    $(this).closest('tr').find('.debit').val('').attr('disabled', true).attr('required', false);
                    $(this).closest('tr').find('.credit').val('').attr('disabled', false).attr('required', true);
                }

                updateTotalDRCR();
            });

            $(document).on('keyup change', '.debit, .credit', function() {
                updateTotalDRCR();
            });

            function updateTotalDRCR() {
                var totalDebit = 0;
                var totalCredit = 0;

                $(".debit").each(function() {
                    totalDebit += parseFloat($(this).val().replace(/\,/g, '')) || 0;
                });

                $(".credit").each(function() {
                    totalCredit += parseFloat($(this).val().replace(/\,/g, '')) || 0;
                });

                $("#totalCredit").text(totalCredit.numberFormat(2));
                $("#totalDebit").text(totalDebit.numberFormat(2));
            }

            var x = {{ $cashFlow->Entries->count() + 1 }};
            $(document).on('click', '#addNewEntry', function(e) {
                var template = $("#entriesTemplate tr").clone();
                e.preventDefault();

                template.find('input, textarea, select').each(function() {
                    $(this).attr('name', $(this).attr('name').replace('[x]', '[' + x.toString() + ']'));
                });

                template.appendTo('#cashFlowEntries tbody');
                x += 1;
            });



            function updateCVNumber() {
                if($("#mode").find(':selected').val() == 'Check' && $("#cf_status").find(':selected').val() != 'Deposit') {
                    $("#cv_number").prop({'disabled': false, 'required': true});
                } else {
                    $("#cv_number").prop({'disabled': true, 'required': false});

                    $("#cv_number").val('');
                }
            }



            $(document).on('click', '#searchCheckNumber', function() {
                var checkNumber = $("#check_number").val().toString();
                var bank = $("#banks").val();

                $.get('{{ url('api/cash-flow/check-number') }}',
                    { check_number : checkNumber, bank : bank},
                    function(result) {
                        var json = result;
                        // var json = $.parseJSON(result);
                        console.log(result);

                        var checkNumberDifference = parseInt(checkNumber) - parseInt(json.check_number);

                        var cvNumber = parseInt(json.cv_number.replace('CV-', '')) + checkNumberDifference;

                        $("#cv_number").val("CV-" + cvNumber);

                        $("#cv_reference_number").val(json.cv_number);
                        $("#cv_reference_check").val(json.check_number);

                    }
                );
            });



            // updatePayeeName();

            $("#payeeType").change(function() {
                updatePayeeName();

                updateCashFlowItems($(this).find(':selected').val())
            });

            function updatePayeeName() {
                var payeeType = $("#payeeType").find(':selected').val();

                if(payeeType == 'Member') {
                    var url = "{{ url('api/users/members') }}";
                } else if(payeeType == 'Employee') {
                    var url = "{{ url('api/users/employees') }}";
                } else if(payeeType == 'Client') {
                    var url = "{{ url('api/clients') }}";
                } else if(payeeType == 'Services') {
                    var url = "{{ url('api/payee-names') }}";
                }



                $("#payeeName").select2({
                    placeholder: {
                        id: '-1', // the value of the option
                        id_number: "Search Payor name here..."
                    },
                    ajax: {
                        url: url,
                        dataType: 'json',
                        delay: 300,
                        data: function (params) {
                            return {
                                q: params.term, // search term
//                            page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            // parse the results into the format expected by Select2
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data, except to indicate that infinite
                            // scrolling can be used
                            params.page = params.page || 1;

                            // console.log();

                            if(payeeType == 'Member' || payeeType == 'Employee') {
                                var items = data.items;
                                $.each(items, function(index, value) {
                                    // console.log(value);
                                    value.id = value.name;
                                });
                            } else if(payeeType == 'Client') {
                                var items = data.items;
                                $.each(items, function(index, value) {
                                    // console.log(value);
                                    value.id = value.company_name;
                                    value.name = value.company_name;
                                });
                            } else if(payeeType == 'Services') {
                                var items = data.items;
                                $.each(items, function(index, value) {
                                    // console.log(value);
                                    value.id = value.payee_name;
                                    value.name = value.payee_name;
                                });
                            }




                            return {
                                results: data.items,
                                pagination: {
                                    more: (params.page * 30) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                    minimumInputLength: 3,
                    templateResult: formatRepo,
                    templateSelection: formatRepoSelection
                });

                // console.log();

            }

            var data = $.parseJSON('{!! json_encode(['id' => $cashFlow->payee_name, 'name' => $cashFlow->payee_name]) !!}');

            var studentSelect = $('#payeeName');
            // console.log(data);
            var option = new Option(data.name, data.id, true, true);
            studentSelect.append(option).trigger('change');
            // manually trigger the `select2:select` event
            studentSelect.trigger({
                type: 'select2:select',
                params: {
                    data: data
                }
            });

            function formatRepo (user) {
                if (user.loading) {
                    return user.name;
                }
//                console.log(user);

                var markup = "<span>"+ user.name +"</span>"

                return markup;
            }

            function formatRepoSelection (user) {
                return user.name;
            }


        })
    </script>
@endsection














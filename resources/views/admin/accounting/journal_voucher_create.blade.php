@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('journal_voucher')}}">Journal Voucher</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">

                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <form action="" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="jv_number">JV Number</label>
                                                <input class="form-control" type="text" id="jv_number" name="jv_number" required/>
                                                <br />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="form-control-label">Date</label>
                                                <div id="date" class="input-group btn-mmddyyyy ">
                                                    <input type="text" class="form-control" name="date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y') }}" required />
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="particulars">Particulars</label>
                                                <input class="form-control" type="text" id="particulars" name="particulars" required/>
                                                <br />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                            <label for="">Banks</label>
                                            <select name="bank" id="" class="form-control">
                                                @foreach(\App\Banks::orderBy('bank_account')->get() as $bank)
                                                    <option value="{{ $bank->bank_account }}">{{ $bank->bank_account }}</option>
                                                @endforeach
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="col-md-12">
                                        <h2 class="content-heading">Entry</h2>
                                    </div>

                                    <div class="col-md-12">
                                        <table id="journalVoucherItems" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Account Type</th>
                                                <th>Account Title</th>
                                                <th width="200">TXN</th>
                                                <th width="200">Debit</th>
                                                <th width="200">Credit</th>
                                                <th></th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div class="">
                                                        <select name="item[0][account_type]" id="" class="form-control accountType">
                                                            @foreach($accountTypes as $accountType)
                                                                <option value="{{ $accountType->account_type }}" data-account-titles='{{ $accountType->AccountTitles }}'>{{ $accountType->account_type }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="">
                                                        <select name="item[0][account_title]" id="" class="form-control accountTitle">
                                                            @foreach($accountTypes->first()->AccountTitles as $accountTitle)
                                                                <option value="{{ $accountTitle->account_title }}">{{ $accountTitle->account_title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="">
                                                        <select name="item[0][txn]" class="form-control txn">
                                                            <option value="Debit">DR</option>
                                                            <option value="Credit">CR</option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td><input type="text" class="form-control debit" name="item[0][debit]" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" required></td>
                                                <td><input type="text" class="form-control credit" name="item[0][credit]" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" disabled></td>
                                                <td class="text-center"><a class="btn btn-sm removeItem btn-danger"><i class="fa fa-remove"></i></a></td>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Total</th>
                                                <th></th>
                                                <th></th>
                                                <th id="totalDebit">0.00</th>
                                                <th id="totalCredit">0.00</th>
                                                <th></th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>

                                    <div class="col-md-6">
                                        <a href="#" id="addNewItem" class="btn btn-primary push-5-r push-10"><i class="fa fa-plus"></i> Add New Item</a>
                                    </div>

                                    <div class="col-md-12">
                                        <br>
                                        <br>
                                        <div class="col-md-2">
                                            Prepared By:
                                        </div>
                                        <div class="col-md-10">
                                            {{ $me->Profile->namefl() }}
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <br />
                                        <a href="{{ url("/admin/accounting/journal_voucher") }}" class="btn btn-danger push-5-r push-10" ><i class="fa fa-times"></i> Cancel</a>
                                        <button class="btn btn-success push-5-r push-10"><i class="fa fa-check"></i> Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="hidden" id="template">
        <table>
            <tr>
                <td>
                    <div class="">
                        <select name="item[x][account_type]" id="" class="form-control accountType">
                            @foreach($accountTypes as $accountType)
                                <option value="{{ $accountType->account_type }}" data-account-titles='{{ $accountType->AccountTitles }}'>{{ $accountType->account_type }}</option>
                            @endforeach
                        </select>
                    </div>
                </td>
                <td>
                    <div class="">
                        <select name="item[x][account_title]" id="" class="form-control accountTitle">
                            @foreach($accountTypes->first()->AccountTitles as $accountTitle)
                                <option value="{{ $accountTitle->account_title }}">{{ $accountTitle->account_title }}</option>
                            @endforeach
                        </select>
                    </div>
                </td>
                <td>
                    <div class="">
                        <select name="item[x][txn]" class="form-control txn">
                            <option value="Debit">DR</option>
                            <option value="Credit">CR</option>
                        </select>
                    </div>
                </td>
                <td><input type="text" class="form-control debit" name="item[x][debit]" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                <td><input type="text" class="form-control credit" name="item[x][credit]" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" disabled></td>
                <td class="text-center"><a class="btn btn-sm removeItem btn-danger"><i class="fa fa-remove"></i></a></td>
            </tr>
        </table>
    </div>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#journal_voucher").dataTable({
                "order": [],
                'pageLength': 100,
                "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ]
            });

            $(document).on('change', ".accountType", function() {
                var accountTitles = $.parseJSON($(this).find(':selected').attr('data-account-titles'));
                var tr = $(this).closest('tr');
                // console.log(accountTitles);


                $(this).closest('tr').find('.accountTitle > option').remove();
                $.each(accountTitles, function(index, value) {
                    tr.find(".accountTitle").append('<option value="'+ value.account_title +'">'+ value.account_title +'</option>');
                });
            });


            $(document).on('change', '.txn', function() {
                var txn = $(this).find(':selected').val();

                if(txn == 'Debit') {
                    $(this).closest('tr').find('.debit').val('').attr('disabled', false).attr('required', true);
                    $(this).closest('tr').find('.credit').val('').attr('disabled', true).attr('required', false);
                } else {
                    $(this).closest('tr').find('.debit').val('').attr('disabled', true).attr('required', false);
                    $(this).closest('tr').find('.credit').val('').attr('disabled', false).attr('required', true);
                }

                updateTotalDRCR();
            });

            $(document).on('keyup change', '.debit, .credit', function() {
                updateTotalDRCR();
            });

            var x = 1;
            $(document).on('click', '#addNewItem', function(e) {
                var template = $("#template tr").clone();
                e.preventDefault();

                template.find('input, textarea, select').each(function() {
                    $(this).attr('name', $(this).attr('name').replace('[x]', '[' + x.toString() + ']'));
                });

                template.appendTo('#journalVoucherItems tbody');
                x += 1;
            });

            $(document).on('click', ".removeItem", function() {
                event.preventDefault();

                $(this).closest('tr').remove();
            });

            function updateTotalDRCR() {
                var totalDebit = 0;
                var totalCredit = 0;

                $(".debit").each(function() {
                    totalDebit += parseFloat($(this).val().replace(/\,/g, '')) || 0;
                });

                $(".credit").each(function() {
                    totalCredit += parseFloat($(this).val().replace(/\,/g, '')) || 0;
                });

                $("#totalCredit").text(totalCredit.numberFormat(2));
                $("#totalDebit").text(totalDebit.numberFormat(2));
            }
        })
    </script>
@endsection

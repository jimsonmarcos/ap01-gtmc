@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('check_request')}}">Check Request</a></li>
        <li><a class="link-effect" href="{{ route('for_request')}}">For Request</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            {{-- New Design --}}
                            <div class="row">

                                <div class="col-md-12">
                                    <br />
                                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                                        <li class="nav-item">
                                            <a id='loans' data-toggle="tab" class="nav-link active" href="#loans_table"> Loans</a>
                                        </li>
                                        <li class="nav-item">
                                            <a id='final' data-toggle="tab" class="nav-link" href="#final_pay_table"> Final Pay</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="tab-content">

                                    <div id="loans_table" class="tab-pane in active">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <br />
                                                    <table id="loansTable" class="table table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th>CLA Number</th>
                                                            <th>ID Number</th>
                                                            <th>Complete Name</th>
                                                            <th>Cost Center</th>
                                                            <th>Loan Type</th>
                                                            <th>Overdue Amount</th>
                                                            <th>Outstanding Balance</th>
                                                            <th>Granted Loan Amount</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($loans as $loan)
                                                            <tr>
                                                                <td>{{ $loan->cla_number }}</td>
                                                                <td>{{ $loan->id_number }}</td>
                                                                <td>{{ $loan->name }}</td>
                                                                <td>{{ $loan->cost_center }}</td>
                                                                <td>{{ $loan->type }}</td>
                                                                <td>{{ $loan->total_overdue_amount }}</td>
                                                                <td>{{ $loan->outstanding_balance }}</td>
                                                                <td>{{ $loan->granted_loan_amount }}</td>
                                                                <td><a href="{{ url("admin/accounting/check_request/create?source=loan&id={$loan->id}") }}" class="btn btn-primary btn-sm">REQUEST</a></td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="final_pay_table" class="tab-pane fade">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <br />
                                                    <table id="final_pay" class="table table-bordered table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th>Employee ID Number</th>
                                                            <th>Employee Name</th>
                                                            <th>Cost Center</th>
                                                            <th>Net Pay</th>
                                                            <th>Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($finalPays as $finalPay)
                                                        <tr>
                                                            <td>{{ $finalPay->Profile->id_number }}</td>
                                                            <td>{{ $finalPay->Profile->namelfm() }}</td>
                                                            <td>{{ $finalPay->Profile->CostCenter->cost_center }}</td>
                                                            <td>{{ number_format($finalPay->net_payable, 2) }}</td>
                                                            <td>
                                                                <a href="{{ url("admin/accounting/check_request/create?source=final_pay&id={$finalPay->id}") }}" class="btn btn-info btn-xs">REQUEST</a>
                                                            </td>
                                                        </tr>
                                                        @endforeach

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            {{-- End New Design --}}
                            {{-- Previous design --}}
                            {{--<div class="row">--}}

                                {{--<div class="col-md-12">--}}
                                    {{--<h2 class="content-heading">For Request</h2>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="col-xs-12" for="cash_flow_item">Cash Flow Item</label>--}}
                                        {{--<div class="col-xs-12">--}}
                                            {{--<select class="form-control" id="cash_flow_item" name="cash_flow_item" size="1">--}}
                                                {{--<option value="CASH_PAYROLL">Cash Payroll</option>--}}
                                                {{--<option value="BILLING_INVOICE">Billing Invoice</option>--}}
                                                {{--<option value="MEMBERSHIP_TERMINATION">Memebership Termination</option>--}}
                                                {{--<option value="BEREAVEMENT_ASSISTANCE">Bereavement Assistance</option>--}}
                                                {{--<option value="SSS">SSS Maternity Benefir Claims</option>--}}
                                                {{--<option value="LAST_PAY">Last Pay</option>--}}
                                                {{--<option value="GTMC_LOAN">GTMC LOAN</option>--}}
                                                {{--<option value="MONOBLOCKS">Monoblocks</option>--}}
                                                {{--<option value="PETTY_CASH">Petty Cash</option>--}}
                                                {{--<option value="PENALTY">Penalty</option>--}}
                                                {{--<option value="RENTALS">Rentals</option>--}}
                                                {{--<option value="BILLS_PAYMENT">Bills Payment</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}


                                {{--<div class="col-md-12">--}}
                                    {{--<div class="block">--}}
                                        {{--<div class="block-content">--}}
                                            {{--<div id="DataTables_Table_1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">--}}
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-sm-6">--}}
                                                        {{--<div class="dataTables_length" id="DataTables_Table_1_length"><label>--}}
                                                                {{--<select name="DataTables_Table_1_length"--}}
                                                                        {{--aria-controls="DataTables_Table_1"--}}
                                                                        {{--class="form-control">--}}
                                                                    {{--<option value="5">5</option>--}}
                                                                    {{--<option value="10">10</option>--}}
                                                                    {{--<option value="15">15</option>--}}
                                                                    {{--<option value="20">20</option>--}}
                                                                {{--</select>--}}
                                                            {{--</label>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-sm-6">--}}
                                                        {{--<div id="DataTables_Table_1_filter" class="dataTables_filter">--}}
                                                            {{--<label>Search:<input type="search" class="form-control" placeholder="" aria-controls="DataTables_Table_1"></label>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-sm-12">--}}
                                                        {{--<table class="table table-bordered table-striped js-dataTable-full dataTable no-footer"--}}
                                                               {{--id="DataTables_Table_1" role="grid" aria-describedby="DataTables_Table_1_info">--}}
                                                            {{--<thead>--}}
                                                            {{--<tr role="row">--}}
                                                                {{--<th class="text-center sorting_asc"--}}
                                                                    {{--tabindex="0"--}}
                                                                    {{--aria-controls="DataTables_Table_1"--}}
                                                                    {{--rowspan="1" colspan="1" aria-sort="ascending"--}}
                                                                    {{--aria-label=": activate to sort column descending">Reference Number</th>--}}

                                                                {{--<th class="sorting"--}}
                                                                    {{--tabindex="0"--}}
                                                                    {{--aria-controls="DataTables_Table_1"--}}
                                                                    {{--rowspan="1" colspan="1"--}}
                                                                    {{--aria-label="Name: activate to sort column ascending">Cash Flow Item</th>--}}

                                                                {{--<th class="hidden-xs sorting"--}}
                                                                    {{--tabindex="0" aria-controls="DataTables_Table_1"--}}
                                                                    {{--rowspan="1" colspan="1"--}}
                                                                    {{--aria-label="Email: activate to sort column ascending">Payee</th>--}}

                                                                {{--<th class="hidden-xs sorting" tabindex="0" aria-controls="DataTables_Table_1"--}}
                                                                    {{--rowspan="1" colspan="1"--}}
                                                                    {{--aria-label="Access: activate to sort column ascending">Particulars</th>--}}

                                                                {{--<th class="text-center sorting_disabled" rowspan="1"--}}
                                                                    {{--colspan="1" aria-label="Actions">Amount</th>--}}


                                                            {{--</tr>--}}
                                                            {{--</thead>--}}
                                                            {{--<tbody>--}}
                                                            {{--<tr role="row" class="odd">--}}
                                                                {{--<td class="font-w600">--}}
                                                                    {{--<a href="#">1</a>--}}
                                                                {{--</td>--}}
                                                                {{--<td class="font-w600"></td>--}}
                                                                {{--<td class="font-w600"></td>--}}
                                                                {{--<td class="font-w600">--}}
                                                                    {{--<span class="label label-danger">YES</span>--}}
                                                                {{--</td>--}}
                                                                {{--<td class="font-w600"></td>--}}

                                                            {{--</tr>--}}
                                                            {{--</tbody>--}}
                                                        {{--</table>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-sm-6">--}}
                                                        {{--<div class="dataTables_info" id="DataTables_Table_1_info"--}}
                                                             {{--role="status" aria-live="polite">Showing <strong>1</strong>-<strong>10</strong> of <strong>40</strong>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-sm-6">--}}
                                                        {{--<div class="dataTables_paginate paging_simple_numbers"--}}
                                                             {{--id="DataTables_Table_1_paginate">--}}
                                                            {{--<ul class="pagination">--}}
                                                                {{--<li class="paginate_button previous disabled"--}}
                                                                    {{--aria-controls="DataTables_Table_1" tabindex="0"--}}
                                                                    {{--id="DataTables_Table_1_previous">--}}
                                                                    {{--<a href="#"><i class="fa fa-angle-left"></i></a>--}}
                                                                {{--</li>--}}

                                                                {{--<li class="paginate_button active" aria-controls="DataTables_Table_1"--}}
                                                                    {{--tabindex="0">--}}
                                                                    {{--<a href="#">1</a>--}}
                                                                {{--</li>--}}

                                                                {{--<li class="paginate_button " aria-controls="DataTables_Table_1"--}}
                                                                    {{--tabindex="0">--}}
                                                                    {{--<a href="#">2</a>--}}
                                                                {{--</li>--}}

                                                                {{--<li class="paginate_button " aria-controls="DataTables_Table_1"--}}
                                                                    {{--tabindex="0">--}}
                                                                    {{--<a href="#">3</a>--}}
                                                                {{--</li>--}}

                                                                {{--<li class="paginate_button " aria-controls="DataTables_Table_1"--}}
                                                                    {{--tabindex="0">--}}
                                                                    {{--<a href="#">4</a>--}}
                                                                {{--</li>--}}

                                                                {{--<li class="paginate_button next" aria-controls="DataTables_Table_1"--}}
                                                                    {{--tabindex="0" id="DataTables_Table_1_next">--}}
                                                                    {{--<a href="#"><i class="fa fa-angle-right"></i></a>--}}
                                                                {{--</li>--}}
                                                            {{--</ul>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{-- End previous design --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('.nav-tabs a').click(function() {
                $(this).tab('show').addClass('active');
                if($(this).attr('id') == 'loans'){
                    $('#final').removeClass('active');
                } else {
                    $('#loans').removeClass('active');
                }
            });

            $('#final_pay').dataTable({
                "pageLength": 100,
                "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ]
            });

            $('#loansTable').dataTable({
                "pageLength": 100,
                "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ]
            });

            // $('#loans').dataTable();
        })
    </script>
@endsection

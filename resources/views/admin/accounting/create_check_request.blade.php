@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('check_request')}}">Check Request</a></li>
        <li><a class="link-effect" href="{{ route('create_check_request')}}">Create Check Request</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12 text-right hidden">
                                    <i class="fa fa-print fa-2x" aria-hidden="true"></i>
                                </div>

                                <form action="" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="col-md-6">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="" for="payee_type">Payee Type</label>
                                                @if(!empty($payee_type))
                                                    <input type="text" class="form-control" name="payee_type" value="{{ $payee_type }}" readonly>
                                                @else
                                                    <select class="form-control" id="payee_type" name="payee_type" size="1" required>
                                                        <option value="Member" @if($payeeType == 'Member') selected @endif>Member</option>
                                                        <option value="Employee" @if($payeeType == 'Employee') selected @endif>Employee</option>
                                                        <option value="Client" @if($payeeType == 'Client') selected @endif>Client</option>
                                                        <option value="Services" @if($payeeType == 'Services') selected @endif>Services</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="payee_name">Payee Name</label>
                                                @if(!empty($payee_name))
                                                    <input type="text" class="form-control" name="payee_name" value="{{ $payee_name }}" readonly>
                                                @else
                                                    <select class="select2 form-control" id="payee_name" name="payee_name" size="1" required>
                                                        <option value=""></option>
                                                        @if($payeeType == 'Member')
                                                            @foreach(\App\Profiles::where('membership_category', 'Member')->orderBy('first_name')->get() as $member)
                                                                <option value="{{ $member->namefl() }}" data-address="{{ $member->CostCenter->Company->address }}">{{ $member->namefl() }}</option>
                                                            @endforeach
                                                        @elseif($payeeType == 'Employee')
                                                            @foreach(\App\Profiles::where('membership_category', 'Employee')->orderBy('first_name')->get() as $employee)
                                                                <option value="{{ $employee->namefl() }}" data-address="<?php /* TODO {{ $employee->Profile->Compensation->PayrollGroup->Company->address }} */ ?>">{{ $employee->namefl() }}</option>
                                                            @endforeach
                                                        @elseif($payeeType == 'Client')
                                                            @foreach(\App\Companies::orderBy('company_code')->get() as $client)
                                                                <option value="{{ $client->company_name }}" data-address="{{ $client->address }}">{{ $client->company_name }}</option>
                                                            @endforeach
                                                        @else
                                                            @foreach(\App\PayeeNames::orderBy('payee_name')->get() as $service)
                                                                <option value="{{ $service->payee_name }}" data-address="">{{ $service->payee_name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                @endif

                                                <br />
                                            </div>
                                        </div>

                                        {{--<div class="col-md-12">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label for="payee_address">Payee Address</label>--}}
                                                {{--<textarea name="payee_address" id="payee_address" rows="3"--}}
                                                          {{--class="form-control"></textarea>--}}
                                                {{--<br />--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-12" for="request_date">Request Date</label>
                                                <div class="col-md-12">
                                                    <div class='input-group btn-dtp-request-date'>
                                                    <input type='text' class="form-control" name="request_date" value="{{ date('m/d/Y') }}" placeholder="mm/dd/yyyy" required />
                                                    <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <h2 class="content-heading">Summary / Schedule of Expenditures</h2>
                                </div>

                                <div id="checkRequestItems" class="col-md-12">
                                    <table class="table table-striped table-bordered table-condensed">
                                        <thead>
                                            <tr>
                                                <th>ITEM</th>
                                                <th>DESCRIPTION</th>
                                                <th width="200">REFERENCE NUMBER</th>
                                                <th width="200">AMOUNT</th>
                                                <th></th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>
                                                    @if(!empty($item))
                                                        <input type="text" class="form-control" name="item[0][cash_flow_item]" value="{{ $item }}" readonly>
                                                    @else
                                                        <select name="item[0][cash_flow_item]" id="" class="form-control cashFlowItem" required>
                                                            @foreach(\App\CashFlowItems::where('payee_type', $payeeType)->orderBy('item')->get() as $cashFlowItem)
                                                                <option value="{{ $cashFlowItem->item }}">{{ $cashFlowItem->item }}</option>
                                                            @endforeach
                                                        </select>
                                                    @endif
                                                </td>
                                                <td>
                                                    <textarea name="item[0][description] " id="" rows="3"
                                                              class="form-control description">{{ !empty($description) ? $description : '' }}</textarea>
                                                </td>
                                                <td>
                                                    <div><input type="text" class="form-control" name="item[0][reference_number]" value="{{ !empty($reference_number) ? $reference_number : '' }}" autocomplete="off"></div>
                                                    <div><label class="css-input css-checkbox css-checkbox-primary">
                                                            <input type="checkbox" class="referenceNumberRequired" name="item[0][is_required]" autocomplete="off"><span></span> Required Reference #
                                                        </label>
                                                    </div>
                                                </td>
                                                <td><input type="text" class="form-control amount" name="item[0][amount]" value="{{ !empty($amount) ? $amount : '' }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" autocomplete="off" required></td>
                                                <td class="text-center">
                                                    @if(empty($item)) <button class="btn btn-xs btn-danger removeItem" type="button"><i class="fa fa-remove"></i></button> @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5">
                                                    <div><label for="">Particulars</label></div>
                                                    <textarea name="item[0][particulars]" class="form-control" id="" rows="3">{{ !empty($item) ? $item : '' }} {{ !empty($payee_name) ? $payee_name : '' }} {{ !empty($description) ? $description : '' }}</textarea>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                @if(empty($source))
                                <div class="col-md-12">
                                    <a href="#" id="addNewItem" class="btn btn-primary push-5-r push-10"><i class="fa fa-plus"></i> Add New Item</a>
                                </div>
                                @else
                                    <input type="hidden" name="source" value="{{ $source }}">
                                    <input type="hidden" name="source_id" value="{{ $source_id }}">
                                @endif

                                <div class="col-md-12">
                                    <hr>
                                    <div class="row">

                                        <div class="col-md-2">
                                            <p style="padding-top: 10px;">Total Amount</p>
                                            <input type="hidden" name="total_amount" id="totalAmountTxt" value="{{ !empty($amount) ? $amount : '0' }}">
                                        </div>
                                        <div class="col-md-4 text-center">
                                            <p id="totalAmount" style="border: 1px blue solid; border-radius: 50px; padding: 10px;font-weight: bold;">Php {{ !empty($amount) ? number_format($amount, 2) : '0.00' }}</p>
                                        </div>
                                        <div class="col-md-12"></div>

                                        <div class="col-md-2">
                                            Amount In Words
                                        </div>
                                        <div class="col-md-6">
                                           <span id="amountIntoWords"></span>
                                        </div>

                                    </div>
                                    <br />
                                </div>

                                <div class="col-md-6">
                                    <div class="row">

                                        <div class="col-md-3">
                                            Requested By:
                                        </div>
                                        <div class="col-md-6">
                                            <label>{{ $me->Profile->namefl()  }}</label>
                                            {{--<select name="requested_by" id="" class="form-control">--}}
                                                {{--@foreach($signatories as $user)--}}
                                                    {{--<option value="{{ $user->id }}">{{ $user->name }}</option>--}}
                                                {{--@endforeach--}}
                                            {{--</select>--}}
                                        </div>

                                        <div class="col-md-12"><br></div>

                                        <div class="col-md-3">
                                            Noted By:
                                        </div>
                                        <div class="col-md-6">
                                            <select name="noted_by" id="" class="form-control">
                                                @foreach($signatories as $user)
                                                    <option value="{{ $user->id }}">{{ "{$user->Profile->first_name} {$user->Profile->last_name}" }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-12"><br></div>

                                        <div class="col-md-3">
                                            Approved By:
                                        </div>
                                        <div class="col-md-6">
                                            <select name="approved_by" id="" class="form-control">
                                                @foreach($signatories as $user)
                                                    <option value="{{ $user->id }}">{{ "{$user->Profile->first_name} {$user->Profile->last_name}" }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                    <div class="col-md-6"></div>

                                    <div class="col-md-12 text-right">
                                        <br />
                                        <button class="btn btn-success push-5-r push-10"><i class="fa fa-save"></i> Submit</button>
                                        <a href="{{ URL::previous() }}" class="btn btn-danger push-5-r push-10"><i class="fa fa-close"></i> Back</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div class="hidden" id="template">
        <table>
            <tr>
                <td>
                    <select name="item[x][cash_flow_item]" id="" class="form-control cashFlowItem" required>
                        @foreach(\App\CashFlowItems::where('status', 'Pending')->where('payee_type', $payeeType)->orderBy('item')->get() as $cashFlowItem)
                            <option value="{{ $cashFlowItem->id }}">{{ $cashFlowItem->item }}</option>
                        @endforeach
                    </select>
                <td>
                    <textarea name="item[x][description]" id="" rows="3" class="form-control description"></textarea>
                </td>
                <td>
                    <div><input type="text" class="form-control" name="item[x][reference_number]" autocomplete="off"></div>
                    <div><label class="css-input css-checkbox css-checkbox-primary">
                            <input type="checkbox" class="referenceNumberRequired" name="item[x][is_required]" autocomplete="off"><span></span> Required Reference #
                        </label>
                    </div>
                </td>
                <td><input type="text" class="form-control amount" name="item[x][amount]" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" autocomplete="off" required></td>
                <td class="text-center"><button class="btn btn-xs btn-danger removeItem" type="button"><i class="fa fa-remove"></i></button></td>td>
            </tr>
            <tr>
                <td colspan="5">
                    <div><label for="">Particulars</label></div>
                    <textarea name="item[x][particulars]" class="form-control" id="" rows="3"></textarea>
                </td>
            </tr>
        </table>
    </div>
@endsection

@section('extendedscript')
    <script>
        var base = '{{ url('admin/accounting/check_request/create') }}';
        var x = 1;
        $(document).ready(function() {
            $(document).on('change', '#payee_type', function() {
                var payee_type = $(this).val();
                location.href = base + '?payee_type=' + payee_type;
            });

            $('.select2').select2();

            // DATEPICKER
            $('.btn-dtp-request-date').datetimepicker({
                format: "MM/DD/YYYY"
                //format: "Y-m-d g:i:s",
            });

            $("#payee_name").on("select2:select", function (evt) {
                var data = evt.params.data;
                var address = data.element.dataset.address;
                if(address != '') {
                    $("#payee_address").val(address);
                }
            });


            $(document).on('click', '#addNewItem', function(e) {
                var template = $("#template tr").clone();
                e.preventDefault();

                template.find('input, textarea, select').each(function() {
                   $(this).attr('name', $(this).attr('name').replace('[x]', '[' + x.toString() + ']'));
                });

                template.appendTo('#checkRequestItems tbody');
                x += 1;
            });


            $(document).on('click', ".removeItem", function() {
                $(this).closest('tr').next().remove();
                $(this).closest('tr').remove();
                updateTotalAmount();
            });


            $(document).on('change, keyup', '#checkRequestItems .amount', function() {
                updateTotalAmount();
            });

            $(document).on('change, keyup', ' #checkRequestItems .description, #checkRequestItems .cashFlowItem', function() {
                var tr = $(this).closest('tr');
                var item = tr.find('.cashFlowItem :selected').text() || '';
                var description = tr.find('.description').val() || '';
                var payeeName = $("#payee_name").find(":selected").text() || '';

                var particulars = item;
                if($.trim(payeeName) != '') {
                    particulars = particulars + ' ' + payeeName;
                }
                if($.trim(description) != '') {
                    particulars = particulars + ' ' + description;
                }

                tr.next().find('textarea').val(particulars);
            });

            updateTotalAmount();

            function updateTotalAmount() {
                var totalAmount = 0;
                var amountIntoWords = '';

                if($("#checkRequestItems .amount").length > 0) {
                    $("#checkRequestItems .amount").each(function() {
                        var amount = parseFloat($(this).val()).toFixed(2) || 0;
                        if($(this).val().length == 0) {
                            amount = 0;
                        }
                        totalAmount += parseFloat(amount);
                    });

                    var pieces = totalAmount.toString().split('.');

                    var decimal = '';
                    var wholeNumber = 0;

                    if(pieces.length == 1) {
                        var wholeNumber = parseFloat(pieces[0]);
                    } else if(pieces.length == 2) {
                        // console.log(pieces);
                        var wholeNumber = parseFloat(pieces[0]);


                        // console.log(pieces[1]);

                        if(pieces[1].length == 1) {
                            // decimal = decimal.numberFormat(2);
                            var decimal = " and "+ pieces[1] + "0/100";
                        } else {
                            var decimal = " and "+ parseInt(pieces[1]) + "/100";
                        }


                    }

                    // console.log(pieces.length);

                    // console.log(totalAmount.numberFormat(2).replace(/\,/g, ''));

                    var amountIntoWords = chunk(wholeNumber.numberFormat(2).replace(/\,/g, ''))
                        .map(inEnglish)
                        .map(appendScale)
                        .filter(isTruthy)
                        .reverse()
                        .join(" ");

                    if(amountIntoWords != '') {
                        amountIntoWords = amountIntoWords + " Pesos " + decimal;
                    }


                }

                $("#totalAmount").text("Php "+ totalAmount.numberFormat(2) +"");
                $("#totalAmountTxt").val(totalAmount);

                $("#amountIntoWords").text(amountIntoWords);

            }
        });



    </script>
@endsection

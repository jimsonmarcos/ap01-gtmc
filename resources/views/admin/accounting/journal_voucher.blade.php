@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('journal_voucher')}}">Journal Voucher</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">

                    <div class="col-sm-12 text-right">
                        <a href="{{ route('create_journal_voucher') }}" class="btn btn-info push-5-r push-10">Create</a>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<div class="col-xs-12">--}}
                                                {{--<label for="jv_number">JV Number</label>--}}
                                                {{--<input class="form-control" type="text" id="jv_number" name="jv_number" required/>--}}
                                                {{--<br />--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<div class="col-xs-12">--}}
                                                {{--<label class="form-control-label">Date</label>--}}
                                                {{--<div id="date" class="input-group btn-mmddyyyy ">--}}
                                                    {{--<input type="text" class="form-control" name="date" placeholder="mm/dd/yyyy" required />--}}
                                                    {{--<span class="input-group-addon"><span class="fa fa-calendar"></span></span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<div class="col-xs-12">--}}
                                                {{--<label for="particulars">Particulars</label>--}}
                                                {{--<input class="form-control" type="text" id="particulars" name="particulars" required/>--}}
                                                {{--<br />--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="col-md-12">
                                        <div class="block">
                                            <div class="block-content">
                                                <table id="journal_voucher" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>JV Number</th>
                                                            <th>Date</th>
                                                            <th>Particulars</th>
                                                            <th>DEBIT</th>
                                                            <th>CREDIT</th>
                                                            <th width="150">ACTION</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($journalVouchers as $journalVoucher)
                                                            <tr>
                                                                <td>{{ $journalVoucher->jv_number }}</td>
                                                                <td>{{ date('m/d/Y', strtotime($journalVoucher->date)) }}</td>
                                                                <td>{{ $journalVoucher->particulars }}</td>
                                                                <td class="text-right">{{ $journalVoucher->Items->sum('debit') }}</td>
                                                                <td class="text-right">{{ $journalVoucher->Items->sum('credit') }}</td>
                                                                <td>
                                                                    <a href="{{ url("/admin/accounting/journal_voucher/print/{$journalVoucher->id}") }}" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Print</a>
                                                                    <a href="{{ url("/admin/accounting/journal_voucher/edit/{$journalVoucher->id}") }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#journal_voucher").dataTable({
                "order": [],
                'pageLength': 100,
                "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ]
            });
        })
    </script>
@endsection

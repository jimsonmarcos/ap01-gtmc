@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('check_request')}}">Check Request</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="{{ route('create_check_request') }}" class="btn btn-primary push-5-r push-10">
                                        <i class="fa fa-ticket" aria-hidden="true"></i> Request
                                    </a>

                                    <a href="{{ route('manage_items') }}" class="btn btn-primary push-5-r push-10">
                                        <i class="fa fa-folder-open" aria-hidden="true"></i> Items
                                    </a>
                                </div>

                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('check_request') }}">
                                                <span class="badge badge-pill badge-default">100</span> All</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('check_request_pending') }}">
                                                <span class="badge badge-pill badge-warning">20</span> Pending</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('check_request_approved') }}">
                                                <span class="badge badge-pill badge-success">70</span> Approved</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('check_request_cancelled') }}">
                                                <span class="badge badge-pill badge-danger">10</span> Cancelled</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12 text-right">
                                    <br />
                                    <!-- Trigger the modal with a button -->
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">
                                        Advanced Search
                                    </button>
                                    <div class="modal fade" id="myModal" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Advanced Search</h4>
                                                </div>
                                                <div class="modal-body text-left">
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-xs-12" for="column_name">Column Name</label>
                                                                <div class="col-xs-12">
                                                                    <select class="form-control" id="column_name" name="column_name" size="1">
                                                                        <option value="payee_type">Payee Type</option>
                                                                        <option value="payee_name">Payee Name</option>
                                                                        <option value="payee_address">Payee Address</option>
                                                                        <option value="crf_number">CRF Number</option>
                                                                        <option value="item">Item</option>
                                                                        <option value="reference_number">Reference Number</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-xs-12">
                                                                    <label for="colum_name_text">Column Name Text</label>
                                                                    <input class="form-control" type="text" id="colum_name_text" name="colum_name_text" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="col-xs-12" for="column_date">Column Date</label>
                                                                <div class="col-xs-12">
                                                                    <select class="form-control" id="column_date" name="column_date" size="1">
                                                                        <option value="request_date">Request Date</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <div class="col-xs-12">
                                                                    <label class="form-control-label">Column Date From</label>
                                                                    <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                                        <input type="text" class="form-control" name="column_date_from" placeholder="mm/dd/yyyy" required="">
                                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <div class="col-xs-12">
                                                                    <label class="form-control-label">Column Date To</label>
                                                                    <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                                        <input type="text" class="form-control" name="column_date_to" placeholder="mm/dd/yyyy" required="">
                                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-success" data-dismiss="modal">View</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="block">
                                        <div class="block-content">
                                            <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="dataTables_length" id="DataTables_Table_1_length"><label>
                                                                <select name="DataTables_Table_1_length"
                                                                        aria-controls="DataTables_Table_1"
                                                                        class="form-control">
                                                                    <option value="5">5</option>
                                                                    <option value="10">10</option>
                                                                    <option value="15">15</option>
                                                                    <option value="20">20</option>
                                                                </select>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div id="DataTables_Table_1_filter" class="dataTables_filter">
                                                            <label>Search:<input type="search" class="form-control" placeholder="" aria-controls="DataTables_Table_1"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <table class="table table-bordered table-striped js-dataTable-full dataTable no-footer"
                                                               id="DataTables_Table_1" role="grid" aria-describedby="DataTables_Table_1_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="text-center sorting_asc"
                                                                    tabindex="0"
                                                                    aria-controls="DataTables_Table_1"
                                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                                    aria-label=": activate to sort column descending"></th>

                                                                <th class="sorting"
                                                                    tabindex="0"
                                                                    aria-controls="DataTables_Table_1"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label="Name: activate to sort column ascending">CRF Number</th>

                                                                <th class="hidden-xs sorting"
                                                                    tabindex="0" aria-controls="DataTables_Table_1"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label="Email: activate to sort column ascending">Payee</th>

                                                                <th class="hidden-xs sorting" tabindex="0" aria-controls="DataTables_Table_1"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label="Access: activate to sort column ascending">Particulars</th>

                                                                <th class="text-center sorting_disabled" rowspan="1"
                                                                    colspan="1" aria-label="Actions">Amount</th>

                                                                <th class="text-center sorting_disabled" rowspan="1"
                                                                    colspan="1" aria-label="Actions">Status</th>

                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row" class="odd">
                                                                <td class="text-center sorting_1">
                                                                    1
                                                                </td>
                                                                <td class="font-w600"></td>
                                                                <td class="font-w600"></td>
                                                                <td class="font-w600">
                                                                    <span class="label label-danger">YES</span>
                                                                </td>
                                                                <td class="font-w600"></td>
                                                                <td class="font-w600"></td>

                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="dataTables_info" id="DataTables_Table_1_info"
                                                             role="status" aria-live="polite">Showing <strong>1</strong>-<strong>10</strong> of <strong>40</strong>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="dataTables_paginate paging_simple_numbers"
                                                             id="DataTables_Table_1_paginate">
                                                            <ul class="pagination">
                                                                <li class="paginate_button previous disabled"
                                                                    aria-controls="DataTables_Table_1" tabindex="0"
                                                                    id="DataTables_Table_1_previous">
                                                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                                                </li>

                                                                <li class="paginate_button active" aria-controls="DataTables_Table_1"
                                                                    tabindex="0">
                                                                    <a href="#">1</a>
                                                                </li>

                                                                <li class="paginate_button " aria-controls="DataTables_Table_1"
                                                                    tabindex="0">
                                                                    <a href="#">2</a>
                                                                </li>

                                                                <li class="paginate_button " aria-controls="DataTables_Table_1"
                                                                    tabindex="0">
                                                                    <a href="#">3</a>
                                                                </li>

                                                                <li class="paginate_button " aria-controls="DataTables_Table_1"
                                                                    tabindex="0">
                                                                    <a href="#">4</a>
                                                                </li>

                                                                <li class="paginate_button next" aria-controls="DataTables_Table_1"
                                                                    tabindex="0" id="DataTables_Table_1_next">
                                                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

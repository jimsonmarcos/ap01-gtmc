@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('check_request')}}">Check Request</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="{{ route('create_check_request') }}" class="btn btn-primary push-5-r push-10">
                                        <i class="fa fa-ticket" aria-hidden="true"></i> Create
                                    </a>

                                    <a href="{{ route('payee_names') }}" class="btn btn-primary push-5-r push-10">
                                        <i class="fa fa-folder-open" aria-hidden="true"></i> Items
                                    </a>

                                    <a href="{{ route('for_request') }}" class="btn btn-primary push-5-r push-10">
                                        <i class="fa fa-bell" aria-hidden="true"></i> For Request
                                    </a>
                                </div>

                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link @if(!isset($checkRequestStatus)) active @endif" href="{{ route('check_request') }}">
                                                <span class="badge badge-pill badge-default">{{ \App\CheckRequests::count() }}</span> All</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if(isset($checkRequestStatus) && $checkRequestStatus == 'Pending') active @endif" href="{{ route('check_request_pending') }}">
                                                <span class="badge badge-pill badge-warning">{{ \App\CheckRequests::where('status', 'Pending')->count() }}</span> Pending</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if(isset($checkRequestStatus) && $checkRequestStatus == 'Approved') active @endif" href="{{ route('check_request_approved') }}">
                                                <span class="badge badge-pill badge-success">{{ \App\CheckRequests::where('status', 'Approved')->count() }}</span> Approved</a>
                                        </li>
                                        <li class="nav-item @if(isset($checkRequestStatus) && $checkRequestStatus == 'Cancelled') active @endif">
                                            <a class="nav-link" href="{{ route('check_request_cancelled') }}">
                                                <span class="badge badge-pill badge-danger">{{ \App\CheckRequests::where('status', 'Cancelled')->count() }}</span> Cancelled</a>
                                        </li>
                                    </ul>
                                </div>

                                {{--This is for advanced search uncomment if needed--}}

                                {{--<div class="col-md-12 text-right">--}}
                                    {{--<br />--}}
                                    {{--<!-- Trigger the modal with a button -->--}}
                                    {{--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">--}}
                                        {{--Advanced Search--}}
                                    {{--</button>--}}
                                    {{--<div class="modal fade" id="myModal" role="dialog">--}}
                                        {{--<div class="modal-dialog">--}}
                                            {{--<!-- Modal content-->--}}
                                            {{--<div class="modal-content">--}}
                                                {{--<div class="modal-header">--}}
                                                    {{--<h4 class="modal-title">Advanced Search</h4>--}}
                                                {{--</div>--}}
                                                {{--<div class="modal-body text-left">--}}
                                                    {{--<div class="row">--}}

                                                        {{--<div class="col-md-6">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label class="col-xs-12" for="column_name">Column Name</label>--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<select class="form-control" id="column_name" name="column_name" size="1">--}}
                                                                        {{--<option value="payee_type">Payee Type</option>--}}
                                                                        {{--<option value="payee_name">Payee Name</option>--}}
                                                                        {{--<option value="payee_address">Payee Address</option>--}}
                                                                        {{--<option value="crf_number">CRF Number</option>--}}
                                                                        {{--<option value="item">Item</option>--}}
                                                                        {{--<option value="reference_number">Reference Number</option>--}}
                                                                    {{--</select>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-6">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<label for="colum_name_text">Column Name Text</label>--}}
                                                                    {{--<input class="form-control" type="text" id="colum_name_text" name="colum_name_text" />--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-4">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label class="col-xs-12" for="column_date">Column Date</label>--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<select class="form-control" id="column_date" name="column_date" size="1">--}}
                                                                        {{--<option value="request_date">Request Date</option>--}}
                                                                    {{--</select>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-4">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<label class="form-control-label">Column Date From</label>--}}
                                                                    {{--<div id="joinDate" class="input-group btn-mmddyyyy ">--}}
                                                                        {{--<input type="text" class="form-control" name="column_date_from" placeholder="mm/dd/yyyy" required="">--}}
                                                                        {{--<span class="input-group-addon"><span class="fa fa-calendar"></span></span>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-4">--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<div class="col-xs-12">--}}
                                                                    {{--<label class="form-control-label">Column Date To</label>--}}
                                                                    {{--<div id="joinDate" class="input-group btn-mmddyyyy ">--}}
                                                                        {{--<input type="text" class="form-control" name="column_date_to" placeholder="mm/dd/yyyy" required="">--}}
                                                                        {{--<span class="input-group-addon"><span class="fa fa-calendar"></span></span>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="modal-footer">--}}
                                                    {{--<button type="button" class="btn btn-success" data-dismiss="modal">View</button>--}}
                                                    {{--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{-- End Advanced Search--}}

                                <div class="col-md-12">
                                    <div class="block">
                                        <div class="block-content">
                                            <br>

                                            <table id="checkRequests" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>CRF NUMBER</th>
                                                        <th>PAYEE</th>
                                                        <th>PARTICULARS</th>
                                                        <th>AMOUNT</th>
                                                        <th>STATUS</th>
                                                        <th width="150">ACTIONS</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($checkRequests as $checkRequest)
                                                        <?php $items = $checkRequest->Items->pluck('particulars'); ?>
                                                        <tr>
                                                            <td>{{ $checkRequest->crf_number }}</td>
                                                            <td>{{ $checkRequest->payee_name }}</td>
                                                            <td>{{ implode(',', $items->toArray()) }}</td>
                                                            <td>P {{ number_format($checkRequest->total_amount, 2) }}</td>
                                                            <td><span class="label label-@if($checkRequest->status == 'Pending')warning @elseif($checkRequest->status == 'Cancelled')danger @elseif($checkRequest->status == 'Approved')success @endif">{{ strtoupper($checkRequest->status) }}</span></td>
                                                            <td>
                                                                @if($checkRequest->status == 'Pending')
                                                                    <a href="{{ url("admin/accounting/check_request/update/{$checkRequest->id}") }}" class="btn btn-sm btn-primary">EDIT</a>
                                                                @else
                                                                    <a href="{{ url("admin/accounting/check_request/view/{$checkRequest->id}") }}" class="btn btn-sm btn-primary">VIEW </a>
                                                                @endif

                                                                @if(!\App\CashFlows::where(['source' => 'Check Request', 'source_id' => $checkRequest->id])->exists())
                                                                    <a href="{{ url("/admin/accounting/cash_flow/create?source=check_request&id={$checkRequest->id}") }}" class="btn btn-sm btn-primary">CASH FLOW <i class="fa fa-arrow-right"></i></a>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#checkRequests").dataTable({
                "order": [],
                'pageLength': 100,
                "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ]
            });
        })
    </script>
@endsection

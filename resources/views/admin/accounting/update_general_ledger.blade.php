@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('general_ledger')}}">General Ledger</a></li>
        <li><a class="link-effect" href="#">Update General Ledger</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <form action="" method="POST" enctype="multipart/form-data">
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="trsnsaction_id">Transaction ID</label>
                                                        <input class="form-control"
                                                               type="text"
                                                               id="trsnsaction_id"
                                                               name="trsnsaction_id"
                                                               value="{{ $transaction[0]->transaction_id }}"required disabled />
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="reference_number">Reference Number</label>
                                                        <input class="form-control"
                                                               type="text"
                                                               id="reference_number"
                                                               name="reference_number"
                                                               value="{{ $transaction[0]->reference_number }}"required disabled />
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="particulars">Particulars</label>
                                                        <input class="form-control"
                                                               type="text"
                                                               id="particulars"
                                                               name="particulars"
                                                               value="{{ $transaction[0]->particulars }}"required disabled />
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label class="form-control-label">Date</label>
                                                        <div id="date" class="input-group btn-mmddyyyy ">
                                                            <input type="text" class="form-control" name="date" value="{{ date('m/d/Y', strtotime($transaction[0]->entry_date)) }}" required disabled>
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="amount">Amount</label>
                                                        <input class="form-control" type="text" id="amount" name="amount" value="{{ $transaction[0]->amount }}"required disabled />
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <h2 class="content-heading">Entry</h2>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="block-content">
                                            <table id="loans" class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Account Type</th>
                                                    <th>Account Title</th>
                                                    <th>TXN</th>
                                                    <th>DEBIT</th>
                                                    <th>CREDIR</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                {{--<tr>--}}
                                                {{--<td>1234</td>--}}
                                                {{--<td>1234</td>--}}
                                                {{--<td>1234</td>--}}
                                                {{--<td>1234</td>--}}
                                                {{--<td>1234</td>--}}
                                                {{--<td>1234</td>--}}
                                                {{--<td>1234</td>--}}
                                                {{--<td class="text-center">--}}
                                                {{--<a href="{{ route('loans_details') }}" class="btn btn-info btn-xs">UPDATE</a>--}}
                                                {{--<a href="{{ route('disbursed_loans') }}" class="btn btn-success btn-xs">RELEASE</a>--}}
                                                {{--</td>--}}
                                                {{--</tr>--}}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <br />
                                        <button class="btn btn-danger push-5-r push-10" type="button"><i class="fa fa-times"></i> Cancel</button>
                                        <button class="btn btn-success push-5-r push-10" type="button"><i class="fa fa-check"></i> Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

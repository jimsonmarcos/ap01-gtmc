@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('check_request')}}">Check Request</a></li>
        <li>Update Request Check</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12 text-right hidden">
                                    <i class="fa fa-print fa-2x" aria-hidden="true"></i>
                                </div>

                                <form action="" method="POST" enctype="multipart/form-data">
                                    <br>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12 col-md-4" for="payee_type">Payee Type</label>
                                                    <div class="col-xs-12 col-md-8">
                                                        {{ $checkRequest->payee_type }}
                                                    </div>
                                                </div>
                                                <br />
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12 col-md-4" for="payee_name">Payee Name</label>
                                                    <div class="col-xs-12 col-md-8">
                                                        {{ $checkRequest->payee_name }}
                                                    </div>
                                                </div>
                                                <br />
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12 col-md-4">
                                                        <label for="payee_address">Payee Address</label>
                                                    </div>
                                                    <div class="col-xs-12 col-md-8">
                                                        {{ $checkRequest->payee_address }}
                                                    </div>
                                                </div>
                                                <br />
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">

                                            @if($checkRequest->status == 'Approved')
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12 col-md-4">
                                                        <label for="crf_number">CRF Number</label>
                                                    </div>
                                                    <div class="col-xs-12 col-md-8">
                                                        {{ $checkRequest->crf_number }}
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                            @endif

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12 col-md-4" for="request_date">Request Date</label>
                                                    <div class="col-xs-12 col-md-8">
                                                        {{ date('F j, Y', strtotime($checkRequest->request_date)) }}
                                                    </div>
                                                </div>
                                                <br />
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <h2 class="content-heading">Summary / Schedule of Expenditures</h2>
                                    </div>

                                    <div class="col-md-12">
                                        <table class="table table-bordered table-condensed">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">ITEM</th>
                                                    <th class="text-center">REFERENCE NUMBER</th>
                                                    <th class="text-center">AMOUNT</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach(\App\CheckRequestItems::where('check_request_id', $checkRequest->id)->get() as $checkRequestItem)
                                                    <tr>
                                                        <td class="text-center">{{ $checkRequestItem->cash_flow_item }}</td>
                                                        <td class="text-center">{{ $checkRequestItem->reference_number }}</td>
                                                        <td class="text-center">{{ $checkRequestItem->amount }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="80">
                                                            <div><label for="">Particulars</label></div>
                                                            {{ $checkRequestItem->particulars }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">

                                            <div class="col-md-2 col-xs-3">
                                                <p style="padding-top: 10px;">Total Amount</p>
                                            </div>
                                            <div class="col-md-3 col-xs-4 text-center">
                                                <p style="border: 1px blue solid; border-radius: 50px; padding: 10px;font-weight: bold;">{{ number_format(\App\CheckRequestItems::where('check_request_id', $checkRequest->id)->get()->sum('amount'), 2) }} PHP</p>
                                            </div>

                                            <div class="col-xs-12"></div>

                                            <div class="col-md-2 col-xs-3">
                                                Amount In Words
                                            </div>
                                            <div class="col-md-5 col-xs-7" id="amountIntoWords" data-amount="{{ \App\CheckRequestItems::where('check_request_id', $checkRequest->id)->get()->sum('amount') }}"></div>

                                        </div>
                                        <br />
                                    </div>

                                    <div class="col-md-12 push-5-t">
                                        <div class="row">
                                            <div class="col-md-2 col-xs-3">Requested By:</div>
                                            <div class="col-md-4 col-xs-4">
                                                {{ $checkRequest->RequestedBy->name }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 push-5-t">
                                        <div class="row">
                                            <div class="col-md-2 col-xs-3"> Noted By:</div>
                                            <div class="col-md-4 col-xs-4">
                                                {{ $checkRequest->NotedBy->name }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 push-5-t">
                                        <div class="row">
                                            <div class="col-md-2 col-xs-3">Approved By:</div>
                                            <div class="col-md-4 col-xs-4">
                                                {{ $checkRequest->ApprovedBy->name }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <br />
                                        @if($checkRequest->status == 'Pending')
{{--                                            <a href="{{ url("/admin/accounting/check_request/Cancel/{$checkRequest->id}") }}" class="btn btn-danger push-5-r push-10"><i class="fa fa-times"></i> Cancel</a>--}}
                                            <a href="{{ url("/admin/accounting/check_request/Approve/{$checkRequest->id}") }}" class="btn btn-success push-5-r push-10"><i class="fa fa-check"></i> Update</a>
                                        @elseif($checkRequest->status == 'Approved')
{{--                                            <a href="{{ url("/admin/accounting/cash_flow_update_request/{$checkRequest->id}") }}" class="btn btn-success push-5-r push-10"> Cash Flow <i class="fa fa-chevron-right"></i></a>--}}
                                        @endif
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            var totalAmount = $("#amountIntoWords").attr('data-amount');

            var pieces = totalAmount.toString().split('.');

            var decimal = '';
            var wholeNumber = 0;

            if(pieces.length == 1) {
                var wholeNumber = parseFloat(pieces[0]);
            } else if(pieces.length == 2) {
                // console.log(pieces);
                var wholeNumber = parseFloat(pieces[0]);


                // console.log(pieces[1]);

                if(pieces[1].length == 1) {
                    // decimal = decimal.numberFormat(2);
                    var decimal = " and "+ pieces[1] + "0/100";
                } else {
                    if(pieces[1] > 0) {
                        var decimal = " and "+ parseInt(pieces[1]) + "/100";
                    }
                }


            }


            var amountIntoWords = chunk(wholeNumber.numberFormat(2).replace(/\,/g, ''))
                .map(inEnglish)
                .map(appendScale)
                .filter(isTruthy)
                .reverse()
                .join(" ");

            amountIntoWords = amountIntoWords + " Pesos " + decimal;
            $("#amountIntoWords").text(amountIntoWords);

        })
    </script>
@endsection

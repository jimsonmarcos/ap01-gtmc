@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('check_request')}}">Check Request</a></li>
        <li><a class="link-effect" href="{{ route('manage_items')}}">Manage Items</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('manage_items') }}">Manage Items</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('add_payee_name') }}">Add Payee Name</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('add_cash_flow_items') }}">Add Cash Flow Items</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <h1>Jim Ndi ko alam ilalagay ko dito kaya data table nlng </h1>
                            <div class="col-md-12">
                                <div class="block">
                                    <div class="block-content">
                                        <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="dataTables_length" id="DataTables_Table_1_length"><label>
                                                            <select name="DataTables_Table_1_length"
                                                                    aria-controls="DataTables_Table_1"
                                                                    class="form-control">
                                                                <option value="5">5</option>
                                                                <option value="10">10</option>
                                                                <option value="15">15</option>
                                                                <option value="20">20</option>
                                                            </select>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div id="DataTables_Table_1_filter" class="dataTables_filter">
                                                        <label>Search:<input type="search" class="form-control" placeholder="" aria-controls="DataTables_Table_1"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class="table table-bordered table-striped js-dataTable-full dataTable no-footer"
                                                           id="DataTables_Table_1" role="grid" aria-describedby="DataTables_Table_1_info">
                                                        <thead>
                                                        <tr role="row">
                                                            <th class="text-center sorting_asc"
                                                                tabindex="0"
                                                                aria-controls="DataTables_Table_1"
                                                                rowspan="1" colspan="1" aria-sort="ascending"
                                                                aria-label=": activate to sort column descending"></th>

                                                            <th class="sorting"
                                                                tabindex="0"
                                                                aria-controls="DataTables_Table_1"
                                                                rowspan="1" colspan="1"
                                                                aria-label="Name: activate to sort column ascending">CRF Number</th>

                                                            <th class="hidden-xs sorting"
                                                                tabindex="0" aria-controls="DataTables_Table_1"
                                                                rowspan="1" colspan="1"
                                                                aria-label="Email: activate to sort column ascending">Payee</th>

                                                            <th class="hidden-xs sorting" tabindex="0" aria-controls="DataTables_Table_1"
                                                                rowspan="1" colspan="1"
                                                                aria-label="Access: activate to sort column ascending">Particulars</th>

                                                            <th class="text-center sorting_disabled" rowspan="1"
                                                                colspan="1" aria-label="Actions">Amount</th>

                                                            <th class="text-center sorting_disabled" rowspan="1"
                                                                colspan="1" aria-label="Actions">Status</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr role="row" class="odd">
                                                            <td class="text-center sorting_1">
                                                                1
                                                            </td>
                                                            <td class="font-w600"></td>
                                                            <td class="font-w600"></td>
                                                            <td class="font-w600">
                                                                <span class="label label-danger">YES</span>
                                                            </td>
                                                            <td class="font-w600"></td>
                                                            <td class="font-w600"></td>

                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="dataTables_info" id="DataTables_Table_1_info"
                                                         role="status" aria-live="polite">Showing <strong>1</strong>-<strong>10</strong> of <strong>40</strong>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="dataTables_paginate paging_simple_numbers"
                                                         id="DataTables_Table_1_paginate">
                                                        <ul class="pagination">
                                                            <li class="paginate_button previous disabled"
                                                                aria-controls="DataTables_Table_1" tabindex="0"
                                                                id="DataTables_Table_1_previous">
                                                                <a href="#"><i class="fa fa-angle-left"></i></a>
                                                            </li>

                                                            <li class="paginate_button active" aria-controls="DataTables_Table_1"
                                                                tabindex="0">
                                                                <a href="#">1</a>
                                                            </li>

                                                            <li class="paginate_button " aria-controls="DataTables_Table_1"
                                                                tabindex="0">
                                                                <a href="#">2</a>
                                                            </li>

                                                            <li class="paginate_button " aria-controls="DataTables_Table_1"
                                                                tabindex="0">
                                                                <a href="#">3</a>
                                                            </li>

                                                            <li class="paginate_button " aria-controls="DataTables_Table_1"
                                                                tabindex="0">
                                                                <a href="#">4</a>
                                                            </li>

                                                            <li class="paginate_button next" aria-controls="DataTables_Table_1"
                                                                tabindex="0" id="DataTables_Table_1_next">
                                                                <a href="#"><i class="fa fa-angle-right"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li>Cash Flow Update Request</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <br />
                                    <h2 class="content-heading text-center">Update From Check Request</h2>
                                </div>

                                <form action="" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="mode">Mode</label>
                                                    <div class="col-xs-12">
                                                        <input type="hidden" name="mode" value="Check" />
                                                        <select class="form-control" id="mode" size="1" required disabled>
                                                            <option>Check</option>
                                                        </select>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label class="form-control-label">Check Date</label>
                                                        <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                            <input type="text" class="form-control" name="check_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y') }}" required>
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        </div>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="check_number">Check Number</label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" type="number" id="check_number" name="check_number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' autocomplete="off" required/>
                                                    </div>
                                                </div>
                                                <br />
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="payee_name">Payee Name</label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" type="text" id="payee_name" name="payee_name" value="{{ $checkRequest->payee_name }}" readonly required/>
                                                    </div>
                                                </div>
                                                <br />
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label class="form-control-label">Entry Date</label>
                                                        <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                            <input type="text" class="form-control" name="entry_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y') }}" required>
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <br />
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="status">Status</label>
                                                    <div class="col-xs-12">
                                                        <select class="form-control" id="status" name="status">
                                                            <option value="Deposit">Deposit</option>
                                                            <option value="Withdrawal">Withdrawal</option>
                                                            <option value="Uncleared">Uncleared</option>
                                                            <option value="Cancelled">Cancelled</option>
                                                            <option value="No Details">No Details</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <br />
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="payee_address">Payee Address</label>
                                                    <div class="col-xs-12">
                                                        <textarea name="payee_address" id="" rows="4"
                                                                  class="form-control">{{ $checkRequest->payee_address }}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="particulars">Particulars</label>
                                                <textarea class="form-control input-lg" id="particulars" name="particulars" rows="5" required> {{ $checkRequest->Items->count() > 0 ? implode(', ', $checkRequest->Items->pluck('cash_flow_item')->toArray()) : "" }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <br />
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="col-xs-6">
                                                    <label>Amount</label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="col-xs-6 text-center">
                                                    <p style="border: 1px blue solid; border-radius: 50px; padding: 10px;font-weight: bold;">{{ number_format($checkRequest->Items->sum('amount'), 2) }} PHP</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="bank">Banks</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="bank" name="bank_id" size="1">
                                                    @foreach(\App\Banks::orderBy('bank_account')->get() as $bank)
                                                    <option value="{{ $bank->id }}">{{ $bank->bank_account }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-xs-12" for="chart_of_accounts">Chart Of Accounts</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="chart_of_account_id" name="chart_of_accounts" size="1" required>

                                                    @foreach(\App\ChartOfAccounts::orderBy('account_type')->get() as $chartOfAccount)
                                                        <option value="{{ $chartOfAccount->id }}" @if($chartOfAccount->id == 2) selected @endif>{{ strtoupper("{$chartOfAccount->account_type} - {$chartOfAccount->account_title}") }}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="reference_number">Reference Number</label>
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" id="reference_number" name="reference_number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' autocomplete="off" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12"></div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="cv_number">CV Number</label>
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" id="cv_number" name="cv_number" required disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <h5 class="">CV Reference</h5>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <div class="col-xs-8">Check Number</div>
                                                    <div class="col-xs-4">[ x ]</div>
                                                </div>
                                            </div>

                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <div class="col-xs-8">CV Number</div>
                                                    <div class="col-xs-4">[ x ]</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label for="remarks">Remarks</label>
                                                <textarea class="form-control input-lg" id="remarks" name="remarks" rows="5" required></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <h2 class="content-heading text-center">
                                            Notes: Will also a automatic create CV Number depending on Bank and will save both in cash flow and or number
                                        </h2>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                                        <button class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> Submit</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection














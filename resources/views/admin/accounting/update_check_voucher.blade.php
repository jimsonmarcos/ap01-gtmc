@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('check_voucher')}}">Check Voucher</a></li>
        <li><a class="link-effect" href="">Update</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                <br />
                                <form action="" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="col-md-6">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="cv_number">CV Number</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="cv_number" value="{{ $checkVoucher->cv_number }}" disabled/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="payee_name">Payee Name</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="payee_name" name="" value="{{ $checkVoucher->payee_name }}" disabled/>
                                                </div>
                                            </div>
                                            <br />
                                        </div>

                                    </div>


                                    <div class="col-md-6">



                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Check Date</label>
                                                    <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                        <input type="text" class="form-control" value="{{ date('m/d/Y', strtotime($checkVoucher->check_date)) }}" placeholder="mm/dd/yyyy" disabled>
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="check_number">Check Number</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="check_number" value="{{ $checkVoucher->check_number }}" disabled/>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12 push-10-t">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="col-xs-12" for="particulars">Particulars</label>
                                                <div class="col-xs-12">

                                                    <textarea class="form-control input-lg" id="particulars" name="particulars" rows="5" disabled>{{ $checkVoucher->particulars }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 push-10-t">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="col-xs-12" for="amount">Amount</label>
                                                <div class="col-xs-12">

                                                    <input class="form-control" type="text" id="amount" value="{{ number_format($checkVoucher->total_amount, 2) }}" required disabled/>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    {{--<div class="col-md-12 push-10-t">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<div class="col-xs-12">--}}
                                                {{--<label class="col-xs-12" for="amount">Amount in words</label>--}}
                                                {{--<div class="col-xs-12">--}}
                                                    {{--<p id="amountIntoWords"></p>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="col-md-12">
                                        <h2 class="content-heading">Entry</h2>
                                    </div>

                                    <div class="col-md-12 push-10-t">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <div class="col-xs-12">

                                                    <table id="cashFlowEntries" class="table table-striped table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center">Account Type</th>
                                                            <th class="text-center">Account Title</th>
                                                            <th class="text-center" width="150">TXN</th>
                                                            <th class="text-center" width="200">DEBIT</th>
                                                            <th class="text-center" width="200">CREDIT</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $x = 0; ?>
                                                        @foreach($checkVoucher->Entries as $entry)
                                                            <tr>
                                                                <td>
                                                                    {{ $entry->account_type }}
                                                                </td>
                                                                <td>
                                                                    {{ $entry->account_title }}
                                                                </td>
                                                                <td>
                                                                    {{ $entry->txn }}
                                                                </td>
                                                                <td>{{ number_format($entry->debit, 2) }}</td>
                                                                <td>{{ number_format($entry->credit, 2) }}</td>
                                                            </tr>
                                                            <?php $x++; ?>
                                                        @endforeach

                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <th>Total</th>
                                                            <th></th>
                                                            <th></th>
                                                            <th id="totalDebit">{{ number_format($checkVoucher->Entries->sum('debit'), 2) }}</th>
                                                            <th id="totalCredit">{{ number_format($checkVoucher->Entries->sum('credit'), 2) }}</th>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <br />

                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="prepared_by">Prepared By</label>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" value="{{ $me->Profile->namefl() }}" disabled>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12"></div>

                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="approved_by">Approved By</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="approved_by" name="approved_by" size="1">
                                                        @foreach($signatories as $user)
                                                            <option value="{{ $user->id }}" @if($checkVoucher->approved_by == $user->id) selected @endif>{{ "{$user->Profile->first_name} {$user->Profile->last_name}" }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <h5 class="" id="receivedFrom">

                                        </h5>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <a class="btn btn-danger push-5-r push-10" onclick="window.history.go(-1); return false;" type="button" ><i class="fa fa-chevron-left"></i> Back</a>
                                        <button class="btn btn-success push-5-r push-10"><i class="fa fa-check"></i> Update</button>
                                        {{--<a href="#" class="btn btn-primary push-5-r push-10">Preview</a>--}}
                                    </div>
                                </form>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="hidden" id="template">
        <table>
            <tr>
                <td>
                    <select name="item[x][chart_of_account_id]" id="" class="form-control">
                        @foreach(\App\ChartOfAccounts::orderBy('account_type')->get() as $item)
                            <option value="{{ $item->id }}" @if($item->id == 2) selected @endif>{{ $item->account_type }} - {{ $item->account_title }}</option>
                        @endforeach
                    </select>
                </td>
                <td width="100">
                    <select name="item[x][txn]" id="" class="form-control txn">
                        <option value="DR">DR</option>
                        <option value="CR">CR</option>
                    </select>
                </td>
                <td><input type="text" class="form-control dbamount" name="item[x][debit]" autocomplete="off" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" required></td>
                <td><input type="text" class="form-control cramount" name="item[x][credit]" autocomplete="off" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" disabled required></td>
                <td class="text-center"><button class="btn btn-xs btn-danger removeItem" type="button"><i class="fa fa-remove"></i></button></td>
            </tr>
        </table>
    </div>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            updateTotalAmount();

            function updateTotalAmount() {
                var totalAmount = 0;
                var amountIntoWords = '';

                var amount = parseFloat($("#amount").val().replace(',', '')).toFixed(2) || 0;
                if($("#amount").val().length == 0) {
                    amount = 0;
                }
                totalAmount += amount;

                // console.log(totalAmount);


                var pieces = totalAmount.toString().split('.');

                var decimal = '';
                var wholeNumber = 0;

                if(pieces.length == 1) {
                    var wholeNumber = parseFloat(pieces[0]);
                } else if(pieces.length == 2) {
                    // console.log(pieces);
                    var wholeNumber = parseFloat(pieces[0]);


                    // console.log(pieces[1]);

                    if(pieces[1].length == 1) {
                        // decimal = decimal.numberFormat(2);
                        var decimal = " and "+ pieces[1] + "0/100";
                    } else {
                        if(pieces[1] > 0) {
                            var decimal = " and "+ parseInt(pieces[1]) + "/100";
                        }
                    }

                }

                // console.log(pieces.length);

                // console.log(totalAmount.numberFormat(2).replace(/\,/g, ''));

                var amountIntoWords = chunk(wholeNumber.numberFormat(2).replace(/\,/g, ''))
                    .map(inEnglish)
                    .map(appendScale)
                    .filter(isTruthy)
                    .reverse()
                    .join(" ");

                if(amountIntoWords != '') {
                    amountIntoWords = amountIntoWords + " Pesos " + decimal;
                }





                // $("#amountIntoWords").text(amountIntoWords);

                $("#receivedFrom").text('Recieved from '+ $("#payee_name").val() +' the sum of '+ amountIntoWords +' PHP '+ $("#amount").val() +'');

            }

            var x = 1;




            $(document).on('change', '#checkVoucherItems .txn', function() {
                if($(this).find(':selected').val() == 'DR') {
                    $(this).closest('tr').find('.cramount').val('').attr('disabled', true);
                    $(this).closest('tr').find('.dbamount').val('').attr('disabled', false);
                } else {
                    $(this).closest('tr').find('.dbamount').val('').attr('disabled', true);
                    $(this).closest('tr').find('.cramount').val('').attr('disabled', false);
                }

                updateTotalAmounts();
            });

            $(document).on('click', '#addNewItem', function(e) {
                var template = $("#template tr").clone();
                e.preventDefault();

                template.find('input, textarea, select').each(function() {
                    $(this).attr('name', $(this).attr('name').replace('[x]', '[' + x.toString() + ']'));
                });

                template.appendTo('#checkVoucherItems tbody');
                x += 1;
            });


            $(document).on('click', ".removeItem", function() {
                $(this).closest('tr').remove();
            })


            $(document).on('change, keyup', '#checkVoucherItems .dbamount, #checkVoucherItems .cramount', function() {
                updateTotalAmounts();

            });

            function updateTotalAmounts() {
                var totalDebitAmount = 0;
                var totalCreditAmount = 0;

                $("#checkVoucherItems .dbamount").each(function() {
                    if($(this).val().length > 0) {
                        var amount = parseFloat($(this).val()).toFixed(2) || 0;
                        totalDebitAmount += parseFloat(amount);
                    }
                });

                $("#checkVoucherItems .cramount").each(function() {
                    if($(this).val().length > 0) {
                        var amount = parseFloat($(this).val()).toFixed(2) || 0;
                        totalCreditAmount += parseFloat(amount);
                    }
                });

                $("#totalDebitAmount").text("P "+ commaSeparateNumber(totalDebitAmount) +" PHP");
                $("#totalCreditAmount").text("P "+ commaSeparateNumber(totalCreditAmount) +" PHP");
            }
        })
    </script>
@endsection

<!-- Side Navbar -->
<nav class="side-navbar" style="background-color: #003366;">
    <br>
    <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
    <ul class="list-unstyled">
        <li class="active"> <a href="./"><i class="icon-home"></i>Home</a></li>

        <li><a href="#ddusers" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-users"></i>User Management </a>
            <ul id="ddusers" class="collapse list-unstyled">
                <li><a href="{{ url('admin/user/add') }}">New User</a></li>
                <li><a href="{{ url('admin/users') }}">User Profile</a></li>
                <li><a href="{{ url('admin/manage-admin/users') }}">Manage Admin</a></li>

            </ul>
        </li>

        <li><a href="#ddclients" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-users"></i>Client Management </a>
            <ul id="ddclients" class="collapse list-unstyled">
                <li><a href="{{ url('admin/clients/add') }}">New Client</a></li>
                <li><a href="{{ url('admin/clients') }}">Client Profile</a></li>
                <li><a href="{{ url('admin/manage-admin/clients') }}">Manage Admin</a></li>
            </ul>
        </li>

        <li><a href="#ddtransactions" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-briefcase"></i>Transactions </a>
            <ul id="ddtransactions" class="collapse list-unstyled">
                <li><a href="{{ route('payments') }}">Payments</a></li>
                <li><a href="{{ route('loans') }}">Loans</a></li>
                <li><a href="{{ url('admin/transactions/loans/calculator') }}">Loan Calculator</a></li>
                <li><a href="{{ url('admin/transactions/invoices') }}">Invoices</a></li>
            </ul>
        </li>


        <li><a href="#ddpayroll" aria-expanded="false" data-toggle="collapse"><i class="fa fa-money" aria-hidden="true"></i>Payroll </a>
            <ul id="ddpayroll" class="collapse list-unstyled">
                <li><a href="{{ url ('admin/payroll/wizard') }}">Payroll Wizard</a></li>
                <li><a href="{{ route('last_pay') }}">Last Pay</a></li>
            </ul>
        </li>


        <li> <a href="#ddaccounting" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-tasks" aria-hidden="true"></i>Accounting </a>
            <ul id="ddaccounting" class="collapse list-unstyled">
                <li><a href="{{ route('check_request') }}">Check Request</a></li>
                <li><a href="{{ route('cash_flow_create') }}">Cash Flow</a></li>
                <li><a href="{{ route('check_voucher') }}">Check Voucher</a></li>
                <li><a href="{{ route('general_ledger') }}">General Ledger</a></li>
                <li><a href="{{ route('journal_voucher') }}">Journal Voucher</a></li>
            </ul>
        </li>

        <li> <a href="tables.html"> <i class="fa fa-bar-chart"></i>Reports </a></li>
    </ul>
</nav>
@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>HMDF M1-1</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="reports_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link " href="{{ route('reports_sss_monthly_contribution') }}">SSS Monthly Contribution</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('reports_hdmf') }}">HDMF Monthly Contribution</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link " href="{{ route('reports_philhealth') }}">PHIC Monthly Contribution</a>
                                        </li>
                                    </ul>
                                </div>

                                <br />

                                <form action="" method="GET">



                                    <div class="col-md-10">
                                        <h2 class="content-heading">Extract Files</h2>
                                        <div class="input-daterange input-group" data-date-format="mm/dd/yyyy">
                                            <input class="form-control btn-mm-yyyy" type="text" id="period_from" name="period_from" value="{{ !empty($period_from) ? $period_from : '' }}" placeholder="Period From">
                                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                            <input class="form-control btn-mm-yyyy" type="text" id="period_to" name="period_to" value="{{ !empty($period_to) ? $period_to : '' }}" placeholder="Period To">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <br /><br /><br />
                                        <button class="btn btn-sm btn-primary">Submit filter</button>
                                    </div>

                                    <br>
                                    <div class="col-md-12 text-right">
                                        <br>
                                        <br>
                                        <button id="print" class="btn btn-sm btn-success"><i class="fa fa-print push-5-r"></i> Print</button>
                                    </div>
                                </form>

                                <div id='print-area' class="col-md-12">
                                    <div class="visible-print">
                                        <h2 class="content-heading text-center">HDMF Monthly Contribution</h2>
                                    </div>

                                    <br />
                                    <table class="table table-striped">
                                        <thead>
                                            <th>HDMF</th>
                                            <th>TIN</th>
                                            <th>Date of Birth</th>
                                            <th>Employee's Name</th>
                                            <th>Cont. Employees</th>
                                            <th>Cont. Employer</th>
                                            <th>Total</th>
                                        </thead>
                                        <tbody>

                                        @if(!empty($employees))
                                            <tbody>
                                            @foreach($employees as $employee)
                                                <tr>
                                                    <td>{{ $employee->hdmf }}</td>
                                                    <td>{{ $employee->tin }}</td>
                                                    <td>{{ date('m-d-Y', strtotime($employee->birthday)) }}</td>
                                                    <td>{{ $employee->name }}</td>
                                                    <td>{{ number_format($employee->hdmf_ee, 2) }}</td>
                                                    <td>{{ number_format($employee->hdmf_er, 2) }}</td>
                                                    <td>{{ number_format($employee->hdmf_ee + $employee->hdmf_er, 2) }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th colspan="4">Grand Total:</th>
                                                <th>{{ number_format($employees->sum('hdmf_ee'), 2) }}</th>
                                                <th>{{ number_format($employees->sum('hdmf_er'), 2) }}</th>
                                                <th>{{ number_format($employees->sum('hdmf_ee') + $employees->sum('hdmf_er'), 2) }}</th>
                                            </tr>
                                            </tfoot>
                                        @else
                                            <tbody>
                                            <td colspan="7">Please select period first.</td>
                                            </tbody>
                                        @endif

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#print").click(function() {
                $.print("#print-area");
            });

            // $("#hdmf-datatable").dataTable({
            //     'pageLength': 100,
            //     "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ]
            // });
        })
    </script>
@endsection

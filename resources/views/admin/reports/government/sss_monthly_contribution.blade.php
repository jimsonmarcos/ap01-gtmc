@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>SSS Monthly Contribution</li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="reports_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('reports_sss_monthly_contribution') }}">SSS Monthly Contribution</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link " href="{{ route('reports_hdmf') }}">HDMF Monthly Contribution</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link " href="{{ route('reports_philhealth') }}">PHIC Monthly Contribution</a>
                                        </li>
                                    </ul>
                                </div>

                                <br />

                                <form action="" method="GET">



                                    <div class="col-md-10">
                                        <h2 class="content-heading">Extract Files</h2>
                                        <div class="input-daterange input-group" data-date-format="mm/dd/yyyy">
                                            <input class="form-control btn-mm-yyyy" type="text" id="period_from" name="period_from" value="{{ !empty($period_from) ? $period_from : '' }}" placeholder="Period From">
                                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                            <input class="form-control btn-mm-yyyy" type="text" id="period_to" name="period_to" value="{{ !empty($period_to) ? $period_to : '' }}" placeholder="Period To">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <br /><br /><br />
                                        <button class="btn btn-sm btn-primary">Submit filter</button>
                                    </div>

                                </form>

                                <br />
                                <div class="col-md-12 text-right">
                                    <br>
                                    <br>
                                    <button id="print" class="btn btn-sm btn-success"><i class="fa fa-print push-5-r"></i> Print</button>
                                </div>

                                <div id='print-area' class="col-md-12">
                                    <div class="visible-print">
                                        <h2 class="content-heading text-center">SSS Monthly Contribution</h2>
                                    </div>

                                    <br />
                                    <table class="table table-striped">
                                        <thead>
                                            <th>Code</th>
                                            <th>Employee's Name</th>
                                            <th>SSS No.</th>
                                            <th>SSS Cont. Employee</th>
                                            <th>SSS Cont. Employer</th>
                                            <th>SSS Cont. Total EE+ER</th>
                                            <th>SSS Cont. EC</th>
                                            <th>SSS Cont. Total</th>
                                        </thead>
                                        @if(!empty($employees))
                                        <tbody>
                                            @foreach($employees as $employee)
                                                <tr>
                                                    <td>{{ $employee->id_number }}</td>
                                                    <td>{{ $employee->name }}</td>
                                                    <td>{{ $employee->sss }}</td>
                                                    <td>{{ number_format($employee->sss_ee, 2) }}</td>
                                                    <td>{{ number_format($employee->sss_er, 2) }}</td>
                                                    <td>{{ number_format($employee->sss_ee + $employee->sss_er, 2) }}</td>
                                                    <td>{{ number_format($employee->sss_ec, 2) }}</td>
                                                    <td>{{ number_format($employee->sss_ee + $employee->sss_er + $employee->sss_ec, 2) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3">Grand Total:</th>
                                                <th>{{ number_format($employees->sum('sss_ee'), 2) }}</th>
                                                <th>{{ number_format($employees->sum('sss_er'), 2) }}</th>
                                                <th>{{ number_format($employees->sum('sss_ee') + $employees->sum('sss_er'), 2) }}</th>
                                                <th>{{ number_format($employees->sum('sss_ec'), 2) }}</th>
                                                <th>{{ number_format($employees->sum('sss_ee') + $employees->sum('sss_er') + $employees->sum('sss_ec'), 2) }}</th>
                                            </tr>
                                        </tfoot>
                                        @else
                                            <tbody>
                                                <td colspan="8">Please select period first.</td>
                                            </tbody>
                                        @endif
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#print").click(function() {
                $.print("#print-area");
            });

            // $("#sss-datatable").dataTable({
            //     'pageLength': 100,
            //     "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ]
            // });
        })
    </script>
@endsection

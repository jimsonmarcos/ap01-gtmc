@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('reports_payroll_summary')}}">Summary of Payroll</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                @include('admin/reports/payroll/nav')

                                <form action="" method="GET">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="principal">Company Code</label>
                                            <div class="col-xs-12">
                                                <select name="company_id" id="company" class="form-control">
                                                    @foreach($companies as $company)
                                                        <option value="{{ $company->id }}" data-costcenters='{{ $company->CostCenters }}' data-payrollgroups='{{ $company->PayrollGroups }}'>{{ $company->company_code }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="company_code">Payroll Group</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="payroll_group" name="payroll_group_id" size="1">

                                                </select>
                                                <br/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="company_code">Cost Center</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="cost_center" name="cost_center_id" size="1">

                                                </select>
                                                <br/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="col-xs-12" for="hire_date_from">Date From</label>
                                            <div class="col-xs-12">
                                                <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                    <input type="text" class="form-control" id="date_from" name="date_from" placeholder="mm/dd/yyyy" value="@if(!empty($request['date_from'])) {{ date('m/d/Y', strtotime($request['date_from'])) }} @endif">
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="col-xs-12" for="hire_date_from">Date To</label>
                                            <div class="col-xs-12">
                                                <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                    <input type="text" class="form-control" id="date_to" name="date_to" placeholder="mm/dd/yyyy" value="@if(!empty($request['date_to'])) {{ date('m/d/Y', strtotime($request['date_to'])) }} @endif">
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="col-xs-12" for="category">Search</label>--}}
                                            {{--<div class="col-xs-12">--}}
                                                {{--<input type="text" id="search" name="search" class="form-control">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="form-control-label"></label>
                                                <div>
                                                    <button class="btn btn-primary btn-sm pull-right push-10-l">Submit Filter</button>
                                                    <a href="{{ url("/admin/reports/payroll/employee/download?{$qs}") }}" class="btn btn-primary btn-sm pull-right">Download Excel <i class="fa fa-file-excel-o"></i></a>
                                                </div>
                                                <br />
                                            </div>
                                        </div>
                                    </div>

                                </form>
                                
                                <div class="col-md-12">
                                    <br />
                                    <table id="users-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Employee Name</th>
                                            <th>Principal</th>
                                            <th>Payroll Group</th>
                                            <th>Payroll Code</th>
                                            {{--<th>Payroll Period</th>--}}
                                            <th>Payroll Date</th>
                                            <th>Net Pay</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($payrollCycles))
                                            @foreach($payrollCycles as $payrollCycle)
                                                <tr>
                                                    <td>{{ $payrollCycle->last_name }}, {{ $payrollCycle->first_name }}</td>
                                                    <td>{{ $payrollCycle->PayrollGroup->Company->company_code }}</td>
                                                    <td>{{ $payrollCycle->PayrollGroup->group_name }}</td>
                                                    <td>{{ $payrollCycle->payroll_code }}</td>
                                                    {{--<td>{{ $payrollCycle->payroll_period }}</td>--}}
                                                    <td>{{ date('m/d/Y', strtotime($request['date_from'])) }} - {{ date('m/d/Y', strtotime($request['date_to'])) }}</td>
                                                    <td>{{ number_format($payrollCycle->net_pay, 2) }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5">No result to display. </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            var c = '{{ !empty($request['company_id']) ? $request['company_id'] : '' }}';
            var pg = '{{ !empty($request['payroll_group_id']) && $request['payroll_group_id'] != 'All' ? $request['payroll_group_id'] : '' }}';
            var cc = '{{ !empty($request['cost_center_id']) && $request['cost_center_id'] != 'All' ? $request['cost_center_id'] : '' }}';

            initCompanies();
            function initCompanies() {
                if(c != '') {
                    $("#company").find('option').each(function() {
                        if($(this).val() == c) {
                            $(this).prop('selected', true);
                        }
                    });
                }

                var company = $("#company").find(':selected');

                var costCenters = company.data('costcenters');
                var payrollGroups = company.data('payrollgroups');

                if(costCenters != undefined && costCenters.length > 0) {
                    $("#cost_center").find('option').remove();
                    $("#cost_center").append('<option>All</option>');
                    $.each(costCenters, function(i, costCenter) {
                        if(cc != '' && cc == costCenter.id) {
                            $("#cost_center").append('<option value="'+ costCenter.id +'" selected>'+ costCenter.cost_center +'</option>');
                        } else {
                            $("#cost_center").append('<option value="'+ costCenter.id +'">'+ costCenter.cost_center +'</option>');
                        }

                    });
                } else {
                    $("#cost_center").find('option').remove();
                    $("#cost_center").append('<option>All</option>');
                }

                if(payrollGroups != undefined && payrollGroups.length > 0) {
                    $("#payroll_group").find('option').remove();
                    $("#payroll_group").append('<option>All</option>');
                    $.each(payrollGroups, function(i, payrollGroup) {
                        if(pg != '' && pg == payrollGroup.id) {
                            $("#payroll_group").append('<option value="'+ payrollGroup.id +'" selected>'+ payrollGroup.group_name +'</option>');
                        } else {
                            $("#payroll_group").append('<option value="'+ payrollGroup.id +'">'+ payrollGroup.group_name +'</option>');
                        }

                    });
                } else {
                    $("#payroll_group").find('option').remove();
                    $("#payroll_group").append('<option>All</option>');
                }
            }

            $("#company").change(function() {
                updateDropdowns();
            });

            // updateDropdowns();



            function updateDropdowns() {
                var company = $("#company").find(':selected');





                if(company.val() != '') {
                    var costCenters = company.data('costcenters');
                    var payrollGroups = company.data('payrollgroups');

                    if(costCenters.length > 0) {
                        $("#cost_center").find('option').remove();
                        $("#cost_center").append('<option>All</option>');
                        $.each(costCenters, function(i, costCenter) {
                            $("#cost_center").append('<option value="'+ costCenter.id +'">'+ costCenter.cost_center +'</option>');
                        });
                    }

                    if(payrollGroups.length > 0) {
                        $("#payroll_group").find('option').remove();
                        $("#payroll_group").append('<option>All</option>');
                        $.each(payrollGroups, function(i, payrollGroup) {
                            $("#payroll_group").append('<option value="'+ payrollGroup.id +'">'+ payrollGroup.group_name +'</option>');
                        });
                    }
                } else {
                    $("#payroll_group").find('option').remove();
                    $("#payroll_group").append('<option>All</option>');

                    $("#cost_center").find('option').remove();
                    $("#cost_center").append('<option>All</option>');
                }

                if(cc != '') {
                    $("#cost_center").find('option').each(function() {
                        if($(this).val() == cc) {
                            $(this).prop('selected', true);
                        }
                    });
                }

                if(pg != '') {
                    $("#payroll_group").find('option').each(function() {
                        if($(this).val() == pg) {
                            $(this).prop('selected', true);
                        }
                    });
                }
            }
        });
    </script>
@endsection

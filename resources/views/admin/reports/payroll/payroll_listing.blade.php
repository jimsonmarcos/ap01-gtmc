@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll Listing</li>

    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                @include('admin/reports/payroll/nav')

                                <form action="" method="GET">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="principal">Company Code</label>
                                            <div class="col-xs-12">
                                                <select name="company_id" id="company" class="form-control">
                                                    @foreach($companies as $company)
                                                        <option value="{{ $company->id }}" data-payrollgroups='{{ $company->PayrollGroups }}'>{{ $company->company_code }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="company_code">Payroll Group</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="payroll_group" name="payroll_group" size="1">

                                                </select>
                                                <br/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="company_code">Payroll Code</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="payroll_code" name="payroll_code" size="1">

                                                </select>
                                                <br/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="form-control-label"></label>
                                                <div>
                                                    <button class="btn btn-primary btn-sm pull-right push-10-l">Submit Filter</button>
                                                    <a href="{{ url("/admin/reports/payroll/listing/download?{$qs}") }}" class="btn btn-primary btn-sm pull-right" download>Download Excel <i class="fa fa-file-excel-o"></i></a>
                                                </div>
                                                <br />
                                            </div>
                                        </div>
                                    </div>

                                </form>



                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <br />
                                        <table id="payroll_listing_dt" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Company Code</th>
                                                <th>Payroll Group</th>
                                                <th>Payroll Code</th>
                                                <th>Employee Name</th>
                                                <th>Net Pay</th>
                                                <th>Coordinator</th>
                                                <th>Transfer Type</th>
                                                <th>Transfer Mode</th>
                                                <th>Account #</th>
                                                <th>GCASH #</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($payrollCycle))
                                                @foreach($payrollCycle->Dtr as $payrollMember)
                                                    <tr>
                                                        <td>{{ $payrollMember->Profile->EmploymentDetails->Principal->company_code }}</td>
                                                        <td>{{ $payrollMember->PayrollCycle->PayrollGroup->group_name }}</td>
                                                        <td>{{ $payrollMember->PayrollCycle->payroll_code }}</td>
{{--                                                        <td>{{ date('m/d/Y', strtotime($payrollMember->payrollCycle->payroll_date)) }}</td>--}}
                                                        <td>{{ $payrollMember->User->Profile->namelfm() }}</td>
                                                        <td>{{ number_format($payrollMember->Summary()->net_pay, 2) }}</td>
                                                        <td>@if(!empty($payrollMember->Profile->EmploymentDetails->Coordinator)) {{ $payrollMember->Profile->EmploymentDetails->Coordinator }} @endif</td>
                                                        <td>@if(!empty($payrollMember->Profile->Compensation->TransferMode->transfer_type)) {{ strtoupper($payrollMember->Profile->Compensation->TransferMode->transfer_type) }} @endif</td>
                                                        <td>@if(!empty($payrollMember->Profile->Compensation->TransferMode->transfer_mode)) {{ strtoupper($payrollMember->Profile->Compensation->TransferMode->transfer_mode) }} @endif</td>
                                                        <td>{{ strtoupper($payrollMember->Profile->Compensation->account_number) }}</td>
                                                        <td>{{ strtoupper($payrollMember->Profile->Compensation->mobile_number) }}</td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            var c = '{{ !empty($request['company_id']) ? $request['company_id'] : '' }}';
            var pg = '{{ !empty($request['payroll_group']) ? $request['payroll_group'] : '' }}';
            var pc = '{{ !empty($request['payroll_code']) ? $request['payroll_code'] : '' }}';

            initCompanies();
            function initCompanies() {
                if(c != '') {
                    $("#company").find('option').each(function() {
                        if($(this).val() == c) {
                            $(this).prop('selected', true);
                        }
                    });
                }

                var company = $("#company").find(':selected');

                var payrollGroups = company.data('payrollgroups');


                if(payrollGroups != undefined && payrollGroups.length > 0) {
                    $("#payroll_group").find('option').remove();
                    $("#payroll_group").append('<option value="All" data-payrollcycles=\'[]\' selected>All</option>');

                    $.each(payrollGroups, function(i, payrollGroup) {
                        if(pg != '' && pg == payrollGroup.id) {
                            $("#payroll_group").append('<option value="'+ payrollGroup.id +'" data-payrollcycles=\''+ JSON.stringify(payrollGroup.payroll_cycles) +'\'>'+ payrollGroup.group_name +'</option>');
                        } else {
                            $("#payroll_group").append('<option value="'+ payrollGroup.id +'" data-payrollcycles=\''+ JSON.stringify(payrollGroup.payroll_cycles) +'\'>'+ payrollGroup.group_name +'</option>');
                        }
                    });

                    var payrollCycles = $("#payroll_group").find(':selected').data('payrollcycles');

                    if(payrollCycles.length > 0) {
                        $("#payroll_code").find('option').remove();

                        payrollCycles.sort(function (a,b){
                            if (a.payroll_code < b.payroll_code)
                                return 1;
                            if (a.payroll_code> b.payroll_code)
                                return -1;
                            return 0;
                        });

                        $.each(payrollCycles, function(i, payrollCycle) {
                            if(payrollCycle.status == 'Processed') {
                                if(pc != '' && pc == payrollCycle.id) {
                                    $("#payroll_code").append('<option value="'+ payrollCycle.id +'" selected>'+ payrollCycle.payroll_code +'</option>');
                                } else {
                                    $("#payroll_code").append('<option value="'+ payrollCycle.id +'">'+ payrollCycle.payroll_code +'</option>');
                                }
                            }
                        });


                    }

                    if($("#payroll_group").find(':selected').val() == 'All') {
                        $("#payroll_code").find('option').remove();

                        // console.log($("#payroll_group").find(':selected').val());

                        $.each($("#payroll_group").find('option'), function(i, payrollGroup) {
                            var payrollCycles = $(this).data('payrollcycles');

                            payrollCycles.sort(function (a,b){
                                if (a.payroll_code < b.payroll_code)
                                    return 1;
                                if (a.payroll_code> b.payroll_code)
                                    return -1;
                                return 0;
                            });

                            // console.log('h');

                            $.each(payrollCycles, function(i, payrollCycle) {
                                if(payrollCycle.status == 'Processed') {
                                    $("#payroll_code").append('<option value="'+ payrollCycle.id +'">'+ payrollCycle.payroll_code +'</option>');
                                }
                            });
                        });
                    }
                } else {
                    $("#payroll_group").find('option').remove();

                    $("#payroll_code").find('option').remove();
                }
            }

            $("#company").change(function() {
                updateDropdowns();
            });

            $("#payroll_group").change(function() {
                var payrollCycles = $(this).find(':selected').data('payrollcycles');

                $("#payroll_code").find('option').remove();

                if(payrollCycles.length > 0) {


                    payrollCycles.sort(function (a,b){
                        if (a.payroll_code < b.payroll_code)
                            return 1;
                        if (a.payroll_code> b.payroll_code)
                            return -1;
                        return 0;
                    });

                    $.each(payrollCycles, function(i, payrollCycle) {
                        if(payrollCycle.status == 'Processed') {
                            $("#payroll_code").append('<option value="'+ payrollCycle.id +'">'+ payrollCycle.payroll_code +'</option>');
                        }
                    });
                }

                if($(this).find(':selected').val() == 'All') {
                    $("#payroll_code").find('option').remove();

                    $.each($(this).find('option'), function(i, payrollGroup) {
                        var payrollCycles = $(this).data('payrollcycles');

                        payrollCycles.sort(function (a,b){
                            if (a.payroll_code < b.payroll_code)
                                return 1;
                            if (a.payroll_code> b.payroll_code)
                                return -1;
                            return 0;
                        });


                        $.each(payrollCycles, function(i, payrollCycle) {
                            if(payrollCycle.status == 'Processed') {
                                $("#payroll_code").append('<option value="'+ payrollCycle.id +'">'+ payrollCycle.payroll_code +'</option>');
                            }
                        });
                    });
                }

                // updateDropdowns();
            });


            function updateDropdowns() {
                var company = $("#company").find(':selected');


                if(company.val() != '') {
                    var costCenters = company.data('costcenters');
                    var payrollGroups = company.data('payrollgroups');

                    $("#payroll_group").find('option').remove();
                    $("#payroll_code").find('option').remove();
                    $("#payroll_group").append('<option value="All" data-payrollcycles=\'[]\'>All</option>');

                    if(payrollGroups.length > 0) {

                        $.each(payrollGroups, function(i, payrollGroup) {
                            $("#payroll_group").append('<option value="'+ payrollGroup.id +'" data-payrollcycles=\''+ JSON.stringify(payrollGroup.payroll_cycles) +'\'>'+ payrollGroup.group_name +'</option>');
                        });

                        var payrollCycles = $("#payroll_group").find(':selected').data('payrollcycles');

                        if(payrollCycles.length > 0) {
                            $("#payroll_code").find('option').remove();

                            payrollCycles.sort(function (a,b){
                                if (a.payroll_code < b.payroll_code)
                                    return 1;
                                if (a.payroll_code> b.payroll_code)
                                    return -1;
                                return 0;
                            });

                            $.each(payrollCycles, function(i, payrollCycle) {
                                if(payrollCycle.status == 'Processed') {
                                    $("#payroll_code").append('<option value="'+ payrollCycle.id +'">'+ payrollCycle.payroll_code +'</option>');
                                }
                            });
                        }
                    }

                    if($("#payroll_group").find(':selected').val() == 'All') {
                        $("#payroll_code").find('option').remove();

                        $.each($("#payroll_group").find('option'), function(i, payrollGroup) {
                            var payrollCycles = $(this).data('payrollcycles');

                            payrollCycles.sort(function (a,b){
                                if (a.payroll_code < b.payroll_code)
                                    return 1;
                                if (a.payroll_code> b.payroll_code)
                                    return -1;
                                return 0;
                            });


                            $.each(payrollCycles, function(i, payrollCycle) {
                                if(payrollCycle.status == 'Processed') {
                                    $("#payroll_code").append('<option value="'+ payrollCycle.id +'">'+ payrollCycle.payroll_code +'</option>');
                                }
                            });
                        });
                    }


                } else {
                    $("#payroll_group").find('option').remove();
                    $("#payroll_group").append('<option>All</option>');
                }

                if(pg != '') {
                    $("#payroll_group").find('option').each(function() {
                        if($(this).val() == pg) {
                            $(this).prop('selected', true);
                        }
                    });
                }
            }
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('reports_payroll_payslip')}}">Payslip</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="reports_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('reports_payroll_listing') }}">Payroll Listing</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link " href="{{ route('reports_payroll_summary') }}">Payroll Summary</a>
                                        </li>
                                    </ul>
                                    <br />
                                </div>

                                <div class="col-md-6">

                                    {{-- This should be select2 --}}
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input class="form-control" type="text" id="search" name="search" >
                                                <span class="input-group-btn">
                                                        <button class="btn btn-primary" type="button">Search</button>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>

                                    <br />

                                    <div class="form-group">
                                        <label class="col-xs-12" for="employee_name">Employee Name</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="employee_name" name="employee_name" />
                                            <br />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-12" for="payroll_group">Payroll Group</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="payroll_group" name="payroll_group" size="1">
                                                <option value="payroll_group">Payroll Group</option>
                                            </select>
                                            <br/>
                                        </div>
                                    </div>

                                    <br />

                                    {{-- This should be select2--}}
                                    <div class="form-group">
                                        <label class="col-xs-12" for="payroll_code">Payroll Code</label>
                                        <div class="col-xs-12">
                                            <input class="form-control" type="text" id="payroll_code" name="payroll_code" />
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6 text-right">
                                    <button class="btn btn-sm btn-primary">GENERATE</button>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <br />
                                        <button class="btn btn-sm btn-primary"><i class="fa fa-arrow-right push-5-r"></i> Export</button>
                                        <button class="btn btn-sm btn-success"><i class="fa fa-print push-5-r"></i> Print</button>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <br />
                                        <table id="users-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Principal</th>
                                            <th>Payroll Group</th>
                                            <th>Employee ID Number</th>
                                            <th>Employee Name</th>
                                            <th>Net Pay</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

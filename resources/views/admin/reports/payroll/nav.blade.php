<div class="col-md-12">
    <ul class="nav nav-tabs" id="reports_tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link @if($nav == 'payroll-listing') active @endif" href="{{ route('reports_payroll_listing') }}">Payroll Listing</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($nav == 'payroll-summary') active @endif" href="{{ route('reports_payroll_summary') }}">Payroll Summary</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($nav == 'employee-summary') active @endif" href="{{ route('reports_payroll_employee') }}">Employee Summary</a>
        </li>
    </ul>
    <br>
</div>
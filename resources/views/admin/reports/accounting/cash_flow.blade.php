@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('reports_accounting_cash_flow')}}">Cash Flow</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                @include('admin.reports.accounting.nav')


                                <div class="col-md-12">

                                    <br>


                                    <form action="">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Search</label>
                                                    <input type="text" class="form-control" name="search">
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Bank Account</label>
                                                    <select name="bank" id="" class="form-control">
                                                        @foreach(\App\Banks::orderBy('bank_account')->get() as $bank)
                                                            <option value="{{ $bank->bank_account }}" @if(!empty($sBank) && $sBank == $bank->bank_account) selected @endif>{{ $bank->bank_account }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Date From</label>
                                                    <input class="form-control btn-mmddyyyy" type="text" id="date_from" name="date_from" value="{{ !empty($date_from) ? $date_from : '' }}" placeholder="">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Date To</label>
                                                    <input class="form-control btn-mmddyyyy" type="text" id="date_to" name="date_to" value="{{ !empty($date_to) ? $date_to : '' }}" placeholder="">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">&nbsp;</label>
                                                    <div>
                                                        <button class="btn btn-primary">Submit filter</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <br />
                                    <div class="col-md-12 text-right">
                                        <a href="{{ url("/admin/reports/accounting/cash_flow/download?{$qs}") }}" class="btn btn-primary btn-sm pull-right" download>Download Excel <i class="fa fa-file-excel-o"></i></a>
                                        {{--<button class="btn btn-sm btn-success"><i class="fa fa-print push-5-r"></i> Print</button>--}}
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <br />
                                    <table id="users-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Bank Account</th>
                                            <th>Status</th>
                                            <th>Entry Date</th>
                                            <th>Mode</th>
                                            <th>Check Number</th>
                                            <th>Particulars</th>
                                            <th>Total Amount</th>
                                            <th>Reference Number</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($cashFlows))
                                            <tbody>
                                            @if($cashFlows->count() > 0)
                                            @foreach($cashFlows as $cashFlow)
                                                <tr>
                                                    <td>{{ $cashFlow->bank }}</td>
                                                    <td>{{ $cashFlow->status }}</td>
                                                    <td>{{ date('m/d/Y', strtotime($cashFlow->entry_date)) }}</td>
                                                    <td>{{ $cashFlow->mode }}</td>
                                                    <td>{{ $cashFlow->check_number }}</td>
                                                    <td>{{ $cashFlow->particulars }}</td>
                                                    <td>{{ number_format($cashFlow->Items->sum('amount'), 2) }}</td>
                                                    <td>{{ $cashFlow->reference_number }}</td>
                                                </tr>
                                            @endforeach

                                            @else
                                            <tbody>
                                                <td colspan="8">No results to display.</td>
                                            </tbody>
                                            @endif
                                        @else
                                            <tbody>
                                                <td colspan="8">Please select date first.</td>
                                            </tbody>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

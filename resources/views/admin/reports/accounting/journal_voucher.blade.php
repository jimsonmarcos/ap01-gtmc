@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('reports_accounting_journal_voucher')}}">Journal Voucher</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                @include('admin.reports.accounting.nav')


                                <div class="col-md-12">

                                    <br>


                                    <form action="">
                                        <div class="row">

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Date From</label>
                                                    <input class="form-control btn-mmddyyyy" type="text" id="date_from" name="date_from" value="{{ !empty($date_from) ? $date_from : '' }}" placeholder="">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Date To</label>
                                                    <input class="form-control btn-mmddyyyy" type="text" id="date_to" name="date_to" value="{{ !empty($date_to) ? $date_to : '' }}" placeholder="">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">&nbsp;</label>
                                                    <div>
                                                        <button class="btn btn-primary">Submit filter</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <br />
                                    <div class="col-md-12 text-right">
                                        <a href="{{ url("/admin/reports/accounting/journal-voucher/download?{$qs}") }}" class="btn btn-primary btn-sm pull-right" download>Download Excel <i class="fa fa-file-excel-o"></i></a>
                                        {{--<button class="btn btn-sm btn-success"><i class="fa fa-print push-5-r"></i> Print</button>--}}
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <br />
                                    <table id="users-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>PARTICULARS</th>
                                            <th>DATE</th>
                                            <th>JV NUMBER</th>
                                            <th>TOTAL DEBIT</th>
                                            <th>TOTAL CREDIT</th>
                                            <th>GRAND TOTAL</th>
                                        </tr>
                                        </thead>
                                        @if(!empty($journalVouchers))
                                            @if($journalVouchers->count() > 0)
                                                @php
                                                    $total = 0;
                                                @endphp
                                                <tbody>
                                                @foreach($journalVouchers as $journalVoucher)
                                                    <tr>
                                                        <td>{{ $journalVoucher->particulars }}</td>
                                                        <td>{{ date('m/d/Y', strtotime($journalVoucher->date)) }}</td>
                                                        <td>{{ $journalVoucher->jv_number }}</td>
                                                        <td>{{ number_format($journalVoucher->Items->where('txn', 'Debit')->sum('debit'), 2) }}</td>
                                                        <td>{{ number_format($journalVoucher->Items->where('txn', 'Credit')->sum('credit'), 2) }}</td>
                                                        <td>{{ number_format($journalVoucher->Items->where('txn', 'Debit')->sum('debit') - $journalVoucher->Items->where('txn', 'Credit')->sum('credit'), 2) }}</td>
                                                    </tr>
                                                    @php
                                                        $total += $journalVoucher->Items->where('txn', 'Debit')->sum('debit') - $journalVoucher->Items->where('txn', 'Credit')->sum('credit');
                                                    @endphp
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="5">TOTAL</th>
                                                        <th>{{ number_format($total, 2) }}</th>
                                                    </tr>
                                                </tfoot>
                                            @else
                                            <tbody>
                                                <td colspan="8">No results to display.</td>
                                            </tbody>
                                            @endif
                                        @else
                                            <tbody>
                                                <td colspan="8">Please select date first.</td>
                                            </tbody>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

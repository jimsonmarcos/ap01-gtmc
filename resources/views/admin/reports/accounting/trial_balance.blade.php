@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('reports_accounting_trial_balance')}}">Trial Balance</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                @include('admin.reports.accounting.nav')


                                <div class="col-md-12">

                                    <br>


                                    <form action="">
                                        <div class="row">
                                            <?php
                                            $months = array(
                                                'January',
                                                'February',
                                                'March',
                                                'April',
                                                'May',
                                                'June',
                                                'July ',
                                                'August',
                                                'September',
                                                'October',
                                                'November',
                                                'December',
                                            );
                                            ?>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="example-select">Month</label>
                                                    <select class="form-control" id="" name="month">
                                                        @for($x = 0; $x < 12; $x++)
                                                            <option value="{{ $x + 1 }}">{{ $months[$x] }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Year</label>
                                                    <input class="form-control btn-yyyy" type="text" id="" name="year" value="{{ !empty($year) ? $year : '' }}" placeholder="">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">&nbsp;</label>
                                                    <div>
                                                        <button class="btn btn-primary">Submit filter</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <br />
                                    <div class="col-md-12 text-right">
                                        <a href="{{ url("/admin/reports/accounting/cash-collection/download?{$qs}") }}" class="btn btn-primary btn-sm pull-right" download>Download Excel <i class="fa fa-file-excel-o"></i></a>
                                        {{--<button class="btn btn-sm btn-success"><i class="fa fa-print push-5-r"></i> Print</button>--}}
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <br />
                                    <table id="users-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th class="text-center" colspan="2">BEGINNING BALANCE <br> January 1, 2018</th>
                                            <th class="text-center" colspan="2">ENDING BALANCE <br> January 31, 2018</th>
                                            <th class="text-center" colspan="2">AUDITOR'S ADJUSTMENTS</th>
                                            <th class="text-center" colspan="2">ADJUSTED TRIAL BALANCE <br> January 31, 2018</th>
                                        </tr>
                                        <tr>
                                            <th>Account Titles</th>
                                            <th>DEBIT</th>
                                            <th>CREDIT</th>
                                            <th>DEBIT</th>
                                            <th>CREDIT</th>
                                            <th>DEBIT</th>
                                            <th>CREDIT</th>
                                            <th>DEBIT</th>
                                            <th>CREDIT</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($cashFlows))
                                            <tbody>
                                            @if($cashFlows->count() > 0)
                                            @foreach($cashFlows as $cashFlow)
                                                <?php
                                                    if($cashFlow->source == 'Check Request') {
                                                        $source = App\CheckRequests::find($cashFlow->source_id);
                                                    }

                                                    if($cashFlow->source == 'Cash Receipt') {
                                                        $source = App\CashReceipts::find($cashFlow->source_id);
                                                    }

                                                ?>
                                                <tr>
                                                    <td>@if($cashFlow->source) {{ $cashFlow->source == 'Check Request' ? $source->payee_name : $source->payor_name }} @endif</td>
                                                    <td>{{ date('m/d/Y', strtotime($source->or_date)) }}</td>
                                                    <td>{{ $source->or_number }}</td>
                                                    <td>{{ $source->reference_number }}</td>
                                                    <td>{{ $cashFlow->bank }}</td>
                                                    <td>{{ date('m/d/Y', strtotime($cashFlow->entry_date)) }}</td>
                                                    <td>{{ number_format($cashFlow->Entries->where('txn', 'Debit')->sum('debit'), 2) }}</td>
                                                    <td>{{ number_format($cashFlow->Entries->where('txn', 'Credit')->sum('credit'), 2) }}</td>
                                                    <td>{{ number_format($cashFlow->Entries->where('txn', 'Debit')->sum('debit') - $cashFlow->Entries->where('txn', 'Credit')->sum('credit'), 2) }}</td>
                                                </tr>
                                            @endforeach

                                            @else
                                            <tbody>
                                                <td colspan="8">No results to display.</td>
                                            </tbody>
                                            @endif
                                        @else
                                            <tbody>
                                                <td colspan="8">Please select month and year first.</td>
                                            </tbody>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

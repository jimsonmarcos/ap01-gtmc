@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Accounting</li>
        <li><a class="link-effect" href="{{ route('reports_accounting_cash_flow')}}">Cash Flow</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                @include('admin\reports\accounting\nav')

                                <div class="col-md-12">

                                    <br>


                                    <form action="">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Search</label>
                                                    <input type="text" class="form-control" name="search">
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Principal</label>
                                                    <select name="bank" id="" class="form-control">
                                                        @foreach(\App\Companies::orderBy('company_code')->get() as $principal)
                                                            <option value="{{ $principal->id }}" @if(!empty($sBank) && $p == $principal->id->id) selected @endif>{{ $principal->company_code }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Date From</label>
                                                    <input class="form-control btn-mmddyyyy" type="text" id="date_from" name="date_from" value="{{ !empty($date_from) ? $date_from : '' }}" placeholder="">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Date To</label>
                                                    <input class="form-control btn-mmddyyyy" type="text" id="date_to" name="date_to" value="{{ !empty($date_to) ? $date_to : '' }}" placeholder="">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">&nbsp;</label>
                                                    <div>
                                                        <button class="btn btn-primary">Submit filter</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <br />
                                    <div class="col-md-12 text-right">
                                        <a href="{{ url("/admin/reports/accounting/sales_book/download?{$qs}") }}" class="btn btn-primary btn-sm pull-right" download>Download Excel <i class="fa fa-file-excel-o"></i></a>
                                        {{--<button class="btn btn-sm btn-success"><i class="fa fa-print push-5-r"></i> Print</button>--}}
                                    </div>

                                </div>

                                <div id="print-area" class="col-md-12">
                                    <div class="visible-print">
                                        <h2 class="content-heading text-center">Sales Book</h2>
                                        <br>
                                        <br>
                                        <br class="">
                                    </div>

                                    <br />
                                    <table id="users-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Billing Invoice</th>
                                            <th>Client</th>
                                            <th>Payroll Period</th>
                                            <th>Date</th>
                                            <th>Account Receivable - Trade</th>
                                            <th>Accounts Payable - Others</th>
                                            <th>Service Fee Income</th>
                                            <th>Remarks</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($salesBook))
                                            <?php $accountsPayable = \App\ChartOfAccounts::where('id', '>=', 52)->where('id', '<=', 67)->get(); ?>
                                            <tbody>
                                            @if($salesBook->count() > 0)
                                            @foreach($salesBook as $sb)
                                                <?php $cashFlow = \App\CashFlows::find($sb->cash_flow_id); ?>
                                                <tr>
                                                    <td>{{ $sb->invoice_number }}</td>
                                                    <td>{{ $sb->company_name }}</td>
                                                    <td>{{ $sb->reference }}</td>
                                                    <td>{{ date('m/d/Y', strtotime($sb->entry_date)) }}</td>
                                                    <td>{{ number_format($cashFlow->Entries->whereIn('account_title', ['Accounts Receivable-Trade (Manpower Services)', 'Accounts Receivable-Trade (Store-Head Office)', 'Accounts Receivable-Trade (Store-Roosevelt)'])->sum('debit'), 2) }}</td>
                                                    <td>{{ number_format($cashFlow->Entries->whereIn('account_title', $accountsPayable->pluck('id'))->sum('credit'), 2) }}</td>
                                                    <td>{{ number_format($cashFlow->Entries->where('account_title', 'Service Fee Income')->sum('debit'), 2) }}</td>
                                                    <td>{{ $cashFlow->remarks }}</td>
                                                </tr>
                                            @endforeach

                                            @else
                                            <tbody>
                                                <td colspan="8">No results to display.</td>
                                            </tbody>
                                            @endif
                                        @else
                                            <tbody>
                                                <td colspan="8">Please select date first.</td>
                                            </tbody>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

<div class="col-md-12">
    <ul class="nav nav-tabs" id="reports_tab" role="tablist">
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link @if($nav == 'cashflow') active @endif" href="{{ route('reports_accounting_cash_flow') }}">Cash Flow</a>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link @if($nav == 'sales-book') active @endif" href="{{ route('reports_accounting_sales_book') }}">Sales Book</a>--}}
        {{--</li>--}}
        <li class="nav-item">
            <a class="nav-link @if($nav == 'schedule') active @endif" href="{{ route('reports_accounting_schedule') }}">Schedule</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($nav == 'cash-collection') active @endif" href="{{ route('reports_accounting_cash_collection') }}">Cash Collection</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($nav == 'cash-disbursement') active @endif" href="{{ route('reports_accounting_cash_disbursement') }}">Cash Disbursement</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if($nav == 'journal-voucher') active @endif" href="{{ route('reports_accounting_journal_voucher') }}">Journal Voucher</a>
        </li>
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link @if($nav == 'trial-balance') active @endif" href="{{ route('reports_accounting_trial_balance') }}">Trial Balance</a>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link @if($nav == 'balance-sheet') active @endif" href="{{ route('reports_accounting_balance_sheet') }}">Balance Sheet</a>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link @if($nav == 'income-statement') active @endif" href="{{ route('reports_accounting_income_statement') }}">Income Statement</a>--}}
        {{--</li>--}}

        {{--<li class="nav-item">--}}
        {{--<a class="nav-link" href="{{ route('reports_accounting_financial_summary') }}">Financial Statement</a>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
        {{--<a class="nav-link" href="{{ route('reports_accounting_income_statement') }}">Income Statement</a>--}}
        {{--</li>--}}
    </ul>
</div>
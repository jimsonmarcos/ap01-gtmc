@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>User Profiles</li>
        <li><a class="link-effect" href="{{ route('reports_leave_credits')}}">Leave Credits</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="reports_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('reports_masterlist') }}">MasterList</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('reports_leave_credits') }}">Leave Credits</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12">
                                    <br>

                                    <form action="" method="GET">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="principal">Principal</label>
                                                <div class="col-xs-12">
                                                    <select name="company_id" id="company" class="form-control">
                                                        <option value="">All</option>
                                                        @foreach($companies as $company)
                                                            <option value="{{ $company->id }}" data-costcenters='{{ $company->CostCenters }}' data-payrollgroups='{{ $company->PayrollGroups }}'>{{ $company->company_code }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="company_code">Payroll Group</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="payroll_group" name="payroll_group" size="1">

                                                    </select>
                                                    <br/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="search">Search</label>
                                                <div class="col-xs-12">
                                                    <input type="text" id="search" name="search" class="form-control" />
                                                    <br/>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label"></label>
                                                    <div>
                                                        <button class="btn btn-primary btn-sm pull-right push-10-l">Submit Filter</button>
                                                        <a href="{{ url("/admin/reports/user_profiles/leave_credits/download?{$qs}") }}" class="btn btn-primary btn-sm pull-right" download>Download Excel <i class="fa fa-file-excel-o"></i></a>
                                                    </div>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                    </form>

                                </div>

                                <div class="col-md-12">
                                    <br />
                                    <table id="users-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Principal</th>
                                            <th>Payroll Group</th>
                                            <th>ID Number</th>
                                            <th>Complete Name</th>
                                            <th>Hire Date</th>
                                            <th>YTD Leave Credits (Days)</th>
                                            <th>YTD Accumulated Leave (Days)</th>
                                            <th>YTD Leave Balance (Days)</th>
                                            <th>YTD Leave Credits (Hrs)</th>
                                            <th>YTD Accumulated Leave (Hrs)</th>
                                            <th>YTD Leave Balance (Hrs)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                                <?php
                                                $leaveHistory = \App\LeaveHistory::select(['leave_history.*', 'payroll_cycles.payroll_date'])->where('leave_history.user_id', $user->user_id)->where('effective_year', date('Y'))->join('payroll_cycles', 'payroll_cycles.id', '=', 'leave_history.payroll_cycle_id')->orderByDesc('payroll_cycles.payroll_date')->get();
                                                ?>
                                                <tr>
                                                    <td>{{ $user->company_code }}</td>
                                                    <td>@if(!empty($user->Compensation->PayrollGroup)) {{ $user->Compensation->PayrollGroup->group_name }} @endif</td>
                                                    <td>{{ $user->id_number }}</td>
                                                    <td>{{ $user->namelfm() }}</td>
                                                    <td>{{ date('Y-m-d', strtotime($user->hire_date)) }}</td>
                                                    <td>@if(\Carbon\Carbon::createFromFormat('Y-m-d', $user->employmentdetails->hire_date)->age > 0) 5 @else 0 @endif</td>
                                                    <td>{{ $leaveHistory->where('status', 'Paid')->sum('days') }}</td>
                                                    <td>@if(\Carbon\Carbon::createFromFormat('Y-m-d', $user->employmentdetails->hire_date)->age > 0 && $leaveHistory->where('status', 'Paid')->sum('days') < 5) {{ 5 - $leaveHistory->where('status', 'Paid')->sum('days') }} @else 0 @endif</td>
                                                    <td>@if(\Carbon\Carbon::createFromFormat('Y-m-d', $user->employmentdetails->hire_date)->age > 0) {{ 5 * 8 }} @else 0 @endif</td>
                                                    <td>{{ $leaveHistory->where('status', 'Paid')->sum('hours') }}</td>
                                                    <td>@if(\Carbon\Carbon::createFromFormat('Y-m-d', $user->employmentdetails->hire_date)->age > 0 && $leaveHistory->where('status', 'Paid')->sum('hours') < 40) {{ (5 * 8) - $leaveHistory->where('status', 'Paid')->sum('hours') }} @else 0 @endif</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

            var c = '{{ !empty($request['company_id']) ? $request['company_id'] : '' }}';
            var pg = '{{ !empty($request['payroll_group']) ? $request['payroll_group'] : '' }}';

            initCompanies();
            function initCompanies() {
                if(c != '') {
                    $("#company").find('option').each(function() {
                        if($(this).val() == c) {
                            $(this).prop('selected', true);
                        }
                    });
                }

                var company = $("#company").find(':selected');

                var payrollGroups = company.data('payrollgroups');


                if(payrollGroups != undefined && payrollGroups.length > 0) {
                    $("#payroll_group").find('option').remove();
                    $("#payroll_group").append('<option>All</option>');
                    $.each(payrollGroups, function(i, payrollGroup) {
                        if(pg != '' && pg == payrollGroup.id) {
                            $("#payroll_group").append('<option value="'+ payrollGroup.id +'" selected>'+ payrollGroup.group_name +'</option>');
                        } else {
                            $("#payroll_group").append('<option value="'+ payrollGroup.id +'">'+ payrollGroup.group_name +'</option>');
                        }

                    });
                } else {
                    $("#payroll_group").find('option').remove();
                    $("#payroll_group").append('<option>All</option>');
                }
            }

            $("#company").change(function() {
                updateDropdowns();
            });


            function updateDropdowns() {
                var company = $("#company").find(':selected');


                if(company.val() != '') {
                    var costCenters = company.data('costcenters');
                    var payrollGroups = company.data('payrollgroups');


                    if(payrollGroups.length > 0) {
                        $("#payroll_group").find('option').remove();
                        $("#payroll_group").append('<option>All</option>');
                        $.each(payrollGroups, function(i, payrollGroup) {
                            $("#payroll_group").append('<option value="'+ payrollGroup.id +'">'+ payrollGroup.group_name +'</option>');
                        });
                    }
                } else {
                    $("#payroll_group").find('option').remove();
                    $("#payroll_group").append('<option>All</option>');
                }

                if(pg != '') {
                    $("#payroll_group").find('option').each(function() {
                        if($(this).val() == pg) {
                            $(this).prop('selected', true);
                        }
                    });
                }
            }

        })
    </script>
@endsection

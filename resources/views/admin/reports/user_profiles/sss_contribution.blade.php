@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>User Profiles</li>
        <li><a class="link-effect" href="{{ route('report_sss_contribution')}}">SSS EE_ER Contribution</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="reports_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('reports_masterlist') }}">MasterList</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('reports_coop_members_list') }}">Coop Members List</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('reports_employee_members_list') }}">Employee Members List</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link " href="{{ route('reports_leave_credits') }}">Leave Credits</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('report_sss_contribution') }}">SSS EE_ER Contribution</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('report_hdmf_contribution') }}">HDMF EE_ER Contribution</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12">
                                    <br />
                                    <div class="col-md-6">
                                        <button class="btn btn-sm btn-primary"><i class="fa fa-arrow-right push-5-r"></i> Export</button>
                                        <button class="btn btn-sm btn-success"><i class="fa fa-print push-5-r"></i> Print</button>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input class="form-control" type="text" id="search" name="search" >
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-primary" type="button">Search</button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <br />
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="date_range">Date Range</label>
                                        <div class="col-md-8">
                                            <div class="input-daterange input-group" data-date-format="mm/dd/yyyy">
                                                <input class="form-control" type="text" id="date_range_from" name="date_range_from" placeholder="From">
                                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                                <input class="form-control" type="text" id="date_range_to" name="date_range_to" placeholder="To">
                                            </div>
                                        </div>
                                    </div>
                                </div>




                                <div class="col-md-12">
                                    <br />
                                    <table id="users-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Complete Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Company Code</th>
                                            <th>Admin Type</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

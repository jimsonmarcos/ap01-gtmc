@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>User Profiles</li>
        <li><a class="link-effect" href="{{ route('reports_coop_members_list')}}">Coop Members List</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="reports_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('reports_masterlist') }}">MasterList</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('reports_coop_members_list') }}">Coop Members List</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('reports_employee_members_list') }}">Employee Members List</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('reports_leave_credits') }}">Leave Credits</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('report_sss_contribution') }}">SSS EE_ER Contribution</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('report_hdmf_contribution') }}">HDMF EE_ER Contribution</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12">
                                    <br />
                                    {{--<div class="col-md-6">--}}
                                        {{--<button class="btn btn-sm btn-primary"><i class="fa fa-arrow-right push-5-r"></i> Export</button>--}}
                                        {{--<button class="btn btn-sm btn-success"><i class="fa fa-print push-5-r"></i> Print</button>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-md-6">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<div class="col-md-12">--}}
                                                {{--<div class="input-group">--}}
                                                    {{--<input class="form-control" type="text" id="search" name="search" >--}}
                                                    {{--<span class="input-group-btn">--}}
                                                        {{--<button class="btn btn-primary" type="button">Search</button>--}}
                                                    {{--</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                </div>

                                <div class="col-md-12">
                                    <br />
                                    <table id="dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Complete Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Company Code</th>
                                            <th>Admin Type</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach(\App\User::where('role', 'Member')->with('profile')->get() as $profile)
                                            <tr>
                                                <td>{{ $profile->Profile->last_name }}, {{ $profile->Profile->first_name }} {{ $profile->Profile->middle_name }}</td>
                                                <td>{{ $profile->username }}</td>
                                                <td>{{ $profile->email }}</td>
                                                <td>@if(!empty($profile->Profile->EmploymentDetails->Principal->company_code)) {{ $profile->Profile->EmploymentDetails->Principal->company_code }} @endif</td>
                                                {{--<td>@if(!empty($profile->admin_type)) {{ $profile->admin_type }} @else {{ $profile->role }} @endif</td>--}}
                                                <td>{{ $profile->role }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(document).ready(function() {
                $("#dt").dataTable();
            })
        })
    </script>
@endsection

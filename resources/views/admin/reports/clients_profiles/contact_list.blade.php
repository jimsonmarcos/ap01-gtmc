@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Client Profiles</li>
        <li><a class="link-effect" href="{{ route('reports_client_contact_list')}}">Contact List</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    @include('admin.reports.clients_profiles.nav')
                                </div>

                                <div class="col-md-12">
                                    <br />
                                    <div class="col-md-6">
                                        <button class="btn btn-sm btn-primary"><i class="fa fa-arrow-right push-5-r"></i> Export</button>
                                        <button class="btn btn-sm btn-success"><i class="fa fa-print push-5-r"></i> Print</button>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input class="form-control" type="text" id="search" name="search" >
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-primary" type="button">Search</button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <br />
                                    <table id="users-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Complete Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Company Code</th>
                                            <th>Admin Type</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

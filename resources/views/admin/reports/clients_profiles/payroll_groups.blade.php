@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Client Profiles</li>
        <li><a class="link-effect" href="{{ route('reports_client_payroll_groups') }}">Payroll Group</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    @include('admin.reports.clients_profiles.nav')
                                </div>

                                <div class="col-md-12">
                                    <br />
                                    {{--<div class="col-md-12 text-right">--}}
                                    {{--<a href="{{ url("/admin/reports/client_profiles/payroll_groups/download?{$qs}") }}" class="btn btn-sm btn-primary" download>Download Excel <i class="fa fa-file-excel-o push-5-r"></i></a>--}}
                                    {{--<button id="print" class="btn btn-sm btn-primary">Print <i class="fa fa-print push-5-r"></i></button>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-md-6">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<div class="col-md-12">--}}
                                            {{--<div class="input-group">--}}
                                            {{--<input class="form-control" type="text" id="search" name="search" >--}}
                                            {{--<span class="input-group-btn">--}}
                                            {{--<button class="btn btn-primary" type="button">Search</button>--}}
                                            {{--</span>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <form>


                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="principal">Status</label>
                                            <div class="col-xs-12">
                                                <select name="status" id="" class="form-control">
                                                    <option value="All" @if(Request::get('status') == 'All') selected @endif>All</option>
                                                    <option value="Active" @if(Request::get('status') == 'Active') selected @endif>Active</option>
                                                    <option value="Deactivated" @if(Request::get('status') == 'Deactivated') selected @endif>Deactivated</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="principal">Principal</label>
                                            <div class="col-xs-12">
                                                <select name="company_id" id="company" class="form-control">
                                                    @if(empty($companies))
                                                        <option value="">N/A</option>
                                                    @else
                                                        <option value=""></option>
                                                        @foreach($principals as $company)
                                                            <option value="{{ $company->id }}" @if(Request::get('company_id') == $company->id) selected @endif>{{ $company->company_code }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="form-control-label"></label>
                                                <div>
                                                    <button class="btn btn-primary btn-sm pull-right push-10-l">Submit Filter</button>
                                                    <a href="{{ url("/admin/reports/client_profiles/payroll_groups/download?{$qs}") }}" class="btn btn-primary btn-sm pull-right" download>Download Excel <i class="fa fa-file-excel-o"></i></a>
                                                </div>
                                                <br />
                                            </div>
                                        </div>
                                    </div>

                                    </form>

                                </div>


                            </div>

                            <div class="col-md-12">
                                <br />
                                <table id="" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Company Code</th>
                                        <th>Payroll Group</th>
                                        <th>Start Date A</th>
                                        <th>Payroll Date A</th>
                                        <th>Start Date B</th>
                                        <th>Payroll Date B</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($companies as $company)
                                        @foreach($company->PayrollGroups as $payrollGroup)
                                            <tr>
                                                <td>{{ $company->company_code }}</td>
                                                <td>{{ $payrollGroup->group_name }}</td>
                                                <td>{{ $payrollGroup->start_date_a }}</td>
                                                <td>{{ $payrollGroup->payroll_date_a }}</td>
                                                <td>{{ $payrollGroup->start_date_b }}</td>
                                                <td>{{ $payrollGroup->payroll_date_b }}</td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            // $("#dt").dataTable({
            //     'pageLength': 100,
            //     "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ]
            // });

            $("#print").click(function() {
                $("#printArea").print();
            })
        })
    </script>
@endsection

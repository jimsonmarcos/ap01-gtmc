@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Client Profiles</li>
        <li><a class="link-effect" href="{{ route('reports_client_cost_centers') }}">Cost Centers</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    @include('admin.reports.clients_profiles.nav')
                                </div>

                                <div class="col-md-12">
                                    <br />

                                    <form>


                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="principal">Status</label>
                                                <div class="col-xs-12">
                                                    <select name="status" id="" class="form-control">
                                                        <option value="All" @if(Request::get('status') == 'All') selected @endif>All</option>
                                                        <option value="Active" @if(Request::get('status') == 'Active') selected @endif>Active</option>
                                                        <option value="Deactivated" @if(Request::get('status') == 'Deactivated') selected @endif>Deactivated</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="principal">Principal</label>
                                                <div class="col-xs-12">
                                                    <select name="company_id" id="company" class="form-control">
                                                        <option value=""></option>
                                                        @foreach($principals as $company)
                                                            <option value="{{ $company->id }}" @if(Request::get('company_id') == $company->id) selected @endif>{{ $company->company_code }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label"></label>
                                                    <div>
                                                        <button class="btn btn-primary btn-sm pull-right push-10-l">Submit Filter</button>
                                                        <a href="{{ url("/admin/reports/client_profiles/cost_centers/download?{$qs}") }}" class="btn btn-primary btn-sm pull-right" download>Download Excel <i class="fa fa-file-excel-o"></i></a>
                                                    </div>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                    </form>

                                </div>


                            </div>

                            <div class="col-md-12">
                                <br />
                                <div id="printArea">
                                    <table id="" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Company Code</th>
                                            <th>Cost Center</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($costCenters as $costCenter)
                                            <tr>
                                                <td>{{ $costCenter->Company->company_code }}</td>
                                                <td>{{ $costCenter->cost_center }}</td>
                                                <td>{{ $costCenter->Company->status }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            // $("#dt").dataTable({
            //     'pageLength': 100,
            //     "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ]
            // });

            $("#print").click(function() {
                $("#printArea").print();
            })
        })
    </script>
@endsection

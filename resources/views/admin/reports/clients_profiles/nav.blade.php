<ul class="nav nav-tabs" id="reports_tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link @if($pageHeader == 'Masterlist') active @endif" href="{{ route('reports_client_masterlist') }}">MasterList</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if($pageHeader == 'Cost Center') active @endif" href="{{ route('reports_client_cost_centers') }}">Cost Center</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if($pageHeader == 'Payroll Group') active @endif" href="{{ route('reports_client_payroll_groups') }}">Payroll Group</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if($pageHeader == 'Client Contact Person') active @endif" href="{{ route('reports_client_contact_persons') }}">Client Contact Person</a>
    </li>
    {{--<li class="nav-item">--}}
        {{--<a class="nav-link " href="{{ route('reports_client_contact_list') }}">Client Contacts</a>--}}
    {{--</li>--}}
</ul>
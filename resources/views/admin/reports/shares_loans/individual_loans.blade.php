@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Shares and Loans</li>
        <li><a class="link-effect" href="{{ route('reports_shares_loans_account_statement')}}">Statement of Account</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="reports_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('reports_shares_loans_gtmc') }}">Loans</a>
                                        </li>
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link " href="{{ route('reports_shares_loans_sss_hdmf') }}">SSS/HDMF Loan</a>--}}
                                        {{--</li>--}}
                                        <li class="nav-item">
                                            <a class="nav-link " href="{{ route('reports_shares_loans_account_statement') }}">Loan Payment</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link " href="{{ route('reports_shares') }}">Shares</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link " href="{{ route('reports_individual_shares')  }}">Individual Shares</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('reports_individual_loans') }}">Individual Loans</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12">

                                    <form>
                                        <div class="col-md-4">
                                            <br />
                                            <div class="form-group">
                                                <label class="col-xs-12" for="person">Select A Person</label>
                                                <div class="col-xs-12">
                                                    <select name="membership_category" id="membership_category" class="form-control">
                                                        <option value="Employee">Employee</option>
                                                        <option value="Member">Member</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <br />
                                            <div class="form-group">
                                                <label class="col-xs-12" for="search">Search</label>

                                                <select name="" id="search" class="form-control"></select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <br /><br />
                                            <button id="print" type="button" class="pull-right btn btn-primary"><i class="fa fa-print push-5-r"></i> Print</button>
                                        </div>
                                    </form>

                                </div>

                                <div id="print-area" class="col-xs-12">
                                    <div class="col-md-12">
                                        <br /><br />
                                        <h2 class="content-heading text-center">LOAN ACCOUNT</h2>

                                        <div class="col-xs-6">
                                            <h6>Name: <span id="name"></span></h6>
                                        </div>

                                        <div class="col-xs-6">
                                            <h6>Join Date: <span id="joinDate"></span></h6>
                                        </div>

                                        <div class="col-xs-6">
                                            <h6>ID Number: <span id="idNumber"></span></h6>
                                        </div>

                                        <div class="col-xs-6">
                                            <h6>Category: <span id="category"></span></h6>
                                        </div>

                                        <div class="col-xs-6">
                                            <h6>Cost Center: <span id="costCenter"></span></h6>
                                        </div>

                                        <div class="col-xs-6">
                                            <h6>Membership Type: <span id="membershipType"></span></h6>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <br>
                                        <br>
                                        <br class=" visible-print">
                                    </div>

                                    <div class="col-md-12">
                                        <table id="individual-loans-datatable" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th class="p-cla">CLA #</th>
                                                <th>Loan Type</th>
                                                <th>Date</th>
                                                <th>Loan Amount</th>
                                                <th>Interest Paid</th>
                                                <th>AR #</th>
                                                <th>Payment Method</th>
                                                <th>Pay Period</th>
                                                <th>Payment</th>
                                                <th>Balance</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="10">No selected user yet</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>
        .select2-container {
            width: 100% !important;
        }

        @media print {
            .p-cla {
                width: 110px !important;
            }
        }
    </style>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            // $("#individual-loans-datatable").dataTable({
            //     'pageLength': 100,
            //     "lengthMenu": [ [100, 200, 500, -1], [100, 200, 500, "All"] ]
            // });

            $("#print").click(function() {
                $.print("#print-area");
                // $("#print-area").print();
            });


            updateMembershipCategory();

            $("#membership_category").change(function() {
                updateMembershipCategory();
            });

            function updateMembershipCategory() {
                var membershipCategory = $("#membership_category").find(':selected').val();

                var url = "{{ url('api/reports/users') }}/";


                $("#search").select2({
                    placeholder: {
                        id: '-1', // the value of the option
                        id_number: "Search "+ membershipCategory +" name here..."
                    },
                    ajax: {
                        url: url + membershipCategory,
                        dataType: 'json',
                        delay: 300,
                        data: function (params) {
                            return {
                                q: params.term, // search term
//                            page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            // parse the results into the format expected by Select2
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data, except to indicate that infinite
                            // scrolling can be used
                            params.page = params.page || 1;

                            // console.log();

                            var items = data.items;
                            $.each(items, function(index, value) {
                                // console.log(value);
                                value.id = value.user_id;
                            });


                            return {
                                results: data.items,
                                pagination: {
                                    more: (params.page * 30) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                    minimumInputLength: 3,
                    templateResult: formatRepo,
                    templateSelection: formatRepoSelection
                });

                // console.log();

            }

            function formatRepo (user) {
                if (user.loading) {
                    return user.id_number;
                }
//                console.log(user);

                var markup = "<span>"+ user.id_number +"</span>"

                return markup;
            }

            function formatRepoSelection (user) {
                return user.id_number;
            }

            $("#search").on("select2:select", function (evt) {
                var user = evt.params.data;
                // console.log(user);

                var url = "{{ url('api/reports/individual-loans') }}/" + user.user_id;

                $("#name").text(user.last_name + ", " + user.first_name);
                $("#idNumber").text(user.id_number);
                $("#costCenter").text(user.company_code + ' - ' + user.cost_center);
                $("#joinDate").text(user.join_date);
                $("#category").text(user.membership_category);
                $("#membershipType").text(user.membership_type);


                $("#page-loader").show();

                $.get(url, function(result) {
                    $("#page-loader").hide();

                    var result = $.parseJSON(result);

                    $("#individual-loans-datatable").find('tbody > tr').remove();

                    if(result.length > 0) {
                        $.each(result, function(key, val) {
                            console.log(val);
                            if(val.type.length > 0) {
                                var loanType = val.type;
                            } else {
                                var loanType = val.loan_type;
                            }

                            $("#individual-loans-datatable").find('tbody').append("<tr>" +
                                "<td>"+ val.cla_number +"</td>" +
                                "<td>"+ loanType +"</td>" +
                                "<td>"+ val.date_request +"</td>" +
                                "<td>"+ parseFloat(val.request_amount).numberFormat(2) +"</td>" +
                                "<td>"+ parseFloat(val.total_interest_paid).numberFormat(2) +"</td>" +
                                "<td>"+ val.ar_number +"</td>" +
                                "<td>"+ val.payment_method +"</td>" +
                                "<td>"+ val.pay_period +"</td>" +
                                "<td>"+ parseFloat(val.total_amount_paid).numberFormat(2) +"</td>" +
                                "<td>"+ parseFloat(val.outstanding_balance).numberFormat(2) +"</td>" +
                                "</tr>");
                        });
                    } else {
                        $("#individual-loans-datatable").find('tbody').append('<tr><td colspan="10">No loans to display.</td></tr>');
                    }

                });
            });
        })
    </script>
@endsection

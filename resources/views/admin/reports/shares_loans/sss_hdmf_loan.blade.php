@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Shares and Loans</li>
        <li><a class="link-effect" href="{{ route('reports_shares_loans_sss_hdmf')}}">SSS/HDMF Loan</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="reports_tab" role="tablist">
                                        <ul class="nav nav-tabs" id="reports_tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('reports_shares_loans_gtmc') }}">GTMC Request Loans</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="{{ route('reports_shares_loans_sss_hdmf') }}">SSS/HDMF Loan</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link " href="{{ route('reports_shares_loans_account_statement') }}">Loan Payment</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link " href="{{ route('reports_shares') }}">Shares</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('reports_individual_shares')  }}">Individual Shares</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('reports_individual_loans') }}">Individual Loans</a>
                                            </li>
                                        </ul>
                                    </ul>
                                </div>

                                <div class="col-md-12">
                                    <br />
                                    <div class="col-md-6">
                                        <button class="btn btn-sm btn-primary"><i class="fa fa-arrow-right push-5-r"></i> Export</button>
                                        <button class="btn btn-sm btn-success"><i class="fa fa-print push-5-r"></i> Print</button>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input class="form-control" type="text" id="search" name="search" >
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-primary" type="button">Search</button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <br />
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="filter_by_name">Filter By Name</label>
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" id="filter_by_name" name="filter_by_name" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="unbound">Unbound</label>
                                            <div class="col-xs-12">
                                                <select class="form-control" id="unbound" name="unbound" size="1">
                                                    <option value="unbound">Unbound</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <label class="col-xs-12" for="loan_type">Type of Loan</label>
                                        <div class="col-xs-6">
                                            <label class="css-input css-radio css-radio-default push-10-r">
                                                <input type="radio" name="type_of_loans" checked=""><span></span> All
                                            </label>
                                            <label class="css-input css-radio css-radio-default">
                                                <input type="radio" name="type_of_loans"><span></span> SSS
                                            </label>
                                            <label class="css-input css-radio css-radio-default">
                                                <input type="radio" name="type_of_loans"><span></span> HDMF
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-right">
                                    <br />
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-plus push-5-r"></i> Generate</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <br />
                                    <table id="users-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Complete Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Company Code</th>
                                            <th>Admin Type</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

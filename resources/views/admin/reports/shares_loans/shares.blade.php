@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Shares and Loans</li>
        <li><a class="link-effect" href="{{ route('reports_shares_loans_account_statement')}}">Statement of Account</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="reports_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('reports_shares_loans_gtmc') }}">Loans</a>
                                        </li>
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link " href="{{ route('reports_shares_loans_sss_hdmf') }}">SSS/HDMF Loan</a>--}}
                                        {{--</li>--}}
                                        <li class="nav-item">
                                            <a class="nav-link " href="{{ route('reports_shares_loans_account_statement') }}">Loan Payment</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('reports_shares') }}">Shares</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('reports_individual_shares')  }}">Individual Shares</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('reports_individual_loans') }}">Individual Loans</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12">
                                    <br>

                                    <form action="" method="GET">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="principal">Principal</label>
                                                <div class="col-xs-12">
                                                    <select name="company_id" id="company" class="form-control">
                                                        <option value="">All</option>
                                                        @foreach($companies as $company)
                                                            <option value="{{ $company->id }}" data-costcenters='{{ $company->CostCenters }}' data-payrollgroups='{{ $company->PayrollGroups }}'>{{ $company->company_code }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="company_code">Payroll Group</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="payroll_group" name="payroll_group" size="1">

                                                    </select>
                                                    <br/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="membership_type">Membership Type</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="membership_type" name="membership_type" size="1">
                                                        <option value="">All</option>
                                                        <option value="Associate" @if(!empty($request['membership_type']) && $request['membership_type'] == 'Associate') selected @endif>Associate</option>
                                                        <option value="Regular" @if(!empty($request['membership_type']) && $request['membership_type'] == 'Regular') selected @endif>Regular</option>
                                                    </select>
                                                    <br/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="membership_status">Membership Status</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="membership_status" name="membership_status" size="1">
                                                        <option value="Active">Active</option>
                                                        <option value="Inactive" @if(!empty($request['membership_status']) && $request['membership_status'] == 'Inactive') selected @endif>Inactive</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="employment_status">Employment Status</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="employment_status" name="employment_status" size="1">
                                                        <option value="Active">Active</option>
                                                        <option value="Deactivated" @if(!empty($request['employment_status']) && $request['employment_status'] == 'Deactivated') selected @endif>Deactivated</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="hire_date_from">Join Date From</label>
                                                <div class="col-xs-12">
                                                    <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                        <input type="text" class="form-control" id="date_from" name="join_date_from" placeholder="mm/dd/yyyy" value="@if(!empty($request['date_from'])) {{ date('m/d/Y', strtotime($request['date_from'])) }} @endif">
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="hire_date_from">Join Date To</label>
                                                <div class="col-xs-12">
                                                    <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                        <input type="text" class="form-control" id="date_to" name="join_date_to" placeholder="mm/dd/yyyy" value="@if(!empty($request['date_to'])) {{ date('m/d/Y', strtotime($request['date_to'])) }} @endif">
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="cost_center">Cost Center</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="cost_center" name="cost_center" size="1">

                                                    </select>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="category">Category</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="category" name="category" size="1">
                                                        <option value="">All</option>
                                                        <option value="Employee" @if(!empty($request['category']) && $request['category'] == 'Employee') selected @endif>Employee</option>
                                                        <option value="Member" @if(!empty($request['category']) && $request['category'] == 'Member') selected @endif>Member</option>
                                                    </select>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="category">Search</label>
                                                <div class="col-xs-12">
                                                    <input type="text" id="search" name="search" class="form-control">
                                                </div>
                                            </div>
                                        </div>



                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label"></label>
                                                    <div>
                                                        <button class="btn btn-primary btn-sm pull-right push-10-l">Submit Filter</button>
                                                        <a href="{{ url("/admin/reports/shares_and_loans/shares/download?{$qs}") }}" class="btn btn-primary btn-sm pull-right" download>Download Excel <i class="fa fa-file-excel-o"></i></a>
                                                    </div>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                    </form>

                                </div>

                                <div class="col-md-12">
                                    <br />
                                    <table id="shares-datatable" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Company Code</th>
                                            <th>Payroll Group</th>
                                            <th>Cost Center</th>
                                            <th>Membership Type</th>
                                            <th>Category</th>
                                            <th>GTMC ID Number</th>
                                            <th>Complete Name</th>
                                            <th>Join Date</th>
                                            <th>Total Shares</th>
                                            <th>Employment Status</th>
                                            <th>Membership Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{ $user->company_code }}</td>
                                                <td>{{ $user->payroll_group }}</td>
                                                <td>{{ $user->cost_center }}</td>
                                                <td>{{ $user->membership_type }}</td>
                                                <td>{{ $user->membership_category }}</td>
                                                <td>{{ $user->id_number }}</td>
                                                <td>{{ $user->namelfm() }}</td>
                                                <td>{{ date('m/d/Y', strtotime($user->join_date)) }}</td>
                                                <td class="text-right">{{ number_format($user->total_shares, 2) }}</td>
                                                <td>{{ $user->EmploymentDetails->status }}</td>
                                                <td>{{ $user->status }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            var c = '{{ !empty($request['company_id']) ? $request['company_id'] : '' }}';
            var pg = '{{ !empty($request['payroll_group']) ? $request['payroll_group'] : '' }}';
            var cc = '{{ !empty($request['cost_center']) ? $request['cost_center'] : '' }}';

            initCompanies();
            function initCompanies() {
                if(c != '') {
                    $("#company").find('option').each(function() {
                        if($(this).val() == c) {
                            $(this).prop('selected', true);
                        }
                    });
                }

                var company = $("#company").find(':selected');

                var costCenters = company.data('costcenters');
                var payrollGroups = company.data('payrollgroups');

                if(costCenters != undefined && costCenters.length > 0) {
                    $("#cost_center").find('option').remove();
                    $("#cost_center").append('<option>All</option>');
                    $.each(costCenters, function(i, costCenter) {
                        if(cc != '' && cc == costCenter.id) {
                            $("#cost_center").append('<option value="'+ costCenter.id +'" selected>'+ costCenter.cost_center +'</option>');
                        } else {
                            $("#cost_center").append('<option value="'+ costCenter.id +'">'+ costCenter.cost_center +'</option>');
                        }

                    });
                } else {
                    $("#cost_center").find('option').remove();
                    $("#cost_center").append('<option>All</option>');
                }

                if(payrollGroups != undefined && payrollGroups.length > 0) {
                    $("#payroll_group").find('option').remove();
                    $("#payroll_group").append('<option>All</option>');
                    $.each(payrollGroups, function(i, payrollGroup) {
                        if(pg != '' && pg == payrollGroup.id) {
                            $("#payroll_group").append('<option value="'+ payrollGroup.id +'" selected>'+ payrollGroup.group_name +'</option>');
                        } else {
                            $("#payroll_group").append('<option value="'+ payrollGroup.id +'">'+ payrollGroup.group_name +'</option>');
                        }

                    });
                } else {
                    $("#payroll_group").find('option').remove();
                    $("#payroll_group").append('<option>All</option>');
                }
            }

            $("#company").change(function() {
                updateDropdowns();
            });

            // updateDropdowns();



            function updateDropdowns() {
                var company = $("#company").find(':selected');





                if(company.val() != '') {
                    var costCenters = company.data('costcenters');
                    var payrollGroups = company.data('payrollgroups');

                    if(costCenters.length > 0) {
                        $("#cost_center").find('option').remove();
                        $("#cost_center").append('<option>All</option>');
                        $.each(costCenters, function(i, costCenter) {
                            $("#cost_center").append('<option value="'+ costCenter.id +'">'+ costCenter.cost_center +'</option>');
                        });
                    }

                    if(payrollGroups.length > 0) {
                        $("#payroll_group").find('option').remove();
                        $("#payroll_group").append('<option>All</option>');
                        $.each(payrollGroups, function(i, payrollGroup) {
                            $("#payroll_group").append('<option value="'+ payrollGroup.id +'">'+ payrollGroup.group_name +'</option>');
                        });
                    }
                } else {
                    $("#payroll_group").find('option').remove();
                    $("#payroll_group").append('<option>All</option>');

                    $("#cost_center").find('option').remove();
                    $("#cost_center").append('<option>All</option>');
                }

                if(cc != '') {
                    $("#cost_center").find('option').each(function() {
                        if($(this).val() == cc) {
                            $(this).prop('selected', true);
                        }
                    });
                }

                if(pg != '') {
                    $("#payroll_group").find('option').each(function() {
                        if($(this).val() == pg) {
                            $(this).prop('selected', true);
                        }
                    });
                }
            }

            initCategory();
            $("#category").change(function() {
                initCategory();
            });

            function initCategory() {
                if($("#category").find(':selected').val() == 'Member') {
                    $("#employment_status, #date_from, #date_to, #payroll_group").prop('disabled', true);
                } else {
                    $("#employment_status, #date_from, #date_to, #payroll_group").prop('disabled', false);
                }
            }
        })
    </script>
@endsection

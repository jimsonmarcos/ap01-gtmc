<!-- Main Navbar-->
<header class="header">
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
                <!-- Navbar Header-->
                <div class="navbar-header">
                    <!-- Navbar Brand --><a href="index.html" class="navbar-brand">
                        <div class="brand-text brand-big hidden-lg-down">
                            <strong>GTMC</strong> <span>Multi-Purpose System</span>
                        </div>
                        <div class="brand-text brand-small"><strong>GTMC</strong></div></a>
                    <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
                    <small><?php echo date('F t, Y h:i:s A'); ?></small>
                </div>
                <!-- Navbar Menu -->
                <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                    <!-- Logout -->
                    <li class="nav-item"><a href="{{ url('logout') }}" class="nav-link logout">Logout<i class="fa fa-sign-out"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>


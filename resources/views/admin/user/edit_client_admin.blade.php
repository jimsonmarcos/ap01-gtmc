@extends('layouts.app')

@section('content')

                <!-- Dashboard Counts Section-->
                <section class="dashboard-counts no-padding-bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="" class="form" method="POST" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label">First Name</label>
                                                        <input id="clientFirstName" type="text" class="form-control" name="first_name" value="{{ strtoupper($client->Client->first_name) }}" required />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Middle Name</label>
                                                        <input id="clientMiddleName" type="text" class="form-control" name="middle_name" value="{{ strtoupper($client->Client->middle_name) }}" required />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Last Name</label>
                                                        <input id="clientLastName" type="text" class="form-control" name="last_name" value="{{ strtoupper($client->Client->last_name) }}" required />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Email Address</label>
                                                        <input id="" type="email" class="form-control" name="email" value="{{ strtolower($client->Client->email) }}" required />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Company Code</label>
                                                        <select name="company_id" id="clientCompanyCode" class="form-control">
                                                            @foreach(\App\Companies::orderBy('company_code')->get() as $company)
                                                                <option value="{{ $company->id }}" @if($client->Client->Company->id == $company->id) selected @endif>{{ $company->company_code }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Admin Type</label>
                                                        <div class="select">
                                                            <select name="role" class="form-control" required>
                                                                <option value="ClientAdmin" @if($client->client_type == 'ClientAdmin') selected @endif>Client Admin</option>
                                                                <option value="ClientUser" @if($client->client_type == 'ClientUser') selected @endif>Client User</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="row justify-content-md-center no-padding">
                                                        <div class="col-md-6 text-center no-padding-left">
                                                            <h4>Username</h4>
                                                            <div class="alert alert-primary" role="alert">
                                                                <strong id="generatedUsername">xxxxx</strong>
                                                            </div>
                                                            <input type="hidden" id="clientUsername" name="username">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 text-right">
                                                    <button class="btn btn-success"><span class="fa fa-save"></span> Submit</button>
                                                    {{--<a href="{{ url('admin/user/add_admin_success') }}" class="btn btn-success">Submit</a>--}}
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#clientFirstName, #clientMiddleName, #clientLastName").on('keyup', function() {
                updateUsername();
            });

            $("#clientCompanyCode").change(function() {
               updateUsername();
            });

            function updateUsername() {
                if($("#clientFirstName").val().trim().length > 0 && $("#clientMiddleName").val().trim().length > 0 && $("#clientLastName").val().trim().length > 0) {
                    var clientUsername = $("#clientCompanyCode").find(':selected').text().trim().toLowerCase() + '.' + $("#clientFirstName").val().trim().substring(0, 1).toLowerCase() + $("#clientMiddleName").val().trim().substring(0, 1).trim().toLowerCase() + $("#clientLastName").val().trim().toLowerCase().replace(' ', '_');
                    $("#generatedUsername").text(clientUsername);
                    $("#clientUsername").attr('value', clientUsername);
                }
            }

            updateUsername();
        });
    </script>
@endsection

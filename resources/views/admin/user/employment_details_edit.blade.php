@extends('layouts.app')

@section('content')


                <!-- Dashboard Counts Section-->
                <section class="dashboard-counts no-padding-bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row no-padding">
                                            <div class="col-md-6">
                                                <strong>Name: {{ $user->name() }}</strong>
                                            </div>

                                            <div class="col-md-6">
                                                <strong>ID Number: {{ $user->id_number }}</strong>
                                            </div>

                                            <div class="col-md-12">
                                                <hr>
                                            </div>

                                            <div class="col-md-12">
                                                <ul class="nav nav-tabs">
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/user/{$id}/profile") }}">Personal Information</a>
                                                    </li>
                                                    @if($user->User->role != 'Member')
                                                        <li class="nav-item">
                                                            <a class="nav-link active" href="{{ url("admin/user/{$id}/employment-details") }}">Employment Details</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="{{ url("admin/user/{$id}/compensations-leave-credits") }}">Compensation and Leave Credits</a>
                                                        </li>
                                                    @endif
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="col-md-12">
                                                <div style="max-width: 600px;"></div>
                                                <br>

                                                <form action="" method="POST" enctype="multipart/form-data">
                                                    {{ csrf_field() }}

                                                    <div class="row no-padding">
                                                        @if(!empty($user->employmentdetails->reason) && date('Y-m-d', strtotime($user->employmentdetails->effective_date)) <= date('Y-m-d'))
                                                        <div class="col-md-4">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="is_active" value="Y"> Change Employment Status to <span class="text-success"><strong>Active</strong></span>?
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12"></div>
                                                        @endif

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Principal</label>
                                                                <select name="principal_id" id="" class="form-control">
                                                                    @foreach(\App\Companies::orderBy('company_code')->get() as $company)
                                                                        <option value="{{ $company->id }}" @if($company->id == $employmentDetails->principal_id) selected @endif>{{ $company->company_code }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Hire Date</label>
                                                                <div class='input-group btn-mmddyyyy'>
                                                                    <input type='text' class="form-control" name="hire_date" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $employmentDetails->hire_date)->format('m/d/Y') }}" placeholder="mm/dd/yyyy" required />
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        @if($employmentDetails->principal_id == 8 && in_array($employmentDetails->position_id, [1, 2]))
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Coordinator</label>
                                                                <select class="form-control" name="coordinator_id">
                                                                    <option value=""></option>
                                                                    @foreach($coordinators as $coordinator)
                                                                        <option value="{{ $coordinator->user_id }}" @if($employmentDetails->coordinator_id == $coordinator->user_id) selected @endif>{{ $coordinator->Profile->name() }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        @endif

                                                        <div class="col-md-12"></div>

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="form-control-label">Position</label>
                                                                    <select name="position_id" id="selectPosition" class="form-control">
                                                                        @foreach(\App\Positions::orderBy('title')->get() as $position)
                                                                            <option value="{{ $position->id }}" name="position_id" @if($position->id == $employmentDetails->position_id) selected @endif>{{ $position->title }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                        <div class="trainee-fields" @if($employmentDetails->position_id != 1) style="display: none;" @endif>
                                                                            <input id="option" type="checkbox" value="Y" name="orientation" @if($employmentDetails->orientation == 'Y') checked @endif @if($employmentDetails->position_id != 1) trainee-fields @endif>
                                                                            <label for="option">Orientation</label>
                                                                        </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4 trainee-fields" @if($employmentDetails->position_id != 1) style="display: none;" @endif>
                                                                <div class="form-group">
                                                                    <label class="form-control-label">Trainee Start Date</label>
                                                                    <div class='input-group btn-mmddyyyy'>
                                                                        <input type='text' class="form-control" name="trainee_start_date" value="{{ !empty($employmentDetails->trainee_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d', $employmentDetails->trainee_start_date)->format('m/d/Y') : date('m/d/y') }}" placeholder="mm/dd/yyyy" required @if($employmentDetails->position_id != 1) disabled @endif />
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4 trainee-fields" @if($employmentDetails->position_id != 1) style="display: none;" @endif>
                                                                <div class="form-group">
                                                                    <label class="form-control-label">Trainee Start Date</label>
                                                                    <div class='input-group btn-mmddyyyy'>
                                                                        <input type='text' class="form-control" name="trainee_end_date" value="{{ !empty($employmentDetails->trainee_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d', $employmentDetails->trainee_end_date)->format('m/d/Y') : date('m/d/y') }}" placeholder="mm/dd/yyyy" required @if($employmentDetails->position_id != 1) disabled @endif />
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>

{{--                                                        @if(in_array($employmentDetails->position_id, [1, 2]))--}}
                                                        {{--@if(!in_array($employmentDetails->position_id, [1, 2])) style="display: none;" @endif--}}
                                                        {{--@if(!in_array($employmentDetails->position_id, [1, 2])) disabled @endif--}}
                                                        <div class="col-md-12  trainee-promoter-fields" ></div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="form-control-label">City Province</label>
                                                                <select id="add-ed-cityprovince" class="form-control" name="province_id" required>
                                                                    <option value="">&nbsp;</option>
                                                                    @foreach($provinces as $province)
                                                                        <option value="{{ $province->id }}" data-areas='{{ $province->Areas->sortBy('area')->toJson() }}' @if($employmentDetails->province_id == $province->id) selected @endif>{{ $province->province }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Area</label>
                                                                <select id="add-ed-area" class="form-control" name="area_id">
                                                                    <option value="">&nbsp;</option>
                                                                    @if(!empty($employmentDetails->area_id))
                                                                        @if(\App\Areas::where('id', $employmentDetails->area_id)->count() > 0)
                                                                            <?php $area = \App\Areas::find($employmentDetails->area_id); ?>
                                                                            @foreach(\App\Areas::where('province_id', $area->province_id)->orderBy('area')->get() as $a)
                                                                                <option value="{{ $a->id }}" @if($a->id == $employmentDetails->area_id) selected @endif>{{ $a->area }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Mall</label>
                                                                <select id="add-ed-mall" class="form-control" name="mall_id">
                                                                    <option value="">&nbsp;</option>
                                                                    @foreach($malls as $mall)
                                                                        <option value="{{ $mall->id }}" @if($employmentDetails->mall_id == $mall->id) selected @endif>{{ $mall->mall }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Outlet</label>
                                                                <select id="add-ed-outlet" class="form-control" name="outlet_id">
                                                                    <option value="">&nbsp;</option>
                                                                    @foreach($outlets as $outlet)
                                                                        <option value="{{ $outlet->id }}" @if($employmentDetails->outlet_id == $outlet->id) selected @endif>{{ $outlet->outlet }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        {{--<div class="col-md-4  trainee-promoter-fields" >--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label class="form-control-label">City Province</label>--}}
                                                                {{--<select id="add-ed-cityprovince" class="form-control" >--}}
                                                                    {{--@foreach($provinces as $province)--}}
                                                                        {{--<option @if($province->province == $department->province) selected @endif>{{ $province->province }}</option>--}}
                                                                    {{--@endforeach--}}
                                                                {{--</select>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-4  trainee-promoter-fields" >--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label class="form-control-label">Area</label>--}}
                                                                {{--<select id="add-ed-area" class="form-control" >--}}
                                                                    {{--@foreach($areas as $area)--}}
                                                                        {{--<option value="{{ $area->area }}" @if($area->area == $department->area) selected @endif>{{ $area->area }}</option>--}}
                                                                    {{--@endforeach--}}
                                                                {{--</select>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-4  trainee-promoter-fields" >--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label class="form-control-label">Mall</label>--}}
                                                                {{--<select id="add-ed-mall" class="form-control" >--}}
                                                                    {{--@foreach($malls as $mall)--}}
                                                                        {{--<option value="{{ $area->mall }}"  @if($mall->mall == $department->mall) selected @endif>{{ $mall->mall }}</option>--}}
                                                                    {{--@endforeach--}}
                                                                {{--</select>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-4  trainee-promoter-fields" >--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label class="form-control-label">Outlet</label>--}}
                                                                {{--<select id="add-ed-outlet" class="form-control" >--}}
                                                                    {{--@foreach($outlets as $outlet)--}}
                                                                        {{--<option value="{{ $area->outlet }}"  @if($outlet->outlet == $department->outlet) selected @endif>{{ $outlet->outlet }}</option>--}}
                                                                    {{--@endforeach--}}
                                                                {{--</select>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-4  trainee-promoter-fields" >--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label class="form-control-label">Department</label>--}}
                                                                {{--<select id="add-ed-department" class="form-control" name="department_id" >--}}
                                                                    {{--@foreach($departments as $d)--}}
                                                                        {{--<option value="{{ $d->id }}" @if($d->department == $department->department) selected @endif>{{ $d->department }}</option>--}}
                                                                    {{--@endforeach--}}
                                                                {{--</select>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--@endif--}}



                                                        <div class="col-md-12">
                                                            <br>
                                                            <br>

                                                            <div>
                                                                <button class="btn btn-primary pull-right"><span class="fa fa-save"></span> Confirm</button>
                                                                <a href="{{ url("admin/user/{$id}/employment-details") }}" class="btn btn-default pull-right" style="margin-right: 5px;"><span class="fa fa-ban"></span> Cancel</a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(document).on('change', '#add-ed-cityprovince', function(e) {
                var areas = $.parseJSON($(this).find(":selected").attr('data-areas'));
                console.log(areas);
                console.log(areas.length);

                $("#add-ed-area").find('option').remove();
                $("#add-ed-area").append("<option value=''>&nbsp;</option>")
//                if(areas.length > 0) {
                    $.each(areas, function(index, json) {
                        console.log(json);
                        $("#add-ed-area").append("<option value='"+ json.id +"'>"+ json.area +"</option>");
                    });
//                }
            });

            $(document).on('change', '#selectPosition', function(e) {
               var positionID = $(this).find(':selected').val();
//               console.log(positionID);

               if(positionID == '1') {
                   console.log('Trainee');
                   $(".trainee-fields").slideDown();
                   $(".trainee-fields").find('input').removeAttr('disabled');
//                   $(".trainee-promoter-fields").slideDown();
//                   $(".trainee-promoter-fields").find('input, select').removeAttr('disabled');
               } else if(positionID == '2') {
//                    $(".trainee-promoter-fields").slideDown();
//                    $(".trainee-promoter-fields").find('input, select').removeAttr('disabled');
                    $(".trainee-fields").slideUp();
                    $(".trainee-fields").find('input').attr('disabled', true);
               } else {
                   $(".trainee-fields").slideUp();
                   $(".trainee-fields").find('input').attr('disabled', true);
//                   $(".trainee-promoter-fields").find('input, select').attr('disabled', true);
               }

            });


            {{--$(document).on('change', '#add-ed-cityprovince', function(e) {--}}
                {{--e.preventDefault();--}}

                {{--$("#add-ed-area, #add-ed-mall, #add-ed-outlet, #add-ed-department").find('option').remove();--}}
                {{--$("#add-ed-area, #add-ed-mall, #add-ed-outlet, #add-ed-department").attr('disabled', 'disabled');--}}
                {{--$("#add-ed-area").append("<option>Loading..</option>");--}}

                {{--var data = {--}}
                    {{--_token : '{{ csrf_token() }}',--}}
                    {{--province : $(this).find(':selected').text()--}}
                {{--};--}}
{{--//               console.log($(this).find(':selected').text());--}}
                {{--$.post('{{ url("api/department/areas") }}', data, function(response) {--}}
{{--//                  console.log(response);--}}
                    {{--var areas = $.parseJSON(response);--}}
                    {{--var option = '';--}}
                    {{--$.each(areas, function(index, json) {--}}
                        {{--var v = json.area;--}}
                        {{--if(json.area.length == 0) {--}}
                            {{--v = 'N/A';--}}
                        {{--}--}}
                        {{--option = option + '<option value="'+ json.area +'">'+ v +'</option>';--}}
                    {{--});--}}

                    {{--$("#add-ed-area > option").remove();--}}
                    {{--$("#add-ed-area").append(option);--}}
                    {{--$("#add-ed-area").removeAttr('disabled');--}}

                    {{--setTimeout(function() {--}}
                        {{--reloadDepartments();--}}
                    {{--}, 300);--}}
                {{--});--}}
            {{--});--}}

            {{--$(document).on('change', '#add-ed-area', function(e) {--}}
                {{--e.preventDefault();--}}

                {{--$("#add-ed-mall, #add-ed-outlet, #add-ed-department").find('option').remove();--}}
                {{--$("#add-ed-mall, #add-ed-outlet, #add-ed-department").attr('disabled', 'disabled');--}}
                {{--$("#add-ed-mall").append("<option>Loading..</option>");--}}

                {{--var data = {--}}
                    {{--_token : '{{ csrf_token() }}',--}}
                    {{--province : $('#add-ed-cityprovince').find(':selected').text(),--}}
                    {{--area : $(this).find(':selected').attr('value')--}}
                {{--};--}}

                {{--console.log(data);--}}

                {{--$.post('{{ url("api/department/malls") }}', data, function(response) {--}}

                    {{--var malls = $.parseJSON(response);--}}
                    {{--var option = '';--}}
                    {{--$.each(malls, function(index, json) {--}}
                        {{--var v = json.mall;--}}
                        {{--if(json.mall.length == 0) {--}}
                            {{--v = 'N/A';--}}
                        {{--}--}}
                        {{--option = option + '<option value="'+ json.mall +'">'+ v +'</option>';--}}
                    {{--});--}}

                    {{--$("#add-ed-mall > option").remove();--}}
                    {{--$("#add-ed-mall").append(option);--}}
                    {{--$("#add-ed-mall").removeAttr('disabled');--}}

                    {{--setTimeout(function() {--}}
                        {{--reloadDepartments();--}}
                    {{--}, 300);--}}
                {{--});--}}
            {{--});--}}

            {{--$(document).on('change', '#add-ed-mall', function(e) {--}}
                {{--e.preventDefault();--}}

                {{--$("#add-ed-outlet, #add-ed-department").find('option').remove();--}}
                {{--$("#add-ed-outlet, #add-ed-department").attr('disabled', 'disabled');--}}
                {{--$("#add-ed-outlet").append("<option>Loading..</option>");--}}

                {{--var data = {--}}
                    {{--_token : '{{ csrf_token() }}',--}}
                    {{--province : $('#add-ed-cityprovince').find(':selected').text(),--}}
                    {{--area : $('#add-ed-area').find(':selected').attr('value'),--}}
                    {{--mall : $("#add-ed-mall").find(':selected').attr('value')--}}
                {{--};--}}

                {{--console.log(data);--}}

                {{--$.post('{{ url("api/department/outlets") }}', data, function(response) {--}}

                    {{--var outlets = $.parseJSON(response);--}}
                    {{--var option = '';--}}
                    {{--$.each(outlets, function(index, json) {--}}
                        {{--var v = json.outlet;--}}
                        {{--if(json.outlet.length == 0) {--}}
                            {{--v = 'N/A';--}}
                        {{--}--}}
                        {{--option = option + '<option value="'+ json.outlet +'">'+ v +'</option>';--}}
                    {{--});--}}

                    {{--$("#add-ed-outlet > option").remove();--}}
                    {{--$("#add-ed-outlet").append(option);--}}
                    {{--$("#add-ed-outlet").removeAttr('disabled');--}}

                    {{--setTimeout(function() {--}}
                        {{--reloadDepartments();--}}
                    {{--}, 300);--}}
                {{--});--}}
            {{--});--}}

            {{--$(document).on('change', '#add-ed-outlet', function(e) {--}}
                {{--e.preventDefault();--}}

                {{--$("#add-ed-department").find('option').remove();--}}
                {{--$("#add-ed-department").attr('disabled', 'disabled');--}}
                {{--$("#add-ed-department").append("<option>Loading..</option>");--}}

                {{--var data = {--}}
                    {{--_token : '{{ csrf_token() }}',--}}
                    {{--province : $('#add-ed-cityprovince').find(':selected').text(),--}}
                    {{--area : $('#add-ed-area').find(':selected').attr('value'),--}}
                    {{--mall : $('#add-ed-mall').find(':selected').attr('value'),--}}
                    {{--outlet : $(this).find(':selected').attr('value')--}}
                {{--};--}}

                {{--$.post('{{ url("api/department/departments") }}', data, function(response) {--}}

                    {{--var departments = $.parseJSON(response);--}}
                    {{--var option = '';--}}
                    {{--$.each(departments, function(index, json) {--}}
                        {{--option = option + '<option value="'+ json.id +'">'+ json.department +'</option>';--}}
                    {{--});--}}

                    {{--$("#add-ed-department > option").remove();--}}
                    {{--$("#add-ed-department").append(option);--}}
                    {{--$("#add-ed-department").removeAttr('disabled');--}}
                {{--});--}}
            {{--});--}}

            {{--function reloadDepartments() {--}}
                {{--if($("#add-ed-area option").length > 0 && $("#add-ed-mall option").length == 0) {--}}

                    {{--if($('#add-ed-area').find(':selected').text() != 'Loading..') {--}}
                        {{--$("#add-ed-mall, #add-ed-outlet, #add-ed-department").find('option').remove();--}}
                        {{--$("#add-ed-mall, #add-ed-outlet, #add-ed-department").attr('disabled', 'disabled');--}}
                        {{--$("#add-ed-mall").append("<option>Loading..</option>");--}}

                        {{--var data = {--}}
                            {{--_token : '{{ csrf_token() }}',--}}
                            {{--province : $('#add-ed-cityprovince').find(':selected').text(),--}}
                            {{--area : $("#add-ed-area").find(':selected').attr('value')--}}
                        {{--};--}}
                        {{--$.post('{{ url("api/department/malls") }}', data, function(response) {--}}

                            {{--var malls = $.parseJSON(response);--}}
                            {{--var option = '';--}}
                            {{--$.each(malls, function(index, json) {--}}
                                {{--var v = json.mall;--}}
                                {{--if(json.mall.length == 0) {--}}
                                    {{--v = 'N/A';--}}
                                {{--}--}}
                                {{--option = option + '<option value="'+ json.mall +'">'+ v +'</option>';--}}
                            {{--});--}}

                            {{--$("#add-ed-mall > option").remove();--}}
                            {{--$("#add-ed-mall").append(option);--}}
                            {{--$("#add-ed-mall").removeAttr('disabled');--}}

                            {{--setTimeout(function() {--}}
                                {{--reloadDepartments();--}}
                            {{--}, 300);--}}

                        {{--});--}}
                    {{--}--}}


                {{--}--}}

                {{--if($("#add-ed-mall option").length > 0 && $("#add-ed-outlet option").length == 0) {--}}

                    {{--if($('#add-ed-mall').find(':selected').text() != 'Loading..') {--}}

                        {{--$("#add-ed-outlet, #add-ed-department").find('option').remove();--}}
                        {{--$("#add-ed-outlet, #add-ed-department").attr('disabled', 'disabled');--}}
                        {{--$("#add-ed-outlet").append("<option>Loading..</option>");--}}

                        {{--var data = {--}}
                            {{--_token : '{{ csrf_token() }}',--}}
                            {{--province : $('#add-ed-cityprovince').find(':selected').text(),--}}
                            {{--area : $('#add-ed-area').find(':selected').attr('value'),--}}
                            {{--mall : $('#add-ed-mall').find(':selected').attr('value')--}}
                        {{--};--}}
                        {{--$.post('{{ url("api/department/outlets") }}', data, function(response) {--}}

                            {{--var outlets = $.parseJSON(response);--}}
                            {{--var option = '';--}}
                            {{--$.each(outlets, function(index, json) {--}}
                                {{--var v = json.outlet;--}}
                                {{--if(json.outlet.length == 0) {--}}
                                    {{--v = 'N/A';--}}
                                {{--}--}}
                                {{--option = option + '<option value="'+ json.outlet +'">'+ v +'</option>';--}}
                            {{--});--}}

                            {{--$("#add-ed-outlet > option").remove();--}}
                            {{--$("#add-ed-outlet").append(option);--}}
                            {{--$("#add-ed-outlet").removeAttr('disabled');--}}

                            {{--setTimeout(function() {--}}
                                {{--reloadDepartments();--}}
                            {{--}, 300);--}}
                        {{--});--}}
                    {{--}--}}

                {{--}--}}

                {{--if($("#add-ed-outlet option").length > 0 && $("#add-ed-department option").length == 0) {--}}

                    {{--if($('#add-ed-outlet').find(':selected').text() != 'Loading..') {--}}
                        {{--$("#add-ed-department").find('option').remove();--}}
                        {{--$("#add-ed-department").attr('disabled', 'disabled');--}}
                        {{--$("#add-ed-department").append("<option>Loading..</option>");--}}

                        {{--var data = {--}}
                            {{--_token : '{{ csrf_token() }}',--}}
                            {{--province : $('#add-ed-cityprovince').find(':selected').text(),--}}
                            {{--area : $('#add-ed-area').find(':selected').attr('value'),--}}
                            {{--mall : $('#add-ed-mall').find(':selected').attr('value'),--}}
                            {{--outlet : $('#add-ed-outlet').find(':selected').attr('value')--}}
                        {{--};--}}
                        {{--$.post('{{ url("api/department/departments") }}', data, function(response) {--}}

                            {{--var departments = $.parseJSON(response);--}}
                            {{--var option = '';--}}
                            {{--$.each(departments, function(index, json) {--}}
                                {{--option = option + '<option value="'+ json.id +'">'+ json.department +'</option>';--}}
                            {{--});--}}

                            {{--$("#add-ed-department > option").remove();--}}
                            {{--$("#add-ed-department").append(option);--}}
                            {{--$("#add-ed-department").removeAttr('disabled');--}}
                        {{--});--}}
                    {{--}--}}
                {{--}--}}
            {{--}--}}
        });
    </script>
@endsection

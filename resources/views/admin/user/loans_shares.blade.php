@extends('layouts.app')

@section('content')


            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row no-padding">
                                        <div class="col-md-12">
                                            <a href="{{ url('/admin/users') }}"><i class="fa fa-arrow-left"></i> Go back to list</a>
                                            <br><br>
                                        </div>

                                        <div class="col-md-6">
                                            <strong>Name: {{ $user->name() }}</strong>
                                        </div>

                                        <div class="col-md-6">
                                            <strong>ID Number: {{ $user->id_number }}</strong>
                                        </div>

                                        <div class="col-md-12">
                                            <hr>
                                        </div>

                                        <div class="col-md-12">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/profile") }}">Personal Information</a>
                                                </li>
                                                @if($user->User->role != 'Member')
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/employment-details") }}">Employment Details</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/compensations-leave-credits") }}">Compensation and Leave Credits</a>
                                                </li>
                                                @endif
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="col-md-12">
                                            @if(in_array(Illuminate\Support\Facades\Auth::user()->role, ['SuperAdmin', 'Admin', 'AdminHR', 'AdminCoop']))
                                            <div style="">
                                                <a href="#" class="btn btn-sm btn-primary pull-right editContribution" style="margin-top: 10px;"><span class="fa fa-edit"></span> Edit Loans and Shares</a>
                                            </div>
                                            @endif
                                            <br>
                                            <br>
                                            <br>

                                            @if(\App\FinalPays::where(['user_id' => $user->user_id, 'status' => 'Released'])->exists())
                                                    <p class="text-right">Membership Status: <strong class="text-danger">Terminated</strong></p>
                                            @else
                                                    <p class="text-right">Membership Status: <strong class="@if($user->status == 'Active') text-success @else text-danger @endif">{{ $user->status }}</strong></p>
                                            @endif


                                            <div class="row no-padding">
                                                <div class="col-md-4">
                                                    <h4>Cooperative</h4>
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td>Membership Type</td>
                                                                <td>{{ $user->membership_type }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cost Center</td>
                                                                <td>{{ !empty($user->CostCenter->cost_center) ? $user->CostCenter->cost_center : 'N/A' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Contribution</td>
                                                                <td>@if(!empty($user->coop_share)) {{ number_format($user->coop_share, 2) }} @else 0 @endif</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Membership Fee</td>
                                                                <td>{{ number_format(\App\OtherPayments::where(['user_id' => $user->user_id, 'transaction_type' => 'MF'])->sum('total_amount_paid'), 2) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Notarial Fee</td>
                                                                <td>{{ number_format(\App\OtherPayments::where(['user_id' => $user->user_id, 'transaction_type' => 'NF'])->sum('total_amount_paid'), 2) }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="col-md-4">
                                                    <h4>Company</h4>
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td>Coop Share</td>
                                                                <td>{{ number_format(\App\OtherPayments::where(['user_id' => $user->user_id, 'transaction_type' => 'COOP SHARE'])->sum('total_amount_paid'), 2) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Coop Savings</td>
                                                                <td>{{ number_format(\App\Loans::where(['user_id' => $user->user_id, 'releasing_status' => 'Released'])->sum('savings'), 2) }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="col-md-4">
                                                    <h4>Government</h4>
                                                    <table class="table">
                                                        <?php $payrollSummary = \App\PayrollSummary::where('user_id', $user->user_id); ?>
                                                        <tbody>
                                                            <tr>
                                                                <td class="text-right">SSS EE & ER</td>
                                                                <td class="text-right">{{ number_format($payrollSummary->sum('sss_ee') + $payrollSummary->sum('sss_er'), 2) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right">HDMF EE & EE</td>
                                                                <td class="text-right">{{ number_format($payrollSummary->sum('hdmf_ee') + $payrollSummary->sum('hdmf_er'), 2) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right">PhilHealth EE & EE</td>
                                                                <td class="text-right">{{ number_format($payrollSummary->sum('philhealth_ee') + $payrollSummary->sum('philhealth_er'), 2) }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div id="beneficariesAcc" data-children=".beneficiaryItems">
                                                    <div class="beneficiaryItems">
                                                        <a class="btn btn-sm btn-primary" data-toggle="collapse" data-parent="#beneficariesAcc" href="#beneficaries" aria-expanded="true" aria-controls="beneficaries">
                                                            View Beneficiaries
                                                        </a>
                                                        <div id="beneficaries" class="collapse" role="tabpanel">
                                                            <a href="#" id="addBeneficiary" class="btn btn-sm btn-primary pull-right"><span class="fa fa-plus"></span> Add Beneficiary</a>
                                                            <br>
                                                            <br>
                                                            <table class="table table-striped table-sm" id="beneficiaries">
                                                                <thead>
                                                                    <tr>
                                                                        <th>First Name</th>
                                                                        <th>Middle Name</th>
                                                                        <th>Last Name</th>
                                                                        <th>Birth Date</th>
                                                                        <th>Relationship to Member</th>
                                                                        <th></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @if($beneficiaries->count() > 0)
                                                                        @foreach($beneficiaries as $beneficiary)
                                                                            <tr>
                                                                                <td>{{ $beneficiary->first_name }}</td>
                                                                                <td>{{ $beneficiary->middle_name }}</td>
                                                                                <td>{{ $beneficiary->last_name }}</td>
                                                                                <td>{{ $beneficiary->birthdate() }}</td>
                                                                                <td>{{ $beneficiary->MemberRelationship->title }}</td>
                                                                                <td>
                                                                                    <a href="#" class="btn btn-sm btn-primary editBeneficiary" data-id="{{ $beneficiary->id }}"><span class="fa fa-edit"></span> </a>
                                                                                    <a href="#" class="btn btn-sm btn-danger deleteBeneficiary" data-id="{{ $beneficiary->id }}"><span class="fa fa-remove"></span> </a>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @else
                                                                        <tr>
                                                                            <td colspan="5">No Beneficiaries yet.</td>
                                                                        </tr>
                                                                    @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr>
                                            </div>

                                            <!-- TODO: Loans -->
                                            {{--<div class="col-md-12">--}}
                                                {{--<h4>Loan Payments <span class="text-danger">WIP</span></h4>--}}

                                                {{--<table class="table">--}}
                                                    {{--<thead>--}}
                                                        {{--<tr>--}}
                                                            {{--<th>Date</th>--}}
                                                            {{--<th>Loan Type</th>--}}
                                                            {{--<th>Loan Amount</th>--}}
                                                            {{--<th>Total Paid Amount</th>--}}
                                                            {{--<th>Outstanding Balance</th>--}}
                                                        {{--</tr>--}}
                                                    {{--</thead>--}}
                                                    {{--<tbody>--}}
                                                        {{--<tr>--}}
                                                            {{--<td>9/9/2015</td>--}}
                                                            {{--<td>SSS Salary Loan</td>--}}
                                                            {{--<td>5,000.00</td>--}}
                                                            {{--<td>3,000.00</td>--}}
                                                            {{--<td>2,000.00</td>--}}
                                                        {{--</tr>--}}
                                                        {{--<tr>--}}
                                                            {{--<td>5/21/2017</td>--}}
                                                            {{--<td>HDMF Multi-Purpose Loan</td>--}}
                                                            {{--<td>15,500.00</td>--}}
                                                            {{--<td>8,700.00</td>--}}
                                                            {{--<td>6,800.00</td>--}}
                                                        {{--</tr>--}}
                                                    {{--</tbody>--}}
                                                {{--</table>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div id="deleteBeneficiary" tabindex="-1" role="dialog" aria-labelledby="deleteBeneficiary" class="modal fade text-left" aria-hidden="true" style="display: none;">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="exampleModalLabel" class="modal-title">Delete Beneficiary</h4>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Beneficiary?</p>
            </div>
            <div class="modal-footer">
                <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                <a id="url" class="btn btn-danger" style="color: #FFF;"><span class="fa fa-remove"></span> Confirm</a>
            </div>
        </div>
    </div>
</div>

<div id="editContribution" tabindex="-1" role="dialog" aria-labelledby="editContribution" class="modal fade text-left" aria-hidden="true" style="display: none;">
    <form action="" method="POST">
        {{ csrf_field() }}

        <div role="document" class="modal-dialog">
            <div class="modal-content">

                @if(in_array(Illuminate\Support\Facades\Auth::user()->role, ['SuperAdmin', 'Admin', 'AdminHR', 'AdminCoop']))
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Edit Loans and Shares</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
                <div class="modal-body">
                    <div class="form-group">
                        <label class="" for="">Membership Status</label>
                        <select name="status" id="" class="form-control">
                            <option value="Active">Active</option>
                            <option value="Inactive" @if($user->status == 'Inactive') selected @endif>Inactive</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="" for="">Membership Type</label>
                        <select id="" name="membership_type" class="form-control" required>
                            <option value="Associate">Associate</option>
                            <option value="Regular" @if($user->membership_type == 'Regular') selected @endif>Regular</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Contribution per Payday</label>
                        <input type="text" class="form-control" value="{{ $user->coop_share }}" name="coop_share">
                    </div>
                    <div class="form-group">
                        <label for="">HDMF Additional Contribution</label>
                        <input type="text" class="form-control" value="{{ $user->hdmf_additional }}" name="hdmf_additional">
                    </div>
                    <div class="form-group">
                        <label for="">Cost Center</label>
                        <select name="cost_center_id" id="" class="form-control">
                            @foreach(\App\CostCenters::where('company_id', $user->EmploymentDetails->Principal->id)->get() as $costCenter)
                                <option value="{{ $costCenter->id }}" @if($user->cost_center_id == $costCenter->id) selected @endif>{{ $costCenter->cost_center }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <button class="btn btn-primary" style="color: #FFF;"><span class="fa fa-save"></span> Confirm</button>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

            $(document).on('click', '.editContribution', function(e) {
                e.preventDefault();

                $("#editContribution").modal('show');
            });

            $(document).on('click', '.editCostCenter', function(e) {
                e.preventDefault();

                $("#editCostCenter").modal('show');
            });

            $(document).on('click', '#addBeneficiary', function(e) {
                e.preventDefault();
                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load("{{ url("modal/beneficiary-add/{$id}") }}", function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');
                        $('.btn-mmddyyyy').datetimepicker({
                            format: "DD/MM/YYYY",
                            //format: "Y-m-d g:i:s",
                        });
                    }
                });
            });

            $(document).on('click', '.editBeneficiary', function(e) {
                e.preventDefault();
                var url = "{{ url("modal/beneficiary-edit/{$id}") }}/" + $(this).attr('data-id');

                $("#page-loader").show();
                $("#ajaxPopup").find('.modal-content').load(url, function(response, status) {
                    if(status == 'success') {
                        $("#page-loader").hide();
                        $("#ajaxPopup").modal('show');
                        $('.btn-mmddyyyy').datetimepicker({
                            format: "DD/MM/YYYY",
                            //format: "Y-m-d g:i:s",
                        });
                    }
                });
            });

            $(".deleteBeneficiary").click(function(e) {
                e.preventDefault();

                var url = "{{ url("admin/user/{$id}/beneficiary/delete") }}/" + $(this).attr('data-id');

                $("#deleteBeneficiary").find('#url').attr('href', url);
                $("#deleteBeneficiary").modal('show');
            });

        })
    </script>
@endsection

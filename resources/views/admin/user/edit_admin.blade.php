@extends('layouts.app')

@section('content')

                <!-- Dashboard Counts Section-->
                <section class="dashboard-counts no-padding-bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="" class="form" method="POST" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">

                                                <style>
                                                    span.selection {
                                                        width: 100%;
                                                    }
                                                </style>

                                                <div class="col-md-4 col-md-offset-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Name</label>
                                                        <p><strong>{{ $admin->Profile->first_name }} {{ $admin->Profile->middle_name }} {{ $admin->Profile->last_name }}</strong></p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label">Email Address</label>
                                                        <p><strong>{{ $admin->Profile->email }}</strong></p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label">Company Code</label>
                                                        <p><strong>{{ $admin->Profile->EmploymentDetails->Principal->company_code }}</strong></p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label">Admin Type</label>
                                                        <div class="select">
                                                            <select name="role" class="form-control" required>
                                                                <option value="Admin" @if($admin->role == 'Admin') selected @endif>Admin</option>
                                                                <option value="AdminPayroll" @if($admin->role == 'AdminPayroll') selected @endif>Admin Payroll</option>
                                                                <option value="AdminCoop" @if($admin->role == 'AdminCoop') selected @endif>Admin Coop</option>
                                                                <option value="AdminHR" @if($admin->role == 'AdminHR') selected @endif>Admin HR</option>
                                                                <option value="AdminAcctg" @if($admin->role == 'AdminAcctg') selected @endif>Admin Accounting</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <br />

                                                    <div class="text-center">
                                                        <button class="btn btn-success"><span class="fa fa-save"></span> Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection

@extends('layouts.app')

@section('content')



            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="row">
                                        <br />
                                        <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link @if($status == 'Active') active @endif" href="{{ url("admin/users/Active") }}"><span class="fa fa-users"></span> Active Users <span class="badge badge-primary">{{ \App\Profiles::where('status', 'Active')->count() }}</span></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link @if($status == 'Inactive') active @endif" href="{{ url("admin/users/Inactive") }}"><span class="fa fa-users"></span> Inactive Users <span class="badge badge-primary">{{ \App\Profiles::whereIn('status', ['Deactivated', 'Inactive'])->count() }}</span></a>
                                            </li>
                                            {{--<li class="nav-item">--}}
                                                {{--<a class="nav-link" href="{{ url('admin/manage-admin/users') }}"><span class="fa fa-user-plus"></span> Manage Admin</a>--}}
                                            {{--</li>--}}
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ url('admin/user/add') }}"><span class="fa fa-user-plus"></span> New User</a>
                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ url('/admin/super/employees-members/password_reset') }}"><span class="fa fa-user-plus"></span> Password Reset</a>
                                            </li>
                                        </ul>
                                        <br />
                                        <table id="users-dt" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Complete Name</th>
                                                <th>ID Number</th>
                                                <th>Membership Category</th>
                                                <th>Principal</th>
                                                <th>Gender</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#users-dt').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url": "{{ url("api/datatables/users/{$status}") }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "name" },
                    { "data": "id_number" },
                    { "data": "membership_category" },
                    { "data": "principal" },
                    { "data": "gender" },
                    { "data": "action" }
                ],
                "pageLength": 100,
                searchDelay: 350

            });
        })
    </script>
@endsection

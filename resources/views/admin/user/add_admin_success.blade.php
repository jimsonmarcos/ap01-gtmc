@extends('layouts.app')

@section('content')


                <!-- Dashboard Counts Section-->
                <section class="dashboard-counts no-padding-bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="" class="form" method="POST" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h1 class="green">The User has been updated to {{ $adminType }}</h1>
                                                </div>

                                                <div class="col-md-12">
                                                    <h4>Username: <span>{{ $newUsername }}</span></h4>
                                                </div>

                                                {{--<div class="col-md-12">--}}
                                                    {{--<p><strong>Note</strong>: Username and Password has been sent to {% email address of the added user admin%}</p>--}}
                                                {{--</div>--}}

                                                <div class="col-md-12 text-right">
                                                    <a href="{{ url('admin/user/add_admin') }}" class="btn btn-primary"><i class="fa fa-chevron-left" aria-hidden="true"></i> Go Back</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection

@section('extendedscript')
    <script>

    </script>
@endsection

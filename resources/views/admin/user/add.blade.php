@extends('layouts.app')

@section('content')


            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-md-10 col-md-offset-1">
                            <div class="card">
                                <div class="card-body">
                                    <form action="" id="userRegistrationForm" class="form" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-center">Generate ID Number</h2>
                                                <br>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">First Name</label>
                                                    <input type="text" id="" class="form-control disable-empty-val" name="first_name" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Middle Name</label>
                                                    <input type="text" id="" class="form-control disable-empty-val" name="middle_name">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Last Name</label>
                                                    <input type="text" id="" class="form-control disable-empty-val" name="last_name" required>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Join Date</label>
                                                    <div id="joinDate" class='input-group btn-mmddyyyy'>
                                                        <input type='text' class="form-control" name="join_date" value="" placeholder="mm/dd/yyyy" required />
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Membership Type</label>
                                                    <select id="selectMembershipType" name="membership_type" class="form-control" required>
                                                        <option value="" selected disabled>Select</option>
                                                        <option value="Associate">Associate</option>
                                                        <option value="Regular">Regular</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Category</label>
                                                    <select id="selectCategory" name="membership_category" class="form-control" required>
                                                        <option value="" selected disabled>Select</option>
                                                        <option value="Member">Member</option>
                                                        <option value="Employee">Employee</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Principal</label>
                                                    <select id="pre-reg-principal" name="principal_id" class="form-control" required>
                                                    <option value="" selected disabled>Select</option>
                                                    
                                                        @foreach($principals as $principal)
                                                            <option value="{{ $principal->id }}">{{ $principal->company_code }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Position</label>
                                                    <select name="position_id" class="form-control" id="selectPosition" disabled>
                                                        <option value=""></option>
                                                        @foreach(\App\Positions::all()->sortBy('title') as $position)
                                                            <option value="{{ $position->id }}">{{ $position->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-12"></div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Cost Center</label>
                                                    <select id="pre-reg-costCenter" name="cost_center_id" class="form-control">
                                                    <option value="" selected disabled>Select</option>
                                                    
                                                        @foreach(\App\CostCenters::where('company_id', $principals->first()->id)->get() as $costCenter)
                                                            <option value="{{ $costCenter->id }}">{{ $costCenter->cost_center }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Payroll Group</label>
                                                    <select name="payroll_group_id" class="form-control" id="selectPayrollGroup" disabled>
                                                        <option value=""></option>
                                                        @foreach(\App\PayrollGroups::where('company_id', $principals->first()->id)->get() as $payrollGroup)
                                                            <option value="{{ $payrollGroup->id }}" @if($payrollGroup->first()) selected @endif>{{ $payrollGroup->group_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-12"><br></div>



                                            <div class="col-md-12">
                                                <div class="row justify-content-md-center no-padding">
                                                    <div class="col-md-6 col-md-offset-3 text-center no-padding-left">
                                                        <h4>ID Number</h4>
                                                        <div class="alert alert-primary" role="alert">
                                                            <?php
                                                            if(\App\UserSeries::count() > 0) {
                                                                $series = \App\UserSeries::min('series');
                                                            } else {
                                                                $series = App\User::get()->max('series') + 1;
                                                            }
                                                            ?>
                                                            <strong id="idNumber" data-date="{{ date('m')  }}-{{ date('y')  }}" data-series="{{ $series }}">GTMC-{{ date('m')  }}-{{ date('y')  }}-M{{ $series }}</strong>
                                                        </div>
                                                        <small id="usernameExists" class="text-danger hidden">Username already exists!</small>
                                                    </div>
                                                </div>
                                            </div>

                                            {{--TODO: Fix Series--}}
                                            <input type="hidden" name="id_number" id="txtIdNumber" value="GTMC-{{ date('m')  }}-{{ date('y')  }}-M{{ $series }}">

                                            <div class="col-md-12"><br><br></div>

                                            <div class="col-md-12 text-right">
                                                <span>Click next to complete Registration Details</span>
                                                <button href="#" class="btn btn-primary">Next <span class="fa fa-chevron-right" style="font-size: 0.8em;"></span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
//            alert('Wow!');

            $("#selectMembershipType").change(function(e) {
               if($(this).find(':selected').val() == 'Associate') {
                   $("#selectCategory").find("option").each(function() {
                      if($(this).val() == 'Employee') {
                          $(this).prop('selected', true);
                          $("#selectCategory").prop('disabled', true);
                      }
                   });
                } else {
                   $("#selectCategory").prop('disabled', false);
                }
//                console.log($(this).find(':selected').val());
                updateIDNumber();
            });

            $(".cbox-same-address").click(function() {
               if($(this).is(':checked') == true) {
                   $("#icoe_contact_address").val($("#user-address").val());
               } else {
                   $("#icoe_contact_address").val('');
               }
            });

            $(".btn-upload-image").click(function(e) {
                e.preventDefault();
                var target = $(this).attr('data-target');

                $("#" + target).click();
            });

            $(document).on('change', "#pre-reg-principal", function() {
                $("#pre-reg-costCenter").find('option').remove();
                $("#pre-reg-costCenter").append("<option>Loading..</option>");

                $("#selectPayrollGroup").find('option').remove();
                $("#selectPayrollGroup").append("<option>Loading..</option>");


                $.get("{{ url('api/cost-centers') }}/" + $("#pre-reg-principal").find(':selected').val(), function(response) {
                    var data = $.parseJSON(response);

                    $("#pre-reg-costCenter").find('option').remove();
                    $.each(data, function(key, costCenter) {
                        var option = "<option value='"+ costCenter.id +"'>"+ costCenter.cost_center +"</option>";
                        $("#pre-reg-costCenter").append(option);
                    });
                });


                $.get("{{ url('api/payroll-groups') }}/" + $("#pre-reg-principal").find(':selected').val(), function(response) {
                    var data = $.parseJSON(response);
                    var option = '<option></option>';

                    $("#selectPayrollGroup").find('option').remove();
                    $.each(data, function(key, payrollGroup) {
                        option = option + "<option value='"+ payrollGroup.id +"'>"+ payrollGroup.group_name +"</option>";
                    });

                    $("#selectPayrollGroup").append(option);

                    if($("#selectCategory > option:selected").text() == 'Employee') {
                        $("#selectPayrollGroup > option").first().next().attr('selected', true);
                    }


                });

            });

            $(document).on('dp.change', '#joinDate', function(e) {
                $("#idNumber").attr('data-date', moment(e.date._d).format('MM-YY'));
                updateIDNumber();
            });

            $(document).on('change', '#selectCategory', function() {
               if($(this).find(':selected').text() == 'Member') {
//                   $("#selectPayrollGroup").attr('disable', true).find('option').first().click();
//                   $("#selectPosition > option").first().attr("selected", true);
               }
            });


            $(document).on('change', "#selectCategory, #selectPosition", function() {
                updateIDNumber();
            });

            function updateIDNumber() {
                var category = 'M';
                if($("#selectCategory").val() == 'Employee') {
                    category = 'E';
                }

//                console.log($("#selectMembershipType").val());

                var series = $("#idNumber").attr('data-series');
                var date = $("#idNumber").attr('data-date');

                $("#idNumber").text('GTMC-' + date + '-' + category + series);
                $("#txtIdNumber").val('GTMC-' + date + '-' + category + series);
                if(category == 'E') {
                    $("#selectPosition, #selectPayrollGroup").removeAttr('disabled').attr('required', 'required').find('option').first().removeAttr('selected');
//                    if($("#selectPosition").val() == '1') {
//                        $("#selectPosition, #selectPayrollGroup").removeAttr('disabled').attr('required', 'required').find('option').first().removeAttr('selected');
//                        $("#idNumber").text('GTMC-00-00-' + category + series);
//                        $("#txtIdNumber").val('GTMC-00-00-' + category + series);
//                    }
                } else if(category == 'M') {

                    $("#selectPosition, #selectPayrollGroup").removeAttr('required').attr('disabled', 'disabled').find('option').removeAttr('selected').first().attr('selected', 'true');
                }

                $.get('/check-username?username='+ category + series, function(result) {
                    if(result == 'success') {
                        $("#usernameExists").addClass('hidden');
                        $("#userRegistrationForm").find('button').attr('disabled', false);
                    } else {
                        $("#usernameExists").removeClass('hidden');
                        $("#userRegistrationForm").find('button').attr('disabled', true);
                    }
                });
            }

            $(document).on('change', ".photo-field", function () {
                var me = $(this);
                var countFiles = $(this)[0].files.length;
                var imgPath = $(this)[0].value;
                var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
                // var image_holder = $(this).parents('.img-upload-group, .img-upload').find('.photo-box, .dropzone');


                if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                    if (typeof (FileReader) != "undefined") {

                        //loop for each file selected for uploaded.
                        for (var i = 0; i < countFiles; i++) {

                            reader = new FileReader();
                            reader.onload = function (e) {
                                $("#form-user-photo").attr('src', e.target.result);
                            }

                            // image_holder.show();
                            console.log($(this)[0].files[i]);
                            reader.readAsDataURL($(this)[0].files[i]);
                        }

                    } else {
                         alert("This browser does not support FileReader.");
                    }
                } else {
                    alert("Uploaded file is not a valid iamge. Please try again.");
                }
            });
        })
    </script>
@endsection

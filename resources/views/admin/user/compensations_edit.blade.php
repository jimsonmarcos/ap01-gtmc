@extends('layouts.app')

@section('content')


            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row no-padding">
                                        <div class="col-md-6">
                                            <strong>Name: {{ $user->name() }}</strong>
                                        </div>

                                        <div class="col-md-6">
                                            <strong>ID Number: {{ $user->id_number }}</strong>
                                        </div>

                                        <div class="col-md-12">
                                            <hr>
                                        </div>

                                        <div class="col-md-12">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/profile") }}">Personal Information</a>
                                                </li>
                                                @if($user->User->role != 'Member')
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/employment-details") }}">Employment Details</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="{{ url("admin/user/{$id}/compensations-leave-credits") }}">Compensation and Leave Credits</a>
                                                </li>
                                                @endif
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="col-md-12">
                                            <form action="" class="form" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}

                                                <div class="row">
                                                    <div class="col-md-12 text-center">
                                                        <h1>Compensation</h1>
                                                        <hr>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Rate Type</label>
                                                            <select class="form-control" name="rate_type" id="rateType">
                                                                <option value="Daily" {{ $compensation->rate_type == 'Daily' ? 'selected' : '' }}>Daily</option>
                                                                <option value="Monthly" {{ $compensation->rate_type == 'Monthly' ? 'selected' : '' }}>Monthly</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Daily Rate Category</label>
                                                            <select class="form-control" name="daily_category" id="dailyRateCategory" {{ $compensation->rate_type == 'Daily' ? '' : 'disabled' }}>
                                                                <option value="Fixed" {{ $compensation->daily_category == 'Fixed' ? 'selected' : '' }}>Fixed</option>
                                                                <option value="Location" {{ $compensation->daily_category == 'Location' ? 'selected' : '' }}>Location</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Location</label>
                                                            <select id="locationRates" class="form-control" name="daily_rate_id" {{ $compensation->daily_category == 'Location' ? '' : 'disabled' }}>
                                                                <option value=""></option>
                                                                @foreach(\App\DailyRates::all()->sortBy('location') as $location)
                                                                    <option value="{{ $location->id }}" data-rate="{{ $location->rate }}" {{ $compensation->daily_rate_id == $location->id ? 'selected' : '' }}>{{ $location->location }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Monthly Rate</label>
                                                            <input id="monthlyRate" type="text" name="monthly_rate" class="form-control" value="{{ $compensation->monthly_rate }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" {{ $compensation->rate_type == 'Daily' ? 'readonly' : '' }}>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Daily Rate</label>
                                                            <input id="dailyRate" type="text" name="daily_rate" class="form-control" value="{{ $compensation->daily_rate }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Hourly Rate</label>
                                                            <input id="hourlyRate" type="text" name="hourly" class="form-control" value="{{ $compensation->hourly }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" readonly>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Mode of Transfer</label>
                                                            <select id="comp-transfermode" name="" class="form-control">
                                                                @foreach($transferModes as $transferMode)
                                                                    <option @if(!empty($compensation->TransferMode->transfer_mode) && $compensation->TransferMode->transfer_mode == $transferMode->transfer_mode) selected @endif>{{ $transferMode->transfer_mode }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Transfer Type</label>
                                                            <select name="" id="comp-transfertype" class="form-control">
                                                                @foreach($transferTypes as $transferType)
                                                                    <option @if(!empty($compensation->TransferMode->transfer_type) && $compensation->TransferMode->transfer_type == $transferType->transfer_type) selected @endif>{{ $transferType->transfer_type }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Bank</label>
                                                            <select class="form-control" name="transfer_mode_id" id="comp-bank">
                                                                @foreach($banks as $bank)
                                                                    <option value="{{ $bank->id }}" @if(!empty($compensation->TransferMode->bank) && $compensation->TransferMode->bank == $bank->bank) selected @endif>{{ $bank->bank }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Mobile Number</label>
                                                            <input id="comp-mobilenumber" type="text" name="mobile_number" class="form-control" maxlength="11" value="{{ $compensation->mobile_number }}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12"></div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Account Number</label>
                                                            <input type="text" id="accountNumber" name="account_number" class="form-control" value="{{ $compensation->account_number }}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Fixed Allowance</label>
                                                            <input type="text" id="fixed_allowance" name="fixed_allowance" class="form-control" value="{{ $compensation->fixed_allowance }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">De Minimis</label>
                                                            <input type="text" id="de_minimis" name="de_minimis" class="form-control" value="{{ $compensation->de_minimis }}" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <h1 class="text-center">Payroll Group</h1>
                                                        <hr>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Client</label>
                                                            <select name="principal_id" id="client" class="form-control">
                                                                @foreach(\App\Companies::orderBy('company_code')->with('payrollgroups')->with('parentcompany.payrollgroups')->get() as $client)
                                                                    <option value="{{ $client->id }}" data-client='{{$client->toJson()}}' @if($client->id == $employmentDetails->principal_id) selected @endif>{{ $client->company_code }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4 @if(empty($compensation->Company->parent_company_id)) hidden @endif" id="parentClient">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Parent Client</label>
                                                            <input type="text" class="form-control" value="@if(!empty($employmentDetails->Company->parent_company_id)) {{ $employmentDetails->Company->ParentCompany->company_code }} @endif" disabled>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Payroll Group</label>
                                                            <select name="payroll_group_id" id="payrollGroups" class="form-control">
                                                                @foreach(\App\PayrollGroups::where('company_id', $employmentDetails->principal_id)->orderBy('group_name')->get() as $payrollGroup)
                                                                    <option value="{{ $payrollGroup->id }}" @if($compensation->payroll_group_id == $payrollGroup->id) selected @endif>{{ $payrollGroup->group_name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12"></div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">SSS No.</label>
                                                            <input id="sss_number" type="text" name="sss" class="form-control" placeholder="(optional)" value="{{ $compensation->sss }}" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                            <div>
                                                                <input id="option" type="checkbox" value="Y" name="compute_sss" @if($compensation->compute_sss == 'Y') checked @endif @if(empty($compensation->sss)) disabled @endif>
                                                                <label for="option">Compute SSS </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">HDMF No.</label>
                                                            <input id="hdmf_number" type="text" name="hdmf" class="form-control" placeholder="(optional)" value="{{ $compensation->hdmf }}">
                                                            <div>
                                                                <input id="option2" type="checkbox" class="" name="compute_hdmf" @if($compensation->compute_hdmf == 'Y') checked @endif @if(empty($compensation->hdmf)) disabled @endif>
                                                                <label for="option2" value="Y">Compute HDMF </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12"></div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">PhilHealth No.</label>
                                                            <input id="philhealth_number" type="text" name="philhealth" class="form-control" placeholder="(optional)" value="{{ $compensation->philhealth }}" maxlength="12" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                            <div>
                                                                <input id="option3" type="checkbox" class="" name="compute_philhealth" value="Y" @if($compensation->compute_philhealth == 'Y') checked @endif @if(empty($compensation->philhealth)) disabled @endif>
                                                                <label for="option3">Compute PhilHealth </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">TIN No.</label>
                                                            <input type="text" name="tin" class="form-control" placeholder="(optional)" value="{{ $compensation->tin }}" maxlength="12" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12"></div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Tax Type</label>
                                                            <select id="taxTypes" class="form-control" name="tax_type_id">
                                                                @if($user->CivilStatus->title == 'SINGLE')
                                                                    @foreach(\App\TaxTypes::whereIn('id', [1,2,3,4,5])->orderBy('title')->get() as $taxType)
                                                                        <option value="{{ $taxType->id }}" @if($taxType->id == $compensation->tax_type_id) selected @endif>{{ $taxType->title }}</option>
                                                                    @endforeach
                                                                @else
                                                                    @foreach(\App\TaxTypes::whereIn('id', [6,7,8,9,10])->orderBy('title')->get() as $taxType)
                                                                        <option value="{{ $taxType->id }}" @if($taxType->id == $compensation->tax_type_id) selected @endif>{{ $taxType->title }}</option>
                                                                    @endforeach
                                                                @endif

                                                            </select>
                                                            <div>
                                                                <input class="isTaxable" id="optionsRadios1" type="radio" value="N" name="deduct_withholding" data-taxable="N" @if($compensation->deduct_withholding == 'N') checked @endif>
                                                                <label for="optionsRadios1">Minimum Wage Earner</label>
                                                            </div>
                                                            <div>
                                                                <input class="isTaxable" id="optionsRadios2" type="radio" value="Y" name="deduct_withholding" data-taxable="Y" @if($compensation->deduct_withholding == 'Y') checked @endif>
                                                                <label for="optionsRadios2">Deduct Withholding Tax</label>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="col-md-12 text-right">
                                                    <div>
                                                        <button class="btn btn-primary pull-right"><span class="fa fa-save"></span> Confirm</button>
                                                        <a href="{{ url()->previous() }}" class="btn btn-secondary pull-right" style="margin-right: 5px;"><span class="fa fa-ban"></span> Cancel</a>

                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
//            $(".isTaxable").change(function() {
//                if($(this).data('taxable').toString() == 'Y') {
//                    $("#taxTypes").removeAttr('disabled').attr('required', 'required').find('option').first().removeAttr('selected');
//                } else {
//                    $("#taxTypes").removeAttr('required').attr('disabled', 'disabled').find('option').first().attr('selected', 'true');
//                }
//            });

            $(document).on('keyup change', "#sss_number, #hdmf_number, #philhealth_number", function(e) {
                if($.trim($(this).val()).length > 0) {
                    if($(this).closest('div.form-group').find('input[type=checkbox]').is(':disabled') == true) {
                        $(this).closest('div.form-group').find('input[type=checkbox]').removeAttr('disabled');
                    }
                } else {
                    $(this).closest('div.form-group').find('input[type=checkbox]').attr({disabled: true, checked: false});
                }
            });

            $("#client").change(function() {
                var me = $(this).find(':selected');
                var json = $.parseJSON(me.attr('data-client'));
                $("#payrollGroups").find('option').remove();

               if(json.parent_company_id == null) {
                   $("#parentClient").addClass('hidden').find('input').val('');

                   $.each(json.payrollgroups, function(i, val) {
                       $("#payrollGroups").append('<option value="'+ val.id +'">'+ val.group_name +'</option>');
                   });
               } else {
                   $("#parentClient").removeClass('hidden').find('input').val(json.parentcompany.company_code);

                   $.each(json.parentcompany.payrollgroups, function(i, val) {
                       $("#payrollGroups").append('<option value="'+ val.id +'">'+ val.group_name +'</option>');
                   });
               }
            });

            $("#rateType").change(function() {
                if($(this).val() == 'Daily') {
                    $("#dailyRateCategory").removeAttr('readonly').removeAttr('disabled').attr('required', 'required').find('option').first().removeAttr('selected');
                    $("#dailyRate").removeAttr('readonly');
                    $("#hourlyRate").attr({ readonly: true});
                    $("#monthlyRate, #hourlyRate").attr({ readonly: true});
                    // $("#dailyRate").removeAttr('readonly').val('');
                    // $("#hourlyRate").attr({ readonly: true, value: '' }).val('');
                    // $("#monthlyRate, #hourlyRate").attr({ readonly: true, value: '' }).val('');
                } else {
                    $("#locationRates").removeAttr('required').attr('disabled', 'disabled').find('option').first().attr('selected', 'true');
                    $("#dailyRateCategory").removeAttr('required').attr('disabled', 'disabled').find('option').first().attr('selected', 'true');
                    $("#monthlyRate").removeAttr('readonly');
                    $("#dailyRate, #hourlyRate").attr('readonly', 'readonly');
                    // $("#dailyRate, #hourlyRate").attr('readonly', 'readonly').val('').attr('value', '');
                }
            });

            $("#dailyRateCategory").change(function() {
                if($(this).val() == 'Location') {
                    $("#dailyRate").val('').attr('readonly', 'readonly');
                    $("#hourlyRate, #monthlyRate").val('');
                    $("#locationRates").removeAttr('disabled').attr('required', 'required').find('option').removeClass('hidden').first().removeAttr('selected');
                    // $("#hourlyRate").val('');
                } else {
                    $("#locationRates > option").addClass('hidden');
                    $("#dailyRate").val('').removeAttr('readonly').removeAttr('disabled');
                    $("#hourlyRate, #monthlyRate").val('');
                    $("#locationRates").removeAttr('required').attr('disabled', 'disabled').find('option').first().attr('selected', 'true');
                    // $("#hourlyRate").val('');
//                    $("#locationRates").removeAttr('required').attr('disabled', 'disabled').find('option').first().attr('selected', 'true');
                }
            });

            $("#monthlyRate").on('keyup', function() {
                var dailyRate = parseFloat($(this).val()) / 26;
                $("#dailyRate").attr('value', dailyRate.toFixed(2)).val(dailyRate.toFixed(2));
                $("#hourlyRate").attr('value', (parseFloat($("#dailyRate").val()) / 8).toFixed(2)).val((parseFloat($("#dailyRate").val()) / 8).toFixed(2));
                // var hourlyRate = dailyRate / 8
                // $("#hourlyRate").attr('value', hourlyRate.toFixed(2));
            });

            $("#locationRates").change(function() {
                if($(this).find(':selected').attr('value') != '') {
                    var location = $(this).find(':selected');
                    $("#hourlyRate").attr('readonly', true);
                    $("#dailyRate").attr('readonly', true);
                    $("#monthlyRate").attr('readonly', true);

                    $("#dailyRate").attr('value', location.attr('data-rate')).val(location.attr('data-rate')).attr('readonly', true);
                    $("#hourlyRate").attr('value', (parseFloat($("#dailyRate").val()) / 8).toFixed(2)).val((parseFloat($("#dailyRate").val()) / 8).toFixed(2));
                    $("#monthlyRate").val(parseFloat($("#dailyRate").val()) * 26)
                    // $("#hourlyRate").attr('value', parseFloat($("#dailyRate").val()) / 8).val(parseFloat($("#dailyRate").val()) / 8).attr('readonly', true);
                    // $("#monthlyRate").val(parseFloat($("#dailyRate").val()) * 26).attr('readonly', true);
                } else {
                    $("#dailyRate, #hourlyRate, #monthlyRate").attr('readonly', false);
                    // $("#dailyRate, #hourlyRate, #monthlyRate").val('').removeAttr('value').attr('readonly', false);
                    $("#hourlyRate").attr('readonly', true);
                }
            });

            $("#dailyRate").on('keyup', function() {
                if($("#dailyRate").val() != '') {
                    $("#hourlyRate").attr('value', (parseFloat($("#dailyRate").val()) / 8).toFixed(2)).val((parseFloat($("#dailyRate").val()) / 8).toFixed(2));
                    $("#monthlyRate").val(parseFloat($("#dailyRate").val()) * 26)
                } else {
                    $("#hourlyRate, #monthlyRate").val('');
                }
            });

            if($('#comp-transfermode').find(':selected').val() == 'BANK DEPOSIT' || $("#comp-transfermode").find(':selected').text() == 'BDO' || $("#comp-transfermode").find(':selected').text() == 'METROBANK' || $("#comp-transfermode").find(':selected').text() == 'UNIONBANK') {
                $("#accountNumber").prop({ 'required' : true });
            } else {
                $("#accountNumber").prop({ 'required' : false });
            }

            $(document).on('change', '#comp-transfermode', function(e) {
                e.preventDefault();

                if($(this).find(':selected').val() == 'BANK DEPOSIT' || $("#comp-transfermode").find(':selected').text() == 'BDO' || $("#comp-transfermode").find(':selected').text() == 'METROBANK' || $("#comp-transfermode").find(':selected').text() == 'UNIONBANK') {
                    $("#accountNumber").prop({ 'required' : true });
                } else {
                    $("#accountNumber").prop({ 'false' : true });
                }

                if($("#comp-transfermode").find(':selected').text() == 'GCASH') {
                    // $("#comp-mobilenumber").prop('readonly', false);
                } else {
                    // $("#comp-mobilenumber").prop('readonly', true);
                }

                $("#comp-transfertype, #comp-bank").find('option').remove();
                $("#comp-transfertype, #comp-bank").attr('disabled', 'disabled');
                $("#comp-transfertype").append("<option>Loading..</option>");

                var data = {
                    _token : '{{ csrf_token() }}',
                    transfer_mode : $(this).find(':selected').text()
                };
//               console.log($(this).find(':selected').text());
                $.post('{{ url("api/transfer-mode/types") }}', data, function(response) {
//                  console.log(response);
                    var transfer_types = $.parseJSON(response);
                    var option = '';
                    $.each(transfer_types, function(index, json) {
                        option = option + '<option>'+ json.transfer_type +'</option>';
                    });

                    $("#comp-transfertype > option").remove();
                    $("#comp-transfertype").append(option);
                    $("#comp-transfertype").removeAttr('disabled');

                    setTimeout(function() {
                        reloadTransferModes();
                    }, 300);
                });
            });

            $(document).on('change', '#comp-transfertype', function(e) {
                e.preventDefault();

                $("#comp-bank").find('option').remove();
                $("#comp-bank").attr('disabled', 'disabled');
                $("#comp-bank").append("<option>Loading..</option>");

                var data = {
                    _token : '{{ csrf_token() }}',
                    transfer_mode : $('#comp-transfermode').find(':selected').text(),
                    transfer_type : $(this).find(':selected').text()
                };

                $.post('{{ url("api/transfer-mode/banks") }}', data, function(response) {

                    var banks = $.parseJSON(response);
                    var option = '';
                    $.each(banks, function(index, json) {
                        option = option + '<option>'+ json.bank +'</option>';
                    });

                    $("#comp-bank > option").remove();
                    $("#comp-bank").append(option);
                    $("#comp-bank").removeAttr('disabled');

                    setTimeout(function() {
                        reloadTransferModes();
                    }, 300);
                });
            });

            function reloadTransferModes() {
                if ($("#comp-transfertype option").length > 0 && $("#comp-bank option").length == 0) {

                    if ($('#comp-transfertype').find(':selected').text() != 'Loading..') {
                        $("#comp-bank").find('option').remove();
                        $("#comp-bank").attr('disabled', 'disabled');
                        $("#comp-bank").append("<option>Loading..</option>");

                        var data = {
                            _token : '{{ csrf_token() }}',
                            transfer_mode : $('#comp-transfermode').find(':selected').text(),
                            transfer_type : $('#comp-transfertype').find(':selected').text()
                        };

                        $.post('{{ url("api/transfer-mode/banks") }}', data, function(response) {

                            var banks = $.parseJSON(response);
                            var option = '';
                            $.each(banks, function(index, json) {
                                option = option + '<option value="'+ json.id +'">'+ json.bank +'</option>';
                            });

                            $("#comp-bank > option").remove();
                            $("#comp-bank").append(option);
                            $("#comp-bank").removeAttr('disabled');

                            setTimeout(function() {
                                reloadTransferModes();
                            }, 300);
                        });
                    }
                }
            }
        })
    </script>
@endsection

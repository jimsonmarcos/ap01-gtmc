@extends('layouts.app')

@section('content')

            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="col-md-3 col-lg-offset-4">
                        <form action="" method="POST">
                            {{ csrf_field() }}

                            {{-- User Profile complete name here--}}
                            <div class="form-group">
                                <label class="form-control-label">Employee Name</label>
                                <input id="employee_name" type="text" class="form-control" name="employee_name" value="{{ $user->name() }}" disabled />
                            </div>

                            {{-- {{ \Carbon\Carbon::createFromFormat('Y-m-d', $employmentDetails->hire_date)->format('m/d/Y') }} --}}
                            <div class="form-group">
                                <label class="form-control-label">Resignation Date</label>
                                <div class='input-group btn-mmddyyyy'>
                                    <input type='text' class="form-control" name="resignation_date" value="@if($user->status == 'Deactivated' || !empty($user->EmploymentDetails->effective_date)) {{ date('m/d/Y', strtotime($user->EmploymentDetails->effective_date)) }} @else {{ date('m/d/Y') }} @endif" placeholder="mm/dd/yyyy" required @if($user->status == 'Deactivated') disabled @endif/>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>

                            {{-- {{ \Carbon\Carbon::createFromFormat('Y-m-d', $employmentDetails->hire_date)->format('m/d/Y') }} --}}
                            <div class="form-group">
                                <label class="form-control-label">Effective Date</label>
                                <div class='input-group btn-mmddyyyy'>
                                    <input type='text' class="form-control" name="effective_date" value="@if($user->status == 'Deactivated' || !empty($user->EmploymentDetails->effective_date)) {{ date('m/d/Y', strtotime($user->EmploymentDetails->effective_date)) }} @else {{ date('m/d/Y') }} @endif" placeholder="mm/dd/yyyy" required @if($user->status == 'Deactivated') disabled @endif />
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="form-control-label">Reason for leaving</label>
                                <select name="reason" id="reason_for_leaving" class="form-control" @if($user->status == 'Deactivated') disabled @endif>
                                    @if($user->status == 'Deactivated' || !empty($user->EmploymentDetails->effective_date))
                                        <option value="">{{ $user->EmploymentDetails->reason }}</option>
                                    @else
                                        <option value="Resigned">Resigned</option>
                                        <option value="AWOL">AWOL</option>
                                        <option value="Terminated">Terminated</option>
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="remarks">Remarks</label>
                                <textarea class="form-control" id="remarks" name="remarks" rows="7" @if($user->status == 'Deactivated') disabled @endif>{{ $user->EmploymentDetails->remarks }}</textarea>
                            </div>

                            @if($user->status == 'Active')
                            <div class="form-group text-center">
                                <a href="{{ url()->previous() }}" class="btn btn-default" style="margin-right: 5px;"><span class="fa fa-ban"></span> Cancel</a>
                                <button class="btn btn-danger"><span class="fa fa-save"></span> Confirm</button>
                            </div>
                            @else
                                <div class="form-group text-center">
                                    <a href="{{ url()->previous() }}" class="btn btn-primary" style="margin-right: 5px;"><span class="fa fa-chevron-left"></span> Back</a>
                                </div>
                            @endif

                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')

@endsection

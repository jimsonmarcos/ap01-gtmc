@extends('layouts.app')

@section('content')

            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row no-padding">
                                        <div class="col-md-12">
                                            <a href="{{ url('/admin/users') }}"><i class="fa fa-arrow-left"></i> Go back to list</a>
                                            <br><br>
                                        </div>
                                        <div class="col-md-6">
                                            <strong>Name: {{ $user->name() }}</strong>
                                        </div>

                                        <div class="col-md-6">
                                            <strong>ID Number: {{ $user->id_number }}</strong>
                                        </div>

                                        <div class="col-md-12">
                                            <hr>
                                        </div>

                                        <div class="col-md-12">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="{{ url("admin/user/{$id}/profile") }}">Personal Information </a>
                                                </li>
                                                @if($user->User->role != 'Member')
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/employment-details") }}">Employment Details</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/compensations-leave-credits") }}">Compensation and Leave Credits</a>
                                                </li>
                                                @endif
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="col-md-9">
                                            @if(in_array(Illuminate\Support\Facades\Auth::user()->role, ['SuperAdmin', 'Admin', 'AdminHR', 'AdminCoop']))
                                            <div>
                                                <a href="{{ url("admin/user/{$id}/profile/edit") }}" class="btn btn-sm btn-primary pull-right" style="margin-top: 10px;"><span class="fa fa-edit"></span> Edit Profile</a>
                                            </div>
                                            @endif

                                            <br>
                                            <br>

                                            <table class="table no-margin-bottom">
                                                <tbody>
                                                    <tr>
                                                        <td width="110">First Name:</td>
                                                        <td width="">{{ strtoupper($user->first_name) }}</td>
                                                        <td width="130">Middle Name:</td>
                                                        <td width="">{{ strtoupper($user->middle_name) }}</td>
                                                        <td width="110">Last Name:</td>
                                                        <td width="">{{ strtoupper($user->last_name) }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td>Civil Status:</td>
                                                        <td>{{ strtoupper($user->CivilStatus->title) }}</td>
                                                        <td>Gender:</td>
                                                        <td colspan="3">{{ strtoupper($user->gender) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Address:</td>
                                                        <td colspan="5">{{ strtoupper($user->address) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mobile:</td>
                                                        <td>{{ $user->mobile1 }}</td>
                                                        <td>{{ $user->mobile2 }}</td>
                                                        <td>Email:</td>
                                                        <td>{{ strtolower($user->email) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Birth Date</td>
                                                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->birthday)->format('M. j, Y') }}</td>
                                                        <td>Age</td>
                                                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->birthday)->age }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mother's Maiden <br />Name</td>
                                                        <td colspan="5">{{ strtoupper($user->mothers_maiden_name) }}</td></tr>
                                                    <tr>
                                                        <td>Educational Attainment</td>
                                                        <td colspan="5">@if(!empty($user->EducationalAttainment->title)) {{ strtoupper($user->EducationalAttainment->title) }} @endif</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <hr />
                                            <h2>In Case of Emergency</h2>
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td>Contact Person</td>
                                                        <td>{{ strtoupper($user->icoe_contact_name) }}</td>
                                                        <td>Contact Number</td>
                                                        <td>{{ strtoupper($user->icoe_contact_mobile) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Address</td>
                                                        <td colspan="3">{{ strtoupper($user->icoe_contact_address) }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="" style="padding: 5px;">
                                                @if(empty($user->photo_path))
                                                    <img src="{{ url("img/user-placeholder.png") }}" alt="" style="max-width: 100%;">
                                                @else
                                                    <img src="{{ url($user->photo_path) }}" alt="" style="max-width: 100%;">
                                                @endif
                                            </div>

                                            @if(in_array(Illuminate\Support\Facades\Auth::user()->role, ['SuperAdmin', 'Admin', 'AdminHR']))
                                            <div class="text-center push-5-t">
                                                <button id="upload-photo" class="btn btn-default"><i class="fa fa-upload"></i> Choose File</button>
                                            </div>

                                            <form class="hidden" action="{{ url("admin/user/{$id}/profile/upload-photo") }}" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <input type="file" id="file-upload-photo" name="profile_picture" accept=".jpg, .jpeg, .png" />
                                            </form>
                                            @endif
                                        </div>

                                        @if($user->User->role == 'Employee')
                                            <div class="col-md-3 text-center">
                                                <br />

                                                {{--<a href="{{ url("admin/user/{$id}/deactivate-user") }}" class="btn btn-danger"><i class="fa fa-remove"></i> @if(!empty($user->EmploymentDetails->reason) && date('Y-m-d', strtotime($user->EmploymentDetails->effective_date)) <= date('Y-m-d')) DEACTIVATED @else DEACTIVATE USER @endif</a>--}}
                                                @if(empty($user->EmploymentDetails->reason))
                                                    <a href="{{ url("admin/user/{$id}/deactivate-user") }}" class="btn btn-danger"><i class="fa fa-remove"></i> DEACTIVATE USER </a>
                                                @else
                                                    <p class="text-left" style="margin-bottom: 5px;">Resignation date: {{ date('M. j, Y', strtotime($user->EmploymentDetails->resignation_date)) }}</p>
                                                    <p class="text-left" style="margin-bottom: 5px;">Effectivity date: {{ date('M. j, Y', strtotime($user->EmploymentDetails->effective_date)) }}</p>
                                                    @if(!empty($user->EmploymentDetails->reason) && date('Y-m-d', strtotime($user->EmploymentDetails->effective_date)) <= date('Y-m-d'))
                                                        @if($user->EmploymentDetails->status != 'Deactivated')
                                                            <a href="{{ url("admin/user/{$id}/reactivate-user") }}" class="btn btn-success" onclick="return confirm('Are you sure you want to reactivate this employee?')"><i class="fa fa-check"></i> ACTIVATE USER </a>
                                                        @endif
                                                    @else
                                                        <a href="{{ url("admin/user/{$id}/cancel-deactivation") }}" class="btn btn-warning" onclick="return confirm('Are you sure you want to cancel the deactivation of this employee?')"><i class="fa fa-check"></i> CANCEL DEACTIVATION </a>
                                                    @endif

                                                @endif
                                            </div>
                                        @endif


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(document).on('click', "#upload-photo", function(e) {
                e.preventDefault();
//                console.log('clicked!');
                $("#file-upload-photo").trigger('click');
            })

            $(document).on('change', "#file-upload-photo", function () {
                $(this).closest('form').submit();
            });
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')



            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#"><span class="fa fa-users"></span> Client Admins</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/manage-admin/clients/add') }}"><span class="fa fa-user-plus"></span> New Client Admin</a>
                                        </li>
                                    </ul>

                                    <br>

                                    <table id="users-dt" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Complete Name</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Company Code</th>
                                                <th>User Access</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                                <tr>
                                                    @if(!empty($user->contact_person_id))
                                                        <td>
                                                            {{ $user->ContactPerson->first_name }}
                                                            @if(!empty($user->ContactPerson->middle_name))
                                                                {{ substr($user->ContactPerson->middle_name, 0, 1) }}.
                                                            @endif
                                                            {{ $user->ContactPerson->last_name }}
                                                        </td>
                                                    @else
                                                        <td>
                                                            {{ $user->Profile->first_name }}
                                                            @if(!empty($user->Profile->middle_name))
                                                                {{ substr($user->Profile->middle_name, 0, 1) }}.
                                                            @endif
                                                            {{ $user->Profile->last_name }}
                                                        </td>
                                                    @endif

                                                    <td>{{ $user->client_username }}</td>
                                                    <td>@if(!empty($user->contact_person_id)) {{ $user->ContactPerson->email }} @else {{ $user->Profile->email }} @endif</td>
                                                    <td>{{ !empty($user->Company->company_code) ? $user->Company->company_code : '' }}</td>
                                                    <td>{{ $user->client_type == 'ClientUser' ? 'Client User' : 'Client Admin' }}</td>
                                                    <td><a href="{{ url("/admin/manage-admin/client/delete/{$user->id}") }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to remove this user?')"><i class="fa fa-remove"></i> Remove</a></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#users-dt').DataTable({

            });
        })
    </script>
@endsection

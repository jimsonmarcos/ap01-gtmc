@extends('layouts.app')

@section('content')

                <!-- Dashboard Counts Section-->
                <section class="dashboard-counts no-padding-bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">

                                        <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link " href="{{ url('admin/manage-admin/users') }}"><span class="fa fa-users"></span> User Admins</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#"><span class="fa fa-user-plus"></span> New Admin</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('employees_and_members_password_reset') }}">
                                                    <span class="fa fa-user"></span> Password Reset
                                                </a>
                                            </li>
                                        </ul>

                                        <br>
                                        <br>

                                        <form action="" class="form" method="POST" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">

                                                <style>
                                                    span.selection {
                                                        width: 100%;
                                                    }
                                                </style>

                                                <div class="col-md-offset-3 col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Name</label>
                                                        <select class="js-data-example-ajax form-control"></select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label">Email Address</label>
                                                        <input id="searchEmail" type="email" class="form-control" name="" placeholder="" disabled />
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label">Company Code</label>
                                                        <input id="searchCompanyCode" type="email" class="form-control" name="" placeholder="" disabled />
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label">Admin Type</label>
                                                        <select name="role" class="form-control" required>
                                                            <option value="Admin">Admin</option>
                                                            <option value="AdminPayroll">Admin Payroll</option>
                                                            <option value="AdminCoop">Admin Coop</option>
                                                            <option value="AdminHR">Admin HR</option>
                                                            <option value="AdminAcctg">Admin Accounting</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <h4>Username</h4>
                                                        <div class="alert alert-primary" role="alert">
                                                            <strong id="adminUsername">xxxxx</strong>
                                                        </div>
                                                        <input type="hidden" id="searchUsername" name="username">
                                                        <input type="hidden" id="searchUserId" name="user_id">
                                                    </div>

                                                    <br />

                                                    <div class="text-center">
                                                        <button class="btn btn-success"><span class="fa fa-save"></span> Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(".js-data-example-ajax").select2({
                ajax: {
                    url: "{{ url('api/users/search/add/admin') }}",
                    dataType: 'json',
                    delay: 300,
                    data: function (params) {
                        return {
                            q: params.term, // search term
//                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'Search User',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            function formatRepo (user) {
                if (user.loading) {
                    return user.text;
                }
                console.log(user);

                var markup = "<span data-fname='"+ user.first_name +"' data-mname='"+ user.middle_name +"' data-lname='"+ user.last_name +"' data-email='"+ user.email+"' data-company='"+ user.company_code+"'>"+ user.name +"</span>"

//                var markup = "<div class='select2-result-repository clearfix'>" +
//                    "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
//                    "<div class='select2-result-repository__meta'>" +
//                    "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

//                if (repo.description) {
//                    markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
//                }

//                markup += "<div class='select2-result-repository__statistics'>" +
//                    "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
//                    "<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
//                    "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
//                    "</div>" +
//                    "</div></div>";

                return markup;
            }

            function formatRepoSelection (user) {
                return user.name;
            }

//            $('.js-data-example-ajax').on("change", function(e) {
//                // what you would like to happen
//                console.log(e);
//            });

            $(".js-data-example-ajax").on("select2:select", function (evt) {

                console.log(evt.params.data);
                var user = evt.params.data;
                $("#searchEmail").attr('value', user.email);
                $("#searchCompanyCode").attr('value', user.principal);
                $("#searchUserId").attr('value', user.user_id);
                var adminusername = 'admin.' + user.first_name.substring(0, 1).toLowerCase() + user.middle_name.substring(0, 1).toLowerCase() + user.last_name.trim().toLowerCase().replace(' ', '_');
                $("#searchUsername").attr('value', adminusername);
                $("#adminUsername").text(adminusername);
            });
        });
    </script>
@endsection

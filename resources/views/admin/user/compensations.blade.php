@extends('layouts.app')

@section('content')


            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row no-padding">
                                        <div class="col-md-12">
                                            <a href="{{ url('/admin/users') }}"><i class="fa fa-arrow-left"></i> Go back to list</a>
                                            <br><br>
                                        </div>
                                        <div class="col-md-6">
                                            <strong>Name: {{ $user->name() }}</strong>
                                        </div>

                                        <div class="col-md-6">
                                            <strong>ID Number: {{ $user->id_number }}</strong>
                                        </div>

                                        <div class="col-md-12">
                                            <hr>
                                        </div>

                                        <div class="col-md-12">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/profile") }}">Personal Information</a>
                                                </li>
                                                @if($user->User->role != 'Member')
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/employment-details") }}">Employment Details</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="{{ url("admin/user/{$id}/compensations-leave-credits") }}">Compensation and Leave Credits</a>
                                                </li>
                                                @endif
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="col-md-12">
                                            <br>

                                            <h3>Payroll Details</h3>
                                            <div class="row no-padding-bottom">
                                                <div class="col-md-3">
                                                    <h4>Compute</h4>
                                                    <ul>
                                                        <li class="text-{{ $compensations->compute_sss == 'Y' ? 'success' : 'danger' }}" style="list-style: none;"><span class="fa {{ $compensations->compute_sss == 'Y' ? 'fa-check' : 'fa-close' }}"></span> SSS No.</li>
                                                        <li class="text-{{ $compensations->compute_hdmf == 'Y' ? 'success' : 'danger' }}" style="list-style: none;"><span class="fa {{ $compensations->compute_hdmf == 'Y' ? 'fa-check' : 'fa-close' }}"></span> HDMF No.</li>
                                                        <li class="text-{{ $compensations->compute_philhealth == 'Y' ? 'success' : 'danger' }}" style="list-style: none;"><span class="fa {{ $compensations->compute_philhealth == 'Y' ? 'fa-check' : 'fa-close' }}"></span> PhilHealth No.</li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-6">
                                                    @if(in_array(Illuminate\Support\Facades\Auth::user()->role, ['SuperAdmin', 'Admin', 'AdminHR', 'AdminCoop']))
                                                    <div style="max-width: 600px;">
                                                        <a href="{{ url("admin/user/{$id}/compensations-leave-credits/edit") }}" class="btn btn-sm btn-primary pull-right" style="margin-top: 10px;"><span class="fa fa-edit"></span> Edit Compensations</a>
                                                    </div>
                                                    @endif
                                                    <br>
                                                    <br>


                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td>Client</td>
                                                                <td>@if(!empty(strtoupper($employmentDetails->Principal->company_code))) {{ strtoupper($employmentDetails->Principal->company_code) }} @endif</td>
                                                            </tr>
                                                            @if(!empty($employmentDetails->Principal->parent_company_id))
                                                                <tr>
                                                                    <td>Parent Client</td>
                                                                    <td>@if(!empty($employmentDetails->Principal->ParentCompany->company_code)) {{ strtoupper($employmentDetails->Principal->ParentCompany->company_code) }} @endif</td>
                                                                </tr>
                                                            @endif
                                                            <tr>
                                                                <td>Payroll Group</td>
                                                                <td>@if(!empty($user->Compensation->PayrollGroup->group_name)) {{ strtoupper($user->Compensation->PayrollGroup->group_name) }} @endif</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Mode of Transfer</td>
                                                                <td>@if(!empty($user->Compensation->TransferMode->transfer_mode)) {{ strtoupper($user->Compensation->TransferMode->transfer_mode) }} @endif</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Transfer Type</td>
                                                                <td>@if(!empty($user->Compensation->TransferMode->transfer_type)) {{ strtoupper($user->Compensation->TransferMode->transfer_type) }} @endif</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Bank</td>
                                                                <td>@if(!empty($user->Compensation->TransferMode->bank)) {{ strtoupper($user->Compensation->TransferMode->bank) }} @endif</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Mobile Number</td>
                                                                <td>{{ strtoupper($compensations->mobile_number) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Account Number</td>
                                                                <td>{{ strtoupper($compensations->account_number) }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <table class="table" style="max-width: 600px;">
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 160px;">Tax Type</td>
                                                        <td>
                                                            {{ !empty($compensations->tax_type_id) ? $compensations->TaxType->title : '' }}
                                                            @if($compensations->deduct_withholding == 'N')
                                                                <br>
                                                                <small class="text-success"><span class="fa fa-check"></span>
                                                                    Minimum Wage Earner
                                                                </small>
                                                            @else
                                                                <br>
                                                                <small class="text-success"><span class="fa fa-check"></span>
                                                                    Deduct Withholding Tax
                                                                </small>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <hr>

                                            <h3>Rate Details</h3>
                                            <table class="table" style="max-width: 600px;">
                                                <tbody>
                                                    <tr>
                                                        <td width="110">Rate Type</td>
                                                        <td width="">{{ $compensations->rate_type }}</td>
                                                        <td width="130">Monthly Rate</td>
                                                        <td width="">
                                                            @if($compensations->rate_type == 'Monthly')
                                                                {{ number_format($compensations->monthly_rate, 2) }}
                                                            @else
                                                                @if($compensations->daily_category == 'Fixed')
                                                                    {{ number_format($compensations->daily_rate * 26, 2) }}
                                                                @else
                                                                    {{ !empty($compensations->daily_rate_id) ? number_format($compensations->DailyRate->rate * 26, 2) : '' }}
                                                                @endif
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110">Daily Rate <br />Category</td>
                                                        <td width="">{{ $compensations->daily_category }}</td>
                                                        <td width="130">Daily Rate</td>
                                                        <td width="">
{{--                                                            @if($compensations->daily_category == 'Fixed')--}}
                                                                {{ number_format($compensations->daily_rate, 2) }}
                                                            {{--@else--}}
{{--                                                                {{ !empty($compensations->daily_rate_id) ? number_format($compensations->DailyRate->rate, 2) : '' }}--}}
                                                            {{--@endif--}}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110">Location</td>
                                                        <td width="">{{ !empty($compensations->daily_rate_id) ? $compensations->DailyRate->location : '' }}</td>
                                                        <td width="130">Hourly Rate</td>
                                                        <td width="">{{ number_format($compensations->hourly, 2) }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <hr>

                                            <div style="max-width: 600px;">
                                            </div>
                                            <h4>Allowances</h4>
                                            <table class="table" style="max-width: 600px;">
                                                <thead>
                                                    <tr>
                                                        <th>Allowance</th>
                                                        <th>Taxable</th>
                                                        <th>Amount</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if($compensations->daily_category == 'Location')
                                                    <tr>
                                                        <td>ECOLA</td>
                                                        <td>Taxable</td>
                                                        <td>{{ !empty($compensations->DailyRate->ecola) ? number_format($compensations->DailyRate->ecola, 2) : '' }}</td>
                                                        <td></td>
                                                    </tr>
                                                    @endif

                                                    <tr>
                                                        <td>FIXED ALLOWANCE</td>
                                                        <td>Non-Taxable</td>
                                                        <td>{{ number_format($compensations->fixed_allowance, 2) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>DE MINIMIS</td>
                                                        <td>Non-Taxable</td>
                                                        <td>{{ number_format($compensations->de_minimis, 2) }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <hr>

                                            <h3>Leave Credits</h3>
                                            <table class="table" style="max-width: 600px;">
                                                <tbody>
                                                    <tr>
                                                        <td width="40%">YTD Leave Credits</td>
                                                        <td width="60">Days</td>
                                                        <td>@if(\Carbon\Carbon::createFromFormat('Y-m-d', $user->employmentdetails->hire_date)->age > 0) 5 @else 0 @endif</td>
                                                        <td width="60">Hrs</td>
                                                        <td>
                                                            @if(\Carbon\Carbon::createFromFormat('Y-m-d', $user->employmentdetails->hire_date)->age > 0) {{ 5 * 8 }} @else 0 @endif
                                                            <!-- {{  floor(floor(((12 - date('m', strtotime($user->employmentdetails->hire_date))) / 12) * 5) + ((date('Y') - date('Y', strtotime($user->employmentdetails->hire_date))) * 5)) * 8 }} -->
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>YTD Accumulated Leave</td>
                                                        <td>Days</td>
                                                        <td>{{ $leaveHistory->where('status', 'Paid')->sum('days') }}</td>
                                                        <td>Hrs</td>
                                                        <td>{{ $leaveHistory->where('status', 'Paid')->sum('hours') }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>YTD Leave Balance</td>
                                                        <td>Days</td>
                                                        <td>@if(\Carbon\Carbon::createFromFormat('Y-m-d', $user->employmentdetails->hire_date)->age > 0 && $leaveHistory->where('status', 'Paid')->sum('days') < 5) {{ 5 - $leaveHistory->where('status', 'Paid')->sum('days') }} @else 0 @endif</td>
                                                        <td>Hrs</td>
                                                        <td>@if(\Carbon\Carbon::createFromFormat('Y-m-d', $user->employmentdetails->hire_date)->age > 0 && $leaveHistory->where('status', 'Paid')->sum('hours') < 40) {{ (5 * 8) - $leaveHistory->where('status', 'Paid')->sum('hours') }} @else 0 @endif</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <h4>Leave History</h4>

                                            <div style="max-width: 600px; margin-top: 10px;">
                                                <table id="leaveHistory" class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>Date</th>
                                                        <th>Leave Type</th>
                                                        <th>Days</th>
                                                        <th>Hrs</th>
                                                        <th>Status</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($leaveHistory as $leave)
                                                        <tr>
                                                            <td>{{ date('m/d/Y', strtotime($leave->payroll_date)) }}</td>
                                                            <td>{{ $leave->leave_type }}</td>
                                                            <td>{{ $leave->day }}</td>
                                                            <td>{{ $leave->hours }}</td>
                                                            <td>{{ $leave->status }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#leaveHistory").dataTable({
                /* Disable initial sort */
                "aaSorting": []
            });
        });
    </script>
@endsection

@extends('layouts.app')

@section('content')


            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row no-padding">
                                        <div class="col-md-6">
                                            <strong>Name: {{ $user->name() }}</strong>
                                        </div>

                                        <div class="col-md-6">
                                            <strong>ID Number: {{ $user->id_number }}</strong>
                                        </div>

                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                        <div class="col-md-12">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="{{ url("admin/user/{$id}/profile") }}">Personal Information</a>
                                                </li>
                                                @if($user->User->role != 'Member')
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/user/{$id}/employment-details") }}">Employment Details</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="{{ url("admin/user/{$id}/compensations-leave-credits") }}">Compensation and Leave Credits</a>
                                                    </li>
                                                @endif
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="col-md-9">
                                            <br>

                                            <form action="" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}

                                                <div class="row no-padding">

                                                    <div class="col-xs-12"></div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">First Name</label>
                                                            <input type="text" class="form-control disable-empty-val" name="first_name" value="{{ $user->first_name }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Middle Name</label>
                                                            <input type="text" class="form-control disable-empty-val" name="middle_name" value="{{ $user->middle_name }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Last Name</label>
                                                            <input type="text" class="form-control disable-empty-val" name="last_name" value="{{ $user->last_name }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="">Address</label>
                                                            <textarea name="address" id="user-address" rows="3"
                                                                      class="form-control">{{ $user->address }}</textarea>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Civil Status</label>
                                                            <select name="civil_status_id" id="" class="form-control">
                                                                @foreach(\App\CivilStatus::all()->sortBy('title') as $civilStatus)
                                                                    <option value="{{ $civilStatus->id }}" @if($civilStatus->id == $user->civil_status_id) selected @endif>{{ $civilStatus->title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Gender</label>
                                                            <input type="text" class="form-control" name="gender" value="{{ $user->gender }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12"></div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Mobile</label>
                                                            <input type="text" class="form-control" name="mobile1" value="{{ $user->mobile1 }}" maxlength="11" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
                                                            <input type="text" class="form-control" name="mobile2" value="{{ $user->mobile2 }}" maxlength="11" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="margin-top: 5px;">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Email</label>
                                                            <input type="email" class="form-control" name="email" value="{{ $user->email }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Birth Date</label>
                                                            <div class='input-group btn-dtp-bday2'>
                                                                <input type='text' class="form-control" name="birthday" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->birthday)->format('m/d/Y') }}" placeholder="mm/dd/yyyy" required />
                                                                <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12"></div>

                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label for="">Mother's Maiden Name</label>
                                                            <input type="text" class="form-control" name="mothers_maiden_name" value="{{ $user->mothers_maiden_name }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label for="">Educational Attainment</label>
                                                            <select name="educational_attainment_id" id="" class="form-control">
                                                                @foreach(\App\EducationalAttainments::all()->sortBy('title') as $educationalAttainment)
                                                                    <option value="{{ $educationalAttainment->id }}" @if($educationalAttainment->id == $user->educational_attainment_id) selected @endif>{{ $educationalAttainment->title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <hr />
                                                        <h2>In Case of Emergency</h2>
                                                    </div>


                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Contact Person</label>
                                                            <input type="text" class="form-control" name="icoe_contact_name" value="{{ $user->icoe_contact_name }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Contact Number</label>
                                                            <input type="text" class="form-control" name="icoe_contact_mobile" value="{{ $user->icoe_contact_mobile }}" maxlength="11" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="">Address</label>
                                                            <textarea name="icoe_contact_address" id="icoe_contact_address" cols="" rows="3"
                                                                      class="form-control">{{ $user->icoe_contact_address }}</textarea>
                                                            <label class="css-input switch switch-sm switch-primary">
                                                                <input type="checkbox" class="cbox-same-address"><span></span> Same as Employee's Address
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <br>
                                                        <br>

                                                        <div>
                                                            <button class="btn btn-primary pull-right"><span class="fa fa-save"></span> Confirm</button>
                                                            <button class="btn btn-secondary pull-right" style="margin-right: 5px;"><span class="fa fa-ban"></span> Cancel</button>

                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-3">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(".cbox-same-address").click(function() {
                if($(this).is(':checked') == true) {
                    $("#icoe_contact_address").val($("#user-address").val());
                    $("#icoe_contact_address").attr('readonly', true);
                } else {
                    $("#icoe_contact_address").val('');
                    $("#icoe_contact_address").attr('readonly', false);
                }
            });

            $("#user-address").on('keyup', function() {
                if($("#icoe_contact_address").attr('readonly') == 'readonly') {
                    $("#icoe_contact_address").val($("#user-address").val());
                }
            })

            // DATEPICKER
            $('.btn-dtp-bday2').datetimepicker({
                format: "MM/DD/YYYY",
                // minDate: moment().subtract(16, 'years').format('MM/DD/YYYY'),
{{--                defaultDate: moment({{ $user->birthday }}).subtract(16, 'years').format('MM/DD/YYYY')--}}
                //format: "Y-m-d g:i:s",
            });
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')



            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#"><span class="fa fa-users"></span> User Admins</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/manage-admin/users/add') }}"><span class="fa fa-user-plus"></span> New Admin</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('employees_and_members_password_reset') }}">
                                                <span class="fa fa-user"></span> Password Reset
                                            </a>
                                        </li>
                                    </ul>

                                    <br>

                                    <table id="users-dt" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Complete Name</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Company Code</th>
                                                <th>Admin Type</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->username }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>{{ !empty($user->Profile->EmploymentDetails->Principal->company_code) ? $user->Profile->EmploymentDetails->Principal->company_code : $user->Company->company_code }}</td>
                                                <td>{{ $user->role }}</td>
                                                <td><a href="{{ url("/admin/manage-admin/delete/{$user->id}") }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to remove this user?')"><i class="fa fa-remove"></i> Remove</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#users-dt').DataTable({
                {{--"processing": true,--}}
                {{--"serverSide": true,--}}
                {{--"ajax":{--}}
                    {{--"url": "{{ url('api/datatables/admin/users') }}",--}}
                    {{--"dataType": "json",--}}
                    {{--"type": "POST",--}}
                    {{--"data":{ _token: "{{csrf_token()}}"}--}}
                {{--},--}}
                {{--"columns": [--}}
                    {{--{ "data": "name" },--}}
                    {{--{ "data": "username" },--}}
                    {{--{ "data": "email" },--}}
                    {{--{ "data": "company_code" },--}}
                    {{--{ "data": "admin_type" },--}}
                    {{--{ "data": "action" },--}}
{{--//                    { "data": "password_reset"}--}}
                {{--]--}}
            });
        })
    </script>
@endsection

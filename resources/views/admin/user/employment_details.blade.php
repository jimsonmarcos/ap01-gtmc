@extends('layouts.app')

@section('content')


            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row no-padding">
                                        <div class="col-md-12">
                                            <a href="{{ url('/admin/users') }}"><i class="fa fa-arrow-left"></i> Go back to list</a>
                                            <br><br>
                                        </div>
                                        <div class="col-md-6">
                                            <strong>Name: {{ $user->name() }}</strong>
                                        </div>

                                        <div class="col-md-6">
                                            <strong>ID Number: {{ $user->id_number }}</strong>
                                        </div>

                                        <div class="col-md-12">
                                            <hr>
                                        </div>

                                        <div class="col-md-12">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/profile") }}">Personal Information</a>
                                                </li>
                                                @if($user->User->role != 'Member')
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="{{ url("admin/user/{$id}/employment-details") }}">Employment Details</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/compensations-leave-credits") }}">Compensation and Leave Credits</a>
                                                </li>
                                                @endif
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="col-md-12">
                                            @if(in_array(Illuminate\Support\Facades\Auth::user()->role, ['SuperAdmin', 'Admin', 'AdminHR', 'AdminCoop']))
                                            <div style="max-width: 600px;">
                                                <a href="{{ url("admin/user/{$id}/employment-details/edit") }}" class="btn btn-sm btn-primary pull-right" style="margin-top: 10px;"><span class="fa fa-edit"></span> Edit Employment Details</a>
                                            </div>
                                            @endif

                                            <br>
                                            <br>

                                            <table class="table no-margin-bottom" style="max-width: 600px;">
                                                <tbody>
                                                    <tr>
                                                        <td>Employment Status:</td>
                                                        <td><strong>@if(!empty($user->employmentdetails->reason) && date('Y-m-d', strtotime($user->employmentdetails->effective_date)) <= date('Y-m-d') && $user->employmentdetails->status == 'Deactivated') <span class="text-danger">{{ strtoupper($user->employmentdetails->reason) }}</span> @else <span class="text-success">ACTIVE</span> @endif</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110">Principal:</td>
                                                        <td width="">{{ !empty($user->employmentdetails->principal) ? $user->employmentdetails->principal->company_code : 'N/A' }}</td>
                                                        <td width="130">Hire Date:</td>
                                                        <td width="">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->employmentdetails->hire_date)->format('M. j, Y') }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110">Position</td>
                                                        <td colspan="5">@if(!empty($user->employmentdetails->position->title)) {{ $user->employmentdetails->position->title }} @endif <?php if($user->employmentdetails->orientation == 'Y' && $user->employmentdetails->position_id == '1') { echo "<br /> <small class='text-success'><span class='fa fa-check'></span> Orientation</small>"; } ?></td>
                                                    </tr>
                                                    <?php if($user->employmentdetails->position_id == '1') { ?>
                                                    <tr>
                                                        <td>Trainee Start<br />Date</td>
                                                        <td>@if(!empty($user->employmentdetails->trainee_start_date)) {{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->employmentdetails->trainee_start_date)->format('M. j, Y') }} @endif</td>
                                                        <td>Trainee End<br />Date</td>
                                                        <td>@if(!empty($user->employmentdetails->trainee_end_date)) {{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->employmentdetails->trainee_end_date)->format('M. j, Y') }} @endif</td>
                                                    </tr>
                                                    <?php } ?>

{{--                                                    @if($user->EmploymentDetails->position_id == 1 || $user->EmploymentDetails->position_id == 2)--}}
                                                    <tr>
                                                        <td width="110">Department</td>
                                                        <td colspan="5">
                                                            <?php
                                                            $department = [];
                                                            if(!empty($user->EmploymentDetails->Outlet->outlet)) $department[] = $user->EmploymentDetails->Outlet->outlet;
                                                            if(!empty($user->EmploymentDetails->Mall->mall)) $department[] = $user->EmploymentDetails->Mall->mall;
                                                            if(!empty($user->EmploymentDetails->Area->area)) $department[] = $user->EmploymentDetails->Area->area;
                                                            if(!empty($user->EmploymentDetails->Province->province)) $department[] = $user->EmploymentDetails->Province->province;
                                                            echo implode(' - ', $department);
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110">City/Province</td>
                                                        <td>@if(!empty($user->EmploymentDetails->Province->province)) {{ strtoupper($user->EmploymentDetails->Province->province) }} @endif</td>
                                                        <td>Mall</td>
                                                        <td colspan="3">@if(!empty($user->EmploymentDetails->Mall->mall)) {{ strtoupper($user->EmploymentDetails->Mall->mall) }} @endif</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110">Area</td>
                                                        <td>@if(!empty($user->EmploymentDetails->Area->area)) {{ strtoupper($user->EmploymentDetails->Area->area) }} @endif</td>
                                                        <td>Outlet</td>
                                                        <td colspan="3">@if(!empty($user->EmploymentDetails->Outlet->outlet)) {{ strtoupper($user->EmploymentDetails->Outlet->outlet) }} @endif</td>
                                                    </tr>
                                                    {{--@endif--}}

                                                    @if(in_array($user->employmentdetails->position_id, [1, 2]))
                                                    <tr>
                                                        <td width="110">Coordinator</td>
                                                        <td colspan="5">{{ !empty($user->EmploymentDetails->Coordinator) ? strtoupper($user->EmploymentDetails->Coordinator->name()) : '' }}</td>
                                                    </tr>
                                                    @endif

                                                    <tr>
                                                        <td width="110">SSS No.</td>
                                                        <td colspan="5">{{ $user->Compensation->sss }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110">HDMF No.</td>
                                                        <td colspan="5">{{ $user->Compensation->hdmf }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110">PHIC No.</td>
                                                        <td colspan="5">{{ $user->Compensation->philhealth }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="110">Tax ID Number</td>
                                                        <td colspan="5">{{ $user->Compensation->tin }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

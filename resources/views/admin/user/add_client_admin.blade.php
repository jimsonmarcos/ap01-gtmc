@extends('layouts.app')

@section('content')

                <!-- Dashboard Counts Section-->
                <section class="dashboard-counts no-padding-bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-md-4 col-md-offset-4">
                                                <div class="row">
                                                    <div class="col-md-12 text-center">
                                                        <div class="form-group">
                                                            <div class="col-xs-12">
                                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                                    <input type="radio" class="selectUserType" name="user_type" value="User" checked><span></span> User
                                                                </label>
                                                                <label class="css-input css-radio css-radio-primary">
                                                                    <input type="radio" class="selectUserType" name="user_type" value="ContactPerson"><span></span> Contact Person
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <form id="userForm" action="" class="form" method="POST" enctype="multipart/form-data" style="margin-top: 20px;">
                                            {{ csrf_field() }}
                                            <div class="row">

                                                <div class="col-md-4 col-md-offset-4">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Select a User</label>
                                                                <select id="employeeID" type="text" class="form-control" name="user_id" placeholder=""  style="width: 100%;"></select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Company Code</label>
                                                                <select name="company_id" id="company" class="form-control">
                                                                    @foreach(\App\Companies::where('status', 'Active')->orderBy('company_code')->get() as $company)
                                                                        <option value="{{ $company->id }}">{{ $company->company_code }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Name</label>
                                                                <input id="name" type="text" class="form-control" name="" placeholder="" disabled />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Email Address</label>
                                                                <input id="email" type="text" class="form-control" name="last_name" placeholder="" disabled />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Client Type</label>
                                                                <select name="client_type" id="userType" class="form-control">
                                                                    <option value="ClientUser">User</option>
                                                                    <option value="ClientAdmin">Admin</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <h4>Username</h4>
                                                            <div class="alert alert-primary" role="alert">
                                                                <strong id="generatedUsername">xxxxx</strong>
                                                            </div>
                                                            <input type="hidden" id="clientUsername" name="client_username">
                                                            <input type="hidden" id="clientUserId" name="user_id">
                                                            {{--<input type="hidden" id="clientCompanyId" name="company_id">--}}
                                                        </div>

                                                        <div class="col-md-12 text-right">
                                                            <button class="btn btn-success"><span class="fa fa-save"></span> Submit</button>
                                                            <a href="{{ url('/admin/manage-admin/clients') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <form id="contactPersonForm" action="" class="form" method="POST" enctype="multipart/form-data" style="margin-top: 20px; display: none;">
                                            {{ csrf_field() }}
                                            <div class="row">

                                                <div class="col-md-4 col-md-offset-4">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Company Code</label>
                                                                <select name="company_id" id="company" class="form-control" required>
                                                                    <option value="">Select Company</option>
                                                                    @foreach(\App\Companies::orderBy('company_code')->get() as $company)
                                                                        <option value="{{ $company->id }}" data-contactpersons="{{ $company->ContactPersons->whereNotIn('id', \App\User::where('contact_person_id', '!=', '')->pluck('contact_person_id')) }}">{{ $company->company_code }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Select Contact Person</label>
                                                                <select name="contact_person_id" id="contact_persons" class="form-control" disabled>

                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Email Address</label>
                                                                <input id="email" type="text" class="form-control" name="" placeholder="" disabled />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Client Type</label>
                                                                <select name="client_type" id="userType" class="form-control">
                                                                    <option value="ClientUser">User</option>
                                                                    <option value="ClientAdmin">Admin</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <h4>Username</h4>
                                                            <div class="alert alert-primary" role="alert">
                                                                <strong id="generatedUsername">xxxxx</strong>
                                                            </div>
                                                            <input type="hidden" id="username" name="username">
                                                        </div>

                                                        <div class="col-md-12 text-right">
                                                            <button class="btn btn-success"><span class="fa fa-save"></span> Submit</button>
                                                            <a href="{{ url('/admin/manage-admin/clients') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back</a>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(".selectUserType").click(function() {
               if($(this).val() == 'User') {
                   $("#contactPersonForm").slideUp('normal', function() {
                       $("#userForm").slideDown('normal');
                   });
               } else {
                   $("#userForm").slideUp('normal', function() {
                       $("#contactPersonForm").slideDown('normal');
                   });
               }
            });

            $(document).on('change', '#contactPersonForm #company', function() {
                var contactPersons = $(this).find(':selected').data('contactpersons');

                $(this).closest('form').find("#email").val('');
                $("#contactPersonForm #generatedUsername").text('xxxxx');
                $("#contactPersonForm #username").val('');

                if(contactPersons.length == 0) {
                    $("#contact_persons").prop('disabled', true);
                    $("#contact_persons").find('option').remove();
                    $("#contact_persons").append('<option>No Contact Persons found.</option>');
                } else {
                    $("#contact_persons").prop('disabled', false);
                    $("#contact_persons").find('option').remove();
                    $("#contact_persons").append("<option>Select Contact Person</option>");
                    $.each(contactPersons, function(index, value) {
                        var username = value.first_name.substring(0,1);
                        // if((value.middle_name).length > 0) {
                        //     username = username + value.middle_name.substring(0,1) + ". ";
                        // }
                        username = username + value.last_name;
                        username = username.replace(' ', '').toLowerCase();
                        // var username = (value.first_name.substring(0,1) + value.middle_name.substring(0,1) + value.last_name).replace(' ', '').toLowerCase();

                        $("#contact_persons").append("<option value='"+ value.id +"' data-email='"+ value.email +"' data-username='"+ username +"'>"+ value.first_name +" "+ value.last_name +"</option>");
                    });
                }
            });

            $(document).on("change", "#contactPersonForm #contact_persons", function() {
                var email = $(this).find(":selected").attr('data-email');

                $(this).closest('form').find("#email").val(email);
                updateCPUsername();
            });

            $("#contactPersonForm #userType").change(function() {
                updateCPUsername();
            });

            function updateCPUsername() {
                var username = $("#contactPersonForm #contact_persons").find(":selected").attr('data-username');
                var companyCode = $("#contactPersonForm #company").find(':selected').text().toLowerCase().replace(' ', '_');

                if($("#contactPersonForm #userType").find(":selected").val() == 'ClientUser') {
                    $("#contactPersonForm #generatedUsername").text(companyCode + "." + username);
                    $("#contactPersonForm #username").val(companyCode + "." + username);
                } else {
                    $("#contactPersonForm #generatedUsername").text(companyCode + ".admin." + username);
                    $("#contactPersonForm #username").val(companyCode + ".admin." + username);
                }
            }

            // "Username > if clientAdmin [companycode].admin.[FirstNameInitial][MiddleNameInitial][LastName]"
            //"Username > if clientUser [companycode].[FirstNameInitial][MiddleNameInitial][LastName]"



            $("#employeeID").select2({
                placeholder: {
                    id: '-1', // the value of the option
                    id_number: "Search user here..."
                },
                ajax: {
                    url: "{{ url('api/users/all/Active') }}",
                    dataType: 'json',
                    delay: 300,
                    data: function (params) {
                        return {
                            q: params.term, // search term
//                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            function formatRepo (user) {
                if (user.loading) {
                    return user.name;
                }
//                console.log(user);

                var markup = "<span data-fname='"+ user.first_name +"' data-mname='"+ user.middle_name +"' data-lname='"+ user.last_name +"' data-email='"+ user.email+"' data-company='"+ user.company_code+"'>"+ user.id_number +"</span>"

                return markup;
            }

            function formatRepoSelection (user) {
                return user.id_number;
            }


            $("#employeeID").on("select2:select", function (evt) {
//                console.log(evt.params.data);
                var user = evt.params.data;
//                $("#employee_name").val(user.name);
//                $("#cost_center").val(user.cost_center);
                var username = user.first_name;
                    if((user.middle_name).length > 0) {
                        username = username + " "+ user.middle_name.substring(0,1) + ". ";
                        username = username + user.last_name;
                    } else {
                        username = username + " " + user.last_name;
                    }

                // console.log(user);

                $("#userForm #company_code").val(user.principal);
                $("#userForm #email").val(user.email);
                $("#userForm #name").val(username);
                // $("#userForm #clientCompanyId").val(user.company_id);
{{--                location.href = "{{url("/admin/transactions/loans/request?user_id=")}}" + user.user_id;--}}

//                $("#searchEmail").attr('value', user.email);
//                $("#searchCompanyCode").attr('value', user.principal);
//                $("#searchUserId").attr('value', user.user_id);
//                var adminusername = 'admin.' + user.first_name.substring(0, 1).toLowerCase() + user.middle_name.substring(0, 1).toLowerCase() + user.last_name.trim().toLowerCase().replace(' ', '_');
//                $("#searchUsername").attr('value', adminusername);
//                $("#adminUsername").text(adminusername);

                updateUserUsername();
            });

            $("#userForm #userType, #userForm #company").change(function() {
                updateUserUsername();
            });

            function updateUserUsername() {
                var user = $('#employeeID').select2('data')[0];
                var username = user.first_name.substring(0, 1).toLowerCase() + user.middle_name.substring(0, 1).toLowerCase() + user.last_name.trim().toLowerCase().replace(' ', '_');
                // var companyCode = user.principal.toLowerCase().replace(' ', '_');
                var companyCode = $("#userForm #company").find(':selected').text().toLowerCase().replace(' ', '_');

                if($("#userForm #userType").find(":selected").val() == 'ClientUser') {
                    $("#userForm #generatedUsername").text(companyCode + "." + username);
                    $("#userForm #clientUsername").val(companyCode + "." + username);
                    $("#userForm #clientUserId").val(user.user_id);
                } else {
                    $("#userForm #generatedUsername").text(companyCode + ".admin." + username);
                    $("#userForm #clientUsername").val(companyCode + ".admin." + username);
                    $("#userForm #clientUserId").val(user.user_id);
                }
            }
        });
    </script>
@endsection

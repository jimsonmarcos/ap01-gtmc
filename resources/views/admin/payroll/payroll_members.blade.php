@extends('layouts.app')

@section('content')

    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ url('/admin/payroll/wizard/')}}">Wizard</a></li>
        <li>Payroll Members</li>
    </ol>

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Group</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->PayrollGroup->group_name }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Period</label>
                                            <input type="text" class="form-control" name="payroll_period" value="{{ $payrollCycle->payroll_period }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Code</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->payroll_code }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Date</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ date('m/d/Y', strtotime($payrollCycle->payroll_date)) }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <hr />
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Status</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->status }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label">&nbsp; </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" />
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                @if(in_array($payrollCycle->status, ['Open', 'In Progress']))
                                <div class="col-md-12 text-right">
                                    @if($approvedItems->count() > 0)
                                        <a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/reset") }}" class="btn btn-danger">
                                            Reset <i class="fa fa-refresh" aria-hidden="true"></i>
                                        </a>
                                    @endif

                                    <a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/upload_dtr") }}" class="btn btn-primary">
                                        <i class="fa fa-upload" aria-hidden="true"></i> Upload Timesheet
                                    </a>
                                    {{--<a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/upload_dtr/manually") }}" class="btn btn-primary">--}}
                                        {{--<i class="fa fa-upload" aria-hidden="true"></i> Upload DTR Manually--}}
                                    {{--</a>--}}
                                    @if($ongoingItems->count() == 0 && $approvedItems->count() > 0)
                                        <a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/process") }}" class="btn btn-primary">
                                            Process <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        </a>
                                    @endif
                                </div>
                                @else



                                    <div class="col-md-12 text-right">
                                        <a href="#" id="exportToExcel" data-href="{{ url("/admin/payroll/wizard/export-processed/{$payrollCycle->id}") }}" class="btn btn-primary">Export to Excel</a>
                                        <a href="#" id="printPayrollMembers" data-href="{{ url("/print/payslips/{$payrollCycle->id}") }}" class="btn-primary btn" target="_blank">Print</a>
                                        <a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/processed_payroll") }}" class="btn btn-primary">
                                            View Processed Payroll <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                @endif

                                @if($approvedItems->count() > 0)
                                <div class="col-md-12 push-20-t">
                                    <table id="dt" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Employee ID Number</th>
                                            <th>Employee Name</th>
                                            <th>Rate Type</th>
                                            <th>Payroll Status</th>
                                            <th>Source</th>
                                            <th>Updated By</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($allItems as $item)
                                                <tr>
                                                    <td></td>
                                                    <td>{{ $item->id_number }}</td>
                                                    <td>{{ $item->User->Profile->namelfm() }}</td>
                                                    <td>{{ $item->User->Profile->Compensation->rate_type }}</td>
                                                    <td>@if(!empty($item->status))Submitted & Approved @else Ongoing @endif</td>
                                                    <td>{{ $item->source }}</td>
                                                    <td>@if(!empty($item->Draft->UpdatedBy->name)) {{ $item->Draft->UpdatedBy->name }} @endif</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            var tbl = $('#dt').DataTable({
                "sorting": [],
                "pageLength": 100,
                columnDefs: [ {
                    orderable: false,
                    className: 'select-checkbox',
                    targets:   0
                } ],
                select: {
                    style:    'multi',
                    selector: 'td:first-child'
                },
                order: [[ 2, 'asc' ]]

            });

            $(document).on('click', "#exportToExcel, #printPayrollMembers", function(e) {
                e.preventDefault();

                var uri = [];
                $.each(tbl.rows('.selected').nodes(), function(i, item) {
                    var data = tbl.row(this).data();
                    uri.push({
                        'id_number' : data[1]
                    });
                    // var uri[i] = [];
                    // uri[i]['id_number'] = data[1];
                    // uri = uri + "id_number[]=" + data[1] + "&";

                    console.log(data);
                });

                uri = encodeURIComponent(JSON.stringify(uri));

                console.log(uri);

                location.href = $(this).data('href') + "?data=" + uri;
            });
        })
    </script>
@endsection

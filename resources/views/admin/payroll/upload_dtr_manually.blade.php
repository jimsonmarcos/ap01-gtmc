@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('payroll_wizard_index')}}">Wizard</a></li>
        <li><a class="link-effect" href="{{ route('payroll_members')}}">Payroll Members</a></li>
        <li><a class="link-effect" href="{{ route('payroll_upload_dtr')}}">Upload Timesheet</a></li>
        <li><a class="link-effect" href="{{ route('payroll_upload_dtr_manually')}}">Manually</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-xs-12" for="payroll_group">Payroll Group</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="payroll_group" name="payroll_group" size="1" required>
                                                <option value="">Cycle 1</option>
                                            </select>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Period</label>
                                            <div id="payroll_period" class="input-group btn-mmddyyyy ">
                                                <input type="text" class="form-control" name="payroll_period" placeholder="mm/dd/yyyy" required>
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-xs-12" for="payroll_code">Payroll Code</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="payroll_code" name="payroll_code" size="1" required>
                                                <option value="">Cycle 1</option>
                                            </select>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Date</label>
                                            <div id="payroll_date" class="input-group btn-mmddyyyy ">
                                                <input type="text" class="form-control" name="payroll_date" placeholder="mm/dd/yyyy" required>
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <hr />
                                </div>


                                <div class="col-md-12">
                                    <form method="post">
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="employee_id">Employee ID</label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" type="text" id="employee_id" name="employee_id" />
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="employee_name">Employee Name</label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" type="text" id="employee_name" name="employee_name" />
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <ul class="nav nav-tabs" id="upload_dtr_manual" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#ddtime-entry" role="tab" aria-controls="ddtime-entry">Time Entry</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#ddsummary" role="tab" aria-controls="ddsummary">Summary</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active table-responsive" id="ddtime-entry" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-md-2" style="padding-top: 3.5em;">
                                                        <div class="form-group">
                                                            <select name="hours_and minutes" class="form-control">
                                                                <option>HRS</option>
                                                                <option>Mins</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="padding-top: 0.5em;">
                                                            <select name="hours_and minutes" class="form-control">
                                                                <option>HRS</option>
                                                                <option>Mins</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="padding-top: 0.5em;">
                                                            <select name="hours_and minutes" class="form-control">
                                                                <option>HRS</option>
                                                                <option>Mins</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="padding-top: 0.5em;">
                                                            <select name="hours_and minutes" class="form-control">
                                                                <option>HRS</option>
                                                                <option>Mins</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="padding-top: 0.5em;">
                                                            <select name="hours_and minutes" class="form-control">
                                                                <option>HRS</option>
                                                                <option>Mins</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 text-center">
                                                        <h6 style="padding-top: 5.5em;"> WH </h6>
                                                        <h6 style="padding-top: 3.5em;"> ABS </h6>
                                                        <h6 style="padding-top: 3.5em;"> LT </h6>
                                                        <h6 style="padding-top: 3em;"> UT </h6>
                                                        <h6 style="padding-top: 3em;"> ND </h6>
                                                        <h6 style="padding-top: 3em;"> VL </h6>
                                                        <h6 style="padding-top: 3em;"> SL </h6>
                                                        <h6 style="padding-top: 3em;"> EL </h6>
                                                        <h6 style="padding-top: 3.5em;"> SPL </h6>
                                                        <h6 style="padding-top: 3.5em;"> ML </h6>
                                                        <h6 style="padding-top: 3.5em;"> PL </h6>
                                                        <h6 style="padding-top: 3.5em;"> PTO </h6>
                                                        <h6 style="padding-top: 3em"> BL </h6>
                                                        <h6 style="padding-top: 3em;"> CL </h6>
                                                    </div>
                                                    <div class="col-md-9" style="overflow-x: scroll;: ">
                                                        <table class="table table-striped">
                                                            <thead>
                                                            <th>1</th>
                                                            <th>2</th>
                                                            <th>3</th>
                                                            <th>4</th>
                                                            <th>5</th>
                                                            <th>6</th>
                                                            <th>7</th>
                                                            <th>8</th>
                                                            <th>9</th>
                                                            <th>10</th>
                                                            <th>11</th>
                                                            <th>12</th>
                                                            <th>13</th>
                                                            <th>14</th>
                                                            <th>15</th>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            for($x=0; $x<=13; $x++){ ?>
                                                            <tr>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                            </tr>
                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-12"><hr/></div>
                                                    <div class="col-md-2" style="padding-top: 3.5em;">
                                                        <div class="form-group">
                                                            <select name="hours_and minutes" class="form-control">
                                                                <option>HRS</option>
                                                                <option>Mins</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="padding-top: 0.5em;">
                                                            <select name="hours_and minutes" class="form-control">
                                                                <option>HRS</option>
                                                                <option>Mins</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="padding-top: 0.5em;">
                                                            <select name="hours_and minutes" class="form-control">
                                                                <option>HRS</option>
                                                                <option>Mins</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="padding-top: 0.5em;">
                                                            <select name="hours_and minutes" class="form-control">
                                                                <option>HRS</option>
                                                                <option>Mins</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="padding-top: 0.5em;">
                                                            <select name="hours_and minutes" class="form-control">
                                                                <option>HRS</option>
                                                                <option>Mins</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="padding-top: 0.5em;">
                                                            <select name="hours_and minutes" class="form-control">
                                                                <option>HRS</option>
                                                                <option>Mins</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="padding-top: 0.5em;">
                                                            <select name="hours_and minutes" class="form-control">
                                                                <option>HRS</option>
                                                                <option>Mins</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 text-center">
                                                        <h6 style="padding-top: 5.5em;"> WH </h6>
                                                        <h6 style="padding-top: 3.5em;"> RD </h6>
                                                        <h6 style="padding-top: 3.5em;"> SPE </h6>
                                                        <h6 style="padding-top: 3em;"> SPERD </h6>
                                                        <h6 style="padding-top: 3em;"> LEG </h6>
                                                        <h6 style="padding-top: 3.5em;"> LEGRD </h6>
                                                        <h6 style="padding-top: 3em;"> OT </h6>
                                                    </div>
                                                    <div class="col-md-9" style="overflow-x: scroll;: ">
                                                        <table class="table table-striped">
                                                            <thead>
                                                            <th>1</th>
                                                            <th>2</th>
                                                            <th>3</th>
                                                            <th>4</th>
                                                            <th>5</th>
                                                            <th>6</th>
                                                            <th>7</th>
                                                            <th>8</th>
                                                            <th>9</th>
                                                            <th>10</th>
                                                            <th>11</th>
                                                            <th>12</th>
                                                            <th>13</th>
                                                            <th>14</th>
                                                            <th>15</th>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            for($x=0; $x<=6; $x++){ ?>
                                                            <tr>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" placeholder="Whole number" min="1" max="10"/>
                                                                </td>
                                                            </tr>
                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="ddsummary" role="tabpanel">
                                                <div class="row">
                                                    <br />
                                                    <div class="col-md-4">
                                                        <h3>Hours</h3>
                                                        <br />
                                                        <p>Work Hours: &nbsp;<span style="padding-left: 4em">8</span></p>
                                                        <p>Rest Day: &nbsp;<span style="padding-left: 5.4em">-</span></p>
                                                        <p>Special Holiday: &nbsp;<span style="padding-left: 2.1em">-</span></p>
                                                        <p>Special RD Holiday: &nbsp;<span style="padding-left: 0.5em">-</span></p>
                                                        <p>Legal Holiday: &nbsp;<span style="padding-left: 3em">-</span></p>
                                                        <p>Legal RD Holiday: &nbsp;<span style="padding-left: 1.4em">-</span></p>
                                                        <p>Overtime: &nbsp;<span style="padding-left: 5em">-</span></p>
                                                        <p>Night Differential: &nbsp;<span style="padding-left: 20px">-</span></p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h3>Deduction</h3>
                                                        <br />
                                                        <p>Absents: &nbsp;<span style="padding-left: 4em">8</span></p>
                                                        <p>Tardiness: &nbsp;<span style="padding-left: 3.4em">-</span></p>
                                                        <p>Undertime: &nbsp;<span style="padding-left: 3em">-</span></p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h3>Leaves</h3>
                                                        <br />
                                                        <p>Vacation Leave: &nbsp;<span style="padding-left: 4em">8</span></p>
                                                        <p>Sick Leave: &nbsp;<span style="padding-left: 6.8em">-</span></p>
                                                        <p>Emergency Leave: &nbsp;<span style="padding-left: 3.1em">-</span></p>
                                                        <p>Solo Parent Leave: &nbsp;<span style="padding-left: 3.2em">-</span></p>
                                                        <p>Maternity Leave: &nbsp;<span style="padding-left: 4.1em">-</span></p>
                                                        <p>Paternity Leave: &nbsp;<span style="padding-left: 4.4em">-</span></p>
                                                        <p>Paid Time-Off: &nbsp;<span style="padding-left: 5.2em">-</span></p>
                                                        <p>Bereavement Leave: &nbsp;<span style="padding-left: 2em;">-</span></p>
                                                        <p>Calamity Leave: &nbsp;<span style="padding-left: 4.2em">-</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 text-right">
                                    <a href="#" class="btn btn-primary">
                                        <i class="fa fa-calculator" aria-hidden="true"></i> Calculate
                                    </a>
                                    <a href="#" class="btn btn-danger">
                                        <i class="fa fa-times" aria-hidden="true"></i> Cancel
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#upload_dtr_manual a:first').tab('show')
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('payroll_wizard_index')}}">Wizard</a></li>
        <li><a class="link-effect" href="{{ route('payroll_members')}}">Payroll Members</a></li>
        <li><a class="link-effect" href="{{ route('payroll_upload_dtr')}}">Upload Timesheet</a></li>
        <li><a class="link-effect" href="{{ route('payroll_upload_dtr_failed')}}">Failed</a></li>
    </ol>
                <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h1>Failed Upload</h1>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-xs-12" for="payroll_group">Payroll Group</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="payroll_group" name="payroll_group" size="1" required>
                                                <option value="">Cycle 1</option>
                                            </select>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Period</label>
                                            <div id="payroll_period" class="input-group btn-mmddyyyy ">
                                                <input type="text" class="form-control" name="payroll_period" placeholder="mm/dd/yyyy" required>
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-xs-12" for="payroll_code">Payroll Code</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="payroll_code" name="payroll_code" size="1" required>
                                                <option value="">Cycle 1</option>
                                            </select>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Date</label>
                                            <div id="payroll_date" class="input-group btn-mmddyyyy ">
                                                <input type="text" class="form-control" name="payroll_date" placeholder="mm/dd/yyyy" required>
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <hr />
                                </div>
                                <div class="col-md-12">
                                    <form method="post">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="employee_id">Employee ID</label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" type="text" id="employee_id" name="employee_id" />
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-xs-12" for="employee_name">Employee Name</label>
                                                    <div class="col-xs-12">
                                                        <input class="form-control" type="text" id="employee_name" name="employee_name" />
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-12">
                                    {{--if employee has no record --}}
                                    {{--Show This--}}
                                    {{--This is not found in the master list.--}}
                                    {{--Else--}}
                                    {{--Show This--}}
                                    {{--Table Below--}}
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        for ($x=0; $x<5; $x++){ ?>
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="form-control"/>
                                            </td>
                                            <td>Manual</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12 text-right">
                                    {{--If employee has no record--}}
                                    {{--Dont display this Overwrite Button--}}
                                    <a href="#" class="btn btn-primary">
                                        <i class="fa fa-pencil" aria-hidden="true"></i> Overwrite
                                    </a>
                                    <a href="#" class="btn btn-danger">
                                        <i class="fa fa-times" aria-hidden="true"></i> Remove
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('final_pay')}}">Final Pay</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <ul class="nav nav-tabs" id="client_profile_tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link text-primary" href="{{ url('/admin/payroll/final_pay/Hold') }}"><span class="badge badge-primary">{{ $holdEmployees }}</span> Hold</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-warning @if($status == 'Pending') active @endif" href="{{ url('/admin/payroll/final_pay/Pending') }}"><span class="badge badge-warning">{{ $pending }}</span> Pending</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-info @if($status == 'Approved') active @endif" href="{{ url('/admin/payroll/final_pay/Approved') }}"><span class="badge badge-info">{{ $approved }}</span> Approved</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-success @if($status == 'Released') active @endif" href="{{ url('/admin/payroll/final_pay/Released') }}"><span class="badge badge-success">{{ $released }}</span> Released</a>
                                    </li>
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link text-success @if($status == 'Disbursed') active @endif" href="{{ url('/admin/payroll/final_pay/Disbursed') }}"><span class="badge badge-success">{{ $disbursed }}</span> Disbursed</a>--}}
                                    {{--</li>--}}
                                </ul>

                                <br />

                                {{--<div>--}}
                                    {{--<button class="btn btn-primary pull-right addFinalPayModal"><i class="fa fa-plus"></i> Add Final Pay</button>--}}
                                    {{--<br>--}}
                                    {{--<br>--}}
                                {{--</div>--}}

                                <br>
                                <br>

                                <table id="final_pay" class="dt table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Employee ID Number</th>
                                        <th>Employee Name</th>
                                        <th>Cost Center</th>
                                        <th>Net Pay</th>
                                        <th>Date Created</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($employees as $employee)
                                        <tr>
                                            <td>{{ $employee->User->Profile->id_number }}</td>
                                            <td>{{ $employee->User->Profile->namelfm() }}</td>
                                            <td>{{ $employee->User->Profile->CostCenter->cost_center }}</td>
                                            <td>{{ number_format($employee->net_payable, 2) }}</td>
                                            <td>
                                                {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $employee->created_at)->format('m/d/Y') }}
                                            </td>
                                            <td>{{ $employee->status }}</td>
                                            <td>
                                                <a href="{{ url("/admin/payroll/final_pay/preview/{$employee->id}") }}"  class="btn btn-success"><i class="fa fa-print"></i> PREVIEW</a>
                                                @if($status == 'Hold')
                                                    <a href="{{ url("/admin/payroll/final_pay/add/{$employee->user_id}") }}" class="btn btn-link"><i class="fa fa-print"></i> PREVIEW</a>
                                                @elseif($status == 'Pending')
                                                    <a href="{{ url("/admin/payroll/final_pay/update/{$employee->id}") }}"  class="btn btn-primary">UPDATE</a>
                                                @elseif($status == 'Approved')
                                                    <a href="{{ url("/admin/payroll/final_pay/disburse/{$employee->id}") }}"  class="btn btn-success">RELEASE</a>
                                                @elseif($status == 'Released')
{{--                                                    <a href="{{ url("/admin/payroll/final_pay/disburse/{$employee->id}") }}"  class="btn btn-success">DISBURSED</a>--}}
                                                @endif


                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <div id="addFinalPayModal" tabindex="-1" role="dialog" aria-labelledby="addFinalPayModal" class="modal fade text-left" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">Select Employee</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label for="employee_id">ID Number</label>
                            <div class="">
                                <select class="form-control" id="employeeID" name="" style="width: 100%;">

                                </select>
                            </div>
                            <small>Search a user first to proceed.</small>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                    <a id="url" class="btn btn-primary" style="color: #FFF;">Confirm</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#final_pay').dataTable({
                "pageLength": 100
            });

            {{--$(document).on('click', '.addFinalPayModal', function(e) {--}}
                {{--e.preventDefault();--}}

                {{--$("#addFinalPayModal").modal('show');--}}
            {{--});--}}

            {{--$("#employeeID").select2({--}}
                {{--dropdownParent: $("#addFinalPayModal"),--}}
                {{--placeholder: 'Search GTMC User here..',--}}
                {{--ajax: {--}}
                    {{--url: "{{ url('api/users/all') }}",--}}
                    {{--dataType: 'json',--}}
                    {{--delay: 300,--}}
                    {{--data: function (params) {--}}
                        {{--return {--}}
                            {{--q: params.term, // search term--}}
{{--//                            page: params.page--}}
                        {{--};--}}
                    {{--},--}}
                    {{--processResults: function (data, params) {--}}
                        {{--// parse the results into the format expected by Select2--}}
                        {{--// since we are using custom formatting functions we do not need to--}}
                        {{--// alter the remote JSON data, except to indicate that infinite--}}
                        {{--// scrolling can be used--}}
                        {{--params.page = params.page || 1;--}}

                        {{--return {--}}
                            {{--results: data.items,--}}
                            {{--pagination: {--}}
                                {{--more: (params.page * 30) < data.total_count--}}
                            {{--}--}}
                        {{--};--}}
                    {{--},--}}
                    {{--cache: true--}}
                {{--},--}}
                {{--escapeMarkup: function (markup) { return markup; }, // let our custom formatter work--}}
                {{--minimumInputLength: 1,--}}
                {{--templateResult: formatRepo,--}}
                {{--templateSelection: formatRepoSelection--}}
            {{--});--}}

            {{--function formatRepo (user) {--}}
                {{--if (user.loading) {--}}
                    {{--return user.name;--}}
                {{--}--}}
{{--//                console.log(user);--}}

                {{--var markup = "<span data-fname='"+ user.first_name +"' data-mname='"+ user.middle_name +"' data-lname='"+ user.last_name +"' data-email='"+ user.email+"' data-company='"+ user.company_code+"'>"+ user.id_number +"</span>"--}}

                {{--return markup;--}}
            {{--}--}}

            {{--function formatRepoSelection (user) {--}}
                {{--return user.id_number;--}}
            {{--}--}}


            {{--$("#employeeID").on("select2:select", function (evt) {--}}
                {{--var user = evt.params.data;--}}
                {{--// console.log(user);--}}

                {{--$("#url").attr('href', "{{url("/admin/payroll/final_pay/add?user_id=")}}" + user.user_id);--}}
            {{--});--}}
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('payroll_wizard_index')}}">Wizard</a></li>
        <li><a class="link-effect" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}") }}">Payroll Members</a></li>
        <li><a class="link-effect" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/upload_dtr")}}">Upload Timesheet</a></li>
    </ol>

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Group</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->PayrollGroup->group_name }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Period</label>
                                            <input type="text" class="form-control" name="payroll_period" value="{{ $payrollCycle->payroll_period }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Code</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->payroll_code }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Date</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ date('m/d/Y', strtotime($payrollCycle->payroll_date)) }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <div class="well">
                                        <form action="" method="POST" enctype="multipart/form-data">
                                            {{ csrf_field() }}

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="form-control-label">*Upload .xls, .xlsx, .csv</label>
                                                    <div class="form-controler">
                                                        <input type="file" class="form-control" name="dtr" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-2" style="margin-top: 25px;">
                                                    <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <br />
                                </div>

                                @if($allItems->count() > 0)
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link @if($status == 'All') active @endif" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/upload_dtr") }}">
                                                <span class="badge badge-primary">{{ $allItems->count() }}</span> All
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if($status == 'Success') active @endif" href="{{ url(url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/upload_dtr/status/Success")) }}">
                                                <span class="badge badge-success">{{ \App\PayrollDtrDraft::where(['payroll_cycle_id' => $payrollCycleId, 'status' => 'Success'])->count() }}</span> Success</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if($status == 'Failed') active @endif" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/upload_dtr/status/Failed") }}">
                                                <span class="badge badge-warning">{{ \App\PayrollDtrDraft::where(['payroll_cycle_id' => $payrollCycleId, 'status' => 'Failed'])->count() }}</span> Failed
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if($status == 'Existing') active @endif" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/upload_dtr/status/Existing") }}">
                                                <span class="badge badge-warning">{{ \App\PayrollDtrDraft::where(['payroll_cycle_id' => $payrollCycleId, 'status' => 'Existing'])->count() }}</span> Existing
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if($status == 'Removed') active @endif" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/upload_dtr/status/Removed") }}">
                                                <span class="badge badge-danger">{{ \App\PayrollDtrDraft::where(['payroll_cycle_id' => $payrollCycleId, 'status' => 'Removed'])->count() }}</span> Removed
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if($status == 'Overwrite') active @endif" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/upload_dtr/status/Overwrite") }}">
                                                <span class="badge badge-info">{{ \App\PayrollDtrDraft::where(['payroll_cycle_id' => $payrollCycleId, 'status' => 'Overwrite'])->count() }}</span> Overwrite
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if($status == 'No Match') active @endif" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/upload_dtr/status/No Match") }}">
                                                <span class="badge badge-default">{{ \App\PayrollDtrDraft::where(['payroll_cycle_id' => $payrollCycleId, 'status' => 'No Match'])->count() }}</span> No Match
                                            </a>
                                        </li>
                                    </ul>
                                </div>


                                    <div class="col-md-12">
                                        <form action="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/upload_dtr/status/Existing") }}" method="POST">
                                            {{ csrf_field() }}

                                            <table id="existingDtr" class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th><input type="checkbox" id="selectAll"></th>
                                                    <th>Employee ID Number</th>
                                                    <th>Employee Name</th>
                                                    <th></th>
                                                    <th width="150">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($items as $item)
                                                        <tr>
                                                            <td><input type="checkbox" name="payroll_dtr_draft[{{ $item->id }}]"></td>
                                                            <td>{{ $item->id_number }}</td>
                                                            <td>{{ $item->status == 'No Match' ? '' : $item->User->Profile->namelfm() }}</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>

                                            <div>
                                                <label class="radio-inline" for="actionRemove">
                                                    <input type="radio" id="actionRemove" name="action" value="Remove" checked> Remove
                                                </label>
                                                <label class="radio-inline" for="actionOverwrite">
                                                    <input type="radio" id="actionOverwrite" name="action" value="Overwrite"> Overwrite
                                                </label>
                                            </div>
    
    
                                            <div class="text-right">
                                                <a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/upload_dtr") }}" class="btn btn-danger"><i class="fa fa-close"></i> CANCEL</a>
                                                <button class="btn btn-success push-10-l"><i class="fa fa-save"></i> CONFIRM</button>
                                            </div>
                                        </form>
                                    </div>

                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#selectAll").click(function(e) {

                $('#existingDtr tbody input:checkbox').not(this).prop('checked', this.checked);

//                if($(this).is(':checked') == true) {
//                    $(this).closest('table').find('tbody').find('input[type=checkbox]').attr('checked', true);
//                } else {
//                    $(this).closest('table').find('tbody').find('input[type=checkbox]').removeAttr('checked');
//                }
            });
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
        <a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}") }}"><i class="fa fa-arrow-left"></i> Back to Payroll Members</a>

        <br>
        <ol class="breadcrumb push-10-t">
            <li>Payroll</li>
            <li><a class="link-effect" href="{{ route('payroll_wizard_index')}}">Wizard</a></li>
            <li><a class="link-effect" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}") }}">Payroll Members</a></li>
            <li><a class="link-effect" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/process") }}">Process</a></li>
        </ol>


    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">



                                <br />

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Group</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->PayrollGroup->group_name }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Period</label>
                                            <input type="text" class="form-control" name="payroll_period" value="{{ $payrollCycle->payroll_period }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Code</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->payroll_code }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Date</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ date('m/d/Y', strtotime($payrollCycle->payroll_date)) }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <hr />
                                </div>

                                <div class="col-md-12">
                                    <form action="" method="POST">
                                        {{ csrf_field() }}

                                        <table id="payroll_process" class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th width="100">Hold Status</th>
                                                <th>Employee ID Number</th>
                                                <th>Employee Name</th>
                                                <th>Net Pay</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($dtr as $d)
                                            <tr>
                                                <td>
                                                    <select name="dtr[{{$d->id}}]" class="form-control">
                                                        <option value="N">No</option>
                                                        <option value="Y">Yes</option>
                                                    </select>
                                                </td>
                                                <td>{{ $d->id_number }}</td>
                                                <td>{{ $d->User->Profile->namelfm() }}</td>
                                                <td>
                                                    <?php
                                                    $user = $d->User;
                                                    $payrollCode = explode('-', $payrollCycle->payroll_code);
                                                    $user_id = $user->id;
                                                    //                                $payroll = \App\PayrollSummary::where(['payroll_cycle_id' => $d->payroll_cycle_id, 'user_id' => $d->User->id])->first();

                                                    if($d->User->Profile->Compensation->rate_type == 'Monthly') {
                                                        $payrollComputation = $payroll = new \App\ComputePayroll($d, ['daily_rate' => $d->User->Profile->Compensation->monthly_rate / 26, 'monthly_rate' => $d->User->Profile->Compensation->monthly_rate / 2, 'rate_type' => 'Monthly']);
                                                    } else {
                                                        if($d->User->Profile->Compensation->daily_category == 'Location') {
                                                            $payrollComputation = $payroll = new \App\ComputePayroll($d, $d->User->Profile->Compensation->DailyRate->rate);
                                                        } else {
                                                            $payrollComputation = $payroll = new \App\ComputePayroll($d, $d->User->Profile->Compensation->daily_rate);
                                                        }
                                                    }



                                                    $ytdPayrollCycles = \App\PayrollCycles::where(['payroll_group_id' => $user->Profile->Compensation->PayrollGroup->id, 'effective_year' => $payrollCycle->effective_year, 'status' => 'Processed'])->pluck('id');
                                                    $ytdPayrollSummary = \App\PayrollSummary::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $d->User->id)->get();

                                                    //        dd(PayrollCycles::where('status', 'Processed')->whereYear('payroll_date', date('Y', strtotime( $data['payrollCycle']->payroll_date)))->pluck('id'));

                                                    $accumulatedHours = 0;
                                                    $totalLeaves = 0;
                                                    $unpaidLeave = 0;
                                                    if(!empty($ytdPayrollSummary)) {
                                                        $ytdDtr = \App\PayrollDtr::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $user->id)->get();
                                                        $accumulatedHours = $ytdDtr->sum('vl') + $ytdDtr->sum('sl') + $ytdDtr->sum('el') + $ytdDtr->sum('spl') + $ytdDtr->sum('ml') + $ytdDtr->sum('pl') + $ytdDtr->sum('pto') + $ytdDtr->sum('bl') + $ytdDtr->sum('cl');
                                                        $leaves = $payroll->vl + $payroll->sl + $payroll->el + $payroll->spl + $payroll->ml + $payroll->pl + $payroll->pto + $payroll->bl + $payroll->cl;

                                                        if($leaves > 0) {
                                                            $bal = (5 * 8) - $accumulatedHours;
                                                            if($bal > 0) {

                                                                $bal -= $leaves;
                                                                if($bal < 0) {
                                                                    $unpaidLeave = abs($bal)* $payroll->hourlyRate();
                                                                }

                                                            } else {
                                                                $unpaidLeave = $leaves* $payroll->hourlyRate();
                                                            }
                                                        }
                                                    }


                                                    $ytdPayrollCycles = \App\PayrollCycles::where(['payroll_group_id' => $user->Profile->Compensation->PayrollGroup->id, 'effective_year' => $payrollCycle->effective_year])->pluck('id');
                                                    $ytdPayrollSummary = \App\PayrollSummary::whereIn('payroll_cycle_id', $ytdPayrollCycles)->get();


                                                    $loans = \App\LoanPaymentSchedules::where(['pay_period' => $payrollCode[1], 'user_id' => $user_id]);

                                                    $sssLoan = 0;
                                                    $hdmfLoan = 0;
                                                    $coopLoan = 0;
                                                    foreach($loans->get() as $lps) {
                                                        if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'SSS') {
                                                            $sssLoan += $lps->Loan->amortization;
                                                        }

                                                        if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'HDMF') {
                                                            $hdmfLoan += $lps->Loan->amortization;
                                                        }

                                                        if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && in_array($lps->Loan->loan_type_id, ['1', '2', '3', '4'])) {
                                                            $coopLoan += $lps->Loan->amortization;
                                                        }
                                                    }

                                                    if($payrollCycle->cycle == 'B') {
                                                        $payrollCycleA = \App\PayrollCycles::where([
                                                            'payroll_group_id' => $payrollCycle->payroll_group_id,
                                                            'effective_year' => $payrollCycle->effective_year,
                                                            'effective_month' => $payrollCycle->effective_month,
                                                            'cycle' => 'A',
                                                            'status' => 'Processed'
                                                        ])->first();

                                                        $payrollSummaryA = \App\PayrollSummary::where('payroll_cycle_id', $payrollCycleA->id)->where('user_id', $user_id)->first();
                                                        $basicPayA = !empty($payrollSummaryA->basic_pay) ? $payrollSummaryA->basic_pay : 0;
                                                    }

                                                    $basicPayB = 0;
                                                    if($payrollCycle->cycle == 'A') {
                                                        $effectiveDate = \Carbon\Carbon::createFromFormat('Y-m-d', "{$payrollCycle->effective_year}-{$payrollCycle->effective_month}-01")->subMonth(1);
                                                        $payrollCycleB = \App\PayrollCycles::where([
                                                            'payroll_group_id' => $payrollCycle->payroll_group_id,
                                                            'effective_year' => $effectiveDate->format('Y'),
                                                            'effective_month' => $effectiveDate->format('m'),
                                                            'cycle' => 'B',
                                                            'status' => 'Processed'
                                                        ])->first();

                                                        if(empty($payrollCycleB)) {
                                                            $basicPayB = 0;
                                                        } else {
                                                            $payrollSummaryB = \App\PayrollSummary::where('payroll_cycle_id', $payrollCycleB->id)->where('user_id', $user_id)->first();
                                                            $basicPayB = !empty($payrollSummaryB->basic_pay) ? $payrollSummaryB->basic_pay : 0;
                                                        }


                                                    }

                                                    $philhealth_ee = 0;
                                                    $philhealth_er = 0 ;
                                                    if($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_philhealth == 'Y') {
                                                        $philHealthContribution = \App\PhilHealthContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first();
                                                        if($philHealthContribution->id == 3) {
                                                            $monthlyPremium = ($payroll->basicPay() + $basicPayB) * ($philHealthContribution->premium_rate / 100);
                                                        } elseif($philHealthContribution->id == 1) {
                                                            $monthlyPremium = 275;
                                                        } elseif($philHealthContribution->id == 2) {
                                                            $monthlyPremium = 1100;
                                                        }

                                                        $philhealth_ee = $philhealth_er = $monthlyPremium / 2;
                                                    }

                                                    $thirteenthMonth = 0;
                                                    if($payrollCycle->calculate_13th_month == 'Y') {
                                                        $lastDecember = \App\PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)) - 1)->whereMonth('payroll_date', 12)->get();
//                                                        $toNovemberThisYear = \App\PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)))->whereMonth('payroll_date', 11)->get();
                                                        $toNovemberThisYear = \App\PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereDate('payroll_date', '>=', date('Y-01-01', strtotime($payrollCycle->payroll_date)))->whereDate('payroll_date', '<=', date('Y-11-30', strtotime($payrollCycle->payroll_date)))->get();

                                                        foreach($lastDecember as $pc) {
                                                            $pastSummary = \App\PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $user_id])->first();
                                                            $thirteenthMonth += !empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0;
                                                        }

                                                        foreach($toNovemberThisYear as $pc) {
                                                            $pastSummary = \App\PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $user_id])->first();
                                                            $thirteenthMonth += !empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0;
                                                        }
                                                    }

                                                    $membershipFee = 0;
                                                    if(\App\OtherPayments::where('user_id', $d->user_id)->where('transaction_type', 'MF')->sum('total_amount_paid') < \App\Keyval::where('key', 'membership_fee')->first()->value) {
                                                        $membershipFee = \App\Keyval::where('key', 'membership_fee')->first()->value / 2;
                                                    }

                                                    $notarialFee = 0;
                                                    if(\App\OtherPayments::where('user_id', $d->user_id)->where('transaction_type', 'NF')->sum('total_amount_paid') < \App\Keyval::where('key', 'notarial_fee')->first()->value) {
                                                        $notarialFee = \App\Keyval::where('key', 'notarial_fee')->first()->value / 2;
                                                    }

                                                    if(empty(\App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ec)) {
                                                        session()->flash('popError', "");

                                                        ?>

                                                        <script>
                                                            alert('{{ $d->user_id }} {{ $d->User->Profile->id_number }} Basic pay for this payroll is {{ number_format($payroll->dailyRate, 0) }}!');
                                                            location.href = '{{ url()->previous() }}'
                                                        </script>
                                                        <?php
                                                        die();
//                                                        dd($d->user_id);
                                                    }

                                                    $payroll = [
                                                        'payroll_cycle_id' => $d->payroll_cycle_id,
                                                        'user_id'=> $d->user_id,
                                                        'basic_pay' => $payroll->basicPay(),
                                                        'ecola' => ($d->Profile->Compensation->daily_category == 'Location') ? ($payroll->rg() / 8) * $d->Profile->Compensation->DailyRate->ecola : 0.00,
                                                        'overtime_pay' => $payroll->OTPay(),
                                                        'holiday_pay' => $payroll->HDPay(),
                                                        'night_differential' => $payroll->NDPay(),
                                                        'holiday_ecola' => $d->Profile->Compensation->daily_category == 'Location' ? ($payroll->leg / 8) * $d->Profile->Compensation->DailyRate->ecola : 0.00,
                                                        'adjustment_wage' => $d->nwage,
                                                        'adjustment_income' => $d->adji,
                                                        'adjustment_deduction' => $d->adjd,
                                                        'de_minimis' => $d->Profile->Compensation->de_minimis,
                                                        'thirteenth_month_bonus' => $thirteenthMonth,
                                                        'thirteenth_month_pro_rated' => $payroll->basicPay() / 12,
                                                        'fixed_allowance' => $d->Profile->Compensation->fixed_allowance,
                                                        'leave_encashment' => 0, // TODO
                                                        'incentives' => $d->incentive,
                                                        'coop_savings' => 0, // Omitted
                                                        'sss_ec' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ec : 0,
                                                        'sss_er' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->er : 0,
                                                        'hdmf_er' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_hdmf == 'Y') ? \App\HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employer_share : 0,
                                                        'philhealth_er' => $philhealth_er,
                                                        'sss_ee' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ee : 0,
                                                        'hdmf_ee' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_hdmf == 'Y') ? \App\HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employee_share + $user->Profile->hdmf_additional : 0,
                                                        'philhealth_ee' => $philhealth_ee,
                                                        'tardiness' => ($payroll->hourlyRate() / 60) * ($payroll->lt + $payroll->ut),
                                                        'absences' => ($payroll->hourlyRate() / 60) * $payroll->abs,
                                                        'sss_loan' => $sssLoan,
                                                        'hdmf_loan' => $hdmfLoan,
                                                        'client_charges' => $d->client_charges,
                                                        'membership_fee' => $membershipFee,
                                                        'notarial_fee' => $notarialFee,
                                                        'coop_loan' => $coopLoan,
                                                        'coop_share' => $d->Profile->coop_share,
                                                        'unpaid_leave' => $unpaidLeave
                                                    ];



                                                    if($unpaidLeave > 0) {
                                                        $incomeTx = $payroll['basic_pay'] + $payrollComputation->rd() + $payrollComputation->spe() + $payrollComputation->sperd() + $payrollComputation->leg() + $payrollComputation->legrd() + $payroll['overtime_pay'] + $payroll['night_differential'] + $payroll['ecola'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
                                                    } else {
                                                        $incomeTx = $payroll['basic_pay'] + $payrollComputation->rd() + $payrollComputation->spe() + $payrollComputation->sperd() + $payrollComputation->leg() + $payrollComputation->legrd() + $payroll['overtime_pay'] + $payroll['night_differential'] + $payrollComputation->vl() + $payrollComputation->sl() + $payrollComputation->el() + $payrollComputation->spl() + $payrollComputation->ml() + $payrollComputation->pl() + $payrollComputation->pto() + $payrollComputation->bl() + $payrollComputation->cl() + $payroll['ecola'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
                                                    }

                                                    $incomeNtx = $payroll['de_minimis'] + $payroll['thirteenth_month_bonus'] + $payroll['fixed_allowance'] + $payroll['leave_encashment'] + $payroll['incentives'] + $payroll['adjustment_income'];
                                                    $deductionTx = $payroll['sss_ee'] + $payroll['hdmf_ee'] + $payroll['philhealth_ee'] + $payroll['tardiness'] + $payroll['absences'];
                                                    $deduction = $payroll['sss_loan'] + $payroll['hdmf_loan'] + $payroll['client_charges'] + $payroll['membership_fee'] + $payroll['notarial_fee'] + $payroll['coop_loan'] + $payroll['coop_share'] + $payroll['adjustment_deduction'];

                                                    $payroll['tax'] = 0;
                                                    if(\App\Compensations::where('user_id', $d->user_id)->first()->deduct_withholding == 'Y') {
                                                        $taxTable = new \App\TaxTable($d->User->Profile->Compensation->TaxType->title, $incomeTx);
                                                        $payroll['tax'] = $taxTable->taxDue();
                                                    }

                                                    $payroll['gross_amount'] = $incomeTx + $incomeNtx;
                                                    $payroll['deductions'] = $deductionTx + $deduction + $unpaidLeave;
                                                    $payroll['net_pay'] = ($payroll['gross_amount'] - $payroll['deductions']) - $payroll['tax'];



                                                    ?>
                                                    {{ number_format($payroll['net_pay'], 2) }}

                                                </td>
                                                <td><a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/payslip_preview/{$d->User->id}") }}" class="btn btn-sm btn-primary">PAYSLIP PREVIEW <i class="fa fa-chevron-right"></i></a></td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                        <br>
                                        <br>

                                        <div class="text-right">
                                            <button id="processPayrollBtn" type="button" class="btn btn-success">
                                                <i class="fa fa-save" aria-hidden="true"></i> Processed
                                            </button>
                                            <a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}") }}" class="btn btn-danger">
                                                <i class="fa fa-times" aria-hidden="true"></i> Cancel
                                            </a>

                                            <div id="processPayrollModal" tabindex="-1" role="dialog" aria-labelledby="processPayrollModal" class="modal fade text-left" aria-hidden="true" style="display: none;">
                                                <div role="document" class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 id="exampleModalLabel" class="modal-title">Process Payroll</h4>
                                                            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Are you sure you want to process this payroll?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button id="" type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>

                                                            <button class="btn btn-primary" style="color: #FFF;"><span class="fa fa-save"></span> Confirm</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $("#payroll_process").DataTable({
                "sorting": [],
                "pageLength": 100
            });

            $(document).on('click', '#processPayrollBtn', function(e) {
                e.preventDefault();

                $("#processPayrollModal").modal('show');
            });
        })
    </script>
@endsection

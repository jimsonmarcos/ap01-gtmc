@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('payroll_wizard_index')}}">Wizard</a></li>
        <li><a class="link-effect" href="{{ route('payroll_members')}}">Payroll Members</a></li>
        <li><a class="link-effect" href="{{ route('payroll_upload_dtr')}}">Upload Timesheet</a></li>
    </ol>

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <div class="form-material floating open">
                                                <select class="form-control" id="payroll_group" name="payroll_group" size="1">
                                                    {{--Data HERE--}}
                                                    {{--@foreach() --}}
                                                    {{--<option></option>--}}
                                                    {{--@endforeach--}}
                                                    <option>January</option>
                                                </select>
                                                <label for="payroll_group">Payroll Group</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <div class="form-material floating">
                                                <input class="form-control" type="text" id="payroll_period" name="payroll_period">
                                                <label for="payroll_period">Payroll Period</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <div class="form-material floating">
                                                <input class="form-control" type="text" id="payroll_code" name="payroll_code">
                                                <label for="payroll_code">Payroll Code</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label">Payroll Date</label>
                                        <div id="joinDate" class="input-group btn-mmddyyyy">
                                            <input type="text" class="form-control" name="payroll_date" placeholder="mm/dd/yyyy" required>
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="form-control-label">*Upload .xls, .xlsx, .csv</label>
                                    <div class="form-material floating">
                                        <input type="file" class="form-control" name="attach_files" />
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/payroll/wizard/payroll_members/upload_dtr/Success') }}">
                                                <span class="badge badge-default">23</span> All
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/payroll/wizard/payroll_members/upload_dtr/Draft') }}">
                                                <span class="badge badge-info">1</span> Draft</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/payroll/wizard/payroll_members/upload_dtr/Unpaid') }}">
                                                <span class="badge badge-danger">0</span> Unpaid
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('admin/payroll/wizard/payroll_members/upload_dtr/Paid') }}">
                                                <span class="badge badge-success">55</span> Paid
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Employee ID Number</th>
                                            <th>Employee Name</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        for ($x=0; $x<5; $x++){ ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <a href="#">Existing</a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

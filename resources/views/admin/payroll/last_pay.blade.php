@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('last_pay')}}">Last Pay</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12 text-right">
                                    <button class="btn btn-info push-5-r push-10" type="button"><i class="fa fa-print"></i> Print</button>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-xs-12" for="company_code">Company Code</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="company_code" name="company_code" size="1">
                                                <option value="1">Code</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-xs-12" for="payroll_group">Payroll Group</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="payroll_group" name="payroll_group" size="1">
                                                <option value="1">Code</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 text-center">
                                    <br />
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Payroll Code</th>
                                                <th>Company Code</th>
                                                <th>Employee Name</th>
                                                <th>Payroll Period</th>
                                                <th>Payroll Date</th>
                                                <th>Payroll Status</th>
                                                <th>Select</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1234</td>
                                                <td>1234</td>
                                                <td>
                                                    <a href="#">Last Name, First Name</a>
                                                </td>
                                                <td>1234</td>
                                                <td>1234</td>
                                                <td>1234</td>
                                                <td>
                                                    <div class="col-xs-12">
                                                        <label class="css-input css-checkbox css-checkbox-primary">
                                                            <input type="checkbox" checked=""><span></span>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

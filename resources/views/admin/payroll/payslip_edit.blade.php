@extends('layouts.app')


@section('content')
    <a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/payslip_preview/{$user->id}") }}"><i class="fa fa-arrow-left"></i> Back to Payroll Preview</a>
    <br>

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" class="form" method="POST">
                                {{ csrf_field() }}
                            <div class="row">
                                <br />

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Employee ID Number</label>
                                            <input type="text" class="form-control" name=""  value="{{ $user->Profile->id_number }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Employee Name</label>
                                            <input type="text" class="form-control" name=""  value="{{ $user->name }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Cost Center</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->PayrollGroup->group_name }} ({{ $payrollCycle->PayrollGroup->Company->company_code }})" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Period</label>
                                            <input type="text" class="form-control" name="payroll_period" value="{{ $payrollCycle->payroll_period }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Code</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->payroll_code }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Date</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ date('m/d/Y', strtotime($payrollCycle->payroll_date)) }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Monthly Rate</label>
                                            <input type="text" class="form-control" name=""  value="@if($user->Profile->Compensation->rate_type == 'Monthly') {{ number_format($payrollComputation->monthlyRate, 2) }} @else N/A @endif" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Daily Rate</label>
                                            <input type="text" class="form-control" name=""  value="{{ number_format($payrollComputation->dailyRate, 2) }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <hr />
                                </div>


                                    {{ csrf_field() }}
                                    <div class="col-md-3">
                                        {{--                                    @if($payroll->sss_er > 0 || $payroll->sss_ec > 0 || $payroll->hdmf_er > 0 || $payroll->philhealth_er > 0)--}}
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="text-center table-header" colspan="2">Employer Paid Benefits</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {{--@if($payroll->sss_er > 0)--}}
                                            <tr>
                                                <td width="50%">SSS ER</td>
                                                <td><input type="text" class="form-control" name="sss_er" value="{{ number_format($payroll->sss_er, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                            </tr>
                                            {{--@endif--}}

                                            {{--                                            @if($payroll->sss_ec > 0)--}}
                                            <tr>
                                                <td>SSS EC</td>
                                                <td><input type="text" class="form-control" name="sss_ec" value="{{ number_format($payroll->sss_ec, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                            </tr>
                                            {{--@endif--}}

                                            {{--                                            @if($payroll->hdmf_er > 0)--}}
                                            <tr>
                                                <td>HDMF ER</td>
                                                <td><input type="text" class="form-control" name="hdmf_er" value="{{ number_format($payroll->hdmf_er, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                            </tr>
                                            {{--@endif--}}

                                            {{--                                            @if($payroll->philhealth_er > 0)--}}
                                            <tr>
                                                <td>PhilHealth ER</td>
                                                <td><input type="text" class="form-control" name="philhealth_er" value="{{ number_format($payroll->philhealth_er, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                            </tr>
                                            {{--@endif--}}
                                            </tbody>
                                        </table>
                                        {{--@endif--}}


                                    </div>

                                    <div class="col-md-5">
                                        <table class="table table-bordered" id="earnings">
                                            <thead>
                                            <tr>
                                                <th width="50%" class="table-header">Hours and Earnings</th>
                                                <th class="text-center table-header">Hrs</th>
                                                <th width="30%" class="text-center table-header">Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
{{--                                            @if($payroll->basic_pay > 0)--}}
                                                <tr>
                                                    <td>BASIC</td>
                                                    <td>{{ $payrollComputation->rg() }}</td>
                                                    <td><input type="text" class="form-control" name="basic_pay" value="{{ number_format($payroll->basic_pay, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->rd() > 0)--}}
                                                <tr>
                                                    <td>RD</td>
                                                    <td>{{ $payrollComputation->rd }}</td>
                                                    <td>{{ number_format($payrollComputation->rd(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->rd() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->spe() > 0)--}}
                                                <tr>
                                                    <td>SPE</td>
                                                    <td>{{ $payrollComputation->sperd }}</td>
                                                    <td>{{ number_format($payrollComputation->spe(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->spe() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->sperd() > 0)--}}
                                                <tr>
                                                    <td>SPERD</td>
                                                    <td>{{ $payrollComputation->sperd }}</td>
                                                    <td>{{ number_format($payrollComputation->sperd(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->sperd() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->leg() > 0)--}}
                                                <tr>
                                                    <td>LEG</td>
                                                    <td>{{ $payrollComputation->leg }}</td>
                                                    <td>{{ number_format($payrollComputation->leg(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->leg() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->legrd() > 0)--}}
                                                <tr>
                                                    <td>LEGRD</td>
                                                    <td>{{ $payrollComputation->legrd }}</td>
                                                    <td>{{ number_format($payrollComputation->legrd(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->legrd() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->OTPay() > 0)--}}
                                                <tr>
                                                    <td>OT</td>
                                                    <td>{{ $payrollComputation->regot }}</td>
                                                    <td>{{ number_format($payrollComputation->OTPay(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->OTPay() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->NDPay() > 0)--}}
                                                <tr>
                                                    <td>ND</td>
                                                    <td>{{ $payrollComputation->nd }}</td>
                                                    <td>{{ number_format($payrollComputation->NDPay(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->NDPay() }}">
                                                </tr>
                                            {{--@endif--}}
{{--                                            @if($payrollComputation->vl() > 0)--}}
                                                <tr>
                                                    <td>VL</td>
                                                    <td>{{ $payrollComputation->vl }}</td>
                                                    <td>{{ number_format($payrollComputation->vl(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->vl() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->sl() > 0)--}}
                                                <tr>
                                                    <td>SL</td>
                                                    <td>{{ $payrollComputation->sl }}</td>
                                                    <td>{{ number_format($payrollComputation->sl(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->sl() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->el() > 0)--}}
                                                <tr>
                                                    <td>EL</td>
                                                    <td>{{ $payrollComputation->el }}</td>
                                                    <td>{{ number_format($payrollComputation->el(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->el() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->spl() > 0)--}}
                                                <tr>
                                                    <td>SPL</td>
                                                    <td>{{ $payrollComputation->spl }}</td>
                                                    <td>{{ number_format($payrollComputation->spl(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->spl() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->ml() > 0)--}}
                                                <tr>
                                                    <td>ML</td>
                                                    <td>{{ $payrollComputation->ml }}</td>
                                                    <td>{{ number_format($payrollComputation->ml(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->ml() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->pl() > 0)--}}
                                                <tr>
                                                    <td>PL</td>
                                                    <td>{{ $payrollComputation->pl }}</td>
                                                    <td>{{ number_format($payrollComputation->pl(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->pl() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->pto() > 0)--}}
                                                <tr>
                                                    <td>PTO</td>
                                                    <td>{{ $payrollComputation->pto }}</td>
                                                    <td>{{ number_format($payrollComputation->pto(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->pto() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->bl() > 0)--}}
                                                <tr>
                                                    <td>BL</td>
                                                    <td>{{ $payrollComputation->bl }}</td>
                                                    <td>{{ number_format($payrollComputation->bl(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->bl() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payrollComputation->cl() > 0)--}}
                                                <tr>
                                                    <td>CL</td>
                                                    <td>{{ $payrollComputation->cl }}</td>
                                                    <td>{{ number_format($payrollComputation->cl(), 2) }}</td>
                                                    <input type="hidden" value="{{ $payrollComputation->cl() }}">
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payroll->ecola > 0)--}}
                                                <tr>
                                                    <td>ECOLA</td>
                                                    <td></td>
                                                    <td><input type="text" class="form-control" name="ecola" value="{{ number_format($payroll->ecola, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payroll->holiday_ecola > 0)--}}
                                                <tr>
                                                    <td>HECOLA</td>
                                                    <td></td>
                                                    <td><input type="text" class="form-control" name="holiday_ecola" value="{{ number_format($payroll->holiday_ecola, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payroll->adjustment_wage > 0)--}}
                                                <tr>
                                                    <td>NWAGE</td>
                                                    <td></td>
                                                    <td><input type="text" class="form-control" name="adjustment_wage" value="{{ number_format($payroll->adjustment_wage, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payroll->de_minimis > 0)--}}
                                                <tr>
                                                    <td>DE MINIMIS</td>
                                                    <td></td>
                                                    <td><input type="text" class="form-control" name="de_minimis" value="{{ number_format($payroll->de_minimis, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payroll->thirteenth_month_bonus > 0)--}}
                                                <tr>
                                                    <td>13TH MONTH</td>
                                                    <td></td>
                                                    <td><input type="text" class="form-control" name="thirteenth_month_bonus" value="{{ number_format($payroll->thirteenth_month_bonus, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payroll->fixed_allowance > 0)--}}
                                                <tr>
                                                    <td>FIXED</td>
                                                    <td></td>
                                                    <td><input type="text" class="form-control" name="fixed_allowance" value="{{ number_format($payroll->fixed_allowance, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payroll->leave_encashment > 0)--}}
                                                <tr>
                                                    <td>LEAVE</td>
                                                    <td></td>
                                                    <td><input type="text" class="form-control" name="leave_encashment" value="{{ number_format($payroll->leave_encashment, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payroll->incentives > 0)--}}
                                                <tr>
                                                    <td>INCENTIVES</td>
                                                    <td></td>
                                                    <td><input type="text" class="form-control" name="incentives" value="{{ number_format($payroll->incentives, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                </tr>
                                            {{--@endif--}}
                                            {{--@if($payroll->adjustment_income > 0)--}}
                                                <tr>
                                                    <td>Other Income</td>
                                                    <td></td>
                                                    <td><input type="text" class="form-control" name="adjustment_income" value="{{ number_format($payroll->adjustment_income, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                </tr>
                                            {{--@endif--}}

                                            @foreach($dtr->Others()->where('type', 'Income') as $x => $income)
                                                <tr>
                                                    <td><button type="button" class="btn btn-xs btn-danger removeOtherIncome"><i class="fa fa-remove"></i></button>
                                                        <select class="form-control" id="" name="other_income[{{ $x }}][title]" size="1" required="" style="display: inline-block;width: 86%;">
                                                            @if(!\App\IncomeDeductionItems::where('type', 'Income')->where('item', $income->title)->exists()) <option value="{{ $income->title }}">{{ $income->title }}</option> @endif
                                                            @foreach(\App\IncomeDeductionItems::where('type', 'Income')->orderBy('item')->get() as $item)
                                                                <option value="{{ $item->item }}" @if($income->title == $item->item) selected @endif>{{ $item->item }}</option>
                                                            @endforeach
                                                        </select>
                                                    <td></td>
                                                    <td><input type="text" class="form-control" name="other_income[{{ $x }}][amount]" value="{{ number_format($income->amount, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" required /></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>


                                        <button id="addMoreIncome" type="button" class="btn btn-sm btn-primary" data-x="{{ $dtr->Others()->where('type', 'Income')->count() + 1 }}"><i class="fa fa-plus"></i> Add More Income</button>
                                    </div>

                                    @if($payroll->absences > 0 ||
                                        $payroll->tardiness > 0 ||
                                        $payroll->sss_ee > 0 ||
                                        $payroll->hdmf_ee > 0 ||
                                        $payroll->philhealth_ee > 0 ||
                                        $payroll->sss_loan > 0 ||
                                        $payroll->hdmf_loan > 0 ||
                                        $payroll->client_charges > 0 ||
                                        $payroll->membership_fee > 0 ||
                                        $payroll->notarial_fee > 0 ||
                                        $payroll->coop_loan > 0 ||
                                        $payroll->coop_share > 0 ||
                                        $payroll->unpaid_leave > 0)
                                        <div class="col-md-4">
                                            <table id="" class="table table-bordered deductions">
                                                <thead>
                                                <tr>
                                                    <th width="50%" class=" table-header">Deductions</th>
                                                    <th class="text-center table-header">Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
{{--                                                @if($payroll->absences > 0)--}}
                                                    <tr>
                                                        <td>Absences</td>
                                                        <td><input type="text" class="form-control" name="absences" value="{{ number_format($payroll->absences, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}
                                                {{--@if($payroll->tardiness > 0)--}}
                                                    <tr>
                                                        <td>Tardiness</td>
                                                        <td><input type="text" class="form-control" name="tardiness" value="{{ number_format($payroll->tardiness, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}
                                                {{--@if($payroll->sss_ee > 0)--}}
                                                    <tr>
                                                        <td>SSS EE</td>
                                                        <td><input type="text" class="form-control" name="sss_ee" value="{{ number_format($payroll->sss_ee, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}
                                                {{--@if($payroll->hdmf_ee > 0)--}}
                                                    <tr>
                                                        <td>HDMF EE</td>
                                                        <td><input type="text" class="form-control" name="hdmf_ee" value="{{ number_format($payroll->hdmf_ee, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}
                                                {{--@if($payroll->philhealth_ee > 0)--}}
                                                    <tr>
                                                        <td>PhilHealth EE</td>
                                                        <td><input type="text" class="form-control" name="philhealth_ee" value="{{ number_format($payroll->philhealth_ee, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}
                                                {{--@if($payroll->sss_loan > 0)--}}
                                                    <tr>
                                                        <td>SSS Loan</td>
                                                        <td><input type="text" class="form-control" name="sss_loan" value="{{ number_format($payroll->sss_loan, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}
                                                {{--@if($payroll->hdmf_loan > 0)--}}
                                                    <tr>
                                                        <td>HDMF Loan</td>
                                                        <td><input type="text" class="form-control" name="hdmf_loan" value="{{ number_format($payroll->hdmf_loan, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}
                                                {{--@if($payroll->client_charges > 0)--}}
                                                    <tr>
                                                        <td>Client Charges</td>
                                                        <td><input type="text" class="form-control" name="client_charges" value="{{ number_format($payroll->client_charges, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}
                                                {{--@if($payroll->membership_fee > 0)--}}
                                                    <tr>
                                                        <td>Membership Fee</td>
                                                        <td><input type="text" class="form-control" name="membership_fee" value="{{ number_format($payroll->membership_fee, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}
                                                {{--@if($payroll->notarial_fee > 0)--}}
                                                    <tr>
                                                        <td>Notarial Fee</td>
                                                        <td><input type="text" class="form-control" name="notarial_fee" value="{{ number_format($payroll->notarial_fee, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}
                                                {{--@if($payroll->coop_loan > 0)--}}
                                                    <tr>
                                                        <td>Coop Loan</td>
                                                        <td><input type="text" class="form-control" name="coop_loan" value="{{ number_format($payroll->coop_loan, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}
                                                {{--@if($payroll->coop_share > 0)--}}
                                                    <tr>
                                                        <td>Coop Share</td>
                                                        <td><input type="text" class="form-control" name="coop_share" value="{{ number_format($payroll->coop_share, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}

{{--                                                @if($payroll->unpaid_leave > 0)--}}
                                                    <tr>
                                                        <td>Unpaid Leave</td>
                                                        <td><input type="text" class="form-control" name="unpaid_leave" value="{{ number_format($payroll->unpaid_leave, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}

{{--                                                @if($payroll->adjustment_deduction > 0)--}}
                                                    <tr>
                                                        <td>Other Deduction</td>
                                                        <td><input type="text" class="form-control" name="adjustment_deduction" value="{{ number_format($payroll->adjustment_deduction, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></td>
                                                    </tr>
                                                {{--@endif--}}

                                                    @foreach($dtr->Others()->where('type', 'Deduction') as $x => $income)
                                                        <tr>
                                                            <td><button type="button" class="btn btn-xs btn-danger removeOtherDeduction"><i class="fa fa-remove"></i></button>
                                                                <select class="form-control" id="" name="other_deduction[{{ $x }}][title]" size="1" required="" style="display: inline-block;width: 82%;">
                                                                    @if(!\App\IncomeDeductionItems::where('type', 'Deduction')->where('item', $income->title)->exists()) <option value="{{ $income->title }}">{{ $income->title }}</option> @endif
                                                                    @foreach(\App\IncomeDeductionItems::where('type', 'Deduction')->orderBy('item')->get() as $item)
                                                                        <option value="{{ $item->item }}" @if($income->title == $item->item) selected @endif>{{ $item->item }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td><input type="text" class="form-control" name="other_deduction[{{ $x }}][amount]" value="{{ number_format($income->amount, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" required /></td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>


                                            <button id="addMoreDeduction" type="button" class="btn btn-sm btn-primary" data-x="{{ $dtr->Others()->where('type', 'Deduction')->count() + 1 }}"><i class="fa fa-plus"></i> Add More Deduction</button>

                                            <br>
                                            <br>
                                            <br>
                                            @endif

                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th width="50%" class="table-header">CURRENT NET PAY</th>
                                                    <th class="text-right table-header">Amount</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <th>GROSS AMOUNT</th>
                                                        <th class="text-right" id="grossAmount">
                                                            {{ number_format($payroll->gross_amount, 2) }}
                                                        </th>
                                                        <input type="hidden" name="gross_amount" id="grossAmountTxt" value="{{ number_format($payroll->gross_amount, 2) }}">
                                                    </tr>
                                                    <tr>
                                                        <th>DEDUCTIONS</th>
                                                        <th class="text-right" id="deductions">
                                                            {{ number_format($payroll->deductions, 2) }}
                                                        </th>
                                                        <input type="hidden" name="deductions" id="deductionsTxt" value="{{ number_format($payroll->deductions, 2) }}">
                                                    </tr>
                                                    <tr>
                                                        <th>TAX</th>
                                                        <th><input type="text" id="tax" class="form-control" name="tax" value="{{ number_format($payroll->tax, 2) }}" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');"></th>
                                                    </tr>
                                                    <tr>
                                                        <th class="table-header">NET PAY</th>
                                                        <th class="table-header text-right" id="netPay">
                                                            {{ number_format($payroll->net_pay, 2) }}
                                                        </th>
                                                        <input type="hidden" name="net_pay" id="netPayTxt" value="{{ number_format($payroll->net_pay, 2) }}">
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>


                            </div>

                            <hr>

                            <div class="text-right">
                                <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> Save Changes</button>
                            </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <table class="hidden">
        <tr class="otherIncomeTemplate">
            <td><button type="button" class="btn btn-xs btn-danger removeOtherIncome"><i class="fa fa-remove"></i></button>
                <select class="form-control" id="" name="other_income[x][title]" size="1" required="" style="display: inline-block;width: 82%;">
                    @foreach(\App\IncomeDeductionItems::where('type', 'Income')->orderBy('item')->get() as $item)
                        <option value="{{ $item->item }}">{{ $item->item }}</option>
                    @endforeach
                </select>
            <td></td>
            <td><input type="text" class="form-control" name="other_income[x][amount]" value="" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" required /></td>
        </tr>

        <tr class="otherDeductionTemplate">
            <td><button type="button" class="btn btn-xs btn-danger removeOtherDeduction"><i class="fa fa-remove"></i></button>
                <select class="form-control" id="" name="other_deduction[x][title]" size="1" required="" style="display: inline-block;width: 82%;">
                    @foreach(\App\IncomeDeductionItems::where('type', 'Deduction')->orderBy('item')->get() as $item)
                        <option value="{{ $item->item }}">{{ $item->item }}</option>
                    @endforeach
                </select>
            </td>
            <td><input type="text" class="form-control" name="other_deduction[x][amount]" value="" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" required /></td>
        </tr>
    </table>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(document).on('click', '.removeOtherDeduction, .removeOtherIncome', function() {
                if($(this).closest('tr').hasClass('otherIncomeTemplate') == false && $(this).closest('tr').hasClass('otherDeductionTemplate') == false) {
                    $(this).closest('tr').remove();
                }

                updateTotals();
            });

            var xIncome = parseInt($("#addMoreIncome").attr('data-x'));
            $("#addMoreIncome").click(function() {
                var template =  $(".otherIncomeTemplate").clone();
                template.removeClass('otherIncomeTemplate');
                template.find('select').first().attr('name', template.find('select').first().attr('name').replace('x', xIncome));
                template.find('input').first().attr('name', template.find('input').last().attr('name').replace('x', xIncome));
                xIncome++;


                $("#earnings > tbody").append(template);
            });

            var xDeduction = parseInt($("#addMoreDeduction").attr('data-x'));
            $("#addMoreDeduction").click(function() {
                var template =  $(".otherDeductionTemplate").clone();
                template.removeClass('otherDeductionTemplate');
                template.find('select').first().attr('name', template.find('select').first().attr('name').replace('x', xDeduction));
                template.find('input').first().attr('name', template.find('input').last().attr('name').replace('x', xDeduction));
                xDeduction++;

                console.log(template);

                $(".deductions > tbody").append(template);
            });

            $(document).on('change keyup', "#earnings input, .deductions input, #tax", function() {
                updateTotals();
            });

            function updateTotals()
            {
                var totalEarnings = 0;

                $("#earnings input").each(function() {
                    var me = parseFloat($(this).val().replace(',', '')) || 0;
                    totalEarnings += me;
                });

                $("#grossAmount").text(totalEarnings.numberFormat(2));
                $("#grossAmountTxt").val(totalEarnings.numberFormat(2));


                var totalDeductions = 0;

                // $(".deductions").each(function() {
                //     $(this).find('input').each(function() {
                //         var me = parseFloat($(this).val().replace(',', '')) || 0;
                //         totalDeductions += me;
                //     });
                // })
                $(".deductions input").each(function() {
                    var me = parseFloat($(this).val().replace(',', '')) || 0;
                    totalDeductions += me;
                });

                $("#deductions").text(totalDeductions.numberFormat(2));
                $("#deductionsTxt").val(totalDeductions.numberFormat(2));

                var tax = parseFloat($("#tax").val().replace(',', '')) || 0;

                $("#netPay").text((totalEarnings - (totalDeductions + tax)).numberFormat(2));
                $("#netPayTxt").val((totalEarnings - (totalDeductions + tax)).numberFormat(2));
            }
        });
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ url('/admin/payroll/wizard/')}}">Wizard</a></li>
        <li><a class="link-effect" href="{{ route('payroll_cycle')}}">Payroll Cycle</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <form action="" method="POST">
                                    {{ csrf_field() }}

                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Effective Year</label>
                                                    <div class="input-group btn-yyyy ">
                                                        <input type="text" class="form-control" id="effective_year" name="effective_year" placeholder="YYYY" value="{{ date('Y') }}" readonly required>
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="effective_month">Effective Month</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="effective_month" name="effective_month" size="1" required>
                                                        @for ($i=1; $i<=12; $i++)
                                                            <option value="{{ date('m', strtotime(date('Y')."-$i")) }}" @if(date('m') == $i) selected @endif>{{ date('F', strtotime(date('Y')."-$i")) }}</option>
                                                        @endfor
                                                        </select>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="cycle">Cycle</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="cycle" name="cycle" size="1" required>
                                                        <option value="A">Cycle A</option>
                                                        <option value="B">Cycle B</option>
                                                    </select>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-xs-12" for="payroll_group">Payroll Group</label>
                                                <div class="col-xs-12">
                                                    <select class="form-control" id="payroll_group" name="payroll_group_id" size="1" required>
                                                        @foreach(\App\PayrollGroups::whereNotIn('company_id', \App\Companies::where('status', 'Active')->where('parent_company_id', '!=', '')->pluck('id'))->orderBy('group_name')->get() as $payrollGroup)
                                                            <option value="{{ $payrollGroup->id }}" data-start-date="{{ $payrollGroup->start_date_a }}" data-payroll-a="{{ $payrollGroup->payroll_date_a }}" data-payroll-b="{{ $payrollGroup->payroll_date_b }}" data-groupname="{{ $payrollGroup->group_name }}">{{ $payrollGroup->group_name }} ({{ !empty($payrollGroup->Company->company_code) ? $payrollGroup->Company->company_code : '' }})</option>
                                                        @endforeach
                                                    </select>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Start Date</label>
                                                    <input type="text" class="form-control" id="start_date" name="start_date" placeholder="mm/dd/yyyy" readonly="true" required>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">End Date</label>
                                                    <input type="text" class="form-control" id="end_date" name="end_date" placeholder="mm/dd/yyyy"  readonly="true" required>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Payroll Period</label>
                                                    <input type="text" class="form-control" id="payroll_period" name="payroll_period" placeholder="mm/dd/yyyy" readonly="true"  required>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Payroll Date</label>
                                                    <input type="text" class="form-control" id="payroll_date" name="payroll_date" placeholder="mm/dd/yyyy" readonly="true"  required>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label for="payroll_code">Payroll Code</label>
                                                    <input type="text" id="payroll_code" class="form-control" name="payroll_code" placeholder="" readonly="true" required>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="css-input css-checkbox css-checkbox-primary">
                                                        <input type="checkbox" id=r" name="calculate_13th_month"><span></span> Calculate 13th Month?
                                                    </label>
                                                </div>
                                            </div>
                                            {{--Start --}}
                                            {{-- Hidden for the meantime --}}
                                            {{--<div class="form-group">--}}
                                                {{--<div class="col-xs-12">--}}
                                                    {{--<label class="css-input css-checkbox css-checkbox-primary">--}}
                                                        {{--<input type="checkbox" id="" name="calculate_leave_encashment"><span></span> Calculate Leave Encashment?--}}
                                                    {{--</label>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{-- End--}}
                                        </div>

                                        <div class="col-md-12 text-right">
                                            <a class="btn btn-danger" onclick="window.history.back()">Cancel</a>
                                            <button class="btn btn-success">Open</button>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            function updatePayrollCycle() {
                var effectiveYear = $("#effective_year").val();
                var effectiveMonth = $("#effective_month").find(':selected').val().toString();
                var cycle = $("#cycle").find(':selected').val();
                var startDate = $("#payroll_group").find(':selected').attr('data-start-date').toString();
                var payrollGroup = $("#payroll_group").find(':selected').attr('data-groupname').replace(' ', '');

                var date1 = moment(effectiveYear + '-' + effectiveMonth + '-' + startDate);
                var date2 = moment(effectiveYear + '-' + effectiveMonth + '-' + date1.endOf('month').format('DD'));
                var diff = date2.diff(date1).days;

                if(cycle == 'A') {
                    var payrollDate = $("#payroll_group").find(':selected').attr('data-payroll-a').toString();

                    // console.log(payrollDate);
                    // alert(payrollDate);

                    if(parseInt(startDate) > 1) {
                        $("#start_date").val(moment(effectiveYear + "/" + effectiveMonth + '/' + startDate).subtract(1, 'months').format("MM/DD/YYYY"));
                        $("#end_date").val(moment(effectiveYear + '-' + effectiveMonth + '-' + startDate).subtract(16, 'd').format("MM/DD/YYYY"));
                    } else {
                        $("#start_date").val(moment(effectiveYear + "/" + effectiveMonth + '/' + startDate).format("MM/DD/YYYY"));
                        $("#end_date").val(moment(effectiveYear + '-' + effectiveMonth + '-' + startDate).add(14, 'd').format("MM/DD/YYYY"));
                    }

//                    $("#start_date").val(effectiveMonth + "/" + startDate + '/' + effectiveYear);
//                     $("#start_date").val(moment(effectiveYear + "/" + effectiveMonth + '/' + startDate).subtract(1, 'months').format("MM/DD/YYYY"));

//                    $("#end_date").val(moment(effectiveYear + "/" + effectiveMonth + '/' + startDate).add(14, 'days').format("MM/DD/YYYY"));
//                     $("#end_date").val(moment(effectiveYear + '-' + effectiveMonth + '-' + startDate).subtract(16, 'd').format("MM/DD/YYYY"));
                } else {
                    var payrollDate = $("#payroll_group").find(':selected').attr('data-payroll-b').toString();

                    if(parseInt(startDate) > 1) {
                        $("#start_date").val(moment(effectiveYear + '-' + effectiveMonth + '-' + startDate).subtract(15, 'd').format("MM/DD/YYYY"));
                        $("#end_date").val(moment(effectiveYear + '-' + effectiveMonth + '-' + startDate).subtract(1, 'days').format('MM/DD/YYYY'));
                    } else {
                        $("#start_date").val(moment(effectiveYear + '-' + effectiveMonth + '-' + startDate).add(15, 'd').format("MM/DD/YYYY"));
                        $("#end_date").val(moment(effectiveYear + '-' + effectiveMonth + '-' + startDate).endOf("month").format('MM/DD/YYYY'));
                    }


                    // $("#start_date").val(moment(effectiveYear + '-' + effectiveMonth + '-' + startDate).subtract(15, 'd').format("MM/DD/YYYY"));
                    // $("#end_date").val(moment(effectiveYear + '-' + effectiveMonth + '-' + startDate).subtract(1, 'days').format('MM/DD/YYYY'));

//                    if(diff <= 5) {
//                        $("#end_date").val(date2.format('MM/DD/YYYY'));
//                    } else {
//                        $("#end_date").val(moment(effectiveYear + '-' + effectiveMonth + '-' + startDate).subtract(1, 'days').format('MM/DD/YYYY'));
//                    }
                }

                if(payrollDate.length == 1) {
                    payrollDate = "0" + payrollDate.toString();
                }

                $("#payroll_period").val($("#start_date").val() + ' - ' + $("#end_date").val());

                // alert(payrollDate);

                if(parseInt(payrollDate) > parseInt(moment(effectiveYear + '-' + effectiveMonth + '-' + 1).endOf("month").format('DD'))) {
                    $("#payroll_date").val(moment(effectiveYear + '-' + effectiveMonth + '-' + 1).endOf("month").format('MM/DD/YYYY'));
                } else {
                    $("#payroll_date").val(effectiveMonth + "/" + payrollDate + '/' + effectiveYear);
                }





                $("#payroll_code").val(payrollGroup + '-' + effectiveYear.toString() + effectiveMonth.toString() + cycle.toString());
            }

            updatePayrollCycle();

            $(document).on('change', "#payroll_group, #cycle, #effective_month, #effective_year", function() {
               updatePayrollCycle();
            });
        })
    </script>
@endsection

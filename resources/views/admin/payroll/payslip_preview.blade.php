@extends('layouts.app')

@section('content')
    <a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/process") }}"><i class="fa fa-arrow-left"></i> Back to Payroll Members</a>

    <div class="text-right">
        <a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/payslip/{$user->id}/edit") }}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit Payroll</a>
    </div>
    <br>

    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('payroll_wizard_index') }}">Wizard</a></li>
        <li><a class="link-effect" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}") }}">Payroll Members</a></li>
        <li><a class="link-effect" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/process") }}">Process Payroll</a></li>
        <li><a class="link-effect" href="#">Payslip Preview</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Employee ID Number</label>
                                            <input type="text" class="form-control" name=""  value="{{ $user->Profile->id_number }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Employee Name</label>
                                            <input type="text" class="form-control" name=""  value="{{ $user->name }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Cost Center</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->PayrollGroup->group_name }} ({{ $payrollCycle->PayrollGroup->Company->company_code }})" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Period</label>
                                            <input type="text" class="form-control" name="payroll_period" value="{{ $payrollCycle->payroll_period }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Code</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->payroll_code }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Date</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ date('m/d/Y', strtotime($payrollCycle->payroll_date)) }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Monthly Rate</label>
                                            <input type="text" class="form-control" name=""  value="@if($user->Profile->Compensation->rate_type == 'Monthly') {{ number_format($payrollComputation->monthlyRate, 2) }} @else N/A @endif" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Daily Rate</label>
                                            <input type="text" class="form-control" name=""  value="{{ number_format($payrollComputation->dailyRate, 2) }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <hr />
                                </div>


                                <div class="col-md-3">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center" colspan="2">Employer Paid Benefits</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td width="50%">SSS ER</td>
                                            <td>{{ number_format($payroll->sss_er, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>SSS EC</td>
                                            <td>{{ number_format($payroll->sss_ec, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>HDMF ER</td>
                                            <td>{{ number_format($payroll->hdmf_er, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>PhilHealth ER</td>
                                            <td>{{ number_format($payroll->philhealth_er, 2) }}</td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th colspan="2" class="text-center">Year to Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td width="50%">Gross Amount</td>
                                            <td>{{ number_format($ytdPayrollSummary->sum('gross_amount'), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Deductions</td>
                                            <td>{{ number_format($ytdPayrollSummary->sum('deductions'), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tax</td>
                                            <td>{{ number_format($ytdPayrollSummary->sum('tax'), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>BASIC</td>
                                            <td>{{ number_format($ytdPayrollSummary->sum('basic_pay'), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>De Minimis</td>
                                            <td>{{ number_format($ytdPayrollSummary->sum('de_minimis'), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Fixed Allowance</td>
                                            <td>{{ number_format($ytdPayrollSummary->sum('fixed_allowance'), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Incentives</td>
                                            <td>{{ number_format($ytdPayrollSummary->sum('incentives'), 2) }}</td>
                                        </tr>
                                        {{--<tr>--}}
                                            {{--<td>SSS ER</td>--}}
                                            {{--<td>{{ number_format($ytdPayrollSummary->sum('sss_er'), 2) }}</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>SSS EC</td>--}}
                                            {{--<td>{{ number_format($ytdPayrollSummary->sum('sss_ec'), 2) }}</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>HDMF ER</td>--}}
                                            {{--<td>{{ number_format($ytdPayrollSummary->sum('hdmf_er'), 2) }}</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>PhilHealth ER</td>--}}
                                            {{--<td>{{ number_format($ytdPayrollSummary->sum('philhealth_er'), 2) }}</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>SSS EE</td>--}}
                                            {{--<td>{{ number_format($ytdPayrollSummary->sum('sss_ee'), 2) }}</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>HDMF EE</td>--}}
                                            {{--<td>{{ number_format($ytdPayrollSummary->sum('hdmf_ee'), 2) }}</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>PhilHealth EE</td>--}}
                                            {{--<td>{{ number_format($ytdPayrollSummary->sum('philhealth_ee'), 2) }}</td>--}}
                                        {{--</tr>--}}
                                        <tr>
                                            <td>SSS Loan</td>
                                            <td>{{ number_format($ytdPayrollSummary->sum('sss_loan'), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>HDMF Loan</td>
                                            <td>{{ number_format($ytdPayrollSummary->sum('hdmf_loan'), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>COOP Loan</td>
                                            <td>{{ number_format($ytdPayrollSummary->sum('coop_loan'), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>COOP Share</td>
                                            <td>{{ number_format($ytdPayrollSummary->sum('coop_share'), 2) }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-5">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="50%">Hours and Earnings</th>
                                            <th class="text-center">Days</th>
                                            <th width="30%" class="text-center">Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>BASIC</td>
                                            <td>{{ ($payrollComputation->rg() / 8) }}</td>
                                            <td>{{ number_format($payroll->basic_pay, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>RD</td>
                                            <td>{{ ($payrollComputation->rd / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->rd(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>SPE</td>
                                            <td>{{ ($payrollComputation->spe / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->spe(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>SPERD</td>
                                            <td>{{ ($payrollComputation->sperd / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->sperd(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>LEG</td>
                                            <td>{{ ($payrollComputation->leg / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->leg(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>LEGRD</td>
                                            <td>{{ ($payrollComputation->legrd / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->legrd(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>OT</td>
                                            <td>{{ ($payrollComputation->regot / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->OTPay(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>ND</td>
                                            <td>{{ ($payrollComputation->nd / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->NDPay(), 2) }}</td>
                                        </tr>
                                        @if($payroll->unpaid_leave == 0)
                                        <tr>
                                            <td>VL</td>
                                            <td>{{ ($payrollComputation->vl / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->vl(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>SL</td>
                                            <td>{{ ($payrollComputation->sl / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->sl(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>EL</td>
                                            <td>{{ ($payrollComputation->el / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->el(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>SPL</td>
                                            <td>{{ ($payrollComputation->spl / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->spl(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>ML</td>
                                            <td>{{ ($payrollComputation->ml / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->ml(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>PL</td>
                                            <td>{{ ($payrollComputation->pl / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->pl(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>PTO</td>
                                            <td>{{ ($payrollComputation->pto / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->pto(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>BL</td>
                                            <td>{{ ($payrollComputation->bl / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->bl(), 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>CL</td>
                                            <td>{{ ($payrollComputation->cl / 8) }}</td>
                                            <td>{{ number_format($payrollComputation->cl(), 2) }}</td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <td>ECOLA</td>
                                            <td></td>
                                            <td>{{ number_format($payroll->ecola, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>HECOLA</td>
                                            <td></td>
                                            <td>{{ number_format($payroll->holiday_ecola, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>NWAGE</td>
                                            <td></td>
                                            <td>{{ number_format($payroll->adjustment_wage, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>DE MINIMIS</td>
                                            <td></td>
                                            <td>{{ number_format($payroll->de_minimis, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>13TH MONTH</td>
                                            <td></td>
                                            <td>{{ number_format($payroll->thirteenth_month_bonus, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>FIXED ALLOWANCE</td>
                                            <td></td>
                                            <td>{{ number_format($payroll->fixed_allowance, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>LEAVE</td>
                                            <td></td>
                                            <td>{{ number_format($payroll->leave_encashment, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>INCENTIVES</td>
                                            <td></td>
                                            <td>{{ number_format($payroll->incentives, 2) }}</td>
                                        </tr>
{{--                                        @if($payroll->adjustment_income > 0)--}}
                                            <tr>
                                                <td>Other Income</td>
                                                <td></td>
                                                <td>{{ number_format($payroll->adjustment_income, 2) }}</td>
                                            </tr>
                                        @foreach($dtr->Others()->where('type', 'Income') as $x => $income)
                                            <tr>
                                                <td>{{ $income->title }}</td>
                                                <td></td>
                                                <td>{{ number_format($income->amount, 2) }}</td>
                                            </tr>
                                        @endforeach
                                        {{--@endif--}}
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-4">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="50%">Deductions</th>
                                            <th class="text-center">Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Absences</td>
                                            <td>{{ number_format($payroll->absences, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tardiness</td>
                                            <td>{{ number_format($payroll->tardiness, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>SSS EE</td>
                                            <td>{{ number_format($payroll->sss_ee, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>HDMF EE</td>
                                            <td>{{ number_format($payroll->hdmf_ee, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>PhilHealth EE</td>
                                            <td>{{ number_format($payroll->philhealth_ee, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>SSS Loan</td>
                                            <td>{{ number_format($payroll->sss_loan, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>HDMF Loan</td>
                                            <td>{{ number_format($payroll->hdmf_loan, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Client Charges</td>
                                            <td>{{ number_format($payroll->client_charges, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Membership Fee</td>
                                            <td>{{ number_format($payroll->membership_fee, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Notarial Fee</td>
                                            <td>{{ number_format($payroll->notarial_fee, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Coop Loan</td>
                                            <td>{{ number_format($payroll->coop_loan, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Coop Share</td>
                                            <td>{{ number_format($payroll->coop_share, 2) }}</td>
                                        </tr>
                                        @if($payroll->unpaid_leave > 0)
                                        <tr>
                                            <td>Unpaid Leave</td>
                                            <td>{{ number_format($payroll->unpaid_leave, 2) }}</td>
                                        </tr>
                                        @endif

{{--                                        @if($payroll->adjustment_deduction > 0)--}}
                                            <tr>
                                                <td>Other Deduction</td>
                                                <td>{{ number_format($payroll->adjustment_deduction, 2) }}</td>
                                            </tr>
                                        {{--@endif--}}

                                        @foreach($dtr->Others()->where('type', 'Deduction') as $x => $income)
                                            <tr>
                                                <td>{{ $income->title }}</td>
                                                <td>{{ number_format($income->amount, 2) }}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="50%">CURRENT NET PAY</th>
                                            <th class="text-center">Amount</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr>
                                            <th>
                                                <div>GROSS AMOUNT</div>
                                                <div>DEDUCTIONS</div>
                                                <div>TAX</div>
                                            </th>
                                            <td>
                                                <div>{{ number_format($payroll->gross_amount, 2) }}</div>
                                                <div>{{ number_format($payroll->deductions, 2) }}</div>
                                                <div>{{ number_format($payroll->tax, 2) }}</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>NET PAY</th>
                                            <th>{{ number_format($payroll->net_pay, 2) }}</th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>


                                <?php /*
                                <div class="col-md-10">
                                    <h2 class="content-heading">Hours and Earnings</h2>
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>Taxable</th>
                                        <th></th>
                                        <th>Non-Taxable</th>
                                        <th></th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Basic Pay</td>
                                            <td>{{ number_format($payroll->basic_pay, 2) }}</td>
                                            <td>De Minimis</td>
                                            <td>{{ number_format($payroll->de_minimis, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>ECOLA</td>
                                            <td>{{ number_format($payroll->ecola, 2) }}</td>
                                            <td>13th Month Bonus</td>
                                            <td>{{ number_format($payroll->thirteenth_month_bonus, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Overtime Pay</td>
                                            <td>{{ number_format($payroll->overtime_pay, 2) }}</td>
                                            <td>Fixed Allowance</td>
                                            <td>{{ number_format($payroll->fixed_allowance, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Holiday Pay</td>
                                            <td>{{ number_format($payroll->holiday_pay, 2) }}</td>
                                            <td>Leave Encashment</td>
                                            <td>{{ number_format($payroll->leave_encashment, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Night Differential</td>
                                            <td>{{ number_format($payroll->night_differential, 2) }}</td>
                                            <td>Incentives</td>
                                            <td>{{ number_format($payroll->incentives, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Holiday ECOLA</td>
                                            <td>{{ number_format($payroll->holiday_ecola, 2) }}</td>
                                            {{--<td>Coop Savings</td>--}}
                                            {{--<td></td>--}}
                                            <td>Adjustment - Income</td>
                                            <td>{{ number_format($payroll->adjustment_income, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Adjustment - Wage</td>
                                            <td>{{ number_format($payroll->adjustment_wage, 2) }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-12"></div>

                                <div class="col-md-6">
                                    <h2 class="content-heading">Before Tax Deductions</h2>
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td>SSS EE</td>
                                            <td>{{ number_format($payroll->sss_ee, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>HDMF EE</td>
                                            <td>{{ number_format($payroll->hdmf_ee, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>PhilHealth EE</td>
                                            <td>{{ number_format($payroll->philhealth_ee, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tardiness</td>
                                            <td>{{ number_format($payroll->tardiness, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Absences</td>
                                            <td>{{ number_format($payroll->absences, 2) }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-6">
                                    <h2 class="content-heading">After Tax Deductions</h2>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>SSS Loan</td>
                                            <td>{{ number_format($payroll->sss_loan, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>HDMF Loan</td>
                                            <td>{{ number_format($payroll->hdmf_loan, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Client Charges</td>
                                            <td>{{ number_format($payroll->client_charges, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Membership Fee</td>
                                            <td>{{ number_format($payroll->membership_fee, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Notarial Fee</td>
                                            <td>{{ number_format($payroll->notarial_fee, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Coop Loan</td>
                                            <td>{{ number_format($payroll->coop_loan, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Coop Share</td>
                                            <td>{{ number_format($payroll->coop_share, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Adjustment - Deduction</td>
                                            <td>{{ number_format($payroll->adjustment_deduction, 2) }}</td>
                                        </tr>
                                    </table>
                                </div>

{{--                                @if($dtr->Profile->Compensation->compute_sss == 'Y' || $dtr->Profile->Compensation->compute_hdmf == 'Y' || $dtr->Profile->Compensation->compute_philhealth == 'Y')--}}
                                <div class="col-md-6">
                                    <h2 class="content-heading">Employer Paid Benefits</h2>
                                    <table class="table table-bordered">
                                        <tbody>
                                        {{--@if($dtr->Profile->Compensation->compute_sss == 'Y')--}}
                                        <tr>
                                            <td>SSS ER</td>
                                            <td>{{ number_format($payroll->sss_er, 2) }}</td>
                                        </tr>
                                        {{--@endif--}}

{{--                                        @if($dtr->Profile->Compensation->compute_hdmf == 'Y')--}}
                                        <tr>
                                            <td>HDMF ER</td>
                                            <td>{{ number_format($payroll->hdmf_er, 2) }}</td>
                                        </tr>
                                        {{--@endif--}}

{{--                                        @if($dtr->Profile->Compensation->compute_philhealth == 'Y')--}}
                                        <tr>
                                            <td>PhilHealth ER</td>
                                            <td>{{ number_format($payroll->philhealth_er, 2) }}</td>
                                        </tr>
                                        {{--@endif--}}
                                        </tbody>
                                    </table>
                                </div>
                                {{--@endif--}}

                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>GROSS AMOUNT</th>
                                            <th>DEDUCTIONS</th>
                                            <th>TAX</th>
                                            <th>NET PAY</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th>CURRENT</th>
                                            <td>{{ number_format($payroll->gross_amount, 2) }}</td>
                                            <td>{{ number_format($payroll->deductions, 2) }}</td>
                                            <td>{{ number_format($payroll->tax, 2) }}</td>
                                            <td>{{ number_format($payroll->net_pay, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <th>YTD</th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                */ ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

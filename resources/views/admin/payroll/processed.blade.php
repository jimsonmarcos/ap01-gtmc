@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('payroll_wizard_index')}}">Wizard</a></li>
        <li><a class="link-effect" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}") }}">Payroll Members</a></li>
        <li><a class="link-effect" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/process") }}">Process</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Group</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->PayrollGroup->group_name }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Period</label>
                                            <input type="text" class="form-control" name="payroll_period" value="{{ $payrollCycle->payroll_period }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Code</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->payroll_code }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Date</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ date('m/d/Y', strtotime($payrollCycle->payroll_date)) }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <hr />
                                </div>

                                <div class="col-md-12">
                                    <form action="" method="POST">
                                        {{ csrf_field() }}

                                        <table class="table table-striped" id="processTable">
                                            <thead>
                                            <tr>
                                                <th>Employee ID Number</th>
                                                <th>Employee Name</th>
                                                <th>Net Pay</th>
                                                <th class="text-center" width="100">Hold Status</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($dtr as $d)
                                            <tr>
                                                <td>{{ $d->id_number }}</td>
                                                <td>{{ $d->User->Profile->namelfm() }}</td>
                                                <td>
                                                    {{ number_format($d->net_pay, 2) }}
                                                </td>
                                                <td class="text-center @if($d->Dtr->hold_status == 'Y') text-danger @else text-success @endif">{{ $d->Dtr->hold_status == 'N' ? 'No' : 'Yes' }}</td>
                                                <td><a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/payslip/{$d->User->id}") }}" class="btn btn-sm btn-primary">VIEW PAYSLIP <i class="fa fa-chevron-right"></i></a></td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </form>
                                </div>

                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#processTable').DataTable({
                "sorting": [],
                "pageLength": 100
            });
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ url('/admin/payroll/wizard/')}}">Wizard</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />
                                <div class="col-md-12">
                                    <a href="{{ route('payroll_cycle') }}" class="btn btn-primary">OPEN</a>
                                    <hr />
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-xs-12" for="company_code">Company Code</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="company_code" name="company_code" size="1" required>
                                                <option value="all">All</option>
                                                @foreach(\App\Companies::where('status', 'Active')->orderBy('company_code')->get() as $company)
                                                    <option value="{{ $company->id }}" @if($company_id == $company->id) selected @endif>{{ $company->company_code }}</option>
                                                @endforeach
                                            </select>
                                            <br />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-xs-12" for="payroll_group">Payroll Group</label>
                                        <div class="col-xs-12">
                                            <select class="form-control" id="payroll_group" name="payroll_group_id" size="1" @if(!isset($payrollGroups)) disabled @endif>
                                                @if(!isset($payrollGroups))
                                                    <option value=""></option>
                                                @else
                                                    <option value="all">All</option>
                                                    @foreach($payrollGroups as $payrollGroup)
                                                        <option value="{{ $payrollGroup->id }}" @if($payroll_group_id == $payrollGroup->id) selected @endif>{{ $payrollGroup->group_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <br />
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="transaction_payment_tab" role="tablist">
                                        <li class="nav-item @if($status != 'Processed') active @endif">
                                            <a class="nav-link" href="{{ url("admin/payroll/wizard?company_id={$company_id}&payroll_group_id={$payroll_group_id}") }}"><span class="badge badge-info">{{ $open }}</span> Open</a>
                                        </li>
                                        <li class="nav-item @if($status == 'Processed') active @endif">
                                            <a class="nav-link" href="{{ url("admin/payroll/wizard?company_id={$company_id}&payroll_group_id={$payroll_group_id}&status=Processed") }}"><span class="badge badge-success">{{ $processed }}</span> Processed</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Payroll Code</th>
                                            <th>Company Code</th>
                                            <th>Payroll Period</th>
                                            <th>Payroll Date</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($payrollCycles as $payrollCycle)
                                            <?php $action = ($payrollCycle->status == 'Processed') ? 'processed' : 'open' ?>
                                            <tr>
                                                <td>
                                                    @if(in_array($payrollCycle->status, ['Processed', 'In Progress']))<a href="{{ url("/admin/payroll/wizard/export-{$action}/{$payrollCycle->id}") }}" class="btn btn-primary">Export to Excel</a>@endif
                                                    @if(in_array($payrollCycle->status, ['Processed', 'In Progress']))<a href="{{ url("/print/payslips/{$payrollCycle->id}") }}" class="btn-primary btn" target="_blank">Print</a>@endif
                                                </td>
                                                <td>
                                                    <a href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycle->id}") }}">{{ $payrollCycle->payroll_code }}</a>
                                                </td>
                                                <td>{{ $payrollCycle->PayrollGroup->Company->company_code }}</td>
                                                <td>{{ $payrollCycle->payroll_period }}</td>
                                                <td>{{ $payrollCycle->payroll_date }}</td>
                                                <td>{{ $payrollCycle->status }}</td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            var base = "{{ url('/admin/payroll/wizard') }}?";
            $(document).on('change', '#company_code', function() {
                var company_id = $(this).find(':selected').val();

                location.href = base + 'company_id=' + company_id;
            });

            $(document).on('change', '#payroll_group', function() {
                var company_id = $("#company_code").find(':selected').val();
                var payroll_group_id = $(this).find(':selected').val();

                location.href = base + 'company_id=' + company_id + '&payroll_group_id=' + payroll_group_id;
            });
        })
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('payroll_wizard_index') }}">Wizard</a></li>
        <li><a class="link-effect" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}") }}">Payroll Members</a></li>
        <li><a class="link-effect" href="{{ url("/admin/payroll/wizard/payroll_members/{$payrollCycleId}/process") }}">Process</a></li>
        <li><a class="link-effect" href="#">Processed Payroll</a></li>

    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Group</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->PayrollGroup->group_name }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Period</label>
                                            <input type="text" class="form-control" name="payroll_period" value="{{ $payrollCycle->payroll_period }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Code</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ $payrollCycle->payroll_code }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="form-control-label">Payroll Date</label>
                                            <input type="text" class="form-control" name="payroll_date"  value="{{ date('m/d/Y', strtotime($payrollCycle->payroll_date)) }}" disabled>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <hr />
                                </div>

                                <form action="" method="POST" enctype="multipart/form-data">
                                    <div class="col-md-6">
                                        <h2 class="content-heading">Hours and Earnings</h2>
                                        <table class="table table-bordered.table-striped">
                                            <thead>
                                                <th>Description</th>
                                                <th>Hours</th>
                                                <th>Current</th>
                                            </thead>
                                            <tbody>
                                                @for($x=1; $x<=3; $x++)
                                                <tr>
                                                    <td>1</td>
                                                    <td>2</td>
                                                    <td>3</td>
                                                </tr>
                                                @endfor
                                            </tbody>
                                        </table>


                                        <h2 class="content-heading">Before Tax Deductions</h2>
                                        <table class="table table-bordered.table-striped">
                                            <thead>
                                            <th>Description</th>
                                            <th>Hours</th>
                                            <th>Current</th>
                                            </thead>
                                            <tbody>
                                            @for($x=1; $x<=3; $x++)
                                                <tr>
                                                    <td>1</td>
                                                    <td>2</td>
                                                    <td>3</td>
                                                </tr>
                                            @endfor
                                            </tbody>
                                        </table>


                                    </div>
                                    <div class="col-md-6">
                                        <h2 class="content-heading">Employer Paid Benefits</h2>
                                        <table class="table table-bordered.table-striped">
                                            <thead>
                                            <th>Description</th>
                                            <th>Hours</th>
                                            <th>Current</th>
                                            </thead>
                                            <tbody>
                                            @for($x=1; $x<=3; $x++)
                                                <tr>
                                                    <td>1</td>
                                                    <td>2</td>
                                                    <td>3</td>
                                                </tr>
                                            @endfor
                                            </tbody>
                                        </table>

                                        <h2 class="content-heading">After Tax Deductions</h2>
                                        <table class="table table-bordered.table-striped">
                                            <thead>
                                            <th>Description</th>
                                            <th>Hours</th>
                                            <th>Current</th>
                                            </thead>
                                            <tbody>
                                            @for($x=1; $x<=3; $x++)
                                                <tr>
                                                    <td>1</td>
                                                    <td>2</td>
                                                    <td>3</td>
                                                </tr>
                                            @endfor
                                            </tbody>
                                        </table>


                                    </div>
                                </form>

                                <div class="col-md-6">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <h2 class="content-heading">Total Gross</h2>
                                        </div>
                                        <div class="col-md-6">
                                            <h2 class="content-heading">1</h2>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <h2 class="content-heading">Total Taxes</h2>
                                        </div>

                                        <div class="col-md-6">
                                            <h2 class="content-heading">1</h2>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <h2 class="content-heading">Total Deductions</h2></div>
                                        <div class="col-md-6">
                                            <h2 class="content-heading">1</h2>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <h2 class="content-heading">Net Pay</h2>
                                        </div>
                                        <div class="col-md-6">
                                            <h2 class="content-heading">1</h2>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

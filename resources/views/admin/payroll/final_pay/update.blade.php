@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('final_pay') }}">Final Pay</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <h3 class="text-center">Final Pay Details</h3>
                                <br>
                                <p class="text-center"><strong>{{ \App\Keyval::where('key', 'account_name')->first()->value }}</strong></p>
                                <br>

                                <form action="" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="col-md-12">

                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="gtmc_id_no">GTMC ID NO.</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="gtmc_id_no" name="gtmc_id_no" value="{{ $employee->Profile->id_number }}" disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="employee_name">Employee Name</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="employee_name" name="employee_name" value="{{ $employee->Profile->namelfm() }}" disabled />
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="position">Position</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="position" name="position" value="{{ $employee->Profile->EmploymentDetails->Position->title }}" disabled />
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="tin_no">TIN No.</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="tin_no" name="tin_no" value="{{ $employee->Profile->Compensation->tin }}" disabled />
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Effective Date</label>
                                                    <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                        <input type="text" class="form-control" name="effective_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y') }}" required disabled>
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="company_code">Company Code</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="company_code" name="company_code" value="{{ $employee->Profile->CostCenter->Company->company_code }}" disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="payroll_group">Payroll Group</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="payroll_group" name="payroll_group" value="{{ $employee->Profile->Compensation->PayrollGroup->group_name }}" disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="cost_center">Cost Center</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="cost_center" name="cost_center" value="{{ $employee->Profile->CostCenter->cost_center }}" disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="rate_type">Rate Type</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="rate_type" name="rate_type" value="{{ $employee->Profile->Compensation->rate_type }}" disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Hire Date</label>
                                                    <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                        <input type="text" class="form-control" name="hire_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y', strtotime($employee->Profile->EmploymentDetails->hire_date)) }}" disabled>
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <br />
                                                </div>
                                            </div>

                                            <br />
                                            <br />
                                            <br />

                                            <div class="form-group">
                                                <label class="col-xs-12" for="monthly_rate">Monthly Rate</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="monthly_rate" name="monthly_rate" value="{{ number_format($employee->Profile->Compensation->daily_rate * 26, 2) }}" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="daily_rate">Daily Rate</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="daily_rate" name="daily_rate" value="{{ number_format($employee->Profile->Compensation->daily_rate, 2) }}" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="hourly_rate">Hourly Rate</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="hourly_rate" name="hourly_rate" value="{{ number_format($employee->Profile->Compensation->daily_rate / 8, 2) }}" required disabled/>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="col-xs-12">
                                            <h3>Final Pay Computation</h3>
                                            <table id="final_pay_computation" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">Particulars</th>
                                                    <th class="text-center" width="30%">Collectible <br> (From Employee)</th>
                                                    <th class="text-center" width="30%">Payable (To Employee)</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Salary (Last Cut off {{ $employee->PayrollCycle->payroll_period }})</td>
                                                    <td></td>
                                                    <td>{{ $employee->salary }}</td>
                                                </tr>

                                                <tr>
                                                    <td>Cash Savings</td>
                                                    <td></td>
                                                    <td>{{ number_format($employee->cash_savings, 2) }}</td>
                                                </tr>

                                                <tr>
                                                    <td>Loan Savings</td>
                                                    <td></td>
                                                    <td>{{ number_format($employee->loan_savings, 2) }}</td>
                                                </tr>

                                                <tr>
                                                    <td>13th Month</td>
                                                    <td></td>
                                                    <td>{{ number_format($employee->thirteenth_month, 2) }}</td>
                                                </tr>

                                                @if(!empty($employee->ExistingLoans))
                                                    @foreach($employee->ExistingLoans as $unpaidLoan)
                                                        <tr>
                                                            <td>
                                                                @if(!empty($unpaidLoan->Loan->type))
                                                                    {{ $unpaidLoan->Loan->type }} Loan
                                                                @else
                                                                    {{ $unpaidLoan->Loan->LoanType->type }}
                                                                @endif
                                                            </td>
                                                            <td>{{ number_format($unpaidLoan->amount, 2) }}</td>
                                                            <td></td>
                                                        </tr>
                                                    @endforeach
                                                @endif

                                                <tr>
                                                    <td>Client Charges</td>
                                                    <td><input type="text" name="client_charges" value="{{ $employee->client_charges }}" id="clientCharges" class="form-control" @if($employee->status == 'Approved') disabled @endif></td>
                                                    <td></td>
                                                </tr>

                                                @if(!empty($employee->OtherCharges))
                                                    @foreach($employee->OtherCharges as $otherCharges)
                                                        <tr>
                                                            <td><input type="text" name="other_changes_existing[{{ $otherCharges->id }}][particular]" value="{{ $otherCharges->particular }}" class="form-control" @if($employee->status == 'Approved') disabled @endif></td>
                                                            <td><input type="text" name="other_changes_existing[{{ $otherCharges->id }}][collectible]" value="{{ $otherCharges->collectible }}" class="form-control other-chages" @if($employee->status == 'Approved') disabled @endif></td>
                                                            <td class="text-right">@if($employee->status == 'Pending') <button class="btn btn-xs btn-primary removeOtherCharges"><i class="fa fa-remove"></i> Remove</button> @endif</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="">Total Collectible</th>
                                                        <th id="totalCollectible">{{ number_format($employee->total_collectible, 2) }}</th>
                                                        <th></th>
                                                        <input type="hidden" name="total_collectible" id="totalCollectibleInput" value="{{ $employee->total_collectible }}">
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">NET Payable</th>
                                                        <th id="netPayable">{{ number_format($employee->net_payable, 2) }}</th>
                                                        <input type="hidden" name="net_payable" id="netPayableInput" value="{{ $employee->net_payable }}">
                                                    </tr>
                                                </tfoot>
                                            </table>

                                            @if($employee->status == 'Pending')
                                            <button id="addOtherCharges" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Other Charges</button>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <br>
                                        <br>
                                        <div class="col-xs-12">
                                            <h3>Summary of 13 Month Pay</h3>
                                            <table id="summary" class="table">
                                                <thead>
                                                <tr>
                                                    <th>Payroll Code</th>
                                                    <th>Payroll Period</th>
                                                    <th>Basic Pay</th>
                                                    <th>Pro-Rated</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($employee->Summaries as $index => $payroll)
                                                    <tr>
                                                        <td>{{ $payroll->payroll_code }}</td>
                                                        <td>{{ $payroll->payroll_period }}</td>
                                                        <td>{{ number_format($payroll->basic_pay, 2) }}</td>
                                                        <td>{{ number_format($payroll->pro_rated, 2) }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-right" colspan="3">Total</th>
                                                        <th>{{ number_format($employee->Summaries->sum('pro_rated'), 2) }}</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label>Remarks</label>
                                                <textarea name="remarks" class="form-control" @if($employee->status == 'Approved') disabled @endif>{{ $employee->remarks }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <table class="table">
                                                <thead>
                                                    <th></th>
                                                    <th></th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Prepared By:</td>
                                                        <td>
                                                            {{ $employee->CreatedBy->Profile->namefl() }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Noted By: </td>
                                                        <td>
                                                            <select name="noted_by" @if($employee->status == 'Approved') disabled @endif>
                                                                @foreach(\App\Signatories::all() as $signatory)
                                                                    <option value="{{ $signatory->user_id }}" @if($employee->noted_by == $signatory->user_id) selected @endif>{{ $signatory->User->Profile->namefl() }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Approved By: </td>
                                                        <td>
                                                            <select name="approved_by" @if($employee->status == 'Approved') disabled @endif>
                                                                @foreach(\App\Signatories::all() as $signatory)
                                                                    <option value="{{ $signatory->user_id }}" @if($employee->approved_by == $signatory->user_id) selected @endif>{{ $signatory->User->Profile->namefl() }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-6 text-right">
                                            <br />
                                            <br />
                                            <br />
                                            <br /><br /><br />
                                            <a href="{{ url("admin/payroll/final_pay/{$employee->status}") }}"class="btn btn-primary">CANCEL</a>
                                            @if($employee->status == 'Pending')
                                            <button name="action" value="update" class="btn btn-success"><i class="fa fa-save"></i> UPDATE</button>
                                            <button name="action" value="approve" class="btn btn-success"><i class="fa fa-check"></i> APPROVE</button>
                                            @elseif($employee->status == 'Approved')
                                                <a href="{{ url("admin/payroll/final_pay/disburse/{$employee->id}") }}" class="btn btn-success"><i class="fa fa-save"></i> RELEASE</a>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            // $('#final_pay_computation').dataTable();
            var x = 0;


            $(document).on('click', '#addOtherCharges', function(e) {
                e.preventDefault();
                var template = $("<tr>" +
                    "<td><input type='text' class='form-control' name='other_charges["+ x +"][particular]' /></td>" +
                    "<td><input type='text' class='form-control other-chages' name='other_charges["+ x +"][collectible]' /></td>" +
                    "<td class='text-right'><button class='btn btn-xs btn-primary removeOtherCharges'><i class='fa fa-remove'></i> Remove</button></td>" +
                    "</tr>");

                template.appendTo($("#final_pay_computation").find('tbody'));
                // $("#final_pay_computation").append(template);


                x++;
            });

            function updateNetPayable() {
                var netPay = parseFloat({{ $employee->salary }});
                var cashSavings = parseFloat({{ $employee->cash_savings }});
                var loanSavings = parseFloat({{ $employee->loan_savings }});
                var totalPaymentsLeft = parseFloat(@if(!empty($employee->ExistingLoans)){{ $employee->ExistingLoans->sum('amount') }}@else{{0}}@endif);
                var thirteenthMonth = parseFloat({{ $employee->thirteenth_month }});
                var clientCharges = parseFloat($("#clientCharges").val().replace(/\,/g, '')) || 0;
                var otherCharges = 0;

                if($('.other-chages').length > 0) {
                    $('.other-chages').each(function(i, v) {
                        var amount = parseFloat($(this).val().replace(/\,/g, '')).toFixed(2) || 0;
                        if($(this).val().length == 0) {
                            amount = 0;
                        }
                        // console.log(amount);
                        otherCharges += parseFloat(amount);
                    });
                }

                var totalCollectible = (totalPaymentsLeft + clientCharges + otherCharges);

                totalNetPayable = (netPay + cashSavings + loanSavings + thirteenthMonth) - (totalPaymentsLeft + clientCharges + otherCharges);
                // console.log(otherCharges);
                $("#totalCollectible").text(totalCollectible.numberFormat(2));
                $("#totalCollectibleInput").val(totalCollectible.numberFormat(2));

                $("#netPayable").text(totalNetPayable.numberFormat(2));
                $("#netPayableInput").val(totalNetPayable.numberFormat(2));
            }

            $(document).on('keyup change', '.other-chages, #clientCharges', function() {
                updateNetPayable();
            });

            updateNetPayable();

            $(document).on('click', '.removeOtherCharges', function() {
                event.preventDefault();
                $(this).closest('tr').remove();
                updateNetPayable();
            });
        })
    </script>
@endsection

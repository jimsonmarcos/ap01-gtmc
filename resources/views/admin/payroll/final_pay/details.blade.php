@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('final_pay') }}">Final Pay</a></li>
        <li><a class="link-effect" href="{{ url("admin/payroll/final_pay_details/{$employee->id}") }}">Details</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <h3 class="text-center">Final Pay Details</h3>

                                <form action="" method="post" enctype="multipart/form-data">

                                    <div class="col-md-12">

                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="gtmc_id_no">GTMC ID NO.</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="gtmc_id_no" name="gtmc_id_no" value="{{ $employee->Profile->id_number }}" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="employee_name">Employee Name</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="employee_name" name="employee_name" value="{{ $employee->Profile->namelfm() }}" required disabled />
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="position">Position</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="position" name="position" value="{{ $employee->Profile->EmploymentDetails->Position->title }}" required disabled />
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="tin_no">TIN No.</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="tin_no" name="tin_no" value="{{ $employee->Profile->Compensation->tin }}" disabled />
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Effective Date</label>
                                                    <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                        <input type="text" class="form-control" name="effective_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y', strtotime($employee->PayrollCycle->payroll_date)) }}" required>
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="company_code">Company Code</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="company_code" name="company_code" value="{{ $employee->Profile->CostCenter->Company->company_code }}" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="payroll_group">Payroll Group</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="payroll_group" name="payroll_group" value="{{ $employee->PayrollCycle->PayrollGroup->group_name }}" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="cost_center">Cost Center</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="cost_center" name="cost_center" value="{{ $employee->Profile->CostCenter->cost_center }}" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="rate_type">Rate Type</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="rate_type" name="rate_type" value="{{ $employee->Profile->Compensation->rate_type }}" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Hire Date</label>
                                                    <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                        <input type="text" class="form-control" name="hire_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y', strtotime($employee->Profile->EmploymentDetails->hire_date)) }}" required>
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <br />
                                                </div>
                                            </div>

                                            <br />
                                            <br />
                                            <br />

                                            <div class="form-group">
                                                <label class="col-xs-12" for="monthly_rate">Monthly Rate</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="monthly_rate" name="monthly_rate" value="{{ number_format($employee->Profile->Compensation->daily_rate * 26, 2) }}" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="daily_rate">Daily Rate</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="daily_rate" name="daily_rate" value="{{ number_format($employee->Profile->Compensation->daily_rate, 2) }}" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="hourly_rate">Hourly Rate</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="hourly_rate" name="hourly_rate" value="{{ number_format($employee->Profile->Compensation->daily_rate / 8, 2) }}" required disabled/>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="col-xs-12">
                                            <h3>Final Pay Computation</h3>
                                            <table id="final_pay_computation" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Particulars</th>
                                                    <th>Collectible</th>
                                                    <th>Payable</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <tr>

                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-xs-12">
                                            <h3>Summary of 13 Month Pay</h3>
                                            <table id="summary" class="table">
                                                <thead>
                                                <tr>
                                                    <th>Payroll Code</th>
                                                    <th>Payroll Period</th>
                                                    <th>Basic Pay</th>
                                                    <th>Pro-Rated</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label>Remarks</label>
                                                <textarea class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <table class="table">
                                                <thead>
                                                    <th></th>
                                                    <th></th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Prepared By:</td>
                                                        <td>
                                                            {{ $employee->Profile->namelfm() }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Noted By: </td>
                                                        <td>
                                                            <select name="noted_by_id">
                                                                @foreach(\App\Signatories::all() as $signatory)
                                                                    <option value="">{{ $signatory->User->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Approved By: </td>
                                                        <td>
                                                            <select name="approved_by_id">
                                                                @foreach(\App\Signatories::all() as $signatory)
                                                                    <option value="">{{ $signatory->User->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-6 text-right">
                                            <br />
                                            <br />
                                            <br />
                                            <br /><br /><br />
                                            <a href="{{ \Illuminate\Support\Facades\URL::previous() }}"class="btn btn-primary">CANCEL</a>
                                            <button class="btn btn-success">SUBMIT</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            // $('#final_pay_computation').dataTable();
        })
    </script>
@endsection

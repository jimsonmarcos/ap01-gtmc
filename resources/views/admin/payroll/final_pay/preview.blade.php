@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('final_pay') }}">Final Pay</a></li>
    </ol>
    <style>
        #finalPayForm p {
            margin-bottom: 10px;
        }

        @media print {
            .btn-print {
                display: none;
            }
        }
    </style>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row" id="print-wrapper">
                                <br />

                                <h3 class="text-center">Final Pay Details</h3>
                                <br>
                                <p class="text-center"><strong>{{ \App\Keyval::where('key', 'account_name')->first()->value }}</strong></p>
                                <br>

                                <form action="" id="finalPayForm" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="col-md-12">

                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="gtmc_id_no">GTMC ID NO.</label>
                                                <div class="col-xs-12">
                                                    <p>{{ $finalPay->Profile->id_number }}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="employee_name">Employee Name</label>
                                                <div class="col-xs-12">
                                                    <p>{{ $finalPay->Profile->namelfm() }}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="position">Position</label>
                                                <div class="col-xs-12">
                                                    <p>{{ $finalPay->Profile->EmploymentDetails->Position->title }}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="tin_no">TIN No.</label>
                                                <div class="col-xs-12">
                                                    <p>{{ $finalPay->Profile->Compensation->tin }}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Effective Date</label>
                                                    <p>{{ date('m/d/Y', strtotime($finalPay->effective_date)) }}</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="company_code">Company Code</label>
                                                <div class="col-xs-12">
                                                    <p>{{ $finalPay->Profile->CostCenter->Company->company_code }}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="payroll_group">Payroll Group</label>
                                                <div class="col-xs-12">
                                                    <p>{{ $finalPay->Profile->Compensation->PayrollGroup->group_name }}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="cost_center">Cost Center</label>
                                                <div class="col-xs-12">
                                                    <p>{{ $finalPay->Profile->CostCenter->cost_center }}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="rate_type">Rate Type</label>
                                                <div class="col-xs-12">
                                                    <p>{{ $finalPay->Profile->Compensation->rate_type }}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Hire Date</label>
                                                    <p>{{ date('m/d/Y', strtotime($finalPay->Profile->EmploymentDetails->hire_date)) }}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="monthly_rate">Monthly Rate</label>
                                                <div class="col-xs-12">
                                                    <p>{{ number_format($finalPay->Profile->Compensation->daily_rate * 26, 2) }}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="daily_rate">Daily Rate</label>
                                                <div class="col-xs-12">
                                                    <p>{{ number_format($finalPay->Profile->Compensation->daily_rate, 2) }}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="hourly_rate">Hourly Rate</label>
                                                <div class="col-xs-12">
                                                    <p>{{ number_format($finalPay->Profile->Compensation->daily_rate / 8, 2) }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="col-xs-12">
                                            <h3>Final Pay Computation</h3>
                                            <table id="final_pay_computation" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Particulars</th>
                                                    <th width="30%">Collectible</th>
                                                    <th width="30%">Payable</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Salary</td>
                                                    <td>{{ $finalPay->salary }}</td>
                                                    <td></td>
                                                </tr>

                                                <tr>
                                                    <td>Cash Savings</td>
                                                    <td>{{ number_format($finalPay->cash_savings, 2) }}</td>
                                                    <td></td>
                                                </tr>
                                                @if(!empty($finalPay->ExistingLoans))
                                                    @foreach($finalPay->ExistingLoans as $unpaidLoan)
                                                        <tr>
                                                            <td>
                                                                @if(!empty($unpaidLoan->Loan->type))
                                                                    {{ $unpaidLoan->Loan->type }} Loan
                                                                @else
                                                                    {{ $unpaidLoan->Loan->LoanType->type }}
                                                                @endif
                                                            </td>
                                                            <td></td>
                                                            <td>{{ number_format($unpaidLoan->amount, 2) }}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif

                                                <tr>
                                                    <td>Client Charges</td>
                                                    <td></td>
                                                    <td>{{ $finalPay->client_charges }}</td>
                                                </tr>

                                                @if(!empty($finalPay->OtherCharges))
                                                    @foreach($finalPay->OtherCharges as $otherCharges)
                                                        <tr>
                                                            <td>{{ $otherCharges->particular }}</td>
                                                            <td></td>
                                                            <td>{{ $otherCharges->payable }}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>

                                            {{--<button id="addOtherCharges" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Other Charges</button>--}}
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <br>
                                        <br>
                                        <div class="col-xs-12">
                                            <h3>Summary of 13 Month Pay</h3>
                                            <table id="summary" class="table">
                                                <thead>
                                                <tr>
                                                    <th>Payroll Code</th>
                                                    <th>Payroll Period</th>
                                                    <th>Basic Pay</th>
                                                    <th>Pro-Rated</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($finalPay->Summaries as $index => $payroll)
                                                    <tr>
                                                        <td>{{ $payroll->payroll_code }}</td>
                                                        <td>{{ $payroll->payroll_period }}</td>
                                                        <td>{{ number_format($payroll->basic_pay, 2) }}</td>
                                                        <td>{{ number_format($payroll->pro_rated, 2) }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-right" colspan="3">Total</th>
                                                        <th>{{ number_format($finalPay->Summaries->sum('pro_rated'), 2) }}</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label>Remarks</label>
                                                <p>{{ $finalPay->remarks }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <table class="table">
                                                <thead>
                                                    <th></th>
                                                    <th></th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Prepared By:</td>
                                                        <td>
                                                            {{ $finalPay->CreatedBy->name }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Noted By: </td>
                                                        <td>
                                                            {{ $finalPay->NotedBy->name }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Approved By: </td>
                                                        <td>
                                                            {{ $finalPay->ApprovedBy->name }}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-6 text-right">
                                            <br />
                                            <br />
                                            <br />
                                            <br /><br /><br />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            // $('#final_pay_computation').dataTable();
            var x = 0;


            $(document).on('click', '#addOtherCharges', function(e) {
                e.preventDefault();
                var template = $("<tr>" +
                    "<td><input type='text' class='form-control' name='other_charges["+ x +"][particular]' /></td>" +
                    "<td></td>" +
                    "<td><input type='text' class='form-control' name='other_charges["+ x +"][payable]' /></td>" +
                    "</tr>");

                template.appendTo($("#final_pay_computation").find('tbody'));
                // $("#final_pay_computation").append(template);


                x++;
            });

            $(".btn-print").click(function(e) {
                e.preventDefault();


            });


        })
    </script>
@endsection

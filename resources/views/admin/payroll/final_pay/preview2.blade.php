@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="">Final Pay</a></li>
        <li><a class="link-effect" href="">Preview</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-12">
                                    <div class="col-md-4 col-md-offset-5">
                                        <h3>FINAL PAY PREVIEW</h3>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <button class="text-right btn btn-primary">PRINT</button>
                                    </div>
                                </div>
                                    <div class="col-md-12">

                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="gtmc_id_no">GTMC ID NO.</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="gtmc_id_no" name="gtmc_id_no" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="employee_name">Employee Name</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="employee_name" name="employee_name" required disabled />
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="position">Position</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="position" name="position" required disabled />
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="tin_no">TIN No.</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="tin_no" name="tin_no" required />
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Effective Date</label>
                                                    <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                        <input type="text" class="form-control" name="effective_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y') }}" required>
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="col-xs-12" for="company_code">Company Code.</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="company_code" name="company_code" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="payroll_group">Payroll Group</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="payroll_group" name="payroll_group" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="cost_center">Cost Center</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="cost_center" name="cost_center" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="rate_type">Rate Type</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="rate_type" name="rate_type" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="form-control-label">Hire Date</label>
                                                    <div id="joinDate" class="input-group btn-mmddyyyy ">
                                                        <input type="text" class="form-control" name="hire_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y') }}" required>
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <br />
                                                </div>
                                            </div>

                                            <br />
                                            <br />
                                            <br />

                                            <div class="form-group">
                                                <label class="col-xs-12" for="monthly_rate">Monthly Rate</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="monthly_rate" name="monthly_rate" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="daily_rate">Daily Rate</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="daily_rate" name="daily_rate" required disabled/>
                                                    <br />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-xs-12" for="hourly_rate">Hourly Rate</label>
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" id="hourly_rate" name="hourly_rate" required disabled/>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <h3>Final Pay Computation</h3>
                                        <table id="final_pay_computation" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Particulars</th>
                                                <th>Collectible</th>
                                                <th>Payable</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <tr>

                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-12">
                                        <h3>Summary of 13 Month Pay</h3>
                                        <table id="summary" class="table">
                                            <thead>
                                            <tr>
                                                <th>Payroll Code</th>
                                                <th>Payroll Period</th>
                                                <th>Basic Pay</th>
                                                <th>Pro-Rated</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-xs-12">Remarks</label>
                                            <textarea class="form-control"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-center">
                                        <div class="col-md-4">
                                            <h3>Prepared by: </h3>
                                            <h6>JUDYlYN D. TOLENTINO</h6>
                                        </div>
                                        <div class="col-md-4">
                                            <h3>Date Created: </h3>
                                            <h6>10/09/2017</h6>
                                        </div>
                                        <div class="col-md-4">
                                            <h3>Noted By: </h3>
                                            <h6>LUZ D. ZAFRA</h6>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            // $('#final_pay_computation').dataTable();
        })
    </script>
@endsection

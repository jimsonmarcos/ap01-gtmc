@extends('layouts.app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li>Payroll</li>
        <li><a class="link-effect" href="{{ route('final_pay') }}">Final Pay</a></li>
    </ol>
    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <form action="" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="col-md-12">

                                        <div class="row">

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="employee_name">Employee Name</label>
                                                    <div><strong>{{ $finalPay->User->Profile->name() }}</strong></div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="employee_id">ID Number</label>
                                                    <div><strong>{{ $finalPay->User->Profile->id_number }}</strong></div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="cost_center">Cost Center</label>
                                                    <div><strong>{{ $finalPay->User->Profile->CostCenter->cost_center }}</strong></div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <hr />
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Payment Method</label>
                                                    <select name="transfer_mode_id" id="transfer_mode_id" class="form-control">
                                                        @foreach(\App\TransferModes::where('transfer_mode', '!=', 'BANK DEPOSIT')->groupBy('transfer_mode')->orderBy('transfer_mode')->get() as $transferMode)
                                                            <option value="{{ $transferMode->id }}">{{ $transferMode->transfer_mode }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <div class="">
                                                        <label class="form-control-label">Released Date</label>
                                                        <div id="joinDate" class="input-group btn-mmddyyyy">
                                                            <input type="text" class="form-control" name="released_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y') }}" required>
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            {{--<div class="col-md-3">--}}
                                            {{--<div class="form-group">--}}
                                            {{--<label for="">Bank Account</label>--}}
                                            {{--<select name="bank_id" id="bank" class="form-control" disabled>--}}
                                            {{--@foreach(\App\Banks::orderBy('bank_account')->get() as $bank)--}}
                                            {{--<option value="{{ $bank->id }}" data-desc="{{ $bank->chart_of_account }}">{{ $bank->bank_account }}</option>--}}
                                            {{--@endforeach--}}
                                            {{--</select>--}}
                                            {{--<div class="help-block" id="bankDescription" style="color: #70b9eb;"><i class="fa fa-info-circle"></i> <span>{{ \App\Banks::orderBy('bank_account')->first()->chart_of_account }}</span></div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Reference Number</label>
                                                    <input type="text" class="form-control" name="reference_number" id="reference_number" maxlength="20" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                </div>
                                            </div>
                                            <div class="col-md-12"></div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Remarks</label>
                                                    <textarea name="disbursed_remarks" id="" cols="" rows="3"
                                                              class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 text-right">
                                            <br />
                                            <br />
                                            <br />
                                            <br /><br /><br />
                                            <a href="{{ url("admin/payroll/final_pay/{$finalPay->status}") }}"class="btn btn-primary">CANCEL</a>
                                            <button class="btn btn-success"><i class="fa fa-save"></i> DISBURSED</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(document).on('change', '#payment_method', function(e) {
                if($(this).find(':selected').text() == 'DEPOSIT') {
                    $("#bank").removeAttr('disabled');
                    $("#reference_number").val('').removeAttr('disabled').attr('required', 'required');
                } else {
                    $("#bank").attr('disabled', 'disabled');
                    $("#reference_number").val('').attr('disabled', 'disabled').removeAttr('required');
                }
            });

            $(document).on('change', "#bank", function() {
                var desc = $(this).find('option:selected').attr('data-desc');
                $("#bankDescription > span").text(desc);
            });

            $("#transfer_mode_id").change(function() {
                if($(this).find(':selected').text() == 'HAND OVER' || $(this).find(':selected').text() == 'PICK UP') {
                    $("#reference_number").val('').attr('disabled', 'disabled').removeAttr('required');
                } else {
                    $("#reference_number").val('').removeAttr('disabled').attr('required', 'required');
                }
            });
        });
    </script>
@endsection

@extends('layouts.app')

@section('content')


            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <form action="" class="form" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h1>Employment Details</h1>
                                                <hr>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Principal</label>
                                                    <p>{{ \App\Companies::find($profile['principal_id'])->company_code }}</p>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Join Date</label>
                                                    <p>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $profile['birthday'])->format('m/d/Y') }}</p>
                                                </div>
                                            </div>

                                            <div class="col-md-12"></div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Position</label>
                                                    <p>{{ \App\Positions::find($profile['position_id'])->title }}</p>
                                                    @if($profile['position_id'] == 1)
                                                    <div>
                                                        <input id="option" type="checkbox" value="Y" name="orientation">
                                                        <label for="option">Orientation</label>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                            @if($profile['position_id'] == 1)
                                            <div class="col-md-12"></div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Trainee Start Date</label>
                                                    <div class='input-group btn-ddmmyyyy'>
                                                        <input type='text' class="form-control" name="trainee_start_date" value="{{ date('d/m/Y') }}" placeholder="dd/mm/yyyy" required />
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Trainee End Date</label>
                                                    <div class='input-group btn-ddmmyyyy'>
                                                        <input type='text' class="form-control" name="trainee_end_date" value="{{ date('d/m/Y') }}" placeholder="dd/mm/yyyy" required />
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif

{{--                                            @if($profile['position_id'] == 1 || $profile['position_id'] == 2)--}}

                                            <div class="col-md-12"></div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">City Province</label>
                                                    <select id="add-ed-cityprovince" class="form-control" name="province_id" required>
                                                        <option value="">&nbsp;</option>
                                                        @foreach($provinces as $province)
                                                            <option value="{{ $province->id }}" data-areas='{{ $province->Areas->sortBy('area')->toJson() }}'>{{ $province->province }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Area</label>
                                                    <select id="add-ed-area" class="form-control" name="area_id" required>
                                                        {{--<option value="">&nbsp;</option>--}}
                                                        {{--@foreach($areas as $area)--}}
                                                            {{--<option value="{{ $outlet->area }}">{{ $area->area }}</option>--}}
                                                        {{--@endforeach--}}
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Mall</label>
                                                    <select id="add-ed-mall" class="form-control" name="mall_id">
                                                        <option value="">&nbsp;</option>
                                                        @foreach($malls as $mall)
                                                            <option value="{{ $mall->id }}">{{ $mall->mall }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Outlet</label>
                                                    <select id="add-ed-outlet" class="form-control" name="outlet_id">
                                                        <option value="">&nbsp;</option>
                                                        @foreach($outlets as $outlet)
                                                            <option value="{{ $outlet->id }}">{{ $outlet->outlet }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            {{--<div class="col-md-4">--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label class="form-control-label">Department</label>--}}
                                                    {{--<select id="add-ed-department" class="form-control" name="department_id">--}}
                                                        {{--@foreach($departments as $department)--}}
                                                            {{--<option value="{{ $department->id }}">{{ $department->department }}</option>--}}
                                                        {{--@endforeach--}}
                                                    {{--</select>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}

                                            {{--@endif--}}
                                            {{--&& $profile['principal_id'] == 8--}}
                                            {{--{{ dd($profile) }}--}}
                                            @if(in_array($profile['position_id'], [1, 2]))
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Coordinator</label>
                                                    <select class="form-control" name="coordinator_id" required>
                                                        @if($coordinators->count() == 0)
                                                            <option value="0">N/A</option>
                                                        @else
                                                            <option value="0">Not Assigned</option>
                                                            @foreach($coordinators as $coordinator)
                                                                <option value="{{ $coordinator->user_id }}">{{ $coordinator->Profile->name() }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            @endif

                                        </div>

                                        <div class="col-md-12 text-right">
                                            <br><br>
                                            <button href="#" class="btn btn-primary">Next <span class="fa fa-chevron-right" style="font-size: 0.8em;"></span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $(document).on('change', '#add-ed-cityprovince', function(e) {
                var areas = $.parseJSON($(this).find(":selected").attr('data-areas'));
//                console.log(areas);

                $("#add-ed-area").find('option').remove();
//                $("#add-ed-area").append("<option value=''>&nbsp;</option>")
//                if(areas.length > 0) {
                    $.each(areas, function(index, json) {
                        console.log(json);
                        $("#add-ed-area").append("<option value='"+ json.id +"'>"+ json.area +"</option>");
                    });
//                }
            });


            {{--$(document).on('change', '#add-ed-cityprovince', function(e) {--}}
                {{--e.preventDefault();--}}

                {{--$("#add-ed-area, #add-ed-mall, #add-ed-outlet, #add-ed-department").find('option').remove();--}}
                {{--$("#add-ed-area, #add-ed-mall, #add-ed-outlet, #add-ed-department").attr('disabled', 'disabled');--}}
                {{--$("#add-ed-area").append("<option>Loading..</option>");--}}

                {{--var data = {--}}
                    {{--_token : '{{ csrf_token() }}',--}}
                    {{--province : $(this).find(':selected').text()--}}
                {{--};--}}
{{--//               console.log($(this).find(':selected').text());--}}
                {{--$.post('{{ url("api/department/areas") }}', data, function(response) {--}}
{{--//                  console.log(response);--}}
                    {{--var areas = $.parseJSON(response);--}}
                    {{--var option = '';--}}
                    {{--$.each(areas, function(index, json) {--}}
                        {{--var v = json.area;--}}
                        {{--if(json.area.length == 0) {--}}
                            {{--v = 'N/A';--}}
                        {{--}--}}
                        {{--option = option + '<option value="'+ json.area +'">'+ v +'</option>';--}}
                    {{--});--}}

                    {{--$("#add-ed-area > option").remove();--}}
                    {{--$("#add-ed-area").append(option);--}}
                    {{--$("#add-ed-area").removeAttr('disabled');--}}

                    {{--setTimeout(function() {--}}
                        {{--reloadDepartments();--}}
                    {{--}, 300);--}}
                {{--});--}}
            {{--});--}}

            {{--$(document).on('change', '#add-ed-area', function(e) {--}}
                {{--e.preventDefault();--}}

                {{--$("#add-ed-mall, #add-ed-outlet, #add-ed-department").find('option').remove();--}}
                {{--$("#add-ed-mall, #add-ed-outlet, #add-ed-department").attr('disabled', 'disabled');--}}
                {{--$("#add-ed-mall").append("<option>Loading..</option>");--}}

                {{--var data = {--}}
                    {{--_token : '{{ csrf_token() }}',--}}
                    {{--province : $('#add-ed-cityprovince').find(':selected').text(),--}}
                    {{--area : $(this).find(':selected').attr('value')--}}
                {{--};--}}

                {{--console.log(data);--}}

                {{--$.post('{{ url("api/department/malls") }}', data, function(response) {--}}

                    {{--var malls = $.parseJSON(response);--}}
                    {{--var option = '';--}}
                    {{--$.each(malls, function(index, json) {--}}
                        {{--var v = json.mall;--}}
                        {{--if(json.mall.length == 0) {--}}
                            {{--v = 'N/A';--}}
                        {{--}--}}
                        {{--option = option + '<option value="'+ json.mall +'">'+ v +'</option>';--}}
                    {{--});--}}

                    {{--$("#add-ed-mall > option").remove();--}}
                    {{--$("#add-ed-mall").append(option);--}}
                    {{--$("#add-ed-mall").removeAttr('disabled');--}}

                    {{--setTimeout(function() {--}}
                        {{--reloadDepartments();--}}
                    {{--}, 300);--}}
                {{--});--}}
            {{--});--}}

            {{--$(document).on('change', '#add-ed-outlet', function(e) {--}}
                {{--e.preventDefault();--}}

                {{--$("#add-ed-outlet, #add-ed-department").find('option').remove();--}}
                {{--$("#add-ed-outlet, #add-ed-department").attr('disabled', 'disabled');--}}
                {{--$("#add-ed-outlet").append("<option>Loading..</option>");--}}

                {{--var data = {--}}
                    {{--_token : '{{ csrf_token() }}',--}}
                    {{--province : $('#add-ed-cityprovince').find(':selected').text(),--}}
                    {{--area : $('#add-ed-area').find(':selected').attr('value'),--}}
                    {{--mall : $(this).find(':selected').attr('value')--}}
                {{--};--}}

                {{--$.post('{{ url("api/department/outlets") }}', data, function(response) {--}}

                    {{--var outlets = $.parseJSON(response);--}}
                    {{--var option = '';--}}
                    {{--$.each(outlets, function(index, json) {--}}
                        {{--var v = json.outlet;--}}
                        {{--if(json.outlet.length == 0) {--}}
                            {{--v = 'N/A';--}}
                        {{--}--}}
                        {{--option = option + '<option value="'+ json.outlet +'">'+ v +'</option>';--}}
                    {{--});--}}

                    {{--$("#add-ed-outlet > option").remove();--}}
                    {{--$("#add-ed-outlet").append(option);--}}
                    {{--$("#add-ed-outlet").removeAttr('disabled');--}}

                    {{--setTimeout(function() {--}}
                        {{--reloadDepartments();--}}
                    {{--}, 300);--}}
                {{--});--}}
            {{--});--}}

            {{--$(document).on('change', '#add-ed-department', function(e) {--}}
                {{--e.preventDefault();--}}

                {{--$("#add-ed-department").find('option').remove();--}}
                {{--$("#add-ed-department").attr('disabled', 'disabled');--}}
                {{--$("#add-ed-department").append("<option>Loading..</option>");--}}

                {{--var data = {--}}
                    {{--_token : '{{ csrf_token() }}',--}}
                    {{--province : $('#add-ed-cityprovince').find(':selected').text(),--}}
                    {{--area : $('#add-ed-area').find(':selected').attr('value'),--}}
                    {{--mall : $('#add-ed-mall').find(':selected').attr('value'),--}}
                    {{--outlet : $(this).find(':selected').attr('value')--}}
                {{--};--}}

                {{--$.post('{{ url("api/department/departments") }}', data, function(response) {--}}

                    {{--var departments = $.parseJSON(response);--}}
                    {{--var option = '';--}}
                    {{--$.each(departments, function(index, json) {--}}
                        {{--option = option + '<option value="'+ json.id +'">'+ json.department +'</option>';--}}
                    {{--});--}}

                    {{--$("#add-ed-department > option").remove();--}}
                    {{--$("#add-ed-department").append(option);--}}
                    {{--$("#add-ed-department").removeAttr('disabled');--}}
                {{--});--}}
            {{--});--}}

            {{--function reloadDepartments() {--}}
                {{--if($("#add-ed-area option").length > 0 && $("#add-ed-mall option").length == 0) {--}}

                    {{--if($('#add-ed-area').find(':selected').text() != 'Loading..') {--}}
                        {{--$("#add-ed-mall, #add-ed-outlet, #add-ed-department").find('option').remove();--}}
                        {{--$("#add-ed-mall, #add-ed-outlet, #add-ed-department").attr('disabled', 'disabled');--}}
                        {{--$("#add-ed-mall").append("<option>Loading..</option>");--}}

                        {{--var data = {--}}
                            {{--_token : '{{ csrf_token() }}',--}}
                            {{--province : $('#add-ed-cityprovince').find(':selected').text(),--}}
                            {{--area : $("#add-ed-area").find(':selected').attr('value')--}}
                        {{--};--}}
                        {{--$.post('{{ url("api/department/malls") }}', data, function(response) {--}}

                            {{--var malls = $.parseJSON(response);--}}
                            {{--var option = '';--}}
                            {{--$.each(malls, function(index, json) {--}}
                                {{--var v = json.mall;--}}
                                {{--if(json.mall.length == 0) {--}}
                                    {{--v = 'N/A';--}}
                                {{--}--}}
                                {{--option = option + '<option value="'+ json.mall +'">'+ v +'</option>';--}}
                            {{--});--}}

                            {{--$("#add-ed-mall > option").remove();--}}
                            {{--$("#add-ed-mall").append(option);--}}
                            {{--$("#add-ed-mall").removeAttr('disabled');--}}

                            {{--setTimeout(function() {--}}
                                {{--reloadDepartments();--}}
                            {{--}, 300);--}}

                        {{--});--}}
                    {{--}--}}


                {{--}--}}

                {{--if($("#add-ed-mall option").length > 0 && $("#add-ed-outlet option").length == 0) {--}}

                    {{--if($('#add-ed-mall').find(':selected').text() != 'Loading..') {--}}

                        {{--$("#add-ed-outlet, #add-ed-department").find('option').remove();--}}
                        {{--$("#add-ed-outlet, #add-ed-department").attr('disabled', 'disabled');--}}
                        {{--$("#add-ed-outlet").append("<option>Loading..</option>");--}}

                        {{--var data = {--}}
                            {{--_token : '{{ csrf_token() }}',--}}
                            {{--province : $('#add-ed-cityprovince').find(':selected').text(),--}}
                            {{--area : $('#add-ed-area').find(':selected').attr('value'),--}}
                            {{--mall : $('#add-ed-mall').find(':selected').attr('value')--}}
                        {{--};--}}
                        {{--$.post('{{ url("api/department/outlets") }}', data, function(response) {--}}

                            {{--var outlets = $.parseJSON(response);--}}
                            {{--var option = '';--}}
                            {{--$.each(outlets, function(index, json) {--}}
                                {{--var v = json.outlet;--}}
                                {{--if(json.outlet.length == 0) {--}}
                                    {{--v = 'N/A';--}}
                                {{--}--}}
                                {{--option = option + '<option value="'+ json.outlet +'">'+ v +'</option>';--}}
                            {{--});--}}

                            {{--$("#add-ed-outlet > option").remove();--}}
                            {{--$("#add-ed-outlet").append(option);--}}
                            {{--$("#add-ed-outlet").removeAttr('disabled');--}}

                            {{--setTimeout(function() {--}}
                                {{--reloadDepartments();--}}
                            {{--}, 300);--}}
                        {{--});--}}
                    {{--}--}}

                {{--}--}}

                {{--if($("#add-ed-outlet option").length > 0 && $("#add-ed-department option").length == 0) {--}}

                    {{--if($('#add-ed-outlet').find(':selected').text() != 'Loading..') {--}}
                        {{--$("#add-ed-department").find('option').remove();--}}
                        {{--$("#add-ed-department").attr('disabled', 'disabled');--}}
                        {{--$("#add-ed-department").append("<option>Loading..</option>");--}}

                        {{--var data = {--}}
                            {{--_token : '{{ csrf_token() }}',--}}
                            {{--province : $('#add-ed-cityprovince').find(':selected').text(),--}}
                            {{--area : $('#add-ed-area').find(':selected').attr('value'),--}}
                            {{--mall : $('#add-ed-mall').find(':selected').attr('value'),--}}
                            {{--outlet : $('#add-ed-outlet').find(':selected').attr('value')--}}
                        {{--};--}}
                        {{--$.post('{{ url("api/department/departments") }}', data, function(response) {--}}

                            {{--var departments = $.parseJSON(response);--}}
                            {{--var option = '';--}}
                            {{--$.each(departments, function(index, json) {--}}
                                {{--option = option + '<option value="'+ json.id +'">'+ json.department +'</option>';--}}
                            {{--});--}}

                            {{--$("#add-ed-department > option").remove();--}}
                            {{--$("#add-ed-department").append(option);--}}
                            {{--$("#add-ed-department").removeAttr('disabled');--}}
                        {{--});--}}
                    {{--}--}}
                {{--}--}}
            {{--}--}}
        })
    </script>
@endsection

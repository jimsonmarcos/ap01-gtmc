@extends('layouts.app')

@section('content')


            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <form action="" class="form" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h1>Compensation</h1>
                                                <hr>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Rate Type</label>
                                                    <select class="form-control" name="rate_type" id="rateType">
                                                        <option value="Daily">Daily</option>
                                                        <option value="Monthly">Monthly</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Daily Rate Category</label>
                                                    <select class="form-control" name="daily_category" id="dailyRateCategory">
                                                        <option value=""></option>
                                                        <option value="Fixed" selected>Fixed</option>
                                                        <option value="Location">Location</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Location</label>
                                                    <select id="locationRates" class="form-control" name="daily_rate_id" disabled>
                                                        <option value=""></option>
                                                        @foreach(\App\DailyRates::all()->sortBy('location') as $location)
                                                            <option value="{{ $location->id }}" data-rate="{{ $location->rate }}">{{ $location->location }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Monthly Rate</label>
                                                    <input id="monthlyRate" type="text" name="monthly_rate" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" readonly>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Daily Rate</label>
                                                    <input id="dailyRate" type="text" name="daily_rate" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Hourly Rate</label>
                                                    <input id="hourlyRate" type="text" name="hourly" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" readonly>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label">Mode of Transfer</label>
                                                    <select id="comp-transfermode" name="" class="form-control">
                                                        @foreach($transferModes as $transferMode)
                                                            <option>{{ $transferMode->transfer_mode }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="form-control-label">Transfer Type</label>
                                                    <select name="" id="comp-transfertype" class="form-control">
                                                        @foreach($transferTypes as $transferType)
                                                            <option>{{ $transferType->transfer_type }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Bank</label>
                                                    <select class="form-control" name="transfer_mode_id" id="comp-bank">
                                                        @foreach($banks as $bank)
                                                            <option value="{{ $bank->id }}">{{ $bank->bank }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label">Mobile Number</label>
                                                    <input id="comp-mobilenumber" type="text" name="mobile_number" class="form-control" maxlength="11" onkeypress='return event.charCode >= 48 && event.charCode <= 57' readonly>
                                                </div>
                                            </div>

                                            <div class="col-md-12"></div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label">Account Number</label>
                                                    <input type="text" id="accountNumber" name="account_number" class="form-control" maxlength="25" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <h1 class="text-center">Payroll Group</h1>
                                                <hr>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Payroll Group</label>
                                                    <p>{{ \App\PayrollGroups::find(session('user.profile')['payroll_group_id'])->group_name }}</p>
                                                </div>
                                            </div>

                                            <div class="col-md-12"></div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">SSS Number</label>
                                                    <input id="sss_number" type="text" name="sss" class="form-control" placeholder="(optional)" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                    <div>
                                                        <label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                                                            <input type="checkbox" name="compute_sss" value="Y" disabled><span></span> <strong>Compute SSS</strong>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">HDMF Number</label>
                                                    <input id="hdmf_number" type="text" name="hdmf" class="form-control" placeholder="(optional)" maxlength="12">
                                                    <div>
                                                        <label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                                                            <input type="checkbox" name="compute_hdmf" value="Y" disabled><span></span> <strong>Compute HDMF</strong>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12"></div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">PhilHealth Number</label>
                                                    <input id="philhealth_number" type="text" name="philhealth" class="form-control" placeholder="(optional)" maxlength="12" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                    <div>
                                                        <label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                                                            <input type="checkbox" name="compute_philhealth" value="Y" disabled><span></span> <strong>Compute PhilHealth</strong>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">TIN Number</label>
                                                    <input type="text" name="tin" class="form-control" placeholder="(optional)" maxlength="12" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                </div>
                                            </div>

                                            <div class="col-md-12"></div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Tax Type</label>
                                                    <select id="taxTypes" class="form-control" name="tax_type_id" >
                                                        @if(\App\CivilStatus::find($profile['civil_status_id'])->title == 'SINGLE')
                                                            @foreach(\App\TaxTypes::whereIn('id', [1,2,3,4,5])->orderBy('title')->get() as $taxType)
                                                                <option value="{{ $taxType->id }}">{{ $taxType->title }}</option>
                                                            @endforeach
                                                        @else
                                                            @foreach(\App\TaxTypes::whereIn('id', [6,7,8,9,10])->orderBy('title')->get() as $taxType)
                                                                <option value="{{ $taxType->id }}">{{ $taxType->title }}</option>
                                                            @endforeach
                                                        @endif

                                                    </select>
                                                    <div>
                                                        <label class="css-input css-radio css-radio-primary push-10-r">
                                                            <input type="radio" class="isTaxable" name="deduct_withholding" value="N" data-taxable="N" checked><span></span> Minimum Wage Earner
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <label class="css-input css-radio css-radio-primary push-10-r">
                                                            <input type="radio" class="isTaxable" name="deduct_withholding" value="Y" data-taxable="Y"><span></span> Deduct Withholding Tax
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="col-md-12 text-right">
                                            <br><br>
                                            <button href="#" class="btn btn-success"><span class="fa fa-save" style="font-size: 0.8em;"></span> Complete Registration</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
//            $(".isTaxable").change(function() {
//                if($(this).data('taxable').toString() == 'Y') {
//                    $("#taxTypes").removeAttr('readonly').attr('required', 'required').find('option').first().removeAttr('selected');
//                } else {
//                    $("#taxTypes").removeAttr('required').attr('readonly', 'readonly').find('option').first().attr('selected', 'true');
//                }
//            });

            $(document).on('keyup change', "#sss_number, #hdmf_number, #philhealth_number", function(e) {
                if($.trim($(this).val()).length > 0) {
                    if($(this).closest('div.form-group').find('input[type=checkbox]').is(':disabled') == true) {
                        $(this).closest('div.form-group').find('input[type=checkbox]').removeAttr('disabled');
                    }
                } else {
                    $(this).closest('div.form-group').find('input[type=checkbox]').attr({disabled: true, checked: false});
                }
            });

            $("#rateType").change(function() {
                if($(this).val() == 'Daily') {
                    $("#dailyRateCategory").removeAttr('readonly').removeAttr('disabled').attr('required', 'required').find('option').first().removeAttr('selected');
                    $("#dailyRate").removeAttr('readonly');
                    $("#hourlyRate").attr({ readonly: true});
                    $("#monthlyRate, #hourlyRate").attr({ readonly: true});
                    $("#monthlyRate, #dailyRate, #hourlyRate").val('');
                } else {
                    $("#locationRates").removeAttr('required').attr('disabled', 'disabled').find('option').first().attr('selected', 'true');
                    $("#dailyRateCategory").removeAttr('required').attr('readonly', 'readonly').find('option').first().attr('selected', 'true');
                    $("#monthlyRate").removeAttr('readonly');
                    $("#dailyRate, #hourlyRate").attr('readonly', 'readonly');
                    $("#monthlyRate, #dailyRate, #hourlyRate").val('');
                }
            });

            $("#dailyRateCategory").change(function() {
                if($(this).val() == 'Location') {
                    $("#dailyRate").val('').attr('readonly', 'readonly');
                    $("#hourlyRate, #monthlyRate").val('');
                    $("#locationRates").removeAttr('disabled').attr('required', 'required').find('option').removeClass('hidden').first().removeAttr('selected');
                } else {
                    $("#locationRates > option").addClass('hidden');
                    $("#dailyRate").val('').removeAttr('readonly').removeAttr('disabled');
                    $("#hourlyRate, #monthlyRate").val('');
                    $("#locationRates").removeAttr('required').attr('disabled', 'disabled').find('option').first().attr('selected', 'true');
//                    $("#locationRates").removeAttr('required').attr('disabled', 'disabled').find('option').first().attr('selected', 'true');
                }
            });

            $("#monthlyRate").on('keyup', function() {
                var dailyRate = parseFloat($(this).val()) / 26;
                $("#dailyRate").attr('value', dailyRate.toFixed(2)).val(dailyRate.toFixed(2));
                $("#hourlyRate").attr('value', (parseFloat($("#dailyRate").val()) / 8).toFixed(2)).val((parseFloat($("#dailyRate").val()) / 8).toFixed(2));

            });

            $("#locationRates").change(function() {
               if($(this).find(':selected').attr('value') != '') {
                   var location = $(this).find(':selected');
                   $("#dailyRate").attr('value', location.attr('data-rate')).val(location.attr('data-rate')).attr('readonly', true);
                   $("#hourlyRate").attr('value', parseFloat($("#dailyRate").val()) / 8).val(parseFloat($("#dailyRate").val()) / 8).attr('readonly', true);
                   $("#monthlyRate").val(parseFloat($("#dailyRate").val()) * 26).attr('readonly', true);
               } else {
                   $("#dailyRate, #hourlyRate, #monthlyRate").val('').removeAttr('value').attr('readonly', false);
                   $("#hourlyRate").attr('readonly', true);
               }
            });

            $("#dailyRate").on('keyup', function() {
                if($("#dailyRate").val() != '') {
                    $("#hourlyRate").attr('value', (parseFloat($("#dailyRate").val()) / 8).toFixed(2)).val((parseFloat($("#dailyRate").val()) / 8).toFixed(2));
                    $("#monthlyRate").val(parseFloat($("#dailyRate").val()) * 26)
                } else {
                    $("#hourlyRate, #monthlyRate").val('');
                }
            });

            $(document).on('change', '#comp-transfermode', function(e) {
                e.preventDefault();

                if($("#comp-transfermode").find(':selected').text() == 'BANK DEPOSIT' || $("#comp-transfermode").find(':selected').text() == 'BDO' || $("#comp-transfermode").find(':selected').text() == 'METROBANK' || $("#comp-transfermode").find(':selected').text() == 'UNIONBANK') {

                    if($("#comp-transfermode").find(':selected').text() == 'BANK DEPOSIT') {
                        $("#accountNumber").attr('maxlength', 25);
                    } else if($("#comp-transfermode").find(':selected').text() == 'BDO') {
                        $("#accountNumber").attr('maxlength', 16);
                    } else if($("#comp-transfermode").find(':selected').text() == 'METROBANK') {
                        $("#accountNumber").attr('maxlength', 10);
                    } else if($("#comp-transfermode").find(':selected').text() == 'UNIONBANK') {
                        $("#accountNumber").attr('maxlength', 12);
                    }

                    $("#accountNumber").val('').attr('disabled', false).attr('required', true);
                    $("#comp-transfertype, #comp-bank").attr('readonly', false);
                } else {
                    $("#accountNumber").val('').attr('disabled', true);
                    $("#comp-mobilenumber").attr('required', false);
                }

                if($("#comp-transfermode").find(':selected').text() == 'GCASH') {
                    $("#comp-mobilenumber").removeAttr('readonly').attr('required', true).attr('maxlength', 11);
                    $("#accountNumber").val('').attr('disabled', true).attr('required', false);
                    $("#comp-transfertype, #comp-bank").attr('readonly', true);
                } else  {
                    $("#comp-mobilenumber").attr('readonly', 'readonly');
                }

                if($("#comp-transfermode").find(':selected').text() == 'PICK UP') {
                    $("#comp-transfertype, #comp-bank").attr('readonly', true);
                }

//                console.log($("#comp-transfermode").find(':selected').text() + " " + $.inArray($("#comp-transfermode").find(':selected').text(), ['COURIER', 'GCASH', 'PICK UP', 'HAND OVER']));
//                if($.inArray($("#comp-transfermode").find(':selected').text(), ['COURIER', 'GCASH', 'PICK UP', 'HAND OVER']) != -1) {
//                    console.log('true');
//                    $("#accountNumber").val('').attr('disabled', true);
//                }

                $("#comp-transfertype, #comp-bank").find('option').remove();
                $("#comp-transfertype, #comp-bank").attr('readonly', 'readonly');
                $("#comp-transfertype").append("<option>Loading..</option>");

                var data = {
                    _token : '{{ csrf_token() }}',
                    transfer_mode : $(this).find(':selected').text()
                };
//               console.log($(this).find(':selected').text());
                $.post('{{ url("api/transfer-mode/types") }}', data, function(response) {
//                  console.log(response);
                    var transfer_types = $.parseJSON(response);
                    var option = '';
                    $.each(transfer_types, function(index, json) {
                        option = option + '<option>'+ json.transfer_type +'</option>';
                    });

                    $("#comp-transfertype > option").remove();
                    $("#comp-transfertype").append(option);
                    $("#comp-transfertype").removeAttr('readonly');

                    setTimeout(function() {
                        reloadTransferModes();
                    }, 300);
                });
            });

            $(document).on('change', '#comp-transfertype', function(e) {
                e.preventDefault();

                $("#comp-bank").find('option').remove();
                $("#comp-bank").attr('readonly', 'readonly');
                $("#comp-bank").append("<option>Loading..</option>");

                var data = {
                    _token : '{{ csrf_token() }}',
                    transfer_mode : $('#comp-transfermode').find(':selected').text(),
                    transfer_type : $(this).find(':selected').text()
                };

                $.post('{{ url("api/transfer-mode/banks") }}', data, function(response) {

                    var banks = $.parseJSON(response);
                    var option = '';
                    $.each(banks, function(index, json) {
                        option = option + '<option>'+ json.bank +'</option>';
                    });

                    $("#comp-bank > option").remove();
                    $("#comp-bank").append(option);
                    $("#comp-bank").removeAttr('readonly');

                    setTimeout(function() {
                        reloadTransferModes();
                    }, 300);
                });
            });

            function reloadTransferModes() {
                if ($("#comp-transfertype option").length > 0 && $("#comp-bank option").length == 0) {

                    if ($('#comp-transfertype').find(':selected').text() != 'Loading..') {
                        $("#comp-bank").find('option').remove();
                        $("#comp-bank").attr('readonly', 'readonly');
                        $("#comp-bank").append("<option>Loading..</option>");

                        var data = {
                            _token : '{{ csrf_token() }}',
                            transfer_mode : $('#comp-transfermode').find(':selected').text(),
                            transfer_type : $('#comp-transfertype').find(':selected').text()
                        };

                        $.post('{{ url("api/transfer-mode/banks") }}', data, function(response) {

                            var banks = $.parseJSON(response);
                            var option = '';
                            $.each(banks, function(index, json) {
                                option = option + '<option value="'+ json.id +'">'+ json.bank +'</option>';
                            });

                            $("#comp-bank > option").remove();
                            $("#comp-bank").append(option);
                            if($("#comp-bank").find(':checked').text() == '') {

                            } else {
                                $("#comp-bank").removeAttr('readonly');
                            }


                            setTimeout(function() {
                                reloadTransferModes();
                            }, 300);
                        });
                    }
                }
            }
        })
    </script>
@endsection

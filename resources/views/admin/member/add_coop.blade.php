@extends('layouts.app')

@section('content')

            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="card">
                                <div class="card-body">
                                    <form action="{{ url('admin/member/add/coop') }}" class="form" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h1>Cooperative</h1>
                                                <hr>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Membership Type</label>
                                                    <p><strong>{{ $profile['membership_type'] }}</strong></p>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Join Date</label>
                                                    <p><strong>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $profile['join_date'])->format('d/m/Y') }}</strong></p>
                                                </div>
                                            </div>

                                            <div class="col-md-12"></div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Principal</label>
                                                    <p>{{ \App\Companies::find($profile['principal_id'])->company_code }}</p>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Cost Center</label>
                                                    <p>{{ \App\CostCenters::find($profile['cost_center_id'])->cost_center }}</p>
                                                </div>
                                            </div>

                                            <div class="col-md-12"></div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Contribution Per PayDay</label>
                                                    <input type="text" name="coop_share" class="form-control" style="max-width: 300px;">
                                                </div>
                                            </div>
                                        </div>



                                        <div class="row">

                                            <div class="col-md-12">
                                                <h1>Beneficiaries</h1>
                                                <hr>
                                            </div>

                                            <div class="col-md-12">
                                                <table class="table table-striped table-sm" id="beneficiaries">
                                                    <thead>
                                                        <tr>
                                                            <th>First Name</th>
                                                            <th>Middle Name</th>
                                                            <th>Last Name</th>
                                                            <th>Birth Date</th>
                                                            <th>Relationship to Member</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><input type="text" name="beneficiaries[0][first_name]" class="form-control" style=""></td>
                                                            <td><input type="text" name="beneficiaries[0][middle_name]" class="form-control" style=""></td>
                                                            <td><input type="text" name="beneficiaries[0][last_name]" class="form-control" style=""></td>
                                                            <td>
                                                                <div class='input-group btn-ddmmyyyy'>
                                                                    <input type='text' class="form-control" name="beneficiaries[0][birthday]" value="{{ date('d/m/Y') }}" placeholder="dd/mm/yyyy" required />
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <select name="beneficiaries[0][member_relationship_id]" class="form-control">
                                                                    @foreach(\App\MemberRelationships::all()->sortBy('title') as $relationsip)
                                                                        <option value="{{ $relationsip->id }}">{{ $relationsip->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td style="vertical-align: middle"><a href="#" class="removeBeneficiary"> <span class="fa fa-remove"></span></a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <a href="#" id="addBeneficiary" class="btn btn-primary btn-sm pull-right"><span class="fa fa-plus"></span> ADD</a>
                                            </div>



                                            <div class="col-md-12 text-right">
                                                <br><br>
                                                <button href="#" class="btn btn-success"><span class="fa fa-save" style="font-size: 0.8em;"></span> Complete Registration</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div id="add-beneficary-template" style="display: none;">
    <table>
        <tr>
            <td><input type="text" name="beneficiaries[0][first_name]" class="form-control" style=""></td>
            <td><input type="text" name="beneficiaries[0][middle_name]" class="form-control" style=""></td>
            <td><input type="text" name="beneficiaries[0][last_name]" class="form-control" style=""></td>
            <td>
                <div class='input-group btn-ddmmyyyy'>
                    <input type='text' class="form-control" name="beneficiaries[0][birthday]" value="{{ date('d/m/Y') }}" placeholder="dd/mm/yyyy" required />
                    <span class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                    </span>
                </div>
            </td>
            <td>
                <select name="beneficiaries[0][member_relationship_id]" class="form-control">
                    @foreach(\App\MemberRelationships::all()->sortBy('title') as $relationsip)
                        <option value="{{ $relationsip->id }}">{{ $relationsip->title }}</option>
                    @endforeach
                </select>
            </td>
            <td style="vertical-align: middle"><a href="#" class="removeBeneficiary"> <span class="fa fa-remove"></span></a></td>
        </tr>
    </table>

</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            var x = 1;
            $(document).on('click', ".removeBeneficiary", function(e) {
                e.preventDefault();
                $(this).closest('tr').remove();
            });

            $(document).on('click', "#addBeneficiary", function(e) {
                e.preventDefault();
                var beneficaryTemplate = $("#add-beneficary-template tr").clone();
                beneficaryTemplate.find('input, select').each(function() {
                    var name = $(this).attr('name');
                    var name = name.replace('0', x.toString());
                    $(this).attr('name', name);
                });

                beneficaryTemplate.appendTo('#beneficiaries > tbody');

                $('.btn-ddmmyyyy').datetimepicker({
                    format: "DD/MM/YYYY",
                    //format: "Y-m-d g:i:s",
                });

                x++;
            });

        })
    </script>
@endsection

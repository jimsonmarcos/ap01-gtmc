@extends('layouts.app')

@section('content')


            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                            <div class="card">
                                <div class="card-body">
                                    <form action="" class="form" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h1>Personal Information</h1>
                                                <hr>
                                            </div>
                                            <div class="col-md-3 col-sm-4 text-center">
                                                <img id="form-user-photo" src="{{ asset('img/user-placeholder.png') }}" alt="Photo" style="max-width: 100%;max-height: 300px;">
                                                <input id="input-user-photo" class="photo-field" type="file" name="photo" style="display: none;" accept="image/*">
                                                <div style="margin-top: 5px;">
                                                    <a href="#" class="btn btn-sm btn-outline-primary btn-upload-image" data-target="input-user-photo">Upload Photo</a>
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-8">
                                                <div class="form-group">
                                                    <label class="form-control-label">ID Number</label>
                                                    <p><strong>{{ $profile['id_number'] }}</strong></p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">First Name</label>
                                                            <p><strong>{{ strtoupper($profile['first_name']) }}</strong></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Middle Name</label>
                                                            <p><strong>{{ strtoupper($profile['middle_name']) }}</strong></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Last Name</label>
                                                            <p><strong>{{ strtoupper($profile['last_name']) }}</strong></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Birth Date</label>
                                                            <div class='input-group btn-dtp-bday'>
                                                                <input type='text' class="form-control" name="birthday" value="" placeholder="mm/dd/yyyy" required />
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Gender</label>
                                                            <select name="gender" class="form-control" required>
                                                                <option value=""></option>
                                                                <option value="MALE">MALE</option>
                                                                <option value="FEMALE">FEMALE</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label">Address</label>
                                                    <input type="text" class="form-control" name="address" id="user-address" placeholder="Blk no. , Street, Barangay, City, Country">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Educational Attainment</label>
                                                            <select name="educational_attainment_id" class="form-control" required>
                                                                <option value=""></option>
                                                                @foreach(\App\EducationalAttainments::all() as $educationalAttainment)
                                                                    <option value="{{ $educationalAttainment->id }}">{{ strtoupper($educationalAttainment->title) }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Mother's Maiden Name</label>
                                                            <input type="text" class="form-control" name="mothers_maiden_name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Civil Status</label>
                                                            <select name="civil_status_id" class="form-control" required>
                                                                <option value=""></option>
                                                                @foreach(\App\CivilStatus::all() as $civilStatus)
                                                                    <option value="{{ $civilStatus->id }}">{{ strtoupper($civilStatus->title) }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Mobile Number</label>
                                                            <input type="text" class="form-control" name="mobile1" placeholder="mobile 1" maxlength="11" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
                                                            <input type="text" class="form-control" name="mobile2" placeholder="optional" maxlength="11" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Email</label>
                                                            <input type="email" class="form-control" name="email">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <h4>In Case of Emergency</h4>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Contact Name</label>
                                                    <input type="text" class="form-control" name="icoe_contact_name">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">Contact Number</label>
                                                    <input type="text" class="form-control" name="icoe_contact_mobile" maxlength="11" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="form-control-label">Contact Address</label>
                                                    <textarea name="icoe_contact_address" id="icoe_contact_address" rows="3"
                                                              class="form-control"></textarea>
                                                    <label class="css-input switch switch-sm switch-primary">
                                                        <input type="checkbox" class="cbox-same-address"><span></span> Same as Employee's Address
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-md-12 text-right">
                                                <button href="#" class="btn btn-primary">Next <span class="fa fa-chevron-right" style="font-size: 0.8em;"></span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
//            alert('Wow!');
            $(".cbox-same-address").click(function() {
                if($(this).is(':checked') == true) {
                    $("#icoe_contact_address").val($("#user-address").val());
                    $("#icoe_contact_address").attr('readonly', true);
                } else {
                    $("#icoe_contact_address").val('');
                    $("#icoe_contact_address").attr('readonly', false);
                }
            });

            $("#user-address").on('keyup', function() {
                if($("#icoe_contact_address").attr('readonly') == 'readonly') {
                    $("#icoe_contact_address").val($("#user-address").val());
                }
            })

            $(".btn-upload-image").click(function(e) {
                e.preventDefault();
                var target = $(this).attr('data-target');

                $("#" + target).click();
            });

            $(document).on('change', ".photo-field", function () {
                var me = $(this);
                var countFiles = $(this)[0].files.length;
                var imgPath = $(this)[0].value;
                var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
                // var image_holder = $(this).parents('.img-upload-group, .img-upload').find('.photo-box, .dropzone');


                if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                    if (typeof (FileReader) != "undefined") {

                        //loop for each file selected for uploaded.
                        for (var i = 0; i < countFiles; i++) {

                            reader = new FileReader();
                            reader.onload = function (e) {
                                $("#form-user-photo").attr('src', e.target.result);
                            }

                            // image_holder.show();
                            console.log($(this)[0].files[i]);
                            reader.readAsDataURL($(this)[0].files[i]);
                        }

                    } else {
                         alert("This browser does not support FileReader.");
                    }
                } else {
//                    alert("Selected file is not a valid image. Please try again.");
                    $("#form-user-photo").attr('src', "{{ asset('img/user-placeholder.png') }}");
                }
            });
        })
    </script>
@endsection

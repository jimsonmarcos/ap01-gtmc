@extends('layouts.print')

@section('content')

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="row" id="print-wrapper">
                                <br />

                                <h3 class="text-center">Final Pay Details</h3>
                                <br>
                                <p class="text-center"><strong>{{ \App\Keyval::where('key', 'account_name')->first()->value }}</strong></p>
                                <br>

                                <form action="" id="finalPayForm" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="col-md-12">

                                        <div class="col-md-6 col-xs-6">

                                            <table class="table">
                                                <tbody>

                                                </tbody>
                                            </table>

                                            <table class="table">

                                                <tbody>
                                                <tr>
                                                    <td class="no-border">Employee Name</td>
                                                    <td class="bordered">{{ strtoupper($finalPay->Profile->namelfm()) }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="no-border">Position</td>
                                                    <td class="bordered">{{ $finalPay->Profile->EmploymentDetails->Position->title }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="no-border">TIN No.</td>
                                                    <td class="bordered">{{ $finalPay->Profile->Compensation->tin }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="no-border">Effective Date</td>
                                                    <td class="bordered">{{ date('m/d/Y', strtotime($finalPay->effective_date)) }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-6 col-xs-6">

                                            <table class="table">
                                                <tr>
                                                    <td class="no-border">Company Code</td>
                                                    <td class="bordered">{{ $finalPay->Profile->CostCenter->Company->company_code }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="no-border">Payroll Group</td>
                                                    <td class="bordered">{{ $finalPay->Profile->Compensation->PayrollGroup->group_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="no-border">Cost Center</td>
                                                    <td class="bordered">{{ $finalPay->Profile->CostCenter->cost_center }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="no-border">Rate Type</td>
                                                    <td class="bordered">{{ $finalPay->Profile->Compensation->rate_type }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="no-border">Hire Date</td>
                                                    <td class="bordered">{{ date('m/d/Y', strtotime($finalPay->Profile->EmploymentDetails->hire_date)) }}</td>
                                                </tr>
                                            </table>

                                            <br>

                                            <table class="table">
                                                <tr>
                                                    <td class="no-border">Monthly Rate</td>
                                                    <td class="bordered">{{ number_format($finalPay->Profile->Compensation->daily_rate * 26, 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="no-border">Daily Rate</td>
                                                    <td class="bordered">{{ number_format($finalPay->Profile->Compensation->daily_rate, 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="no-border">Hourly Rate</td>
                                                    <td class="bordered">{{ number_format($finalPay->Profile->Compensation->daily_rate / 8, 2) }}</td>
                                                </tr>
                                            </table>

                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="col-xs-12">
                                            <h3>Final Pay Computation</h3>
                                            <br>
                                            <table id="final_pay_computation" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th class="text-center" width="50%" colspan="2" style="vertical-align: middle;">Particulars</th>
                                                    <th class="text-center" width="25%" style="vertical-align: middle;">Collectible <br> (From Employee)</th>
                                                    <th class="text-center" width="25%" style="vertical-align: middle;">Payable (To Employee)</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Salary (Last Cut off)</td>
                                                    <td>{{ $finalPay->PayrollCycle->payroll_period }}</td>
                                                    <td></td>
                                                    <td class="text-right">{{ $finalPay->salary }}</td>
                                                </tr>

                                                <tr>
                                                    <td>Cash Savings</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="text-right">{{ number_format($finalPay->cash_savings, 2) }}</td>
                                                </tr>

                                                <tr>
                                                    <td>Loan Savings</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="text-right">{{ number_format($finalPay->loan_savings, 2) }}</td>
                                                </tr>

                                                <tr>
                                                    <td>13th Month</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="text-right">{{ number_format($finalPay->thirteenth_month, 2) }}</td>
                                                </tr>

                                                @if(!empty($finalPay->ExistingLoans))
                                                    @foreach($finalPay->ExistingLoans as $unpaidLoan)
                                                        <tr>
                                                            <td>
                                                                @if(!empty($unpaidLoan->Loan->type))
                                                                    {{ $unpaidLoan->Loan->type }} Loan
                                                                @else
                                                                    {{ $unpaidLoan->Loan->LoanType->type }}
                                                                @endif
                                                            </td>
                                                            <td></td>
                                                            <td class="text-right">{{ number_format($unpaidLoan->amount, 2) }}</td>
                                                            <td></td>
                                                        </tr>
                                                    @endforeach
                                                @endif

                                                <tr>
                                                    <td>Client Charges</td>
                                                    <td></td>
                                                    <td class="text-right">{{ $finalPay->client_charges }}</td>
                                                    <td></td>
                                                </tr>

                                                @if(!empty($finalPay->OtherCharges))
                                                    @foreach($finalPay->OtherCharges as $otherCharges)
                                                        <tr>
                                                            <td>{{ $otherCharges->particular }}</td>
                                                            <td></td>
                                                            <td class="text-right">{{ $otherCharges->collectible }}</td>
                                                            <td></td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td>TOTALS</td>
                                                        <td></td>
                                                        <td class="text-right">{{ number_format($finalPay->total_collectible, 2) }}</td>
                                                        <td class="text-right">{{ number_format($finalPay->total_payable, 2) }}</td>
                                                    </tr>
                                                </tfoot>
                                            </table>

                                            <p id="net-payable">
                                                NET Payable
                                                <span class="pull-right" >
                                                    {{ number_format($finalPay->net_payable, 2) }}
                                                </span>
                                            </p>

                                            {{--<button id="addOtherCharges" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Other Charges</button>--}}
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-xs-12">
                                            <br>
                                            <h3>Summary of 13 Month Pay</h3>
                                            <br>
                                            <table id="summary" class="table">
                                                <thead>
                                                <tr>
                                                    <th class="text-center no-border">Payroll Code</th>
                                                    <th class="text-center no-border">Payroll Period</th>
                                                    <th class="text-center no-border">Basic Pay</th>
                                                    <th class="text-center no-border">Pro-Rated</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($finalPay->Summaries as $index => $payroll)
                                                    <tr>
                                                        <td class="text-center no-border">{{ $payroll->payroll_code }}</td>
                                                        <td class="text-center no-border">{{ $payroll->payroll_period }}</td>
                                                        <td class="text-right no-border">{{ number_format($payroll->basic_pay, 2) }}</td>
                                                        <td class="text-right no-border">{{ number_format($payroll->pro_rated, 2) }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th class="text-center has-border-bottom" style="border: none; border-top: 1px solid #000; border-bottom: 1px solid #000;">Total</th>
                                                    <th class="has-border-bottom" style="border: none; border-top: 1px solid #000; border-bottom: 1px solid #000;"></th>
                                                    <th class="has-border-bottom" style="border: none; border-top: 1px solid #000; border-bottom: 1px solid #000;"></th>
                                                    <th class="text-right has-border-bottom" style="border: none; border-top: 1px solid #000; border-bottom: 1px solid #000;">{{ number_format($finalPay->Summaries->sum('pro_rated'), 2) }}</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <table class="table">
                                            <tr>
                                                <th style="border: none !important;" width="20%">Remarks</th>
                                                <td class="bordered" style="height: 60px;">{{ $finalPay->remarks }}</td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="col-xs-6">
                                        <table class="table">
                                            <tr>
                                                <th class="no-border" width="40%">Prepared By:</th>
                                                <th class="no-border"></th>
                                            </tr>
                                            <tr>
                                                <td class="no-border"></td>
                                                <th class="no-border">{{ $finalPay->CreatedBy->Profile->namefl() }}</th>
                                            </tr>
                                            <tr>
                                                <th class="no-border"></th>
                                                <td class="no-border" style="font-style: italic;">@if(!empty($finalPay->CreatedBy->Profile->EmploymentDetails->Position->title))
                                                        {{ $finalPay->CreatedBy->Profile->EmploymentDetails->Position->title }}
                                                    @else
                                                        {{ $finalPay->CreatedBy->position }}
                                                    @endif</td>
                                            </tr>
                                        </table>

                                        <table class="table">
                                            <tr>
                                                <th class="no-border" width="40%">Noted By:</th>
                                                <th class="no-border"></th>
                                            </tr>
                                            <tr>
                                                <td class="no-border"></td>
                                                <th class="no-border">{{ strtoupper($finalPay->NotedBy->User->Profile->namefl()) }}</th>
                                            </tr>
                                            <tr>
                                                <th class="no-border"></th>
                                                <td class="no-border" style="font-style: italic;">@if(empty($finalPay->NotedBy->position))
                                                        {{ $finalPay->NotedBy->User->Profile->EmploymentDetails->Position->title }}
                                                    @else
                                                        {{ $finalPay->NotedBy->Position->title }}
                                                    @endif</td>
                                            </tr>
                                        </table>

                                        <table class="table">
                                            <tr>
                                                <th class="no-border" width="40%">Approved By:</th>
                                                <th class="no-border"></th>
                                            </tr>
                                            <tr>
                                                <td class="no-border"></td>
                                                <th class="no-border">{{ strtoupper($finalPay->ApprovedBy->User->Profile->namefl()) }}</th>
                                            </tr>
                                            <tr>
                                                <th class="no-border"></th>
                                                <td class="no-border" style="font-style: italic;">
                                                    @if(empty($finalPay->ApprovedBy->position))
                                                        {{ $finalPay->ApprovedBy->User->Profile->EmploymentDetails->Position->title }}
                                                    @else
                                                        {{ $finalPay->ApprovedBy->Position->title }}
                                                    @endif</td>
                                            </tr>
                                        </table>


                                    </div>

                                    <div class="col-xs-3 col-xs-offset-3">
                                        <table class="table">
                                            <tr>
                                                <th class="text-center no-border">Date Created:</th>
                                            </tr>
                                            <tr>
                                                <th class="text-right no-border">{{ date('d/m/Y', strtotime($finalPay->created_at)) }}</th>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="col-xs-12">
                                        <p class="text-right">Printed by {{ $me->Profile->namefl() }} in {{ date('d/m/Y') }} </p>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>
        body {
            background: #FFF;
        }

        p {
            line-height: .1 !important;
        }

        .no-border {
            border: none !important;
        }

        .bordered {
            border: 1px solid #000 !important;
        }

        .table-bordered td, .table-bordered th {
            border: 1px solid #000 !important;
        }

        td, th, tr {
            position: relative
        }

        #net-payable {
            padding: 0 10px;
            font-weight: bold;
            position: relative;
            margin-top: -10px;
        }

        #net-payable:before {
            content: '';
            left: 0;
            bottom: -10px;
            position: absolute;
            width: 100%;
            border-bottom: 1px solid #000;
        }

        #net-payable:after {
            content: '';
            left: 0;
            bottom: -13px;
            position: absolute;
            width: 100%;
            border-bottom: 1px solid #000;
        }

        .has-border-bottom:after {
            content: '';
            left: 0;
            bottom: -4px;
            position: absolute;
            width: 100%;
            border-bottom: 1px solid #000;
        }

        @media print {
            th, td {
                font-size: 10px !important;
                padding-top: 0px !important;
                padding-bottom: 0px !important;
            }

            p {
                font-size: 11px;
            }

            h4 {
                font-size: 14px;
            }

            h3 {
                font-size: 18px;
            }

            #main-content {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                margin-top: 0 !important;
            }

            #net-payable {
                font-size: 10.5px;
            }

        }

        hr {
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            window.print();
        })
    </script>
@endsection

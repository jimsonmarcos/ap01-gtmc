@extends('layouts.print')

@section('content')

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xs-3 text-right">
                                    {{--<img src="" alt="">--}}
                                </div>
                                <div class="col-xs-9">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="row">
                                        <div class="col-xs-5"><strong>Employee Name</strong></div>
                                        <div class="col-xs-7">{{ $loan->User->Profile->name() }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-5"><strong>Employee ID No</strong></div>
                                        <div class="col-xs-7">{{ $loan->User->Profile->id_number }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-5"><strong>Cost Center</strong></div>
                                        <div class="col-xs-7">{{ $loan->User->Profile->CostCenter->cost_center }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-5"><strong>CLA No</strong></div>
                                        <div class="col-xs-7">{{ $loan->cla_number }}</div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-5"><strong>@if($loan->is_voucher == 'N') Check No @else Voucher Number @endif</strong></div>
                                        <div class="col-xs-7">@if($loan->is_voucher == 'N') {{ $loan->check_number }} @else {{ $loan->cv_number }} @endif</div>
                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <div class="row">
                                        <div class="col-xs-6"><strong>Loan Type</strong></div>
                                        <div class="col-xs-6">@if($loan->is_voucher == 'N') {{ $loan->LoanType->type }} @else {{ $loan->loan_type }} @endif</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6"><strong>Loan Interest</strong></div>
                                        <div class="col-xs-6 text-right">@if($loan->is_voucher == 'N') {{ number_format($loan->LoanType->interest, 2) }}@endif%</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6"><strong>Total Loan Amount</strong></div>
                                        <div class="col-xs-6 text-right">{{ number_format($loan->granted_loan_amount, 2) }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6"><strong>Months to Pay</strong></div>
                                        <div class="col-xs-6 text-right">{{ $loan->months_to_pay }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6"><strong>Amortization</strong></div>
                                        <div class="col-xs-6 text-right">{{ number_format($loan->amortization, 2) }}</div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <br>
                                    <h3 style="text-decoration: underline;">LOAN AMORTIZATION SCHEDULE</h3>
                                    <br>
                                </div>
                                <div class="col-xs-12">
                                    <table class="table table-bordered table-condensed">
                                        <thead>
                                        <tr>
                                            <th class="text-center">MONTH</th>
                                            <th class="text-center">PAY PERIOD</th>
                                            <th class="text-center">DUE DATE</th>
                                            <th class="text-center">AMORTIZATION</th>
                                            <th class="text-center">OUTSTANDING BALANCE</th>
                                        </tr>
                                        </thead>


                                        <tbody>

                                        <?php
                                        $outstandingBalance = $paymentSchedules->sum('amortization');
                                        ?>

                                        <tr>
                                            <th class="text-center">0</th>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-right"></td>
                                            <th class="text-right">{{ number_format($outstandingBalance, 2) }}</th>
                                        </tr>


                                        @foreach($paymentSchedules as $paymentSchedule)
                                            <?php
                                            $outstandingBalance -= $paymentSchedule->amortization;
                                            ?>

                                            <tr>
                                                <th class="text-center">{{ $paymentSchedule->month }}</th>
                                                <td class="text-center">{{ $paymentSchedule->pay_period }}</td>
                                                <td class="text-center">{{ $paymentSchedule->due_date }}</td>
                                                <td class="text-right">{{ number_format($paymentSchedule->amortization, 2) }}</td>
                                                <th class="text-right">{{ $outstandingBalance > 0 ? number_format($outstandingBalance, 2) : '-' }}</th>
                                            </tr>
                                        @endforeach
                                        </tbody>



                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>
        body {
            background: #FFF;
        }

        p {
            line-height: .1 !important;
        }

        @media print {
            th, td {
                font-size: 10px;
                padding-top: 0px !important;
                padding-bottom: 0px !important;
            }

            p {
                font-size: 11px;
            }

            h4 {
                font-size: 14px;
            }

            h3 {
                font-size: 18px;
            }

            #main-content {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                margin-top: 0 !important;
            }
        }

        hr {
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            window.print();
        })
    </script>
@endsection

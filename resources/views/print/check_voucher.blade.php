@extends('layouts.print')

@section('content')

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-xs-12 text-center">
                                    <p>{{ strtoupper(\App\Keyval::where('key', 'account_name')->first()->value) }}</p>
                                    <p>{{ \App\Keyval::where('key', 'address')->first()->value }}</p>
                                    <p>{{ \App\Keyval::where('key', 'contact_number')->first()->value }}</p>
                                    <br>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <h4><strong><u>CHECK VOUCHER</u></strong></h4>
                                    <br>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <p><strong>CV No.: <?php echo $checkVoucher->cv_number ?></strong></p>
                                        </div>
                                        <div class="col-xs-offset-2 col-xs-4">
                                            <p><strong>Date: <?php echo date('m/d/Y', strtotime($checkVoucher->check_date)) ?></strong></p>
                                        </div>
                                        <div class="col-xs-6">
                                            <p><strong>Payee: <?php echo $checkVoucher->payee_name ?></strong></p>
                                        </div>
                                        <div class="col-xs-offset-2 col-xs-4">
                                            <p><strong>{{ $checkVoucher->bank }} CHECK#{{ $checkVoucher->check_number }}</strong></p>
                                        </div>
                                    </div>
                                </div>

                                <style>
                                    p {
                                        line-height: .1 !important;
                                    }

                                    @media print {
                                        th, td {
                                            font-size: 10px !important;
                                            padding-top: 0px !important;
                                            padding-bottom: 0px !important;
                                        }

                                        .table-header {
                                            font-size: 10px !important;
                                        }

                                        p {
                                            font-size: 11px;
                                        }

                                        h4 {
                                            font-size: 14px;
                                        }

                                        h3 {
                                            font-size: 18px;
                                        }

                                        #main-content {
                                            padding-top: 0 !important;
                                            padding-bottom: 0 !important;
                                            margin-top: 0 !important;
                                        }
                                    }

                                    hr {
                                        margin-top: 10px;
                                        margin-bottom: 10px;
                                    }
                                </style>

                                <div class="col-xs-12">
                                    <br>
                                    <div class="row">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="text-center table-header" colspan="4">PARTICULARS</th>
                                            </tr>
                                            <tr>
                                                <th colspan="4">&nbsp;</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($checkVoucher->Items as $item)
                                                <tr>
                                                    <td class="text-left" width="40%">{{ $item->item }} - {{ $item->description }}</td>
                                                    <td class="text-right" width="20%"></td>
                                                    <td class="text-right" width="20%"></td>
                                                    <td class="text-right" width="20%">{{ number_format($item->amount, 2) }}</td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>ENTRY:</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            @foreach($checkVoucher->Entries as $item)
                                                <tr>
                                                    <td class="text-left" width="40%">@if($item->txn == 'Credit') &nbsp;&nbsp;&nbsp;&nbsp; @endif {{ $item->account_title }}</td>
                                                    <td class="text-right" width="20%">{{ $item->debit > 0 ? number_format($item->debit, 2) : '' }}</td>
                                                    <td class="text-right" width="20%">{{ $item->credit > 0 ? number_format($item->credit, 2) : '' }}</td>
                                                    <td></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <th class="text-right"><strong>{{ $checkVoucher->Entries->sum('debit') > 0 ? number_format($checkVoucher->Entries->sum('debit'), 2) : '' }}</strong></th>
                                                    <th class="text-right"><strong>{{ $checkVoucher->Entries->sum('credit') > 0 ? number_format($checkVoucher->Entries->sum('credit'), 2) : '' }}</strong></th>
                                                    <th></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <br>

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div><strong>Prepared By: {{ $checkVoucher->PreparedBy->Profile->namefl() }}</strong></div>
                                            <br>
                                            <br>
                                            <div><strong>Approved By: {{ $checkVoucher->ApprovedBy->Profile->namefl() }}</strong></div>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <div class="">Reference {{ \App\CheckRequests::find($checkVoucher->source_id)->crf_number }} | Status {{ $checkVoucher->status }}</div>
                                        </div>

                                        <div class="col-xs-6">
                                            <div><strong>Received from {{ strtoupper(\App\Keyval::where('key', 'account_name')->first()->value) }} the sum of <span id="amountInWords" data-amount="<?php echo $checkVoucher->total_amount ?>" style="text-transform: uppercase;"></span> Only (PHP {{ number_format($checkVoucher->total_amount, 2) }})</strong></div>
                                            <br>
                                            <br>
                                            <div class="text-center">
                                                <strong>
                                                    By: <span style="color: #000">___________________________________________</span>
                                                    <br>
                                                    Payee's Signature
                                                </strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>

    <div class="container-fluid" style="position: absolute; bottom: 0;right: 0; width: 100%">
        <div class="row">
            <div class="col-xs-6">

            </div>
            <div class="col-xs-6">
                <div class="text-right">Generated {{ date('m/d/Y H:i:sa') }}</div>
            </div>
        </div>

    </div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            window.print();

            var totalAmount = $("#amountInWords").attr('data-amount');

            var pieces = totalAmount.toString().split('.');

            var decimal = '';
            var wholeNumber = 0;

            if(pieces.length == 1) {
                var wholeNumber = parseFloat(pieces[0]);
            } else if(pieces.length == 2) {
                // console.log(pieces);
                var wholeNumber = parseFloat(pieces[0]);


                // console.log(pieces[1]);

                if(pieces[1].length == 1) {
                    // decimal = decimal.numberFormat(2);
                    var decimal = " and "+ pieces[1] + "0/100";
                } else {
                    if(pieces[1] > 0) {
                        var decimal = " and "+ parseInt(pieces[1]) + "/100";
                    }
                }


            }

            var amountIntoWords = chunk(wholeNumber.numberFormat(2).replace(/\,/g, ''))
                .map(inEnglish)
                .map(appendScale)
                .filter(isTruthy)
                .reverse()
                .join(" ");

            if(amountIntoWords != '') {
                amountIntoWords = amountIntoWords + " Pesos" + decimal;
            }

            $("#amountInWords").text(amountIntoWords);
        })
    </script>
@endsection

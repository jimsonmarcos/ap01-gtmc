@extends('layouts.print')

@section('content')

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="row" id="print-wrapper">
                                <br />

                                <h3 class="text-center">Billing Invoice</h3>
                                <br>

                                <div class="col-xs-8">

                                    <div class="row">
                                        <div class="col-xs-4">
                                            @if(!empty(\App\Keyval::where('key', 'account_logo')->first()->value))
                                            <img src="{{ url(\App\Keyval::where('key', 'account_logo')->first()->value) }}" alt="" width="100%">
                                            @endif
                                        </div>
                                        <div class="col-xs-8">
                                            <table class="table table-condensed">
                                                <tbody>
                                                <tr>
                                                    <td class="no-border">{{ \App\Keyval::where('key', 'account_name')->first()->value }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="no-border">{{ \App\Keyval::where('key', 'address')->first()->value }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="no-border">{{ \App\Keyval::where('key', 'business_tax_type')->first()->value }} TIN: {{ \App\Keyval::where('key', 'tin_number')->first()->value }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    {{--<div>--}}
                                        {{--<div id="logo" style="background-image: url('{{ url(\App\Keyval::where('key', 'account_logo')->first()->value) }}');"></div>--}}
                                    {{--</div>--}}



                                </div>

                                <div class="col-xs-4">
                                    <table class="table table-condensed">
                                        <tbody>
                                        <tr>
                                            <td class="no-border" width="100">Invoice #</td>
                                            <td class="no-border">{{ $invoice->invoice_number }}</td>
                                        </tr>
                                        <tr>
                                            <td class="no-border">Invoice Date</td>
                                            <td class="no-border">{{ date('m/d/Y', strtotime($invoice->invoice_date)) }}</td>
                                        </tr>
                                        <tr>
                                            <td class="no-border">Due Date</td>
                                            <td class="no-border">{{ date('m/d/Y', strtotime($invoice->due_date)) }}</td>
                                        </tr>
                                        <tr>
                                            <td class="no-border">Reference</td>
                                            <td class="no-border">{{ $invoice->reference }}</td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <div class="text-center">
                                        <strong>Amount Due</strong>
                                        <div>
                                            <span id="totalAmountTxt" style="display: inline-block; border: 1px blue solid; padding: 1em 4em; border-radius: 10px; margin-top: 10px;">PHP {{ number_format($invoice->Items->sum('billing_amount', 2), 2) }} </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <br>
                                    <br>
                                </div>

                                <div class="col-xs-8">
                                    <div>
                                        <strong>Invoice To:</strong> <br>
                                        <span>{{ $invoice->company_name }}</span> <br>
                                        <span>{{ $invoice->company_address }}</span>
                                    </div>
                                    <br>
                                    <div>
                                        <strong>Terms</strong> <br>
                                        <span> Due
                                            @if($invoice->Terms->days > 0)
                                                in {{ $invoice->Terms->days }} days
                                            @else
                                                {{ $invoice->Terms->terms }}
                                            @endif
                                        </span>
                                        <br><br><br>
                                    </div>


                                </div>

                                <div class="clearfix"></div>

                                <div class="col-xs-12">
                                    <table class="table table-condensed" style="margin-bottom: 0">
                                        <table id="invoiceItems" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="300">DESCRIPTION</th>
                                                <th class="text-center" width="80">HEAD COUNT</th>
                                                <th class="text-center" width="80">HOURS</th>
                                                <th class="text-center" width="150">AMOUNT</th>
                                                <th class="text-center">TAX</th>
                                                <th class="text-center" width="150">TAX AMOUNT</th>
                                                <th class="text-center">BILLING AMOUNT</th>
                                            </tr>

                                            </thead>
                                            <tbody>
                                            @foreach($invoice->Items as $item)
                                                <tr>
                                                    <td>{{ $item->description }}</td>
                                                    <td class="text-center">{{ $item->head_count }}</td>
                                                    <td class="text-center">{{ $item->hours }}</td>
                                                    <td class="text-right">{{ number_format($item->amount, 2) }}</td>
                                                    <td class="text-center">{{ $item->BusinessTaxType->tax }}</td>
                                                    <td class="text-right">{{ number_format($item->tax_amount, 2) }}</td>
                                                    <td class="text-right">{{ number_format($item->billing_amount, 2) }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </table>
                                    <div class="text-right">
                                        <span style="display: inline-block; margin-top: 10px;font-weight: bold;width: 150px; text-align: left;"><strong>Sub Total</strong></span> <span id="totalAmountTxt" style="display: inline-block; border: 1px blue solid; padding: 1em 4em; border-radius: 10px; margin-top: 10px; font-weight: bold;">P {{ number_format($invoice->Items->sum('billing_amount', 2), 2) }} PHP</span>
                                    </div>
                                    <div class="text-right">
                                        <span style="display: inline-block; margin-top: 10px;font-weight: bold;width: 150px; text-align: left;"><strong>Total</strong></span> <span id="totalAmountTxt" style="display: inline-block; border: 1px blue solid; padding: 1em 4em; border-radius: 10px; margin-top: 10px; font-weight: bold;">P {{ number_format($invoice->Items->sum('billing_amount', 2), 2) }} PHP</span>
                                    </div>

                                    <br>
                                    <br>
                                </div>

                                <div class="col-xs-6">
                                    <div><strong>NOTES</strong></div>
                                    <div>{{ $invoice->note_to_recipient }}</div>
                                </div>

                                <div class="col-xs-6">
                                    <div><strong>TERMS AND CONDITIONS</strong></div>
                                    <div>{{ $invoice->terms_and_conditions }}</div>
                                </div>

                                <div class="col-xs-12"><br></div>

                                <div class="col-xs-6">
                                    <div><strong>Prepared By</strong></div>
                                    <div>
                                        {{ $invoice->PreparedBy->Profile->namefl() }}
                                        <br>
                                        {{ $invoice->PreparedBy->Profile->EmploymentDetails->Position->title }}
                                    </div>
                                </div>

                                <div class="col-xs-6">
                                    <div><strong>Approved By</strong></div>
                                    <div>
                                        {{ $invoice->ApprovedBy->Profile->namefl() }}
                                        <br>
                                        {{ $invoice->ApprovedBy->Profile->EmploymentDetails->Position->title }}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>
        body {
            background: #FFF;
        }

        p {
            line-height: .1 !important;
        }

        .no-border {
            border: none !important;
        }

        .bordered {
            border: 1px solid #000 !important;
        }

        .table-bordered td, .table-bordered th {
            border: 1px solid #000 !important;
        }

        td, th, tr {
            position: relative
        }

        #net-payable {
            padding: 0 10px;
            font-weight: bold;
            position: relative;
            margin-top: -10px;
        }

        #net-payable:before {
            content: '';
            left: 0;
            bottom: -10px;
            position: absolute;
            width: 100%;
            border-bottom: 1px solid #000;
        }

        #net-payable:after {
            content: '';
            left: 0;
            bottom: -13px;
            position: absolute;
            width: 100%;
            border-bottom: 1px solid #000;
        }

        .has-border-bottom:after {
            content: '';
            left: 0;
            bottom: -4px;
            position: absolute;
            width: 100%;
            border-bottom: 1px solid #000;
        }

        @media print {
            th, td {
                font-size: 10px !important;
                padding-top: 0px !important;
                padding-bottom: 0px !important;
            }

            p {
                font-size: 11px;
            }

            h4 {
                font-size: 14px;
            }

            h3 {
                font-size: 18px;
            }

            #main-content {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                margin-top: 0 !important;
            }

            #net-payable {
                font-size: 10.5px;
            }

            #logo {
                content:url({{ url("{$invoice->Company->logo_path}") }});
            }


        }

        hr {
            margin-top: 10px;
            margin-bottom: 10px;
        }

        #logo {
            display: inline-block;
            position: relative;
            height: 200px;
            width: 200px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center center;

        }


    </style>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            window.print();
        })
    </script>
@endsection

@extends('layouts.print')

@section('content')

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            @foreach($dtr as $d)
                            <?php
                                $user = $d->User;
                                $payrollCode = explode('-', $payrollCycle->payroll_code);
                                $user_id = $user->id;
//                                $payroll = \App\PayrollSummary::where(['payroll_cycle_id' => $d->payroll_cycle_id, 'user_id' => $d->User->id])->first();

                                if($d->User->Profile->Compensation->rate_type == 'Monthly') {
                                    $payrollComputation = $payroll = new \App\ComputePayroll($d, ['daily_rate' => $d->User->Profile->Compensation->monthly_rate / 26, 'monthly_rate' => $d->User->Profile->Compensation->monthly_rate / 2, 'rate_type' => 'Monthly']);
                                } else {
                                    if($d->User->Profile->Compensation->daily_category == 'Location') {
                                        $payrollComputation = $payroll = new \App\ComputePayroll($d, $d->User->Profile->Compensation->DailyRate->rate);
                                    } else {
                                        $payrollComputation = $payroll = new \App\ComputePayroll($d, $d->User->Profile->Compensation->daily_rate);
                                    }
                                }

//                                dd($payroll);

                                $ytdPayrollCycles = \App\PayrollCycles::where(['payroll_group_id' => $user->Profile->Compensation->PayrollGroup->id, 'effective_year' => $payrollCycle->effective_year, 'status' => 'Processed'])->pluck('id');
                                $ytdPayrollSummary = \App\PayrollSummary::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $d->User->id)->get();

                                //        dd(PayrollCycles::where('status', 'Processed')->whereYear('payroll_date', date('Y', strtotime( $data['payrollCycle']->payroll_date)))->pluck('id'));

                                $accumulatedHours = 0;
                                $totalLeaves = 0;
                                $unpaidLeave = 0;
                                if(!empty($ytdPayrollSummary)) {
                                    $ytdDtr = \App\PayrollDtr::whereIn('payroll_cycle_id', $ytdPayrollCycles)->where('user_id', $user->id)->get();
                                    $accumulatedHours = $ytdDtr->sum('vl') + $ytdDtr->sum('sl') + $ytdDtr->sum('el') + $ytdDtr->sum('spl') + $ytdDtr->sum('ml') + $ytdDtr->sum('pl') + $ytdDtr->sum('pto') + $ytdDtr->sum('bl') + $ytdDtr->sum('cl');
                                    $leaves = $payroll->vl + $payroll->sl + $payroll->el + $payroll->spl + $payroll->ml + $payroll->pl + $payroll->pto + $payroll->bl + $payroll->cl;

                                    if($leaves > 0) {
                                        $bal = (5 * 8) - $accumulatedHours;
                                        if($bal > 0) {

                                            $bal -= $leaves;
                                            if($bal < 0) {
                                                $unpaidLeave = abs($bal)* $payroll->hourlyRate();
                                            }

                                        } else {
                                            $unpaidLeave = $leaves* $payroll->hourlyRate();
                                        }
                                    }
                                }


                                $loans = \App\LoanPaymentSchedules::where(['pay_period' => $payrollCode[1], 'user_id' => $user_id]);

                                $sssLoan = 0;
                                $hdmfLoan = 0;
                                $coopLoan = 0;
                                foreach($loans->get() as $lps) {
                                    if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'SSS') {
                                        $sssLoan += $lps->Loan->amortization;
                                    }

                                    if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && $lps->Loan->loan_type == 'HDMF') {
                                        $hdmfLoan += $lps->Loan->amortization;
                                    }

                                    if($lps->Loan->releasing_status == 'Released' && $lps->Loan->is_paid == 'N' && in_array($lps->Loan->loan_type_id, ['1', '2', '3', '4'])) {
                                        $coopLoan += $lps->Loan->amortization;
                                    }
                                }

                                if($payrollCycle->cycle == 'B') {
                                    $payrollCycleA = \App\PayrollCycles::where([
                                        'payroll_group_id' => $payrollCycle->payroll_group_id,
                                        'effective_year' => $payrollCycle->effective_year,
                                        'effective_month' => $payrollCycle->effective_month,
                                        'cycle' => 'A',
                                        'status' => 'Processed'
                                    ])->first();

                                    $payrollSummaryA = \App\PayrollSummary::where('payroll_cycle_id', $payrollCycleA->id)->where('user_id', $user_id)->first();
                                    $basicPayA = !empty($payrollSummaryA->basic_pay) ? $payrollSummaryA->basic_pay : 0;
                                }

                                $basicPayB = 0;
                                if($payrollCycle->cycle == 'A') {
                                    $effectiveDate = \Carbon\Carbon::createFromFormat('Y-m-d', "{$payrollCycle->effective_year}-{$payrollCycle->effective_month}-01")->subMonth(1);
                                    $payrollCycleB = \App\PayrollCycles::where([
                                        'payroll_group_id' => $payrollCycle->payroll_group_id,
                                        'effective_year' => $effectiveDate->format('Y'),
                                        'effective_month' => $effectiveDate->format('m'),
                                        'cycle' => 'B',
                                        'status' => 'Processed'
                                    ])->first();

                                    if(empty($payrollCycleB)) {
                                        $basicPayB = 0;
                                    } else {
                                        $payrollSummaryB = \App\PayrollSummary::where('payroll_cycle_id', $payrollCycleB->id)->where('user_id', $user_id)->first();
                                        $basicPayB = !empty($payrollSummaryB->basic_pay) ? $payrollSummaryB->basic_pay : 0;
                                    }


                                }

                                $philhealth_ee = 0;
                                $philhealth_er = 0 ;
                                if($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_philhealth == 'Y') {
                                    $philHealthContribution = \App\PhilHealthContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first();
                                    if($philHealthContribution->id == 3) {
                                        $monthlyPremium = ($payroll->basicPay() + $basicPayB) * ($philHealthContribution->premium_rate / 100);
                                    } elseif($philHealthContribution->id == 1) {
                                        $monthlyPremium = 275;
                                    } elseif($philHealthContribution->id == 2) {
                                        $monthlyPremium = 1100;
                                    }

                                    $philhealth_ee = $philhealth_er = $monthlyPremium / 2;
                                }

                                $thirteenthMonth = 0;
                                if($payrollCycle->calculate_13th_month == 'Y' && App\EmploymentDetails::where('user_id', $user_id)->first()->position_id != 1) {
                                    $lastDecember = \App\PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)) - 1)->whereMonth('payroll_date', 12)->get();
                                    $toNovemberThisYear = \App\PayrollCycles::where(['payroll_group_id' => $payrollCycle->payroll_group_id, 'status' => 'Processed'])->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)))->whereMonth('payroll_date', 11)->get();

                                    foreach($lastDecember as $pc) {
                                        $pastSummary = \App\PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $user_id])->first();
                                        $thirteenthMonth += !empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0;
                                    }

                                    foreach($toNovemberThisYear as $pc) {
                                        $pastSummary = \App\PayrollSummary::where(['payroll_cycle_id' => $pc->id, 'user_id' => $user_id])->first();
                                        $thirteenthMonth += !empty($pastSummary->basic_pay) ? $pastSummary->basic_pay / 12 : 0;
                                    }
                                }

                                $membershipFee = 0;
                                if(\App\OtherPayments::where('user_id', $d->user_id)->where('transaction_type', 'MF')->sum('total_amount_paid') < \App\Keyval::where('key', 'membership_fee')->first()->value) {
                                    $membershipFee = \App\Keyval::where('key', 'membership_fee')->first()->value / 2;
                                }

                                $notarialFee = 0;
                                if(\App\OtherPayments::where('user_id', $d->user_id)->where('transaction_type', 'NF')->sum('total_amount_paid') < \App\Keyval::where('key', 'notarial_fee')->first()->value) {
                                    $notarialFee = \App\Keyval::where('key', 'notarial_fee')->first()->value / 2;
                                }


                                $payroll = [
                                    'payroll_cycle_id' => $d->payroll_cycle_id,
                                    'user_id'=> $d->user_id,
                                    'basic_pay' => $payroll->basicPay(),
                                    'ecola' => ($d->Profile->Compensation->daily_category == 'Location') ? ($payroll->rg() / 8) * $d->Profile->Compensation->DailyRate->ecola : 0.00,
                                    'overtime_pay' => $payroll->OTPay(),
                                    'holiday_pay' => $payroll->HDPay(),
                                    'night_differential' => $payroll->NDPay(),
                                    'holiday_ecola' => $d->Profile->Compensation->daily_category == 'Location' ? ($payroll->leg / 8) * $d->Profile->Compensation->DailyRate->ecola : 0.00,
                                    'adjustment_wage' => $d->nwage,
                                    'adjustment_income' => $d->adji,
                                    'adjustment_deduction' => $d->adjd,
                                    'de_minimis' => $d->Profile->Compensation->de_minimis,
                                    'thirteenth_month_bonus' => $thirteenthMonth,
                                    'thirteenth_month_pro_rated' => $payroll->basicPay() / 12,
                                    'fixed_allowance' => $d->Profile->Compensation->fixed_allowance / 2,
                                    'leave_encashment' => 0, // TODO
                                    'incentives' => $d->incentive,
                                    'coop_savings' => 0, // Omitted
                                    'sss_ec' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ec : 0,
                                    'sss_er' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->er : 0,
                                    'hdmf_er' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_hdmf == 'Y') ? \App\HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employer_share : 0,
                                    'philhealth_er' => $philhealth_er,
                                    'sss_ee' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_sss == 'Y') ? \App\SSSContributions::where('compensation_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('compensation_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->ee : 0,
                                    'hdmf_ee' => ($payrollCycle->cycle == 'A' && $d->Profile->Compensation->compute_hdmf == 'Y') ? \App\HDMFContributions::where('salary_range_start', '<=', $payroll->basicPay() + $basicPayB)->where('salary_range_end', '>=', $payroll->basicPay() + $basicPayB)->first()->employee_share : 0,
                                    'philhealth_ee' => $philhealth_ee,
                                    'tardiness' => ($payroll->hourlyRate() / 60) * ($payroll->lt + $payroll->ut),
                                    'absences' => ($payroll->hourlyRate() / 60) * $payroll->abs,
                                    'sss_loan' => $sssLoan,
                                    'hdmf_loan' => $hdmfLoan,
                                    'client_charges' => $d->client_charges,
                                    'membership_fee' => $membershipFee,
                                    'notarial_fee' => $notarialFee,
                                    'coop_loan' => $coopLoan,
                                    'coop_share' => $d->Profile->coop_share,
                                    'unpaid_leave' => $unpaidLeave
                                ];



                                if($unpaidLeave > 0) {
                                    $incomeTx = $payroll['basic_pay'] + $payrollComputation->rd() + $payrollComputation->spe() + $payrollComputation->sperd() + $payrollComputation->leg() + $payrollComputation->legrd() + $payroll['overtime_pay'] + $payroll['night_differential'] + $payroll['ecola'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
                                } else {
                                    $incomeTx = $payroll['basic_pay'] + $payrollComputation->rd() + $payrollComputation->spe() + $payrollComputation->sperd() + $payrollComputation->leg() + $payrollComputation->legrd() + $payroll['overtime_pay'] + $payroll['night_differential'] + $payrollComputation->vl() + $payrollComputation->sl() + $payrollComputation->el() + $payrollComputation->spl() + $payrollComputation->ml() + $payrollComputation->pl() + $payrollComputation->pto() + $payrollComputation->bl() + $payrollComputation->cl() + $payroll['ecola'] + $payroll['holiday_ecola'] + $payroll['adjustment_wage'];
                                }

                                $incomeNtx = $payroll['de_minimis'] + $payroll['thirteenth_month_bonus'] + $payroll['fixed_allowance'] + $payroll['leave_encashment'] + $payroll['incentives'] + $payroll['adjustment_income'];
                                $deductionTx = $payroll['sss_ee'] + $payroll['hdmf_ee'] + $payroll['philhealth_ee'] + $payroll['tardiness'] + $payroll['absences'];
                                $deduction = $payroll['sss_loan'] + $payroll['hdmf_loan'] + $payroll['client_charges'] + $payroll['membership_fee'] + $payroll['notarial_fee'] + $payroll['coop_loan'] + $payroll['coop_share'] + $payroll['adjustment_deduction'];

                                $payroll['tax'] = 0;
                                if(\App\Compensations::where('user_id', $d->user_id)->first()->deduct_withholding == 'Y') {
                                    $taxTable = new \App\TaxTable($d->User->Profile->Compensation->TaxType->title, $incomeTx);
                                    $payroll['tax'] = $taxTable->taxDue();
                                }

                                $payroll['gross_amount'] = $incomeTx + $incomeNtx;
                                $payroll['deductions'] = $deductionTx + $deduction + $unpaidLeave;
                                $payroll['net_pay'] = ($payroll['gross_amount'] - $payroll['deductions']) - $payroll['tax'];

                                $payroll = (object) $payroll;

//                                dd($payroll);


                            ?>
                            <div class="row" style="height: 468px;">
                                <br />

                                <div class="col-xs-12 text-center">
                                    <h3>{{ $user->Profile->EmploymentDetails->Principal->company_name }}</h3>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <hr>
                                    <h4><strong>PAYSLIP</strong></h4>
                                    <hr>
                                </div>

                                <div class="col-xs-12">
                                    <div class="col-xs-6">
                                        <p><strong>Name:</strong> {{ $user->name  }}</p>
                                    </div>
                                    @if($user->Profile->Compensation->rate_type == 'Monthly')
                                        <div class="col-xs-6">
                                            <p><strong>Semi-monthly Rate:</strong> {{ number_format($payrollComputation->monthlyRate, 2)  }}</p>
                                        </div>
                                    @else
                                        <div class="col-xs-6">
                                            <p><strong>Daily Rate:</strong> {{ number_format($payrollComputation->dailyRate, 2)  }}</p>
                                        </div>
                                    @endif

                                    <div class="col-xs-6">
                                        <p><strong>Employee ID:</strong> {{ $user->Profile->id_number  }}</p>
                                    </div>

                                    <div class="col-xs-6">
                                        <p><strong>Payroll Date:</strong> {{ date('m/d/Y', strtotime($payrollCycle->payroll_date))  }}</p>
                                    </div>

                                    <div class="col-xs-6">
                                        <p style="margin-bottom: 0;"><strong>Cost Center:</strong> {{ $payrollCycle->PayrollGroup->group_name }} ({{ $payrollCycle->PayrollGroup->Company->company_code }}) </p>
                                        {{--{{ $user->Profile->CostCenter->cost_center  }}--}}
                                    </div>

                                    <div class="col-xs-6">
                                        <p style="margin-bottom: 0;"><strong>Payroll Period:</strong> {{ $payrollCycle->payroll_period  }}</p>
                                    </div>
                                </div>

                                <style>
                                    p {
                                        line-height: .1 !important;
                                    }

                                    @media print {
                                        th, td {
                                            font-size: 10px;
                                            padding-top: 0px !important;
                                            padding-bottom: 0px !important;
                                        }

                                        .table-header {
                                            font-size: 10px !important;
                                        }

                                        p {
                                            font-size: 11px;
                                        }

                                        h4 {
                                            font-size: 14px;
                                        }

                                        h3 {
                                            font-size: 18px;
                                        }

                                        #main-content {
                                            padding-top: 0 !important;
                                            padding-bottom: 0 !important;
                                            margin-top: 0 !important;
                                        }
                                    }

                                    hr {
                                        margin-top: 10px;
                                        margin-bottom: 10px;
                                    }
                                </style>

                                <div class="col-xs-12">
                                    <hr />
                                    <div class="row">
                                        <div class="col-xs-4">
                                            @if($payroll->sss_er > 0 || $payroll->sss_ec > 0 || $payroll->hdmf_er > 0 || $payroll->philhealth_er > 0)
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center table-header" colspan="2">Employer Paid Benefits</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if($payroll->sss_er > 0)
                                                <tr>
                                                    <td width="50%">SSS ER</td>
                                                    <td>{{ number_format($payroll->sss_er, 2) }}</td>
                                                </tr>
                                                @endif

                                                @if($payroll->sss_ec > 0)
                                                <tr>
                                                    <td>SSS EC</td>
                                                    <td>{{ number_format($payroll->sss_ec, 2) }}</td>
                                                </tr>
                                                @endif

                                                @if($payroll->hdmf_er > 0)
                                                <tr>
                                                    <td>HDMF ER</td>
                                                    <td>{{ number_format($payroll->hdmf_er, 2) }}</td>
                                                </tr>
                                                @endif

                                                @if($payroll->philhealth_er > 0)
                                                <tr>
                                                    <td>PhilHealth ER</td>
                                                    <td>{{ number_format($payroll->philhealth_er, 2) }}</td>
                                                </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                            @endif


                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th colspan="2" class="text-center table-header">Year to Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($ytdPayrollSummary->sum('gross_amount') > 0)
                                                <tr>
                                                    <td width="50%">Gross Amount</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('gross_amount'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('deductions') > 0)
                                                <tr>
                                                    <td>Deductions</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('deductions'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('tax') > 0)
                                                <tr>
                                                    <td>Tax</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('tax'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('basic_pay') > 0)
                                                <tr>
                                                    <td>BASIC</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('basic_pay'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('de_minimis') > 0)
                                                <tr>
                                                    <td>De Minimis</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('de_minimis'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('fixed_allowance') > 0)
                                                <tr>
                                                    <td>Fixed Allowance</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('fixed_allowance'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('incentives') > 0)
                                                <tr>
                                                    <td>Incentives</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('incentives'), 2) }}</td>
                                                </tr>
                                                @endif
                                                {{--@if($ytdPayrollSummary->sum('sss_er') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>SSS ER</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('sss_er'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('sss_ec') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>SSS EC</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('sss_ec'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('hdmf_er') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>HDMF ER</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('hdmf_er'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('philhealth_er') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>PhilHealth ER</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('philhealth_er'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('sss_ee') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>SSS EE</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('sss_ee'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('hdmf_ee') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>HDMF EE</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('hdmf_ee'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('philhealth_ee') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>PhilHealth EE</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('philhealth_ee'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                @if($ytdPayrollSummary->sum('sss_loan') > 0)
                                                <tr>
                                                    <td>SSS Loan</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('sss_loan'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('hdmf_loan') > 0)
                                                <tr>
                                                    <td>HDMF Loan</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('hdmf_loan'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('coop_loan') > 0)
                                                <tr>
                                                    <td>COOP Loan</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('coop_loan'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('coop_share') > 0)
                                                <tr>
                                                    <td>COOP Share</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('coop_share'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($unpaidLeave > 0)
                                                <tr>
                                                    <td>Unpaid Leave</td>
                                                    <td>{{ number_format($unpaidLeave, 2) }}</td>
                                                </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-xs-4">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th width="60%" class="table-header">Hours and Earnings</th>
                                                    <th class="text-center table-header">Days</th>
                                                    <th width="30%" class="text-center table-header">Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($payroll->basic_pay > 0)
                                                <tr>
                                                    <td>BASIC</td>
                                                    <td>{{ ($payrollComputation->rg() / 8) }}</td>
                                                    <td>{{ number_format($payroll->basic_pay, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->rd() > 0)
                                                <tr>
                                                    <td>RD</td>
                                                    <td>{{ ($payrollComputation->rd / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->rd(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->spe() > 0)
                                                <tr>
                                                    <td>SPE</td>
                                                    <td>{{ ($payrollComputation->spe / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->spe(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->sperd() > 0)
                                                <tr>
                                                    <td>SPERD</td>
                                                    <td>{{ ($payrollComputation->sperd / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->sperd(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->leg() > 0)
                                                <tr>
                                                    <td>LEG</td>
                                                    <td>{{ ($payrollComputation->leg / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->leg(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->legrd() > 0)
                                                <tr>
                                                    <td>LEGRD</td>
                                                    <td>{{ ($payrollComputation->legrd / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->legrd(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->OTPay() > 0)
                                                <tr>
                                                    <td>OT</td>
                                                    <td>{{ ($payrollComputation->regot / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->OTPay(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->NDPay() > 0)
                                                <tr>
                                                    <td>ND</td>
                                                    <td>{{ ($payrollComputation->nd / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->NDPay(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->vl() > 0)
                                                <tr>
                                                    <td>VL</td>
                                                    <td>{{ ($payrollComputation->vl / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->vl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->sl() > 0)
                                                <tr>
                                                    <td>SL</td>
                                                    <td>{{ ($payrollComputation->sl / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->sl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->el() > 0)
                                                <tr>
                                                    <td>EL</td>
                                                    <td>{{ ($payrollComputation->el / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->el(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->spl() > 0)
                                                <tr>
                                                    <td>SPL</td>
                                                    <td>{{ ($payrollComputation->spl / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->spl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->ml() > 0)
                                                <tr>
                                                    <td>ML</td>
                                                    <td>{{ ($payrollComputation->ml / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->ml(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->pl() > 0)
                                                <tr>
                                                    <td>PL</td>
                                                    <td>{{ ($payrollComputation->pl / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->pl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->pto() > 0)
                                                <tr>
                                                    <td>PTO</td>
                                                    <td>{{ ($payrollComputation->pto / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->pto(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->bl() > 0)
                                                <tr>
                                                    <td>BL</td>
                                                    <td>{{ ($payrollComputation->bl / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->bl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->cl() > 0)
                                                <tr>
                                                    <td>CL</td>
                                                    <td>{{ ($payrollComputation->cl / 8) }}</td>
                                                    <td>{{ number_format($payrollComputation->cl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->ecola > 0)
                                                <tr>
                                                    <td>ECOLA</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->ecola, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->holiday_ecola > 0)
                                                <tr>
                                                    <td>HECOLA</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->holiday_ecola, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->adjustment_wage > 0)
                                                <tr>
                                                    <td>NWAGE</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->adjustment_wage, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->de_minimis > 0)
                                                <tr>
                                                    <td>DE MINIMIS</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->de_minimis, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->thirteenth_month_bonus > 0)
                                                <tr>
                                                    <td>13TH MONTH</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->thirteenth_month_bonus, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->fixed_allowance > 0)
                                                <tr>
                                                    <td>FIXED</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->fixed_allowance, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->leave_encashment > 0)
                                                <tr>
                                                    <td>LEAVE</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->leave_encashment, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->incentives > 0)
                                                <tr>
                                                    <td>INCENTIVES</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->incentives, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->adjustment_income > 0)
                                                    <tr>
                                                        <td>Other Income</td>
                                                        <td></td>
                                                        <td>{{ number_format($payroll->adjustment_income, 2) }}</td>
                                                    </tr>
                                                @endif
                                                @foreach($d->Others()->where('type', 'Income') as $x => $income)
                                                    <tr>
                                                        <td>{{ $income->title }}</td>
                                                        <td></td>
                                                        <td>{{ number_format($income->amount, 2) }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        @if($payroll->absences > 0 ||
                                            $payroll->tardiness > 0 ||
                                            $payroll->sss_ee > 0 ||
                                            $payroll->hdmf_ee > 0 ||
                                            $payroll->philhealth_ee > 0 ||
                                            $payroll->sss_loan > 0 ||
                                            $payroll->hdmf_loan > 0 ||
                                            $payroll->client_charges > 0 ||
                                            $payroll->membership_fee > 0 ||
                                            $payroll->notarial_fee > 0 ||
                                            $payroll->coop_loan > 0 ||
                                            $payroll->coop_share > 0)
                                        <div class="col-xs-4">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th width="50%" class=" table-header">Deductions</th>
                                                    <th class="text-center table-header">Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($payroll->absences > 0)
                                                <tr>
                                                    <td>Absences</td>
                                                    <td>{{ number_format($payroll->absences, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->tardiness > 0)
                                                <tr>
                                                    <td>Tardiness</td>
                                                    <td>{{ number_format($payroll->tardiness, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->sss_ee > 0)
                                                <tr>
                                                    <td>SSS EE</td>
                                                    <td>{{ number_format($payroll->sss_ee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->hdmf_ee > 0)
                                                <tr>
                                                    <td>HDMF EE</td>
                                                    <td>{{ number_format($payroll->hdmf_ee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->philhealth_ee > 0)
                                                <tr>
                                                    <td>PhilHealth EE</td>
                                                    <td>{{ number_format($payroll->philhealth_ee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->sss_loan > 0)
                                                <tr>
                                                    <td>SSS Loan</td>
                                                    <td>{{ number_format($payroll->sss_loan, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->hdmf_loan > 0)
                                                <tr>
                                                    <td>HDMF Loan</td>
                                                    <td>{{ number_format($payroll->hdmf_loan, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->client_charges > 0)
                                                <tr>
                                                    <td>Client Charges</td>
                                                    <td>{{ number_format($payroll->client_charges, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->membership_fee > 0)
                                                <tr>
                                                    <td>Membership Fee</td>
                                                    <td>{{ number_format($payroll->membership_fee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->notarial_fee > 0)
                                                <tr>
                                                    <td>Notarial Fee</td>
                                                    <td>{{ number_format($payroll->notarial_fee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->coop_loan > 0)
                                                <tr>
                                                    <td>Coop Loan</td>
                                                    <td>{{ number_format($payroll->coop_loan, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->coop_share > 0)
                                                <tr>
                                                    <td>Coop Share</td>
                                                    <td>{{ number_format($payroll->coop_share, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->adjustment_deduction > 0)
                                                    <tr>
                                                        <td>Other Deduction</td>
                                                        <td>{{ number_format($payroll->adjustment_deduction, 2) }}</td>
                                                    </tr>
                                                @endif
                                                @foreach($d->Others()->where('type', 'Deduction') as $x => $income)
                                                    <tr>
                                                        <td>{{ $income->title }}</td>
                                                        <td>{{ number_format($income->amount, 2) }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            @endif

                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th width="50%" class="table-header">CURRENT NET PAY</th>
                                                    <th class="text-center table-header">Amount</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <tr>
                                                    <th class="table-header">
                                                        <div>GROSS AMOUNT</div>
                                                        <div>DEDUCTIONS</div>
                                                        <div>TAX</div>
                                                    </th>
                                                    <td>
                                                        <div>{{ number_format($payroll->gross_amount, 2) }}</div>
                                                        @if($payroll->deductions > 0)<div>{{ number_format($payroll->deductions, 2) }}</div> @endif
                                                        @if($payroll->tax > 0)<div>{{ number_format($payroll->tax, 2) }}</div> @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th class="table-header">NET PAY</th>
                                                    <th class="table-header">{{ number_format($payroll->net_pay, 2) }}</th>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <hr>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            window.print();
        })
    </script>
@endsection

@extends('layouts.print')

@section('content')

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-xs-12 text-center">
                                    <h3>{{ \App\Keyval::where('key', 'account_name')->first()->value }}</h3>
                                    <br>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <h4><strong><u>CHECK REQUEST FORM</u></strong></h4>
                                    <br>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-offset-8 col-xs-4">
                                            <p><strong><?php echo $checkRequest->crf_number ?></strong></p>
                                            <br>
                                        </div>
                                        <div class="col-xs-6">
                                            <p><strong>Name of Employee/Payee: <?php echo $checkRequest->payee_name ?></strong></p>
                                        </div>
                                        <div class="col-xs-offset-2 col-xs-4">
                                            <p><strong>Date: <?php echo date('m/d/Y', strtotime($checkRequest->request_date)) ?></strong></p>
                                        </div>
                                        <div class="col-xs-6">
                                            <p><strong>Amount in words: <span id="amountInWords" data-amount="<?php echo $checkRequest->total_amount ?>"></span></strong></p>
                                        </div>
                                        <div class="col-xs-offset-2 col-xs-4">
                                            <p><strong>Amount: Php <?php echo number_format($checkRequest->total_amount, 2); ?></strong></p>
                                        </div>
                                    </div>
                                </div>

                                <style>
                                    p {
                                        line-height: .1 !important;
                                    }

                                    @media print {
                                        th, td {
                                            font-size: 10px;
                                            padding-top: 0px !important;
                                            padding-bottom: 0px !important;
                                        }

                                        .table-header {
                                            font-size: 10px !important;
                                        }

                                        p {
                                            font-size: 11px;
                                        }

                                        h4 {
                                            font-size: 14px;
                                        }

                                        h3 {
                                            font-size: 18px;
                                        }

                                        #main-content {
                                            padding-top: 0 !important;
                                            padding-bottom: 0 !important;
                                            margin-top: 0 !important;
                                        }
                                    }

                                    hr {
                                        margin-top: 10px;
                                        margin-bottom: 10px;
                                    }
                                </style>

                                <div class="col-xs-12">
                                    <br>
                                    <div class="row">
                                        <h5><strong>SUMMARY/SCHEDULE OF EXPENDITURES</strong></h5>
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="text-center table-header" width="35%">ITEM</th>
                                                <th class="text-center table-header" width="35%">REFERENCE NUMBER</th>
                                                <th class="text-center table-header" width="30%">AMOUNT</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($checkRequest->Items as $item)
                                                <tr>
                                                    <td class="text-center">{{ $item->cash_flow_item }}</td>
                                                    <td class="text-center">{{ $item->reference_number }}</td>
                                                    <td class="text-center">{{ number_format($item->amount, 2) }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <table class="table">
                                            <tbody>
                                            <tr style="border: none;">
                                                <td style="border: none;" width="35%"></td>
                                                <td class="text-center" style="border: none;" width="35%"><strong>TOTAL AMOUNT</strong></td>
                                                <td class="text-center" style="border: none;" width="30%"><span id="totalAmount" style="margin: 10px 0;display: inline-block; border: 1px blue solid; border-radius: 50px; padding: 10px 50px;font-weight: bold;">Php {{ !empty($checkRequest->total_amount) ? number_format($checkRequest->total_amount, 2) : '0.00' }}</span></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <br>

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div>Requested By:</div>
                                            <br>
                                            <br>
                                            <div><strong>{{ $checkRequest->RequestedBy->Profile->namefl() }}</strong></div>
                                            <br>
                                            <br>
                                        </div>

                                        <div class="col-xs-6">
                                            <div>Noted By:</div>
                                            <br>
                                            <br>
                                            <div><strong>{{ $checkRequest->NotedBy->Profile->namefl() }}</strong></div>
                                            <br>
                                            <br>
                                        </div>

                                        <div class="col-xs-6">
                                            <div>Approved By:</div>
                                            <br>
                                            <br>
                                            <div><strong>{{ $checkRequest->ApprovedBy->Profile->namefl() }}</strong></div>
                                        </div>

                                        <div class="col-xs-6">
                                            <div><strong>Received by/Date:</strong></div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            window.print();

            var totalAmount = $("#amountInWords").attr('data-amount');

            var pieces = totalAmount.toString().split('.');

            var decimal = '';
            var wholeNumber = 0;

            if(pieces.length == 1) {
                var wholeNumber = parseFloat(pieces[0]);
            } else if(pieces.length == 2) {
                // console.log(pieces);
                var wholeNumber = parseFloat(pieces[0]);


                // console.log(pieces[1]);

                if(pieces[1].length == 1) {
                    // decimal = decimal.numberFormat(2);
                    var decimal = " and "+ pieces[1] + "0/100";
                } else {
                    if(pieces[1] > 0) {
                        var decimal = " and "+ parseInt(pieces[1]) + "/100";
                    }
                }


            }

            var amountIntoWords = chunk(wholeNumber.numberFormat(2).replace(/\,/g, ''))
                .map(inEnglish)
                .map(appendScale)
                .filter(isTruthy)
                .reverse()
                .join(" ");

            if(amountIntoWords != '') {
                amountIntoWords = amountIntoWords + " Pesos" + decimal;
            }

            $("#amountInWords").text(amountIntoWords);
        })
    </script>
@endsection

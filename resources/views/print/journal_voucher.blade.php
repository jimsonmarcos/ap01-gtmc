@extends('layouts.print')

@section('content')

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-xs-12 text-center">
                                    <p>{{ strtoupper(\App\Keyval::where('key', 'account_name')->first()->value) }}</p>
                                    <p>{{ \App\Keyval::where('key', 'address')->first()->value }}</p>
                                    <p>{{ \App\Keyval::where('key', 'contact_number')->first()->value }}</p>
                                    <br>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <h4><strong><u>JOURNAL VOUCHER</u></strong></h4>
                                    <br>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <p><strong><?php echo $journalVoucher->jv_number ?></strong></p>
                                        </div>
                                        <div class="col-xs-offset-2 col-xs-4">
                                            <p><strong>Date: <?php echo date('m/d/Y', strtotime($journalVoucher->date)) ?></strong></p>
                                        </div>
                                    </div>
                                </div>

                                <style>
                                    p {
                                        line-height: .1 !important;
                                    }

                                    @media print {
                                        th, td {
                                            font-size: 10px !important;
                                            padding-top: 0px !important;
                                            padding-bottom: 0px !important;
                                        }

                                        .table-header {
                                            font-size: 10px !important;
                                        }

                                        p {
                                            font-size: 11px;
                                        }

                                        h4 {
                                            font-size: 14px;
                                        }

                                        h3 {
                                            font-size: 18px;
                                        }

                                        #main-content {
                                            padding-top: 0 !important;
                                            padding-bottom: 0 !important;
                                            margin-top: 0 !important;
                                        }
                                    }

                                    hr {
                                        margin-top: 10px;
                                        margin-bottom: 10px;
                                    }
                                </style>

                                <div class="col-xs-12">
                                    <br>
                                    <div class="row">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="text-center table-header" colspan="1">&nbsp;</th>
                                                <th class="text-center table-header" colspan="1">DR</th>
                                                <th class="text-center table-header" colspan="1">CR</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>ENTRY:</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            @foreach($journalVoucher->Items as $item)
                                                <tr>
                                                    <td class="text-left" width="40%">@if($item->txn == 'Credit') &nbsp;&nbsp;&nbsp;&nbsp; @endif {{ $item->account_title }}</td>
                                                    <td class="text-right" width="30%">{{ $item->debit > 0 ? number_format($item->debit, 2) : '' }}</td>
                                                    <td class="text-right" width="30%">{{ $item->credit > 0 ? number_format($item->credit, 2) : '' }}</td>
                                                </tr>
                                            @endforeach
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>REMARKS: {{ $journalVoucher->particulars }}</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <th class="text-right"><strong>{{ $journalVoucher->Items->sum('debit') > 0 ? number_format($journalVoucher->Items->sum('debit'), 2) : '' }}</strong></th>
                                                    <th class="text-right"><strong>{{ $journalVoucher->Items->sum('credit') > 0 ? number_format($journalVoucher->Items->sum('credit'), 2) : '' }}</strong></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <br>

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div><strong>Prepared By:</strong></div>
                                            <br>
                                            <br>
                                            <div><strong>{{ $journalVoucher->User->Profile->namefl() }}</strong></div>
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>

    <div class="container-fluid" style="position: absolute; bottom: 0;right: 0;">
        <div class="text-right">Generated {{ date('m/d/Y H:i:sa') }}</div>
    </div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            window.print();
        })
    </script>
@endsection

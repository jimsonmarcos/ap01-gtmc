@extends('layouts.print')

@section('content')

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-xs-12 text-center">
                                    <h3>{{ $user->Profile->EmploymentDetails->Principal->company_name }}</h3>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <hr>
                                    <h4><strong>PAYSLIP</strong></h4>
                                    <hr>
                                </div>

                                <div class="col-xs-12">
                                    <div class="col-xs-6">
                                        <p><strong>Name:</strong> {{ $user->name  }}</p>
                                    </div>
                                    @if($user->Profile->Compensation->rate_type == 'Monthly')
                                        <div class="col-xs-6">
                                            <p><strong>Semi-monthly Rate:</strong> {{ number_format($payrollComputation->monthlyRate, 2)  }}</p>
                                        </div>
                                    @else
                                        <div class="col-xs-6">
                                            <p><strong>Daily Rate:</strong> {{ number_format($payrollComputation->dailyRate, 2)  }}</p>
                                        </div>
                                    @endif

                                    <div class="col-xs-6">
                                        <p><strong>Employee ID:</strong> {{ $user->Profile->id_number  }}</p>
                                    </div>

                                    <div class="col-xs-6">
                                        <p><strong>Payroll Date:</strong> {{ date('m/d/Y', strtotime($payrollCycle->payroll_date))  }}</p>
                                    </div>

                                    <div class="col-xs-6">
                                        <p style="margin-bottom: 0;"><strong>Cost Center:</strong> {{ $payrollCycle->PayrollGroup->group_name }} ({{ $payrollCycle->PayrollGroup->Company->company_code }}) </p>
                                        {{--{{ $user->Profile->CostCenter->cost_center  }}--}}
                                    </div>

                                    <div class="col-xs-6">
                                        <p style="margin-bottom: 0;"><strong>Payroll Period:</strong> {{ $payrollCycle->payroll_period  }}</p>
                                    </div>
                                </div>

                                <style>
                                    p {
                                        line-height: .1 !important;
                                    }

                                    @media print {
                                        th, td {
                                            font-size: 10px;
                                            padding-top: 0px !important;
                                            padding-bottom: 0px !important;
                                        }

                                        .table-header {
                                            font-size: 10px !important;
                                        }

                                        p {
                                            font-size: 11px;
                                        }

                                        h4 {
                                            font-size: 14px;
                                        }

                                        h3 {
                                            font-size: 18px;
                                        }

                                        #main-content {
                                            padding-top: 0 !important;
                                            padding-bottom: 0 !important;
                                            margin-top: 0 !important;
                                        }
                                    }

                                    hr {
                                        margin-top: 10px;
                                        margin-bottom: 10px;
                                    }
                                </style>

                                <div class="col-xs-12">
                                    <hr />
                                    <div class="row">
                                        <div class="col-xs-4">
                                            @if($payroll->sss_er > 0 || $payroll->sss_ec > 0 || $payroll->hdmf_er > 0 || $payroll->philhealth_er > 0)
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center table-header" colspan="2">Employer Paid Benefits</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if($payroll->sss_er > 0)
                                                <tr>
                                                    <td width="50%">SSS ER</td>
                                                    <td>{{ number_format($payroll->sss_er, 2) }}</td>
                                                </tr>
                                                @endif

                                                @if($payroll->sss_ec > 0)
                                                <tr>
                                                    <td>SSS EC</td>
                                                    <td>{{ number_format($payroll->sss_ec, 2) }}</td>
                                                </tr>
                                                @endif

                                                @if($payroll->hdmf_er > 0)
                                                <tr>
                                                    <td>HDMF ER</td>
                                                    <td>{{ number_format($payroll->hdmf_er, 2) }}</td>
                                                </tr>
                                                @endif

                                                @if($payroll->philhealth_er > 0)
                                                <tr>
                                                    <td>PhilHealth ER</td>
                                                    <td>{{ number_format($payroll->philhealth_er, 2) }}</td>
                                                </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                            @endif


                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th colspan="2" class="text-center table-header">Year to Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($ytdPayrollSummary->sum('gross_amount') > 0)
                                                <tr>
                                                    <td width="50%">Gross Amount</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('gross_amount'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('deductions') > 0)
                                                <tr>
                                                    <td>Deductions</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('deductions'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('tax') > 0)
                                                <tr>
                                                    <td>Tax</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('tax'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('basic_pay') > 0)
                                                <tr>
                                                    <td>BASIC</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('basic_pay'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('de_minimis') > 0)
                                                <tr>
                                                    <td>De Minimis</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('de_minimis'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('fixed_allowance') > 0)
                                                <tr>
                                                    <td>Fixed Allowance</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('fixed_allowance'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('incentives') > 0)
                                                <tr>
                                                    <td>Incentives</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('incentives'), 2) }}</td>
                                                </tr>
                                                @endif
                                                {{--@if($ytdPayrollSummary->sum('sss_er') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>SSS ER</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('sss_er'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('sss_ec') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>SSS EC</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('sss_ec'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('hdmf_er') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>HDMF ER</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('hdmf_er'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('philhealth_er') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>PhilHealth ER</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('philhealth_er'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('sss_ee') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>SSS EE</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('sss_ee'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('hdmf_ee') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>HDMF EE</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('hdmf_ee'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('philhealth_ee') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>PhilHealth EE</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('philhealth_ee'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                @if($ytdPayrollSummary->sum('sss_loan') > 0)
                                                <tr>
                                                    <td>SSS Loan</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('sss_loan'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('hdmf_loan') > 0)
                                                <tr>
                                                    <td>HDMF Loan</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('hdmf_loan'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('coop_loan') > 0)
                                                <tr>
                                                    <td>COOP Loan</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('coop_loan'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('coop_share') > 0)
                                                <tr>
                                                    <td>COOP Share</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('coop_share'), 2) }}</td>
                                                </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-xs-4">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th width="60%" class="table-header">Hours and Earnings</th>
                                                    <th class="text-center table-header">Hrs</th>
                                                    <th width="30%" class="text-center table-header">Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($payroll->basic_pay > 0)
                                                <tr>
                                                    <td>BASIC</td>
                                                    <td>{{ $payrollComputation->rg() }}</td>
                                                    <td>{{ number_format($payroll->basic_pay, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->rd() > 0)
                                                <tr>
                                                    <td>RD</td>
                                                    <td>{{ $payrollComputation->rd }}</td>
                                                    <td>{{ number_format($payrollComputation->rd(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->spe() > 0)
                                                <tr>
                                                    <td>SPE</td>
                                                    <td>{{ $payrollComputation->sperd }}</td>
                                                    <td>{{ number_format($payrollComputation->spe(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->sperd() > 0)
                                                <tr>
                                                    <td>SPERD</td>
                                                    <td>{{ $payrollComputation->sperd }}</td>
                                                    <td>{{ number_format($payrollComputation->sperd(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->leg() > 0)
                                                <tr>
                                                    <td>LEG</td>
                                                    <td>{{ $payrollComputation->leg }}</td>
                                                    <td>{{ number_format($payrollComputation->leg(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->legrd() > 0)
                                                <tr>
                                                    <td>LEGRD</td>
                                                    <td>{{ $payrollComputation->legrd }}</td>
                                                    <td>{{ number_format($payrollComputation->legrd(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->OTPay() > 0)
                                                <tr>
                                                    <td>OT</td>
                                                    <td>{{ $payrollComputation->regot }}</td>
                                                    <td>{{ number_format($payrollComputation->OTPay(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->NDPay() > 0)
                                                <tr>
                                                    <td>ND</td>
                                                    <td>{{ $payrollComputation->nd }}</td>
                                                    <td>{{ number_format($payrollComputation->NDPay(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->vl() > 0)
                                                <tr>
                                                    <td>VL</td>
                                                    <td>{{ $payrollComputation->vl }}</td>
                                                    <td>{{ number_format($payrollComputation->vl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->sl() > 0)
                                                <tr>
                                                    <td>SL</td>
                                                    <td>{{ $payrollComputation->sl }}</td>
                                                    <td>{{ number_format($payrollComputation->sl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->el() > 0)
                                                <tr>
                                                    <td>EL</td>
                                                    <td>{{ $payrollComputation->el }}</td>
                                                    <td>{{ number_format($payrollComputation->el(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->spl() > 0)
                                                <tr>
                                                    <td>SPL</td>
                                                    <td>{{ $payrollComputation->spl }}</td>
                                                    <td>{{ number_format($payrollComputation->spl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->ml() > 0)
                                                <tr>
                                                    <td>ML</td>
                                                    <td>{{ $payrollComputation->ml }}</td>
                                                    <td>{{ number_format($payrollComputation->ml(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->pl() > 0)
                                                <tr>
                                                    <td>PL</td>
                                                    <td>{{ $payrollComputation->pl }}</td>
                                                    <td>{{ number_format($payrollComputation->pl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->pto() > 0)
                                                <tr>
                                                    <td>PTO</td>
                                                    <td>{{ $payrollComputation->pto }}</td>
                                                    <td>{{ number_format($payrollComputation->pto(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->bl() > 0)
                                                <tr>
                                                    <td>BL</td>
                                                    <td>{{ $payrollComputation->bl }}</td>
                                                    <td>{{ number_format($payrollComputation->bl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->cl() > 0)
                                                <tr>
                                                    <td>CL</td>
                                                    <td>{{ $payrollComputation->cl }}</td>
                                                    <td>{{ number_format($payrollComputation->cl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->ecola > 0)
                                                <tr>
                                                    <td>ECOLA</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->ecola, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->holiday_ecola > 0)
                                                <tr>
                                                    <td>HECOLA</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->holiday_ecola, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->adjustment_wage > 0)
                                                <tr>
                                                    <td>NWAGE</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->adjustment_wage, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->de_minimis > 0)
                                                <tr>
                                                    <td>DE MINIMIS</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->de_minimis, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->thirteenth_month_bonus > 0)
                                                <tr>
                                                    <td>13TH MONTH</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->thirteenth_month_bonus, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->fixed_allowance > 0)
                                                <tr>
                                                    <td>FIXED</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->fixed_allowance, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->leave_encashment > 0)
                                                <tr>
                                                    <td>LEAVE</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->leave_encashment, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->incentives > 0)
                                                <tr>
                                                    <td>INCENTIVES</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->incentives, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->adjustment_income > 0)
                                                    <tr>
                                                        <td>Other Income</td>
                                                        <td></td>
                                                        <td>{{ number_format($payroll->adjustment_income, 2) }}</td>
                                                    </tr>
                                                @endif
                                                @foreach($payroll->Others()->where('type', 'Income') as $x => $income)
                                                    <tr>
                                                        <td>{{ $income->title }}</td>
                                                        <td></td>
                                                        <td>{{ number_format($income->amount, 2) }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        @if($payroll->absences > 0 ||
                                            $payroll->tardiness > 0 ||
                                            $payroll->sss_ee > 0 ||
                                            $payroll->hdmf_ee > 0 ||
                                            $payroll->philhealth_ee > 0 ||
                                            $payroll->sss_loan > 0 ||
                                            $payroll->hdmf_loan > 0 ||
                                            $payroll->client_charges > 0 ||
                                            $payroll->membership_fee > 0 ||
                                            $payroll->notarial_fee > 0 ||
                                            $payroll->coop_loan > 0 ||
                                            $payroll->coop_share > 0)
                                        <div class="col-xs-4">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th width="50%" class=" table-header">Deductions</th>
                                                    <th class="text-center table-header">Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($payroll->absences > 0)
                                                <tr>
                                                    <td>Absences</td>
                                                    <td>{{ number_format($payroll->absences, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->tardiness > 0)
                                                <tr>
                                                    <td>Tardiness</td>
                                                    <td>{{ number_format($payroll->tardiness, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->sss_ee > 0)
                                                <tr>
                                                    <td>SSS EE</td>
                                                    <td>{{ number_format($payroll->sss_ee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->hdmf_ee > 0)
                                                <tr>
                                                    <td>HDMF EE</td>
                                                    <td>{{ number_format($payroll->hdmf_ee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->philhealth_ee > 0)
                                                <tr>
                                                    <td>PhilHealth EE</td>
                                                    <td>{{ number_format($payroll->philhealth_ee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->sss_loan > 0)
                                                <tr>
                                                    <td>SSS Loan</td>
                                                    <td>{{ number_format($payroll->sss_loan, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->hdmf_loan > 0)
                                                <tr>
                                                    <td>HDMF Loan</td>
                                                    <td>{{ number_format($payroll->hdmf_loan, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->client_charges > 0)
                                                <tr>
                                                    <td>Client Charges</td>
                                                    <td>{{ number_format($payroll->client_charges, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->membership_fee > 0)
                                                <tr>
                                                    <td>Membership Fee</td>
                                                    <td>{{ number_format($payroll->membership_fee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->notarial_fee > 0)
                                                <tr>
                                                    <td>Notarial Fee</td>
                                                    <td>{{ number_format($payroll->notarial_fee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->coop_loan > 0)
                                                <tr>
                                                    <td>Coop Loan</td>
                                                    <td>{{ number_format($payroll->coop_loan, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->coop_share > 0)
                                                <tr>
                                                    <td>Coop Share</td>
                                                    <td>{{ number_format($payroll->coop_share, 2) }}</td>
                                                </tr>
                                                @endif


                                                @if($payroll->unpaid_leave > 0)
                                                    <tr>
                                                        <td>Unpaid Leave</td>
                                                        <td>{{ number_format($payroll->unpaid_leave, 2) }}</td>
                                                    </tr>
                                                @endif
                                                @if($payroll->adjustment_deduction > 0)
                                                    <tr>
                                                        <td>Other Deduction</td>
                                                        <td>{{ number_format($payroll->adjustment_deduction, 2) }}</td>
                                                    </tr>
                                                @endif
                                                @foreach($payroll->Others()->where('type', 'Deduction') as $x => $income)
                                                    <tr>
                                                        <td>{{ $income->title }}</td>
                                                        <td>{{ number_format($income->amount, 2) }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            @endif

                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th width="50%" class="table-header">CURRENT NET PAY</th>
                                                    <th class="text-center table-header">Amount</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <tr>
                                                    <th class="table-header">
                                                        <div>GROSS AMOUNT</div>
                                                        <div>DEDUCTIONS</div>
                                                        <div>TAX</div>
                                                    </th>
                                                    <td>
                                                        <div>{{ number_format($payroll->gross_amount, 2) }}</div>
                                                        <div>@if($payroll->deductions > 0){{ number_format($payroll->deductions, 2) }} @else &nbsp; @endif </div>
                                                        <div>@if($payroll->tax > 0){{ number_format($payroll->tax, 2) }} @else &nbsp; @endif </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th class="table-header">NET PAY</th>
                                                    <th class="table-header">{{ number_format($payroll->net_pay, 2) }}</th>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>



                                <?php /*

                                <?php
                                    $left1Sum = $payroll->basic_pay;
                                    $left1 = [];

                                    $left1[] = [
                                        'description' => 'Basic Pay',
                                        'value' => number_format($payroll->basic_pay, 2)
                                    ];

                                  if($payroll->ecola > 0) {
                                      $left1[] = [
                                          'description' => 'ECOLA Tx',
                                          'value' => number_format($payroll->ecola, 2)
                                      ];
                                      $left1Sum += $payroll->ecola;
                                  }

                                  if($payroll->overtime_pay > 0) {
                                      $left1[] = [
                                          'description' => 'Overtime Tx',
                                          'value' => number_format($payroll->overtime_pay, 2)
                                      ];
                                      $left1Sum += $payroll->overtime_pay;
                                  }

                                  if($payroll->holiday_pay > 0) {
                                      $left1[] = [
                                          'description' => 'Holiday Pay Tx',
                                          'value' => number_format($payroll->holiday_pay, 2)
                                      ];
                                      $left1Sum += $payroll->holiday_pay;
                                  }

                                  if($payroll->night_differential > 0) {
                                      $left1[] = [
                                          'description' => 'Night Diff. Tx',
                                          'value' => number_format($payroll->night_differential, 2)
                                      ];
                                      $left1Sum += $payroll->night_differential;
                                  }

                                  if($payroll->holiday_ecola > 0) {
                                      $left1[] = [
                                          'description' => 'ECOLA Hol. Tx',
                                          'value' => number_format($payroll->holiday_ecola, 2)
                                      ];
                                      $left1Sum += $payroll->holiday_ecola;
                                  }

                                  if($payroll->adjustment_wage > 0) {
                                      $left1[] = [
                                          'description' => 'Adj. - Wage Tx',
                                          'value' => number_format($payroll->adjustment_wage, 2)
                                      ];
                                      $left1Sum += $payroll->adjustment_wage;
                                  }

                                  if($payroll->de_minimis > 0) {
                                      $left1[] = [
                                          'description' => 'De Minimis NTx',
                                          'value' => number_format($payroll->de_minimis, 2)
                                      ];
                                      $left1Sum += $payroll->de_minimis;
                                  }

                                  if($payroll->thirteenth_month_bonus > 0) {
                                      $left1[] = [
                                          'description' => '13th Mo. Bonus NTx',
                                          'value' => number_format($payroll->thirteenth_month_bonus, 2)
                                      ];
                                      $left1Sum += $payroll->thirteenth_month_bonus;
                                  }

                                  if($payroll->fixed_allowance > 0) {
                                      $left1[] = [
                                          'description' => 'Fixed Allowance NTx',
                                          'value' => number_format($payroll->fixed_allowance, 2)
                                      ];
                                      $left1Sum += $payroll->fixed_allowance;
                                  }

                                  if($payroll->leave_encashment > 0) {
                                      $left1[] = [
                                          'description' => 'Leave Encashment NTx',
                                          'value' => number_format($payroll->leave_encashment, 2)
                                      ];
                                      $left1Sum += $payroll->leave_encashment;
                                  }

                                  if($payroll->incentives > 0) {
                                      $left1[] = [
                                          'description' => 'Incentives NTx',
                                          'value' => number_format($payroll->incentives, 2)
                                      ];
                                      $left1Sum += $payroll->incentives;
                                  }

                                  if($payroll->adjustment_income > 0) {
                                      $left1[] = [
                                          'description' => 'Adj. - Income NTx',
                                          'value' => number_format($payroll->adjustment_income, 2)
                                      ];
                                      $left1Sum += $payroll->adjustment_income;
                                  }

                                  $left2Sum = $ytd->sum('basic_pay');
                                  $left2 = [
                                      [
                                          'description' => 'Basic Pay',
                                          'value' => number_format($ytd->sum('basic_pay'), 2)
                                      ]
                                  ];

                                  if($ytd->sum('ecola') > 0) {
                                      $left2[] = [
                                          'description' => 'ECOLA Tx',
                                          'value' => number_format($ytd->sum('ecola'), 2)
                                      ];
                                      $left2Sum += $ytd->sum('ecola');
                                  }

                                  if($ytd->sum('overtime_pay') > 0) {
                                    $left2[] = [
                                        'description' => 'Overtime Tx',
                                        'value' => number_format($ytd->sum('overtime_pay'), 2)
                                    ];
                                      $left2Sum += $ytd->sum('overtime_pay');
                                  }

                                  if($ytd->sum('holiday_pay') > 0) {
                                    $left2[] = [
                                        'description' => 'Holiday Tx',
                                        'value' => number_format($ytd->sum('holiday_pay'), 2)
                                    ];
                                      $left2Sum += $ytd->sum('holiday_pay');
                                  }

                                  if($ytd->sum('night_differential') > 0) {
                                    $left2[] = [
                                        'description' => 'Night Diff. Tx',
                                        'value' => number_format($ytd->sum('night_differential'), 2)
                                    ];
                                      $left2Sum += $ytd->sum('night_differential');
                                  }

                                  if($ytd->sum('holiday_ecola') > 0) {
                                    $left2[] = [
                                        'description' => 'ECOLA Hol. Tx',
                                        'value' => number_format($ytd->sum('holiday_ecola'), 2)
                                    ];
                                      $left2Sum += $ytd->sum('holiday_ecola');
                                  }

                                  if($ytd->sum('adjustment_wage') > 0) {
                                    $left2[] = [
                                        'description' => 'Adj. - Wage Tx',
                                        'value' => number_format($ytd->sum('adjustment_wage'), 2)
                                    ];
                                      $left2Sum += $ytd->sum('adjustment_wage');
                                  }

                                  if($ytd->sum('de_minimis') > 0) {
                                    $left2[] = [
                                        'description' => 'De Minimis NTx',
                                        'value' => number_format($ytd->sum('de_minimis'), 2)
                                    ];
                                      $left2Sum += $ytd->sum('de_minimis');
                                  }

                                  if($ytd->sum('thirteenth_month_bonus') > 0) {
                                    $left2[] = [
                                        'description' => '13th Month Bonus NTx',
                                        'value' => number_format($ytd->sum('thirteenth_month_bonus'), 2)
                                    ];
                                      $left2Sum += $ytd->sum('thirteenth_month_bonus');
                                  }

                                  if($ytd->sum('fixed_allowance') > 0) {
                                    $left2[] = [
                                        'description' => 'Fixed Allowance NTx',
                                        'value' => number_format($ytd->sum('fixed_allowance'), 2)
                                    ];
                                      $left2Sum += $ytd->sum('fixed_allowance');
                                  }

                                  if($ytd->sum('leave_encashment') > 0) {
                                    $left2[] = [
                                        'description' => 'Leave Encashment NTx',
                                        'value' => number_format($ytd->sum('leave_encashment'), 2)
                                    ];
                                      $left2Sum += $ytd->sum('leave_encashment');
                                  }

                                  if($ytd->sum('incentives') > 0) {
                                    $left2[] = [
                                        'description' => 'Incentives NTx',
                                        'value' => number_format($ytd->sum('incentives'), 2)
                                    ];
                                      $left2Sum += $ytd->sum('incentives');
                                  }

                                  if($ytd->sum('adjustment_income') > 0) {
                                    $left2[] = [
                                        'description' => 'Adj. - Income NTx',
                                        'value' => number_format($ytd->sum('adjustment_income'), 2)
                                    ];
                                      $left2Sum += $ytd->sum('adjustment_income');
                                  }


                                  $right1Sum = $payroll->hdmf_er;
                                  $right1 = [];

                                  if($payroll->hdmf_er > 0) {
                                      $right1[] = [
                                          'description' => 'HDMF Contri - ER Share',
                                          'value' => $payroll->hdmf_er
                                      ];
                                      $right1Sum += $payroll->hdmf_er;
                                  }

                                  if($payroll->philhealth_er > 0) {
                                      $right1[] = [
                                          'description' => 'Philhealth Contri - ER Share',
                                          'value' => $payroll->philhealth_er
                                      ];
                                      $right1Sum += $payroll->philhealth_er;
                                  }

                                  if($payroll->sss_er > 0) {
                                      $right1[] = [
                                          'description' => 'SSS Contri - ER Share',
                                          'value' => $payroll->sss_er
                                      ];
                                      $right1Sum += $payroll->sss_er;
                                  }

                                $right2Sum = $ytd->sum('hdmf_er');
                                $right2 = [];

                                if($ytd->sum('hdmf_er') > 0) {
                                    $right2[] = [
                                        'description' => 'HDMF Contri - ER Share',
                                        'value' => number_format($ytd->sum('hdmf_er'), 2)
                                    ];
                                    $right2Sum += $ytd->sum('hdmf_er');
                                }

                                if($ytd->sum('philhealth_er') > 0) {
                                    $right2[] = [
                                        'description' => 'Philhealth Contri - ER Share',
                                        'value' => number_format($ytd->sum('philhealth_er'), 2)
                                    ];
                                    $right2Sum += $ytd->sum('philhealth_er');
                                }

                                if($ytd->sum('sss_er') > 0) {
                                    $right2[] = [
                                        'description' => 'SSS Contri - ER Share',
                                        'value' => number_format($ytd->sum('sss_er'), 2)
                                    ];
                                    $right2Sum += $ytd->sum('sss_er');
                                }

                                ?>

                                <div class="col-xs-12">
                                    <div class="col-xs-12">
                                        <table class="table-bordered table-condensed" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" colspan="4">HOURS AND EARNINGS</th>
                                                    <th class="text-center" colspan="4">EMPLOYER PAID BENEFITS</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center" colspan="2">CURRENT</th>
                                                    <th class="text-center" colspan="2">YTD</th>
                                                    <th class="text-center" colspan="2">CURRENT</th>
                                                    <th class="text-center" colspan="2">YTD</th>
                                                </tr>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Earnings</th>
                                                    <th>Description</th>
                                                    <th>Earnings</th>

                                                    <th>Description</th>
                                                    <th>Current</th>
                                                    <th>Description</th>
                                                    <th>YTD</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $rows = count($left1) > count($left2) ? count($left1) : count($left2);
                                                $rows = count($left1) > count($right1) ? count($left1) : count($right1);
                                                $rows = count($left1) > count($right2) ? count($left1) : count($right2);
                                                ?>
                                                @for($x = 0; $x < $rows; $x++)
{{--                                                    {{ count($left1) ." < ". count(count($left2)) . " {$rows} " }}--}}
                                                    @if(isset($left1[$x]) || isset($left2[$x]) || isset($right1[$x]) || isset($right2[$x]))
                                                        <tr>
                                                            @if(isset($left1[$x]))
                                                                <td>{{ $left1[$x]['description'] }}</td>
                                                                <td>{{ $left1[$x]['value'] }}</td>
                                                            @else
                                                                <td>{{ $x }}</td>
                                                                <td></td>
                                                            @endif

                                                            @if(!empty($left2[$x]))
                                                                <td>{{ $left2[$x]['description'] }}</td>
                                                                <td>{{ $left2[$x]['value'] }}</td>
                                                            @else
                                                                <td></td>
                                                                <td></td>
                                                            @endif

                                                            @if(!empty($right1[$x]))
                                                                <td>{{ $right1[$x]['description'] }}</td>
                                                                <td>{{ $right1[$x]['value'] }}</td>
                                                            @else
                                                                <td></td>
                                                                <td></td>
                                                            @endif

                                                            @if(!empty($right2[$x]))
                                                                <td>{{ $right2[$x]['description'] }}</td>
                                                                <td>{{ $right2[$x]['value'] }}</td>
                                                            @else
                                                                <td></td>
                                                                <td></td>
                                                            @endif
                                                        </tr>
                                                    @endif
                                                @endfor
                                            </tbody>
                                            <?php
                                                $totalLeft1 = 0;
                                                $totalLeft1 += $left1Sum;
                                                $totalLeft2 = 0;
                                                $totalLeft2 += $left2Sum;
                                                $totalRight1 = 0;
                                                $totalRight1 += $right1Sum;
                                                $totalRight2 = 0;
                                                $totalRight2 += $right2Sum;
                                            ?>
                                            <tfoot>
                                                <tr>
                                                    <th>Total</th>
                                                    <th>@if($left1Sum > 0) {{ number_format($left1Sum, 2) }}  @endif</th>
                                                    <td></td>
                                                    <th>@if($left2Sum > 0) {{ number_format($left2Sum, 2) }}  @endif</th>

                                                    <td></td>
                                                    <th>@if($right1Sum > 0) {{ number_format($right1Sum, 2) }}  @endif</th>
                                                    <td></td>
                                                    <th>@if($right2Sum > 0) {{ number_format($right2Sum, 2) }}  @endif</th>

                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>

                                    <?php
                                    $left1Sum = 0;
                                    $left1 = [];

                                    if($payroll->sss_ee > 0) {
                                        $left1[] = [
                                            'description' => 'SSS EE',
                                            'value' => number_format($payroll->sss_ee, 2)
                                        ];
                                        $left1Sum += $payroll->sss_ee;
                                    }

                                    if($payroll->hdmf_ee > 0) {
                                        $left1[] = [
                                            'description' => 'HDMF EE',
                                            'value' => number_format($payroll->hdmf_ee, 2)
                                        ];
                                        $left1Sum += $payroll->hdmf_ee;
                                    }

                                    if($payroll->philhealth_ee > 0) {
                                        $left1[] = [
                                            'description' => 'Philhealth EE',
                                            'value' => number_format($payroll->philhealth_ee, 2)
                                        ];
                                        $left1Sum += $payroll->philhealth_ee;
                                    }

                                    if($payroll->tardiness > 0) {
                                        $left1[] = [
                                            'description' => 'Tardiness',
                                            'value' => number_format($payroll->tardiness, 2)
                                        ];
                                        $left1Sum += $payroll->tardiness;
                                    }

                                    if($payroll->absences > 0) {
                                        $left1[] = [
                                            'description' => 'Absences',
                                            'value' => number_format($payroll->absences, 2)
                                        ];
                                        $left1Sum += $payroll->absences;
                                    }

                                    $left2Sum = 0;
                                    $left2 = [];

                                    if($ytd->sum('sss_ee') > 0) {
                                        $left2[] = [
                                            'description' => 'SSS EE',
                                            'value' => number_format($ytd->sum('sss_ee'), 2)
                                        ];
                                        $left2Sum += $ytd->sum('sss_ee');
                                    }

                                    if($ytd->sum('hdmf_ee') > 0) {
                                        $left2[] = [
                                            'description' => 'HDMF EE',
                                            'value' => number_format($ytd->sum('hdmf_ee'), 2)
                                        ];
                                        $left2Sum += $ytd->sum('hdmf_ee');
                                    }

                                    if($ytd->sum('philhealth_ee') > 0) {
                                        $left2[] = [
                                            'description' => 'Philhealth EE',
                                            'value' => number_format($ytd->sum('philhealth_ee'), 2)
                                        ];
                                        $left2Sum += $ytd->sum('philhealth_ee');
                                    }

                                    if($ytd->sum('tardiness') > 0) {
                                        $left2[] = [
                                            'description' => 'Tardiness',
                                            'value' => number_format($ytd->sum('tardiness'), 2)
                                        ];
                                        $left2Sum += $ytd->sum('tardiness');
                                    }

                                    if($ytd->sum('absences') > 0) {
                                        $left2[] = [
                                            'description' => 'Absences',
                                            'value' => number_format($ytd->sum('absences'), 2)
                                        ];
                                        $left2Sum += $ytd->sum('absences');
                                    }


                                    $right1Sum = 0;
                                    $right1 = [];

                                    if($payroll->sss_loan > 0) {
                                        $right1[] = [
                                            'description' => 'SSS Loan',
                                            'value' => number_format($payroll->sss_loan, 2)
                                        ];
                                        $right1Sum += $payroll->sss_loan;
                                    }

                                    if($payroll->hdmf_loan > 0) {
                                        $right1[] = [
                                            'description' => 'HDMF Loan',
                                            'value' => number_format($payroll->hdmf_loan, 2)
                                        ];
                                        $right1Sum += $payroll->hdmf_loan;
                                    }

                                    if($payroll->client_charges > 0) {
                                        $right1[] = [
                                            'description' => 'Client Charges',
                                            'value' => number_format($payroll->client_charges, 2)
                                        ];
                                        $right1Sum += $payroll->client_charges;
                                    }

                                    if($payroll->membership_fee > 0) {
                                        $right1[] = [
                                            'description' => 'Membership Fee',
                                            'value' => number_format($payroll->membership_fee, 2)
                                        ];
                                        $right1Sum += $payroll->membership_fee;
                                    }

                                    if($payroll->notarial_fee > 0) {
                                        $right1[] = [
                                            'description' => 'Notarial Fee',
                                            'value' => number_format($payroll->notarial_fee, 2)
                                        ];
                                        $right1Sum += $payroll->notarial_fee;
                                    }

                                    if($payroll->coop_share > 0) {
                                        $right1[] = [
                                            'description' => 'Coop Share',
                                            'value' => number_format($payroll->coop_share, 2)
                                        ];
                                        $right1Sum += $payroll->coop_share;
                                    }

                                    if($payroll->coop_loan > 0) {
                                        $right1[] = [
                                            'description' => 'Coop Loan',
                                            'value' => number_format($payroll->coop_loan, 2)
                                        ];
                                        $right1Sum += $payroll->coop_loan;
                                    }

                                    if($payroll->adjustment_deduction > 0) {
                                        $right1[] = [
                                            'description' => 'Adj. - Deduction',
                                            'value' => number_format($payroll->adjustment_deduction, 2)
                                        ];
                                        $right1Sum += $payroll->adjustment_deduction;
                                    }

//                                    if($payroll->appliance_loan > 0) {
//                                        $right1[] = [
//                                            'description' => 'Appliance Loan',
//                                            'value' => number_format($payroll->appliance_loan, 2)
//                                        ];
//                                        $right1Sum += $payroll->appliance_loan;
//                                    }
//
//                                    if($payroll->educational_loan > 0) {
//                                        $right1[] = [
//                                            'description' => 'Educcational Loan',
//                                            'value' => number_format($payroll->educational_loan, 2)
//                                        ];
//                                        $right1Sum += $payroll->educational_loan;
//                                    }
//
//                                    if($payroll->emergency_loan > 0) {
//                                        $right1[] = [
//                                            'description' => 'Emergency Loan',
//                                            'value' => number_format($payroll->emergency_loan, 2)
//                                        ];
//                                        $right1Sum += $payroll->emergency_loan;
//                                    }


                                    $right2Sum = 0;
                                    $right2 = [];

                                    if($ytd->sum('sss_loan') > 0) {
                                        $right2[] = [
                                            'description' => 'SSS Loan',
                                            'value' => number_format($ytd->sum('sss_loan'), 2)
                                        ];
                                        $right2Sum += $ytd->sum('sss_loan');
                                    }

                                    if($ytd->sum('hdmf_loan') > 0) {
                                        $right2[] = [
                                            'description' => 'HDMF Loan',
                                            'value' => number_format($ytd->sum('hdmf_loan'), 2)
                                        ];
                                        $right2Sum += $ytd->sum('hdmf_loan');
                                    }

                                    if($ytd->sum('client_charges') > 0) {
                                        $right2[] = [
                                            'description' => 'Client Charges',
                                            'value' => number_format($ytd->sum('client_charges'), 2)
                                        ];
                                        $right2Sum += $ytd->sum('client_charges');
                                    }

                                    if($ytd->sum('membership_fee') > 0) {
                                        $right2[] = [
                                            'description' => 'Membership Fee',
                                            'value' => number_format($ytd->sum('membership_fee'), 2)
                                        ];
                                        $right2Sum += $ytd->sum('membership_fee');
                                    }

                                    if($ytd->sum('notarial_fee') > 0) {
                                        $right2[] = [
                                            'description' => 'Notarial Fee',
                                            'value' => number_format($ytd->sum('notarial_fee'), 2)
                                        ];
                                        $right2Sum += $ytd->sum('notarial_fee');
                                    }

                                    if($ytd->sum('coop_share') > 0) {
                                        $right2[] = [
                                            'description' => 'Coop Share',
                                            'value' => number_format($ytd->sum('coop_share'), 2)
                                        ];
                                        $right2Sum += $ytd->sum('coop_share');
                                    }

                                    if($ytd->sum('coop_loan') > 0) {
                                        $right2[] = [
                                            'description' => 'Coop Loan',
                                            'value' => number_format($ytd->sum('coop_loan'), 2)
                                        ];
                                        $right2Sum += $ytd->sum('coop_loan');
                                    }

                                    if($ytd->sum('adjustment_deduction') > 0) {
                                        $right2[] = [
                                            'description' => 'Adj. - Deduction',
                                            'value' => number_format($ytd->sum('adjustment_deduction'), 2)
                                        ];
                                        $right2Sum += $ytd->sum('adjustment_deduction');
                                    }

//                                    if($ytd->sum('appliance_loan') > 0) {
//                                        $right2[] = [
//                                            'description' => 'Appliance Loan',
//                                            'value' => number_format($ytd->sum('appliance_loan'), 2)
//                                        ];
//                                        $right2Sum += $ytd->sum('appliance_loan');
//                                    }
//
//                                    if($ytd->sum('educational_loan') > 0) {
//                                        $right2[] = [
//                                            'description' => 'Educcational Loan',
//                                            'value' => number_format($ytd->sum('educational_loan'), 2)
//                                        ];
//                                        $right2Sum += $ytd->sum('educational_loan');
//                                    }
//
//                                    if($ytd->sum('emergency_loan') > 0) {
//                                        $right2[] = [
//                                            'description' => 'Emergency Loan',
//                                            'value' => number_format($ytd->sum('emergency_loan'), 2)
//                                        ];
//                                        $right2Sum += $ytd->sum('emergency_loan');
//                                    }


                                    ?>
                                    <div class="col-xs-12 push-10-t">
                                        <table class="table-bordered table-condensed" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" colspan="4">BEFORE TAX DEDUCTIONS</th>
                                                    <th class="text-center" colspan="4">AFTER TAX DEDUCTIONS</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center" colspan="2">CURRENT</th>
                                                    <th class="text-center" colspan="2">YTD</th>
                                                    <th class="text-center" colspan="2">CURRENT</th>
                                                    <th class="text-center" colspan="2">YTD</th>
                                                </tr>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Current</th>
                                                    <th>Description</th>
                                                    <th>YTD</th>

                                                    <th>Description</th>
                                                    <th>Current</th>
                                                    <th>Description</th>
                                                    <th>YTD</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php

                                                $count = count($left1) > count($left2) ? count($left1) : count($left2);
                                                $count = count($left1) > count($right1) ? count($left1) : count($right1);
                                                $count = count($left1) > count($right2) ? count($left1) : count($right2);
                                                ?>
                                                @for($x = 0; $x < $count; $x++)
                                                    <tr>
                                                        @if(!empty($left1[$x]))
                                                            <td>{{ $left1[$x]['description'] }}</td>
                                                            <td>{{ $left1[$x]['value'] }}</td>
                                                        @else
                                                            <td></td>
                                                            <td></td>
                                                        @endif

                                                        @if(!empty($left2[$x]))
                                                            <td>{{ $left2[$x]['description'] }}</td>
                                                            <td>{{ $left2[$x]['value'] }}</td>
                                                        @else
                                                            <td></td>
                                                            <td></td>
                                                        @endif

                                                        @if(!empty($right1[$x]))
                                                            <td>{{ $right1[$x]['description'] }}</td>
                                                            <td>{{ $right1[$x]['value'] }}</td>
                                                        @else
                                                            <td></td>
                                                            <td></td>
                                                        @endif

                                                        @if(!empty($right2[$x]))
                                                            <td>{{ $right2[$x]['description'] }}</td>
                                                            <td>{{ $right2[$x]['value'] }}</td>
                                                        @else
                                                            <td></td>
                                                            <td></td>
                                                        @endif
                                                    </tr>
                                                @endfor
                                            </tbody>

                                            <?php
                                            $totalLeft1 += $left1Sum;
                                            $totalLeft2 += $left2Sum;
                                            $totalRight1 += $right1Sum;
                                            $totalRight2 += $right2Sum;
                                            ?>
                                            <tfoot>
                                                <tr>
                                                    <th>Total</th>
                                                    <th>@if($left1Sum > 0) {{ number_format($left1Sum, 2) }}  @endif</th>
                                                    <td></td>
                                                    <th>@if($left2Sum > 0) {{ number_format($left2Sum, 2) }}  @endif</th>

                                                    <td></td>
                                                    <th>@if($right1Sum > 0) {{ number_format($right1Sum, 2) }}  @endif</th>
                                                    <td></td>
                                                    <th>@if($right2Sum > 0) {{ number_format($right2Sum, 2) }}  @endif</th>

                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>

                                    <div class="col-xs-12 push-10-t">
                                        <table class="table-bordered table-condensed" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th width="12.5%"></th>
                                                    <th width="12.5%" colspan="2">Total Gross</th>
                                                    <th width="12.5%" colspan="2">Total Taxes</th>
                                                    <th width="12.5%" colspan="2">Total Deductions</th>
                                                    <th width="12.5%">Net Pay</th>
                                                </tr>
                                                <tr>
                                                    <th>Current</th>
                                                    <th colspan="2">{{ number_format($payroll->gross_amount, 2) }}</th>
                                                    <th colspan="2">{{ number_format($payroll->tax, 2) }}</th>
                                                    <th colspan="2">{{ number_format($payroll->deductions, 2) }}</th>
                                                    <th>{{ number_format($payroll->net_pay, 2) }}</th>
                                                </tr>
                                                <tr>
                                                    <th>YTD</th>
                                                    <th colspan="2">{{ number_format($ytd->sum('gross_amount'), 2) }}</th>
                                                    <th colspan="2">{{ number_format($ytd->sum('tax'), 2) }}</th>
                                                    <th colspan="2">{{ number_format($ytd->sum('deductions'), 2) }}</th>
                                                    <th>{{ number_format($ytd->sum('net_pay'), 2) }}</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>

                                    <div class="col-xs-offset-7 col-xs-5 push-5-t">
                                        <table class="table-bordered table-condensed" style="width: 100%;">
                                            <tr>
                                                <th width="50%">NET PAY :</th>
                                                <th width="50%">{{ number_format($payroll->net_pay, 2) }}</th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                */ ?>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            window.print();
        })
    </script>
@endsection

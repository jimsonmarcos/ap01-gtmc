@extends('layouts.print')

@section('content')

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            @foreach($dtr as $d)
                            <?php
                                $user = $d->User;
                                $payroll = \App\PayrollSummary::where(['payroll_cycle_id' => $d->payroll_cycle_id, 'user_id' => $d->User->id])->first();

                                if(!empty($id_numbers) && !empty($d->User->Profile->id_number) && !in_array($d->User->Profile->id_number, $id_numbers)) {
                                    ?>
                                    @continue
                                    <?php
                                }

                                if ($d->User->Profile->Compensation->rate_type == 'Monthly') {
                                    $payrollComputation = new \App\ComputePayroll($d, ['daily_rate' => $d->User->Profile->Compensation->monthly_rate / 26, 'monthly_rate' => $d->User->Profile->Compensation->monthly_rate / 2, 'rate_type' => 'Monthly']);
                                } else {
                                    if ($d->User->Profile->daily_category == 'Location') {
                                        $payrollComputation = new \App\ComputePayroll($d, $d->User->Profile->Compensation->DailyRate->rate);
                                    } else {
                                        $payrollComputation = new \App\ComputePayroll($d, $d->User->Profile->Compensation->daily_rate);
                                    }
                                }

                                $ytd = \App\PayrollSummary::whereIn('payroll_cycle_id', \App\PayrollCycles::where('status', 'Processed')->whereYear('payroll_date', date('Y', strtotime($payrollCycle->payroll_date)))->pluck('id'))->where('user_id', $d->User->id)->get();
                                //        dd(PayrollCycles::where('status', 'Processed')->whereYear('payroll_date', date('Y', strtotime( $data['payrollCycle']->payroll_date)))->pluck('id'));

                                $ytdPayrollCycles = \App\PayrollCycles::where(['payroll_group_id' => $user->Profile->Compensation->PayrollGroup->id, 'effective_year' => $payrollCycle->effective_year])->pluck('id');
                                $ytdPayrollSummary = \App\PayrollSummary::whereIn('payroll_cycle_id', $ytdPayrollCycles)->get();

                            ?>
                            <div class="row" style="height: 468px;">
                                <br />

                                <div class="col-xs-12 text-center">
                                    <h3>{{ $user->Profile->EmploymentDetails->Principal->company_name }}</h3>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <hr>
                                    <h4><strong>PAYSLIP</strong></h4>
                                    <hr>
                                </div>

                                <div class="col-xs-12">
                                    <div class="col-xs-6">
                                        <p><strong>Name:</strong> {{ $user->name  }}</p>
                                    </div>
                                    @if($user->Profile->Compensation->rate_type == 'Monthly')
                                        <div class="col-xs-6">
                                            <p><strong>Semi-monthly Rate:</strong> {{ number_format($payrollComputation->monthlyRate, 2)  }}</p>
                                        </div>
                                    @else
                                        <div class="col-xs-6">
                                            <p><strong>Daily Rate:</strong> {{ number_format($payrollComputation->dailyRate, 2)  }}</p>
                                        </div>
                                    @endif

                                    <div class="col-xs-6">
                                        <p><strong>Employee ID:</strong> {{ $user->Profile->id_number  }}</p>
                                    </div>

                                    <div class="col-xs-6">
                                        <p><strong>Payroll Date:</strong> {{ date('m/d/Y', strtotime($payrollCycle->payroll_date))  }}</p>
                                    </div>

                                    <div class="col-xs-6">
                                        <p style="margin-bottom: 0;"><strong>Cost Center:</strong> {{ $payrollCycle->PayrollGroup->group_name }} ({{ $payrollCycle->PayrollGroup->Company->company_code }}) </p>
                                        {{--{{ $user->Profile->CostCenter->cost_center  }}--}}
                                    </div>

                                    <div class="col-xs-6">
                                        <p style="margin-bottom: 0;"><strong>Payroll Period:</strong> {{ $payrollCycle->payroll_period  }}</p>
                                    </div>
                                </div>

                                <style>
                                    p {
                                        line-height: .1 !important;
                                    }

                                    @media print {
                                        th, td {
                                            font-size: 10px;
                                            padding-top: 0px !important;
                                            padding-bottom: 0px !important;
                                        }

                                        .table-header {
                                            font-size: 10px !important;
                                        }

                                        p {
                                            font-size: 11px;
                                        }

                                        h4 {
                                            font-size: 14px;
                                        }

                                        h3 {
                                            font-size: 18px;
                                        }

                                        #main-content {
                                            padding-top: 0 !important;
                                            padding-bottom: 0 !important;
                                            margin-top: 0 !important;
                                        }
                                    }

                                    hr {
                                        margin-top: 10px;
                                        margin-bottom: 10px;
                                    }
                                </style>

                                <div class="col-xs-12">
                                    <hr />
                                    <div class="row">
                                        <div class="col-xs-4">
                                            @if($payroll->sss_er > 0 || $payroll->sss_ec > 0 || $payroll->hdmf_er > 0 || $payroll->philhealth_er > 0)
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center table-header" colspan="2">Employer Paid Benefits</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if($payroll->sss_er > 0)
                                                <tr>
                                                    <td width="50%">SSS ER</td>
                                                    <td>{{ number_format($payroll->sss_er, 2) }}</td>
                                                </tr>
                                                @endif

                                                @if($payroll->sss_ec > 0)
                                                <tr>
                                                    <td>SSS EC</td>
                                                    <td>{{ number_format($payroll->sss_ec, 2) }}</td>
                                                </tr>
                                                @endif

                                                @if($payroll->hdmf_er > 0)
                                                <tr>
                                                    <td>HDMF ER</td>
                                                    <td>{{ number_format($payroll->hdmf_er, 2) }}</td>
                                                </tr>
                                                @endif

                                                @if($payroll->philhealth_er > 0)
                                                <tr>
                                                    <td>PhilHealth ER</td>
                                                    <td>{{ number_format($payroll->philhealth_er, 2) }}</td>
                                                </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                            @endif


                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th colspan="2" class="text-center table-header">Year to Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($ytdPayrollSummary->sum('gross_amount') > 0)
                                                <tr>
                                                    <td width="50%">Gross Amount</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('gross_amount'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('deductions') > 0)
                                                <tr>
                                                    <td>Deductions</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('deductions'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('tax') > 0)
                                                <tr>
                                                    <td>Tax</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('tax'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('basic_pay') > 0)
                                                <tr>
                                                    <td>BASIC</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('basic_pay'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('de_minimis') > 0)
                                                <tr>
                                                    <td>De Minimis</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('de_minimis'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('fixed_allowance') > 0)
                                                <tr>
                                                    <td>Fixed Allowance</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('fixed_allowance'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('incentives') > 0)
                                                <tr>
                                                    <td>Incentives</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('incentives'), 2) }}</td>
                                                </tr>
                                                @endif
                                                {{--@if($ytdPayrollSummary->sum('sss_er') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>SSS ER</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('sss_er'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('sss_ec') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>SSS EC</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('sss_ec'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('hdmf_er') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>HDMF ER</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('hdmf_er'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('philhealth_er') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>PhilHealth ER</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('philhealth_er'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('sss_ee') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>SSS EE</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('sss_ee'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('hdmf_ee') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>HDMF EE</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('hdmf_ee'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                {{--@if($ytdPayrollSummary->sum('philhealth_ee') > 0)--}}
                                                {{--<tr>--}}
                                                    {{--<td>PhilHealth EE</td>--}}
                                                    {{--<td>{{ number_format($ytdPayrollSummary->sum('philhealth_ee'), 2) }}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endif--}}
                                                @if($ytdPayrollSummary->sum('sss_loan') > 0)
                                                <tr>
                                                    <td>SSS Loan</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('sss_loan'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('hdmf_loan') > 0)
                                                <tr>
                                                    <td>HDMF Loan</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('hdmf_loan'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('coop_loan') > 0)
                                                <tr>
                                                    <td>COOP Loan</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('coop_loan'), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($ytdPayrollSummary->sum('coop_share') > 0)
                                                <tr>
                                                    <td>COOP Share</td>
                                                    <td>{{ number_format($ytdPayrollSummary->sum('coop_share'), 2) }}</td>
                                                </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-xs-4">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th width="60%" class="table-header">Hours and Earnings</th>
                                                    <th class="text-center table-header">Days</th>
                                                    <th width="30%" class="text-center table-header">Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($payroll->basic_pay > 0)
                                                <tr>
                                                    <td>BASIC</td>
                                                    <td>{{ round($payrollComputation->rg() / 26, 2) }}</td>
                                                    <td>{{ number_format($payroll->basic_pay, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->rd() > 0)
                                                <tr>
                                                    <td>RD</td>
                                                    <td>{{ round($payrollComputation->rd / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->rd(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->spe() > 0)
                                                <tr>
                                                    <td>SPE</td>
                                                    <td>{{ round($payrollComputation->sperd / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->spe(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->sperd() > 0)
                                                <tr>
                                                    <td>SPERD</td>
                                                    <td>{{ round($payrollComputation->sperd / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->sperd(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->leg() > 0)
                                                <tr>
                                                    <td>LEG</td>
                                                    <td>{{ round($payrollComputation->leg / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->leg(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->legrd() > 0)
                                                <tr>
                                                    <td>LEGRD</td>
                                                    <td>{{ round($payrollComputation->legrd / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->legrd(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->OTPay() > 0)
                                                <tr>
                                                    <td>OT</td>
                                                    <td>{{ round($payrollComputation->regot / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->OTPay(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->NDPay() > 0)
                                                <tr>
                                                    <td>ND</td>
                                                    <td>{{ round($payrollComputation->nd / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->NDPay(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->vl() > 0)
                                                <tr>
                                                    <td>VL</td>
                                                    <td>{{ round($payrollComputation->vl / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->vl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->sl() > 0)
                                                <tr>
                                                    <td>SL</td>
                                                    <td>{{ round($payrollComputation->sl / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->sl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->el() > 0)
                                                <tr>
                                                    <td>EL</td>
                                                    <td>{{ round($payrollComputation->el / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->el(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->spl() > 0)
                                                <tr>
                                                    <td>SPL</td>
                                                    <td>{{ round($payrollComputation->spl / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->spl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->ml() > 0)
                                                <tr>
                                                    <td>ML</td>
                                                    <td>{{ round($payrollComputation->ml / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->ml(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->pl() > 0)
                                                <tr>
                                                    <td>PL</td>
                                                    <td>{{ round($payrollComputation->pl / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->pl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->pto() > 0)
                                                <tr>
                                                    <td>PTO</td>
                                                    <td>{{ round($payrollComputation->pto / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->pto(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->bl() > 0)
                                                <tr>
                                                    <td>BL</td>
                                                    <td>{{ round($payrollComputation->bl / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->bl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payrollComputation->cl() > 0)
                                                <tr>
                                                    <td>CL</td>
                                                    <td>{{ round($payrollComputation->cl / 26, 2) }}</td>
                                                    <td>{{ number_format($payrollComputation->cl(), 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->ecola > 0)
                                                <tr>
                                                    <td>ECOLA</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->ecola, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->holiday_ecola > 0)
                                                <tr>
                                                    <td>HECOLA</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->holiday_ecola, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->adjustment_wage > 0)
                                                <tr>
                                                    <td>NWAGE</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->adjustment_wage, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->de_minimis > 0)
                                                <tr>
                                                    <td>DE MINIMIS</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->de_minimis, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->thirteenth_month_bonus > 0)
                                                <tr>
                                                    <td>13TH MONTH</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->thirteenth_month_bonus, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->fixed_allowance > 0)
                                                <tr>
                                                    <td>FIXED</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->fixed_allowance, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->leave_encashment > 0)
                                                <tr>
                                                    <td>LEAVE</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->leave_encashment, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->incentives > 0)
                                                <tr>
                                                    <td>INCENTIVES</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->incentives, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->adji > 0)
                                                <tr>
                                                    <td>Other Income</td>
                                                    <td></td>
                                                    <td>{{ number_format($payroll->adji, 2) }}</td>
                                                </tr>
                                                @endif

                                                @foreach($payroll->Others()->where('type', 'Income') as $x => $income)
                                                    <tr>
                                                        <td>{{ $income->title }}</td>
                                                        <td></td>
                                                        <td>{{ number_format($income->amount, 2) }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        @if($payroll->absences > 0 ||
                                            $payroll->tardiness > 0 ||
                                            $payroll->sss_ee > 0 ||
                                            $payroll->hdmf_ee > 0 ||
                                            $payroll->philhealth_ee > 0 ||
                                            $payroll->sss_loan > 0 ||
                                            $payroll->hdmf_loan > 0 ||
                                            $payroll->client_charges > 0 ||
                                            $payroll->membership_fee > 0 ||
                                            $payroll->notarial_fee > 0 ||
                                            $payroll->coop_loan > 0 ||
                                            $payroll->coop_share > 0)
                                        <div class="col-xs-4">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th width="50%" class=" table-header">Deductions</th>
                                                    <th class="text-center table-header">Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($payroll->absences > 0)
                                                <tr>
                                                    <td>Absences</td>
                                                    <td>{{ number_format($payroll->absences, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->tardiness > 0)
                                                <tr>
                                                    <td>Tardiness</td>
                                                    <td>{{ number_format($payroll->tardiness, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->sss_ee > 0)
                                                <tr>
                                                    <td>SSS EE</td>
                                                    <td>{{ number_format($payroll->sss_ee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->hdmf_ee > 0)
                                                <tr>
                                                    <td>HDMF EE</td>
                                                    <td>{{ number_format($payroll->hdmf_ee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->philhealth_ee > 0)
                                                <tr>
                                                    <td>PhilHealth EE</td>
                                                    <td>{{ number_format($payroll->philhealth_ee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->sss_loan > 0)
                                                <tr>
                                                    <td>SSS Loan</td>
                                                    <td>{{ number_format($payroll->sss_loan, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->hdmf_loan > 0)
                                                <tr>
                                                    <td>HDMF Loan</td>
                                                    <td>{{ number_format($payroll->hdmf_loan, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->client_charges > 0)
                                                <tr>
                                                    <td>Client Charges</td>
                                                    <td>{{ number_format($payroll->client_charges, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->membership_fee > 0)
                                                <tr>
                                                    <td>Membership Fee</td>
                                                    <td>{{ number_format($payroll->membership_fee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->notarial_fee > 0)
                                                <tr>
                                                    <td>Notarial Fee</td>
                                                    <td>{{ number_format($payroll->notarial_fee, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->coop_loan > 0)
                                                <tr>
                                                    <td>Coop Loan</td>
                                                    <td>{{ number_format($payroll->coop_loan, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->coop_share > 0)
                                                <tr>
                                                    <td>Coop Share</td>
                                                    <td>{{ number_format($payroll->coop_share, 2) }}</td>
                                                </tr>
                                                @endif
                                                @if($payroll->adji > 0)
                                                    <tr>
                                                        <td>Other Deduction</td>
                                                        <td></td>
                                                        <td>{{ number_format($payroll->adjd, 2) }}</td>
                                                    </tr>
                                                @endif

                                                @foreach($payroll->Others()->where('type', 'Deduction') as $x => $income)
                                                    <tr>
                                                        <td>{{ $income->title }}</td>
                                                        <td>{{ number_format($income->amount, 2) }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            @endif

                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th width="50%" class="table-header">CURRENT NET PAY</th>
                                                    <th class="text-center table-header">Amount</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <tr>
                                                    <th class="table-header">
                                                        <div>GROSS AMOUNT</div>
                                                        @if($payroll->deductions > 0)<div>DEDUCTIONS</div> @endif
                                                        @if($payroll->tax > 0)<div>TAX</div> @endif
                                                    </th>
                                                    <td>
                                                        <div>{{ number_format($payroll->gross_amount, 2) }}</div>
                                                        @if($payroll->deductions > 0)<div>{{ number_format($payroll->deductions, 2) }}</div> @endif
                                                        @if($payroll->tax > 0)<div>{{ number_format($payroll->tax, 2) }}</div> @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th class="table-header">NET PAY</th>
                                                    <th class="table-header">{{ number_format($payroll->net_pay, 2) }}</th>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <hr>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            window.print();
        })
    </script>
@endsection

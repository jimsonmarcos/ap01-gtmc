@extends('layouts.print')

@section('content')

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-xs-12 text-center">
                                    <h2>{{ $user->Profile->EmploymentDetails->Principal->company_name }}</h2>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <hr>
                                    <h3><strong>PAYSLIP</strong></h3>
                                    <hr>
                                </div>

                                <div class="col-xs-12">
                                    <div class="col-xs-6">
                                        <p><strong>Name:</strong> {{ $user->name  }}</p>
                                    </div>
                                    @if($user->Profile->Compensation->rate_type == 'Monthly')
                                        <div class="col-xs-6">
                                            <p><strong>Semi-monthly Rate:</strong> {{ number_format($payrollComputation->monthlyRate, 2)  }}</p>
                                        </div>
                                    @else
                                        <div class="col-xs-6">
                                            <p><strong>Daily Rate:</strong> {{ number_format($payrollComputation->dailyRate, 2)  }}</p>
                                        </div>
                                    @endif

                                    <div class="col-xs-6">
                                        <p><strong>Employee ID:</strong> {{ $user->Profile->id_number  }}</p>
                                    </div>

                                    <div class="col-xs-6">
                                        <p><strong>Payroll Date:</strong> {{ date('m/d/Y', strtotime($payrollCycle->payroll_date))  }}</p>
                                    </div>

                                    <div class="col-xs-6">
                                        <p><strong>Cost Center:</strong> {{ $user->Profile->CostCenter->cost_center  }}</p>
                                    </div>

                                    <div class="col-xs-6">
                                        <p><strong>Payroll Period:</strong> {{ $payrollCycle->payroll_period  }}</p>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <hr />
                                </div>

                                <div class="col-xs-12">
                                    <div class="col-md-10">
                                        <h2 class="content-heading">Hours and Earnings</h2>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th colspan="4">Taxable</th>
                                                    <th colspan="4">Non-Taxable</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="text-center" colspan="2"><strong>Description</strong></td>
                                                <td class="text-center"><strong>Current</strong></td>
                                                <td class="text-center"><strong>YTD</strong></td>
                                                <td class="text-center" colspan="2"><strong>Description</strong></td>
                                                <td class="text-center"><strong>Current</strong></td>
                                                <td class="text-center"><strong>YTD</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Basic Pay</td>
                                                <td>{{ number_format($payroll->basic_pay, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('basic_pay'), 2) }}</td>
                                                <td colspan="2">De Minimis</td>
                                                <td>{{ number_format($payroll->de_minimis, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('de_minimis'), 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">ECOLA</td>
                                                <td>{{ number_format($payroll->ecola, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('ecola'), 2) }}</td>
                                                <td colspan="2">13th Month Bonus</td>
                                                <td>{{ number_format($payroll->thirteenth_month_bonus, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('thirteenth_month_bonus'), 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Overtime Pay</td>
                                                <td>{{ number_format($payroll->overtime_pay, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('overtime_pay'), 2) }}</td>
                                                <td colspan="2">Fixed Allowance</td>
                                                <td>{{ number_format($payroll->fixed_allowance, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('fixed_allowance'), 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Holiday Pay</td>
                                                <td>{{ number_format($payroll->holiday_pay, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('holiday_pay'), 2) }}</td>
                                                <td colspan="2">Leave Encashment</td>
                                                <td>{{ number_format($payroll->leave_encashment, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('leave_encashment'), 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Night Differential</td>
                                                <td>{{ number_format($payroll->night_differential, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('night_differential'), 2) }}</td>
                                                <td colspan="2">Incentives</td>
                                                <td>{{ number_format($payroll->incentives, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('incentives'), 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Holiday ECOLA</td>
                                                <td>{{ number_format($payroll->holiday_ecola, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('holiday_ecola'), 2) }}</td>
                                                {{--<td>Coop Savings</td>--}}
                                                {{--<td></td>--}}
                                                <td colspan="2">Adjustment - Income</td>
                                                <td>{{ number_format($payroll->adjustment_income, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('adjustment_income'), 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Adjustment - Wage</td>
                                                <td>{{ number_format($payroll->adjustment_wage, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('adjustment_wage'), 2) }}</td>
                                                <td colspan="4"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Other Incomes</td>
                                                <td>{{ number_format($payroll->other_incomes, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('other_incomes'), 2) }}</td>
                                                <td colspan="4"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-12"></div>

                                    <div class="col-xs-6">
                                        <h2 class="content-heading">Before Tax Deductions</h2>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <td class="text-center" colspan="2"><strong>Description</strong></td>
                                                    <td class="text-center"><strong>Current</strong></td>
                                                    <td class="text-center"><strong>YTD</strong></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td colspan="2">SSS EE</td>
                                                <td>{{ number_format($payroll->sss_ee, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('sss_ee'), 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">HDMF EE</td>
                                                <td>{{ number_format($payroll->hdmf_ee, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('hdmf_ee'), 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">PhilHealth EE</td>
                                                <td>{{ number_format($payroll->philhealth_ee, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('philhealth_ee'), 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Tardiness</td>
                                                <td>{{ number_format($payroll->tardiness, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('tardiness'), 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Absences</td>
                                                <td>{{ number_format($payroll->absences, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('absences'), 2) }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-xs-6">
                                        <h2 class="content-heading">After Tax Deductions</h2>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <td class="text-center" colspan="2"><strong>Description</strong></td>
                                                    <td class="text-center"><strong>Current</strong></td>
                                                    <td class="text-center"><strong>YTD</strong></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="2">SSS Loan</td>
                                                    <td>{{ number_format($payroll->sss_loan, 2) }}</td>
                                                    <td>{{ number_format($ytd->sum('sss_loan'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">HDMF Loan</td>
                                                    <td>{{ number_format($payroll->hdmf_loan, 2) }}</td>
                                                    <td>{{ number_format($ytd->sum('hdmf_loan'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Client Charges</td>
                                                    <td>{{ number_format($payroll->client_charges, 2) }}</td>
                                                    <td>{{ number_format($ytd->sum('client_charges'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Membership Fee</td>
                                                    <td>{{ number_format($payroll->membership_fee, 2) }}</td>
                                                    <td>{{ number_format($ytd->sum('membership_fee'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Notarial Fee</td>
                                                    <td>{{ number_format($payroll->notarial_fee, 2) }}</td>
                                                    <td>{{ number_format($ytd->sum('notarial_fee'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Coop Loan</td>
                                                    <td>{{ number_format($payroll->coop_loan, 2) }}</td>
                                                    <td>{{ number_format($ytd->sum('coop_loan'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Coop Share</td>
                                                    <td>{{ number_format($payroll->coop_share, 2) }}</td>
                                                    <td>{{ number_format($ytd->sum('coop_share'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Unpaid Leave</td>
                                                    <td>{{ number_format($payroll->unpaid_leave, 2) }}</td>
                                                    <td>{{ number_format($ytd->sum('unpaid_leave'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Adjustment - Deduction</td>
                                                    <td>{{ number_format($payroll->adjustment_deduction, 2) }}</td>
                                                    <td>{{ number_format($ytd->sum('adjustment_deduction'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Other Deductions</td>
                                                    <td>{{ number_format($payroll->other_deductions, 2) }}</td>
                                                    <td>{{ number_format($ytd->sum('other_deductions'), 2) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    {{--                                @if($payroll->Profile->Compensation->compute_sss == 'Y' || $payroll->Profile->Compensation->compute_hdmf == 'Y' || $payroll->Profile->Compensation->compute_philhealth == 'Y')--}}
                                    <div class="col-xs-12"></div>
                                    <div class="col-xs-6">
                                        <h2 class="content-heading">Employer Paid Benefits</h2>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <td class="text-center" colspan="2"><strong>Description</strong></td>
                                                    <td class="text-center"><strong>Current</strong></td>
                                                    <td class="text-center"><strong>YTD</strong></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {{--                                            @if($payroll->Profile->Compensation->compute_sss == 'Y')--}}
                                            <tr>
                                                <td colspan="2">SSS ER</td>
                                                <td>{{ number_format($payroll->sss_er, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('sss_er'), 2) }}</td>
                                            </tr>
                                            {{--@endif--}}

                                            {{--                                            @if($payroll->Profile->Compensation->compute_hdmf == 'Y')--}}
                                            <tr>
                                                <td colspan="2">HDMF ER</td>
                                                <td>{{ number_format($payroll->hdmf_er, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('hdmf_er'), 2) }}</td>
                                            </tr>
                                            {{--@endif--}}

                                            {{--                                            @if($payroll->Profile->Compensation->compute_philhealth == 'Y')--}}
                                            <tr>
                                                <td colspan="2">PhilHealth ER</td>
                                                <td>{{ number_format($payroll->philhealth_er, 2) }}</td>
                                                <td>{{ number_format($ytd->sum('philhealth_er'), 2) }}</td>
                                            </tr>
                                            {{--@endif--}}
                                            </tbody>
                                        </table>
                                    </div>
                                    {{--@endif--}}

                                    <div class="col-md-12">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>GROSS AMOUNT</th>
                                                <th>DEDUCTIONS</th>
                                                <th>TAX</th>
                                                <th>NET PAY</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th><strong>Current</strong></th>
                                                <td>{{ number_format($payroll->gross_amount, 2) }}</td>
                                                <td>{{ number_format($payroll->deductions, 2) }}</td>
                                                <td>{{ number_format($payroll->tax, 2) }}</td>
                                                <td>{{ number_format($payroll->net_pay, 2) }}</td>
                                            </tr>
                                            <tr>
                                                <th><strong>YTD</strong></th>
                                                <td>{{ number_format($ytd->sum('gross_amount'), 2) }}</td>
                                                <td>{{ number_format($ytd->sum('deductions'), 2) }}</td>
                                                <td>{{ number_format($ytd->sum('tax'), 2) }}</td>
                                                <td>{{ number_format($ytd->sum('net_pay'), 2) }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            window.print();
        })
    </script>
@endsection

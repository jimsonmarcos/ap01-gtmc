@extends('layouts.users_app')

@section('content')

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-10 col-md-offset-1">
                                    <br>

                                    <h3>Leave Credits</h3>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td width="40%">YTD Leave Credits</td>
                                            <td width="60">Days</td>
                                            <td>@if(\Carbon\Carbon::createFromFormat('Y-m-d', $user->Profile->EmploymentDetails->hire_date)->age > 0) 5 @else 0 @endif</td>
                                            <td width="60">Hrs</td>
                                            <td>
                                                @if(\Carbon\Carbon::createFromFormat('Y-m-d', $user->Profile->EmploymentDetails->hire_date)->age > 0) {{ 5 * 8 }} @else 0 @endif
                                            <!-- {{  floor(floor(((12 - date('m', strtotime($user->Profile->EmploymentDetails->hire_date))) / 12) * 5) + ((date('Y') - date('Y', strtotime($user->Profile->EmploymentDetails->hire_date))) * 5)) * 8 }} -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>YTD Accumulated Leave</td>
                                            <td>Days</td>
                                            <td>{{ $leaveHistory->where('status', 'Paid')->sum('days') }}</td>
                                            <td>Hrs</td>
                                            <td>{{ $leaveHistory->where('status', 'Paid')->sum('hours') }}</td>
                                        </tr>
                                        <tr>
                                            <td>YTD Leave Balance</td>
                                            <td>Days</td>
                                            <td>@if(\Carbon\Carbon::createFromFormat('Y-m-d', $user->Profile->EmploymentDetails->hire_date)->age > 0 && $leaveHistory->where('status', 'Paid')->sum('days') < 5) {{ 5 - $leaveHistory->where('status', 'Paid')->sum('days') }} @else 0 @endif</td>
                                            <td>Hrs</td>
                                            <td>@if(\Carbon\Carbon::createFromFormat('Y-m-d', $user->Profile->EmploymentDetails->hire_date)->age > 0 && $leaveHistory->where('status', 'Paid')->sum('hours') < 40) {{ (5 * 8) - $leaveHistory->where('status', 'Paid')->sum('hours') }} @else 0 @endif</td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <h4>Leave History</h4>

                                    <div style="margin-top: 10px;">
                                        <table id="leaveHistory" class="table">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Leave Type</th>
                                                <th>Days</th>
                                                <th>Hrs</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($leaveHistory->count() > 0)
                                            @foreach($leaveHistory as $leave)
                                                <tr>
                                                    <td>{{ date('m/d/Y', strtotime($leave->payroll_date)) }}</td>
                                                    <td>{{ $leave->leave_type }}</td>
                                                    <td>{{ $leave->day }}</td>
                                                    <td>{{ $leave->hours }}</td>
                                                    <td>{{ $leave->status }}</td>
                                                </tr>
                                            @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="5">No records yet.</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
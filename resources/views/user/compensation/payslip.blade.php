@extends('layouts.users_app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li><a class="link-effect" href="{{ route('user_payslip') }}">Payslip</a></li>
    </ol>

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="row">
                                    <br />

                                    {!! $banner !!}

                                    <br />

                                    <hr />

                                    <div class="col-md-12">
                                        <h2 class="content-heading">CutOff Period</h2>
                                        @foreach (\App\PayrollCycles::whereIn('id', \App\PayrollDtr::where('user_id', $user->id)->pluck('payroll_cycle_id'))->get() as $payrollCycle)
                                            <h2 class="content-heading">01/16/2017 - 01/15/2017
                                                &nbsp; &nbsp;
                                                {{--<span><a href="#">VIEW</a></span> || <span><a href="#">EXPORT</a></span>--}}
                                            </h2>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@extends('layouts.users_app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li><a class="link-effect" href="{{ route('user_leave_credits') }}">Leave Credits</a></li>
    </ol>

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                {!! $banner !!}

                                <br />

                                <hr />

                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <h2 class="content-heading" name="base_salary">Base Salary:</h2>
                                        <h2 class="content-heading" name="leaves">Number of Acculmulated Leave/s:</h2>
                                        <h2 class="content-heading" name="vacation_leave">Total Vacation Leave/s Used:</h2>
                                        <h2 class="content-heading" name="sick_leave">Total Sick Leave/s Used:</h2>
                                    </div>
                                    <div class="col-md-8">
                                        <h2 class="content-heading" name="base_salary">
                                            @if($user->Profile->Compensation->rate_type == 'Monthly')
                                                {{ number_format($compensations->monthly_rate, 2) }}
                                            @else
                                                @if($user->Profile->Compensation->daily_category == 'Fixed')
                                                    {{ number_format($user->Profile->Compensation->daily_rate * 26, 2) }}
                                                @else
                                                    {{ !empty($user->Profile->Compensation->daily_rate_id) ? number_format($user->Profile->Compensation->DailyRate->rate * 26, 2) : '' }}
                                                @endif
                                            @endif
                                        </h2>
                                        <h2 class="content-heading" name="leaves">@if($user->Profile->EmploymentDetails->hire_date != '0000-00-00') {{ floor((floor(((12 - date('m', strtotime($user->Profile->EmploymentDetails->hire_date))) / 12) * 5) + ((date('Y') - date('Y', strtotime($user->Profile->EmploymentDetails->hire_date))) * 5)) / 5) * 5 }} @else NONE @endif </h2>
                                        <h2 class="content-heading" name="vacation_leave">None</h2>
                                        <h2 class="content-heading" name="sick_leave">None</h2>
                                    </div>
                                    <hr />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
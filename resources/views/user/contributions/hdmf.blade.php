@extends('layouts.users_app')

@section('content')

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <table id="loans" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Pay Period</th>
                                        <th>Payroll Date</th>
                                        <th>EE</th>
                                        <th>ER</th>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if($contributions->count() > 0)
                                        @foreach($contributions as $contribution)
                                        <?php
                                        $summary = \App\PayrollSummary::where(['payroll_cycle_id' => $contribution->id, 'user_id' => $user->id])->first();

                                        ?>
                                        @if(($summary->hdmf_ee + $summary->hdmf_ec + $summary->hdmf_er) > 0)
                                        <tr>
                                            <td>{{ $contribution->payroll_period }}</td>
                                            <td>{{ date('m/d/Y', strtotime($contribution->payroll_date)) }}</td>
                                            <td>{{ number_format($summary->hdmf_ee, 2) }}</td>
                                            <td>{{ number_format($summary->hdmf_er, 2) }}</td>
                                            <td>{{ number_format($summary->hdmf_ee + $summary->hdmf_er, 2) }}</td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4">No records found.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection
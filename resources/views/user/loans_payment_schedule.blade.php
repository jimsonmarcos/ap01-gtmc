@extends('layouts.users_app')

@section('content')

    <ol class="breadcrumb push-10">
        <li><a href="{{ url("employee/loans") }}">Loans</a></li>
        <li>Payment Schedule</li>
    </ol>

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-12">
                                    <br>

                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="col-md-12">
                                                    <ul class="nav nav-tabs" id="" role="tablist">
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="{{ url("employee/loan/{$loan->cla_number}") }}"><i class="fa fa-info-circle"></i> Details</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="{{ url("employee/loan/{$loan->cla_number}/loan-amortization-schedule") }}"><i class="fa fa-calendar"></i> Loan Amortization Schedule</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link active" href="{{ url("employee/loan/{$loan->cla_number}/payment-schedule") }}"><i class="fa fa-calendar"></i> Payment Schedule</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="{{ url("employee/loan/{$loan->cla_number}/payments") }}"><i class="fa fa-money"></i> Payments</a>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <div class="col-sm-12 push-10-t">
                                                    <div class="row">

                                                        <div class="col-md-12">
                                                            <table class="table table-bordered table-condensed">
                                                                <thead>
                                                                <tr>
                                                                    <th class="text-center">MONTH</th>
                                                                    <th class="text-center">PAY PERIOD</th>
                                                                    <th class="text-center">DUE DATE</th>
                                                                    <th class="text-center">AMORTIZATION</th>
                                                                    <th class="text-center">LAPSED INTEREST</th>
                                                                    <th class="text-center">OVERDUE AMOUNT</th>
                                                                    <th class="text-center">AMOUNT PAID</th>
                                                                    <th class="text-center">OUTSTANDING BALANCE</th>
                                                                </tr>
                                                                </thead>


                                                                <tbody>
                                                                <?php
                                                                $outstandingBalance = $loan->is_voucher == 'N' ? $loan->granted_loan_amount : $loan->beginning_balance;
                                                                ?>

                                                                {{--<tr>--}}
                                                                {{--<th class="text-right" colspan="7"></th>--}}
                                                                {{--<th class="text-right">{{ number_format($outstandingBalance, 2) }}</th>--}}
                                                                {{--</tr>--}}


                                                                @foreach($paymentSchedules as $paymentSchedule)
                                                                    <?php
                                                                    $payment = \App\LoanPayments::where('loan_id', $loan->id)->where('pay_period', $paymentSchedule->pay_period)->first();
                                                                    $amountPaid = !empty($payment) ? number_format($payment->total_amount_paid, 2) : number_format(0, 2);
                                                                    $outstandingBalance += $paymentSchedule->lapsed_interest;
                                                                    $outstandingBalance -= !empty($payment) ? $payment->total_amount_paid : 0;
                                                                    ?>

                                                                    <tr>
                                                                        <th class="text-center">{{ $paymentSchedule->month }}</th>
                                                                        <td class="text-center">{{ $paymentSchedule->pay_period }}</td>
                                                                        <td class="text-center">{{ $paymentSchedule->due_date }}</td>
                                                                        <td class="text-right">{{ number_format($paymentSchedule->amortization, 2) }}</td>
                                                                        <td class="text-right">{{ number_format($paymentSchedule->lapsed_interest, 2) }}</td>
                                                                        <td class="text-right">{{ number_format($paymentSchedule->amortization + $paymentSchedule->lapsed_interest, 2) }}</td>
                                                                        <td class="text-right">{{ $amountPaid }}</td>
                                                                        <th class="text-right">{{ $outstandingBalance > 0.9 ? number_format($outstandingBalance, 2) : '-' }}</th>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>



                                                            </table>
                                                        </div>

                                                    </div>
                                                </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#loans').DataTable({
                "lengthMenu": [[ 20, 50, 100, 200, -1], [20, 50, 100, 200, 'All']],
                "pageLength": 20,
                "order": [[ 0, "desc" ]]
            });
        })
    </script>
@endsection
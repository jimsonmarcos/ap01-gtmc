@extends('layouts.users_app')

@section('content')

    <ol class="breadcrumb push-10">
        <li><a href="{{ url("employee/loans") }}">Loans</a></li>
        <li>Payments</li>
    </ol>

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-12">
                                    <br>

                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="col-md-12">
                                                    <ul class="nav nav-tabs" id="" role="tablist">
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="{{ url("employee/loan/{$loan->cla_number}") }}"><i class="fa fa-info-circle"></i> Details</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="{{ url("employee/loan/{$loan->cla_number}/loan-amortization-schedule") }}"><i class="fa fa-calendar"></i> Loan Amortization Schedule</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="{{ url("employee/loan/{$loan->cla_number}/payment-schedule") }}"><i class="fa fa-calendar"></i> Payment Schedule</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link active" href="{{ url("employee/loan/{$loan->cla_number}/payments") }}"><i class="fa fa-money"></i> Payments</a>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="row">

                                                        @if($payments->count() > 0)
                                                        <div class="col-md-10 col-md-offset-1">
                                                            <?php $paymentSchedules = \App\LoanPaymentSchedules::where(['loan_id' => $loan->id]); ?>


                                                                <br>
                                                                <table class="table table-bordered" style="width: 300px;">
                                                                    <tr>
                                                                        <th class="text-center" width="170">TOTAL AMOUNT PAID</th>
                                                                        <th class="text-right">{{ number_format($payments->sum('total_amount_paid'), 2) }}</th>
                                                                    </tr>
                                                                </table>

                                                                <br>
                                                                <table class="table table-bordered push-10-t">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>TRANSACTION CODE</th>
                                                                        <th>AR NUMBER</th>
                                                                        <th>PAY PERIOD</th>
                                                                        <th>DUE DATE</th>
                                                                        <th>PAYMENT DATE</th>
                                                                        <th>OVERDUE AMOUNT</th>
                                                                        <th>AMOUNT PAID</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($payments as $payment)
                                                                        <?php $paymentSchedule = \App\LoanPaymentSchedules::where(['loan_id' => $payment->loan_id, 'pay_period' => $payment->pay_period])->first() ?>

                                                                        <tr>
                                                                            <th>{{ $payment->transaction_id }}</th>
                                                                            <th>{{ $payment->ar_number }}</th>
                                                                            <td>{{ $payment->pay_period }}</td>
                                                                            <td>{{ date('m/d/Y', strtotime($paymentSchedule->due_date)) }}</td>
                                                                            <td>{{ date('m/d/Y', strtotime($payment->payment_date)) }}</td>
                                                                            <td class="text-right">{{ number_format($paymentSchedule->amortization + $paymentSchedule->lapsed_interest, 2) }}</td>
                                                                            <td class="text-right">{{ number_format($payment->total_amount_paid, 2) }}</td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>

                                                        </div>

                                                        @else
                                                            <div class="col-xs-12">
                                                                <br>
                                                                <p>No payments made yet.</p>
                                                            </div>
                                                        @endif

                                                    </div>
                                                </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#loans').DataTable({
                "lengthMenu": [[ 20, 50, 100, 200, -1], [20, 50, 100, 200, 'All']],
                "pageLength": 20,
                "order": [[ 0, "desc" ]]
            });
        })
    </script>
@endsection
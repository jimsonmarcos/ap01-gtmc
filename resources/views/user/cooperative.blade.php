@extends('layouts.users_app')

@section('content')

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-12">
                                    <br>

                                    <p class="text-right">Membership Status: <strong class="@if($user->Profile->status == 'Active') text-success @else text-danger @endif">{{ $user->Profile->status }}</strong></p>

                                    <div class="row no-padding">
                                        <div class="col-md-4">
                                            <h4>Cooperative</h4>
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>Membership Type</td>
                                                    <td>{{ $user->Profile->membership_type }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Cost Center</td>
                                                    <td>{{ !empty($user->Profile->CostCenter->cost_center) ? $user->Profile->CostCenter->cost_center : 'N/A' }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Contribution</td>
                                                    <td>@if(!empty($user->Profile->coop_share)) {{ number_format($user->Profile->coop_share, 2) }} @else 0 @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Membership Fee</td>
                                                    <td>{{ number_format(\App\OtherPayments::where(['user_id' => $user->Profile->user_id, 'transaction_type' => 'MF'])->sum('total_amount_paid'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Notarial Fee</td>
                                                    <td>{{ number_format(\App\OtherPayments::where(['user_id' => $user->Profile->user_id, 'transaction_type' => 'NF'])->sum('total_amount_paid'), 2) }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-4">
                                            <h4>Company</h4>
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>Coop Share</td>
                                                    <td>{{ number_format(\App\OtherPayments::where(['user_id' => $user->Profile->user_id, 'transaction_type' => 'COOP SHARE'])->sum('total_amount_paid'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Coop Savings</td>
                                                    <td>{{ number_format(\App\Loans::where(['user_id' => $user->Profile->user_id, 'releasing_status' => 'Released'])->sum('savings'), 2) }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-4">
                                            <h4>Government</h4>
                                            <table class="table">
                                                <?php $payrollSummary = \App\PayrollSummary::where('user_id', $user->Profile->user_id); ?>
                                                <tbody>
                                                <tr>
                                                    <td class="text-right">SSS EE & ER</td>
                                                    <td class="text-right">{{ number_format($payrollSummary->sum('sss_ee') + $payrollSummary->sum('sss_er'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">HDMF EE & EE</td>
                                                    <td class="text-right">{{ number_format($payrollSummary->sum('hdmf_ee') + $payrollSummary->sum('hdmf_er'), 2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">PhilHealth EE & EE</td>
                                                    <td class="text-right">{{ number_format($payrollSummary->sum('philhealth_ee') + $payrollSummary->sum('philhealth_er'), 2) }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <table class="table table-striped table-sm" id="beneficiaries">
                                        <thead>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Middle Name</th>
                                            <th>Last Name</th>
                                            <th>Birth Date</th>
                                            <th>Relationship to Member</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($beneficiaries->count() > 0)
                                            @foreach($beneficiaries as $beneficiary)
                                                <tr>
                                                    <td>{{ $beneficiary->first_name }}</td>
                                                    <td>{{ $beneficiary->middle_name }}</td>
                                                    <td>{{ $beneficiary->last_name }}</td>
                                                    <td>{{ $beneficiary->birthdate() }}</td>
                                                    <td>{{ $beneficiary->MemberRelationship->title }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5">No beneficiaries found.</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@extends('layouts.users_app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li><a class="link-effect" href="{{ route('user_sss_contribution') }}">SSS Contribution</a></li>
    </ol>

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                {!! $banner !!}

                                <br />

                                <hr />

                                <div class="col-md-12">
                                    <h2 class="content-heading" name="base_salary">Current</h2>
                                    <table id="user-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Complete Name</th>
                                            <th>ID Number</th>
                                            <th>Membership Category</th>
                                            <th>Principal</th>
                                            <th>Gender</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-12">
                                    <h2 class="content-heading" name="base_salary">YTD</h2>
                                    <table id="user-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Complete Name</th>
                                            <th>ID Number</th>
                                            <th>Membership Category</th>
                                            <th>Principal</th>
                                            <th>Gender</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
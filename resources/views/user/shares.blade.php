@extends('layouts.users_app')

@section('content')

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <table id="loans" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>AR Number</th>
                                        <th>Payment Method</th>
                                        <th>Pay Period</th>
                                        <th>Amount</th>
                                        <th>Interest</th>
                                        <th>Total Amount Paid</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if($shares->count() > 0)
                                        @foreach($shares as $share)
                                        <tr>
                                            <td>{{ $share->ar_number }}</td>
                                            <td>{{ $share->payment_method }}</td>
                                            <td>{{ $share->pay_period }}</td>
                                            <td>{{ number_format($share->amount, 2) }}</td>
                                            <td>{{ number_format($share->interest, 2) }}</td>
                                            <td>{{ number_format($share->total_amount_paid, 2) }}</td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6">No records found.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection
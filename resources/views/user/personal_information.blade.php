@extends('layouts.users_app')

@section('content')
    {{--<ol class="breadcrumb push-10-t">--}}
        {{--<li><a class="link-effect" href="{{ route('user_personal_information') }}">Personal Information</a></li>--}}
    {{--</ol>--}}

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-9">
                                    <br>
                                    <br>

                                    <table class="table no-margin-bottom">
                                        <tbody>
                                        <tr>
                                            <td width="110">First Name:</td>
                                            <td width="215">{{ strtoupper($user->Profile->first_name) }}</td>
                                            <td width="130">Middle Name:</td>
                                            <td width="">{{ strtoupper($user->Profile->middle_name) }}</td>
                                            <td width="110">Last Name:</td>
                                            <td width="">{{ strtoupper($user->Profile->last_name) }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td width="110">Civil Status:</td>
                                            <td width="215">{{ !empty($user->Profile->CivilStatus->title) ? strtoupper($user->Profile->CivilStatus->title) : '' }}</td>
                                            <td width="130">Gender:</td>
                                            <td colspan="3">{{ strtoupper($user->Profile->gender) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Address:</td>
                                            <td colspan="5">{{ strtoupper($user->Profile->address) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Mobile:</td>
                                            <td>{{ $user->Profile->mobile1 }}
                                                @if(!empty($user->Profile->mobile2))
                                                    <br>{{ $user->Profile->mobile2 }}
                                                @endif
                                            </td>
                                            <td>Email:</td>
                                            <td colspan="2">{{ strtolower($user->Profile->email) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Birth Date</td>
                                            <td>{{ !empty($user->Profile->birthday) ?  \Carbon\Carbon::createFromFormat('Y-m-d', $user->Profile->birthday)->format('M. j, Y') : '' }}</td>
                                            <td>Age</td>
                                            <td colspan="3">{{ !empty($user->Profile->birthday) ?  \Carbon\Carbon::createFromFormat('Y-m-d', $user->Profile->birthday)->age : '' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Mother's Maiden <br />Name</td>
                                            <td colspan="5">{{ strtoupper($user->Profile->mothers_maiden_name) }}</td></tr>
                                        <tr>
                                            <td>Educational Attainment</td>
                                            <td colspan="5">@if(!empty($user->Profile->EducationalAttainment->title)) {{ strtoupper($user->Profile->EducationalAttainment->title) }} @endif</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <hr />
                                    <h2>In Case of Emergency</h2>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td width="155">Contact Person</td>
                                            <td width="300">{{ strtoupper($user->Profile->icoe_contact_name) }}</td>
                                            <td>Contact Number</td>
                                            <td>{{ strtoupper($user->Profile->icoe_contact_mobile) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td colspan="3">{{ strtoupper($user->Profile->icoe_contact_address) }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <div class="" style="padding: 5px;">
                                        @if(empty($user->Profile->photo_path))
                                            <img src="{{ url("img/user-placeholder.png") }}" alt="" style="max-width: 100%;">
                                        @else
                                            <img src="{{ url($user->Profile->photo_path) }}" alt="" style="max-width: 100%;">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
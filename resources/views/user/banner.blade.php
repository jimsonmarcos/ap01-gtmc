<div class="col-md-6">
    <div class="col-lg-6 animated fadeIn">
        <div class="img-container" style="border: 1px red solid">
            <img class="img-responsive" src="" alt="Employee Picture">
            <div class="img-options">
                <div class="img-options-content">
                    <h3 class="font-w400 text-white push-5">Image Caption</h3>
                    <h4 class="h6 font-w400 text-white-op push-15">Some Extra Info</h4>
                    <a class="btn btn-sm btn-default" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                    <a class="btn btn-sm btn-default" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <h2 class="content-heading">{{ $user->Profile->last_name }}, {{ $user->Profile->first_name }} {{ substr($user->Profile->middle_name, 1, 1) }}.</h2>
    <h2 class="content-heading">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->Profile->birthday)->age }}, {{ $user->Profile->CivilStatus->title }}, {{ $user->Profile->gender }}</h2>
    <h2 class="content-heading">{{ $user->Profile->address }}</h2>
    <h2 class="content-heading">{{ $user->Profile->mobile1 }} {{ $user->Profile->mobile2 }} </h2>
    <h2 class="content-heading">{{ $user->email }}</h2>
</div>
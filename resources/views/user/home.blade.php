@extends('layouts.users_app')

@section('content')
    <ol class="breadcrumb push-10-t">
        <li><a class="link-effect" href="{{ route('user_home') }}">Home</a></li>
    </ol>

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />


                                {!! $banner !!}

                                <br />

                                <hr />

                                <div class="col-md-12">
                                    <table id="user-dt" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>{{ $user->name }}</th>
                                            <th>{{ $user->Profile->id_number }}</th>
                                            <th>{{ $user->Profile->membership_category }}</th>
                                            <th>{{ $user->Profile->EmploymentDetails->Principal->company_code }}</th>
                                            <th>{{ $user->Profile->gender }}</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
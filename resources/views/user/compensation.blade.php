@extends('layouts.users_app')

@section('content')

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-12">
                                    <br>

                                    <h3>Payroll Details</h3>
                                    <div class="row no-padding-bottom">
                                        <br>
                                        <div class="col-md-3">
                                            <h4>Compute</h4>
                                            <ul>
                                                <li class="text-{{ $user->Profile->Compensation->compute_sss == 'Y' ? 'success' : 'danger' }}" style="list-style: none;"><span class="fa {{ $user->Profile->Compensation->compute_sss == 'Y' ? 'fa-check' : 'fa-close' }}"></span> SSS No.</li>
                                                <li class="text-{{ $user->Profile->Compensation->compute_hdmf == 'Y' ? 'success' : 'danger' }}" style="list-style: none;"><span class="fa {{ $user->Profile->Compensation->compute_hdmf == 'Y' ? 'fa-check' : 'fa-close' }}"></span> HDMF No.</li>
                                                <li class="text-{{ $user->Profile->Compensation->compute_philhealth == 'Y' ? 'success' : 'danger' }}" style="list-style: none;"><span class="fa {{ $user->Profile->Compensation->compute_philhealth == 'Y' ? 'fa-check' : 'fa-close' }}"></span> PhilHealth No.</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <br>


                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td width="150">Client</td>
                                                    <td>@if(!empty(strtoupper($user->Profile->EmploymentDetails->Principal->company_code))) {{ strtoupper($user->Profile->EmploymentDetails->Principal->company_code) }} @endif</td>
                                                </tr>
                                                @if(!empty($user->Profile->EmploymentDetails->Principal->parent_company_id))
                                                    <tr>
                                                        <td>Parent Client</td>
                                                        <td>@if(!empty($user->Profile->EmploymentDetails->Principal->ParentCompany->company_code)) {{ strtoupper($user->Profile->EmploymentDetails->Principal->ParentCompany->company_code) }} @endif</td>
                                                    </tr>
                                                @endif
                                                <tr>
                                                    <td>Payroll Group</td>
                                                    <td>@if(!empty($user->Profile->Compensation->PayrollGroup->group_name)) {{ strtoupper($user->Profile->Compensation->PayrollGroup->group_name) }} @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Mode of Transfer</td>
                                                    <td>@if(!empty($user->Profile->Compensation->TransferMode->transfer_mode)) {{ strtoupper($user->Profile->Compensation->TransferMode->transfer_mode) }} @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Transfer Type</td>
                                                    <td>@if(!empty($user->Profile->Compensation->TransferMode->transfer_type)) {{ strtoupper($user->Profile->Compensation->TransferMode->transfer_type) }} @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Bank</td>
                                                    <td>@if(!empty($user->Profile->Compensation->TransferMode->bank)) {{ strtoupper($user->Profile->Compensation->TransferMode->bank) }} @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Mobile Number</td>
                                                    <td>{{ strtoupper($user->Profile->Compensation->mobile_number) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Account Number</td>
                                                    <td>{{ strtoupper($user->Profile->Compensation->account_number) }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <table class="table" style="max-width: 600px;">
                                        <tbody>
                                        <tr>
                                            <td style="width: 160px;">Tax Type</td>
                                            <td>
                                                {{ !empty($user->Profile->Compensation->tax_type_id) ? $user->Profile->Compensation->TaxType->title : '' }}
                                                @if($user->Profile->Compensation->deduct_withholding == 'N')
                                                    <br>
                                                    <small class="text-success"><span class="fa fa-check"></span>
                                                        Minimum Wage Earner
                                                    </small>
                                                @else
                                                    <br>
                                                    <small class="text-success"><span class="fa fa-check"></span>
                                                        Deduct Withholding Tax
                                                    </small>
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <hr>

                                    <h3>Rate Details</h3>
                                    <table class="table" style="max-width: 600px;">
                                        <tbody>
                                        <tr>
                                            <td width="110">Rate Type</td>
                                            <td width="">{{ $user->Profile->Compensation->rate_type }}</td>
                                            <td width="130">Monthly Rate</td>
                                            <td width="">
                                                @if($user->Profile->Compensation->rate_type == 'Monthly')
                                                    {{ number_format($user->Profile->Compensation->monthly_rate, 2) }}
                                                @else
                                                    @if($user->Profile->Compensation->daily_category == 'Fixed')
                                                        {{ number_format($user->Profile->Compensation->daily_rate * 26, 2) }}
                                                    @else
                                                        {{ !empty($user->Profile->Compensation->daily_rate_id) ? number_format($user->Profile->Compensation->DailyRate->rate * 26, 2) : '' }}
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="110">Daily Rate <br />Category</td>
                                            <td width="">{{ $user->Profile->Compensation->daily_category }}</td>
                                            <td width="130">Daily Rate</td>
                                            <td width="">
                                                @if($user->Profile->Compensation->daily_category == 'Fixed')
                                                    {{ number_format($user->Profile->Compensation->daily_rate, 2) }}
                                                @else
                                                    {{ !empty($user->Profile->Compensation->daily_rate_id) ? number_format($user->Profile->Compensation->DailyRate->rate, 2) : '' }}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="110">Location</td>
                                            <td width="">{{ !empty($user->Profile->Compensation->daily_rate_id) ? $user->Profile->Compensation->DailyRate->location : '' }}</td>
                                            <td width="130">Hourly Rate</td>
                                            <td width="">
                                                @if($user->Profile->Compensation->rate_type == 'Daily')
                                                    {{ number_format($user->Profile->Compensation->daily_rate / 8, 2) }}
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <hr>

                                    <div style="max-width: 600px;">
                                    </div>
                                    <h4>Allowances</h4>
                                    <table class="table" style="max-width: 600px;">
                                        <thead>
                                        <tr>
                                            <th>Allowance</th>
                                            <th>Taxable</th>
                                            <th>Amount</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($user->Profile->Compensation->daily_category == 'Location')
                                            <tr>
                                                <td>ECOLA</td>
                                                <td>Taxable</td>
                                                <td>{{ !empty($user->Profile->Compensation->DailyRate->ecola) ? number_format($user->Profile->Compensation->DailyRate->ecola, 2) : '' }}</td>
                                                <td></td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <td>FIXED ALLOWANCE</td>
                                            <td>Non-Taxable</td>
                                            <td>{{ number_format($user->Profile->Compensation->fixed_allowance, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>DE MINIMIS</td>
                                            <td>Non-Taxable</td>
                                            <td>{{ number_format($user->Profile->Compensation->de_minimis, 2) }}</td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
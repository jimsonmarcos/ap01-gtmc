@extends('layouts.users_app')

@section('content')

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-12">
                                    <br>

                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <table id="loans" class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>CLA Number</th>
                                                        <th width="130">ID Number</th>
                                                        <th>Complete Name</th>
                                                        <th>Cost Center</th>
                                                        <th>Loan Type</th>
                                                        <th>Overdue Amount</th>
                                                        <th>Outstanding Balance</th>
                                                        <th>Granted Loan Amount</th>
                                                        <th>Status</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($loans as $loan)
                                                            <tr>
                                                                <td>@if($loan->status == 'Approved') <a href="/employee/loan/{{ $loan->cla_number }}">{{ $loan->cla_number }}</a> @else {{ $loan->cla_number }} @endif</td>
                                                                <td>{{ $loan->id_number }}</td>
                                                                <td>{{ trim("{$loan->last_name}, {$loan->first_name} {$loan->middle_name}")}}</td>
                                                                <td>{{ $loan->cost_center }}</td>
                                                                <td>{{ ($loan->is_voucher == 'N') ? $loan->loanType->type : $loan->loan_type }}</td>
                                                                <td>{{ number_format( $loan->total_overdue_amount, 2) }}</td>
                                                                <td>{{ number_format( $loan->outstanding_balance, 2) }}</td>
                                                                <td>{{ number_format( $loan->granted_loan_amount, 2) }}</td>
                                                                <td>{{ $loan->status }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            $('#loans').DataTable({
                "lengthMenu": [[ 20, 50, 100, 200, -1], [20, 50, 100, 200, 'All']],
                "pageLength": 20,
                "order": [[ 0, "desc" ]]
            });
        })
    </script>
@endsection
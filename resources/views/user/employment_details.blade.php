@extends('layouts.users_app')

@section('content')
    {{--<ol class="breadcrumb push-10-t">--}}
        {{--<li><a class="link-effect" href="{{ route('user_employment_details') }}">Employment Details</a></li>--}}
    {{--</ol>--}}

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <div class="col-md-8 col-md-offset-2">
                                    <table class="table no-margin-bottom" style="max-width: 700px;">
                                        <tbody>
                                        <tr>
                                            <td width="150">Employment Status:</td>
                                            <td colspan="3"><strong>@if(!empty($user->Profile->employmentdetails->reason) && date('Y-m-d', strtotime($user->Profile->employmentdetails->effective_date)) <= date('Y-m-d')) <span class="text-danger">{{ strtoupper($user->Profile->employmentdetails->reason) }}</span> @else <span class="text-success">ACTIVE</span> @endif</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="110">Principal:</td>
                                            <td width="200">@if(!empty($user->Profile->employmentdetails->principal)) {{ $user->Profile->employmentdetails->principal->company_code }} @endif</td>
                                            <td width="130">Hire Date:</td>
                                            <td width="">@if(!empty($user->Profile->employmentdetails->principal)) {{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->Profile->employmentdetails->hire_date)->format('M. j, Y') }} @endif</td>
                                        </tr>
                                        <tr>
                                            <td width="110">Position</td>
                                            <td colspan="5">@if(!empty($user->Profile->employmentdetails->position->title)) {{ $user->Profile->employmentdetails->position->title }} @endif <?php if($user->Profile->employmentdetails->orientation == 'Y' && $user->Profile->employmentdetails->position_id == '1') { echo "<br /> <small class='text-success'><span class='fa fa-check'></span> Orientation</small>"; } ?></td>
                                        </tr>
                                        <?php if($user->Profile->employmentdetails->position_id == '1') { ?>
                                        <tr>
                                            <td>Trainee Start<br />Date</td>
                                            <td>@if(!empty($user->Profile->employmentdetails->trainee_start_date)) {{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->Profile->employmentdetails->trainee_start_date)->format('M. j, Y') }} @endif</td>
                                            <td>Trainee End<br />Date</td>
                                            <td>@if(!empty($user->Profile->employmentdetails->trainee_end_date)) {{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->Profile->employmentdetails->trainee_end_date)->format('M. j, Y') }} @endif</td>
                                        </tr>
                                        <?php } ?>

                                        {{--                                                    @if($user->Profile->EmploymentDetails->position_id == 1 || $user->Profile->EmploymentDetails->position_id == 2)--}}
                                        <tr>
                                            <td width="110">Department</td>
                                            <td colspan="5">
                                                <?php
                                                $department = [];
                                                if(!empty($user->Profile->EmploymentDetails->Outlet->outlet)) $department[] = $user->Profile->EmploymentDetails->Outlet->outlet;
                                                if(!empty($user->Profile->EmploymentDetails->Mall->mall)) $department[] = $user->Profile->EmploymentDetails->Mall->mall;
                                                if(!empty($user->Profile->EmploymentDetails->Area->area)) $department[] = $user->Profile->EmploymentDetails->Area->area;
                                                if(!empty($user->Profile->EmploymentDetails->Province->province)) $department[] = $user->Profile->EmploymentDetails->Province->province;
                                                echo implode(' - ', $department);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="110">City/Province</td>
                                            <td>@if(!empty($user->Profile->EmploymentDetails->Province->province)) {{ strtoupper($user->Profile->EmploymentDetails->Province->province) }} @endif</td>
                                            <td>Mall</td>
                                            <td colspan="3">@if(!empty($user->Profile->EmploymentDetails->Mall->mall)) {{ strtoupper($user->Profile->EmploymentDetails->Mall->mall) }} @endif</td>
                                        </tr>
                                        <tr>
                                            <td width="110">Area</td>
                                            <td>@if(!empty($user->Profile->EmploymentDetails->Area->area)) {{ strtoupper($user->Profile->EmploymentDetails->Area->area) }} @endif</td>
                                            <td>Outlet</td>
                                            <td colspan="3">@if(!empty($user->Profile->EmploymentDetails->Outlet->outlet)) {{ strtoupper($user->Profile->EmploymentDetails->Outlet->outlet) }} @endif</td>
                                        </tr>
                                        {{--@endif--}}

                                        @if(in_array($user->Profile->employmentdetails->position_id, [1, 2]))
                                            <tr>
                                                <td width="110">Coordinator</td>
                                                <td colspan="5">{{ !empty($user->Profile->EmploymentDetails->Coordinator) ? strtoupper($user->Profile->EmploymentDetails->Coordinator->name()) : '' }}</td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <td width="110">SSS No.</td>
                                            <td colspan="5">{{ $user->Profile->Compensation->sss }}</td>
                                        </tr>
                                        <tr>
                                            <td width="110">HDMF No.</td>
                                            <td colspan="5">{{ $user->Profile->Compensation->hdmf }}</td>
                                        </tr>
                                        <tr>
                                            <td width="110">PHIC No.</td>
                                            <td colspan="5">{{ $user->Profile->Compensation->philhealth }}</td>
                                        </tr>
                                        <tr>
                                            <td width="110">Tax ID Number</td>
                                            <td colspan="5">{{ $user->Profile->Compensation->tin }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
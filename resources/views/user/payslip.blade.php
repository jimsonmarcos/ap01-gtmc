@extends('layouts.users_app')

@section('content')

    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <br />

                                <table id="loans" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Pay Period</th>
                                        <th>Payroll Date</th>
                                        <th>Payroll Code</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if($payrollCycles->count() > 0)
                                        @foreach($payrollCycles as $payrollCycle)
                                        <tr>
                                            <td>{{ $payrollCycle->payroll_period }}</td>
                                            <td>{{ date('m/d/Y', strtotime($payrollCycle->payroll_date)) }}</td>
                                            <td>{{ $payrollCycle->payroll_code }}</td>
                                            <td><a href="{{ url("/print/payslip/{$payrollCycle->id}/{$payrollCycle->DTR->where('user_id', $user->id)->first()->id}/detailed") }}" class="btn btn-primary" target="_blank"><i class="fa fa-print"></i> Print</a></td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4">No records found.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection
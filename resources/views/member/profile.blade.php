@extends('layouts.members_app')

@section('content')

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            {{--<div class="row no-padding">--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<strong>Name: {{ $user->name() }}</strong>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<strong>ID Number: {{ $user->id_number }}</strong>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-12">--}}
                                    {{--<hr>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-12">--}}
                                    {{--<ul class="nav nav-tabs">--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link active" href="{{ url("admin/user/{$id}/profile") }}">Personal Information </a>--}}
                                        {{--</li>--}}
                                        {{--@if($user->User->role != 'Member')--}}
                                            {{--<li class="nav-item">--}}
                                                {{--<a class="nav-link" href="{{ url("admin/user/{$id}/employment-details") }}">Employment Details</a>--}}
                                            {{--</li>--}}
                                            {{--<li class="nav-item">--}}
                                                {{--<a class="nav-link" href="{{ url("admin/user/{$id}/compensations-leave-credits") }}">Compensation and Leave Credits</a>--}}
                                            {{--</li>--}}
                                        {{--@endif--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-9">--}}
                                    {{--<div>--}}
                                        {{--<a href="{{ url("admin/user/{$id}/profile/edit") }}" class="btn btn-sm btn-primary pull-right" style="margin-top: 10px;"><span class="fa fa-edit"></span> Edit Profile</a>--}}
                                    {{--</div>--}}
                                    {{--<br>--}}
                                    {{--<br>--}}

                                    {{--<table class="table no-margin-bottom">--}}
                                        {{--<tbody>--}}
                                        {{--<tr>--}}
                                            {{--<td width="110">First Name:</td>--}}
                                            {{--<td width="">{{ strtoupper($user->first_name) }}</td>--}}
                                            {{--<td width="130">Middle Name:</td>--}}
                                            {{--<td width="">{{ strtoupper($user->middle_name) }}</td>--}}
                                            {{--<td width="110">Last Name:</td>--}}
                                            {{--<td width="">{{ strtoupper($user->last_name) }}</td>--}}
                                        {{--</tr>--}}
                                        {{--</tbody>--}}
                                    {{--</table>--}}
                                    {{--<table class="table">--}}
                                        {{--<tbody>--}}
                                        {{--<tr>--}}
                                            {{--<td>Civil Status:</td>--}}
                                            {{--<td>{{ strtoupper($user->CivilStatus->title) }}</td>--}}
                                            {{--<td>Gender:</td>--}}
                                            {{--<td colspan="3">{{ strtoupper($user->gender) }}</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>Address:</td>--}}
                                            {{--<td colspan="5">{{ strtoupper($user->address) }}</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>Mobile:</td>--}}
                                            {{--<td>{{ $user->mobile1 }}</td>--}}
                                            {{--<td>{{ $user->mobile2 }}</td>--}}
                                            {{--<td>Email:</td>--}}
                                            {{--<td>{{ strtolower($user->email) }}</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>Birth Date</td>--}}
                                            {{--<td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->birthday)->format('M. j, Y') }}</td>--}}
                                            {{--<td>Age</td>--}}
                                            {{--<td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->birthday)->age }}</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>Mother's Maiden <br />Name</td>--}}
                                            {{--<td colspan="5">{{ strtoupper($user->mothers_maiden_name) }}</td></tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>Educational Attainment</td>--}}
                                            {{--<td colspan="5">{{ strtoupper($user->EducationalAttainment->title) }}</td>--}}
                                        {{--</tr>--}}
                                        {{--</tbody>--}}
                                    {{--</table>--}}
                                    {{--<hr />--}}
                                    {{--<h2>In Case of Emergency</h2>--}}
                                    {{--<table class="table">--}}
                                        {{--<tbody>--}}
                                        {{--<tr>--}}
                                            {{--<td>Contact Person</td>--}}
                                            {{--<td>{{ strtoupper($user->icoe_contact_name) }}</td>--}}
                                            {{--<td>Contact Number</td>--}}
                                            {{--<td>{{ strtoupper($user->icoe_contact_mobile) }}</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>Address</td>--}}
                                            {{--<td colspan="3">{{ strtoupper($user->icoe_contact_address) }}</td>--}}
                                        {{--</tr>--}}
                                        {{--</tbody>--}}
                                    {{--</table>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-3">--}}

                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="row">
                                <div class="col-md-6">
                                    <strong>Name: {{ $user->name() }}</strong>
                                </div>

                                <div class="col-md-6">
                                    <strong>ID Number: {{ $user->id_number }}</strong>
                                </div>

                                <div class="col-md-12">
                                    <hr>
                                </div>

                                <div class="col-md-12">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            {{--<a class="nav-link active" href="{{ url("admin/user/{$id}/profile") }}">Personal Information </a>--}}
                                            <a class="nav-link active" href="#">Personal Information </a>
                                        </li>
                                        @if($user->User->role != 'Member')
                                            <li class="nav-item">
                                                {{--<a class="nav-link" href="{{ url("admin/user/{$id}/employment-details") }}">Employment Details</a>--}}
                                                <a class="nav-link" href="#">Employment Details</a>
                                            </li>
                                            <li class="nav-item">
                                                {{--<a class="nav-link" href="{{ url("admin/user/{$id}/compensations-leave-credits") }}">Compensation and Leave Credits</a>--}}
                                                <a class="nav-link" href="#">Compensation and Leave Credits</a>
                                            </li>
                                        @endif
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>--}}
                                            {{--<a class="nav-link" href="{{ route('member_loan_shares', ['id' => $user->id ]) }}">Loans and Shares</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>--}}
                                            {{--<a class="nav-link" href="{{ route('member_loan_payments', ['id' => $user->id ]) }}">Loans Payments</a>--}}
                                        {{--</li>--}}
                                    </ul>
                                </div>

                                <div class="col-md-9">
                                    @if($user->User->role != 'Member')
                                    <div>
                                        <a href="#" class="btn btn-sm btn-primary pull-right" style="margin-top: 10px;"><span class="fa fa-edit"></span> Edit Profile</a>
                                        {{--<a href="{{ url("admin/user/{$id}/profile/edit") }}" class="btn btn-sm btn-primary pull-right" style="margin-top: 10px;"><span class="fa fa-edit"></span> Edit Profile</a>--}}
                                    </div>
                                    @endif
                                    <br>
                                    <br>

                                    <table class="table no-margin-bottom">
                                        <tbody>
                                        <tr>
                                            <td width="110">First Name:</td>
                                            <td width="">{{ strtoupper($user->first_name) }}</td>
                                            <td width="130">Middle Name:</td>
                                            <td width="">{{ strtoupper($user->middle_name) }}</td>
                                            <td width="110">Last Name:</td>
                                            <td width="">{{ strtoupper($user->last_name) }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td>Civil Status:</td>
                                            <td>@if(!empty($user->CivilStatus->title)) {{ strtoupper($user->CivilStatus->title) }} @endif</td>
                                            <td>Gender:</td>
                                            <td colspan="3">{{ strtoupper($user->gender) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Address:</td>
                                            <td colspan="5">{{ strtoupper($user->address) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Mobile:</td>
                                            <td>{{ $user->mobile1 }}</td>
                                            <td>{{ $user->mobile2 }}</td>
                                            <td>Email:</td>
                                            <td>{{ strtolower($user->email) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Birth Date</td>
                                            <td>@if($user->birthday != '0000-00-00') {{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->birthday)->format('M. j, Y') }} @endif</td>
                                            <td>Age</td>
                                            <td>@if($user->birthday != '0000-00-00') {{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->birthday)->age }} @endif</td>
                                        </tr>
                                        <tr>
                                            <td>Mother's Maiden <br />Name</td>
                                            <td colspan="5">{{ strtoupper($user->mothers_maiden_name) }}</td></tr>
                                        <tr>
                                            <td>Educational Attainment</td>
                                            <td colspan="5">@if(!empty($user->EducationalAttainment->title)) {{ strtoupper($user->EducationalAttainment->title) }} @endif</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <hr />
                                    <h2>In Case of Emergency</h2>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td>Contact Person</td>
                                            <td>{{ strtoupper($user->icoe_contact_name) }}</td>
                                            <td>Contact Number</td>
                                            <td>{{ strtoupper($user->icoe_contact_mobile) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td colspan="3">{{ strtoupper($user->icoe_contact_address) }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                {{--<div class="col-md-3">--}}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
    </div>
    </div>

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection

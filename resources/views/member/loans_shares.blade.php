@extends('layouts.members_app')

@section('content')


            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row no-padding">
                                        <div class="col-md-6">
                                            <strong>Name: {{ $user->name() }}</strong>
                                        </div>

                                        <div class="col-md-6">
                                            <strong>ID Number: {{ $user->id_number }}</strong>
                                        </div>

                                        <div class="col-md-12">
                                            <hr>
                                        </div>

                                        <div class="col-md-12">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">Loans and Shares</a>
                                                </li>
                                                {{--<li class="nav-item">--}}
                                                    {{--<a class="nav-link" href="{{ route('member_profile') }}">Personal Information</a>--}}
                                                {{--</li>--}}
                                                @if($user->User->role != 'Member')
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/employment-details") }}">Employment Details</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ url("admin/user/{$id}/compensations-leave-credits") }}">Compensation and Leave Credits</a>
                                                </li>
                                                @endif
                                                {{--<li class="nav-item">--}}
                                                    {{--<a class="nav-link active" href="#">Loans and Shares</a>--}}
                                                {{--</li>--}}
                                                {{--<li class="nav-item">--}}
                                                    {{--<a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>--}}
                                                    {{--<a class="nav-link" href="{{ route('member_loan_payments', ['id' => $user->id ]) }}">Loans Payments</a>--}}
                                                {{--</li>--}}
                                            </ul>
                                        </div>

                                        <div class="col-md-12">
                                            <br>
                                            <p class="text-right">Membership Status: <strong class="text-success">Active</strong></p>

                                            <div class="row no-padding">
                                                <div class="col-md-4">
                                                    <h4>Cooperative</h4>
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td>Membership Type</td>
                                                                <td>{{ $user->membership_type }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cost Center</td>
                                                                <td>{{ $user->CostCenter->cost_center }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Contribution</td>
                                                                <td>{{ $user->coop_share }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="col-md-4">
                                                    <h4>Company</h4>
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td>Coop Share</td>
                                                                <td><!-- TODO: Display Coop Share --></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Coop Savings</td>
                                                                <td><!-- TODO: Display Coop Savings --></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>


                                            </div>

                                            <div class="col-md-12">
                                                <div id="beneficariesAcc" data-children=".beneficiaryItems">
                                                    <div class="beneficiaryItems">
                                                        <a class="btn btn-sm btn-primary" data-toggle="collapse" data-parent="#beneficariesAcc" href="#beneficaries" aria-expanded="true" aria-controls="beneficaries">
                                                            View Beneficiaries
                                                        </a>
                                                        <div id="beneficaries" class="collapse" role="tabpanel">
                                                            <br>
                                                            <br>
                                                            <table class="table table-striped table-sm" id="beneficiaries">
                                                                <thead>
                                                                    <tr>
                                                                        <th>First Name</th>
                                                                        <th>Middle Name</th>
                                                                        <th>Last Name</th>
                                                                        <th>Birth Date</th>
                                                                        <th>Relationship to Member</th>
                                                                        <th></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @if($beneficiaries->count() > 0)
                                                                        @foreach($beneficiaries as $beneficiary)
                                                                            <tr>
                                                                                <td>{{ $beneficiary->first_name }}</td>
                                                                                <td>{{ $beneficiary->middle_name }}</td>
                                                                                <td>{{ $beneficiary->last_name }}</td>
                                                                                <td>{{ $beneficiary->birthdate() }}</td>
                                                                                <td>{{ $beneficiary->MemberRelationship->title }}</td>
                                                                                <td>
                                                                                    <a href="#" class="btn btn-sm btn-primary editBeneficiary" data-id="{{ $beneficiary->id }}"><span class="fa fa-edit"></span> </a>
                                                                                    <a href="#" class="btn btn-sm btn-danger deleteBeneficiary" data-id="{{ $beneficiary->id }}"><span class="fa fa-remove"></span> </a>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @else
                                                                        <tr>
                                                                            <td colspan="5">No Beneficiaries yet.</td>
                                                                        </tr>
                                                                    @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr>
                                            </div>

                                            {{--<div class="col-md-12">--}}
                                                {{--<h4>Loan Payments <span class="text-danger">WIP</span></h4>--}}

                                                {{--<table class="table">--}}
                                                    {{--<thead>--}}
                                                        {{--<tr>--}}
                                                            {{--<th>Date</th>--}}
                                                            {{--<th>Loan Type</th>--}}
                                                            {{--<th>Loan Amount</th>--}}
                                                            {{--<th>Total Paid Amount</th>--}}
                                                            {{--<th>Outstanding Balance</th>--}}
                                                        {{--</tr>--}}
                                                    {{--</thead>--}}
                                                    {{--<tbody>--}}
                                                        {{--<tr>--}}
                                                            {{--<td>9/9/2015</td>--}}
                                                            {{--<td>SSS Salary Loan</td>--}}
                                                            {{--<td>5,000.00</td>--}}
                                                            {{--<td>3,000.00</td>--}}
                                                            {{--<td>2,000.00</td>--}}
                                                        {{--</tr>--}}
                                                        {{--<tr>--}}
                                                            {{--<td>5/21/2017</td>--}}
                                                            {{--<td>HDMF Multi-Purpose Loan</td>--}}
                                                            {{--<td>15,500.00</td>--}}
                                                            {{--<td>8,700.00</td>--}}
                                                            {{--<td>6,800.00</td>--}}
                                                        {{--</tr>--}}
                                                    {{--</tbody>--}}
                                                {{--</table>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>



@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {



        })
    </script>
@endsection

@extends('layouts.members_app')

@section('content')

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            {{--<div class="row no-padding">--}}
                            {{--<div class="col-md-6">--}}
                            {{--<strong>Name: {{ $user->name() }}</strong>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-6">--}}
                            {{--<strong>ID Number: {{ $user->id_number }}</strong>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-12">--}}
                            {{--<hr>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-12">--}}
                            {{--<ul class="nav nav-tabs">--}}
                            {{--<li class="nav-item">--}}
                            {{--<a class="nav-link active" href="{{ url("admin/user/{$id}/profile") }}">Personal Information </a>--}}
                            {{--</li>--}}
                            {{--@if($user->User->role != 'Member')--}}
                            {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="{{ url("admin/user/{$id}/employment-details") }}">Employment Details</a>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="{{ url("admin/user/{$id}/compensations-leave-credits") }}">Compensation and Leave Credits</a>--}}
                            {{--</li>--}}
                            {{--@endif--}}
                            {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-9">--}}
                            {{--<div>--}}
                            {{--<a href="{{ url("admin/user/{$id}/profile/edit") }}" class="btn btn-sm btn-primary pull-right" style="margin-top: 10px;"><span class="fa fa-edit"></span> Edit Profile</a>--}}
                            {{--</div>--}}
                            {{--<br>--}}
                            {{--<br>--}}

                            {{--<table class="table no-margin-bottom">--}}
                            {{--<tbody>--}}
                            {{--<tr>--}}
                            {{--<td width="110">First Name:</td>--}}
                            {{--<td width="">{{ strtoupper($user->first_name) }}</td>--}}
                            {{--<td width="130">Middle Name:</td>--}}
                            {{--<td width="">{{ strtoupper($user->middle_name) }}</td>--}}
                            {{--<td width="110">Last Name:</td>--}}
                            {{--<td width="">{{ strtoupper($user->last_name) }}</td>--}}
                            {{--</tr>--}}
                            {{--</tbody>--}}
                            {{--</table>--}}
                            {{--<table class="table">--}}
                            {{--<tbody>--}}
                            {{--<tr>--}}
                            {{--<td>Civil Status:</td>--}}
                            {{--<td>{{ strtoupper($user->CivilStatus->title) }}</td>--}}
                            {{--<td>Gender:</td>--}}
                            {{--<td colspan="3">{{ strtoupper($user->gender) }}</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                            {{--<td>Address:</td>--}}
                            {{--<td colspan="5">{{ strtoupper($user->address) }}</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                            {{--<td>Mobile:</td>--}}
                            {{--<td>{{ $user->mobile1 }}</td>--}}
                            {{--<td>{{ $user->mobile2 }}</td>--}}
                            {{--<td>Email:</td>--}}
                            {{--<td>{{ strtolower($user->email) }}</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                            {{--<td>Birth Date</td>--}}
                            {{--<td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->birthday)->format('M. j, Y') }}</td>--}}
                            {{--<td>Age</td>--}}
                            {{--<td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $user->birthday)->age }}</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                            {{--<td>Mother's Maiden <br />Name</td>--}}
                            {{--<td colspan="5">{{ strtoupper($user->mothers_maiden_name) }}</td></tr>--}}
                            {{--<tr>--}}
                            {{--<td>Educational Attainment</td>--}}
                            {{--<td colspan="5">{{ strtoupper($user->EducationalAttainment->title) }}</td>--}}
                            {{--</tr>--}}
                            {{--</tbody>--}}
                            {{--</table>--}}
                            {{--<hr />--}}
                            {{--<h2>In Case of Emergency</h2>--}}
                            {{--<table class="table">--}}
                            {{--<tbody>--}}
                            {{--<tr>--}}
                            {{--<td>Contact Person</td>--}}
                            {{--<td>{{ strtoupper($user->icoe_contact_name) }}</td>--}}
                            {{--<td>Contact Number</td>--}}
                            {{--<td>{{ strtoupper($user->icoe_contact_mobile) }}</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                            {{--<td>Address</td>--}}
                            {{--<td colspan="3">{{ strtoupper($user->icoe_contact_address) }}</td>--}}
                            {{--</tr>--}}
                            {{--</tbody>--}}
                            {{--</table>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-3">--}}

                            {{--</div>--}}
                            {{--</div>--}}

                            <div class="row">
                                <div class="col-md-6">
                                    <strong>Name: {{ $user->name() }}</strong>
                                </div>

                                <div class="col-md-6">
                                    <strong>ID Number: {{ $user->id_number }}</strong>
                                </div>

                                <div class="col-md-12">
                                    <hr>
                                </div>

                                <div class="col-md-12">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            {{--<a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>--}}
                                            <a class="nav-link active" href="{{ route('member_loan_payments', ['id' => $user->id ]) }}">Loans Payments</a>
                                        </li>
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link active" href="{{ url("admin/user/{$id}/profile") }}">Personal Information </a>--}}
                                            {{--<a class="nav-link" href="{{ url('/member/profile') }}">Personal Information </a>--}}
                                        {{--</li>--}}
                                        @if($user->User->role != 'Member')
                                            <li class="nav-item">
                                                {{--<a class="nav-link" href="{{ url("admin/user/{$id}/employment-details") }}">Employment Details</a>--}}
                                                <a class="nav-link" href="#">Employment Details</a>
                                            </li>
                                            <li class="nav-item">
                                                {{--<a class="nav-link" href="{{ url("admin/user/{$id}/compensations-leave-credits") }}">Compensation and Leave Credits</a>--}}
                                                <a class="nav-link" href="#">Compensation and Leave Credits</a>
                                            </li>
                                        @endif
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>--}}
                                            {{--<a class="nav-link" href="{{ route('member_loan_shares', ['id' => $user->id ]) }}">Loans and Shares</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link" href="{{ url("admin/user/{$id}/loans-shares") }}">Loans and Shares</a>--}}
                                            {{--<a class="nav-link active" href="{{ route('member_loan_payments', ['id' => $user->id ]) }}">Loans Payments</a>--}}
                                        {{--</li>--}}
                                    </ul>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <h4>Unpaid Loans</h4>
                                        <br>

                                        {{--<div class="text-right">--}}
                                        {{--<a href="{{ route('transactions_loan_add_payment') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Loan Payment</a>--}}
                                        {{--</div>--}}


                                        <table id="unpaidLoans" class="table">
                                            <thead>
                                            <tr class="text-center">
                                                <th>CLA Number</th>
                                                <th>ID Number</th>
                                                <th>Employee Name</th>
                                                <th>Loan Type</th>
                                                <th>Due Date</th>
                                                <th>Amortization</th>
                                                <th>Outstanding Amount</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                {{--<div class="col-md-3">--}}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    </div>
    </div>
    </div> 

@endsection

@section('extendedscript')
    <script>
        $(document).ready(function() {
            var table = $('#unpaidLoans').DataTable({
                "processing": true,
                "serverSide": true,
                "bFilter": false,
                "searching": true,
                "dom": "ltip",  // Remove global search box
                "ajax":{
                    "url": "{{ url('api/datatables/unpaid-loans/') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "cla_number" },
                    { "data": "id_number" },
                    { "data": "name" },
                    { "data": "loan_type" },
                    { "data": "due_date" },
                    { "data": "amortization" },
                    { "data": "outstanding_amount" },
                    { "data": "action" }
                ]

            });
        })
    </script>
@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('login/{id}', function(\Illuminate\Http\Request $request, $id) {
    Auth::loginUsingId($id);

    if($request->get('is_admin')) {
        session()->put('is_admin', TRUE);
    }

    return redirect('/authenticate');
});

Route::get('login/user/{username}', function(\Illuminate\Http\Request $request, $username) {
    Auth::loginUsingId($username);

    return redirect('/authenticate');
});

Route::get('login/admin/{username}', function(\Illuminate\Http\Request $request, $username) {
    Auth::loginUsingId($username);

    if($request->get('is_admin')) {
        session()->put('is_admin', TRUE);
    }

    return redirect('/authenticate');
});

Route::get('/', function () {
    if (!Auth::check()) {
        return redirect('login');
    }

    return redirect('/authenticate');
});

Route::get('/update-payroll-period', function () {
    foreach(\App\PayrollCycles::all() as $payrollCycle) {
        $period = explode('-', $payrollCycle->payroll_code);
        $payrollCycle->period = $period[1];
        $payrollCycle->save();
    }

});

Route::get('/reset-account-settings', function () {
    \App\Keyval::where('key', 'account_logo')->update(['value' => '']);
    \App\Keyval::where('key', 'account_name')->update(['value' => '']);
    \App\Keyval::where('key', 'account_code')->update(['value' => '']);
    \App\Keyval::where('key', 'address')->update(['value' => '']);
    \App\Keyval::where('key', 'email_address')->update(['value' => '']);
    \App\Keyval::where('key', 'contact_number')->update(['value' => '']);
    \App\Keyval::where('key', 'sss_number')->update(['value' => '']);
    \App\Keyval::where('key', 'hdmf_number')->update(['value' => '']);
    \App\Keyval::where('key', 'philhealth_number')->update(['value' => '']);
    \App\Keyval::where('key', 'tin_number')->update(['value' => '']);
    \App\Keyval::where('key', 'business_tax_type')->update(['value' => '']);
});

Route::get('/reset-clients', function () {
    \Illuminate\Support\Facades\DB::statement("truncate companies");
    \Illuminate\Support\Facades\DB::statement("truncate companies_contact_persons");
    \Illuminate\Support\Facades\DB::statement("truncate payroll_groups");
    \Illuminate\Support\Facades\DB::statement("truncate cost_centers");
});

Route::get('/reset-users', function() {
    \Illuminate\Support\Facades\DB::statement("truncate users");
    \Illuminate\Support\Facades\DB::statement("truncate profiles");
    \Illuminate\Support\Facades\DB::statement("truncate employment_details");
    \Illuminate\Support\Facades\DB::statement("truncate compensations");
    $pw = '$2y$10$yyoOuydPfTNEMWe2yuIpUODXnOPEQX7e4dS3/8.jh2k3kmeo36p2K';
    \Illuminate\Support\Facades\DB::statement("INSERT INTO `users` (`company_id`, `series`, `name`, `first_name`, `middle_name`, `last_name`, `username`, `email`, `password`, `is_new`, `role`, `remember_token`, `created_at`, `updated_at`) 
                                                VALUES ('', 1001, 'Administrator', '', '', '', 'admin', '', '". $pw ."', 'Y', 'SuperAdmin', 'Xd9XawlU4fTEm3kavD7mj9WgBDd6ly2ofWYBgNCIh6vd8athtPAvDlyRXPRo', NULL, '2019-09-08 14:34:50')");


//    return redirect('/');
});
//
//Route::get('/reset-all', function() {
//    \Illuminate\Support\Facades\DB::statement("truncate beneficiaries");
//    \Illuminate\Support\Facades\DB::statement("truncate cash_flows");
//    \Illuminate\Support\Facades\DB::statement("truncate cash_flow_items");
//    \Illuminate\Support\Facades\DB::statement("truncate cash_flow_item_list");
//    \Illuminate\Support\Facades\DB::statement("truncate cash_flow_particulars");
//    \Illuminate\Support\Facades\DB::statement("truncate cash_receipts");
//    \Illuminate\Support\Facades\DB::statement("truncate cash_receipt_items");
//    \Illuminate\Support\Facades\DB::statement("truncate check_requests");
//    \Illuminate\Support\Facades\DB::statement("truncate check_request_items");
//    \Illuminate\Support\Facades\DB::statement("truncate check_vouchers");
//    \Illuminate\Support\Facades\DB::statement("truncate check_voucher_items");
//    \Illuminate\Support\Facades\DB::statement("truncate final_pays");
//    \Illuminate\Support\Facades\DB::statement("truncate final_pay_13th_summary");
//    \Illuminate\Support\Facades\DB::statement("truncate final_pay_existing_loans");
//    \Illuminate\Support\Facades\DB::statement("truncate final_pay_other_charges");
//    \Illuminate\Support\Facades\DB::statement("truncate invoices");
//    \Illuminate\Support\Facades\DB::statement("truncate invoice_files");
//    \Illuminate\Support\Facades\DB::statement("truncate invoice_item_list");
////    \Illuminate\Support\Facades\DB::statement("truncate invoice_terms");
//    \Illuminate\Support\Facades\DB::statement("truncate journal_vouchers");
//    \Illuminate\Support\Facades\DB::statement("truncate journal_voucher_items");
//    \Illuminate\Support\Facades\DB::statement("truncate leave_history");
//    \Illuminate\Support\Facades\DB::statement("truncate loans");
//    \Illuminate\Support\Facades\DB::statement("truncate loan_payments");
//    \Illuminate\Support\Facades\DB::statement("truncate loan_payment_schedules");
//    \Illuminate\Support\Facades\DB::statement("truncate other_payments");
//    \Illuminate\Support\Facades\DB::statement("truncate payroll_cycles");
//    \Illuminate\Support\Facades\DB::statement("truncate payroll_dtr");
//    \Illuminate\Support\Facades\DB::statement("truncate payroll_dtr_draft");
//    \Illuminate\Support\Facades\DB::statement("truncate payroll_dtr_upload");
//    \Illuminate\Support\Facades\DB::statement("truncate payroll_summary");
//    \Illuminate\Support\Facades\DB::statement("truncate user_series");
//    \Illuminate\Support\Facades\DB::statement("truncate signatories");
//
//
//    \Illuminate\Support\Facades\DB::statement("truncate users");
//    \Illuminate\Support\Facades\DB::statement("truncate profiles");
//    \Illuminate\Support\Facades\DB::statement("truncate employment_details");
//    \Illuminate\Support\Facades\DB::statement("truncate compensations");
//    \Illuminate\Support\Facades\DB::statement("INSERT INTO `users` (`company_id`, `series`, `name`, `first_name`, `middle_name`, `last_name`, `username`, `email`, `password`, `is_new`, `role`, `admin_type`, `admin_username`, `remember_token`, `created_at`, `updated_at`) VALUES (NULL, 1001, 'Administrator', NULL, NULL, NULL, 'admin', '', '". '$2y$10$kiJsixxMWMvfER5i4CDsZe0rpKCoZwTYHqukteHQweoJPznPd9e/6' ."', 'Y', 'Employee', 'SuperAdmin', 'admin', 'RLNcxPH72thhxsyR0LgIE82lmTo7K2Q67MIJbk0F0BSjkunacpNRKgt9kyAd', NULL, '2018-01-11 12:49:49');");
//
//
//    return redirect('/');
//});

//Route::get('/update-user-series', function() {
//    ini_set('max_execution_time', 300); //300 seconds = 5 minutes
//    \Illuminate\Support\Facades\DB::statement("truncate user_series");
//
//    $userSeries = 1001;
//    for($userSeries; $userSeries < 100001; $userSeries++) {
//        if(\App\User::where('series', $userSeries)->count() == 0) {
//            \App\UserSeries::create(['series' => $userSeries]);
//        }
//    }
//});

//Route::get('/for-deletion', 'HomeController@for_deletion');
//Route::get('/payroll-group-update', 'HomeController@payroll_group_update');

Auth::routes();

Route::get('/logout', function () {
    if (Auth::check()) {
        Auth::logout();
    }

    session()->forget('is_admin');
    session()->forget('is_client');
    return redirect('/');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/fix/admin-users', function() {
    $admins = \App\User::where('admin_type', '!=', '')->where('admin_type', '!=', 'SuperAdmin')->get();
//    dd($admins);

    foreach($admins as $admin) {
        \App\User::create([
            'username' => $admin->admin_username,
            'password' => bcrypt($admin->admin_username),
            'name' => $admin->name,
            'role' => $admin->admin_type
        ]);
    }
});


//Route::group(['prefix' => "jobseeker", "middleware" => "jobseeker"], function() {

Route::group(['prefix' => "admin", 'middleware' => 'admin'], function () {
    Route::get('/dashboard', 'DashboardController@dashboard');
    Route::get('/notification/lapsed-loans', 'NotificationController@loan_lapses');
    Route::get('/notification/lapsed-loans/coop', 'NotificationController@coop_loan_lapses');

    // User Management
    Route::get('/user/add', 'Admin\UserController@add');
    Route::post('/user/add', 'Admin\UserController@add_post');

    Route::get('/users', 'Admin\UserController@index');
    Route::get('/users/{status?}', 'Admin\UserController@index');
    Route::get('/complete-registration', 'Admin\UserController@complete_registration');
    Route::get('/user/{id}', 'Admin\UserController@profile');
    Route::get('/user/{id}/profile', 'Admin\UserController@profile');
    Route::post('/user/{id}/profile/upload-photo', 'Admin\UserController@upload_photo');
    Route::get('/user/{id}/profile/edit', 'Admin\UserController@profile_edit');
    Route::post('/user/{id}/profile/edit', 'Admin\UserController@profile_edit_post');
    Route::get('/user/{id}/employment-details', 'Admin\UserController@employment_details');
    Route::get('/user/{id}/employment-details/edit', 'Admin\UserController@employment_details_edit');
    Route::post('/user/{id}/employment-details/edit', 'Admin\UserController@employment_details_edit_post');
    Route::get('/user/{id}/compensations-leave-credits', 'Admin\UserController@compensations');
    Route::get('/user/{id}/compensations-leave-credits/edit', 'Admin\UserController@compensations_edit');
    Route::post('/user/{id}/compensations-leave-credits/edit', 'Admin\UserController@compensations_edit_post');
    Route::get('/user/{id}/loans-shares', 'Admin\UserController@loans_shares');
    Route::post('/user/{id}/loans-shares', 'Admin\UserController@loans_shares_post');
    Route::get('/user/{id}/deactivate-user', 'Admin\UserController@deactivate_user');
    Route::post('/user/{id}/deactivate-user', 'Admin\UserController@deactivate_user_post');
    Route::get('/user/{id}/reactivate-user', 'Admin\UserController@reactivate_user');
    Route::get('/user/{id}/cancel-deactivation', 'Admin\UserController@cancel_deactivation');

    // Beneficiary
    Route::post('/user/{id}/beneficiary/add', 'Admin\UserController@beneficiary_add');
    Route::post('/user/{id}/beneficiary/edit/{b_id}', 'Admin\UserController@beneficiary_edit');
    Route::get('/user/{id}/beneficiary/delete/{b_id}', function ($user_id, $b_id) {
        App\Beneficiaries::find($b_id)->delete();
        return redirect()->back();
    });

    // Members
    Route::get('/member/add', 'Admin\MemberController@add');
    Route::post('/member/add', 'Admin\MemberController@add_post');
    Route::get('/member/add/coop', 'Admin\MemberController@add_coop');
    Route::post('/member/add/coop', 'Admin\MemberController@add_coop_post');

    // Employees
    Route::get('/employee/add', 'Admin\EmployeeController@add');
    Route::post('/employee/add', 'Admin\EmployeeController@add_post');
    Route::get('/employee/add/coop', 'Admin\EmployeeController@add_coop');
    Route::post('/employee/add/coop', 'Admin\EmployeeController@add_coop_post');
    Route::get('/employee/add/employment', 'Admin\EmployeeController@add_employment');
    Route::post('/employee/add/employment', 'Admin\EmployeeController@add_employment_post');
    Route::get('/employee/add/compensation', 'Admin\EmployeeController@add_compensation');
    Route::post('/employee/add/compensation', 'Admin\EmployeeController@add_compensation_post');

    // Client Management
    // Client Profile

    Route::get('/clients', 'Admin\ClientManagement\ClientController@index');
    Route::get('/clients/add', 'Admin\ClientManagement\ClientController@add');
    Route::post('/clients/add', 'Admin\ClientManagement\ClientController@add_post');
    Route::get('/clients/add/contact-persons', 'Admin\ClientManagement\ClientController@add_contact_persons');
    Route::post('/clients/add/contact-persons', 'Admin\ClientManagement\ClientController@add_contact_persons_post');
    Route::get('/clients/add/cost-center', 'Admin\ClientManagement\ClientController@add_cost_center');
    Route::post('/clients/add/cost-center', 'Admin\ClientManagement\ClientController@add_cost_center_post');
    Route::get('/clients/add/billing', 'Admin\ClientManagement\ClientController@add_billing');
    Route::post('/clients/add/billing', 'Admin\ClientManagement\ClientController@add_billing_post');
    Route::post('/clients/add/complete-registration', 'Admin\ClientManagement\ClientController@complete_registration');
    Route::get('/client/{id}', 'Admin\ClientManagement\ClientController@profile');
    Route::post('/client/{id}/profile/upload-logo', 'Admin\ClientManagement\ClientController@upload_logo');
    Route::get('/client/{id}/edit', 'Admin\ClientManagement\ClientController@profile_edit');
    Route::post('/client/{id}/edit', 'Admin\ClientManagement\ClientController@profile_edit_post');
    Route::get('/client/{id}/delete', function ($id) {
        App\Companies::find($id)->delete();
        return redirect(url('admin/clients'));
    });

    Route::get('/client/{id}/activate', function ($id) {
        App\Companies::where('id', $id)->update(['status' => 'Active']);

        session()->flash('popSuccess', 'Client successfully activated!');
        return redirect()->back();
    });

    Route::get('/client/{company}/deactivate', function (\App\Companies $company) {
        if($company->client_id === 'C01') {
            session()->flash('popError', 'This Client must always be Active.');
        } else {
            $company->status = 'Deactivated';
            $company->save();
            session()->flash('popSuccess', 'Client successfully deactivated!');
        }


        return redirect()->back();
    });
    // Contact Persons
    Route::get('/client/{id}/contact-persons', 'Admin\ClientManagement\ClientController@contact_persons');
    Route::post('/client/{id}/contact-persons', 'Admin\ClientManagement\ClientController@contact_persons_add');
    Route::post('/client/{id}/contact-persons', 'Admin\ClientManagement\ClientController@contact_persons_add');
    Route::post('/client/{id}/contact-persons/edit/{cp_id}', 'Admin\ClientManagement\ClientController@contact_persons_edit');
    Route::get('/client/{id}/contact-persons/delete/{cp_id}', function ($id, $cp_id) {
        App\CompaniesContactPersons::find($cp_id)->delete();
        return redirect()->back();
    });
    // Payroll Groups
    Route::post('/client/{id}/payroll-group/add', 'Admin\ClientManagement\ClientController@payroll_group_add');
    Route::post('/client/{id}/payroll-group/edit/{pg_id}', 'Admin\ClientManagement\ClientController@payroll_group_edit');
    Route::get('/client/{id}/payroll-group/delete/{pg_id}', function ($client_id, $pg_id) {
        App\PayrollGroups::find($pg_id)->delete();
        return redirect()->back();
    });
    // Cost Centers
    Route::get('/client/{id}/cost-center', 'Admin\ClientManagement\ClientController@cost_center');
    Route::post('/client/{id}/cost-center/add', 'Admin\ClientManagement\ClientController@cost_center_add');
    Route::post('/client/{id}/cost-center/edit/{cc_id}', 'Admin\ClientManagement\ClientController@cost_center_edit');
    Route::get('/client/{id}/cost-center/delete/{cc_id}', function ($client_id, $cc_id) {
        App\CostCenters::find($cc_id)->delete();
        return redirect()->back();
    });

//    Route::get('/client/{id}/billings', 'Admin\ClientManagement\ClientController@billings');
    Route::get('/client/{id}/billings', 'Admin\ClientManagement\ClientController@client_billing')->name('admin_client_billing');
    Route::post('/client/{id}/billings', 'Admin\ClientManagement\ClientController@client_billing_post')->name('admin_client_billing');
    Route::get('/client/{id}/user-accounts', 'Admin\ClientManagement\ClientController@user_accounts');

    Route::post('/client/{id}/admin-breakdown', 'Admin\ClientManagement\ClientController@admin_breakdown_post');

    /*
     * Manage User Admin
     */

    Route::get('/manage-admin/users', 'Admin\AdminController@index');
    Route::get('/manage-admin/users/add/', 'Admin\AdminController@add_admin');
    Route::post('/manage-admin/users/add/', 'Admin\AdminController@add_admin');
    Route::get('/manage-admin/user/{user_id}', 'Admin\AdminController@edit_admin');
    Route::post('/manage-admin/user/{user_id}', 'Admin\AdminController@edit_admin');

    Route::get('/manage-admin/delete/{user_id}', function($id) {
//        \App\User::where('id', $id)->update(['company_id' => '', 'admin_username' => '', 'admin_type' => '']);
        \App\User::where('id', $id)->delete();

        session()->flash('popSuccess', 'User Admin successfully removed!');
        return redirect()->back();
    });

    /*
     * Manage Client Admin
     */

    Route::get('/manage-admin/clients', 'Admin\AdminController@index_clients');
    Route::get('/manage-admin/clients/add/', 'Admin\AdminController@add_client_admin');
    Route::post('/manage-admin/clients/add/', 'Admin\AdminController@add_client_admin');
    Route::get('/manage-admin/client/{user_id}', 'Admin\AdminController@edit_client_admin');
    Route::post('/manage-admin/client/{user_id}', 'Admin\AdminController@edit_client_admin');
    Route::get('/manage-admin/client/delete/{user_id}', function($id) {
        if(in_array(\App\User::find($id)->role, ['Member', 'Employee'])) {
            \App\User::where('id', $id)->update(['client_type' => '', 'client_username' => '']);
        } else {
            \App\User::where('id', $id)->delete();
        }

        session()->flash('popSuccess', 'Client successfully removed!');
        return redirect()->back();
    });

//    Route::get('/user/add_admin/success', 'Admin\UserController@add_admin_success');


    //  Transactions
    // Payments
    Route::get('/transactions/payments', 'Admin\Transactions\PaymentController@index')->name('payments');
    Route::get('/transactions/payments/loans/add-payment', 'Admin\Transactions\PaymentController@loan_add_payment')->name('transactions_loan_add_payment');
    Route::get('/transactions/payments/coop_shares', 'Admin\Transactions\PaymentController@coop_shares')->name('coop_shares');
    Route::get('/transactions/payments/coop_shares/add', 'Admin\Transactions\PaymentController@coop_shares_add')->name('coop_shares_add');
    Route::get('/transactions/payments/coop-share/{id}', 'Admin\Transactions\PaymentController@coop_share_view');

    Route::get('/transactions/notarial_fee', 'Admin\Transactions\PaymentController@notarial_fee')->name('notarial_fee');
    Route::get('/transactions/payments/notarial-fee/{id}', 'Admin\Transactions\PaymentController@coop_share_view');

    Route::get('/transactions/membership_fee', 'Admin\Transactions\PaymentController@membership_fee')->name('membership_fee');
    Route::get('/transactions/payments/notarial-fee/{id}', 'Admin\Transactions\PaymentController@coop_share_view');

    Route::get('/transactions/payments/withdrawals', 'Admin\Transactions\PaymentController@withdrawals')->name('withdrawals');
    Route::get('/transactions/payments/withdrawals/add', 'Admin\Transactions\PaymentController@withdrawals_add')->name('withdrawals_add');
    Route::get('/transactions/payments/add', 'Admin\Transactions\PaymentController@add_other_payment')->name('add_other_payment');
    Route::post('/transactions/payments/add', 'Admin\Transactions\PaymentController@add_other_payment_post');
    // Loans
    Route::get('/transactions/loans', 'Admin\Transactions\LoansController@index')->name('loans');


    Route::get('/transactions/loans/status/{status}', 'Admin\Transactions\LoansController@index');

    Route::get('/transactions/loans/details', 'Admin\Transactions\LoansController@details_edit')->name('loans_details');
    Route::get('/transactions/loans/request', 'Admin\Transactions\LoansRequestController@create')->name('loans_request');
    Route::post('/transactions/loans/request', 'Admin\Transactions\LoansRequestController@create_post')->name('loans_request');

    Route::get('/transactions/loans/update/{id}', 'Admin\Transactions\LoansController@details_edit');
    Route::post('/transactions/loans/update/{id}', 'Admin\Transactions\LoansController@details_edit_post');

    Route::get('/transactions/loans/release/{id}', 'Admin\Transactions\LoansController@disbursed');
    Route::post('/transactions/loans/release/{id}', 'Admin\Transactions\LoansController@disbursed_post');

    Route::get('/transactions/loans/voucher', 'Admin\Transactions\LoansVoucherController@create')->name('loans_voucher');
    Route::post('/transactions/loans/voucher', 'Admin\Transactions\LoansVoucherController@create_post');

    Route::get('/transactions/loans/calculator', 'Admin\Transactions\LoansCalculatorController@create')->name('loans_calculator');


    Route::get('/transactions/loans/{id}/payments', 'Admin\Transactions\LoansController@payments');
    Route::get('/transactions/loans/{id}/payments/add', 'Admin\Transactions\LoansController@payment_add');
    Route::post('/transactions/loans/{id}/payments/add', 'Admin\Transactions\LoansController@payment_add_post');

    Route::get('/transactions/loans/{id}', 'Admin\Transactions\LoansController@details');
    Route::get('/transactions/loans/{id}/payment-schedule', 'Admin\Transactions\LoansController@payments_schedule');
    Route::get('/transactions/loans/{id}/loan-amortization-schedule', 'Admin\Transactions\LoansController@loan_amortization_schedule');

    // Invoices
    Route::get('/transactions/invoices/', 'Admin\Transactions\InvoicesController@view')->name('invoices');
    Route::get('/transactions/invoices/status/{status?}', 'Admin\Transactions\InvoicesController@view');
    Route::get('/transactions/invoices/edit/{id}', 'Admin\Transactions\InvoicesController@invoice_edit');
    Route::post('/transactions/invoices/edit/{id}', 'Admin\Transactions\InvoicesController@invoice_edit_post');
    Route::get('/transactions/invoices/view/{id}', 'Admin\Transactions\InvoicesController@invoice_view');
    Route::get('/transactions/invoices/print/{id}', 'Admin\Transactions\InvoicesController@invoice_print');
    Route::get('/transactions/invoices/send/{id}', 'Admin\Transactions\InvoicesController@invoice_send');
    Route::get('/transactions/invoices/delete/{id}', 'Admin\Transactions\InvoicesController@invoice_delete');
    Route::get('/transactions/invoices/file/{id}', function($id) {
        $file = \App\InvoiceFiles::find($id);

        return response()->download(
            storage_path("app/".$file->file_path),
            $file->file_name,
            [],
            'inline'
        );
    });
//    Route::get('/transactions/invoices/status/Draft', 'Admin\Transactions\InvoicesController@invoices_status_draft')->name('invoices_status_draft');
//    Route::get('/transactions/invoices/status/Unpaid', 'Admin\Transactions\InvoicesController@invoices_status_unpaid')->name('invoices_status_unpaid');
//    Route::get('/transactions/invoices/status/Paid', 'Admin\Transactions\InvoicesController@invoices_status_paid')->name('invoices_status_paid');
    Route::get('/transactions/invoices/items', 'Admin\Transactions\InvoicesController@item_list')->name('invoices_item');
    Route::get('/transactions/invoices/items/new_tax', 'Admin\Transactions\InvoicesItemController@add_new_tax')->name('invoices_tax');
    Route::get('/transactions/invoices/create_invoice', 'Admin\Transactions\InvoicesController@invoice_create')->name('invoices_create');
    Route::post('/transactions/invoices/create_invoice', 'Admin\Transactions\InvoicesController@invoice_create')->name('invoices_create');
    Route::post('/transactions/invoices/create_invoice_submit', 'Admin\Transactions\InvoicesController@invoice_create_post')->name('invoices_create_submit');
    Route::get('/transactions/invoices/create_invoice/description', 'Admin\Transactions\InvoicesController@add_description')->name('invoices_create_description');
    Route::get('/transactions/invoices/create_invoice/preview', 'Admin\Transactions\InvoicesController@preview')->name('invoices_preview');
    Route::get('/transactions/invoices/create_invoice/add_item', 'Admin\Transactions\InvoicesItemController@create')->name('invoices_create_item');
    Route::post('/transactions/invoices/create_invoice/add_item', 'Admin\Transactions\InvoicesItemController@create_post')->name('invoices_create_item');
    Route::get('/transactions/invoices/create_invoice/add_invoice_item', 'Admin\Transactions\InvoicesItemController@add_invoice_item')->name('add_invoice_item');
    Route::post('/transactions/invoices/create_invoice/add_invoice_item', 'Admin\Transactions\InvoicesItemController@add_invoice_item_post')->name('add_invoice_item');
    Route::get('/transactions/invoices/{id}/update_invoice' . 'Admin\Transactions\InvoicesController@invoice_edit');


    // Payroll
    // Payroll Wizard
    Route::get('/payroll/wizard', 'Admin\Payroll\WizardController@index')->name('payroll_wizard_index');
    Route::get('/payroll/wizard/payroll_cycle', 'Admin\Payroll\WizardController@create_payroll_cycle')->name('payroll_cycle');
    Route::post('/payroll/wizard/payroll_cycle', 'Admin\Payroll\WizardController@create_payroll_cycle_post')->name('payroll_cycle');

    Route::get('/payroll/wizard/payroll_members/{id}', 'Admin\Payroll\WizardController@payroll_members')->name('payroll_members');
    Route::get('/payroll/wizard/payroll_members/{id}/upload_dtr', 'Admin\Payroll\WizardController@upload_dtr')->name('payroll_upload_dtr');
    Route::post('/payroll/wizard/payroll_members/{id}/upload_dtr', 'Admin\Payroll\WizardController@upload_dtr_post')->name('payroll_upload_dtr');
    Route::get('/payroll/wizard/payroll_members/{id}/upload_dtr/Approve', 'Admin\Payroll\WizardController@upload_dtr_approve');
    Route::get('/payroll/wizard/payroll_members/{id}/upload_dtr/Cancel', 'Admin\Payroll\WizardController@upload_dtr_cancel');
    Route::post('/payroll/wizard/payroll_members/{id}/upload_dtr/status/Existing', 'Admin\Payroll\WizardController@upload_dtr_existing');
    Route::get('/payroll/wizard/payroll_members/{id}/upload_dtr/status/{status}', 'Admin\Payroll\WizardController@upload_dtr')->name('payroll_upload_dtr_status');
    Route::get('/payroll/wizard/payroll_members/{id}/upload_dtr/manually', 'Admin\Payroll\WizardController@upload_dtr_manually')->name('payroll_upload_dtr_manually');
    Route::get('/payroll/wizard/payroll_members/{id}/upload_dtr/failed', 'Admin\Payroll\WizardController@upload_dtr_failed')->name('payroll_upload_dtr_failed');
    Route::get('/payroll/wizard/payroll_members/{id}/reset', 'Admin\Payroll\WizardController@reset');
    Route::get('/payroll/wizard/payroll_members/{id}/process', 'Admin\Payroll\WizardController@process')->name('payroll_process');
    Route::post('/payroll/wizard/payroll_members/{id}/process', 'Admin\Payroll\WizardController@process_post');
    Route::get('/payroll/wizard/payroll_members/{id}/processed_payroll', 'Admin\Payroll\WizardController@processed');
    Route::get('/payroll/wizard/payroll_members/{id}/payslip/{user_id}', 'Admin\Payroll\WizardController@processed_payroll');
    Route::get('/payroll/wizard/payroll_members/{id}/payslip/{user_id}/edit', 'Admin\Payroll\WizardController@payslip_edit');
    Route::post('/payroll/wizard/payroll_members/{id}/payslip/{user_id}/edit', 'Admin\Payroll\WizardController@payslip_edit_post');
    Route::get('/payroll/wizard/payroll_members/{id}/payslip_preview/{user_id}', 'Admin\Payroll\WizardController@payslip_preview');

    // Export to Excel
    Route::get('/payroll/wizard/export-open/{id}', 'Admin\Payroll\WizardController@exportOpen');
    Route::get('/payroll/wizard/export-processed/{id}', 'Admin\Payroll\WizardController@exportProcessed');

    // Last Pay
    Route::get('/payroll/last_pay', 'Admin\Payroll\LastPayController@index')->name('last_pay');

    // Final Pay
    Route::get('/payroll/final_pay/{status?}', 'Admin\Payroll\LastPayController@final_pay')->name('final_pay');
    Route::get('/payroll/final_pay/add/{user_id}', 'Admin\Payroll\LastPayController@final_pay_add');
    Route::post('/payroll/final_pay/add/{user_id}', 'Admin\Payroll\LastPayController@final_pay_add_post');
    Route::get('/payroll/final_pay/update/{final_pay_id}', 'Admin\Payroll\LastPayController@update');
    Route::post('/payroll/final_pay/update/{final_pay_id}', 'Admin\Payroll\LastPayController@update_post');
    Route::get('/payroll/final_pay/release/{final_pay_id}', 'Admin\Payroll\LastPayController@release');
    Route::get('/payroll/final_pay/preview/{final_pay_id}', 'Admin\Payroll\LastPayController@preview');
    Route::get('/payroll/final_pay/disburse/{final_pay_id}', 'Admin\Payroll\LastPayController@disburse');
    Route::post('/payroll/final_pay/disburse/{final_pay_id}', 'Admin\Payroll\LastPayController@disburse_post');
    Route::get('/payroll/final_pay_details/{dtr_id}', 'Admin\Payroll\LastPayController@final_pay_details');
    Route::get('/payroll/final_pay_preview', 'Admin\Payroll\LastPayController@final_pay_preview');

    // Accounting
    // Check Request
    Route::get('/accounting/check_request/', 'Admin\Accounting\CheckRequestController@index')->name('check_request');
    Route::get('/accounting/check_request/status/Pending', 'Admin\Accounting\CheckRequestController@index')->name('check_request_pending');
    Route::get('/accounting/check_request/status/Approved', 'Admin\Accounting\CheckRequestController@index')->name('check_request_approved');
    Route::get('/accounting/check_request/status/Cancelled', 'Admin\Accounting\CheckRequestController@index')->name('check_request_cancelled');
    Route::get('/accounting/check_request/create', 'Admin\Accounting\CheckRequestController@create_check_request')->name('create_check_request');
    Route::post('/accounting/check_request/create', 'Admin\Accounting\CheckRequestController@create_check_request_post')->name('create_check_request');
    Route::get('/accounting/check_request/print/{id}', 'Admin\Accounting\CheckRequestController@print');
    Route::get('/accounting/check_request/view/{id}', 'Admin\Accounting\CheckRequestController@update_check_request')->name('view_check_request');
    Route::post('/accounting/check_request/view/{id}', 'Admin\Accounting\CheckRequestController@update_check_request_post');


    // Payee Names
    Route::get('/accounting/check_request/payee-names', 'Admin\Accounting\CheckRequestController@add_payee_name')->name('payee_names');
    Route::post('/accounting/check_request/payee-names', 'Admin\Accounting\CheckRequestController@add_payee_name')->name('payee_names');
    Route::post('/accounting/check_request/payee-names/{id}', 'Admin\Accounting\CheckRequestController@edit_payee_name');
    Route::get('/accounting/check_request/payee-names-delete/{id}', 'Admin\Accounting\CheckRequestController@delete_payee_name');


    // Cash Flow Items
    Route::get('/accounting/check_request/cash-flow-items', 'Admin\Accounting\CheckRequestController@add_cash_flow_items')->name('cash_flow_items');
    Route::post('/accounting/check_request/cash-flow-items', 'Admin\Accounting\CheckRequestController@add_cash_flow_items')->name('cash_flow_items');
    Route::get('/accounting/check_request/cash-flow-items-delete/{id}', 'Admin\Accounting\CheckRequestController@delete_cash_flow_items')->name('cash_flow_item_delete');

    // For Request
    Route::get('/accounting/check_request/for_request', 'Admin\Accounting\CheckRequestController@for_request')->name('for_request');


    Route::get('/accounting/check_request/update/{id}', 'Admin\Accounting\CheckRequestController@update_check_request')->name('view_check_request');
    Route::post('/accounting/check_request/update/{id}', 'Admin\Accounting\CheckRequestController@update_check_request_post');
    Route::get('/accounting/check_request/{status?}/{id}', 'Admin\Accounting\CheckRequestController@update_check_request_status')->name('view_check_request');


    // Cash Flow
    Route::get('/accounting/cash_flow/', 'Admin\Accounting\CashFlowController@index')->name('cash_flow');
    Route::get('/accounting/cash_flow/create', 'Admin\Accounting\CashFlowController@create_get')->name('cash_flow_create');
    Route::post('/accounting/cash_flow/create', 'Admin\Accounting\CashFlowController@create_post')->name('cash_flow_create_post');
    Route::get('/accounting/cash_flow/edit/{cashFlowId}', 'Admin\Accounting\CashFlowController@cash_flow_update');
    Route::post('/accounting/cash_flow/edit/{cashFlowId}', 'Admin\Accounting\CashFlowController@cash_flow_update_post');
    Route::get('/accounting/cash_flow_update_request/{checkRequestId}', 'Admin\Accounting\CashFlowController@cash_flow_update_request')->name('cash_flow_update_request');
    Route::post('/accounting/cash_flow_update_request/{checkRequestId}', 'Admin\Accounting\CashFlowController@cash_flow_update_request_post')->name('cash_flow_update_request');

    // Check Voucher
    Route::get('/accounting/check_voucher', 'Admin\Accounting\CheckVoucherController@index')->name('check_voucher');
    Route::get('/accounting/check_voucher/print/{id}', 'Admin\Accounting\CheckVoucherController@print');
    Route::get('/accounting/check_voucher/view/{id}', 'Admin\Accounting\CheckVoucherController@view_check_voucher');
    Route::get('/accounting/check_voucher/edit/{id}', 'Admin\Accounting\CheckVoucherController@update_check_voucher');
    Route::post('/accounting/check_voucher/edit/{id}', 'Admin\Accounting\CheckVoucherController@update_check_voucher_post');

    // Cash Receipt
    Route::get('/accounting/cash_receipt', 'Admin\Accounting\CashReceiptController@index')->name('cash_receipt');
    Route::get('/accounting/cash_receipt/create', 'Admin\Accounting\CashReceiptController@create_cash_receipt')->name('create_cash_receipt');
    Route::post('/accounting/cash_receipt/create', 'Admin\Accounting\CashReceiptController@post');

    // General Ledger
    Route::get('/accounting/general_ledger', 'Admin\Accounting\GeneralLedgerController@index')->name('general_ledger');
    Route::get('/accounting/general_ledger/download', 'Admin\Accounting\GeneralLedgerController@download');
    Route::get('/accounting/general_ledger/update/{transaction_id}', 'Admin\Accounting\GeneralLedgerController@update')->name('update_general_ledger');

    // Journal Voucher
    Route::get('/accounting/journal_voucher', 'Admin\Accounting\JournalVoucherController@index')->name('journal_voucher');
    Route::get('/accounting/journal_voucher/create', 'Admin\Accounting\JournalVoucherController@create_journal_voucher')->name("create_journal_voucher");
    Route::post('/accounting/journal_voucher/create', 'Admin\Accounting\JournalVoucherController@create');
    Route::get('/accounting/journal_voucher/edit/{id}', 'Admin\Accounting\JournalVoucherController@edit_journal_voucher');
    Route::post('/accounting/journal_voucher/edit/{id}', 'Admin\Accounting\JournalVoucherController@update');
    Route::get('/accounting/journal_voucher/print/{id}', 'Admin\Accounting\JournalVoucherController@print');

    // Super Admin or Admin admin routes
    Route::get('/super/admin_tools', 'Admin\Head\AdminToolsController@index')->name('head_admin_tools');
    Route::get('/super/admin_settings', 'Admin\Head\AdminToolsController@settings')->name('head_admin_settings');

    Route::get('/super/position', 'Admin\Head\PositionController@index')->name('head_position');
    Route::get('/super/position/create', 'Admin\Head\PositionController@create_position')->name('head_position_create');
    Route::post('/super/position/create', 'Admin\Head\PositionController@create_position_post')->name('head_position_create');
    Route::post('/super/position/edit/{id}', 'Admin\Head\PositionController@position_edit');
    Route::get('/super/position/delete/{id}', 'Admin\Head\PositionController@position_delete');

    Route::get('/super/educational-attainment', 'Admin\Head\EducationalAttainmentController@index')->name('head_educational_attainment');
    Route::get('/super/educational-attainment/create', 'Admin\Head\EducationalAttainmentController@create_educational_attainment')->name('head_educational_attainment_create');
    Route::post('/super/educational-attainment/create', 'Admin\Head\EducationalAttainmentController@create_educational_attainment_post')->name('head_educational_attainment_create');
    Route::post('/super/educational-attainment/edit/{id}', 'Admin\Head\EducationalAttainmentController@educational_attainment_edit');
    Route::get('/super/educational-attainment/delete/{id}', 'Admin\Head\EducationalAttainmentController@educational_attainment_delete');

    Route::get('/super/audit-log', 'Admin\Head\AuditLogController@index')->name('head_audit_log');
    Route::post('/super/audit-log/datatables', 'Admin\Head\AuditLogController@datatables');
    Route::get('/super/civil-status', 'Admin\Head\CivilStatusController@index')->name('head_civil_status');
    Route::get('/super/civil-status/create', 'Admin\Head\CivilStatusController@create_civil_status')->name('head_civil_status_create');
    Route::post('/super/civil-status/create', 'Admin\Head\CivilStatusController@create_civil_status_post')->name('head_civil_status_create');
    Route::post('/super/civil-status/edit/{id}', 'Admin\Head\CivilStatusController@civil_status_edit');
    Route::get('/super/civil-status/delete/{id}', 'Admin\Head\CivilStatusController@civil_status_delete');

    Route::get('/super/daily-category', 'Admin\Head\DailyCategoryController@index')->name('head_daily_category');
    Route::get('/super/daily-category/create', 'Admin\Head\DailyCategoryController@create_daily_category')->name('head_daily_category_create');
    Route::post('/super/daily-category/create', 'Admin\Head\DailyCategoryController@create_daily_category_post')->name('head_daily_category_create');
    Route::post('/super/daily-category/edit/{id}', 'Admin\Head\DailyCategoryController@daily_category_edit');
    Route::get('/super/daily-category/delete/{id}', 'Admin\Head\DailyCategoryController@daily_category_delete');

    Route::get('/super/mode-of-transfer', 'Admin\Head\ModeOfTransferController@index')->name('head_mode_of_transfer');
    Route::get('/super/mode-of-transfer/create', 'Admin\Head\ModeOfTransferController@create_mode_of_transfer')->name('head_mode_of_transfer_create');
    Route::post('/super/mode-of-transfer/create', 'Admin\Head\ModeOfTransferController@create_mode_of_transfer_post')->name('head_mode_of_transfer_create');
    Route::post('/super/mode-of-transfer/edit/{id}', 'Admin\Head\ModeOfTransferController@mode_of_transfer_edit');
    Route::get('/super/mode-of-transfer/delete/{id}', 'Admin\Head\ModeOfTransferController@mode_of_transfer_delete');

    Route::get('/super/tax-type', 'Admin\Head\TaxTypeController@index')->name('head_tax_type');
    Route::get('/super/tax-type/create', 'Admin\Head\TaxTypeController@create_tax_type')->name('head_tax_type_create');
    Route::post('/super/tax-type/create', 'Admin\Head\TaxTypeController@tax_type_create_post')->name('head_tax_type_create_post');

    Route::get('/super/business-tax-type', 'Admin\Head\BusinessTaxTypeController@index')->name('head_business_tax_type');
    Route::get('/super/business-tax-type/create', 'Admin\Head\BusinessTaxTypeController@create_tax_type')->name('head_business_tax_type_create');
    Route::post('/super/business-tax-type/create', 'Admin\Head\BusinessTaxTypeController@tax_type_create_post');
    Route::post('/super/business-tax-type/edit/{id}', 'Admin\Head\BusinessTaxTypeController@edit');
    Route::get('/super/business-tax-type/delete/{id}', 'Admin\Head\BusinessTaxTypeController@delete');

    Route::get('/super/loan-type', 'Admin\Head\LoanTypeController@index')->name('head_loan_type');
    Route::get('/super/loan-type/create', 'Admin\Head\LoanTypeController@create_tax_type')->name('head_loan_type_create');
    Route::post('/super/loan-type/create', 'Admin\Head\LoanTypeController@tax_type_create_post');
    Route::post('/super/loan-type/edit/{id}', 'Admin\Head\LoanTypeController@edit');
    Route::get('/super/loan-type/delete/{id}', 'Admin\Head\LoanTypeController@delete');

    // Earning type renamed to Income Type
    Route::get('/super/income-type', 'Admin\Head\EarningTypeController@index')->name('head_earning_type');
    Route::get('/super/income-type', 'Admin\Head\EarningTypeController@index')->name('head_earning_type');
    Route::get('/super/income-type/create', 'Admin\Head\EarningTypeController@create_earning_type')->name('head_earning_type_create');

    Route::get('/super/deduction-type', 'Admin\Head\DeductionTypeController@index')->name('head_deduction_type');
    Route::get('/super/deduction-type/create', 'Admin\Head\DeductionTypeController@create_deduction_type')->name('head_deduction_type_create');

    Route::get('/super/area-mall-outlet', 'Admin\Head\AreaMallOutletController@index')->name('head_area_mall_outlet');
    Route::get('/super/area-mall-outlet/create', 'Admin\Head\AreaMallOutletController@create_area_mall_outlet')->name('head_area_mall_outlet_create');
    Route::post('/super/area-mall-outlet/create', 'Admin\Head\AreaMallOutletController@create_area_mall_outlet_post')->name('head_area_mall_outlet_create');
    Route::post('/super/department/edit/{id}', 'Admin\Head\AreaMallOutletController@area_mall_outlet_edit');
    Route::get('/super/department/delete/{id}', 'Admin\Head\AreaMallOutletController@area_mall_outlet_delete');

    Route::get('/super/provinces', 'Admin\Head\AreaMallOutletController@provinces')->name('head_provinces');
    Route::post('/super/provinces', 'Admin\Head\AreaMallOutletController@provinces');
    Route::post('/super/province/edit/{id}', 'Admin\Head\AreaMallOutletController@province_edit');
    Route::get('/super/province/delete/{id}', 'Admin\Head\AreaMallOutletController@province_delete');

    Route::get('/super/coop-share-interest', 'Admin\Head\CoopShareInterestController@index')->name('head_coop_share_interest');
    Route::post('/super/coop-share-interest', 'Admin\Head\CoopShareInterestController@save');

    Route::get('/super/loanable-percentage', 'Admin\Head\LoanablePercentageController@index')->name('head_loanalable_percentage');
    Route::post('/super/loanable-percentage', 'Admin\Head\LoanablePercentageController@save');

    Route::get('/super/membership-fee', 'Admin\Head\MembershipFeeController@index')->name('head_membership_fee');
    Route::post('/super/membership-fee', 'Admin\Head\MembershipFeeController@save');

    Route::get('/super/notarial-fee', 'Admin\Head\NotarialFeeController@index')->name('head_notarial_fee');
    Route::post('/super/notarial-fee', 'Admin\Head\NotarialFeeController@save');

    Route::get('/super/income-deduction-items', 'Admin\Head\IncomeDeductionItemsController@index')->name('head_income_deduction_items');
    Route::get('/super/income-deduction-items/{type}', 'Admin\Head\IncomeDeductionItemsController@index');
    Route::post('/super/income-deduction-items', 'Admin\Head\IncomeDeductionItemsController@save');
    Route::post('/super/income-deduction-items/deductions', 'Admin\Head\IncomeDeductionItemsController@save');
    Route::get('/super/income-deduction-item/delete/{id}', 'Admin\Head\IncomeDeductionItemsController@delete');

    Route::get('/super/areas', 'Admin\Head\AreaMallOutletController@areas')->name('head_areas');
    Route::post('/super/areas', 'Admin\Head\AreaMallOutletController@areas');
    Route::post('/super/area/edit/{id}', 'Admin\Head\AreaMallOutletController@area_edit');
    Route::get('/super/area/delete/{id}', 'Admin\Head\AreaMallOutletController@area_delete');

    Route::get('/super/malls', 'Admin\Head\AreaMallOutletController@malls')->name('head_malls');
    Route::post('/super/malls', 'Admin\Head\AreaMallOutletController@malls');
    Route::post('/super/mall/edit/{id}', 'Admin\Head\AreaMallOutletController@mall_edit');
    Route::get('/super/mall/delete/{id}', 'Admin\Head\AreaMallOutletController@mall_delete');

    Route::get('/super/outlets', 'Admin\Head\AreaMallOutletController@outlets')->name('head_outlets');
    Route::post('/super/outlets', 'Admin\Head\AreaMallOutletController@outlets');
    Route::post('/super/outlet/edit/{id}', 'Admin\Head\AreaMallOutletController@outlet_edit');
    Route::get('/super/outlet/delete/{id}', 'Admin\Head\AreaMallOutletController@outlet_delete');


    Route::get('/super/relationship', 'Admin\Head\RelationshipController@relationship')->name('head_relationship');
    Route::get('/super/relationship/create', 'Admin\Head\RelationshipController@relationship_create')->name('head_relationship_create');
    Route::post('/super/relationship/create', 'Admin\Head\RelationshipController@relationship_create_post')->name('head_relationship_create_post');
    Route::post('/super/relationship/edit/{id}', 'Admin\Head\RelationshipController@relationship_edit');
    Route::get('/super/relationship/delete/{id}', 'Admin\Head\RelationshipController@relationship_delete');

    Route::get('/super/bank', 'Admin\Head\BankController@bank')->name('head_bank');
    Route::get('/super/bank/create', 'Admin\Head\BankController@bank_create')->name('head_bank_create');
    Route::post('/super/bank/create', 'Admin\Head\BankController@bank_create_post')->name('head_bank_create_post');
    Route::post('/super/bank/edit/{id}', 'Admin\Head\BankController@bank_edit');
    Route::get('/super/bank/delete/{id}', 'Admin\Head\BankController@bank_delete');

    Route::get('/super/signatories/', 'Admin\Head\SignatoriesController@signatories')->name('head_signatories');
    Route::get('/super/signatories/create/{role?}', 'Admin\Head\SignatoriesController@signatories_create')->name('head_signatories_create');
    Route::post('/super/signatories/create/{role?}', 'Admin\Head\SignatoriesController@signatories_create_post')->name('head_signatories_create');
    Route::get('/super/signatories/delete/{id?}', 'Admin\Head\SignatoriesController@signatories_delete');

    Route::get('/super/membership_type', 'Admin\Head\MembershipTypeController@membership_type')->name('head_membership_type');
    Route::get('/super/membership_type/create', 'Admin\Head\MembershipTypeController@membership_type_create')->name('head_membership_type_create');

    Route::get('/super/membership_category', 'Admin\Head\MembershipCategoryController@membership_category')->name('head_membership_category');
    Route::get('/super/membership_category/create', 'Admin\Head\MembershipCategoryController@membership_category_create')->name('head_membership_category_create');

    Route::get('/super/monthly_tax_schedule/', 'Admin\Head\TaxScheduleController@monthly_tax_schedule')->name('head_monthly_tax_schedule');
    Route::get('/super/monthly_tax_schedule/update', 'Admin\Head\TaxScheduleController@monthly_tax_schedule_update')->name('head_monthly_tax_schedule_update');

    Route::get('/super/semi_monthly_tax_schedule', 'Admin\Head\TaxScheduleController@semi_monthly_tax_schedule')->name('head_semi_monthly_tax_schedule');
    Route::get('/super/semi_monthly_tax_schedule_update', 'Admin\Head\TaxScheduleController@semi_monthly_tax_schedule_update')->name('head_semi_monthly_tax_schedule_update');

    Route::get('/super/weekly_tax_schedule', 'Admin\Head\TaxScheduleController@weekly_tax_schedule')->name('head_weekly_tax_schedule');
    Route::get('/super/weekly_tax_schedule_update', 'Admin\Head\TaxScheduleController@weekly_tax_schedule_update')->name('head_weekly_tax_schedule_update');

    Route::get('/super/daily_tax_schedule', 'Admin\Head\TaxScheduleController@daily_tax_schedule')->name('head_daily_tax_schedule');
    Route::get('/super/daily_tax_schedule_update', 'Admin\Head\TaxScheduleController@daily_tax_schedule_update')->name('head_daily_tax_schedule_update');

    Route::get('/super/sss_schedule', 'Admin\Head\SSSController@sss_schedule')->name('head_sss_schedule');
    Route::get('/super/sss_schedule/create', 'Admin\Head\SSSController@create_sss_schedule')->name('sss_schedule_create');
    Route::post('/super/sss_schedule/create', 'Admin\Head\SSSController@create_sss_schedule_post')->name('sss_schedule_create');
    Route::post('/super/sss_schedule/edit/{id}', 'Admin\Head\SSSController@sss_schedule_edit');
    Route::get('/super/sss_schedule/delete/{id}', 'Admin\Head\SSSController@sss_schedule_delete');
    Route::get('/super/sss_schedule/upload', 'Admin\Head\SSSController@sss_schedule_update')->name('head_sss_schedule_update');

    Route::get('/super/hdmf_schedule', 'Admin\Head\HDMFController@hdmf_schedule')->name('head_hdmf_schedule');
    Route::get('/super/hdmf_schedule/update', 'Admin\Head\HDMFController@hdmf_schedule_update')->name('head_hdmf_schedule_update');
    Route::get('/super/hdmf_schedule/create', 'Admin\Head\HDMFController@create_hdmf_schedule')->name('hdmf_schedule_create');
    Route::post('/super/hdmf_schedule/create', 'Admin\Head\HDMFController@create_hdmf_schedule_post')->name('hdmf_schedule_create');
    Route::post('/super/hdmf_schedule/edit/{id}', 'Admin\Head\HDMFController@hdmf_schedule_edit');
    Route::get('/super/hdmf_schedule/delete/{id}', 'Admin\Head\HDMFController@hdmf_schedule_delete');

    Route::get('/super/philhealth_schedule', 'Admin\Head\PhilHealthController@philhealth_schedule')->name('head_philhealth_schedule');
    Route::get('/super/philhealth_schedule/update', 'Admin\Head\PhilHealthController@philhealth_schedule_update')->name('head_philhealth_schedule_update');
    Route::get('/super/philhealth_schedule/create', 'Admin\Head\PhilHealthController@create_philhealth_schedule')->name('philhealth_schedule_create');
    Route::post('/super/philhealth_schedule/create', 'Admin\Head\PhilHealthController@create_philhealth_schedule_post')->name('philhealth_schedule_create');
    Route::post('/super/philhealth_schedule/edit/{id}', 'Admin\Head\PhilHealthController@philhealth_schedule_edit');
    Route::get('/super/philhealth_schedule/delete/{id}', 'Admin\Head\PhilHealthController@philhealth_schedule_delete');

    Route::get('/super/import/employees', 'Admin\Head\ImportEmployeesController@index')->name('import_employees');
    Route::post('/super/import/employees', 'Admin\Head\ImportEmployeesController@post')->name('import_employees_post');
    Route::get('/super/import/members', 'Admin\Head\ImportMembersController@index')->name('import_members');
    Route::post('/super/import/members', 'Admin\Head\ImportMembersController@post')->name('import_members_post');
    Route::get('/super/import/loans', 'Admin\Head\ImportLoansController@index')->name('import_loans');
    Route::post('/super/import/loans', 'Admin\Head\ImportLoansController@post')->name('import_loans_post');
    Route::get('/super/import/loan-payments', 'Admin\Head\ImportLoanPaymentsController@index')->name('import_loan_payments');
    Route::post('/super/import/loan-payments', 'Admin\Head\ImportLoanPaymentsController@post')->name('import_loan_payments_post');
    Route::get('/super/import/other-payments', 'Admin\Head\ImportOtherPaymentsController@index')->name('import_other_payments');
    Route::post('/super/import/other-payments', 'Admin\Head\ImportOtherPaymentsController@post')->name('import_other_payments_post');

    Route::get('/super/account', 'Admin\Head\AccountController@account')->name('head_account');
    Route::post('/super/account', 'Admin\Head\AccountController@account_post')->name('head_account');

    Route::get('/super/area', 'Admin\Head\AreaController@area')->name('head_area');
    Route::get('/super/area/create', 'Admin\Head\AreaController@area_create')->name('head_area_create');

    Route::get('/super/mall', 'Admin\Head\MallController@index')->name('head_mall');
    Route::get('/super/mall/create', 'Admin\Head\MallController@create_mall')->name('head_mall_create');

    Route::get('/super/store_outlet', 'Admin\Head\StoreOutletController@index')->name('head_store_outlet');
    Route::get('/super/store_outlet/create', 'Admin\Head\StoreOutletController@create_store_outlet')->name('head_store_outlet_create');

    Route::get('/super/income_category', 'Admin\Head\IncomeCategoryController@index')->name('head_income_category');
    Route::get('/super/income_category/create', 'Admin\Head\IncomeCategoryController@create_income_category')->name('head_income_category_create');

    Route::get('/super/deduction_category', 'Admin\Head\DeductionCategoryController@index')->name('head_deduction_category');
    Route::get('/super/deduction_category/create', 'Admin\Head\DeductionCategoryController@create_deduction_category')->name('head_deduction_category_create');

    Route::get('/super/premium-rate', 'Admin\Head\PremiumRateController@index')->name('head_premium_rate');
    Route::get('/super/premium-rate/{id}/edit', 'Admin\Head\PremiumRateController@edit_premium_rate');
    Route::post('/super/premium-rate/{id}/edit', 'Admin\Head\PremiumRateController@edit_premium_rate_post');
    Route::get('/super/premium-rate/create', 'Admin\Head\PremiumRateController@create_premium_rate')->name('head_premium_rate_create');

    Route::get('/super/admin/password_reset', 'PasswordResetController@admin_password_reset')->name('admin_password_reset');
    Route::get('/super/client/password_reset', 'PasswordResetController@client_password_reset')->name('client_password_reset');
    Route::get('/super/employees-members/password_reset/{userAccess?}', 'PasswordResetController@employees_and_members_password_reset')->name('employees_and_members_password_reset');
    Route::post('/super/employees-members/password_reset/{userAccess?}', 'PasswordResetController@reset_password_post');

    // Reports
    Route::get('/reports/user_profiles/masterlist', 'Admin\Reports\UserProfilesController@masterlist')->name('reports_masterlist');
    Route::get('/reports/user_profiles/masterlist/download', 'Admin\Reports\UserProfilesController@masterlist_download');
    Route::get('/reports/user_profiles/coop_members_list', 'Admin\Reports\UserProfilesController@coop_members_list')->name('reports_coop_members_list');
    Route::get('/reports/user_profiles/employee_members_list', 'Admin\Reports\UserProfilesController@employee_members_list')->name('reports_employee_members_list');
    Route::get('/reports/user_profiles/leave_credits', 'Admin\Reports\UserProfilesController@leave_credits')->name('reports_leave_credits');
    Route::get('/reports/user_profiles/leave_credits/download', 'Admin\Reports\UserProfilesController@leave_credits_download');
    Route::get('/reports/user_profiles/sss_ee_er_contribution', 'Admin\Reports\UserProfilesController@sss_contribution')->name('report_sss_contribution');
    Route::get('/reports/user_profiles/hdmf_ee_er_contribution', 'Admin\Reports\UserProfilesController@hdmf_contribution')->name('report_hdmf_contribution');

    Route::get('/reports/client_profiles/masterlist', 'Admin\Reports\ClientProfilesController@masterlist')->name('reports_client_masterlist');
    Route::get('/reports/client_profiles/masterlist/download', 'Admin\Reports\ClientProfilesController@masterlist_download')->name('reports_client_masterlist_download');
    Route::get('/reports/client_profiles/cost_centers', 'Admin\Reports\ClientProfilesController@cost_centers')->name('reports_client_cost_centers');
    Route::get('/reports/client_profiles/cost_centers/download', 'Admin\Reports\ClientProfilesController@cost_centers_download');
    Route::get('/reports/client_profiles/payroll_groups', 'Admin\Reports\ClientProfilesController@payroll_groups')->name('reports_client_payroll_groups');
    Route::get('/reports/client_profiles/payroll_groups/download', 'Admin\Reports\ClientProfilesController@payroll_groups_download');
    Route::get('/reports/client_profiles/contact_persons', 'Admin\Reports\ClientProfilesController@contact_persons')->name('reports_client_contact_persons');
    Route::get('/reports/client_profiles/contact_persons/download', 'Admin\Reports\ClientProfilesController@contact_persons_download');
    Route::get('/reports/client_profiles/contact_list', 'Admin\Reports\ClientProfilesController@contact_list')->name('reports_client_contact_list');

    Route::get('/reports/payroll/listing', 'Admin\Reports\PayrollListingController@index')->name('reports_payroll_listing');
    Route::get('/reports/payroll/listing/download', 'Admin\Reports\PayrollListingController@download');
    Route::get('/reports/payroll/employee', 'Admin\Reports\EmployeeSummaryController@index')->name('reports_payroll_employee');
    Route::get('/reports/payroll/employee/download', 'Admin\Reports\EmployeeSummaryController@download');
    Route::get('/reports/payroll/payslip', 'Admin\Reports\PayrollController@payslip')->name('reports_payroll_payslip');
    Route::get('/reports/payroll/summary_of_payroll', 'Admin\Reports\PayrollController@summary')->name('reports_payroll_summary');
    Route::get('/reports/payroll/summary_of_payroll/download', 'Admin\Reports\PayrollController@summary_download');

//    Route::get('/reports/payroll-listing', 'Admin\Reports\PayrollListingController@index')->name('reports_payroll_listing');

    Route::get('/reports/shares_and_loans/gtmc_request_loan', 'Admin\Reports\SharesAndLoansController@gtmc_request_loan')->name('reports_shares_loans_gtmc');
    Route::get('/reports/shares_and_loans/gtmc_request_loan/download', 'Admin\Reports\SharesAndLoansController@gtmc_request_loan_download');
    Route::get('/reports/shares_and_loans/sss_hdmf_loan', 'Admin\Reports\SharesAndLoansController@sss_hdmf_loan')->name('reports_shares_loans_sss_hdmf');
    Route::get('/reports/shares_and_loans/loan_payment/statement_of_account', 'Admin\Reports\SharesAndLoansController@account_statement')->name('reports_shares_loans_account_statement');
    Route::get('/reports/shares_and_loans/loan_payment/coop_share_contribution', 'Admin\Reports\SharesAndLoansController@coop_share_contribution')->name('reports_shares_loans_coop_share_contribution');
    Route::get('/reports/shares_and_loans/payments', 'Admin\Reports\SharesAndLoansController@payments')->name('reports_payments');
    Route::get('/reports/shares_and_loans/payments/download', 'Admin\Reports\SharesAndLoansController@payments')->name('reports_payments_download');
    Route::get('/reports/shares_and_loans/shares', 'Admin\Reports\SharesAndLoansController@shares')->name('reports_shares');
    Route::get('/reports/shares_and_loans/shares/download', 'Admin\Reports\SharesAndLoansController@shares_download');
    Route::get('/reports/shares_and_loans/individual_shares', 'Admin\Reports\SharesAndLoansController@individual_shares')->name('reports_individual_shares');
    Route::get('/reports/shares_and_loans/individual_loans', 'Admin\Reports\SharesAndLoansController@individual_loans')->name('reports_individual_loans');

    Route::get('/reports/accounting/cash_flow', 'Admin\Reports\AccountingController@cash_flow')->name('reports_accounting_cash_flow');
    Route::get('/reports/accounting/cash_flow/download', 'Admin\Reports\AccountingController@cash_flow_download');
    Route::get('/reports/accounting/sales_book', 'Admin\Reports\AccountingController@sales_book')->name('reports_accounting_sales_book');
    Route::get('/reports/accounting/sales_book/download', 'Admin\Reports\AccountingController@sales_book_download');
    Route::get('/reports/accounting/schedule', 'Admin\Reports\AccountingController@schedule')->name('reports_accounting_schedule');
    Route::get('/reports/accounting/schedule/download', 'Admin\Reports\AccountingController@schedule_download');
    Route::get('/reports/accounting/cash-collection', 'Admin\Reports\AccountingController@cash_collection')->name('reports_accounting_cash_collection');
    Route::get('/reports/accounting/cash-collection/download', 'Admin\Reports\AccountingController@cash_collection_download');
    Route::get('/reports/accounting/cash-disbursement', 'Admin\Reports\AccountingController@cash_disbursement')->name('reports_accounting_cash_disbursement');
    Route::get('/reports/accounting/cash-disbursement/download', 'Admin\Reports\AccountingController@cash_disbursement_download');
    Route::get('/reports/accounting/journal-voucher', 'Admin\Reports\AccountingController@journal_voucher')->name('reports_accounting_journal_voucher');
    Route::get('/reports/accounting/journal-voucher/download', 'Admin\Reports\AccountingController@journal_voucher_download');
    Route::get('/reports/accounting/trial-balance', 'Admin\Reports\AccountingController@trial_balance')->name('reports_accounting_trial_balance');
    Route::get('/reports/accounting/trial-balance/download', 'Admin\Reports\AccountingController@trial_balance_download');
    Route::get('/reports/accounting/balance-sheet', 'Admin\Reports\AccountingController@balance_sheet')->name('reports_accounting_balance_sheet');
    Route::get('/reports/accounting/balance-sheet/download', 'Admin\Reports\AccountingController@balance_sheet_download');
    Route::get('/reports/accounting/income-statement', 'Admin\Reports\AccountingController@income_statement')->name('reports_accounting_income_statement');
    Route::get('/reports/accounting/income-statement/download', 'Admin\Reports\AccountingController@income_statement_download');
    Route::get('/reports/accounting/summary', 'Admin\Reports\AccountingController@summary')->name('reports_accounting_summary');
    Route::get('/reports/accounting/financial_summary', 'Admin\Reports\AccountingController@financial_summary')->name('reports_accounting_financial_summary');

    Route::get('/reports/billing/', 'Admin\Reports\BillingController@index')->name('report_billing');

    Route::get('/reports/government/sss_monthly_contribution', 'Admin\Reports\GovernmentReports@sss_monthly_contribution')->name('reports_sss_monthly_contribution');

    Route::get('/reports/government/hdmf-m1-1', 'Admin\Reports\GovernmentReports@hdmf')->name('reports_hdmf');

    Route::get('/reports/government/philhealth', 'Admin\Reports\GovernmentReports@philhealth')->name('reports_philhealth');
});

Route::group(['prefix' => "client", 'middleware' => 'client'], function () {
    Route::get('/dashboard', 'Client\ClientInformationController@index')->name('client_dashboard');
    Route::get('/contact-persons', 'Client\ContactPersonsController@index')->name('client_contact_persons');
    Route::get('/cost-centers', 'Client\CostCentersController@index')->name('client_cost_centers');
    Route::get('/billings', 'Client\BillingsController@index')->name('client_billings');
    Route::get('/user-profiles/{status?}', 'Client\UserProfilesController@index')->name('client_user_profiles');
    Route::get('/user-profile/{id_number}', 'Client\UserProfilesController@profile');
    Route::get('/user-profile/{id_number}/employment-details', 'Client\UserProfilesController@employment_details');
    Route::get('/user-profile/{id_number}/compensations-leave-credits', 'Client\UserProfilesController@compensation');
    Route::get('/user-profile/{id_number}/loans-shares', 'Client\UserProfilesController@coop');
    Route::get('/payroll', 'Client\PayrollsController@index')->name('client_payrolls');
    Route::get('/invoices', 'Client\InvoicesController@index')->name('client_invoices');
    Route::get('/payroll-listings', 'Client\PayrollListingsController@index')->name('client_payroll_listings');
    Route::get('/payroll-listings/edit/{id_number}', 'Client\PayrollListingsController@edit');
    Route::post('/payroll-listings/edit/{id_number}', 'Client\PayrollListingsController@edit_post');

//    Route::get('/contact-persons', 'Client\ContactPersonsController@index')->name('client_contact_persons');
//    Route::get('/contact-persons/add', 'Client\ContactPersonsController@add_contact_persons')->name('client_contact_persons_add');
//
//    Route::get('/payroll-groups', 'Client\PayrollGroupsController@index')->name('client_payroll_groups');
//    Route::get('/payroll-groups/add', 'Client\PayrollGroupsController@add_payroll_groups')->name('client_payroll_groups_add');
//
//    Route::get('/cost-center', 'Client\CostCenterController@index')->name('client_cost_center');
//    Route::get('/cost-center/add', 'Client\CostCenterController@add_cost_center')->name('client_cost_center_add');
//
//    Route::get('/billing-details', 'Client\BillingController@billing_details')->name('client_billing_details');
//    Route::get('/billing-details/update', 'Client\BillingController@billing_details_update')->name('client_billing_details_update');
//
//    Route::get('/invoices/', 'Client\BillingController@invoices')->name('client_invoices');
});

Route::group(['prefix' => "user", 'middleware' => 'employee'], function () {
    /*Route::get('/home', 'UserController@index')->name('user_home');*/
    Route::get('/dashboard', 'DashboardController@dashboard')->name('user_dashboard');

    Route::get('/personal_information', 'User\PersonalInformationController@index')->name('user_personal_information');

    Route::get('/employment_details', 'User\EmploymentDetails@index')->name('user_employment_details');

    Route::get('/compensation/leave_credits', 'User\Compensation\LeaveCreditsController@index')->name('user_leave_credits');

    Route::get('/compensation/payslip', 'User\Compensation\PayslipController@index')->name('user_payslip');

    Route::get('/loans_and_shares/sss_contribution', 'User\LoansAndShares\SSSController@contribution')->name('user_sss_contribution');
    Route::get('/loans_and_shares/sss_loan', 'User\LoansAndShares\SSSController@loan')->name('user_sss_loan');

    Route::get('/loans_and_shares/hdmf_contribution', 'User\LoansAndShares\HDMFController@contribution')->name('user_hdmf_contribution');
    Route::get('/loans_and_shares/hdmf_loan', 'User\LoansAndShares\HDMFController@loan')->name('user_hdmf_loan');

    Route::get('/loans_and_shares/philhealth_contribution', 'User\LoansAndShares\PhilHealthController@contribution')->name('user_philhealth_contribution');

    Route::get('/loans_and_shares/coop_share', 'User\LoansAndShares\CoopController@share')->name('user_coop_share');
    Route::get('/loans_and_shares/coop_savings', 'User\LoansAndShares\CoopController@savings')->name('user_coop_savings');
    Route::get('/loans_and_shares/coop_loans', 'User\LoansAndShares\CoopController@loans')->name('user_coop_loans');
});

// PRINT

Route::group(['prefix' => "print"], function () {
    Route::get('payslip/{payrollCycleId}/{dtrId}/detailed', 'PrintController@payslip');
    Route::get('payslip/{payrollCycleId}/{dtrId}/actual', 'PrintController@payslip');
    Route::get('payslips/{payrollCycleId}', 'PrintController@payslips');


    Route::get('loan/loan-amortization-schedule/{loanId}', 'PrintController@loan_amortization_schedule');
});


// API

Route::group(['prefix' => "api"], function () {
    Route::get('users/employees/{status?}', 'ApiController@employees');
    Route::get('users/members', 'ApiController@members');
    Route::get('users/all/{status?}', 'ApiController@users');

    Route::get('clients', 'ApiController@clients');
    Route::get('payee-names', 'ApiController@payee_names');

    Route::post('datatables/users/{status?}', 'DatatablesController@users');
    Route::post('datatables/clients', 'DatatablesController@clients');
    Route::get('cost-centers/{id}', 'ApiController@cost_centers');
    Route::get('payroll-groups/{id}', 'ApiController@payroll_groups');
    Route::post('department/areas', 'ApiController@department_areas');
    Route::post('department/malls', 'ApiController@department_malls');
    Route::post('department/outlets', 'ApiController@department_outlets');
    Route::post('department/departments', 'ApiController@department_departments');
    Route::post('transfer-mode/types', 'ApiController@transfer_mode_types');
    Route::post('transfer-mode/banks', 'ApiController@transfer_mode_banks');

    Route::get('users/search/add/admin', 'ApiController@users_search_add_admin');
    Route::post('datatables/admin/users', 'DatatablesController@admin_users');
    Route::post('datatables/admin/clients', 'DatatablesController@admin_clients');
    Route::post('datatables/loans/{status}', 'DatatablesController@loans');

    Route::post('datatables/user/loans/{user_id}', 'DatatablesController@user_loans');

    Route::post('datatables/unpaid-loans', 'DatatablesController@unpaid_loans');
    Route::post('datatables/other-payments/{type}', 'DatatablesController@payments_coop_shares');

    // Invoice Item Add
    Route::get('/payroll-for-invoice-item/{payroll_cycle_id}', function ($payroll_cycle_id) {

        $payroll_dtr = \App\PayrollDtr::where('payroll_cycle_id', $payroll_cycle_id)->get();
        $result = [
            'total_head_count' => $payroll_dtr->count(),
            'total_hours' => $payroll_dtr->sum('rh'),
            'billing_amount' => \App\PayrollSummary::where('payroll_cycle_id', $payroll_cycle_id)->get()->sum('total_billing')
        ];

        return json_encode($result);
    });

    Route::get('/cash-receipt/invoice-number', function (Illuminate\Http\Request $request) {
        $payorName = $request->payor_name;

        $data = \App\Invoices::where(['company_name' => $payorName, 'status' => 'Unpaid'])->get()->toJson();

        return $data;
    });

    Route::get('/cash-flow/check-number', function (Illuminate\Http\Request $request) {
        $checkNumber = $request->check_number;
        $bank = $request->bank;

        $data = \App\CashFlows::where(['bank' => $bank])->whereRaw("CAST(`check_number` AS UNSIGNED) < $checkNumber")->where('check_number', '!=', '')->orderByDesc('cv_number')->first();

        return $data;
    });


    // Reports

    Route::get('reports/users/{membershipCategory}', 'ApiController@reports_users');
    Route::get('reports/individual-loans/{idNumber}', 'ApiController@reports_individual_loans');
    Route::get('reports/individual-shares/{idNumber}', 'ApiController@reports_individual_shares');
});

/*
 * Member Middleware
 */
Route::group(['prefix' => "member", 'middleware' => 'member'], function () {
    Route::get('/profile', 'User\PersonalInformationController@index')->name('member_profile');

    Route::get('/personal_information', 'User\PersonalInformationController@index')->name('member_personal_information');

    Route::get('/cooperative', 'User\Cooperative@index')->name('member_coop');

    Route::get('/loans', 'User\LoansController@index')->name('member_loans');

    Route::get('/contributions/shares', 'User\Shares@index')->name('member_shares');



//    Route::get('/profile', 'Member\ProfileController@index')->name('member_profile');
//
//    Route::get('/user/{id}/loans-shares', 'Member\ProfileController@loans_shares')->name('member_loan_shares');
//    Route::get('/user/{id}/loans-payments', 'Member\ProfileController@loan_payments')->name('member_loan_payments');

});

/*
 * Member Middleware
 */
Route::group(['prefix' => "employee", 'middleware' => 'employee'], function () {
    Route::get('/profile', 'User\PersonalInformationController@index')->name('employee_profile');

    Route::get('/dashboard', 'DashboardController@dashboard')->name('user_dashboard');

    Route::get('/personal_information', 'User\PersonalInformationController@index')->name('user_personal_information');

    Route::get('/employment_details', 'User\EmploymentDetails@index')->name('user_employment_details');

    Route::get('/compensation', 'User\Compensation@index')->name('user_compensation');

    Route::get('/leave_credits', 'User\LeaveCredits@index')->name('user_leave_credits');

    Route::get('/cooperative', 'User\Cooperative@index')->name('user_coop');

    Route::get('/loans', 'User\LoansController@index')->name('user_loans');
    Route::get('/loan/{cla_number}', 'User\LoansController@details');
    Route::get('/loan/{cla_number}/loan-amortization-schedule', 'User\LoansController@las');
    Route::get('/loan/{cla_number}/payment-schedule', 'User\LoansController@payment_schedule');
    Route::get('/loan/{cla_number}/payments', 'User\LoansController@payments');

    Route::get('/payslip', 'User\PaySlip@index')->name('user_payslip');

    Route::get('/contributions/shares', 'User\Shares@index')->name('user_shares');

    Route::get('/contributions/sss', 'User\ContributionSSS@index')->name('user_sss');

    Route::get('/contributions/hdmf', 'User\ContributionHDMF@index')->name('user_hdmf');

    Route::get('/contributions/phic', 'User\ContributionPHIC@index')->name('user_phic');

//    Route::get('/compensation/payslip', 'User\Compensation\PayslipController@index')->name('user_payslip');

//    Route::get('/loans_and_shares/sss_contribution', 'User\LoansAndShares\SSSController@contribution')->name('user_sss_contribution');
//    Route::get('/loans_and_shares/sss_loan', 'User\LoansAndShares\SSSController@loan')->name('user_sss_loan');
//
//    Route::get('/loans_and_shares/hdmf_contribution', 'User\LoansAndShares\HDMFController@contribution')->name('user_hdmf_contribution');
//    Route::get('/loans_and_shares/hdmf_loan', 'User\LoansAndShares\HDMFController@loan')->name('user_hdmf_loan');

    Route::get('/loans_and_shares/philhealth_contribution', 'User\LoansAndShares\PhilHealthController@contribution')->name('user_philhealth_contribution');

    Route::get('/loans_and_shares/coop_share', 'User\LoansAndShares\CoopController@share')->name('user_coop_share');
    Route::get('/loans_and_shares/coop_savings', 'User\LoansAndShares\CoopController@savings')->name('user_coop_savings');
    Route::get('/loans_and_shares/coop_loans', 'User\LoansAndShares\CoopController@loans')->name('user_coop_loans');


    Route::get('/loans-and-shares', 'Member\ProfileController@loans_shares')->name('employee_loan_and_shares');
});

Route::group(['prefix' => "settings"], function () {
    Route::get('/change-password', 'SettingsController@change_password');
    Route::post('/change-password', 'SettingsController@change_password_post');
});

/*
 * Modals Route
 */
Route::group(['prefix' => "modal"], function () {
    // Cost Center
    Route::get('/cost-center-add/{client_id}', function ($client_id) {
        return view('modal.cost_center_add', compact('client_id'));
    });

    Route::get('/cost-center-edit/{client_id}/{id}', function ($client_id, $id) {
        $costCenter = \App\CostCenters::find($id);
        return view('modal.cost_center_edit', compact('client_id', 'costCenter'));
    });

    // Payroll Group
    Route::get('/payroll-group-add/{client_id}', function ($client_id) {
        return view('modal.payroll_group_add', compact('client_id'));
    });
    Route::get('/payroll-group-edit/{client_id}/{id}', function ($client_id, $id) {
        $payrollGroup = \App\PayrollGroups::find($id);
        return view('modal.payroll_group_edit', compact('client_id', 'payrollGroup'));
    });

    // Contact Person
    Route::get('/contact-person-add/{client_id}', function ($client_id) {
        return view('modal.contact_person_add', compact('client_id'));
    });
    Route::get('/contact-person-edit/{client_id}/{cp_id}', function ($client_id, $cp_id) {
        $contactPerson = \App\CompaniesContactPersons::find($cp_id);
        return view('modal.contact_person_edit', compact('client_id', 'contactPerson'));
    });

    // Beneficiary
    Route::get('/beneficiary-add/{user_id}', function ($user_id) {
        return view('modal.beneficiary_add', compact('user_id'));
    });
    Route::get('/beneficiary-edit/{user_id}/{id}', function ($user_id, $id) {
        $beneficiary = \App\Beneficiaries::find($id);
        return view('modal.beneficiary_edit', compact('user_id', 'beneficiary'));
    });

    // Positions
    Route::get('/position-edit/{id}', function ($id) {
        $position = \App\Positions::find($id);
        return view('modal.position_edit', compact('position'));
    });

    // Member Relationships
    Route::get('/relationship-edit/{id}', function ($id) {
        $relationship = App\MemberRelationships::find($id);
        return view('modal.relationship_edit', compact('relationship'));
    });

    // Transfer Mode Banks
    Route::get('/bank-edit/{id}', function ($id) {
        $bank = \App\TransferModes::find($id);
        return view('modal.bank_edit', compact('bank'));
    });

    // Tax Type
    // To be Fixed
    Route::get('/tax-edit/{id}', function ($id) {
        $data['tax'] = \App\TaxTypes::find($id);
        $data['tax_types'] = \App\TaxTypes::all();
        return view('modal.tax_type_edit', $data);
    });

    // Educational Attainment
    Route::get('/educational-attainment-edit/{id}', function ($id) {
        $educationalAttainment = \App\EducationalAttainments::find($id);
        return view('modal.educational_attainment_edit', compact('educationalAttainment'));
    });

    // Educational Attainment
    Route::get('/civil-status-edit/{id}', function ($id) {
        $civilStatus = \App\CivilStatus::find($id);
        return view('modal.civil_status_edit', compact('civilStatus'));
    });

    // Department
    Route::get('/department-edit/{id}', function ($id) {
        $department = \App\Departments::find($id);
        return view('modal.department_edit', compact('department'));
    });

    // Daily Rates
    Route::get('/daily-rate-edit/{id}', function ($id) {
        $dailyRate = \App\DailyRates::find($id);
        return view('modal.daily_rate_edit', compact('dailyRate'));
    });

    // Mode of Transfer
    Route::get('/mode-of-transfer-edit/{id}', function ($id) {
        $transferMode = \App\TransferModes::find($id);
        return view('modal.mode_of_transfer_edit', compact('transferMode'));
    });

    // Payee Names
    Route::get('/payee-name-edit/{id}', function ($id) {
        $payeeName = \App\PayeeNames::find($id);
        return view('modal.payee_name_edit', compact('payeeName'));
    });

    // SSS Contribution
    Route::get('/sss-contribution-edit/{id}', function ($id) {
        $SSSContribution = \App\SSSContributions::find($id);
        return view('modal.sss_contribution_edit', compact('SSSContribution'));
    });

    // PhilHealth Contribution
    Route::get('/philhealth-contribution-edit/{id}', function ($id) {
        $PhilHealthContribution = \App\PhilHealthContributions::find($id);
        return view('modal.philhealth_contribution_edit', compact('PhilHealthContribution'));
    });

    // HDMF Contribution
    Route::get('/hdmf-contribution-edit/{id}', function ($id) {
        $HDMFContribution = \App\HDMFContributions::find($id);
        return view('modal.hdmf_contribution_edit', compact('HDMFContribution'));
    });

    // Province
    Route::get('/province-edit/{id}', function ($id) {
        $province = \App\Provinces::find($id);
        return view('modal.province_edit', compact('province'));
    });

    // Area
    Route::get('/area-edit/{id}', function ($id) {
        $area = \App\Areas::find($id);
        return view('modal.area_edit', compact('area'));
    });

    // Mall
    Route::get('/mall-edit/{id}', function ($id) {
        $mall = \App\Malls::find($id);
        return view('modal.mall_edit', compact('mall'));
    });

    // Outlet
    Route::get('/outlet-edit/{id}', function ($id) {
        $outlet = \App\Outlets::find($id);
        return view('modal.outlet_edit', compact('outlet'));
    });

    // Business Tax TYPE
    Route::get('/business-tax-type-edit/{id}', function ($id) {
        $businessTaxType = \App\BusinessTaxTypes::find($id);
        return view('modal.business_tax_type_edit', compact('businessTaxType'));
    });

    // Loan TYPE
    Route::get('/loan-type-edit/{id}', function ($id) {
        $loanType = \App\LoanTypes::find($id);
        return view('modal.loan_type_edit', compact('loanType'));
    });


    // Income Deduction Items
    Route::get('/income-deduction-item-edit/{id}', function ($id) {
        $item = \App\IncomeDeductionItems::find($id);
        return view('modal.income_deduction_item_edit', compact('item'));
    });

    // Income Deduction Items
    Route::get('/admin-breakdown/{id}', function ($id) {
        $company = \App\Companies::find($id);
        return view('modal.admin_breakdown', compact('company'));
    });

});

Route::get('/authenticate', function () {
//    dd(Auth::check());
    if (!Auth::check()) {
        return redirect('/login');
    }

    $user = \App\User::find(Auth::user()->id);

//    dd($user);


    if ($user->username == 'admin' || (!empty(session('is_admin')) && session('is_admin') == TRUE)) {
        return redirect('/admin/dashboard');
    }

//    dd(in_array($user->role, ['Admin', 'AdminPayroll', 'AdminCoop', 'AdminHR', 'AdminAcctg']));

    if(in_array($user->role, ['Admin', 'AdminPayroll', 'AdminCoop', 'AdminHR', 'AdminAcctg'])) {
        if($user->is_active == 0) {
            return redirect('/logout');
        }

        return redirect('/admin/dashboard');
    }

    if (!empty(session('is_client')) && session('is_client') == TRUE) {
        return redirect('/client/dashboard');
    }

    if ($user->role == 'Employee' && !empty($user->Profile->EmploymentDetails) && $user->Profile->EmploymentDetails->reason == 'Resigned') {
        return redirect('/logout');
    }


    if (!empty($user->Profile) && $user->Profile->status == 'Deactivated') {
        return redirect('/logout');
    }

    if (in_array($user->role, ['ClientAdmin', 'ClientUser'])) {
        return redirect('/client/contact-persons');
    }



//    if (($user->role == 'Employee' && !empty($user->admin_type))) {
//        return redirect('/admin/dashboard');
//    }

    if ($user->role == 'Employee') {
        return redirect()->route('employee_profile');
    }

    if ($user->role == 'Member') {
        return redirect()->route('member_profile');
    }


    return redirect('/admin/dashboard');
});

Route::get('upload-employees', 'AdminController@uploadEmployees');
Route::get('upload-members', 'AdminController@uploadMembers');
Route::get('upload-provinces', 'AdminController@uploadProvinces');
Route::get('upload-areas', 'AdminController@uploadAreas');
Route::get('upload-malls', 'AdminController@uploadMalls');
Route::get('upload-outlets', 'AdminController@uploadOutlets');
//Route::get('user-series', function() {
//   for($x = 7453; $x < \App\User::max('series'); $x++) {
//        if(\App\Profiles::where('series', $x)->count() == 0) {
//            \App\UserSeries::create(['series' => $x]);
//        }
//   }
//});

Route::get('update-departments', function () {
    foreach (\App\EmploymentDetails::get() as $e) {
        if (!empty($e->Department)) {
//            dd($e->Department);
            $d = $e->Department;

            if (\App\Provinces::where('province', $d->province)->count() > 0) {
                $e->province_id = \App\Provinces::where('province', $d->province)->first()->id;
                $e->save();
            }

            if (!empty($d->province) && !empty($d->area)) {
                $province = \App\Provinces::where('province', $d->province)->first();
                if (\App\Areas::where('province_id', $province->id)->where('area', $d->area)->count() > 0) {
                    $e->area_id = \App\Areas::where('province_id', $province->id)->where('area', $d->area)->first()->id;
                    $e->save();
                }
            } elseif (empty($d->province) && !empty($d->area)) {
                if (\App\Areas::where('area', $d->area)->count() > 0) {
                    $e->area_id = \App\Areas::where('area', $d->area)->first()->id;
                    $e->save();
                }
            }

            if (\App\Malls::where('mall', $d->mall)->count() > 0) {
                $e->mall_id = \App\Malls::where('mall', $d->mall)->first()->id;
                $e->save();
            }

            if (\App\Outlets::where('outlet', $d->outlet)->count() > 0) {
                $e->outlet_id = \App\Outlets::where('outlet', $d->outlet)->first()->id;
                $e->save();
            }
        }
    }
});

Route::get('/check-username', function () {
    $username = \Illuminate\Support\Facades\Input::get('username');

    $status = 'success';
    if (\App\User::where('username', $username)->exists()) {
        $status = 'failed';
    }

    return $status;
});


Route::get('/cron/deactivate-employment-details', function () {
//    $employmentDetails = \App\EmploymentDetails::where('effective_date', '<=', date('Y-m-d'))->whereNotNull('reason')->get();
    $employmentDetails = \App\EmploymentDetails::join('profiles as p', 'p.user_id', '=', 'employment_details.user_id', 'INNER')->where('effective_date', '<=', date('Y-m-d'))->where('p.status', 'Active')->whereNotNull('reason')->get();
//    dd($employmentDetails);
    if (!empty($employmentDetails)) {
        foreach ($employmentDetails as $employmentDetail) {
            \App\EmploymentDetails::where('user_id', $employmentDetail->user_id)->update(['status' => 'Inactive']);
            \App\Profiles::where('user_id', $employmentDetail->user_id)->update(['status' => 'Inactive']);
        }
    }
});

Route::get('/cron/final-pay/disburse', function () {
//    $finalPays = \App\FinalPays::where(['status' => 'Released', 'released_date' => date('Y-m-d')])->get();
//
//    foreach($finalPays as $finalPay) {
//        \App\EmploymentDetails::where('user_id', $finalPay->user_id)->update(['status' => 'Inactive']);
//        \App\Profiles::where('user_id', $finalPay->user_id)->update(['status' => 'Inactive']);
//
//        $coopWithdrawal = [
//            'ar_number' => 'AR-000',
//            'payment_date' => $finalPay->released_date,
//            'transaction_type' => 'COOP Withdrawal',
//            'payment_method' => 'Cash',
//            'user_id' => $finalPay->user_id,
//            'pay_period' => '',
//            'amount' => $finalPay->cash_savings,
//            'total_amount_paid' => $finalPay->cash_savings
//        ];
//        OtherPayments::create($coopWithdrawal);
//    }
});

Route::get('/cron/update-payment-schedule', function () {
    \Illuminate\Support\Facades\DB::statement(\Illuminate\Support\Facades\DB::raw("
        UPDATE
        loan_payment_schedules lps
        INNER JOIN (
            SELECT * FROM (
                SELECT
                    ps.id,
                    ps.amortization * (lt.interest * .01) as lapsed_interest_amount
                FROM loan_payment_schedules ps
                LEFT JOIN loans l
                    ON l.id = ps.loan_id
                LEFT JOIN loan_types lt
                    ON lt.id = l.loan_type_id
                WHERE ps.id IN (
                    SELECT 
                    ps.id
                    FROM loan_payment_schedules ps
                    LEFT JOIN loans l ON l.id = ps.loan_id
                    WHERE
                        DATE_ADD(`due_date`, INTERVAL 5 DAY) < CURDATE()
                        AND
                        l.is_voucher = 'N'
                        AND
                        l.is_paid = 'N'
                        AND
                        ps.lapsed_interest = 0
                )
            ) t1
        ) lps2
            ON lps.id = lps2.id
        SET
            lps.lapsed_interest = lps2.lapsed_interest_amount 
    "));
});